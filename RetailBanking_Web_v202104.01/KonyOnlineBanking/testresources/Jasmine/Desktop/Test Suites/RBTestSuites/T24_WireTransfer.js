describe("T24_WireTransfer", function() {
	beforeEach(async function() {
	
	  //await kony.automation.playback.wait(10000);
	  strLogger=[];
	  var formName='';
	  formName=kony.automation.getCurrentForm();
	  appLog('Inside before Each function');
	  appLog("Current form name is  :: "+formName);
	  if(formName==='frmDashboard'){
	    appLog('Already in dashboard');
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else if(await kony.automation.playback.waitFor([formName,"customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from '+formName);
	    kony.automation.flexcontainer.click([formName,"customheader","topmenu","flxaccounts"]);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else if(await kony.automation.playback.waitFor([formName,"customheadernew","topmenu","flxAccounts"],5000)){
	    appLog('Moving back from '+formName);
	    kony.automation.flexcontainer.click([formName,"customheadernew","topmenu","flxAccounts"]);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else{
	    appLog("Form name is not available");
	  }
	
	//   if(await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000)){
	//     appLog('Already in dashboard');
	//   }else if(await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxUserName"],5000)){
	//     appLog('Inside Login Screen');
	//   }else if(await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmAccountsDetails');
	//     kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmProfileManagement');
	//     kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmManageBeneficiaries","customheadernew","topmenu","flxAccounts"],5000)){
	//     appLog('Moving back from frmManageBeneficiaries');
	//     kony.automation.flexcontainer.click(["frmManageBeneficiaries","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryConfirmEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryAcknowledgementEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakePayment","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakePayment');
	//     kony.automation.flexcontainer.click(["frmMakePayment","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConfirmEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmConfirmEuro');
	//     kony.automation.flexcontainer.click(["frmConfirmEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAcknowledgementEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAcknowledgementEuro');
	//     kony.automation.flexcontainer.click(["frmAcknowledgementEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPastPaymentsEurNew');
	//     kony.automation.flexcontainer.click(["frmPastPaymentsEurNew","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmScheduledPaymentsEurNew');
	//     kony.automation.flexcontainer.click(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmConsolidatedStatements');
	//     kony.automation.flexcontainer.click(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCardManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmCardManagement');
	//     kony.automation.flexcontainer.click(["frmCardManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBulkPayees');
	//     kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayee","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakeOneTimePayee');
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayment","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakeOneTimePayment');
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmOneTimePaymentConfirm');
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmOneTimePaymentAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmManagePayees');
	//     kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayABill');
	//     kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayBillConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayBillConfirm');
	//     kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayBillAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayScheduled');
	//     kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddPayee1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddPayee1');
	//     kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddPayeeInformation","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddPayeeInformation');
	//     kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayeeDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayeeDetails');
	//     kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmVerifyPayee","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmVerifyPayee');
	//     kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayeeAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayHistory');
	//     kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayActivation","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayActivation');
	//     kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayActivationAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountConfirm');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountAck');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountConfirm');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountAck');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersWindow","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersWindow');
	//     kony.automation.flexcontainer.click(["frmWireTransfersWindow","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferMakeTransfer');
	//     kony.automation.flexcontainer.click(["frmWireTransferMakeTransfer","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConfirmDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmConfirmDetails');
	//     kony.automation.flexcontainer.click(["frmConfirmDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep3');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep3","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTemplatePrimaryDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTemplatePrimaryDetails');
	//     kony.automation.flexcontainer.click(["frmCreateTemplatePrimaryDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBulkTemplateAddRecipients');
	//     kony.automation.flexcontainer.click(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTempSelectRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTempSelectRecipients');
	//     kony.automation.flexcontainer.click(["frmCreateTempSelectRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTempAddDomestic');
	//     kony.automation.flexcontainer.click(["frmCreateTempAddDomestic","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersRecent","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersRecent');
	//     kony.automation.flexcontainer.click(["frmWireTransfersRecent","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersManageRecipients');
	//     kony.automation.flexcontainer.click(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmActivateWireTransfer","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmActivateWireTransfer');
	//     kony.automation.flexcontainer.click(["frmActivateWireTransfer","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmPersonalFinanceManagement');
	//     kony.automation.flexcontainer.click(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCustomViews","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmCustomViews');
	//     kony.automation.flexcontainer.click(["frmCustomViews","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmStopPayments","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmStopPayments');
	//     kony.automation.flexcontainer.click(["frmStopPayments","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmNAO","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmNAO');
	//     kony.automation.flexcontainer.click(["frmNAO","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmSavingsPotLanding","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmSavingsPotLanding');
	//     kony.automation.flexcontainer.click(["frmSavingsPotLanding","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateSavingsPot","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateSavingsPot');
	//     kony.automation.flexcontainer.click(["frmCreateSavingsPot","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateSavingsGoal","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateSavingsGoal');
	//     kony.automation.flexcontainer.click(["frmCreateSavingsGoal","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateGoalConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateGoalConfirm');
	//     kony.automation.flexcontainer.click(["frmCreateGoalConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateGoalAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateGoalAck');
	//     kony.automation.flexcontainer.click(["frmCreateGoalAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudget","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudget');
	//     kony.automation.flexcontainer.click(["frmCreateBudget","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmEditGoal","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmEditGoal');
	//     kony.automation.flexcontainer.click(["frmEditGoal","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudgetConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudgetConfirm');
	//     kony.automation.flexcontainer.click(["frmCreateBudgetConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudgetAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudgetAck');
	//     kony.automation.flexcontainer.click(["frmCreateBudgetAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmEditBudget","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmEditBudget');
	//     kony.automation.flexcontainer.click(["frmEditBudget","customheadernew","flxAccounts"]);
	//   }else{
	//     appLog("Form name is not available");
	//   }
	  
	},480000);
	
	afterEach(async function() {
	
	  //await kony.automation.playback.wait(10000);
	  
	  var formName='';
	  formName=kony.automation.getCurrentForm();
	  appLog('Inside after Each function');
	  appLog("Current form name is  :: "+formName);
	  if(formName==='frmDashboard'){
	    appLog('Already in dashboard');
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else if(await kony.automation.playback.waitFor([formName,"customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from '+formName);
	    kony.automation.flexcontainer.click([formName,"customheader","topmenu","flxaccounts"]);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else if(await kony.automation.playback.waitFor([formName,"customheadernew","topmenu","flxAccounts"],5000)){
	    appLog('Moving back from '+formName);
	    kony.automation.flexcontainer.click([formName,"customheadernew","topmenu","flxAccounts"]);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else{
	    appLog("Form name is not available");
	  }
	
	//   if(await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000)){
	//     appLog('Already in dashboard');
	//   }else if(await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxUserName"],5000)){
	//     appLog('Inside Login Screen');
	//   }else if(await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmAccountsDetails');
	//     kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmProfileManagement');
	//     kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmManageBeneficiaries","customheadernew","topmenu","flxAccounts"],5000)){
	//     appLog('Moving back from frmManageBeneficiaries');
	//     kony.automation.flexcontainer.click(["frmManageBeneficiaries","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryConfirmEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryAcknowledgementEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakePayment","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakePayment');
	//     kony.automation.flexcontainer.click(["frmMakePayment","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConfirmEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmConfirmEuro');
	//     kony.automation.flexcontainer.click(["frmConfirmEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAcknowledgementEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAcknowledgementEuro');
	//     kony.automation.flexcontainer.click(["frmAcknowledgementEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPastPaymentsEurNew');
	//     kony.automation.flexcontainer.click(["frmPastPaymentsEurNew","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmScheduledPaymentsEurNew');
	//     kony.automation.flexcontainer.click(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmConsolidatedStatements');
	//     kony.automation.flexcontainer.click(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCardManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmCardManagement');
	//     kony.automation.flexcontainer.click(["frmCardManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBulkPayees');
	//     kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayee","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakeOneTimePayee');
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayment","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakeOneTimePayment');
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmOneTimePaymentConfirm');
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmOneTimePaymentAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmManagePayees');
	//     kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayABill');
	//     kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayBillConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayBillConfirm');
	//     kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayBillAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayScheduled');
	//     kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddPayee1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddPayee1');
	//     kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddPayeeInformation","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddPayeeInformation');
	//     kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayeeDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayeeDetails');
	//     kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmVerifyPayee","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmVerifyPayee');
	//     kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayeeAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayHistory');
	//     kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayActivation","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayActivation');
	//     kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayActivationAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountConfirm');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountAck');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountConfirm');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountAck');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersWindow","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersWindow');
	//     kony.automation.flexcontainer.click(["frmWireTransfersWindow","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferMakeTransfer');
	//     kony.automation.flexcontainer.click(["frmWireTransferMakeTransfer","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConfirmDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmConfirmDetails');
	//     kony.automation.flexcontainer.click(["frmConfirmDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep3');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep3","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTemplatePrimaryDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTemplatePrimaryDetails');
	//     kony.automation.flexcontainer.click(["frmCreateTemplatePrimaryDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBulkTemplateAddRecipients');
	//     kony.automation.flexcontainer.click(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTempSelectRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTempSelectRecipients');
	//     kony.automation.flexcontainer.click(["frmCreateTempSelectRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTempAddDomestic');
	//     kony.automation.flexcontainer.click(["frmCreateTempAddDomestic","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersRecent","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersRecent');
	//     kony.automation.flexcontainer.click(["frmWireTransfersRecent","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersManageRecipients');
	//     kony.automation.flexcontainer.click(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmActivateWireTransfer","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmActivateWireTransfer');
	//     kony.automation.flexcontainer.click(["frmActivateWireTransfer","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmPersonalFinanceManagement');
	//     kony.automation.flexcontainer.click(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCustomViews","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmCustomViews');
	//     kony.automation.flexcontainer.click(["frmCustomViews","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmStopPayments","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmStopPayments');
	//     kony.automation.flexcontainer.click(["frmStopPayments","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmNAO","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmNAO');
	//     kony.automation.flexcontainer.click(["frmNAO","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmSavingsPotLanding","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmSavingsPotLanding');
	//     kony.automation.flexcontainer.click(["frmSavingsPotLanding","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateSavingsPot","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateSavingsPot');
	//     kony.automation.flexcontainer.click(["frmCreateSavingsPot","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateSavingsGoal","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateSavingsGoal');
	//     kony.automation.flexcontainer.click(["frmCreateSavingsGoal","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateGoalConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateGoalConfirm');
	//     kony.automation.flexcontainer.click(["frmCreateGoalConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateGoalAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateGoalAck');
	//     kony.automation.flexcontainer.click(["frmCreateGoalAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudget","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudget');
	//     kony.automation.flexcontainer.click(["frmCreateBudget","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmEditGoal","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmEditGoal');
	//     kony.automation.flexcontainer.click(["frmEditGoal","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudgetConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudgetConfirm');
	//     kony.automation.flexcontainer.click(["frmCreateBudgetConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudgetAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudgetAck');
	//     kony.automation.flexcontainer.click(["frmCreateBudgetAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmEditBudget","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmEditBudget');
	//     kony.automation.flexcontainer.click(["frmEditBudget","customheadernew","flxAccounts"]);
	//   }else{
	//     appLog("Form name is not available");
	//   }
	  
	  //strLogger=null;
	
	},480000);
	
	async function NavigateToWireTransfer_AddRecipitent(){
	
	  appLog("Intiated method to Navigate AddRecipitent Screen");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransfer3flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransfer3flxMyAccounts"]);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","lblAddAccountHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddKonyAccountStep1","lblAddAccountHeading"], "text")).not.toBe("");
	
	  appLog("successfully Navigated to AddRecipitent Screen");
	
	}
	
	async function EnterDomestic_RecipitentDetails_Step1(RecipientName,AddressLine1,AddressLine2,City,ZipCode){
	
	  appLog("Intiated method to Enter Domestic Recipitent details - Step1");
	
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","tbxRecipientName"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep1","tbxRecipientName"],RecipientName);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","tbxAddressLine1"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep1","tbxAddressLine1"],AddressLine1);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","tbxAddressLine2"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep1","tbxAddressLine2"],AddressLine2);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","tbxCity"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep1","tbxCity"],City);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","lbxState"],15000);
	  kony.automation.listbox.selectItem(["frmWireTransferAddKonyAccountStep1","lbxState"], "Dubai");
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","tbxZipcode"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep1","tbxZipcode"],ZipCode);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","btnProceed"],15000);
	  kony.automation.button.click(["frmWireTransferAddKonyAccountStep1","btnProceed"]);
	
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","lblStep2"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddKonyAccountStep2","lblStep2"], "text")).toContain("Step 2");
	
	  appLog("Successfully Entered Domestic Recipitent details - Step1");
	}
	
	async function EnterDomestic_BankDetails_Step2(RoutingNumber,AccNumber,NickName,BankName,BankAddressLine1,BankAddressLine2,BankCity,BankZipcode){
	
	  appLog("Intiated method to Enter Domestic Bank details - Step2");
	
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxSwiftCode"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxSwiftCode"],RoutingNumber);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxAccountNumber"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxAccountNumber"],AccNumber);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxReAccountNumber"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxReAccountNumber"],AccNumber);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxNickName"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxNickName"],NickName);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxBankName"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxBankName"],BankName);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxBankAddressLine1"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxBankAddressLine1"],BankAddressLine1);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxBankAddressLine2"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxBankAddressLine2"],BankAddressLine2);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxBankCity"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxBankCity"],BankCity);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","lbxBankState"],15000);
	  kony.automation.listbox.selectItem(["frmWireTransferAddKonyAccountStep2","lbxBankState"], "Dubai");
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","tbxBankZipcode"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddKonyAccountStep2","tbxBankZipcode"],BankZipcode);
	  await kony.automation.scrollToWidget(["frmWireTransferAddKonyAccountStep2","btnAddRecipent"]);
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","btnAddRecipent"],15000);
	  kony.automation.button.click(["frmWireTransferAddKonyAccountStep2","btnAddRecipent"]);
	
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountConfirm","lblAddAccountHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddKonyAccountConfirm","lblAddAccountHeading"], "text")).not.toBe("");
	
	  appLog("Successfully Entered Domestic Bank details - Step2");
	
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountConfirm","btnConfirm"],15000);
	  kony.automation.button.click(["frmWireTransferAddKonyAccountConfirm","btnConfirm"]);
	
	  appLog("Successfully Clicked on Confirm Button");
	
	}
	
	async function VerifyAddDomesticRecipitentSuccessMsg(){
	
	  appLog("Intiated method to verify AddDomesticRecipitent SuccessMsg");
	
	  var Success=await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountAck","lblSuccessAcknowledgement"],15000);
	  if(Success){
	    await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","rtxDowntimeWarning"],15000)){
	    //expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddKonyAccountStep1","rtxDowntimeWarning"], "text")).toEqual("SOME");
	    await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"]);
	
	    appLog("Failed with : rtxDowntimeWarning");
	    fail("Failed with : "+kony.automation.widget.getWidgetProperty(["frmWireTransferAddKonyAccountStep1","rtxDowntimeWarning"], "text"));
	  }else{
	    // This is the condition for use cases where it won't throw error on UI but struck at same screen
	    appLog("Unable to add Recipitent");
	    fail("Unable to add Recipitent");
	  }
	}
	
	async function EnterInternational_RecipitentDetails_Step1(RecipientName,AddressLine1,AddressLine2,City,ZipCode){
	
	  appLog("Intiated method to Enter International Recipitent details - Step1");
	
	  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","btnInternationalAccount"],15000);
	  kony.automation.button.click(["frmWireTransferAddKonyAccountStep1","btnInternationalAccount"]);
	  await kony.automation.playback.wait(10000);
	  var Status=await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","lbxCountry"],15000);
	  expect(Status).toBe(true,"Failed to Navigate to International recipitent")
	  kony.automation.listbox.selectItem(["frmWireTransferAddInternationalAccountStep1","lbxCountry"], "AU");
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","tbxRecipientName"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep1","tbxRecipientName"],RecipientName);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","tbxAddressLine1"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep1","tbxAddressLine1"],AddressLine1);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","tbxAddressLine2"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep1","tbxAddressLine2"],AddressLine2);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","tbxCity"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep1","tbxCity"],City);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","lbxState"],15000);
	  kony.automation.listbox.selectItem(["frmWireTransferAddInternationalAccountStep1","lbxState"], "Independencia");
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","tbxZipcode"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep1","tbxZipcode"],ZipCode);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","btnProceed"],15000);
	  kony.automation.button.click(["frmWireTransferAddInternationalAccountStep1","btnProceed"]);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","lblStep2"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddInternationalAccountStep2","lblStep2"], "text")).toContain("Step 2");
	
	  appLog("Successfully Entered International Recipitent details - Step1");
	}
	
	async function EnterInternational_BankDetails_Step2(SwiftCode,IBAN,AccNumber,NickName,BankName,BankAddressLine1,BankAddressLine2,BankCity,BankZipcode){
	
	  appLog("Intiated method to Enter International Bank details - Step2");
	
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxSwiftCode"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxSwiftCode"],SwiftCode);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxIBANOrIRC"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxIBANOrIRC"],IBAN);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxAccountNumber"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxAccountNumber"],AccNumber);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxReAccountNumber"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxReAccountNumber"],AccNumber);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxNickName"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxNickName"],NickName);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxBankName"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxBankName"],BankName);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxBankAddressLine1"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxBankAddressLine1"],BankAddressLine1);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxBankAddressLine2"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxBankAddressLine2"],BankAddressLine2);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxBankCity"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxBankCity"],BankCity);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","lbxBankState"],15000);
	  kony.automation.listbox.selectItem(["frmWireTransferAddInternationalAccountStep2","lbxBankState"], "Independencia");
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxBankZipcode"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxBankZipcode"],BankZipcode);
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","btnAddRecipent"],15000);
	  kony.automation.button.click(["frmWireTransferAddInternationalAccountStep2","btnAddRecipent"]);
	
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountConfirm","lblAddAccountHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddInternationalAccountConfirm","lblAddAccountHeading"], "text")).not.toBe("");
	
	  appLog("Successfully Entered International Bank details - Step1");
	
	  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountConfirm","btnConfirm"],15000);
	  kony.automation.button.click(["frmWireTransferAddInternationalAccountConfirm","btnConfirm"]);
	
	  appLog("Successfully Clicked on Confirm Button");
	
	}
	
	
	async function VerifyAddInternationalRecipitentSuccessMsg(){
	
	  appLog("Intiated method to verify AddInternationalRecipitentSuccessMsg SuccessMsg");
	
	  var Success=await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountAck","lblSuccessAcknowledgement"],15000);
	  if(Success){
	    await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"]);
	  }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","rtxDowntimeWarning"],15000)){
	    //expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddInternationalAccountStep1","rtxDowntimeWarning"], "text")).toEqual("SOME");
	    await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"]);
	
	    appLog("Failed with : rtxDowntimeWarning");
	    fail("Failed with : "+kony.automation.widget.getWidgetProperty(["frmWireTransferAddInternationalAccountStep1","rtxDowntimeWarning"], "text"));
	  }else{
	    // This is the condition for use cases where it won't throw error on UI but struck at same screen
	    appLog("Unable to add Recipitent");
	    fail("Unable to add Recipitent");
	  }
	}
	
	
	async function NavigateToMakeTransfer(){
	
	  appLog("Intiated method to Navigate to Make Transfers");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransfer0flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransfer0flxMyAccounts"]);
	  await kony.automation.playback.wait(5000);
	  //await kony.automation.playback.waitFor(["frmWireTransfersWindow","lblAddAccountHeading"],15000);
	  //expect(kony.automation.widget.getWidgetProperty(["frmWireTransfersWindow","lblAddAccountHeading"], "text")).not.toBe("");
	  appLog("Succesfully Navigated to Make Transfers screen");
	}
	
	async function ClickOnMakeTransferLink(){
	
	  appLog("Intiated method to click on Make Transfer Link ");
	  await kony.automation.playback.waitFor(["frmWireTransfersWindow","segWireTransfers"],15000);
	  kony.automation.button.click(["frmWireTransfersWindow","segWireTransfers[0]","btnAction"]);
	  await kony.automation.playback.wait(10000);
	  //await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","lblAddAccountHeading"],15000);
	  //expect(kony.automation.widget.getWidgetProperty(["frmWireTransferMakeTransfer","lblAddAccountHeading"], "text")).not.toBe("");
	  appLog("Successfully clicked on Make Transfer Link ");
	
	}
	
	async function MakeWireTransfer(){
	
	  await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","txtTransferFrom"],15000);
	  kony.automation.widget.touch(["frmWireTransferMakeTransfer","txtTransferFrom"], [110,12],null,null);
	  await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","segTransferFrom"],15000);
	  kony.automation.flexcontainer.click(["frmWireTransferMakeTransfer","segTransferFrom[0,0]","flxAmount"]);
	  await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","tbxAmount"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferMakeTransfer","tbxAmount"],"1.5");
	  await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","tbxNotes"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferMakeTransfer","tbxNotes"],"TEST");
	  await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","btnStepContinue"],15000);
	  kony.automation.button.click(["frmWireTransferMakeTransfer","btnStepContinue"]);
	  await kony.automation.playback.wait(5000);
	  //await kony.automation.playback.waitFor(["frmConfirmDetails","lblAddAccountHeading"],15000);
	  //expect(kony.automation.widget.getWidgetProperty(["frmConfirmDetails","lblAddAccountHeading"], "text")).not.toBe("");
	
	  await kony.automation.playback.waitFor(["frmConfirmDetails","lblFavoriteEmailCheckBoxMain"],15000);
	  await kony.automation.scrollToWidget(["frmConfirmDetails","lblFavoriteEmailCheckBoxMain"]);
	  kony.automation.widget.touch(["frmConfirmDetails","lblFavoriteEmailCheckBoxMain"], null,null,[16,11]);
	  await kony.automation.playback.waitFor(["frmConfirmDetails","flxAgree"],15000);
	  kony.automation.flexcontainer.click(["frmConfirmDetails","flxAgree"]);
	
	  await kony.automation.playback.waitFor(["frmConfirmDetails","btnConfirm"],15000);
	  kony.automation.button.click(["frmConfirmDetails","btnConfirm"]);
	  await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmConfirmDetails","lblSuccessAcknowledgement"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmConfirmDetails","lblSuccessAcknowledgement"], "text")).not.toBe("");
	  await kony.automation.playback.waitFor(["frmConfirmDetails","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmConfirmDetails","customheadernew","flxAccounts"]);
	
	}
	
	async function NavigateToWireOneTimePayment(){
	
	  appLog("Intiated method to navigate to OneTime WireTransfer");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransfer4flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransfer4flxMyAccounts"]);
	  await kony.automation.playback.wait(10000);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","lblAddAccountHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferOneTimePaymentStep1","lblAddAccountHeading"], "text")).not.toBe("");
	
	  appLog("Successfully navigated to OneTime WireTransfer");
	
	}
	async function EnterOneTimeDomesticRecipitentDetails(RecipientName,AddressLine1,AddressLine2,City,ZipCode){
	
	  appLog("Intiated method to Enter OneTime-DomesticRecipitentDetails- Step1");
	
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","tbxRecipientName"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep1","tbxRecipientName"],RecipientName);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","tbxAddressLine1"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep1","tbxAddressLine1"],AddressLine1);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","tbxAddressLine2"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep1","tbxAddressLine2"],AddressLine2);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","tbxCity"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep1","tbxCity"],City);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","lbxState"],15000);
	  kony.automation.listbox.selectItem(["frmWireTransferOneTimePaymentStep1","lbxState"], "Independencia");
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","tbxZipcode"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep1","tbxZipcode"],ZipCode);
	
	  appLog("Successfully Entered OneTime-DomesticRecipitentDetails- Step1");
	
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","btnProceed"],15000);
	  await kony.automation.scrollToWidget(["frmWireTransferOneTimePaymentStep1","btnProceed"]);
	  kony.automation.button.click(["frmWireTransferOneTimePaymentStep1","btnProceed"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Clicked on Proceed Button");
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","lblStep2"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferOneTimePaymentStep2","lblStep2"], "text")).not.toBe("");
	  appLog("Successfully Verified Step2 screen");
	}
	
	async function EnterOneTimeDomesticBankDetails(SwiftCode,AccNumber,NickName,BankName,BankAddressLine1,BankAddressLine2,BankCity,BankZipcode){
	
	  appLog("Intiated method to enter OneTime-DomesticBankDetails-Step2");
	
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxSwiftCode"]);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxSwiftCode"],SwiftCode);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxAccountNumber"]);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxAccountNumber"],AccNumber);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxReAccountNumber"]);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxReAccountNumber"],AccNumber);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxNickName"]);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxNickName"],NickName);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxBankName"]);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxBankName"],BankName);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxBankAddressLine1"]);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxBankAddressLine1"],BankAddressLine1);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxBankAddressLine2"]);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxBankAddressLine2"],BankAddressLine2);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxBankCity"]);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxBankCity"],BankCity);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","lbxBankState"]);
	  kony.automation.listbox.selectItem(["frmWireTransferOneTimePaymentStep2","lbxBankState"], "Indiana");
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","tbxBankZipcode"]);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep2","tbxBankZipcode"],BankZipcode);
	
	  appLog("Successfully Entered OneTime-DomesticRecipitentDetails- Step1");
	
	  await kony.automation.scrollToWidget(["frmWireTransferOneTimePaymentStep2","btnStep2Proceed"]);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","btnStep2Proceed"]);
	  kony.automation.button.click(["frmWireTransferOneTimePaymentStep2","btnStep2Proceed"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Clicked on Proceed Button");
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","lblAddAccountHeading"]);
	  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferOneTimePaymentStep3","lblStep3"], "text")).not.toBe("");
	  appLog("Successfully Verified Step3 screen");
	}
	
	async function MakeOneTimeWireTransfer(){
	
	  appLog("Intiated method to Make-OneTimeTransfer");
	
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","txtTransferFrom"],15000);
	  kony.automation.widget.touch(["frmWireTransferOneTimePaymentStep3","txtTransferFrom"], [140,21],null,null);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","segTransferFrom"],15000);
	  kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep3","segTransferFrom[0,0]","flxAmount"]);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","tbxAmount"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep3","tbxAmount"],"1.5");
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","tbxNote"],15000);
	  kony.automation.textbox.enterText(["frmWireTransferOneTimePaymentStep3","tbxNote"],"OneTimePayment");
	  await kony.automation.scrollToWidget(["frmWireTransferOneTimePaymentStep3","btnStep3MakeTransfer"]);
	  await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","btnStep3MakeTransfer"],15000);
	  kony.automation.button.click(["frmWireTransferOneTimePaymentStep3","btnStep3MakeTransfer"]);
	  appLog("Successfully clicked on Make-OneTimeTransfer button");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmConfirmDetails","lblAddAccountHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmConfirmDetails","lblAddAccountHeading"], "text")).not.toBe("");
	  appLog("Successfully clicked on Make-OneTimeTransfer button");
	
	  await ConfirmDomesticOneTimeWireTransfer();
	
	}
	
	async function ConfirmDomesticOneTimeWireTransfer(){
	
	  await kony.automation.playback.waitFor(["frmConfirmDetails","lblFavoriteEmailCheckBoxMain"],15000);
	  kony.automation.widget.touch(["frmConfirmDetails","lblFavoriteEmailCheckBoxMain"], null,null,[14,16]);
	  await kony.automation.playback.waitFor(["frmConfirmDetails","flxAgree"],15000);
	  kony.automation.flexcontainer.click(["frmConfirmDetails","flxAgree"]);
	  appLog("Successfully Accepted Terms and conditions");
	
	  await kony.automation.playback.waitFor(["frmConfirmDetails","btnConfirm"],15000);
	  kony.automation.button.click(["frmConfirmDetails","btnConfirm"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully clicked on confirm button");
	
	}
	
	async function VerifyOneTimeDomesticWireTransferSuccessMsg(){
	
	  var success=await kony.automation.playback.waitFor(["frmConfirmDetails","lblSuccessAcknowledgement"],15000);
	  if(success){
	    //expect(kony.automation.widget.getWidgetProperty(["frmConfirmDetails","lblSuccessAcknowledgement"], "text")).not.toBe("");
	    appLog("Successfully Verified Onetime Wire Transfer Success Message");
	    await kony.automation.playback.waitFor(["frmConfirmDetails","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmConfirmDetails","customheadernew","flxAccounts"]);
	    appLog("Successfully Moved back to Accounts Dashboard");
	  }else{
	    appLog("Unable to Perform OneTimeDomestic WireTransfers");
	    fail("Unable to Perform OneTimeDomestic WireTransfers");
	
	  }
	
	}
	
	async function NavigatetoCreateNewTemplate(){
	
	  appLog("Intiated method to navigate to create Template");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransfer5flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransfer5flxMyAccounts"]);
	  await kony.automation.playback.wait(10000);
	  //await kony.automation.playback.waitFor(["frmCreateTemplatePrimaryDetails","lblAddAccountHeading"],15000);
	  //expect(kony.automation.widget.getWidgetProperty(["frmCreateTemplatePrimaryDetails","lblAddAccountHeading"], "text")).not.toBe("");
	
	  appLog("Successfully navigated to NewTemplate creation");
	}
	
	async function EnterTemplateDetails(TemplateName){
	
	  await kony.automation.playback.waitFor(["frmCreateTemplatePrimaryDetails","tbxRecipientName"],15000);
	  kony.automation.textbox.enterText(["frmCreateTemplatePrimaryDetails","tbxRecipientName"],TemplateName);
	  appLog("Successfully entered template name");
	  await kony.automation.playback.waitFor(["frmCreateTemplatePrimaryDetails","btnContinue"],15000);
	  await kony.automation.scrollToWidget(["frmCreateTemplatePrimaryDetails","btnContinue"]);
	  kony.automation.button.click(["frmCreateTemplatePrimaryDetails","btnContinue"]);
	  appLog("Successfully cliced on Continue button");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","lblHeading"]);
	  expect(kony.automation.widget.getWidgetProperty(["frmBulkTemplateAddRecipients","lblHeading"], "text")).not.toBe("");
	}
	
	async function selectExistingRecipitentOption(){
	
	  await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","lblExistingRecipients"],15000);
	  kony.automation.widget.touch(["frmBulkTemplateAddRecipients","lblExistingRecipients"], null,null,[16,14]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully selected existing recipitent option");
	  await kony.automation.playback.waitFor(["frmCreateTempSelectRecipients","lblAddAccountHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCreateTempSelectRecipients","lblAddAccountHeading"], "text")).not.toBe("");
	}
	
	async function CreateNewTemplate_Existingrecipitent(){
	
	  await kony.automation.playback.waitFor(["frmCreateTempSelectRecipients","lblStatus"],15000);
	  kony.automation.widget.touch(["frmCreateTempSelectRecipients","lblStatus"], null,null,[16,14]);
	  await kony.automation.playback.waitFor(["frmCreateTempSelectRecipients","flxSelectAll"],15000);
	  kony.automation.flexcontainer.click(["frmCreateTempSelectRecipients","flxSelectAll"]);
	  appLog("Successfully selected recipitent Select All Button");
	
	  await kony.automation.playback.waitFor(["frmCreateTempSelectRecipients","btnContinue"],15000);
	  kony.automation.button.click(["frmCreateTempSelectRecipients","btnContinue"]);
	  appLog("Successfully selected Continue button");
	
	  await VerifyNewTemplateSuccessMsg();
	
	}
	
	async function selectNewManualRecipitentOption(){
	
	  await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","lblManualRecipients"],15000);
	  kony.automation.widget.touch(["frmBulkTemplateAddRecipients","lblManualRecipients"], null,null,[16,14]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully selected New Manual Recipitent option");
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","lblAddAccountHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmCreateTempAddDomestic","lblAddAccountHeading"], "text")).not.toBe("");
	}
	
	async function EnterManualRecipitentTemplateDetails(RecipientName,AddressLine1,AddressLine2,City,ZipCode,RoutingNumber,AccNumber,NickName,BankName,BankAddressLine1,BankAddressLine2,BankCity,BankZipcode){
	
	  appLog("Intiated method to enter ManualRecipitentTemplateDetails");
	
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxRecipientName"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxRecipientName"],RecipientName);
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxAddressLine1"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxAddressLine1"],AddressLine1);
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxAddressLine2"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxAddressLine2"],AddressLine2);
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxCity"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxCity"],City);
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","lbxState"]);
	  kony.automation.listbox.selectItem(["frmCreateTempAddDomestic","lbxState"], "Independencia");
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxZipcode"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxZipcode"],ZipCode);
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxRoutingNumber"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxRoutingNumber"],RoutingNumber);
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxRecipientAccountNumber"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxRecipientAccountNumber"],AccNumber);
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxReEnterAccountNumber"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxReEnterAccountNumber"],AccNumber);
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxAccountNickName"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxAccountNickName"],NickName);
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxRecipientBankName"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxRecipientBankName"],BankName);
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxBankAddressLine1"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxBankAddressLine1"],BankAddressLine1);
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxBankAddressLIne2"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxBankAddressLIne2"],BankAddressLine2);
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxCompanyCity"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxCompanyCity"],BankCity);
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","lbxCompanyState"]);
	  kony.automation.listbox.selectItem(["frmCreateTempAddDomestic","lbxCompanyState"], "Independencia");
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","tbxCompanyZipCode"],15000);
	  kony.automation.textbox.enterText(["frmCreateTempAddDomestic","tbxCompanyZipCode"],BankZipcode);
	
	  appLog("Successfully entered ManualRecipitentTemplateDetails");
	
	  await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","btnProceed"],15000);
	  kony.automation.button.click(["frmCreateTempAddDomestic","btnProceed"]);
	  appLog("Successfully Clicked on Proceed button");
	}
	
	
	async function VerifyNewTemplateSuccessMsg(){
	
	  var Success=await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","rtxMakeTransferError"],15000);
	  //expect(kony.automation.widget.getWidgetProperty(["frmBulkTemplateAddRecipients","rtxMakeTransferError"], "text")).not.toBe("");
	  if(Success){
	    appLog("Successfully created new Template");
	    await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"]);
	    appLog("Successfully Moved back to Accounts Dashboard");
	  }else{
	    appLog("Failed to create Template -Existing Recipitent");
	    fail("Failed to create Template -Existing Recipitent");
	  }
	
	}
	
	async function NavigateToWireHistoryTab(){
	
	  appLog("Intiated method to navigate to History Tab");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransfer1flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransfer1flxMyAccounts"]);
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmWireTransfersRecent","lblAddAccountHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmWireTransfersRecent","lblAddAccountHeading"], "text")).not.toBe("");
	
	  appLog("Successfully Navigated to History Tab");
	}
	
	async function VerifyWireTransferHistory(){
	
	  appLog("Intiated method to check Transfer History");
	
	  var History=await kony.automation.playback.waitFor(["frmWireTransfersRecent","segWireTransfers"],15000);
	  if(History){
	    kony.automation.flexcontainer.click(["frmWireTransfersRecent","segWireTransfers[0]","flxDropdown"]);
	    appLog("Successfully verified History");
	    await kony.automation.playback.waitFor(["frmWireTransfersRecent","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmWireTransfersRecent","customheadernew","flxAccounts"]);
	    appLog("Successfully ovedBack to DashBoard");
	  }else{
	    appLog("Custom Message : No History availble at this moment");
	    await kony.automation.playback.waitFor(["frmWireTransfersRecent","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmWireTransfersRecent","customheadernew","flxAccounts"]);
	  }
	}
	
	async function NavigateToWireRecipitentsTab(){
	
	  appLog("Intiated method to WireRecipitents Tab");
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransfer2flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransfer2flxMyAccounts"]);
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmWireTransfersManageRecipients","lblAddAccountHeading"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmWireTransfersManageRecipients","lblAddAccountHeading"], "text")).not.toBe("");
	
	  appLog("Successfully Navigated to WireRecipitents Tab");
	}
	
	async function VerifyWireRecipitents(){
	
	  appLog("Intiated method to check wire Recipitents");
	
	  var List=await kony.automation.playback.waitFor(["frmWireTransfersManageRecipients","segWireTransfers"],15000);
	  if(List){
	    kony.automation.flexcontainer.click(["frmWireTransfersManageRecipients","segWireTransfers[0]","flxDropdown"]);
	    appLog("Successfully verified Wire recipitents");
	    await kony.automation.playback.waitFor(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"]);
	    appLog("Successfully ovedBack to DashBoard");
	  }else{
	    appLog("Custom Message : No Recipitents availble at this moment");
	    await kony.automation.playback.waitFor(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"]);
	  }
	}
	
	
	async function ActivateWireTransfersTermsConditions(){
	
	  // First click on Any one of Wire Transfer
	  appLog("Intiated method to Navigate to Activate WireTransfer");
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransfer0flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransfer0flxMyAccounts"]);
	  await kony.automation.playback.wait(10000);
	  appLog("Successfully Navigated to wire transfer screen");
	
	  var Terms=await kony.automation.playback.waitFor(["frmActivateWireTransfer","lblAddAccountHeading"],15000);
	  if(Terms){
	    appLog("Intiated method to accept terms and conditions");
	    await kony.automation.playback.waitFor(["frmActivateWireTransfer","lbxDefaultAccountForSending"],15000);
	    await kony.automation.scrollToWidget(["frmActivateWireTransfer","lbxDefaultAccountForSending"]);
	    kony.automation.listbox.selectItem(["frmActivateWireTransfer","lbxDefaultAccountForSending"],AllAccounts.Current.accno);
	    appLog("Successfully Selected default account number");
	    await kony.automation.playback.waitFor(["frmActivateWireTransfer","lblFavoriteEmailCheckBoxMain"],15000);
	    kony.automation.widget.touch(["frmActivateWireTransfer","lblFavoriteEmailCheckBoxMain"], null,null,[10,22]);
	    await kony.automation.playback.waitFor(["frmActivateWireTransfer","flxCheckbox"],15000);
	    kony.automation.flexcontainer.click(["frmActivateWireTransfer","flxCheckbox"]);
	    appLog("Successfully accepted Terms and conditions");
	    await kony.automation.playback.waitFor(["frmActivateWireTransfer","btnProceed"],15000);
	    kony.automation.button.click(["frmActivateWireTransfer","btnProceed"]);
	    appLog("Successfully clicked on Proceed button");
	    await kony.automation.playback.waitFor(["frmWireTransfersWindow","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmWireTransfersWindow","customheadernew","flxAccounts"]);
	    appLog("Successfully Navigated to Accounts DashBoard");
	
	  }else{
	    appLog("WireTransfers is activated already");
	    await kony.automation.playback.waitFor(["frmWireTransfersWindow","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmWireTransfersWindow","customheadernew","flxAccounts"]);
	    appLog("Successfully Navigated to Accounts DashBoard");
	  }
	}
	
	
	
	it("APrerequesite_ActivateWireTransfer", async function() {
	  
	  await ActivateWireTransfersTermsConditions();
	  
	},TimeOuts.WireTransfers.activateWireTransfer);
	
	it("AddDomesticRecipitent", async function() {
	  
	  
	  var RecipientName=WireTransers.DomesticRecipitentDetails.BeneficiaryName;
	  var AddressLine1=WireTransers.DomesticRecipitentDetails.Address1;
	  var AddressLine2=WireTransers.DomesticRecipitentDetails.Address2;
	  var City=WireTransers.DomesticRecipitentDetails.city;
	  var ZipCode=WireTransers.DomesticRecipitentDetails.zipcode;
	  
	  var RoutingNumber=WireTransers.DomesticBankdetails.RoutingNumber+getRandomNumber(7);
	  var AccNumber=WireTransers.DomesticBankdetails.AccNumber+getRandomNumber(7);
	  var NickName=WireTransers.DomesticBankdetails.Nickname;
	  var BankName=WireTransers.DomesticBankdetails.BankName;
	  var BankAddressLine1=WireTransers.DomesticBankdetails.BankAddressLine1;
	  var BankAddressLine2=WireTransers.DomesticBankdetails.BankAddressLine2;
	  var BankCity=WireTransers.DomesticBankdetails.BankCity;
	  var BankZipcode=WireTransers.DomesticBankdetails.BankZipcode;
	  
	  await NavigateToWireTransfer_AddRecipitent();
	  await EnterDomestic_RecipitentDetails_Step1(RecipientName,AddressLine1,AddressLine2,City,ZipCode);
	  await EnterDomestic_BankDetails_Step2(RoutingNumber,AccNumber,NickName,BankName,BankAddressLine1,BankAddressLine2,BankCity,BankZipcode);
	  await VerifyAddDomesticRecipitentSuccessMsg();
	  
	},TimeOuts.WireTransfers.Payment);
	
	it("AddInternationalRecipitent", async function() {
	  
	  
	  var RecipientName=WireTransers.InternationalRecipitentDetails.BeneficiaryName;
	  var AddressLine1=WireTransers.InternationalRecipitentDetails.Address1;
	  var AddressLine2=WireTransers.InternationalRecipitentDetails.Address2;
	  var City=WireTransers.InternationalRecipitentDetails.city;
	  var ZipCode=WireTransers.InternationalRecipitentDetails.zipcode;
	  
	  var SwiftCode=WireTransers.InternationalBankdetails.SWIFT;
	  var IBAN=WireTransers.InternationalBankdetails.IBAN+getRandomNumber(7);
	  var AccNumber=WireTransers.InternationalBankdetails.AccNumber+getRandomNumber(7);
	  var NickName=WireTransers.InternationalBankdetails.Nickname;
	  var BankName=WireTransers.InternationalBankdetails.BankName;
	  var BankAddressLine1=WireTransers.InternationalBankdetails.BankAddressLine1;
	  var BankAddressLine2=WireTransers.InternationalBankdetails.BankAddressLine2;
	  var BankCity=WireTransers.InternationalBankdetails.BankCity;
	  var BankZipcode=WireTransers.InternationalBankdetails.BankZipcode;
	  
	  
	  await NavigateToWireTransfer_AddRecipitent();
	  await EnterInternational_RecipitentDetails_Step1(RecipientName,AddressLine1,AddressLine2,City,ZipCode);
	  await EnterInternational_BankDetails_Step2(SwiftCode,IBAN,AccNumber,NickName,BankName,BankAddressLine1,BankAddressLine2,BankCity,BankZipcode);
	  await VerifyAddInternationalRecipitentSuccessMsg();
	  
	},TimeOuts.WireTransfers.Payment);
	
	it("VerifyMakeWireTransfer", async function() {
	  
	  await NavigateToMakeTransfer();
	  await ClickOnMakeTransferLink();
	  await MakeWireTransfer();
	  
	},TimeOuts.WireTransfers.Payment);
	
	it("VerifyOneTimeWireTransfer", async function() {
	  
	  var RecipientName=WireTransers.DomesticRecipitentDetails.BeneficiaryName;
	  var AddressLine1=WireTransers.DomesticRecipitentDetails.Address1;
	  var AddressLine2=WireTransers.DomesticRecipitentDetails.Address2;
	  var City=WireTransers.DomesticRecipitentDetails.city;
	  var ZipCode=WireTransers.DomesticRecipitentDetails.zipcode;
	  
	  var SwiftCode=WireTransers.InternationalBankdetails.SWIFT;
	  var AccNumber=WireTransers.DomesticBankdetails.AccNumber+getRandomNumber(7);
	  var NickName=WireTransers.DomesticBankdetails.Nickname;
	  var BankName=WireTransers.DomesticBankdetails.BankName;
	  var BankAddressLine1=WireTransers.DomesticBankdetails.BankAddressLine1;
	  var BankAddressLine2=WireTransers.DomesticBankdetails.BankAddressLine2;
	  var BankCity=WireTransers.DomesticBankdetails.BankCity;
	  var BankZipcode=WireTransers.DomesticBankdetails.BankZipcode;
	  
	  await NavigateToWireOneTimePayment();
	  await EnterOneTimeDomesticRecipitentDetails(RecipientName,AddressLine1,AddressLine2,City,ZipCode);
	  await EnterOneTimeDomesticBankDetails(SwiftCode,AccNumber,NickName,BankName,BankAddressLine1,BankAddressLine2,BankCity,BankZipcode);
	  await MakeOneTimeWireTransfer();
	  await VerifyOneTimeDomesticWireTransferSuccessMsg();
	  
	},TimeOuts.WireTransfers.Payment);
	
	it("CreateTemplate_ExistingRecipitent", async function() {
	  
	  await NavigatetoCreateNewTemplate();
	  await EnterTemplateDetails("Temp_ExistingRecipitent"+getRandomString(5));
	  await selectExistingRecipitentOption();
	  await CreateNewTemplate_Existingrecipitent();
	  
	},TimeOuts.WireTransfers.Payment);
	
	it("CreateTemplate_ManualRecipitent", async function() {
	  
	  var RecipientName=WireTransers.DomesticRecipitentDetails.BeneficiaryName;
	  var AddressLine1=WireTransers.DomesticRecipitentDetails.Address1;
	  var AddressLine2=WireTransers.DomesticRecipitentDetails.Address2;
	  var City=WireTransers.DomesticRecipitentDetails.city;
	  var ZipCode=WireTransers.DomesticRecipitentDetails.zipcode;
	  
	  var RoutingNumber=WireTransers.DomesticBankdetails.RoutingNumber+getRandomNumber(7);
	  var AccNumber=WireTransers.DomesticBankdetails.AccNumber+getRandomNumber(7);
	  var NickName=WireTransers.DomesticBankdetails.Nickname;
	  var BankName=WireTransers.DomesticBankdetails.BankName;
	  var BankAddressLine1=WireTransers.DomesticBankdetails.BankAddressLine1;
	  var BankAddressLine2=WireTransers.DomesticBankdetails.BankAddressLine2;
	  var BankCity=WireTransers.DomesticBankdetails.BankCity;
	  var BankZipcode=WireTransers.DomesticBankdetails.BankZipcode;
	  
	  await NavigatetoCreateNewTemplate();
	  await EnterTemplateDetails("Temp_ManualRecipitent"+getRandomString(5));
	  await selectNewManualRecipitentOption();
	  await EnterManualRecipitentTemplateDetails(RecipientName,AddressLine1,AddressLine2,City,ZipCode,RoutingNumber,AccNumber,NickName,BankName,BankAddressLine1,BankAddressLine2,BankCity,BankZipcode);
	  await VerifyNewTemplateSuccessMsg();
	  
	},TimeOuts.WireTransfers.Payment);
	
	it("VerifyWireRecipitentsList", async function() {
	  
	  await NavigateToWireRecipitentsTab();
	  await VerifyWireRecipitents();
	  
	},TimeOuts.WireTransfers.Payment);
	
	it("VerifyWireTransferHistory", async function() {
	  
	  await NavigateToWireHistoryTab();
	  await VerifyWireTransferHistory();
	  
	},TimeOuts.WireTransfers.Payment);
});