describe("T24_BillPaySuite", function() {
	beforeEach(async function() {
	
	  //await kony.automation.playback.wait(10000);
	  strLogger=[];
	  var formName='';
	  formName=kony.automation.getCurrentForm();
	  appLog('Inside before Each function');
	  appLog("Current form name is  :: "+formName);
	  if(formName==='frmDashboard'){
	    appLog('Already in dashboard');
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else if(await kony.automation.playback.waitFor([formName,"customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from '+formName);
	    kony.automation.flexcontainer.click([formName,"customheader","topmenu","flxaccounts"]);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else if(await kony.automation.playback.waitFor([formName,"customheadernew","topmenu","flxAccounts"],5000)){
	    appLog('Moving back from '+formName);
	    kony.automation.flexcontainer.click([formName,"customheadernew","topmenu","flxAccounts"]);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else{
	    appLog("Form name is not available");
	  }
	
	//   if(await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000)){
	//     appLog('Already in dashboard');
	//   }else if(await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxUserName"],5000)){
	//     appLog('Inside Login Screen');
	//   }else if(await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmAccountsDetails');
	//     kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmProfileManagement');
	//     kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmManageBeneficiaries","customheadernew","topmenu","flxAccounts"],5000)){
	//     appLog('Moving back from frmManageBeneficiaries');
	//     kony.automation.flexcontainer.click(["frmManageBeneficiaries","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryConfirmEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryAcknowledgementEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakePayment","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakePayment');
	//     kony.automation.flexcontainer.click(["frmMakePayment","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConfirmEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmConfirmEuro');
	//     kony.automation.flexcontainer.click(["frmConfirmEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAcknowledgementEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAcknowledgementEuro');
	//     kony.automation.flexcontainer.click(["frmAcknowledgementEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPastPaymentsEurNew');
	//     kony.automation.flexcontainer.click(["frmPastPaymentsEurNew","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmScheduledPaymentsEurNew');
	//     kony.automation.flexcontainer.click(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmConsolidatedStatements');
	//     kony.automation.flexcontainer.click(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCardManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmCardManagement');
	//     kony.automation.flexcontainer.click(["frmCardManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBulkPayees');
	//     kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayee","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakeOneTimePayee');
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayment","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakeOneTimePayment');
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmOneTimePaymentConfirm');
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmOneTimePaymentAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmManagePayees');
	//     kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayABill');
	//     kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayBillConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayBillConfirm');
	//     kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayBillAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayScheduled');
	//     kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddPayee1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddPayee1');
	//     kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddPayeeInformation","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddPayeeInformation');
	//     kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayeeDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayeeDetails');
	//     kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmVerifyPayee","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmVerifyPayee');
	//     kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayeeAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayHistory');
	//     kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayActivation","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayActivation');
	//     kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayActivationAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountConfirm');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountAck');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountConfirm');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountAck');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersWindow","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersWindow');
	//     kony.automation.flexcontainer.click(["frmWireTransfersWindow","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferMakeTransfer');
	//     kony.automation.flexcontainer.click(["frmWireTransferMakeTransfer","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConfirmDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmConfirmDetails');
	//     kony.automation.flexcontainer.click(["frmConfirmDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep3');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep3","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTemplatePrimaryDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTemplatePrimaryDetails');
	//     kony.automation.flexcontainer.click(["frmCreateTemplatePrimaryDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBulkTemplateAddRecipients');
	//     kony.automation.flexcontainer.click(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTempSelectRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTempSelectRecipients');
	//     kony.automation.flexcontainer.click(["frmCreateTempSelectRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTempAddDomestic');
	//     kony.automation.flexcontainer.click(["frmCreateTempAddDomestic","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersRecent","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersRecent');
	//     kony.automation.flexcontainer.click(["frmWireTransfersRecent","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersManageRecipients');
	//     kony.automation.flexcontainer.click(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmActivateWireTransfer","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmActivateWireTransfer');
	//     kony.automation.flexcontainer.click(["frmActivateWireTransfer","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmPersonalFinanceManagement');
	//     kony.automation.flexcontainer.click(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCustomViews","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmCustomViews');
	//     kony.automation.flexcontainer.click(["frmCustomViews","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmStopPayments","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmStopPayments');
	//     kony.automation.flexcontainer.click(["frmStopPayments","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmNAO","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmNAO');
	//     kony.automation.flexcontainer.click(["frmNAO","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmSavingsPotLanding","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmSavingsPotLanding');
	//     kony.automation.flexcontainer.click(["frmSavingsPotLanding","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateSavingsPot","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateSavingsPot');
	//     kony.automation.flexcontainer.click(["frmCreateSavingsPot","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateSavingsGoal","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateSavingsGoal');
	//     kony.automation.flexcontainer.click(["frmCreateSavingsGoal","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateGoalConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateGoalConfirm');
	//     kony.automation.flexcontainer.click(["frmCreateGoalConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateGoalAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateGoalAck');
	//     kony.automation.flexcontainer.click(["frmCreateGoalAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudget","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudget');
	//     kony.automation.flexcontainer.click(["frmCreateBudget","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmEditGoal","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmEditGoal');
	//     kony.automation.flexcontainer.click(["frmEditGoal","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudgetConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudgetConfirm');
	//     kony.automation.flexcontainer.click(["frmCreateBudgetConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudgetAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudgetAck');
	//     kony.automation.flexcontainer.click(["frmCreateBudgetAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmEditBudget","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmEditBudget');
	//     kony.automation.flexcontainer.click(["frmEditBudget","customheadernew","flxAccounts"]);
	//   }else{
	//     appLog("Form name is not available");
	//   }
	  
	},480000);
	
	afterEach(async function() {
	
	  //await kony.automation.playback.wait(10000);
	  
	  var formName='';
	  formName=kony.automation.getCurrentForm();
	  appLog('Inside after Each function');
	  appLog("Current form name is  :: "+formName);
	  if(formName==='frmDashboard'){
	    appLog('Already in dashboard');
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else if(await kony.automation.playback.waitFor([formName,"customheader","topmenu","flxaccounts"],5000)){
	    appLog('Moving back from '+formName);
	    kony.automation.flexcontainer.click([formName,"customheader","topmenu","flxaccounts"]);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else if(await kony.automation.playback.waitFor([formName,"customheadernew","topmenu","flxAccounts"],5000)){
	    appLog('Moving back from '+formName);
	    kony.automation.flexcontainer.click([formName,"customheadernew","topmenu","flxAccounts"]);
	    await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
	  }else{
	    appLog("Form name is not available");
	  }
	
	//   if(await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],5000)){
	//     appLog('Already in dashboard');
	//   }else if(await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxUserName"],5000)){
	//     appLog('Inside Login Screen');
	//   }else if(await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmAccountsDetails');
	//     kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmProfileManagement');
	//     kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmManageBeneficiaries","customheadernew","topmenu","flxAccounts"],5000)){
	//     appLog('Moving back from frmManageBeneficiaries');
	//     kony.automation.flexcontainer.click(["frmManageBeneficiaries","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryConfirmEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryConfirmEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddBeneficiaryAcknowledgementEuro');
	//     kony.automation.flexcontainer.click(["frmAddBeneficiaryAcknowledgementEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakePayment","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakePayment');
	//     kony.automation.flexcontainer.click(["frmMakePayment","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConfirmEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmConfirmEuro');
	//     kony.automation.flexcontainer.click(["frmConfirmEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAcknowledgementEuro","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAcknowledgementEuro');
	//     kony.automation.flexcontainer.click(["frmAcknowledgementEuro","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPastPaymentsEurNew","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPastPaymentsEurNew');
	//     kony.automation.flexcontainer.click(["frmPastPaymentsEurNew","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmScheduledPaymentsEurNew');
	//     kony.automation.flexcontainer.click(["frmScheduledPaymentsEurNew","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmConsolidatedStatements');
	//     kony.automation.flexcontainer.click(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCardManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmCardManagement');
	//     kony.automation.flexcontainer.click(["frmCardManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBulkPayees');
	//     kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayee","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakeOneTimePayee');
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayee","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayment","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmMakeOneTimePayment');
	//     kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmOneTimePaymentConfirm');
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmOneTimePaymentAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmManagePayees');
	//     kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayABill');
	//     kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayBillConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayBillConfirm');
	//     kony.automation.flexcontainer.click(["frmPayBillConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayBillAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayScheduled');
	//     kony.automation.flexcontainer.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddPayee1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddPayee1');
	//     kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmAddPayeeInformation","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmAddPayeeInformation');
	//     kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayeeDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayeeDetails');
	//     kony.automation.flexcontainer.click(["frmPayeeDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmVerifyPayee","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmVerifyPayee');
	//     kony.automation.flexcontainer.click(["frmVerifyPayee","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmPayeeAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayHistory');
	//     kony.automation.flexcontainer.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayActivation","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayActivation');
	//     kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBillPayActivationAcknowledgement');
	//     kony.automation.flexcontainer.click(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountConfirm');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddKonyAccountAck');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddKonyAccountAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountConfirm');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferAddInternationalAccountAck');
	//     kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersWindow","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersWindow');
	//     kony.automation.flexcontainer.click(["frmWireTransfersWindow","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferMakeTransfer","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferMakeTransfer');
	//     kony.automation.flexcontainer.click(["frmWireTransferMakeTransfer","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmConfirmDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmConfirmDetails');
	//     kony.automation.flexcontainer.click(["frmConfirmDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep1","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep1');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep1","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep2","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep2');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep2","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransferOneTimePaymentStep3","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransferOneTimePaymentStep3');
	//     kony.automation.flexcontainer.click(["frmWireTransferOneTimePaymentStep3","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTemplatePrimaryDetails","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTemplatePrimaryDetails');
	//     kony.automation.flexcontainer.click(["frmCreateTemplatePrimaryDetails","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmBulkTemplateAddRecipients');
	//     kony.automation.flexcontainer.click(["frmBulkTemplateAddRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTempSelectRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTempSelectRecipients');
	//     kony.automation.flexcontainer.click(["frmCreateTempSelectRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateTempAddDomestic","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateTempAddDomestic');
	//     kony.automation.flexcontainer.click(["frmCreateTempAddDomestic","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersRecent","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersRecent');
	//     kony.automation.flexcontainer.click(["frmWireTransfersRecent","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmWireTransfersManageRecipients');
	//     kony.automation.flexcontainer.click(["frmWireTransfersManageRecipients","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmActivateWireTransfer","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmActivateWireTransfer');
	//     kony.automation.flexcontainer.click(["frmActivateWireTransfer","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmPersonalFinanceManagement');
	//     kony.automation.flexcontainer.click(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCustomViews","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmCustomViews');
	//     kony.automation.flexcontainer.click(["frmCustomViews","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmStopPayments","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmStopPayments');
	//     kony.automation.flexcontainer.click(["frmStopPayments","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmNAO","customheader","topmenu","flxaccounts"],5000)){
	//     appLog('Moving back from frmNAO');
	//     kony.automation.flexcontainer.click(["frmNAO","customheader","topmenu","flxaccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmSavingsPotLanding","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmSavingsPotLanding');
	//     kony.automation.flexcontainer.click(["frmSavingsPotLanding","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateSavingsPot","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateSavingsPot');
	//     kony.automation.flexcontainer.click(["frmCreateSavingsPot","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateSavingsGoal","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateSavingsGoal');
	//     kony.automation.flexcontainer.click(["frmCreateSavingsGoal","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateGoalConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateGoalConfirm');
	//     kony.automation.flexcontainer.click(["frmCreateGoalConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateGoalAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateGoalAck');
	//     kony.automation.flexcontainer.click(["frmCreateGoalAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudget","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudget');
	//     kony.automation.flexcontainer.click(["frmCreateBudget","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmEditGoal","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmEditGoal');
	//     kony.automation.flexcontainer.click(["frmEditGoal","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudgetConfirm","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudgetConfirm');
	//     kony.automation.flexcontainer.click(["frmCreateBudgetConfirm","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmCreateBudgetAck","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmCreateBudgetAck');
	//     kony.automation.flexcontainer.click(["frmCreateBudgetAck","customheadernew","flxAccounts"]);
	//   }else if(await kony.automation.playback.waitFor(["frmEditBudget","customheadernew","flxAccounts"],5000)){
	//     appLog('Moving back from frmEditBudget');
	//     kony.automation.flexcontainer.click(["frmEditBudget","customheadernew","flxAccounts"]);
	//   }else{
	//     appLog("Form name is not available");
	//   }
	  
	  //strLogger=null;
	
	},480000);
	
	async function verifyAccountsLandingScreen(){
	
	  //await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  await kony.automation.scrollToWidget(["frmDashboard","customheader","topmenu","flxaccounts"]);
	}
	
	async function SelectAccountsOnDashBoard(AccountType){
	
	  appLog("Intiated method to analyze accounts data Dashboard");
	
	  var Status=false;
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  var finished = false;
	  for(var x = 0; x <segLength && !finished; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    //appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	
	      var seg="segAccounts["+x+","+y+"]";
	
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	      if(typeOfAccount.includes(AccountType)){
	        kony.automation.widget.touch(["frmDashboard","accountList",seg,"flxContent"], null,null,[303,1]);
	        kony.automation.flexcontainer.click(["frmDashboard","accountList",seg,"flxAccountDetails"]);
	        appLog("Successfully Clicked on : <b>"+accountName+"</b>");
	        await kony.automation.playback.wait(10000);
	        
	        finished = true;
	        Status=true;
	        break;
	      }
	    }
	  }
	
	  expect(Status).toBe(true,"Failed to click on AccType: <b>"+AccountType+"</b>");
	}
	
	async function clickOnFirstCheckingAccount(){
	
	  appLog("Intiated method to click on First Checking account");
	  SelectAccountsOnDashBoard("Checking");
	  //appLog("Successfully Clicked on First Checking account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstSavingsAccount(){
	
	  appLog("Intiated method to click on First Savings account");
	  SelectAccountsOnDashBoard("Saving");
	  //appLog("Successfully Clicked on First Savings account");
	  //await kony.automation.playback.wait(5000);
	}
	
	
	async function clickOnFirstCreditCardAccount(){
	
	  appLog("Intiated method to click on First CreditCard account");
	  SelectAccountsOnDashBoard("Credit");
	  //appLog("Successfully Clicked on First CreditCard account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstDepositAccount(){
	
	  appLog("Intiated method to click on First Deposit account");
	  SelectAccountsOnDashBoard("Deposit");
	  //appLog("Successfully Clicked on First Deposit account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstLoanAccount(){
	
	  appLog("Intiated method to click on First Loan account");
	  SelectAccountsOnDashBoard("Loan");
	  //appLog("Successfully Clicked on First Loan account");
	  //await kony.automation.playback.wait(5000);
	}
	
	async function clickOnFirstAvailableAccount(){
	
	  appLog("Intiated method to click on First available account on DashBoard");
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  kony.automation.widget.touch(["frmDashboard","accountList","segAccounts[0,0]","flxContent"], null,null,[303,1]);
	  kony.automation.flexcontainer.click(["frmDashboard","accountList","segAccounts[0,0]","flxAccountDetails"]);
	  appLog("Successfully Clicked on First available account on DashBoard");
	  await kony.automation.playback.wait(5000);
	}
	
	async function clickOnSearch_AccountDetails(){
	
	  appLog("Intiated method to click on Search Glass Icon");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","flxSearch"],15000);
	  kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","flxSearch"]);
	  appLog("Successfully Clicked on Seach Glass Icon");
	}
	
	async function selectTranscationtype(TransactionType){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTransactionType"], TransactionType);
	  appLog("Successfully selected Transcation type : <b>"+TransactionType+"</b>");
	}
	
	async function enterKeywordtoSearch(keyword){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtKeyword"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtKeyword"],keyword);
	  appLog("Successfully entered keyword for search : <b>"+keyword+"</b>");
	}
	
	async function selectAmountRange(AmountRange1,AmountRange2){
	
	  appLog("Intiated method to select Amount Range : ["+AmountRange1+","+AmountRange2+"]");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeFrom"],AmountRange1);
	  appLog("Successfully selected amount range From");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],15000);
	  kony.automation.textbox.enterText(["frmAccountsDetails","accountTransactionList","txtAmountRangeTo"],AmountRange2);
	  appLog("Successfully selected amount range To");
	
	  appLog("Successfully selected amount Range : ["+AmountRange1+","+AmountRange2+"]");
	}
	
	async function selectCustomdate(){
	
	  appLog("Intiated method to select Custom Date Range");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"], "CUSTOM_DATE_RANGE");
	  appLog("Successfully selected Date Range");
	}
	
	async function selectTimePeriod(){
	
	  appLog("Intiated method to select custom timeperiod");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"],15000);
	  kony.automation.listbox.selectItem(["frmAccountsDetails","accountTransactionList","lstbxTimePeriod"], "LAST_THREE_MONTHS");
	  appLog("Successfully selected Time period");
	}
	
	async function clickOnAdvancedSearchBtn(){
	
	  appLog("Intiated method to click on Search Button with given search criteria");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","btnSearch"],15000);
	  kony.automation.button.click(["frmAccountsDetails","accountTransactionList","btnSearch"]);
	  appLog("Successfully clicked on Search button");
	  await kony.automation.playback.wait(5000);
	}
	
	async function validateSearchResult() {
	
	  var noResult=await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"],15000);
	  if(noResult){
	    appLog("No Results found with given criteria..");
	    expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","rtxNoPaymentMessage"], "text")).toEqual("No Transactions Found");
	  }else{
	    await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
	    kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","segTransactions[0,0]","flxDropdown"]);
	    appLog("Successfully clicked on Transcation with given search criteria");
	  }
	}
	
	async function scrolltoTranscations_accountDetails(){
	
	  appLog("Intiated method to scroll to Transcations under account details");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lblTransactions"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","lblTransactions"],"text")).toEqual("Transactions");
	
	  //await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
	  //await kony.automation.scrollToWidget(["frmAccountsDetails","accountTransactionList","segTransactions"]);
	
	}
	
	async function VerifyAdvancedSearch_byAmount(AmountRange1,AmountRange2){
	
	  await clickOnSearch_AccountDetails();
	  await selectAmountRange(AmountRange1,AmountRange2);
	  await clickOnAdvancedSearchBtn();
	  await validateSearchResult();
	
	}
	
	async function VerifyAdvancedSearch_byTranxType(TransactionType){
	
	  await clickOnSearch_AccountDetails();
	  await selectTranscationtype(TransactionType);
	  await clickOnAdvancedSearchBtn();
	  await validateSearchResult();
	
	}
	
	async function VerifyAdvancedSearch_byDate(){
	
	  await clickOnSearch_AccountDetails();
	  await selectTimePeriod();
	  await clickOnAdvancedSearchBtn();
	  await validateSearchResult();
	}
	
	async function VerifyAdvancedSearch_byKeyword(keyword){
	
	  await clickOnSearch_AccountDetails();
	  await enterKeywordtoSearch(keyword);
	  await clickOnAdvancedSearchBtn();
	  await validateSearchResult();
	}
	
	async function MoveBackToLandingScreen_AccDetails(){
	
	  appLog("Intiated method to Move back to Account Dashboard from AccountsDetails");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmAccountsDetails","customheader","topmenu","flxaccounts"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  appLog("Successfully Moved back to Account Dashboard");
	}
	
	async function VerifyAccountOnDashBoard(AccountType){
	
	  appLog("Intiated method to verify : <b>"+AccountType+"</b>");
	  var myList = new Array();
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  var finished = false;
	  for(var x = 0; x <segLength && !finished; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    //appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	
	      var seg="segAccounts["+x+","+y+"]";
	
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	      if(typeOfAccount.includes(AccountType)){
	        appLog("Successfully verified : <b>"+accountName+"</b>");
	        myList.push("TRUE");
	        finished = true;
	        break;
	      }else{
	        myList.push("FALSE");
	      }
	    }
	  }
	
	  appLog("My Actual List is :: "+myList);
	  var Status=JSON.stringify(myList).includes("TRUE");
	  appLog("Over all Result is  :: <b>"+Status+"</b>");
	  expect(Status).toBe(true);
	}
	
	
	async function VerifyCheckingAccountonDashBoard(){
	
	  appLog("Intiated method to verify Checking account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[0,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[0,0]","lblAccountName"], "text")).toContain("Checking");
	  VerifyAccountOnDashBoard("Checking");
	}
	
	async function VerifySavingsAccountonDashBoard(){
	
	  appLog("Intiated method to verify Savings account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[1,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[1,0]","lblAccountName"], "text")).toContain("Saving");
	  VerifyAccountOnDashBoard("Saving");
	}
	async function VerifyCreditCardAccountonDashBoard(){
	
	  appLog("Intiated method to verify CreditCard account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[2,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[2,0]","lblAccountName"], "text")).toContain("Credit");
	  VerifyAccountOnDashBoard("Credit");
	}
	
	async function VerifyDepositAccountonDashBoard(){
	
	  appLog("Intiated method to verify Deposit account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[3,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[3,0]","lblAccountName"], "text")).toContain("Deposit");
	  VerifyAccountOnDashBoard("Deposit");
	}
	
	async function VerifyLoanAccountonDashBoard(){
	
	  appLog("Intiated method to verify Loan account in Dashboard");
	
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	  //   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts[4,0]","lblAccountName"],15000);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts[4,0]","lblAccountName"], "text")).toContain("Loan");
	  VerifyAccountOnDashBoard("Loan");
	}
	
	async function verifyViewAllTranscation(){
	
	  appLog("Intiated method to view all Tranx in AccountDetails");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","lblTransactions"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","lblTransactions"],"text")).toEqual("Transactions");
	}
	
	
	async function verifyContextMenuOptions(myList_Expected){
	
	  //var myList_Expected = new Array();
	  //myList_Expected.push("Transfer","Pay Bill","Stop Check Payment","Manage Cards","View Statements","Account Alerts");
	  myList_Expected.push(myList_Expected);
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	  var segLength=accounts_Size.length;
	  //appLog("Length is :: "+segLength);
	  var myList = new Array();
	
	  for(var x = 0; x <segLength-1; x++) {
	
	    var seg="segAccountListActions["+x+"]";
	    //appLog("Segment is :: "+seg);
	    await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"lblUsers"],15000);
	    var options=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"lblUsers"], "text");
	    //appLog("Text is :: "+options);
	    myList.push(options);
	  }
	
	  appLog("My Actual List is :: "+myList);
	  appLog("My Expected List is:: "+myList_Expected);
	
	  let isFounded = myList.some( ai => myList_Expected.includes(ai) );
	  //appLog("isFounded"+isFounded);
	  expect(isFounded).toBe(true);
	}
	
	async function MoveBackToLandingScreen_Accounts(){
	
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxaccounts"]);
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	}
	
	
	async function verifyAccountSummary_CheckingAccounts(){
	
	  appLog("Intiated method to verify account summary for Checking Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountSummary_DepositAccounts(){
	
	  appLog("Intiated method to verify account summary for Deposit Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue3Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountSummary_CreditCardAccounts(){
	
	  appLog("Intiated method to verify account summary for CreditCard Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue3Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue4Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	
	}
	
	async function verifyAccountSummary_LoanAccounts(){
	
	  appLog("Intiated method to verify account summary for Loan Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function verifyAccountSummary_SavingsAccounts(){
	
	  appLog("Intiated method to verify account summary for Savings Account");
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue1Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue6Tab1","lblFormat"],5000);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue7Tab1","lblFormat"],5000);
	}
	
	async function VerifyPendingWithdrawls_accountSummary(){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","btnTab1"],15000);
	  kony.automation.button.click(["frmAccountsDetails","summary","btnTab1"]);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"]);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","summary","FormatValue2Tab1","lblFormat"], "text")).not.toBe("");
	
	}
	
	async function VerifyInterestdetails_accountSummary(){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","btnTab2"],15000);
	  kony.automation.button.click(["frmAccountsDetails","summary","btnTab2"]);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","lbl6Tab2"],10000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","summary","lbl6Tab2"], "text")).not.toBe("");
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","lbl7Tab2"],10000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","summary","lbl7Tab2"], "text")).not.toBe("");
	
	}
	
	async function VerifySwiftCode_accountSummary(){
	
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","btnTab3"],15000);
	  kony.automation.button.click(["frmAccountsDetails","summary","btnTab3"]);
	  await kony.automation.playback.waitFor(["frmAccountsDetails","summary","lbl6Tab3"],10000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","summary","lbl6Tab3"], "text")).not.toBe("");
	}
	
	async function selectContextMenuOption(Option){
	
	  appLog("Intiated method to select context menu option :: "+Option);
	
	  var myList = new Array();
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"],30000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu","segAccountListActions"],"data");
	
	  var segLength=accounts_Size.length;
	  appLog("Length is :: "+segLength);
	  for(var x = 0; x <segLength; x++) {
	
	    var seg="segAccountListActions["+x+"]";
	    //appLog("Segment will be :: "+seg);
	    await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg],15000);
	    var TransfersText=kony.automation.widget.getWidgetProperty(["frmDashboard","accountListMenu",seg,"lblUsers"], "text");
	    appLog("Menu Item Text is :: "+TransfersText);
	    if(TransfersText===Option){
	      appLog("Option to be selected is :"+TransfersText);
	       await kony.automation.playback.waitFor(["frmDashboard","accountListMenu",seg,"flxAccountTypes"],15000);
	      //kony.automation.flexcontainer.click(["frmDashboard","accountListMenu",seg,"flxAccountTypes"]);
	      kony.automation.widget.touch(["frmDashboard","accountListMenu",seg,"flxAccountTypes"], null,null,[45,33]);
	      appLog("Successfully selected menu option  : <b>"+TransfersText+"</b>");
	      await kony.automation.playback.wait(10000);
	      myList.push("TRUE");
	      break;
	    }else{
	      myList.push("FALSE");
	    }
	  }
	
	  appLog("My Actual List is :: "+myList);
	  var Status=JSON.stringify(myList).includes("TRUE");
	  appLog("Over all Result is  :: <b>"+Status+"</b>");
	
	  expect(Status).toBe(true,"Failed to click on option <b>"+Option+"</b>");
	}
	
	// async function SelectContextualOnDashBoard(AccountType){
	
	//   appLog("Intiated method to analyze accounts data Dashboard");
	
	//   var Status=false;
	
	//   await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],15000);
	//   var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	//   var segLength=accounts_Size.length;
	
	//   var finished = false;
	//   for(var x = 0; x <segLength && !finished; x++) {
	
	//     var segHeaders="segAccounts["+x+",-1]";
	
	//     var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	//     var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	//     //appLog('Sub accounts size is '+subaccounts_Length);
	
	//     for(var y = 0; y <subaccounts_Length; y++){
	
	//       var seg="segAccounts["+x+","+y+"]";
	
	//       var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	//       var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	//       if(typeOfAccount.includes(AccountType)){
	//         await kony.automation.scrollToWidget(["frmDashboard","accountList",seg]);
	//         kony.automation.flexcontainer.click(["frmDashboard","accountList",seg,"flxMenu"]);
	//         appLog("Successfully Clicked on Menu of : <b>"+accountName+"</b>");
	//         await kony.automation.playback.wait(5000);
	//         finished = true;
	//         Status=true;
	//         break;
	//       }
	//     }
	//   }
	
	//   expect(Status).toBe(true,"Failed to click on Menu of AccType: <b>"+AccountType+"</b>");
	// }
	
	async function SelectContextualOnDashBoard(AccountNumber){
	
	  appLog("Intiated method to select Menu of account : "+AccountNumber);
	  await kony.automation.playback.wait(5000);
	  var Status=false;
	
	  await kony.automation.playback.waitFor(["frmDashboard","accountList","segAccounts"],30000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	
	  var finished = false;
	  for(var x = 0; x <segLength && !finished; x++) {
	
	    var segHeaders="segAccounts["+x+",-1]";
	
	    var subaccounts_Size=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",segHeaders,"lblAccountTypeNumber"],"text");
	    var subaccounts_Length= parseInt(subaccounts_Size.substring(1, 2));
	    appLog('Sub accounts size is '+subaccounts_Length);
	
	    for(var y = 0; y <subaccounts_Length; y++){
	
	      var seg="segAccounts["+x+","+y+"]";
	
	      var accountName=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountName"], "text");
	      var typeOfAccount=kony.automation.widget.getWidgetProperty(["frmDashboard","accountList",seg,"lblAccountType"], "text");
	      appLog("Account Name is : "+accountName);
	      if(accountName.includes(AccountNumber)){
	        kony.automation.flexcontainer.click(["frmDashboard","accountList",seg,"flxMenu"]);
	        appLog("Successfully Clicked on Menu of : <b>"+accountName+"</b>");
	        await kony.automation.playback.wait(5000);
	        // Validate really list is displayed or not
	        var menuList=await kony.automation.playback.waitFor(["frmDashboard","accountListMenu","segAccountListActions"],15000);
	        expect(menuList).toBe(true,"Failed to display contextual menu list items");
	        finished = true;
	        Status=true;
	        break;
	      }
	    }
	  }
	
	  expect(Status).toBe(true,"Failed to click on Menu of AccNumber: <b>"+AccountNumber+"</b>");
	}
	
	async function verifyVivewStatementsHeader(){
	
	  appLog('Intiated method to verify account statement header');
	  var Status=await kony.automation.playback.waitFor(["frmAccountsDetails","viewStatementsnew","lblViewStatements"],15000);
	  //kony.automation.widget.getWidgetProperty(["frmAccountsDetails","viewStatementsnew","lblViewStatements"], "text")).toContain("Statements");
	  expect(Status).toBe(true,"Failed to Navigate to Account Statements");
	  //appLog('Successfully Verified Account Statement Header');
	}
	
	async function VerifyPostedPendingTranscation(){
	
	  appLog('Intiated method to verify Pending and Posted Transcations');
	  await kony.automation.playback.waitFor(["frmAccountsDetails","accountTransactionList","segTransactions"],15000);
	  var TransType=kony.automation.widget.getWidgetProperty(["frmAccountsDetails","accountTransactionList","segTransactions[0,-1]","lblTransactionHeader"], "text");
	  if(TransType.includes("Posted")||TransType.includes("Pending")){
	    //kony.automation.flexcontainer.click(["frmAccountsDetails","accountTransactionList","segTransactions[0,0]","flxDropdown"]);
	    //await kony.automation.playback.wait(5000);
	    appLog('Transfer Type is : '+TransType);
	  }else{
	    appLog('No Pending or Posted Transcation available');
	  }
	
	}
	
	
	async function VerifyStopChequePaymentScreen(){
	  
	  appLog("Intiated method to verify StopChequePayment Screen");
	  var Status=await kony.automation.playback.waitFor(["frmStopPayments","lblChequeBookRequests"],30000);
	  expect(Status).toBe(true,"Failed to Verify StopChequePayment screen");
	  appLog("Successfully Verified StopChequePayment");
	  
	}
	
	async function VerifyAccountAlertScreen(){
	  
	  appLog("Intiated method to verify AccountAlert Screen");
	  var Status=await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAlertsHeading"],30000);
	  expect(Status).toBe(true,"Failed to Verify AccountAlert screen");
	  appLog("Successfully Verified AccountAlert");
	}
	
	async function MoveBack_Accounts_StopChequeBook(){
	  
	  appLog("Intiated method to Move back to Account Dashboard from StopPayments");
	  await kony.automation.playback.waitFor(["frmStopPayments","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmStopPayments","customheadernew","flxAccounts"]);
	
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  appLog("Successfully Moved back to Account Dashboard");
	
	}
	
	async function MoveBack_Accounts_ProfileManagement(){
	
	  // Move back to base state
	  appLog("Intiated method to Move back to Account Dashboard from ProfileManagement");
	  await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],15000);
	  kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
	  
	  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  appLog("Successfully Moved back to Account Dashboard");
	}
	
	
	async function navigateToBillPay(){
	
	  appLog("Intiated method to navigate to BillPay");
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","BillPayflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","BillPayflxAccountsMenu"]);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","BillPay0flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","BillPay0flxMyAccounts"]);
	  appLog("Successfully clicked on BillPay option");
	  await kony.automation.playback.wait(5000);
	  var isPayeeScreen=await kony.automation.playback.waitFor(["frmBulkPayees","lblTransactions"],15000);
	  if(isPayeeScreen){
	    expect(kony.automation.widget.getWidgetProperty(["frmBulkPayees","lblTransactions"], "text")).not.toBe("");
	  }else if(await kony.automation.playback.waitFor(["frmBillPayActivation","lblWarning"],5000)){
	    appLog("Custom Message : Activate Billpay feature to proceed further");
	  }else{
	    appLog("Custom Message : Failed to Navigate to BillPay screen");
	  }
	}
	
	async function navigateToOneTimePayment(){
	
	  appLog("Intiated method to navigate to OneTimePayment");
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
	  appLog("Successfully clicked on Menu on Dashboard");
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","BillPayflxAccountsMenu"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","BillPayflxAccountsMenu"]);
	  appLog("Successfully clicked on Billpay option");
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","BillPay4flxMyAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","BillPay4flxMyAccounts"]);
	  appLog("Successfully clicked on OneTimePayment option");
	  await kony.automation.playback.wait(10000);
	
	}
	
	async function enterOneTimePayeeInformation(payeeName,zipcode,accno,accnoAgain,mobileno){
	
	  appLog("Intiated method to enter OneTime Payee Information");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","tbxName"],15000);
	  kony.automation.textbox.enterText(["frmMakeOneTimePayee","tbxName"],payeeName);
	  appLog("Successfully entered payee name to auto select : <b>"+payeeName+"</b>");
	  await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","segPayeesName"],15000);
	  kony.automation.flexcontainer.click(["frmMakeOneTimePayee","segPayeesName[3]","flxNewPayees"]);
	  appLog("Successfully selected payee name from list");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtZipCode"],15000);
	  kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtZipCode"],zipcode);
	  appLog("Successfully entered zipcode : <b>"+zipcode+"</b>");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtAccountNumber"],15000);
	  kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtAccountNumber"],accno);
	  appLog("Successfully entered acc number : <b>"+accno+"</b>");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtAccountNumberAgain"],15000);
	  kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtAccountNumberAgain"],accnoAgain);
	  appLog("Successfully Re-entered account number : <b>"+accnoAgain+"</b>");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtmobilenumber"],15000);
	  kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtmobilenumber"],mobileno);
	  appLog("Successfully entered mobile number : <b>"+mobileno+"</b>");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","btnNext"],15000);
	  kony.automation.button.click(["frmMakeOneTimePayee","btnNext"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully clicked on Next button");
	}
	
	async function enterOneTimePaymentdetails(amount,note){
	
	  appLog("Intiated method to enter details for OneTime payment");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayment","txtPaymentAmount"],15000);
	  kony.automation.textbox.enterText(["frmMakeOneTimePayment","txtPaymentAmount"],amount);
	  appLog("Successfully entered amount : <b>"+amount+"</b>");
	
	  appLog("Intiated method to Select Payee From Acc for OneTime payment");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayment","txtTransferFrom"],15000);
	  kony.automation.widget.touch(["frmMakeOneTimePayment","txtTransferFrom"], [264,20],null,null);
	  kony.automation.flexcontainer.click(["frmMakeOneTimePayment","segTransferFrom[0,0]","flxAccountListItem"]);
	  appLog("Successfully selected Bill PayFrom");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayment","txtNotes"],15000);
	  kony.automation.textbox.enterText(["frmMakeOneTimePayment","txtNotes"],note);
	  appLog("Successfully entered note value : <b>"+note+"</b>");
	
	  await kony.automation.playback.waitFor(["frmMakeOneTimePayment","btnNext"],15000);
	  kony.automation.button.click(["frmMakeOneTimePayment","btnNext"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully clicked on Next button");
	}
	
	async function confirmOneTimePaymnet(){
	
	  appLog("Intiated method to confirm OneTimePayment");
	
	  await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","flxImgCheckBox"],15000);
	  kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","flxImgCheckBox"]);
	  appLog("Successfully accepted Checkbox");
	
	  await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","btnConfirm"],15000);
	  kony.automation.button.click(["frmOneTimePaymentConfirm","btnConfirm"]);
	  appLog("Successfully Clicked on Confirm Button");
	
	}
	
	async function verifyOneTimePaymentSuccessMsg(){
	
	  appLog("Intiated method to verify OneTimePayment SuccessMsg");
	
	  await kony.automation.playback.wait(5000);
	  var success=await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement"],30000);
	
	  if(success){
	    //await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","flxSuccess","lblSuccessMessage"],15000);
	    //expect(kony.automation.widget.getWidgetProperty(["frmOneTimePaymentAcknowledgement","flxSuccess","lblSuccessMessage"],"text")).not.toBe('');
	    await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	    appLog("Successfully Moved back to Accounts Dashboard");
	  }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayment","rtxDowntimeWarning"],5000)){
	    //appLog("Logged in User is not authorized to perform this action");
	    //fail('Logged in User is not authorized to perform this action');
	    appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmMakeOneTimePayment","rtxDowntimeWarning"],"text"));
	    fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmMakeOneTimePayment","rtxDowntimeWarning"],"text"));
	
	
	    await kony.automation.playback.waitFor(["frmMakeOneTimePayment","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
	    appLog("Successfully Moved back to Accounts Dashboard");
	  }else{
	    appLog("Unable to perform OneTimePayment");
	  }
	
	}
	
	
	async function navigateToManagePayee(){
	
	  await navigateToBillPay();
	  appLog("Intiated method to navigate to Manage Payee list");
	  await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"],15000);
	  kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	  appLog("Successfully clicked on Manage payee Button");
	  await kony.automation.playback.wait(5000);
	}
	
	async function selectPayee_ManagePayeeList(payeename){
	
	  appLog("Intiated method to select Payee from Manage Payee list : <b>"+payeename+"</b>");
	
	  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","txtSearch"],15000);
	  kony.automation.textbox.enterText(["frmManagePayees","manageBiller","txtSearch"],payeename);
	  appLog("Successfully entered Payee "+payeename);
	
	  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","btnConfirm"],15000);
	  kony.automation.flexcontainer.click(["frmManagePayees","manageBiller","btnConfirm"]);
	  appLog("Successfully clicked on Search button");
	  await kony.automation.playback.wait(5000);
	
	  //   await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","txtSearch"],15000);
	  //   kony.automation.textbox.enterText(["frmManagePayees","manageBiller","txtSearch"], [ { modifierCapsLock:true, key : 'A' },
	  //                                                             { modifierCapsLock:true, key : 'B' },
	  // 															{ modifierCapsLock:true, key : 'C' },
	  //                                                             { modifierCapsLock:false, keyCode : 13 }
	  // 														]);
	
	  appLog("Intiated Method to verify Payee <b>"+payeename+"</b>");
	
	  var PayeeList=await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","segmentBillPay"],15000);
	  if(PayeeList){
	    //expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","manageBiller","segmentBillPay[0]","lblColumn1"],"text")).toEqual(payeename);
	    expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","manageBiller","segmentBillPay[0]","lblColumn1"],"text")).not.toBe('');
	  }else if(await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","rtxNoPaymentMessage"],5000)){
	    appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmManagePayees","manageBiller","rtxNoPaymentMessage"],"text"));
	    fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmManagePayees","manageBiller","rtxNoPaymentMessage"],"text"));
	
	  }else{
	    appLog("Unable to find Payee in ManagePayees List");
	  }
	
	}
	
	async function clickOnBillPayBtn_ManagePayees(){
	
	
	  // BillPay and Active ebill has same locator hence verifying text and doing operation accordingly, Instead of directly failing.
	
	  appLog("Intiated method to click on Billpay button from Manage Payee list");
	
	  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","segmentBillPay"],15000);
	
	  var ButtonName=kony.automation.widget.getWidgetProperty(["frmManagePayees","manageBiller","segmentBillPay[0]","btnAction"], "text");
	
	  //appLog('Button Name is : '+ButtonName);
	
	  if(ButtonName==='Activate ebill'){
	
	    appLog("Info : <b>"+ButtonName+"</b>"+" is Available instead of BillPay button");
	    //Activate e Bill to convert button to PayaBill. instead of failing we can proceed execution
	    kony.automation.button.click(["frmManagePayees","manageBiller","segmentBillPay[0]","btnAction"]);
	    await kony.automation.playback.waitFor(["frmManagePayees","btnProceedIC"],15000);
	    kony.automation.button.click(["frmManagePayees","btnProceedIC"]);
	    appLog('Successfully clicked on YES button');
	    await kony.automation.playback.wait(10000);
	    kony.automation.button.click(["frmManagePayees","manageBiller","segmentBillPay[0]","btnAction"]);
	    appLog("Successfully clicked on BillPay button");
	
	  }else{
	
	    // We can directly click on BillPay button
	    kony.automation.button.click(["frmManagePayees","manageBiller","segmentBillPay[0]","btnAction"]);
	    appLog("Successfully clicked on BillPay button");
	    await kony.automation.playback.wait(5000);
	  }
	}
	
	
	async function enterAmount_SheduleBillPay(amount){
	
	  appLog("Intiated method to enter amount : <b>"+amount+"</b>");
	  await kony.automation.playback.waitFor(["frmPayABill","txtSearch"],15000);
	  kony.automation.textbox.enterText(["frmPayABill","txtSearch"],amount);
	  appLog("Successfully entered amount : <b>"+amount+"</b>");
	
	  await SelectPayFromAcc_SheduleBillPay();
	}
	
	async function SelectPayFromAcc_SheduleBillPay(){
	
	  appLog("Intiated method to Select Payee From");
	
	  await kony.automation.playback.waitFor(["frmPayABill","txtTransferFrom"],15000);
	  kony.automation.widget.touch(["frmPayABill","txtTransferFrom"], [600,17],null,null);
	  kony.automation.flexcontainer.click(["frmPayABill","segTransferFrom[0,0]","flxAccountListItem"]);
	
	  appLog("Successfully selected Payee from the list");
	}
	
	async function selectfrequency_SheduledBillPay(freq){
	
	  appLog("Intiated method to select freq : <b>"+freq+"</b>");
	  await kony.automation.playback.waitFor(["frmPayABill","lbxFrequency"],15000);
	  kony.automation.listbox.selectItem(["frmPayABill","lbxFrequency"],freq);
	  appLog("Successfully selected freq : "+freq);
	}
	
	async function SelectDateRange_SheduledBillpay() {
	
	  //new chnage in 202010
	  //await kony.automation.playback.wait(5000);
	  appLog("Intiated method to select DateRange");
	  await kony.automation.playback.waitFor(["frmPayABill","lbxForHowLong"],15000);
	  kony.automation.listbox.selectItem(["frmPayABill","lbxForHowLong"], "ON_SPECIFIC_DATE");
	
	  await kony.automation.playback.waitFor(["frmPayABill","calSendOn"],15000);
	  kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
	  appLog("Successfully selected sendOn Date");
	  await kony.automation.playback.waitFor(["frmPayABill","calEndingOn"],15000);
	  kony.automation.calendar.selectDate(["frmPayABill","calEndingOn"], [11,21,2021]);
	  appLog("Successfully selected EndOn Date");
	}
	
	async function SelectSendOnDate_SheduledBillpay() {
	
	  await kony.automation.playback.waitFor(["frmPayABill","calSendOn"],15000);
	  kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
	  appLog("Successfully selected sendOn Date");
	}
	
	async function SelectOccurences_SheduledBillPay(occurences) {
	  //new chnage in 202010
	  appLog("Intiated method to select N.of Occurences");
	  await kony.automation.playback.waitFor(["frmPayABill","lbxForHowLong"],15000);
	  kony.automation.listbox.selectItem(["frmPayABill","lbxForHowLong"], "NO_OF_RECURRENCES");
	  await kony.automation.playback.waitFor(["frmPayABill","txtEndingOn"],15000);
	  kony.automation.textbox.enterText(["frmPayABill","txtEndingOn"],occurences);
	  appLog("Successfully selected Occurences : <b>"+occurences+"</b>");
	}
	
	async function EnterNoteValue_SheduledBillPay(notes) {
	
	  appLog("Intiated method to enter note value");
	  await kony.automation.playback.waitFor(["frmPayABill","txtNotes"],15000);
	  kony.automation.textbox.enterText(["frmPayABill","txtNotes"],notes);
	  appLog("Successfully entered Note value : <b>"+notes+"</b>");
	
	  appLog("Intiated method to click on Confirm button");
	  await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"],15000);
	  kony.automation.button.click(["frmPayABill","btnConfirm"]);
	  appLog("Successfully clicked on Confirm button");
	}
	
	async function confirmSheduledBillpay(){
	
	  appLog("Intiated method to Confirm Sheduled BillPayment");
	
	  await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"],15000);
	  kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
	  appLog("Successfully accepted terms check box");
	
	  await kony.automation.playback.waitFor(["frmPayBillConfirm","btnConfirm"],15000);
	  kony.automation.button.click(["frmPayBillConfirm","btnConfirm"]);
	  appLog("Successfully clicked on Confirm button");
	}
	
	async function cancelSheduledBillPay(){
	
	  appLog("Intiated method to CANCEL Sheduled BillPayment");
	
	  await kony.automation.playback.waitFor(["frmPayBillConfirm","btnCancel"],15000);
	  kony.automation.button.click(["frmPayBillConfirm","btnCancel"]);
	  appLog("Successfully clicked on Cancel button");
	
	  await kony.automation.playback.waitFor(["frmPayBillConfirm","CancelPopup","lblPopupMessage"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPayBillConfirm","CancelPopup","lblPopupMessage"],"text")).toEqual("Are you sure you want to cancel this transaction?");
	
	  await kony.automation.playback.waitFor(["frmPayBillConfirm","CancelPopup","btnYes"],15000);
	  kony.automation.button.click(["frmPayBillConfirm","CancelPopup","btnYes"]);
	  appLog("Successfully clicked on YES button");
	
	  await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	  appLog("Successfully MovedBack to Account DashBoard");
	}
	
	async function verifySheduledBillpaySuccessMsg(){
	
	  appLog("Intiated method to verify Sheduled BillPay SuccessMsg");
	
	  await kony.automation.playback.wait(5000);
	  var Success= await kony.automation.playback.waitFor(["frmPayBillAcknowledgement"],30000);
	
	  if(Success){
	    //expect(kony.automation.widget.getWidgetProperty(["frmPayBillAcknowledgement","lblSuccessMessage"],"text")).not.toBe('');
	    await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	    appLog("Successfully MovedBack to Account DashBoard");
	  }else if(await kony.automation.playback.waitFor(["frmPayABill","rtxDowntimeWarning"],15000)){
	    //Checking for exception message
	    //Move back to dashboard again there is an exception message
	    appLog("Exception while performing a Sheduled BillPay");
	    await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	    await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
	    expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	    //appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmPayABill","rtxDowntimeWarning"],"text"));
	    //fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmPayABill","rtxDowntimeWarning"],"text"));
	    appLog("Failed : Unable to Perform Successfull Transcation. Failed with rtxDowntimeWarning");
	    fail("Failed : Unable to Perform Successfull Transcation. Failed with rtxDowntimeWarning");
	  }else{
	    appLog("Unable to verify Success Message");
	  }
	
	}
	
	async function navigateToSheduledBillPay(){
	
	  await navigateToBillPay();
	  await kony.automation.playback.waitFor(["frmBulkPayees","btnScheduled"],15000);
	  kony.automation.button.click(["frmBulkPayees","btnScheduled"]);
	  appLog("Successfully clicked on Sheduled tab");
	  await kony.automation.playback.wait(5000);
	}
	
	async function clickOnEditButton_SheduledBillPayment(){
	
	  appLog("Intiated method to click on Edit button");
	  await kony.automation.playback.waitFor(["frmBillPayScheduled","segmentBillpay"],15000);
	  kony.automation.button.click(["frmBillPayScheduled","segmentBillpay[0]","btnEdit"])
	  appLog("Successfully clicked on Edit button");
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmPayABill","lblPayABill"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPayABill","lblPayABill"],"text")).toEqual("Pay a Bill");
	
	}
	
	async function UpdatedSheduledBillPayment(notes){
	
	  await SelectPayFromAcc_SheduleBillPay();
	  await selectfrequency_SheduledBillPay("Once");
	  await EnterNoteValue_SheduledBillPay(notes);
	  await confirmSheduledBillpay();
	
	}
	async function EditSheduledBillPay(notes){
	
	  var nopayments=await kony.automation.playback.waitFor(["frmBillPayScheduled","rtxNoPaymentMessage"],15000);
	  if(nopayments){
	    appLog("There are no sheduled payments");
	    //Move back to accounts
	    await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"],15000);
	    kony.automation.button.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	    appLog("Successfully MovedBack to Account DashBoard");
	  }else{
	
	    appLog("There are few sheduled payments");
	    await clickOnEditButton_SheduledBillPayment();
	    await UpdatedSheduledBillPayment(notes);
	    var warning=await kony.automation.playback.waitFor(["frmPayABill","rtxDowntimeWarning"],15000);
	    if(warning){
	      await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],15000);
	      kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	      await verifyAccountsLandingScreen();
	      appLog("Successfully MovedBack to Account DashBoard");
	      //fail("Custom Message :: Amount Greater than Allowed Maximum Deposit");
	      appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmPayABill","rtxDowntimeWarning"],"text"));
	      fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmPayABill","rtxDowntimeWarning"],"text"));
	
	    }else{
	      await verifySheduledBillpaySuccessMsg();
	      await verifyAccountsLandingScreen();
	    }  
	
	  }
	}
	
	async function clickOnAddPayeeLink(){
	
	  appLog("Intiated method to click on Add payee link");
	  await kony.automation.playback.waitFor(["frmBulkPayees","flxAddPayee"],15000);
	  kony.automation.flexcontainer.click(["frmBulkPayees","flxAddPayee"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Clicked on addPayee link");
	}
	
	async function enterPayeeDetails_UsingPayeeinfo(payeeName,address1,address2,city,zipcode,accno,note){
	
	  appLog("Intiated method to Add Payee Details");
	
	  await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","btnEnterPayeeInfo"],15000);
	  kony.automation.button.click(["frmAddPayee1","addPayee","btnEnterPayeeInfo"]);
	  appLog("Successfully Clicked on EnterPayeeInfo Tab");
	  //await kony.automation.playback.wait(5000);
	
	  await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","tbx1Tab2"],15000);
	  kony.automation.textbox.enterText(["frmAddPayee1","addPayee","tbx1Tab2"],payeeName);
	  appLog("Successfully Entered Payee name as : <b>"+payeeName+"</b>");
	  await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","tbx2Tab2"],15000);
	  kony.automation.textbox.enterText(["frmAddPayee1","addPayee","tbx2Tab2"],address1);
	  appLog("Successfully Entered Address Line1 as : <b>"+address1+"</b>");
	  kony.automation.textbox.enterText(["frmAddPayee1","addPayee","tbx3Tab2"],address2);
	  appLog("Successfully Entered Address Line1 as : <b>"+address2+"</b>");
	  kony.automation.listbox.selectItem(["frmAddPayee1","addPayee","lbxCountry"], "IN");
	  kony.automation.listbox.selectItem(["frmAddPayee1","addPayee","lbxStateValue"], "IN-KA");
	  kony.automation.textbox.enterText(["frmAddPayee1","addPayee","tbx5Tab2"],city);
	  appLog("Successfully Entered CityName as : <b>"+city+"</b>");
	  kony.automation.textbox.enterText(["frmAddPayee1","addPayee","tbxZipCodeTab2"],zipcode);
	  appLog("Successfully Entered Zipcode as : <b>"+zipcode+"</b>");
	  kony.automation.flexcontainer.click(["frmAddPayee1","addPayee","flxContainer"]);
	  kony.automation.textbox.enterText(["frmAddPayee1","addPayee","tbx6Tab2"],accno);
	  appLog("Successfully Entered account number as : <b>"+accno+"</b>");
	  kony.automation.textbox.enterText(["frmAddPayee1","addPayee","tbx7Tab2"],accno);
	  appLog("Successfully Re-Entered account number as : <b>"+accno+"</b>");
	  kony.automation.button.click(["frmAddPayee1","addPayee","btnRight1"]);
	
	}
	
	async function clickOnNextButton_payeeDetails(){
	
	  appLog("Intiated method verify Payee Details");
	  await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","editDetailsBtnRight1"],15000);
	  kony.automation.button.click(["frmAddPayee1","addPayee","editDetailsBtnRight1"]);
	  appLog("Successfully Clicked on Next button ");
	
	  //await linkPayee();
	}
	
	async function SelectPayeeBankingType_payeeDetails(BankingType){
	
	  appLog("Intiated method to click on AddRecepientContinue");
	  var btnAddRecepient=await kony.automation.playback.waitFor(["frmPayeeDetails","btnAddRecepientContinue"],15000);
	  if(btnAddRecepient){
	    kony.automation.button.click(["frmPayeeDetails","btnAddRecepientContinue"]);
	    appLog("Successfully Clicked on AddRecepientContinue button ");
	    await kony.automation.playback.wait(5000);
	  }else{
	    appLog("Selecting Banking type screen is not available");
	  }
	
	}
	
	async function linkPayee(){
	
	  var linkreciptent=await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","contractList","lblHeader"],15000);
	
	  if(linkreciptent){
	    await kony.automation.playback.wait(5000);
	    kony.automation.widget.touch(["frmAddPayee1","addPayee","contractList","lblCheckBoxSelectAll"], [9,10],null,null);
	    kony.automation.flexcontainer.click(["frmAddPayee1","addPayee","contractList","flxCol4"]);
	    appLog("Successfully selected select All CheckBox");
	    kony.automation.button.click(["frmAddPayee1","addPayee","contractList","btnAction6"]);
	    appLog("Successfully Clicked on Link Reciptent Continue Button");
	  }
	}
	
	async function clickOnConfirmButton_verifyPayee(){
	
	  appLog("Intiated method to confirm Payee Details");
	  await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","btnAction6"],15000);
	  kony.automation.button.click(["frmAddPayee1","addPayee","btnAction6"]);
	  appLog("Successfully Clicked on Confirm button ");
	}
	
	async function verifyAddPayeeSuccessMsg(){
	
	  appLog("Intiated method to verify Add payee SuccessMsg");
	  //await kony.automation.playback.wait(5000);
	  var success=await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","lblSection1Message"],30000);
	
	  if(success){
	    expect(kony.automation.widget.getWidgetProperty(["frmAddPayee1","addPayee","lblSection1Message"],"text")).not.toBe('');
	    await kony.automation.playback.waitFor(["frmAddPayee1","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	    appLog("Successfully Moved back to Accounts dashboard");
	
	  }else if(await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","rtxDowntimeWarning"],5000)){
	
	    appLog("Intiated method to verify DowntimeWarning");
	    await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","rtxDowntimeWarning"],15000);
	    kony.automation.flexcontainer.click(["frmAddPayee1","addPayee","rtxDowntimeWarning"]);
	
	    appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmAddPayee1","addPayee","rtxDowntimeWarning"],"text"));
	    fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmAddPayee1","addPayee","rtxDowntimeWarning"],"text"));
	
	  }else{
	    appLog("Unable to add Payee");
	  }
	
	}
	
	
	async function expandPayee_ManagePayee(){
	
	  appLog("Intiated method to Expand payee from Manage payee");
	  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","segmentBillPay"],15000);
	  kony.automation.flexcontainer.click(["frmManagePayees","manageBiller","segmentBillPay[0]","flxDropdown"]);
	  appLog("Successfully clicked on Manage Payees dropdown arrow");
	}
	
	async function MoveBackToDashBoard_ManagePayees(){
	
	  await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	  appLog("Successfully Moved back to Accounts dashboard");
	}
	async function clickOnEditBtn_ManagePayees(){
	
	  await expandPayee_ManagePayee();
	  appLog("Intiated method to Edit Biller");
	  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","segmentBillPay"],15000);
	  kony.automation.button.click(["frmManagePayees","manageBiller","segmentBillPay[0]","btn3"]);
	  appLog("Successfully clicked on Editbutton under manage payee");
	}
	
	async function deletePayee_ManagePayee(){
	
	  appLog("Intiated method to Delete Payee");
	  await expandPayee_ManagePayee();
	  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","segmentBillPay"],15000);
	  kony.automation.button.click(["frmManagePayees","manageBiller","segmentBillPay[0]","btn4"]);
	  appLog("Successfully clicked on Delete button under manage payee");
	  await kony.automation.playback.waitFor(["frmManagePayees","btnYesIC"],15000);
	  kony.automation.button.click(["frmManagePayees","btnYesIC"]);
	  appLog("Successfully clicked on YES button on delete biller");
	  await kony.automation.playback.wait(5000);
	  await MoveBackToDashBoard_ManagePayees();
	}
	
	async function EditPayee_ManagePayee(){
	
	  appLog("Intiated method to Edit Payee");
	
	  await clickOnEditBtn_ManagePayees();
	
	//   appLog("Intiated method to updated biller");
	//   await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","tbxName"],15000);
	//   kony.automation.textbox.enterText(["frmAddPayee1","addPayee","tbxName"],"EditBiller");
	//   appLog("Successfully entered Updated biller value");
	
	  appLog("Intiated method to click on Continue button");
	  await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","btnSave"],15000);
	  kony.automation.button.click(["frmAddPayee1","addPayee","btnSave"]);
	  appLog("Successfully Clicked on Save button")
	
	  // Currently not present on UI
	  //   appLog("Intiated method to click on Savelink Continue button");
	  //   await kony.automation.playback.waitFor(["frmManagePayees","contractList","btnAction6"],15000);
	  //   kony.automation.button.click(["frmManagePayees","contractList","btnAction6"]);
	  //   await kony.automation.playback.wait(5000);
	  //   appLog("Successfully Clicked on Savelink Continue button");
	
	  await verifyUpdatePayeeSuccessMsg();
	
	}
	
	
	async function verifyUpdatePayeeSuccessMsg(){
	
	  appLog("Intiated method to verify Update payee SuccessMsg");
	  await kony.automation.playback.wait(5000);
	  var successMsg=await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","lblSection1Message"],30000);
	  if(successMsg){
	    expect(kony.automation.widget.getWidgetProperty(["frmAddPayee1","addPayee","lblSection1Message"],"text")).not.toBe('');
	    await kony.automation.playback.waitFor(["frmAddPayee1","customheadernew","flxAccounts"],15000);
	    kony.automation.flexcontainer.click(["frmAddPayee1","customheadernew","flxAccounts"]);
	    appLog("Successfully Moved back to Accounts dashboard");
	  }else if(await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","rtxDowntimeWarning"],5000)){
	    appLog("Intiated method to verify DowntimeWarning");
	    await kony.automation.playback.waitFor(["frmAddPayee1","addPayee","rtxDowntimeWarning"],15000);
	    kony.automation.flexcontainer.click(["frmAddPayee1","addPayee","rtxDowntimeWarning"]);
	
	    appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmAddPayee1","addPayee","rtxDowntimeWarning"],"text"));
	    fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmAddPayee1","addPayee","rtxDowntimeWarning"],"text"));
	
	  }else{
	    appLog("Unable to add Payee");
	  }
	
	}
	
	async function navigateToPastBillPay(){
	
	  appLog("Intiated method to navigate to Billpay History");
	  await navigateToBillPay();
	  await kony.automation.playback.waitFor(["frmBulkPayees","btnHistory"],15000);
	  kony.automation.button.click(["frmBulkPayees","btnHistory"]);
	  appLog("Successfully clicked on History tab");
	  await kony.automation.playback.wait(5000);
	}
	
	async function clickonRepeatButton_PastBillpay(){
	
	  appLog("Intiated method to click on Repeat button");
	  await kony.automation.playback.waitFor(["frmBillPayHistory","segmentBillpay"],15000);
	  kony.automation.button.click(["frmBillPayHistory","segmentBillpay[0]","btnRepeat"]);
	  appLog("Successfully clicked on Repeat tab");
	  await kony.automation.playback.wait(5000);
	}
	
	async function repeatPastBillPayment(note){
	
	  appLog("Intiated method to Repeat a BillPay");
	
	  var nopayments=await kony.automation.playback.waitFor(["frmBillPayHistory","rtxNoPaymentMessage"],15000);
	
	  if(nopayments){
	    appLog("There are no History payments");
	    //Move back to accounts
	    await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"],15000);
	    kony.automation.button.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	    appLog("Successfully Moved back to Accounts dashboard");
	  }else{
	
	    await clickonRepeatButton_PastBillpay();
	    await SelectPayFromAcc_SheduleBillPay();
	    await EnterNoteValue_SheduledBillPay(note);
	    await confirmSheduledBillpay();
	
	    var warning=await kony.automation.playback.waitFor(["frmPayABill","rtxDowntimeWarning"],15000);
	    if(warning){
	      await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],15000);
	      kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	      await verifyAccountsLandingScreen();
	      appLog("Successfully Moved back to Accounts dashboard");
	      //fail("Custom Message :: Amount Greater than Allowed Maximum Deposit");
	      appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmPayABill","rtxDowntimeWarning"],"text"));
	      fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmPayABill","rtxDowntimeWarning"],"text"));
	
	
	    }else{
	      await verifySheduledBillpaySuccessMsg();
	      await verifyAccountsLandingScreen();
	      appLog("Successfully Moved back to Accounts dashboard");
	    }
	
	  }
	}
	
	async function clickOnAllpayeesTab(){
	
	  appLog("Intiated method to click on Allpayees tab");
	  await kony.automation.playback.waitFor(["frmBulkPayees","btnAllPayees"],15000);
	  kony.automation.button.click(["frmBulkPayees","btnAllPayees"]);
	  appLog("Successfully clicked on Allpayees tab");
	  await kony.automation.playback.wait(5000);
	}
	
	async function verifyAllPayeesList(){
	
	  appLog("Intiated method to verify Allpayees List");
	
	  var PayeeList=await kony.automation.playback.waitFor(["frmBulkPayees","segmentBillpay"],15000);
	
	  if(PayeeList){
	    kony.automation.flexcontainer.click(["frmBulkPayees","segmentBillpay[0]","flxDropdown"]);
	    appLog("Successfully verified on Allpayees List");
	  }else if(await kony.automation.playback.waitFor(["frmBulkPayees","rtxNoPaymentMessage"],5000)){
	
	    appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmBulkPayees","rtxNoPaymentMessage"],"text"));
	    fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmBulkPayees","rtxNoPaymentMessage"],"text"));
	
	  }else {
	    appLog("Unable to verify Allpayees List");
	  }
	
	}
	
	async function MoveBackToDashBoard_AllPayees(){
	
	  await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],15000);
	  kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	}
	
	async function clickOnSavePayeeButton_OneTimePay(){
	
	  appLog("Intiated method to Save Payee from OneTime Payment");
	
	  await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","btnSavePayee"],15000);
	  kony.automation.button.click(["frmOneTimePaymentAcknowledgement","btnSavePayee"]);
	  appLog("Successfully Clicked on Save button");
	
	  //Continue Button
	  await kony.automation.playback.waitFor(["frmPayeeDetails","btnDetailsConfirm"],15000);
	  kony.automation.button.click(["frmPayeeDetails","btnDetailsConfirm"]);
	  appLog("Successfully Clicked on Continue button");
	
	  //Confirm Button
	  await kony.automation.playback.waitFor(["frmVerifyPayee","btnConfirm"],15000);
	  kony.automation.button.click(["frmVerifyPayee","btnConfirm"]);
	  appLog("Successfully Clicked on Confirm button");
	
	  await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAddPayee"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPayeeAcknowledgement","lblAddPayee"],"text")).toEqual("Add Payee");
	
	
	  await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],"text")).toContain("has been added.");
	  appLog("Successfully verified Added payee");
	
	  await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","btnViewAllPayees"],15000);
	  kony.automation.button.click(["frmPayeeAcknowledgement","btnViewAllPayees"]);
	  appLog("Successfully clicked on ViewAll payees button");
	
	}
	
	async function activateBillPayTermsconditions(){
	
	  appLog("Intiated method to Activate Billpay TC's");
	
	  var warning=await kony.automation.playback.waitFor(["frmBillPayActivation","lblWarning"],15000);
	  if(warning){
	    //expect(kony.automation.widget.getWidgetProperty(["frmBillPayActivation","lblWarning"], "text")).toEqual("Please activate My Bills.");
	    appLog("Intiated method to select Default account");
	    await kony.automation.playback.waitFor(["frmBillPayActivation","txtTransferFrom"],15000);
	    kony.automation.widget.touch(["frmBillPayActivation","txtTransferFrom"], [97,15],null,null);
	    kony.automation.flexcontainer.click(["frmBillPayActivation","segTransferFrom[0,0]","flxAmount"]);
	    appLog("Successfully Selected Default BillPay Acc");
	    await kony.automation.playback.waitFor(["frmBillPayActivation","lblFavoriteEmailCheckBox"],15000);
	    kony.automation.widget.touch(["frmBillPayActivation","lblFavoriteEmailCheckBox"], null,null,[14,13]);
	    appLog("Successfully accepted checkbox");
	    await kony.automation.playback.waitFor(["frmBillPayActivation","flxAgree"],15000);
	    kony.automation.flexcontainer.click(["frmBillPayActivation","flxAgree"]);
	    appLog("Successfully clicked on AgreeFlex");
	    await kony.automation.playback.waitFor(["frmBillPayActivation","btnProceed"],15000);
	    kony.automation.button.click(["frmBillPayActivation","btnProceed"]);
	    appLog("Successfully clicked on Proceed button");
	    var Success=await kony.automation.playback.waitFor(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"],15000)
	    if(Success){
	      kony.automation.flexcontainer.click(["frmBillPayActivationAcknowledgement","customheadernew","flxAccounts"]);
	      appLog('Successfully Acivated BillPay feature');
	    }else if(await kony.automation.playback.waitFor(["frmBillPayActivation","rtxErrorMessage"],5000)){
	      fail('Error while activating BillPay feature');
	      await kony.automation.playback.waitFor(["frmBillPayActivation","customheadernew","flxAccounts"],15000);
	      kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
	    }else {
	      fail('Failed to activate Billpay Feature');
	    }
	  }else{
	    appLog("Already accepted billpay activation..Moveback to dashboard");
	    await MoveBackToDashBoard_AllPayees();
	  }
	}
	
	async function activateNewlyAddedpayee(){
	
	  appLog('Intiated method to activate Newly Added Payee');
	
	  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","segmentBillPay"],15000);
	
	  var ButtonName=kony.automation.widget.getWidgetProperty(["frmManagePayees","manageBiller","segmentBillPay[0]","btnAction"], "text");
	
	  //appLog('Button Name is : '+ButtonName);
	
	  if(ButtonName==='Activate ebill'){
	
	    kony.automation.button.click(["frmManagePayees","manageBiller","segmentBillPay[0]","btnAction"]);
	    appLog('Successfully clicked on activate button');
	
	    var activate=await kony.automation.playback.waitFor(["frmManagePayees","lblWarningOneIC"],15000);
	    if(activate){
	      await kony.automation.playback.waitFor(["frmManagePayees","btnProceedIC"],15000);
	      kony.automation.button.click(["frmManagePayees","btnProceedIC"]);
	      appLog('Successfully clicked on YES button');
	      await kony.automation.playback.wait(10000);
	      await MoveBackToDashBoard_ManagePayees();
	    }else {
	      appLog('Failed : Unable to Activate Added Payee');
	      fail('Failed : Unable to Activate Added Payee');
	    }
	  }else {
	    appLog('Payee Already activated');
	  }
	
	}
	
	it("APrerequestie_ActivateMyBills", async function() {
	
	  await navigateToBillPay();
	  await activateBillPayTermsconditions();
	  
	},120000);
	
	it("PayBills_AddPayee", async function() {
	
	 // Add payee and Then Delete same payee
	  var unique_RecipitentName=BillPay.addPayee.unique_RecipitentName+getRandomString(5);
	  var unique_Accnumber="0"+new Date().getTime();
	
	  await navigateToBillPay();
	  await clickOnAddPayeeLink();
	  await enterPayeeDetails_UsingPayeeinfo(unique_RecipitentName,BillPay.addPayee.address1,BillPay.addPayee.address2,BillPay.addPayee.city,BillPay.addPayee.zipcode,unique_Accnumber,"PayBills_AddPayee");
	  await clickOnNextButton_payeeDetails();
	  await clickOnConfirmButton_verifyPayee();
	  await verifyAddPayeeSuccessMsg();
	  await verifyAccountsLandingScreen();
	  
	  //Activate Payee to initilize payments
	  await navigateToManagePayee();
	  await selectPayee_ManagePayeeList(unique_RecipitentName);
	  await activateNewlyAddedpayee();
	  
	  
	   //Delete same payee
	  //await navigateToManagePayee();
	  //await selectPayee_ManagePayeeList("Test");
	  //await deletePayee_ManagePayee();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.waitFor(["frmBulkPayees","flxAddPayee"]);
	//   kony.automation.flexcontainer.click(["frmBulkPayees","flxAddPayee"]);
	//   await kony.automation.playback.waitFor(["frmAddPayee1","btnEnterPayeeInfo"]);
	//   kony.automation.button.click(["frmAddPayee1","btnEnterPayeeInfo"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterName"]);
	//   kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterName"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterAddress"]);
	//   kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterAddress"],"LR PALLI");
	//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterAddressLine2"]);
	//   kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterAddressLine2"],"ATMAKUR");
	//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxCity"]);
	//   kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxCity"],"ATMAKUR");
	//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterZipCode"]);
	//   kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterZipCode"],"500055");
	//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterAccountNmber"]);
	//   kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterAccountNmber"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxConfirmAccNumber"]);
	//   kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxConfirmAccNumber"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","flxClick"]);
	//   kony.automation.flexcontainer.click(["frmAddPayeeInformation","flxClick"]);
	//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxAdditionalNote"]);
	//   kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxAdditionalNote"],"ADD PAYYE");
	//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","btnReset"]);
	//   kony.automation.button.click(["frmAddPayeeInformation","btnReset"]);
	//   await kony.automation.playback.waitFor(["frmPayeeDetails","btnDetailsConfirm"]);
	//   kony.automation.button.click(["frmPayeeDetails","btnDetailsConfirm"]);
	//   await kony.automation.playback.waitFor(["frmVerifyPayee","btnConfirm"]);
	//   kony.automation.button.click(["frmVerifyPayee","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAcknowledgementMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],"text")).toContain("has been added.");
	
	//   await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   //Delete same payee
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
	//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"Test");
	//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","segmentBillpay[0]","flxDropdown"]);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnDeleteBiller"]);
	//   await kony.automation.playback.waitFor(["frmManagePayees","DeletePopup","btnYes"]);
	//   kony.automation.button.click(["frmManagePayees","DeletePopup","btnYes"]);
	//   await kony.automation.playback.wait(5000);
	
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},120000);
	
	it("PayBills_EditPayee", async function() {
	  
	  await navigateToManagePayee();
	  await selectPayee_ManagePayeeList(BillPay.addPayee.unique_RecipitentName);
	  await clickOnEditBtn_ManagePayees();
	  await EditPayee_ManagePayee();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
	//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"ABC");
	//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","segmentBillpay[0]","flxDropdown"]);
	//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnEditBiller"]);
	  
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","segmentBillpay[0]","flxDetailsWrapper"]);
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.textbox.enterText(["frmManagePayees","segmentBillpay[0]","tbxPinCode"],"123456");
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnSave"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},120000);
	
	it("PayBills_DeletePayee", async function() {
	  
	  // Add payee and Then Delete same payee
	  var delete_RecipitentName=BillPay.deletePayee.delete_RecipitentName+getRandomString(5);
	  var unique_Accnumber="0"+new Date().getTime();
	
	  await navigateToBillPay();
	  await clickOnAddPayeeLink();
	  await enterPayeeDetails_UsingPayeeinfo(delete_RecipitentName,BillPay.deletePayee.address1,BillPay.deletePayee.address2,BillPay.deletePayee.city,BillPay.deletePayee.zipcode,unique_Accnumber,"PayBills_DeletePayee");
	  await clickOnNextButton_payeeDetails();
	  await clickOnConfirmButton_verifyPayee();
	  await verifyAddPayeeSuccessMsg();
	  await verifyAccountsLandingScreen();
	  
	   //Delete same payee
	  await navigateToManagePayee();
	  await selectPayee_ManagePayeeList(delete_RecipitentName);
	  await deletePayee_ManagePayee();
	
	//    var unique_RecipitentName="Test Automation_"+new Date().getTime();
	//   // Add payee and Then Delete same payee
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.waitFor(["frmBulkPayees","flxdeletePayee"]);
	//   kony.automation.flexcontainer.click(["frmBulkPayees","flxdeletePayee"]);
	//   await kony.automation.playback.waitFor(["frmdeletePayee1","btnEnterPayeeInfo"]);
	//   kony.automation.button.click(["frmdeletePayee1","btnEnterPayeeInfo"]);
	//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","tbxEnterName"]);
	//   kony.automation.textbox.enterText(["frmdeletePayeeInformation","tbxEnterName"],unique_RecipitentName);
	//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","tbxEnterAddress"]);
	//   kony.automation.textbox.enterText(["frmdeletePayeeInformation","tbxEnterAddress"],"LR PALLI");
	//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","tbxEnterAddressLine2"]);
	//   kony.automation.textbox.enterText(["frmdeletePayeeInformation","tbxEnterAddressLine2"],"ATMAKUR");
	//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","tbxCity"]);
	//   kony.automation.textbox.enterText(["frmdeletePayeeInformation","tbxCity"],"ATMAKUR");
	//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","tbxEnterZipCode"]);
	//   kony.automation.textbox.enterText(["frmdeletePayeeInformation","tbxEnterZipCode"],"500055");
	//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","tbxEnterAccountNmber"]);
	//   kony.automation.textbox.enterText(["frmdeletePayeeInformation","tbxEnterAccountNmber"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","tbxConfirmAccNumber"]);
	//   kony.automation.textbox.enterText(["frmdeletePayeeInformation","tbxConfirmAccNumber"],"1234567890");
	//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","flxClick"]);
	//   kony.automation.flexcontainer.click(["frmdeletePayeeInformation","flxClick"]);
	//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","tbxAdditionalNote"]);
	//   kony.automation.textbox.enterText(["frmdeletePayeeInformation","tbxAdditionalNote"],"ADD PAYYE");
	//   await kony.automation.playback.waitFor(["frmdeletePayeeInformation","btnReset"]);
	//   kony.automation.button.click(["frmdeletePayeeInformation","btnReset"]);
	//   await kony.automation.playback.waitFor(["frmPayeeDetails","btnDetailsConfirm"]);
	//   kony.automation.button.click(["frmPayeeDetails","btnDetailsConfirm"]);
	//   await kony.automation.playback.waitFor(["frmVerifyPayee","btnConfirm"]);
	//   kony.automation.button.click(["frmVerifyPayee","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAcknowledgementMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],"text")).toContain("has been added.");
	
	//   await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   //Delete same payee
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
	//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"Test");
	//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","segmentBillpay[0]","flxDropdown"]);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnDeleteBiller"]);
	//   await kony.automation.playback.waitFor(["frmManagePayees","DeletePopup","btnYes"]);
	//   kony.automation.button.click(["frmManagePayees","DeletePopup","btnYes"]);
	//   await kony.automation.playback.wait(5000);
	
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},120000);
	
	it("PayBills_MakeOneTimePayment", async function() {
	
	   await navigateToOneTimePayment();
	   await enterOneTimePayeeInformation(BillPay.oneTimePay.payeeName,BillPay.oneTimePay.zipcode,BillPay.oneTimePay.accno,BillPay.oneTimePay.accnoAgain,BillPay.oneTimePay.mobileno);
	   await enterOneTimePaymentdetails(BillPay.oneTimePay.amountValue,"PayBills_MakeOneTimePayment");
	   await confirmOneTimePaymnet();
	   await verifyOneTimePaymentSuccessMsg();
	   await verifyAccountsLandingScreen();
	  
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","flxMakeOneTimePayment"]);
	//   kony.automation.flexcontainer.click(["frmBulkPayees","flxMakeOneTimePayment"]);
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","tbxName"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","tbxName"],"A");
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","segPayeesName"]);
	//   kony.automation.flexcontainer.click(["frmMakeOneTimePayee","segPayeesName[3]","flxNewPayees"]);
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtZipCode"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtZipCode"],"500055");
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtAccountNumber"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtAccountNumber"],"1234567890");
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtAccountNumberAgain"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtAccountNumberAgain"],"1234567890");
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtmobilenumber"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtmobilenumber"],"1234567890");
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","btnNext"]);
	//   kony.automation.button.click(["frmMakeOneTimePayee","btnNext"]);
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayment","txtPaymentAmount"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayment","txtPaymentAmount"],"2.1");
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayment","txtNotes"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayment","txtNotes"],"test OneTime payment");
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayment","btnNext"]);
	//   kony.automation.button.click(["frmMakeOneTimePayment","btnNext"]);
	
	//   await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","flxImgCheckBox"]);
	//   kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","flxImgCheckBox"]);
	
	//   await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmOneTimePaymentConfirm","btnConfirm"]);
	  
	//   //expect(kony.automation.widget.getWidgetProperty(["frmOneTimePaymentAcknowledgement","flxSuccess","lblSuccessMessage"],"text")).toEqual("Success! Your transaction has been completed");
	  
	//   await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},120000);
	
	it("SheduledPayment_Daily", async function() {
	  
	  
	  await navigateToManagePayee();
	  await selectPayee_ManagePayeeList(BillPay.schedulePay.payeeName);
	  await clickOnBillPayBtn_ManagePayees();
	  await enterAmount_SheduleBillPay(BillPay.schedulePay.amountValue);
	  await selectfrequency_SheduledBillPay("Daily");
	  await SelectDateRange_SheduledBillpay();
	  await EnterNoteValue_SheduledBillPay("Sheduled BillPay-Daily");
	  await confirmSheduledBillpay();
	  await verifySheduledBillpaySuccessMsg();
	  await verifyAccountsLandingScreen();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
	//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"ABC");
	//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","segmentBillpay[0]","flxBillPaymentManagePayeesSelected","lblPayee"],"text")).toContain("ABC");
	
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnPayBill"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtSearch"],"1.2");
	//   await kony.automation.playback.waitFor(["frmPayABill","lbxFrequency"]);
	//   kony.automation.listbox.selectItem(["frmPayABill","lbxFrequency"], "Daily");
	
	//   //new chnage in 202010
	//   await kony.automation.playback.waitFor(["frmPayABill","lbxForHowLong"]);
	//   kony.automation.listbox.selectItem(["frmPayABill","lbxForHowLong"], "ON_SPECIFIC_DATE");
	//       //kony.automation.listbox.selectItem(["frmPayABill","lbxForHowLong"], "NO_OF_RECURRENCES");
	//   //new chnage in 202010
	  
	//   await kony.automation.playback.waitFor(["frmPayABill","calSendOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","calEndingOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calEndingOn"], [11,29,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtNotes"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtNotes"],"Sheduled BillPay-Daily");
	//   await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"]);
	//   kony.automation.button.click(["frmPayABill","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"]);
	//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmPayBillConfirm","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","lblSuccessMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmPayBillAcknowledgement","lblSuccessMessage"],"text")).toContain("We successfully scheduled your transfer");
	
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	
	},120000);
	
	it("SheduledPayment_EveryTwoWeeks", async function() {
	  
	  await navigateToManagePayee();
	  await selectPayee_ManagePayeeList(BillPay.schedulePay.payeeName);
	  await clickOnBillPayBtn_ManagePayees();
	  await enterAmount_SheduleBillPay(BillPay.schedulePay.amountValue);
	  await selectfrequency_SheduledBillPay("BiWeekly");
	  await SelectDateRange_SheduledBillpay();
	  await EnterNoteValue_SheduledBillPay("Sheduled BillPay-Every Two Weeks");
	  await confirmSheduledBillpay();
	  await verifySheduledBillpaySuccessMsg();
	  await verifyAccountsLandingScreen();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
	//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"ABC");
	//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","segmentBillpay[0]","flxBillPaymentManagePayeesSelected","lblPayee"],"text")).toContain("ABC");
	
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnPayBill"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtSearch"],"1.2");
	//   await kony.automation.playback.waitFor(["frmPayABill","lbxFrequency"]);
	//   kony.automation.listbox.selectItem(["frmPayABill","lbxFrequency"], "Every Two Weeks");
	//   await kony.automation.playback.waitFor(["frmPayABill","calSendOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","calEndingOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calEndingOn"], [11,29,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtNotes"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtNotes"],"Sheduled BillPay-Every Two Weeks");
	//   await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"]);
	//   kony.automation.button.click(["frmPayABill","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"]);
	//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmPayBillConfirm","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","lblSuccessMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmPayBillAcknowledgement","lblSuccessMessage"],"text")).toContain("Success!");
	
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	
	},120000);
	
	it("SheduledPayment_Monthly", async function() {
	  
	  await navigateToManagePayee();
	  await selectPayee_ManagePayeeList(BillPay.schedulePay.payeeName);
	  await clickOnBillPayBtn_ManagePayees();
	  await enterAmount_SheduleBillPay(BillPay.schedulePay.amountValue);
	  await selectfrequency_SheduledBillPay("Monthly");
	  await SelectDateRange_SheduledBillpay();
	  await EnterNoteValue_SheduledBillPay("Sheduled BillPay-Monthly");
	  await confirmSheduledBillpay();
	  await verifySheduledBillpaySuccessMsg();
	  await verifyAccountsLandingScreen();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
	//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"ABC");
	//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","segmentBillpay[0]","flxBillPaymentManagePayeesSelected","lblPayee"],"text")).toContain("ABC");
	
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnPayBill"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtSearch"],"1.2");
	//   await kony.automation.playback.waitFor(["frmPayABill","lbxFrequency"]);
	//   kony.automation.listbox.selectItem(["frmPayABill","lbxFrequency"], "Monthly");
	//   await kony.automation.playback.waitFor(["frmPayABill","calSendOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","calEndingOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calEndingOn"], [11,29,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtNotes"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtNotes"],"Sheduled BillPay-Monthly");
	//   await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"]);
	//   kony.automation.button.click(["frmPayABill","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"]);
	//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmPayBillConfirm","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","lblSuccessMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmPayBillAcknowledgement","lblSuccessMessage"],"text")).toContain("Success!");
	
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	
	},120000);
	
	it("SheduledPayment_Quarterly", async function() {
	  
	  await navigateToManagePayee();
	  await selectPayee_ManagePayeeList(BillPay.schedulePay.payeeName);
	  await clickOnBillPayBtn_ManagePayees();
	  await enterAmount_SheduleBillPay(BillPay.schedulePay.amountValue);
	  await selectfrequency_SheduledBillPay("Quarterly");
	  await SelectDateRange_SheduledBillpay();
	  await EnterNoteValue_SheduledBillPay("Sheduled BillPay-Quarterly");
	  await confirmSheduledBillpay();
	  await verifySheduledBillpaySuccessMsg();
	  await verifyAccountsLandingScreen();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
	//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"ABC");
	//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","segmentBillpay[0]","flxBillPaymentManagePayeesSelected","lblPayee"],"text")).toContain("ABC");
	
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnPayBill"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtSearch"],"1.2");
	//   await kony.automation.playback.waitFor(["frmPayABill","lbxFrequency"]);
	//   kony.automation.listbox.selectItem(["frmPayABill","lbxFrequency"], "Quarterly");
	//   await kony.automation.playback.waitFor(["frmPayABill","calSendOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","calEndingOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calEndingOn"], [11,29,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtNotes"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtNotes"],"Sheduled BillPay-Quarterly");
	//   await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"]);
	//   kony.automation.button.click(["frmPayABill","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"]);
	//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmPayBillConfirm","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","lblSuccessMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmPayBillAcknowledgement","lblSuccessMessage"],"text")).toContain("Success!");
	
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	
	},120000);
	
	it("SheduledPayment_Weekly", async function() {
	  
	  await navigateToManagePayee();
	  await selectPayee_ManagePayeeList(BillPay.schedulePay.payeeName);
	  await clickOnBillPayBtn_ManagePayees();
	  await enterAmount_SheduleBillPay(BillPay.schedulePay.amountValue);
	  await selectfrequency_SheduledBillPay("Weekly");
	  await SelectDateRange_SheduledBillpay();
	  await EnterNoteValue_SheduledBillPay("Sheduled BillPay-Weekly");
	  await confirmSheduledBillpay();
	  await verifySheduledBillpaySuccessMsg();
	  await verifyAccountsLandingScreen();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
	//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"ABC");
	//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","segmentBillpay[0]","flxBillPaymentManagePayeesSelected","lblPayee"],"text")).toContain("ABC");
	
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnPayBill"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtSearch"],"1.2");
	//   await kony.automation.playback.waitFor(["frmPayABill","lbxFrequency"]);
	//   kony.automation.listbox.selectItem(["frmPayABill","lbxFrequency"], "Weekly");
	//   await kony.automation.playback.waitFor(["frmPayABill","calSendOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","calEndingOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calEndingOn"], [11,29,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtNotes"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtNotes"],"Sheduled BillPay-Weekly");
	//   await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"]);
	//   kony.automation.button.click(["frmPayABill","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"]);
	//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmPayBillConfirm","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","lblSuccessMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmPayBillAcknowledgement","lblSuccessMessage"],"text")).toContain("Success!");
	
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	
	},120000);
	
	it("SheduledPayment_Yearly", async function() {
	  
	  await navigateToManagePayee();
	  await selectPayee_ManagePayeeList(BillPay.schedulePay.payeeName);
	  await clickOnBillPayBtn_ManagePayees();
	  await enterAmount_SheduleBillPay(BillPay.schedulePay.amountValue);
	  await selectfrequency_SheduledBillPay("Yearly");
	  await SelectDateRange_SheduledBillpay();
	  await EnterNoteValue_SheduledBillPay("Sheduled BillPay-Yearly");
	  await confirmSheduledBillpay();
	  await verifySheduledBillpaySuccessMsg();
	  await verifyAccountsLandingScreen();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
	//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"ABC");
	//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","segmentBillpay[0]","flxBillPaymentManagePayeesSelected","lblPayee"],"text")).toContain("ABC");
	
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnPayBill"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtSearch"],"1.2");
	//   await kony.automation.playback.waitFor(["frmPayABill","lbxFrequency"]);
	//   kony.automation.listbox.selectItem(["frmPayABill","lbxFrequency"], "Yearly");
	//   await kony.automation.playback.waitFor(["frmPayABill","calSendOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","calEndingOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calEndingOn"], [11,29,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtNotes"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtNotes"],"Sheduled BillPay-Yearly");
	//   await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"]);
	//   kony.automation.button.click(["frmPayABill","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"]);
	//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmPayBillConfirm","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","lblSuccessMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmPayBillAcknowledgement","lblSuccessMessage"],"text")).toContain("Success!");
	
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	
	},120000);
	
	it("VerifyAddPayee_BillPayFlow", async function() {
	  
	  // Add payee and Then Delete same payee
	  var unique_RecipitentName=BillPay.addPayee.unique_RecipitentName+getRandomString(5);
	  var unique_Accnumber="0"+new Date().getTime();
	
	  await navigateToBillPay();
	  await clickOnAddPayeeLink();
	  await enterPayeeDetails_UsingPayeeinfo(unique_RecipitentName,BillPay.addPayee.address1,BillPay.addPayee.address2,BillPay.addPayee.city,BillPay.addPayee.zipcode,unique_Accnumber,"PayBills_AddPayee");
	  await clickOnNextButton_payeeDetails();
	  await clickOnConfirmButton_verifyPayee();
	  await verifyAddPayeeSuccessMsg();
	  await verifyAccountsLandingScreen();
	  
	  //Activate Payee to initilize payments
	  await navigateToManagePayee();
	  await selectPayee_ManagePayeeList(unique_RecipitentName);
	  await activateNewlyAddedpayee();
	 
	
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","flxMakeOneTimePayment"]);
	//   kony.automation.flexcontainer.click(["frmBulkPayees","flxMakeOneTimePayment"]);
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","tbxName"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","tbxName"],"A");
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","segPayeesName"]);
	//   kony.automation.flexcontainer.click(["frmMakeOneTimePayee","segPayeesName[3]","flxNewPayees"]);
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtZipCode"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtZipCode"],"500055");
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtAccountNumber"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtAccountNumber"],"1234567890");
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtAccountNumberAgain"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtAccountNumberAgain"],"1234567890");
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtmobilenumber"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtmobilenumber"],"1234567890");
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayee","btnNext"]);
	//   kony.automation.button.click(["frmMakeOneTimePayee","btnNext"]);
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayment","txtPaymentAmount"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayment","txtPaymentAmount"],"2.1");
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayment","txtNotes"]);
	//   kony.automation.textbox.enterText(["frmMakeOneTimePayment","txtNotes"],"test OneTime payment");
	
	//   await kony.automation.playback.waitFor(["frmMakeOneTimePayment","btnNext"]);
	//   kony.automation.button.click(["frmMakeOneTimePayment","btnNext"]);
	
	//   await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","flxImgCheckBox"]);
	//   kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","flxImgCheckBox"]);
	
	//   await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmOneTimePaymentConfirm","btnConfirm"]);
	
	//   await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","btnSavePayee"]);
	//   kony.automation.button.click(["frmOneTimePaymentAcknowledgement","btnSavePayee"]);
	  
	//   await kony.automation.playback.waitFor(["frmPayeeDetails","btnDetailsConfirm"]);
	//   kony.automation.button.click(["frmPayeeDetails","btnDetailsConfirm"]);
	  
	//   await kony.automation.playback.waitFor(["frmVerifyPayee","btnConfirm"]);
	//   kony.automation.button.click(["frmVerifyPayee","btnConfirm"]);
	  
	//   await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAddPayee"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmPayeeAcknowledgement","lblAddPayee"],"text")).toEqual("Add Payee");
	  
	//   await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAcknowledgementMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],"text")).toContain("has been added.");
	  
	  
	//   await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","btnViewAllPayees"]);
	//   kony.automation.button.click(["frmPayeeAcknowledgement","btnViewAllPayees"]);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	
	},120000);
	
	it("VerifyAllpayees", async function() {
	  
	  await navigateToBillPay();
	  await clickOnAllpayeesTab();
	  await verifyAllPayeesList();
	  await MoveBackToDashBoard_AllPayees();
	  await verifyAccountsLandingScreen();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnAllPayees"]);
	//   kony.automation.button.click(["frmBulkPayees","btnAllPayees"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmBulkPayees","segmentBillpay"]);
	//   kony.automation.flexcontainer.click(["frmBulkPayees","segmentBillpay[0]","flxDropdown"]);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	
	},120000);
	
	it("VerifyPayement_PayeeDetails", async function() {
	
	  await navigateToManagePayee();
	  await selectPayee_ManagePayeeList(BillPay.schedulePay.payeeName);
	  await clickOnBillPayBtn_ManagePayees();
	  await enterAmount_SheduleBillPay(BillPay.schedulePay.amountValue);
	  await selectfrequency_SheduledBillPay("Yearly");
	  await SelectDateRange_SheduledBillpay();
	  await EnterNoteValue_SheduledBillPay("Sheduled BillPay-Yearly");
	  await confirmSheduledBillpay();
	  await verifySheduledBillpaySuccessMsg();
	  await verifyAccountsLandingScreen();
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	  
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
	//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"ABC");
	//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","segmentBillpay[0]","flxBillPaymentManagePayeesSelected","lblPayee"],"text")).toContain("ABC");
	
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnPayBill"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtSearch"],"1.2");
	//   await kony.automation.playback.waitFor(["frmPayABill","lbxFrequency"]);
	//   kony.automation.listbox.selectItem(["frmPayABill","lbxFrequency"], "Yearly");
	//   await kony.automation.playback.waitFor(["frmPayABill","calSendOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","calEndingOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calEndingOn"], [11,29,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtNotes"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtNotes"],"Sheduled BillPay-Yearly");
	//   await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"]);
	//   kony.automation.button.click(["frmPayABill","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"]);
	//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmPayBillConfirm","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","lblSuccessMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmPayBillAcknowledgement","lblSuccessMessage"],"text")).toContain("Success!");
	
	//   await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	
	},120000);
	
	it("VerifySearchFunctionality", async function() {
	  
	  await navigateToManagePayee();
	  await selectPayee_ManagePayeeList(BillPay.addPayee.unique_RecipitentName);
	  await MoveBackToDashBoard_ManagePayees();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
	//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"ABC");
	//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","segmentBillpay[0]","flxBillPaymentManagePayeesSelected","lblPayee"],"text")).toContain("ABC");
	 
	 
	//   await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},120000);
	
	it("RepeatBillPayment", async function() {
	  
	  await navigateToPastBillPay();
	  await repeatPastBillPayment("Repeat a BillPay");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnHistory"]);
	//   kony.automation.button.click(["frmBulkPayees","btnHistory"]);
	//   await kony.automation.playback.wait(5000);
	  
	//   var nopayments=await kony.automation.playback.waitFor(["frmBillPayHistory","rtxNoPaymentMessage"],10000);
	 
	//   if(nopayments){
	//     kony.print("There are no History payments");
	//     //Move back to accounts
	//     await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"]);
	//     kony.automation.button.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
	//   }else{
	//     await kony.automation.playback.waitFor(["frmBillPayHistory","segmentBillpay"]);
	//   kony.automation.button.click(["frmBillPayHistory","segmentBillpay[0]","btnRepeat"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmPayABill","txtNotes"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtNotes"],"Repeat a BillPay");
	//   await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"]);
	//   kony.automation.button.click(["frmPayABill","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"]);
	//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmPayBillConfirm","btnConfirm"]);
	
	//   var warning=await kony.automation.playback.waitFor(["frmPayABill","rtxDowntimeWarning"],10000);
	//   if(warning){
	//     await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"]);
	//     kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	//     await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//     expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	//     //fail("Custom Message :: Amount Greater than Allowed Maximum Deposit");
	
	//   }else{
	//     await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//     kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//     await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//     expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	//   }
	    
	//   }
	
	},120000);
	
	it("ModifyShedulePayment", async function() {
	  
	  
	  await navigateToSheduledBillPay();
	  await EditSheduledBillPay("Updated Sheduled BillPay");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnScheduled"]);
	//   kony.automation.button.click(["frmBulkPayees","btnScheduled"]);
	//   await kony.automation.playback.wait(5000);
	  
	 
	//   var nopayments=await kony.automation.playback.waitFor(["frmBillPayScheduled","rtxNoPaymentMessage"],10000);
	//   if(nopayments){
	//     kony.print("There are no sheduled payments");
	//     //Move back to accounts
	//     await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	//     kony.automation.button.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
	//   }else{
	
	//   kony.print("There are few sheduled payments");
	//   await kony.automation.playback.waitFor(["frmBillPayScheduled","segmentBillpay"]);
	//   kony.automation.button.click(["frmBillPayScheduled","segmentBillpay[0]","btnEdit"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmPayABill","lblPayABill"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmPayABill","lblPayABill"],"text")).toEqual("Pay a Bill");
	
	
	//   //Update frequency and Notes as well
	//   await kony.automation.playback.waitFor(["frmPayABill","lbxFrequency"]);
	//   kony.automation.listbox.selectItem(["frmPayABill","lbxFrequency"], "Yearly");
	//   await kony.automation.playback.waitFor(["frmPayABill","calSendOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","calEndingOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calEndingOn"], [11,29,2021]);
	
	//   await kony.automation.playback.waitFor(["frmPayABill","txtNotes"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtNotes"],"Updated Sheduled BillPay");
	
	//   await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"]);
	//   kony.automation.button.click(["frmPayABill","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"]);
	//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxCheckBoxTnC"]);
	//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxCheckBoxTnC"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","btnConfirm"]);
	//   kony.automation.button.click(["frmPayBillConfirm","btnConfirm"]);
	
	//   var warning=await kony.automation.playback.waitFor(["frmPayABill","rtxDowntimeWarning"],10000);
	//   if(warning){
	//     await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"]);
	//     kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
	//     await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//     expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	//     //fail("Custom Message :: Amount Greater than Allowed Maximum Deposit");
	    
	//   }else{
	//     await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","lblSuccessMessage"]);
	//     expect(kony.automation.widget.getWidgetProperty(["frmPayBillAcknowledgement","lblSuccessMessage"],"text")).toContain("Success!");
	//     await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//     kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
	//     await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//     expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	//   }  
	// }
	  
	},120000);
	
	it("Cancel_BillPay", async function() {
	 
	  await navigateToManagePayee();
	  await selectPayee_ManagePayeeList(BillPay.schedulePay.payeeName);
	  await clickOnBillPayBtn_ManagePayees();
	  await enterAmount_SheduleBillPay(BillPay.schedulePay.amountValue);
	  await selectfrequency_SheduledBillPay("Yearly");
	  await SelectDateRange_SheduledBillpay();
	  await EnterNoteValue_SheduledBillPay("Sheduled BillPay-Yearly");
	  await cancelSheduledBillPay();
	  await verifyAccountsLandingScreen();
	
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"]);
	//   kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmManagePayees","txtSearch"],"ABC");
	//   await kony.automation.playback.waitFor(["frmManagePayees","btnConfirm"]);
	//   kony.automation.flexcontainer.click(["frmManagePayees","btnConfirm"]);
	//   await kony.automation.playback.wait(5000);
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","segmentBillpay[0]","flxBillPaymentManagePayeesSelected","lblPayee"],"text")).toContain("ABC");
	
	
	//   await kony.automation.playback.waitFor(["frmManagePayees","segmentBillpay"]);
	//   kony.automation.button.click(["frmManagePayees","segmentBillpay[0]","btnPayBill"]);
	//   await kony.automation.playback.wait(5000);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtSearch"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtSearch"],"1.2");
	//   await kony.automation.playback.waitFor(["frmPayABill","lbxFrequency"]);
	//   kony.automation.listbox.selectItem(["frmPayABill","lbxFrequency"], "Yearly");
	//   await kony.automation.playback.waitFor(["frmPayABill","calSendOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","calEndingOn"]);
	//   kony.automation.calendar.selectDate(["frmPayABill","calEndingOn"], [11,29,2021]);
	//   await kony.automation.playback.waitFor(["frmPayABill","txtNotes"]);
	//   kony.automation.textbox.enterText(["frmPayABill","txtNotes"],"Sheduled BillPay-Yearly");
	//   await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"]);
	//   kony.automation.button.click(["frmPayABill","btnConfirm"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"]);
	//   kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
	
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","btnCancel"]);
	//   kony.automation.button.click(["frmPayBillConfirm","btnCancel"]);
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","CancelPopup","lblPopupMessage"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmPayBillConfirm","CancelPopup","lblPopupMessage"],"text")).toEqual("Are you sure you want to cancel this transaction?");
	//   await kony.automation.playback.waitFor(["frmPayBillConfirm","CancelPopup","btnYes"]);
	//   kony.automation.button.click(["frmPayBillConfirm","CancelPopup","btnYes"]);
	
	//   await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
	//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
	
	},120000);
});