describe("PreLoginSuite", function() {
	async function PreLogin_NavigateToFAQ(){
	
	  appLog("Intiated method to Navigate to About US");
	
	  await kony.automation.playback.waitFor(["frmLogin","btnFaqs"],10000);
	  kony.automation.button.click(["frmLogin","btnFaqs"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Navigated to About US");
	}
	async function MoveBacktoLogin_FAQ(){
	
	  await kony.automation.playback.waitFor(["frmOnlineHelp","customheader","headermenu","btnLogout"],10000);
	  kony.automation.button.click(["frmOnlineHelp","customheader","headermenu","btnLogout"]);
	  appLog("Successfully Moved back to Login Screen");
	}
	
	async function PreLogin_NavigateToPrivacyPolicy(){
	
	  appLog("Intiated method to Navigate to PrivacyPolicy");
	
	  await kony.automation.playback.waitFor(["frmLogin","btnPrivacy"],10000);
	  kony.automation.button.click(["frmLogin","btnPrivacy"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Navigated to PrivacyPolicy");
	}
	async function MoveBacktoLogin_PrivacyPolicyScreen(){
	
	  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","headermenu","btnLogout"],10000);
	  kony.automation.button.click(["frmContactUsPrivacyTandC","customheader","headermenu","btnLogout"]);
	  appLog("Successfully Moved back to Login Screen");
	}
	async function PreLogin_NavigateToTermsConditions(){
	
	  appLog("Intiated method to Navigate to TC's");
	
	  await kony.automation.playback.waitFor(["frmLogin","btnTermsAndConditions"],10000);
	  kony.automation.button.click(["frmLogin","btnTermsAndConditions"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Navigated to TC's");
	}
	async function MoveBacktoLogin_TermsConditions(){
	
	  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","headermenu","btnLogout"],10000);
	  kony.automation.button.click(["frmContactUsPrivacyTandC","customheader","headermenu","btnLogout"]);
	  appLog("Successfully Moved back to Login Screen");
	}
	async function PreLogin_NavigateToContactUs(){
	
	  appLog("Intiated method to Navigate to ContactUs");
	
	  await kony.automation.playback.waitFor(["frmLogin","btnContactUs"],10000);
	  kony.automation.button.click(["frmLogin","btnContactUs"]);
	  await kony.automation.playback.wait(5000);
	  appLog("Successfully Navigated to Contact US");
	}
	async function MoveBacktoLogin_ContactUsScreen(){
	
	  await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","headermenu","btnLogout"],10000);
	  kony.automation.button.click(["frmContactUsPrivacyTandC","customheader","headermenu","btnLogout"]);
	  appLog("Successfully Moved back to Login Screen");
	}
	
	async function ClickonLanguageDropdown(){
	
	  appLog("Intiated method to click on Language change dropwdown");
	  await kony.automation.playback.waitFor(["frmLogin","flxDropdown"],10000);
	  kony.automation.flexcontainer.click(["frmLogin","flxDropdown"]);
	  appLog("Successfully clicked on Dropdown");
	
	}
	
	async function selectLanguage(language){
	
	  appLog("Intiated method to select Language from dropwdown");
	  await kony.automation.playback.waitFor(["frmLogin","segLanguagesList"],30000);
	  switch (language) {
	    case "US":
	      kony.automation.flexcontainer.click(["frmLogin","segLanguagesList[0]","flxLangList"]);
	      appLog("Successfully Selected Language  as: <b>"+language+"</b>");
	      break;
	    case "UK":
	      kony.automation.flexcontainer.click(["frmLogin","segLanguagesList[1]","flxLangList"]);
	      appLog("Successfully Selected Language  as: <b>"+language+"</b>");
	      break;
	    case "Spanish":
	      kony.automation.flexcontainer.click(["frmLogin","segLanguagesList[2]","flxLangList"]);
	      appLog("Successfully Selected Language  as: <b>"+language+"</b>");
	      break;
	    case "German":
	      kony.automation.flexcontainer.click(["frmLogin","segLanguagesList[3]","flxLangList"]);
	      appLog("Successfully Selected Language  as: <b>"+language+"</b>");
	      break;
	    case "French":
	      kony.automation.flexcontainer.click(["frmLogin","segLanguagesList[4]","flxLangList"]);
	      appLog("Successfully Selected Language  as: <b>"+language+"</b>");
	      break;
	  }
	  
	}
	
	async function ClickonYesbutton(){
	
	  appLog("Intiated method to click on YES button");
	  await kony.automation.playback.waitFor(["frmLogin","CustomChangeLanguagePopup","btnYes"]);
	  kony.automation.button.click(["frmLogin","CustomChangeLanguagePopup","btnYes"]);
	  appLog("Successfully clicked on YES button");
	  await kony.automation.playback.wait(5000);
	  // intermetient fix where after YES button not agaon selecting dropdown- Changing Focus
	  await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxUserName"]);
	  kony.automation.widget.touch(["frmLogin","loginComponent","tbxUserName"], [123,16],null,null);
	  //kony.automation.flexcontainer.click(["frmLogin","loginComponent","segUsers[0]","flxUserNames"]);
	  kony.automation.textbox.enterText(["frmLogin","loginComponent","tbxUserName"],"ChangeLanguage");
	}
	
	
	
	it("PreLogin_VerifyLanguage_French", async function() {
	  
	  await ClickonLanguageDropdown();
	  await selectLanguage("French");
	  await ClickonYesbutton();
	  
	},30000);
	
	it("PreLogin_VerifyLanguage_German", async function() {
	  
	  await ClickonLanguageDropdown();
	  await selectLanguage("German");
	  await ClickonYesbutton();
	  
	},30000);
	
	it("PreLogin_VerifyLanguage_Spanish", async function() {
	  
	  await ClickonLanguageDropdown();
	  await selectLanguage("Spanish");
	  await ClickonYesbutton();
	  
	},30000);
	
	it("PreLogin_VerifyLanguage_USEnglish", async function() {
	  
	  await ClickonLanguageDropdown();
	  await selectLanguage("US");
	  await ClickonYesbutton();
	  
	},30000);
	
	it("AboutUS-FAQ", async function() {
	
	  await PreLogin_NavigateToFAQ();
	  await MoveBacktoLogin_FAQ();
	//   await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
	//   kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
	
	//   await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","customhamburger","AboutUsflxAccountsMenu"]);
	//   kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","AboutUsflxAccountsMenu"]);
	
	//   await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","customhamburger","AboutUs4flxMyAccounts"]);
	//   kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","AboutUs4flxMyAccounts"]);
	
	//   await kony.automation.playback.waitFor(["frmOnlineHelp","help","lblHeading"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmOnlineHelp","help","lblHeading"], "text")).toEqual("Help");
	
	//   await kony.automation.playback.waitFor(["frmOnlineHelp","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmOnlineHelp","customheader","topmenu","flxaccounts"]);
	
	},30000);
	
	it("AboutUS-PrivacyPolicy", async function() {
	
	  await PreLogin_NavigateToPrivacyPolicy();
	  await MoveBacktoLogin_PrivacyPolicyScreen();
	  
	//   await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
	//   kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
	
	//   await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","customhamburger","AboutUsflxAccountsMenu"]);
	//   kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","AboutUsflxAccountsMenu"]);
	
	//   await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","customhamburger","AboutUs1flxMyAccounts"]);
	//   kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","AboutUs1flxMyAccounts"]);
	
	//   await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","lblContentHeader"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmContactUsPrivacyTandC","lblContentHeader"], "text")).toEqual("Privacy Policy");
	
	//   await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
	
	},30000);
	
	it("PreLogin_ContactUs", async function() {
	  
	  await PreLogin_NavigateToContactUs();
	  await MoveBacktoLogin_ContactUsScreen();
	  
	// 	await kony.automation.playback.waitFor(["frmLogin","btnContactUs"]);
	// 	kony.automation.button.click(["frmLogin","btnContactUs"]);
	//     await kony.automation.playback.wait(5000);
	// 	await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","headermenu","btnLogout"]);
	// 	kony.automation.button.click(["frmContactUsPrivacyTandC","customheader","headermenu","btnLogout"]);
	  
	},30000);
	
	it("AboutUS-TermsConditions", async function() {
	  
	  await PreLogin_NavigateToTermsConditions();
	  await MoveBacktoLogin_TermsConditions();
	//   await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
	//   kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
	
	//   await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","customhamburger","AboutUsflxAccountsMenu"]);
	//   kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","AboutUsflxAccountsMenu"]);
	
	//   await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","customhamburger","AboutUs0flxMyAccounts"]);
	//   kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","AboutUs0flxMyAccounts"]);
	
	//   await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","lblContentHeader"]);
	//   expect(kony.automation.widget.getWidgetProperty(["frmContactUsPrivacyTandC","lblContentHeader"], "text")).toEqual("Terms & Conditions");
	
	//   await kony.automation.playback.waitFor(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
	//   kony.automation.flexcontainer.click(["frmContactUsPrivacyTandC","customheader","topmenu","flxaccounts"]);
	  
	},30000);
});