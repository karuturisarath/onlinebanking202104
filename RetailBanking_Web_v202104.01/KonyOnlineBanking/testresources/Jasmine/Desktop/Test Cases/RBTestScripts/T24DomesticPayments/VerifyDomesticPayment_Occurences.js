it("VerifyDomesticPayment_Occurences", async function() {
  
  await navigateToMakePayments();
  await SelectFromAccount(Payments.Domestic.FromAcc);
  await SelectToAccount(Payments.Domestic.ToAcc);
  await EnterAmount(Payments.Domestic.Amount);
  await SelectFrequency("Monthly");
  await SelectDateRange();
  await EnterNoteValue("VerifyDomesticPayment_Occurences");
  await ConfirmTransfer();
  await VerifyTransferSuccessMessage();
  
},TimeOuts.DomesticPayments.Payment);