it("VerifyInstantDomesticPayment_FutureDate", async function() {
  
  await navigateToMakePayments();
  await SelectFromAccount(Payments.Domestic.FromAcc);
  await SelectToAccount(Payments.Domestic.ToAcc);
  await EnterAmount(Payments.Domestic.Amount);
  await SelectSendOnDate();
  await EnterNoteValue("VerifyInstantDomesticPayment_FutureDate");
  await ConfirmTransfer();
  await VerifyTransferSuccessMessage();
  
},TimeOuts.DomesticPayments.Payment);