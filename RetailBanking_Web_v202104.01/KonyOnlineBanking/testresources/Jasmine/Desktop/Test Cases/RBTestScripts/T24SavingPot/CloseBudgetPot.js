it("CloseBudgetPot", async function() {
  
  await clickOnFirstAvailableAccount();
  await navigateToSavingPot();
  await closeMyBudget();
  
},120000);