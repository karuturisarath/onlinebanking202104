it("CancelInternationalRecurringPayment", async function() {
  
  await navigateToManageTranscations();
  await ClickonRecurringTab();
  await selectActiveOrders();
  await clickOnCancelSeriesButton(ManageBeneficiary.International.BeneficiaryName);
  
},TimeOuts.TrasferActivities.PaymentActivity);