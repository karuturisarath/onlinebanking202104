it("RepeatInternationalPayment", async function() {
  
  await navigateToManageTranscations();
  await selectCompletedTransfers();
  await clickOnRepeatButton(ManageBeneficiary.International.BeneficiaryName);

},TimeOuts.TrasferActivities.PaymentActivity);