it("VerifySameBankPayment_FutureDate", async function() {
  
  await navigateToMakePayments();
  await SelectFromAccount(Payments.SameBank.FromAcc);
  await SelectToAccount(Payments.SameBank.ToAcc);
  await EnterAmount(Payments.SameBank.Amount);
  await SelectSendOnDate();
  await EnterNoteValue("VerifySameBankPayment_FutureDate");
  await ConfirmTransfer();
  await VerifyTransferSuccessMessage();
  
},TimeOuts.SameBankPayments.Payment);