it("EditP2PRecipitent", async function() {

   // Add a recipitent and Then delete the same recipitent
  
 var unique_RecipitentName=ManageRecipients.p2pAccount.unique_RecipitentName+getRandomString(5);
 var email=ManageRecipients.p2pAccount.email;
  
  var unique_EditRecipitentName=ManageRecipients.p2pAccount.unique_EditRecipitentName+getRandomString(5);
  var unique_EditNickName=ManageRecipients.p2pAccount.unique_EditNickName+getRandomString(5);
  
  await NavigateToManageRecipitents();
  await clickonAddP2PAccounttab();
  await enterP2PAccountDetails_Email(unique_RecipitentName,email);
  await verifyAddingNewReciptientSuccessMsg();
  await verifyAccountsLandingScreen();
  
   //Edit Added Recipitent
  await NavigateToManageRecipitents();
  await clickOnP2PRecipitentsTab();
  await SearchforPayee_External(unique_RecipitentName);
  await EditP2PReciptent(unique_EditRecipitentName,unique_EditNickName);
  await verifyAddingNewReciptientSuccessMsg();
  await verifyAccountsLandingScreen();
  
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");

//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnRecipients"]);
//   kony.automation.button.click(["frmFastManagePayee","btnRecipients"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","flxAddReciepient"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","flxAddReciepient"]);

//   await kony.automation.playback.waitFor(["frmFastAddRecipient","tbxRecipientNameKA"]);
//   kony.automation.textbox.enterText(["frmFastAddRecipient","tbxRecipientNameKA"],"TestP2PRecipitent");

//   await kony.automation.playback.waitFor(["frmFastAddRecipient","tbxAccountNickNameKA"]);
//   kony.automation.textbox.enterText(["frmFastAddRecipient","tbxAccountNickNameKA"],"P2P");

//   await kony.automation.playback.waitFor(["frmFastAddRecipient","flxNUORadioBtn1"]);
//   kony.automation.flexcontainer.click(["frmFastAddRecipient","flxNUORadioBtn1"]);

//   await kony.automation.playback.waitFor(["frmFastAddRecipient","tbxPhoneNumberKA"]);
//   kony.automation.textbox.enterText(["frmFastAddRecipient","tbxPhoneNumberKA"],"1234567890");

//   await kony.automation.playback.waitFor(["frmFastAddRecipient","btnAddAccountKA"]);
//   kony.automation.button.click(["frmFastAddRecipient","btnAddAccountKA"]);

//   await kony.automation.playback.waitFor(["frmFastAddRecipientConfirm","btnConfirm"]);
//   kony.automation.button.click(["frmFastAddRecipientConfirm","btnConfirm"]);
//   await kony.automation.playback.waitFor(["frmFastAddRecipientAcknowledgement","lblSuccessMessage"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddRecipientAcknowledgement","lblSuccessMessage"],"text")).toContain("has been added.");

//   await kony.automation.playback.waitFor(["frmFastAddRecipientAcknowledgement","customheadernew","flxAccounts"]);
//   kony.automation.flexcontainer.click(["frmFastAddRecipientAcknowledgement","customheadernew","flxAccounts"]);
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

//   // Edit the added Recipitent
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnRecipients"]);
//   kony.automation.button.click(["frmFastManagePayee","btnRecipients"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search1","txtSearch"]);
//   kony.automation.textbox.enterText(["frmFastManagePayee","Search1","txtSearch"],"TestP2PRecipitent");

//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search1","btnConfirm"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","Search1","btnConfirm"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
//   kony.automation.button.click(["frmFastManagePayee","segmentTransfers[0]","btnEdit"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastAddRecipient","lblTransfers"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddRecipient","lblTransfers"],"text")).toContain("Edit");
//   await kony.automation.playback.waitFor(["frmFastAddRecipient","flxNUORadioBtn1"]);
//   kony.automation.flexcontainer.click(["frmFastAddRecipient","flxNUORadioBtn1"]);
//   await kony.automation.playback.waitFor(["frmFastAddRecipient","tbxPhoneNumberKA"]);
//   kony.automation.textbox.enterText(["frmFastAddRecipient","tbxPhoneNumberKA"],"1234567890");

//   //Having issue in Save button
//   await kony.automation.playback.waitFor(["frmFastAddRecipient","btnAddAccountKA"]);
//   kony.automation.button.click(["frmFastAddRecipient","btnAddAccountKA"]);

//   var successMsg=await kony.automation.playback.waitFor(["frmFastAddRecipientAcknowledgement","lblSuccessMessage"],10000);

//   if(!successMsg){
//     // Move back to base state
//     await kony.automation.playback.waitFor(["frmFastAddRecipient","customheadernew","flxAccounts"]);
//     kony.automation.flexcontainer.click(["frmFastAddRecipient","customheadernew","flxAccounts"]);
//   }else{
//     await kony.automation.playback.waitFor(["frmFastAddRecipientAcknowledgement","lblSuccessMessage"]);
//     expect(kony.automation.widget.getWidgetProperty(["frmFastAddRecipientAcknowledgement","lblSuccessMessage"],"text")).toContain("has been successfully edited");
//     await kony.automation.playback.waitFor(["frmFastAddRecipientAcknowledgement","customheadernew","flxAccounts"]);
//     kony.automation.flexcontainer.click(["frmFastAddRecipientAcknowledgement","customheadernew","flxAccounts"]);
//     await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//     expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

//   }
  
//   // Delete the recipitent to clean list

//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnExternalAccounts"]);
//   kony.automation.button.click(["frmFastManagePayee","btnExternalAccounts"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","txtSearch"]);
//   kony.automation.textbox.enterText(["frmFastManagePayee","Search","txtSearch"],unique_RecipitentName);
//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","btnConfirm"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","Search","btnConfirm"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
//   kony.automation.button.click(["frmFastManagePayee","segmentTransfers[0]","btnRemoveRecipient"]);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup","lblPopupMessage"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","CustomPopup","lblPopupMessage"],"text")).toEqual("Are you sure you want to delete this account?");

//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup","btnYes"]);
//   kony.automation.button.click(["frmFastManagePayee","CustomPopup","btnYes"]);
//   await kony.automation.playback.wait(5000);
  
//   var error= await kony.automation.playback.waitFor(["frmFastManagePayee","rtxMakeTransferErro"],5000);
  
//   if(error){
//     fail("There was a technical delay. Please try again.");
//   }

//   await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");


},120000);