it("VerifyNewStatusColumn_RecurringTab", async function() {
  
  await navigateToManageTranscations();
  await ClickonRecurringTab();
  await VerifyNewStatusColumn_StandardOrders();
  await MoveBackFrom_SheduledTransferActivities();
  
},TimeOuts.TrasferActivities.ActivitiesList);