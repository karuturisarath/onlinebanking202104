it("ViewUncategorizedTranscation", async function() {

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","ACCOUNTS6flxMyAccounts"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","ACCOUNTS6flxMyAccounts"]);
  await kony.automation.playback.wait(10000);
  await kony.automation.playback.waitFor(["frmPersonalFinanceManagement","CommonHeader","lblHeading"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmPersonalFinanceManagement","CommonHeader","lblHeading"], "text")).not.toBe("");
  await kony.automation.playback.waitFor(["frmPersonalFinanceManagement","CommonHeader","btnRequest"],15000);
  kony.automation.button.click(["frmPersonalFinanceManagement","CommonHeader","btnRequest"]);
  await kony.automation.playback.wait(10000);
  //await kony.automation.playback.waitFor(["frmPersonalFinanceManagement","CommonHeaderUncategorized","lblHeading"],15000);
  //expect(kony.automation.widget.getWidgetProperty(["frmPersonalFinanceManagement","CommonHeaderUncategorized","lblHeading"], "text")).toContain("Transactions");
  //await kony.automation.playback.waitFor(["frmPersonalFinanceManagement","TransactionsUnCategorized","segTransactions"],30000);
  //kony.automation.flexcontainer.click(["frmPersonalFinanceManagement","TransactionsUnCategorized","segTransactions[0]","flxDropdown"]);
  await kony.automation.playback.waitFor(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"],15000);
  kony.automation.flexcontainer.click(["frmPersonalFinanceManagement","customheader","topmenu","flxaccounts"]);

},120000);