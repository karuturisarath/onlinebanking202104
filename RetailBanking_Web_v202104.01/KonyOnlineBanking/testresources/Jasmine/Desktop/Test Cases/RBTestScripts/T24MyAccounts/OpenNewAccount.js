it("OpenNewAccount", async function() {
  
  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","ACCOUNTS2flxMyAccounts"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","ACCOUNTS2flxMyAccounts"]);
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmNAO","segProductsNAO"],15000);
  kony.automation.flexcontainer.click(["frmNAO","segProductsNAO[0]","flxCheckBox"]);
  await kony.automation.playback.waitFor(["frmNAO","flxTCContentsCheckbox"],15000);
  await kony.automation.scrollToWidget(["frmNAO","flxTCContentsCheckbox"]);
  kony.automation.flexcontainer.click(["frmNAO","flxTCContentsCheckbox"]);
  await kony.automation.playback.waitFor(["frmNAO","btnContinueProceed"],15000);
  kony.automation.button.click(["frmNAO","btnContinueProceed"]);
  await kony.automation.playback.waitFor(["frmNAO","ReviewApplicationNAO","btnReviewApplicationSubmit"],15000);
  kony.automation.button.click(["frmNAO","ReviewApplicationNAO","btnReviewApplicationSubmit"]);
  await kony.automation.playback.waitFor(["frmNAO","SubmitApplication","btnYes"],15000);
  kony.automation.button.click(["frmNAO","SubmitApplication","btnYes"]);
  var error=await kony.automation.playback.waitFor(["frmNAO","rtxDowntimeWarning"],15000);
  if(error){
    fail("Unable to Open new Account");
  }
  await kony.automation.playback.waitFor(["frmNAO","customheader","topmenu","flxaccounts"]);
  kony.automation.flexcontainer.click(["frmNAO","customheader","topmenu","flxaccounts"]);
  
},120000);