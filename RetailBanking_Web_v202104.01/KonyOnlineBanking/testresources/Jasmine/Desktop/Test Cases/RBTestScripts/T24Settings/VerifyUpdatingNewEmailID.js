it("VerifyUpdatingNewEmailID", async function() {
  
  var updatedemailid=Settings.email.updatedemailid+getRandomString(3)+"@infinity.com";
  
  await NavigateToProfileSettings();
  await selectProfileSettings_EmailAddress();
  await ProfileSettings_UpdateEmailAddress(updatedemailid);
  await MoveBackToDashBoard_ProfileManagement();
  await verifyAccountsLandingScreen();
  
},120000);