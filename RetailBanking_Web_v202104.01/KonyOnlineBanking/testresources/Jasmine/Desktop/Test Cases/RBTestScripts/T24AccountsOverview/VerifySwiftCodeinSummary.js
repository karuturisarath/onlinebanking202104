it("VerifySwiftCodeinSummary", async function() {
  
  await clickOnFirstCheckingAccount();
  await VerifySwiftCode_accountSummary();
  await MoveBackToLandingScreen_AccDetails();
  
},60000);