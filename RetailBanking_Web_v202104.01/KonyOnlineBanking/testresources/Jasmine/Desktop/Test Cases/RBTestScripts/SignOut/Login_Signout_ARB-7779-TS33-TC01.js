it("Login_Signout_ARB-7779-TS33-TC01", async function() {

  await kony.automation.playback.waitFor(["frmDashboard","customheader","headermenu","btnLogout"]);
  kony.automation.button.click(["frmDashboard","customheader","headermenu","btnLogout"]);
  await kony.automation.playback.waitFor(["frmDashboard","CustomPopup","btnYes"]);
  kony.automation.button.click(["frmDashboard","CustomPopup","btnYes"]);
  await kony.automation.playback.wait(30000);

},60000);