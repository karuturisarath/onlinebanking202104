it("QuarterlySheduledTransfer-DateRange", async function() {
  
  await navigateToUnifiedTransfers();
  await ClickonMakeTransfrBtn("SameBank");
  await SelectUTFFromAccount(UTFPayments.SameBank.FromAcc);
  await SelectUTFToAccount(UTFPayments.SameBank.ToAcc);
  await Enter_CCY_AmountValue(UTFPayments.SameBank.Amount);
  await SelectUTFFrequency("Qtrly");
  await SelectUTFDateRange();
  await EnterUTFNoteValue("SameBank-QtrlySheduledTransfer-DateRange");
  await clickonUTFConfirmBtn();
  await VerifyUTFTransferSuccessMsg();
  
  },TimeOuts.UnifiedTransfers.Transfers);