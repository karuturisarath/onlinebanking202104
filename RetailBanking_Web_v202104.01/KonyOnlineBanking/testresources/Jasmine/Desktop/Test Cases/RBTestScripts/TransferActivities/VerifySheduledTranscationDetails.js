it("VerifySheduledTranscationDetails", async function() {

  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxPayBills"]);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxPayBills"]);
  await kony.automation.playback.wait(10000);
  await kony.automation.playback.waitFor(["frmFastTransfersActivites","lblTransfers"]);
  expect(kony.automation.widget.getWidgetProperty(["frmFastTransfersActivites","lblTransfers"],"text")).toEqual("Transfer Activities");

  await kony.automation.playback.waitFor(["frmFastTransfersActivites","btnMakeTransfer"]);
  kony.automation.button.click(["frmFastTransfersActivites","btnMakeTransfer"]);
  await kony.automation.playback.wait(5000);
  
  var noTransfers=await kony.automation.playback.waitFor(["frmFastTransfersActivites","rtxNoPaymentMessage"],10000);

if(noTransfers){
  await kony.automation.playback.waitFor(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
  kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
}else{
  await kony.automation.playback.waitFor(["frmFastTransfersActivites","segmentTransfers"]);
  kony.automation.flexcontainer.click(["frmFastTransfersActivites","segmentTransfers[0]","flxDropdown"]);

  await kony.automation.playback.waitFor(["frmFastTransfersActivites","segmentTransfers[0]","flxFastPastTransfersSelected","lblReferenceNumberValue"]);
  expect(kony.automation.widget.getWidgetProperty(["frmFastTransfersActivites","segmentTransfers[0]","flxFastPastTransfersSelected","lblReferenceNumberValue"],"text")).not.toBe('');

  await kony.automation.playback.waitFor(["frmFastTransfersActivites","segmentTransfers[0]","flxFastPastTransfersSelected","lblFromAccountValue"]);
  expect(kony.automation.widget.getWidgetProperty(["frmFastTransfersActivites","segmentTransfers[0]","flxFastPastTransfersSelected","lblFromAccountValue"],"text")).not.toBe('');

  await kony.automation.playback.waitFor(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
  kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
}

  

},60000);