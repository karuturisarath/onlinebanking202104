it("AddDomesticBenefeciary_SameName", async function() {
  
  var IBAN=ManageBeneficiary.Domestic.IBANList[1].IBAN;
  var BeneficiaryName=ManageBeneficiary.Domestic.BeneficiaryName;
  var Nickname=ManageBeneficiary.Domestic.Nickname;
  var Address1=ManageBeneficiary.Domestic.Address1;
  var Address2=ManageBeneficiary.Domestic.Address2;
  var city=ManageBeneficiary.Domestic.city;
  var zipcode=ManageBeneficiary.Domestic.zipcode;
             
  await NavigateToManageBeneficiary();
  if(await isBenefeciaryAlreadyAdded(IBAN)){
    await MoveBackFrom_ManageBeneficiaries();
  }else{
  await clickonAddNewBeneficiaryBtn();
  await enterDomesticBeneficiaryDetails(IBAN,BeneficiaryName,Nickname,Address1,Address2,city,zipcode);
  await SubmitBankBeneficiaryDetails();
  await VerifyAddBeneficiarySuccessMsg();
  }
  
},TimeOuts.ManageBenefeciary.AddBenefeciary);