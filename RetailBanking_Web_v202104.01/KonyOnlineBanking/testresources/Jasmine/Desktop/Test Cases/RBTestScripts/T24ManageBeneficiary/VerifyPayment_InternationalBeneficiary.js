it("VerifyPayment_InternationalBeneficiary", async function() {
  
  await NavigateToManageBeneficiary();
  await ClickonMakePaymentLink_BeneficiariesList(Payments.International.ToAcc);
  await SelectFromAccount(Payments.International.FromAcc);
  await EnterAmount(Payments.International.Amount);
  await EnterNoteValue("VerifyPayment_InternationalBeneficiary");
  await ConfirmTransfer();
  await VerifyTransferSuccessMessage();
  
},TimeOuts.ManageBenefeciary.PaymentBenefeciary);