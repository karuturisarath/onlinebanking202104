it("FeebBackFunctionality", async function() {

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"]);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"]);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUsflxAccountsMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","AboutUs5flxMyAccounts"]);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","AboutUs5flxMyAccounts"]);
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
  
  await kony.automation.playback.waitFor(["frmCustomerFeedback","Feedback","lblHeading"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmCustomerFeedback","Feedback","lblHeading"], "text")).toEqual("FeedBack");
  await kony.automation.playback.waitFor(["frmCustomerFeedback","Feedback","flxRating5"],15000);
  kony.automation.flexcontainer.click(["frmCustomerFeedback","Feedback","flxRating5"]);
  await kony.automation.playback.waitFor(["frmCustomerFeedback","Feedback","txtareaUserComments"],15000);
  kony.automation.textarea.enterText(["frmCustomerFeedback","Feedback","txtareaUserComments"],"Test automation FeedBack");
  await kony.automation.playback.waitFor(["frmCustomerFeedback","Feedback","confirmButtons","btnConfirm"],15000);
  kony.automation.button.click(["frmCustomerFeedback","Feedback","confirmButtons","btnConfirm"]);
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
  
  await kony.automation.playback.waitFor(["frmCustomerFeedback","acknowledgment","lblTransactionMessage"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmCustomerFeedback","acknowledgment","lblTransactionMessage"],"text")).toContain("Thank you");
  await kony.automation.playback.waitFor(["frmCustomerFeedback","btnAddAnotherAccount"],15000);
  kony.automation.button.click(["frmCustomerFeedback","btnAddAnotherAccount"]);
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);

  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

},60000);