it("AddRecipient_InternationalAccount_TS04-TC01", async function() {

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"]);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransferflxAccountsMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","WireTransfer3flxMyAccounts"]);
  await kony.automation.scrollToWidget(["frmDashboard","customheader","customhamburger","WireTransfer3flxMyAccounts"]);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","WireTransfer3flxMyAccounts"]);

  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1"]);
  await kony.automation.playback.waitFor(["frmWireTransferAddKonyAccountStep1","btnInternationalAccount"]);
  kony.automation.button.click(["frmWireTransferAddKonyAccountStep1","btnInternationalAccount"]);
  await kony.automation.playback.wait(10000);

  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","lblStep1"]);
  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddInternationalAccountStep1","lblStep1"], "text")).toEqual("Step 1: Recipient Details");


  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","lbxCountry"]);
  kony.automation.listbox.selectItem(["frmWireTransferAddInternationalAccountStep1","lbxCountry"], "Australia");
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","tbxRecipientName"]);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep1","tbxRecipientName"],"Inter_Auto");
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","tbxAddressLine1"]);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep1","tbxAddressLine1"],"Add1");
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","tbxAddressLine2"]);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep1","tbxAddressLine2"],"Add2");
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","tbxCity"]);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep1","tbxCity"],"Sydney");
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","tbxZipcode"]);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep1","tbxZipcode"],"500055");
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep1","btnProceed"]);
  kony.automation.button.click(["frmWireTransferAddInternationalAccountStep1","btnProceed"]);


 
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2"]);
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","lblStep2"]);
  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddInternationalAccountStep2","lblStep2"], "text")).toEqual("Step 2 - Recipient Account Details");

  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxSwiftCode"]);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxSwiftCode"],"BOFAUS3NXXX");
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxIBANOrIRC"]);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxIBANOrIRC"],"1234567890");
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxAccountNumber"]);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxAccountNumber"],"1234567890");
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxReAccountNumber"]);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxReAccountNumber"],"1234567890");
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxNickName"]);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxNickName"],"Auto_Inter");
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxBankName"]);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxBankName"],"BOA");
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxBankAddressLine1"]);
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxBankAddressLine1"]);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxBankAddressLine1"],"ADD1");
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxBankAddressLine2"]);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxBankAddressLine2"],"ADD2");
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxBankCity"]);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxBankCity"],"SYDNEY");
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","tbxBankZipcode"]);
  kony.automation.textbox.enterText(["frmWireTransferAddInternationalAccountStep2","tbxBankZipcode"],"500055");
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountStep2","btnAddRecipent"]);
  kony.automation.button.click(["frmWireTransferAddInternationalAccountStep2","btnAddRecipent"]);

  
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountConfirm"]);
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountConfirm","lblAddAccountHeading"]);
  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddInternationalAccountConfirm","lblAddAccountHeading"], "text")).toEqual("Add Recipient - Confirmation");
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountConfirm","lblRecipientDetails"]);
  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddInternationalAccountConfirm","lblRecipientDetails"], "text")).toEqual("Recipient Details");
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountConfirm","lblBankDetails"]);
  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddInternationalAccountConfirm","lblBankDetails"], "text")).toEqual("Bank Details");
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountConfirm","btnConfirm"]);
  kony.automation.button.click(["frmWireTransferAddInternationalAccountConfirm","btnConfirm"]);

  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountAck"]);
  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddInternationalAccountAck","lblAddAccountHeading"], "text")).toEqual("Add Recipient - Acknowledgment");
  expect(kony.automation.widget.getWidgetProperty(["frmWireTransferAddInternationalAccountAck","lblSuccessAcknowledgement"], "text")).toContain("added succesfully!");


  // Move back to base state
  await kony.automation.playback.waitFor(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"]);
  kony.automation.flexcontainer.click(["frmWireTransferAddInternationalAccountAck","customheadernew","flxAccounts"]);

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"]);
  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

},90000);