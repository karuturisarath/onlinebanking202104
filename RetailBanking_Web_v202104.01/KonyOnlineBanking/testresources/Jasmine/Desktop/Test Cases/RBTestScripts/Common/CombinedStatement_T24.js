async function navigateToCombinedStatements(){

  await navigateToAccontStatements();
  
  await kony.automation.playback.waitFor(["frmAccountsDetails","viewStatementsnew","btnCombinedStatements"],15000);
  kony.automation.button.click(["frmAccountsDetails","viewStatementsnew","btnCombinedStatements"]);
  await kony.automation.playback.wait(5000);

  appLog("Successfully Navigated to CombinedStatement Screen");
}

async function navigateToAccontStatements(){

  appLog("Intiated method to Navigate CombinedStatement Screen");
  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","ACCOUNTS7flxMyAccounts"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","ACCOUNTS7flxMyAccounts"]);
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmAccountsDetails","viewStatementsnew","lblViewStatements"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","viewStatementsnew","lblViewStatements"], "text")).not.toBe(""); 
}

async function verifyDownLoadOption(){

  appLog("Intiated method to verify Download option");
  await kony.automation.playback.waitFor(["frmAccountsDetails","viewStatementsnew","lblDownload"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmAccountsDetails","viewStatementsnew","lblDownload"], "text")).not.toBe("");

}

async function clickOnGenarateNewStatement(){

  appLog("Intiated method to click on Genarate new Statement");

  await kony.automation.playback.waitFor(["frmAccountsDetails","viewStatementsnew","btnCombinedStatements"],15000);
  kony.automation.button.click(["frmAccountsDetails","viewStatementsnew","btnConfirm"]);
  appLog("Successfully clicked on to Genarate new Statement button");

}

async function ValidateDefaultFilterValue(){

  appLog("Intiated method to validate default filter value");
  await kony.automation.playback.waitFor(["frmConsolidatedStatements","lblSelectedFilter"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmConsolidatedStatements","lblSelectedFilter"], "text")).toContain("All Accounts");
}

async function selectPreferedAccountsFilter(){

  await kony.automation.playback.waitFor(["frmConsolidatedStatements","flxFilterListBox"],15000);
  kony.automation.flexcontainer.click(["frmConsolidatedStatements","flxFilterListBox"]);
}


async function selectFromDate(){

  appLog("Intiated method to Select Statement From Date");
  await kony.automation.playback.waitFor(["frmConsolidatedStatements","calFromDate"],15000);
  kony.automation.calendar.selectDate(["frmConsolidatedStatements","calFromDate"], [4,17,2021]);
  //var today = new Date();
  //kony.automation.calendar.selectDate(["frmConsolidatedStatements","calFromDate"], [(today.getDate()-2),(today.getMonth()+1),today.getFullYear()]);
  appLog("Successfully selected From date for statement");
}

async function selectAccountforStatement(){

  appLog("Intiated method to Select From acc for Statement");
  await kony.automation.playback.waitFor(["frmConsolidatedStatements","segAccounts"],15000);
  kony.automation.flexcontainer.click(["frmConsolidatedStatements","segAccounts[0,-1]","flxCheckBox"]);
}

async function clickOnCombinedStatementContinueButton(){

  appLog("Intiated method to Click on ContinueButton");
  await kony.automation.playback.waitFor(["frmConsolidatedStatements","btnDownloadStatements"],15000);
  await kony.automation.scrollToWidget(["frmConsolidatedStatements","btnDownloadStatements"]);
  kony.automation.button.click(["frmConsolidatedStatements","btnDownloadStatements"]);
  appLog("Successfully Clicked on ContinueButton");
}

async function selectFormatAsPDF(){

  appLog("Intiated method to select PDF format");
  await kony.automation.playback.waitFor(["frmConsolidatedStatements","flxFilterListBoxPopup"],15000);
  kony.automation.flexcontainer.click(["frmConsolidatedStatements","flxFilterListBoxPopup"]);
  kony.automation.flexcontainer.click(["frmConsolidatedStatements","segFileFilters[0]","flxFileFilter"]);

}

async function selectFormatAsExcel(){

  appLog("Intiated method to select Excel format");
  await kony.automation.playback.waitFor(["frmConsolidatedStatements","flxFilterListBoxPopup"],15000);
  kony.automation.flexcontainer.click(["frmConsolidatedStatements","flxFilterListBoxPopup"]);
  kony.automation.flexcontainer.click(["frmConsolidatedStatements","segFileFilters[1]","flxFileFilter"]);
}

async function selectFormatAsCSV(){

  appLog("Intiated method to select CSV format");
  await kony.automation.playback.waitFor(["frmConsolidatedStatements","flxFilterListBoxPopup"],15000);
  kony.automation.flexcontainer.click(["frmConsolidatedStatements","flxFilterListBoxPopup"]);
  kony.automation.flexcontainer.click(["frmConsolidatedStatements","segFileFilters[2]","flxFileFilter"]);
}

async function clickOnCreateStatementButton(){

  appLog("Intiated method to Click on Create StatementButton");
  await kony.automation.playback.waitFor(["frmConsolidatedStatements","btnDownload"],15000);
  kony.automation.button.click(["frmConsolidatedStatements","btnDownload"]);
  await kony.automation.playback.wait(5000);
}

async function verifyStatementCreationPopupMsg(){

  appLog("Intiated method to verify Successfull Statement Creation");
  var Status=await kony.automation.playback.waitFor(["frmConsolidatedStatements","lblPreparingStatementmessage"],15000);
  if(Status){
    kony.automation.button.click(["frmConsolidatedStatements","btnOkay"]);
    appLog("Successfully clicked on Statement success message");
  }else{
    appLog("Custom Message : Failed to genarate Combined Statement");
    fail("Custom Message : Failed to genarate Combined Statement");
  }

}

async function MoveBackFrom_CombinedStatements(){

  appLog("Intiated method to move back from ConsolidatedStatements");
  await kony.automation.playback.waitFor(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"],15000);
  kony.automation.flexcontainer.click(["frmConsolidatedStatements","customheader","topmenu","flxaccounts"]);
  appLog("Successfully moved back to account dashboard");
}

