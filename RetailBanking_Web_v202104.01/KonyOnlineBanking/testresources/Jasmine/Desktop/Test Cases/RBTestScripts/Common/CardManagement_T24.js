async function navigateToCardManegement(){

  appLog("Intiated method to Navigate Card Management Screen");

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","ACCOUNTS4flxMyAccounts"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","ACCOUNTS4flxMyAccounts"]);
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmCardManagement","myCards","lblMyCardsHeader"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","myCards","lblMyCardsHeader"], "text")).toEqual("My Cards");

  appLog("Successfully Navigated to Card Management Screen");
}
async function VerifyNoCardsError(){

  noCardsError=false;
  appLog("Intiated method to verify Cards available or Not");
  var noCardsError=await kony.automation.playback.waitFor(["frmCardManagement","myCards","lblNoCardsError"],30000);
  //var noCardsError=kony.automation.widget.getWidgetProperty(["frmCardManagement","myCards","lblNoCardsError"], "text");
  appLog("no Cards Error :: "+noCardsError);
  return noCardsError;
}

async function applyforNewCard(){

  appLog("Intiated method to apply for new Card");

  if(await VerifyNoCardsError()){
    appLog("No cards avalable - applyNew");
    await kony.automation.playback.waitFor(["frmCardManagement","myCards","btnApplyForCard"],15000);
    kony.automation.button.click(["frmCardManagement","myCards","btnApplyForCard"]);
    appLog("Successfully clicked on Apply Card button");
  }else{
    appLog("Cards are available already- Request new Card");
    await kony.automation.playback.waitFor(["frmCardManagement","flxRequestANewCard"],15000);
    kony.automation.flexcontainer.click(["frmCardManagement","flxRequestANewCard"]);
    appLog("Successfully clicked on Request new Card button");
  }

}

async function addNewCardDetails(){

  appLog("Intiated method to add new card details");

  await kony.automation.playback.waitFor(["frmCardManagement","segAccounts"],15000);
  kony.automation.flexcontainer.click(["frmCardManagement","segAccounts[0]","flxAccountNameWrapper"]);
  appLog("Successfully clicked on Flex accounts");
  await kony.automation.playback.waitFor(["frmCardManagement","segCardProducts"],15000);
  kony.automation.button.click(["frmCardManagement","segCardProducts[0]","btnSelect"]);
  appLog("Successfully clicked on btnSelect");
  await kony.automation.playback.waitFor(["frmCardManagement","tbxNameOnCard"],15000);
  kony.automation.textbox.enterText(["frmCardManagement","tbxNameOnCard"],"JasmineCard"+getRandomString(5));
  appLog("Successfully entered card name ");
  await kony.automation.playback.waitFor(["frmCardManagement","tbxEnterCardPIN"],15000);
  kony.automation.textbox.enterText(["frmCardManagement","tbxEnterCardPIN"],"5050");
  appLog("Successfully entered card PIN ");
  await kony.automation.playback.waitFor(["frmCardManagement","tbxConfirmCardPIN"],15000);
  kony.automation.textbox.enterText(["frmCardManagement","tbxConfirmCardPIN"],"5050");
  appLog("Successfully Re-entered card PIN ");
  await kony.automation.playback.waitFor(["frmCardManagement","btnNewCardContinue"],15000);
  kony.automation.button.click(["frmCardManagement","btnNewCardContinue"]);
  appLog("Successfully clicked on CONTINUE button");

  var Success=await kony.automation.playback.waitFor(["frmCardManagement","Acknowledgement","lblCardTransactionMessage"],30000);
  expect(Success).toEqual(true,"Failed to add new card details");

  await MoveBackfrom_MyCards();
}
async function activateNewCard(){

  appLog("Intiated method to activate new card");
  var MyValues=await ClickOnCardOptions("Activate Card");
  if(MyValues[0]===true){
    await kony.automation.playback.waitFor(["frmCardManagement","tbxCVVNumber"],15000);
    //appLog("Before converting is : "+MyValues[1]);
    var myCVV=""+MyValues[1].toString();
    appLog("Intiated method to enter CVV as : "+myCVV);
    kony.automation.textbox.enterText(["frmCardManagement","tbxCVVNumber"],myCVV);
    appLog("Successfully entered CVV as : "+myCVV);
    await kony.automation.playback.waitFor(["frmCardManagement","btnContinue2"],15000);
    kony.automation.button.click(["frmCardManagement","btnContinue2"]);
    var Success=await kony.automation.playback.waitFor(["frmCardManagement","Acknowledgement","lblCardTransactionMessage"],30000);
    //expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","Acknowledgement","lblCardTransactionMessage"], "text")).toEqual("activated");
    expect(Success).toEqual(true,"Failed to Activate new card");

    await MoveBackfrom_MyCards();
  }else{
    appLog("all cards are activated alreay");
    await MoveBackfrom_MyCards();
  }
}
async function VerifyMaskedAccountNumber(){

  appLog("Intiated method to verify Masked account number");
  await kony.automation.playback.waitFor(["frmCardManagement","myCards","segMyCards"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","myCards","segMyCards[0]","rtxValue1"], "text")).toContain("XXXX");
  appLog("Successfully Validated Masked account number");
}

async function MoveBackfrom_MyCards(){

  appLog("Intiated method to moveback from CardManagement");
  await kony.automation.playback.waitFor(["frmCardManagement","customheader","topmenu","flxaccounts"],15000);
  kony.automation.flexcontainer.click(["frmCardManagement","customheader","topmenu","flxaccounts"]);
  appLog("Successfully Moved to DashBoard Screen");

}

async function ClickOnCardOptions(CardOption){

  // Possible Options were : Lock Card,Replace Card,Report Lost,Set Limits,Change PIN,Activate Card

  appLog("Intiated method to click on Option is : <b>"+CardOption+"</b>");
  var Status=false;
  var maskedAccNumber="";
  var cvvnumber="";
  var Values = new Array();

  await kony.automation.playback.waitFor(["frmCardManagement","myCards","segMyCards"],15000);
  var segData=kony.automation.widget.getWidgetProperty(["frmCardManagement","myCards","segMyCards"],"data");
  var segLength=segData.length;
  //appLog("Length is : "+segLength);

  var finished = false;
  for(var x = 0; x <segLength && !finished; x++) {
    for(var y = 1; y <=5; y++){
      //appLog("x value is : "+x);
      //appLog("y value is : "+y);
      var Option=kony.automation.widget.getWidgetProperty(["frmCardManagement","myCards","segMyCards["+x+"]","btnAction"+y], "text");
      if(CardOption===Option){
        appLog("Successfully found option : <b>"+CardOption+"</b>");
        // get account number before clicking on it
        var accnumber=kony.automation.widget.getWidgetProperty(["frmCardManagement","myCards","segMyCards["+x+"]","rtxValue1"], "text");
        kony.automation.button.click(["frmCardManagement","myCards","segMyCards["+x+"]","btnAction"+y]);
        await kony.automation.playback.wait(5000);
        appLog("Successfully clicked on Option : <b>"+CardOption+"</b>");
        finished = true;
        Status=true;
        maskedAccNumber=accnumber;
        // get these value before Break statement
        cvvnumber=await getCVVNumber(maskedAccNumber);
        Values[0]=Status;
        Values[1]=cvvnumber;
        break;
      }
    }
  }

  expect(Status).toBe(true,"Failed to get Option from List : <b>"+CardOption+"</b>");
  return Values;
}

async function getCVVNumber(maskedAccNumber){

  //appLog("Intiated method to get required values from account number");
  var CVV=maskedAccNumber.substring(16,19);
  appLog("CVV is : "+CVV);
  return CVV;
}

async function VerifylockCard(){

  await ClickOnCardOptions("Lock Card");

  await kony.automation.playback.waitFor(["frmCardManagement","CardLockVerificationStep","confirmHeaders","lblHeading"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","CardLockVerificationStep","confirmHeaders","lblHeading"], "text")).not.toBe("");
  appLog("Successfully verified LockCard Screen");
  await kony.automation.playback.waitFor(["frmCardManagement","CardLockVerificationStep","CardActivation","flxCheckbox"],15000);
  kony.automation.widget.touch(["frmCardManagement","CardLockVerificationStep","CardActivation","flxCheckbox"], null,null,[45,33]);
  appLog("Successfully Clicked on CheckBox");
  await kony.automation.playback.waitFor(["frmCardManagement","CardLockVerificationStep","confirmButtons","btnConfirm"],15000);
  kony.automation.button.click(["frmCardManagement","CardLockVerificationStep","confirmButtons","btnConfirm"]);
  //await kony.automation.playback.wait(5000);
  appLog("Successfully Clicked on Confirm Button");

  await VerifyCardTransactionMessage();
}

async function VerifyUnlockCard(){

  await ClickOnCardOptions("Unlock Card");

  await kony.automation.playback.waitFor(["frmCardManagement","CardActivation","lblHeader"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","CardActivation","lblHeader"], "text")).not.toBe("");
  appLog("Successfully Verified Unlock Card form");
  await kony.automation.playback.waitFor(["frmCardManagement","CardActivation","CardActivation","flxCheckbox"],15000);
  kony.automation.widget.touch(["frmCardManagement","CardActivation","CardActivation","flxCheckbox"], null,null,[45,33]);
  appLog("Successfully Clicked on CheckBox");
  await kony.automation.playback.waitFor(["frmCardManagement","CardActivation","btnProceed"],15000);
  kony.automation.button.click(["frmCardManagement","CardActivation","btnProceed"]);
  //await kony.automation.playback.wait(5000);
  appLog("Successfully Clicked on Continue Button");

  await VerifyCardTransactionMessage();
}

async function VerifyChangePIN(){

  await ClickOnCardOptions("Change PIN");

  await kony.automation.playback.waitFor(["frmCardManagement","CardLockVerificationStep","confirmHeaders","lblHeading"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","CardLockVerificationStep","confirmHeaders","lblHeading"], "text")).not.toBe("");
  appLog("Successfully Verified Change PIN Card form");
  await kony.automation.playback.waitFor(["frmCardManagement","CardLockVerificationStep","tbxCurrentPIN"],15000);
  kony.automation.textbox.enterText(["frmCardManagement","CardLockVerificationStep","tbxCurrentPIN"],"5050");
  kony.automation.textbox.enterText(["frmCardManagement","CardLockVerificationStep","tbxNewPIN"],"5050");
  kony.automation.textbox.enterText(["frmCardManagement","CardLockVerificationStep","tbxConfirmPIN"],"5050");
  kony.automation.textbox.enterText(["frmCardManagement","CardLockVerificationStep","tbxNote"],"Change PIN");
  appLog("Successfully Entered Change PIN details");
  await kony.automation.playback.waitFor(["frmCardManagement","CardLockVerificationStep","confirmButtons","btnConfirm"],15000);
  kony.automation.button.click(["frmCardManagement","CardLockVerificationStep","confirmButtons","btnConfirm"]);
  appLog("Successfully Clicked on Continue Button");

  await VerifyCardTransactionMessage();
}

async function setDailyWithDrawLimit(){

  await ClickOnCardOptions("Set Limits");

  await kony.automation.playback.waitFor(["frmCardManagement","lblSetCardLimitsHeader"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","lblSetCardLimitsHeader"], "text")).toEqual("Set Card Limits");
  appLog("Successfully Verified SetLimits form");
  await kony.automation.playback.waitFor(["frmCardManagement","btnEditLimitWithdrawal"],15000);
  kony.automation.button.click(["frmCardManagement","btnEditLimitWithdrawal"]);
  appLog("Successfully Clicked on EditLimitWithdrawal Button");
  await kony.automation.playback.waitFor(["frmCardManagement","limitWithdrawalSlider"],15000);
  kony.automation.slider.slide(["frmCardManagement","limitWithdrawalSlider"], 12450);
  await kony.automation.playback.waitFor(["frmCardManagement","confirmButtons","btnConfirm"],15000);
  kony.automation.button.click(["frmCardManagement","confirmButtons","btnConfirm"]);
  appLog("Successfully Clicked on Confirm Button");

  await VerifyCardTransactionMessage();


}

async function setDailyPurchaseLimit(){

  await ClickOnCardOptions("Set Limits");

  await kony.automation.playback.waitFor(["frmCardManagement","lblSetCardLimitsHeader"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","lblSetCardLimitsHeader"], "text")).toEqual("Set Card Limits");
  appLog("Successfully Verified SetLimits form");
  await kony.automation.playback.waitFor(["frmCardManagement","btnEditLimitPurchase"],15000);
  kony.automation.button.click(["frmCardManagement","btnEditLimitPurchase"]);
  appLog("Successfully Clicked on EditLimitPurchase Button");
  kony.automation.slider.slide(["frmCardManagement","limitPurchaseSlider"], 7250);
  kony.automation.button.click(["frmCardManagement","confirmButtons","btnConfirm"]);
  appLog("Successfully Clicked on Confirm Button");

  await VerifyCardTransactionMessage();

}

async function VerifyCardTransactionMessage(){

  var Success=await kony.automation.playback.waitFor(["frmCardManagement","Acknowledgement","lblCardTransactionMessage"],30000);
  if(Success){
    appLog("Intiated method to Verify Transfer SuccessMessage");
    expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","Acknowledgement","lblCardTransactionMessage"], "text")).not.toBe("");
    await MoveBackfrom_MyCards();
  }else if(await kony.automation.playback.waitFor(["frmCardManagement","rtxDowntimeWarning"],5000)){
    appLog("Failed with : rtxDowntimeWarning");
    fail("Failed with : "+kony.automation.widget.getWidgetProperty(["frmCardManagement","rtxDowntimeWarning"], "text"));
    await MoveBackfrom_MyCards();
  }else {
    appLog("Unable to perform Successfull Transcation");
    fail("Unable to perform Successfull Transcation");
  }

}

async function NavigateToManageTravelPlan(){

  appLog("Intiated method to navigate to ManageTravelPlan form");
  await kony.automation.playback.waitFor(["frmCardManagement","flxMangeTravelPlans"],15000);
  kony.automation.flexcontainer.click(["frmCardManagement","flxMangeTravelPlans"]);
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmCardManagement","myCards","lblMyCardsHeader"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","myCards","lblMyCardsHeader"], "text")).not.toBe("");
  appLog("Successfully Verified TravelPlan form");
}

async function CreateNewTravelPlan(Destination,PhoneNumber,NoteValue){

  await NavigateToManageTravelPlan();

  appLog("Intiated method to create new Travel Plan");
  await kony.automation.playback.waitFor(["frmCardManagement","flxMangeTravelPlans"],15000);
  kony.automation.flexcontainer.click(["frmCardManagement","flxMangeTravelPlans"]);
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmCardManagement","lblMyCardsHeader"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","lblMyCardsHeader"], "text")).toContain("Travel Plan");
  appLog("Successfully Verified New TravelPlan form");

  await kony.automation.playback.waitFor(["frmCardManagement","calFrom"],15000);
  kony.automation.calendar.selectDate(["frmCardManagement","calFrom"], [5,25,2021]);
  await kony.automation.playback.waitFor(["frmCardManagement","calTo"],15000);
  kony.automation.calendar.selectDate(["frmCardManagement","calTo"], [5,30,2021]);
  appLog("Successfully Entered Travel Plan Calender");
  await kony.automation.playback.waitFor(["frmCardManagement","txtDestination"],15000);
  kony.automation.textbox.enterText(["frmCardManagement","txtDestination"],Destination);
  await kony.automation.playback.waitFor(["frmCardManagement","segDestinationList"],15000);
  kony.automation.flexcontainer.click(["frmCardManagement","segDestinationList[1]","flxCustomListBox"]);
  kony.automation.flexcontainer.click(["frmCardManagement","flxAddFeatureRequestandimg"]);
  appLog("Successfully Added Destination");
  await kony.automation.playback.waitFor(["frmCardManagement","txtPhoneNumber"],15000);
  kony.automation.textbox.enterText(["frmCardManagement","txtPhoneNumber"],PhoneNumber);
  appLog("Successfully entered Mobile number");
  await kony.automation.playback.waitFor(["frmCardManagement","txtareaUserComments"],15000);
  kony.automation.textarea.enterText(["frmCardManagement","txtareaUserComments"],NoteValue);
  appLog("Successfully entered Travel Note values");
  await kony.automation.playback.waitFor(["frmCardManagement","btnContinue"],15000);
  kony.automation.button.click(["frmCardManagement","btnContinue"]);
  appLog("Successfully Clicked on continue button");
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmCardManagement","myCards","segMyCards"],15000);
  kony.automation.widget.touch(["frmCardManagement","myCards","segMyCards[0]","flxCheckBox"], null,null,[26,27]);
  appLog("Successfully Clicked on CheckBox");
  await kony.automation.playback.waitFor(["frmCardManagement","btnCardsContinue"],15000);
  kony.automation.button.click(["frmCardManagement","btnCardsContinue"]);
  appLog("Successfully Clicked on Continue Button");
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmCardManagement","lblConfirmTravelPlan"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmCardManagement","lblConfirmTravelPlan"], "text")).toEqual("Confirm Travel Plan");
  await kony.automation.playback.waitFor(["frmCardManagement","btnConfirm"],15000);
  kony.automation.button.click(["frmCardManagement","btnConfirm"]);
  appLog("Successfully Clicked on Confirm Button");
  await kony.automation.playback.wait(5000);

  await VerifyCardTransactionMessage();
}


async function EditTravelPlan(UpdatedNumber){

  await NavigateToManageTravelPlan();

  appLog("Intiated method to Edit Travel Plan");
  await kony.automation.playback.waitFor(["frmCardManagement","myCards","segMyCards"],15000);
  kony.automation.button.click(["frmCardManagement","myCards","segMyCards[0]","btnAction1"]);
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmCardManagement","txtPhoneNumber"],15000);
  kony.automation.textbox.enterText(["frmCardManagement","txtPhoneNumber"],UpdatedNumber);
  appLog("Successfully Updated Phone Number");
  await kony.automation.playback.waitFor(["frmCardManagement","btnContinue"],15000);
  kony.automation.button.click(["frmCardManagement","btnContinue"]);
  appLog("Successfully Clicked on Continue Button");
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmCardManagement","myCards","segMyCards"],15000);
  kony.automation.widget.touch(["frmCardManagement","myCards","segMyCards[0]","flxCheckBox"], null,null,[26,28]);
  appLog("Successfully Clicked on CheckBox");
  await kony.automation.playback.waitFor(["frmCardManagement","btnCardsContinue"],15000);
  kony.automation.button.click(["frmCardManagement","btnCardsContinue"]);
  appLog("Successfully Clicked on Continue Button");
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmCardManagement","btnConfirm"],15000);
  kony.automation.button.click(["frmCardManagement","btnConfirm"]);
  appLog("Successfully Clicked on Confirm Button");

  await VerifyCardTransactionMessage();
}

async function DeleteTravelPlan(){

  await NavigateToManageTravelPlan();

  appLog("Intiated method to Delete Travel Plan");
  await kony.automation.playback.waitFor(["frmCardManagement","myCards","segMyCards"],15000);
  kony.automation.button.click(["frmCardManagement","myCards","segMyCards[0]","btnAction2"]);
  appLog("Successfully Clicked on Delete Button");
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmCardManagement","CustomAlertPopup","btnYes"],15000);
  kony.automation.button.click(["frmCardManagement","CustomAlertPopup","btnYes"]);
  appLog("Successfully Clicked on YES Button");
  await kony.automation.playback.wait(5000);

  await MoveBackfrom_MyCards();

}

async function searchforCards(SaerchCard){

  appLog("Intiated method to Search for Cards");
  await kony.automation.playback.waitFor(["frmCardManagement","myCards","txtSearch"],15000);
  kony.automation.widget.touch(["frmCardManagement","myCards","txtSearch"], [113,16],null,null);
  await kony.automation.playback.waitFor(["frmCardManagement","myCards","txtSearch"],15000);
  kony.automation.textbox.enterText(["frmCardManagement","myCards","txtSearch"],SaerchCard);
  appLog("Successfully entered card : <b>"+SaerchCard+"</b>");
  await kony.automation.playback.waitFor(["frmCardManagement","myCards","btnConfirm"],15000);
  kony.automation.flexcontainer.click(["frmCardManagement","myCards","btnConfirm"]);
  appLog("Successfully clicked on Confirm/Search button");

  await MoveBackfrom_MyCards();
}

