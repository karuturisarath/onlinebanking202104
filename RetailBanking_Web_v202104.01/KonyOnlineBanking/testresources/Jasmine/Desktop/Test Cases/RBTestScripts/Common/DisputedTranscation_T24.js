async function navigateToDisputeTranscation(){

  appLog("Intiated method to navigate to DisputeTranscation");
  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","ACCOUNTS5flxMyAccounts"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","ACCOUNTS5flxMyAccounts"]);
  await kony.automation.playback.wait(10000);
  await kony.automation.playback.waitFor(["frmDisputedTransactionsList","lblDisputeTransactions"],30000);
  expect(kony.automation.widget.getWidgetProperty(["frmDisputedTransactionsList","lblDisputeTransactions"], "text")).toContain("Disputed Transactions");
  appLog("Successfully navigate to DisputeTranscation");
}

async function DisputeTranscation_SendMsg(){

  var isDispute=await kony.automation.playback.waitFor(["frmDisputedTransactionsList","segDisputeTransactions"],15000);
  if(isDispute){
    kony.automation.flexcontainer.click(["frmDisputedTransactionsList","segDisputeTransactions[0]","flxDropdown"]);
    await kony.automation.playback.waitFor(["frmDisputedTransactionsList","segDisputeTransactions"]);
    kony.automation.button.click(["frmDisputedTransactionsList","segDisputeTransactions[0]","btnViewMessage"]);
    await kony.automation.playback.wait(10000);
    await EnterMessageDetails();
    await MoveBackToDashBoard_Messages();
  }else{
    appLog("No Dispute Transcations List");
    await MoveBackfrom_DisputeTranscation();
  }

}

async function MoveBackfrom_DisputeTranscation(){

  await kony.automation.playback.waitFor(["frmDisputedTransactionsList","customheadernew","flxAccounts"],15000);
  kony.automation.flexcontainer.click(["frmDisputedTransactionsList","customheadernew","flxAccounts"]);
}
