async function navigateToBillPay(){

  appLog("Intiated method to navigate to BillPay");
  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);
  appLog("Successfully clicked on BillPay tab");
  await kony.automation.playback.wait(5000);
}

async function navigateToOneTimePayment(){

  //await kony.automation.playback.waitFor(["frmBulkPayees","flxMakeOneTimePayment"],15000);
  //kony.automation.flexcontainer.click(["frmBulkPayees","flxMakeOneTimePayment"]);

  //Navigating using Menu
  appLog("Intiated method to navigate to OneTimePayment");
  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  appLog("Successfully clicked on Menu on Dashboard");
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","BillPayflxAccountsMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","BillPayflxAccountsMenu"]);
  appLog("Successfully clicked on Billpay option");
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","BillPay4flxMyAccounts"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","BillPay4flxMyAccounts"]);
  appLog("Successfully clicked on OneTimePayment option");
  await kony.automation.playback.wait(15000);

}

async function enterOneTimePayeeInformation(payeeName,zipcode,accno,accnoAgain,mobileno){

  appLog("Intiated method to enter OneTime Payee Information");

  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","tbxName"],15000);
  kony.automation.textbox.enterText(["frmMakeOneTimePayee","tbxName"],payeeName);
  appLog("Successfully entered payee name to auto select : <b>"+payeeName+"</b>");
  await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","segPayeesName"],15000);
  kony.automation.flexcontainer.click(["frmMakeOneTimePayee","segPayeesName[3]","flxNewPayees"]);
  appLog("Successfully selected payee name from list");

  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtZipCode"],15000);
  kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtZipCode"],zipcode);
  appLog("Successfully entered zipcode : <b>"+zipcode+"</b>");

  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtAccountNumber"],15000);
  kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtAccountNumber"],accno);
  appLog("Successfully entered acc number : <b>"+accno+"</b>");

  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtAccountNumberAgain"],15000);
  kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtAccountNumberAgain"],accnoAgain);
  appLog("Successfully Re-entered account number : <b>"+accnoAgain+"</b>");

  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","txtmobilenumber"],15000);
  kony.automation.textbox.enterText(["frmMakeOneTimePayee","txtmobilenumber"],mobileno);
  appLog("Successfully entered mobile number : <b>"+mobileno+"</b>");

  await kony.automation.playback.waitFor(["frmMakeOneTimePayee","btnNext"],15000);
  kony.automation.button.click(["frmMakeOneTimePayee","btnNext"]);
  await kony.automation.playback.wait(5000);
  appLog("Successfully clicked on Next button");
}

async function enterOneTimePaymentdetails(amount,note){

  appLog("Intiated method to enter details for OneTime payment");

  await kony.automation.playback.waitFor(["frmMakeOneTimePayment","txtPaymentAmount"],15000);
  kony.automation.textbox.enterText(["frmMakeOneTimePayment","txtPaymentAmount"],amount);
  appLog("Successfully entered amount : <b>"+amount+"</b>");

  appLog("Intiated method to Select Payee From Acc for OneTime payment");

  await kony.automation.playback.waitFor(["frmMakeOneTimePayment","txtTransferFrom"],15000);
  kony.automation.widget.touch(["frmMakeOneTimePayment","txtTransferFrom"], [264,20],null,null);
  kony.automation.flexcontainer.click(["frmMakeOneTimePayment","segTransferFrom[0,0]","flxAccountListItem"]);
  appLog("Successfully selected Bill PayFrom");

  await kony.automation.playback.waitFor(["frmMakeOneTimePayment","txtNotes"],15000);
  kony.automation.textbox.enterText(["frmMakeOneTimePayment","txtNotes"],note);
  appLog("Successfully entered note value : <b>"+note+"</b>");

  await kony.automation.playback.waitFor(["frmMakeOneTimePayment","btnNext"],15000);
  kony.automation.button.click(["frmMakeOneTimePayment","btnNext"]);
  await kony.automation.playback.wait(5000);
  appLog("Successfully clicked on Next button");
}

async function confirmOneTimePaymnet(){

  appLog("Intiated method to confirm OneTimePayment");

  await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","flxImgCheckBox"],15000);
  kony.automation.flexcontainer.click(["frmOneTimePaymentConfirm","flxImgCheckBox"]);
  appLog("Successfully accepted Checkbox");

  await kony.automation.playback.waitFor(["frmOneTimePaymentConfirm","btnConfirm"],15000);
  kony.automation.button.click(["frmOneTimePaymentConfirm","btnConfirm"]);
  appLog("Successfully Clicked on Confirm Button");
  
}

async function verifyOneTimePaymentSuccessMsg(){

  appLog("Intiated method to verify OneTimePayment SuccessMsg");
  
  await kony.automation.playback.wait(5000);
  var success=await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement"],30000);

  if(success){
    //await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","flxSuccess","lblSuccessMessage"],15000);
    //expect(kony.automation.widget.getWidgetProperty(["frmOneTimePaymentAcknowledgement","flxSuccess","lblSuccessMessage"],"text")).not.toBe('');
    await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"],15000);
    kony.automation.flexcontainer.click(["frmOneTimePaymentAcknowledgement","customheadernew","flxAccounts"]);
    appLog("Successfully Moved back to Accounts Dashboard");
  }else if(await kony.automation.playback.waitFor(["frmMakeOneTimePayment","rtxDowntimeWarning"],5000)){
    //appLog("Logged in User is not authorized to perform this action");
    //fail('Logged in User is not authorized to perform this action');
    appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmMakeOneTimePayment","rtxDowntimeWarning"],"text"));
    fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmMakeOneTimePayment","rtxDowntimeWarning"],"text"));


    await kony.automation.playback.waitFor(["frmMakeOneTimePayment","customheadernew","flxAccounts"],15000);
    kony.automation.flexcontainer.click(["frmMakeOneTimePayment","customheadernew","flxAccounts"]);
    appLog("Successfully Moved back to Accounts Dashboard");
  }else{
    appLog("Unable to perform OneTimePayment");
  }

}


async function navigateToManagePayee(){

  await navigateToBillPay();
  appLog("Intiated method to navigate to Manage Payee list");
  await kony.automation.playback.waitFor(["frmBulkPayees","btnManagePayee"],15000);
  kony.automation.button.click(["frmBulkPayees","btnManagePayee"]);
  appLog("Successfully clicked on Manage payee Button");
  await kony.automation.playback.wait(5000);
}

async function selectPayee_ManagePayeeList(payeename){

  appLog("Intiated method to select Payee from Manage Payee list : <b>"+payeename+"</b>");

  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","txtSearch"],15000);
  kony.automation.textbox.enterText(["frmManagePayees","manageBiller","txtSearch"],payeename);
  appLog("Successfully entered Payee "+payeename);

  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","btnConfirm"],15000);
  kony.automation.flexcontainer.click(["frmManagePayees","manageBiller","btnConfirm"]);
  appLog("Successfully clicked on Search button");
  await kony.automation.playback.wait(5000);

  //   await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","txtSearch"],15000);
  //   kony.automation.textbox.enterText(["frmManagePayees","manageBiller","txtSearch"], [ { modifierCapsLock:true, key : 'A' },
  //                                                             { modifierCapsLock:true, key : 'B' },
  // 															{ modifierCapsLock:true, key : 'C' },
  //                                                             { modifierCapsLock:false, keyCode : 13 }
  // 														]);

  appLog("Intiated Method to verify Payee <b>"+payeename+"</b>");

  var PayeeList=await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","segmentBillPay"],15000);
  if(PayeeList){
    //expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","manageBiller","segmentBillPay[0]","lblColumn1"],"text")).toEqual(payeename);
    expect(kony.automation.widget.getWidgetProperty(["frmManagePayees","manageBiller","segmentBillPay[0]","lblColumn1"],"text")).not.toBe('');
  }else if(await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","rtxNoPaymentMessage"],5000)){
    appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmManagePayees","manageBiller","rtxNoPaymentMessage"],"text"));
    fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmManagePayees","manageBiller","rtxNoPaymentMessage"],"text"));

  }else{
    appLog("Unable to find Payee in ManagePayees List");
  }

}

async function clickOnBillPayBtn_ManagePayees(){


  // BillPay and Active ebill has same locator hence verifying text and doing operation accordingly, Instead of directly failing.
  
  appLog("Intiated method to click on Billpay button from Manage Payee list");

  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","segmentBillPay"],15000);

  var ButtonName=kony.automation.widget.getWidgetProperty(["frmManagePayees","manageBiller","segmentBillPay[0]","btnAction"], "text");

  //appLog('Button Name is : '+ButtonName);
  
  if(ButtonName==='Activate ebill'){

    appLog("Info : <b>"+ButtonName+"</b>"+" is Available instead of BillPay button");
    //Activate e Bill to convert button to PayaBill. instead of failing we can proceed execution
    kony.automation.button.click(["frmManagePayees","manageBiller","segmentBillPay[0]","btnAction"]);
    await kony.automation.playback.waitFor(["frmManagePayees","btnProceedIC"],15000);
    kony.automation.button.click(["frmManagePayees","btnProceedIC"]);
    appLog('Successfully clicked on YES button');
    await kony.automation.playback.wait(10000);
    kony.automation.button.click(["frmManagePayees","manageBiller","segmentBillPay[0]","btnAction"]);
    appLog("Successfully clicked on BillPay button");

  }else{
    
    // We can directly click on BillPay button
    kony.automation.button.click(["frmManagePayees","manageBiller","segmentBillPay[0]","btnAction"]);
    appLog("Successfully clicked on BillPay button");
    await kony.automation.playback.wait(5000);
  }
}


async function enterAmount_SheduleBillPay(amount){

  appLog("Intiated method to enter amount : <b>"+amount+"</b>");
  await kony.automation.playback.waitFor(["frmPayABill","txtSearch"],15000);
  kony.automation.textbox.enterText(["frmPayABill","txtSearch"],amount);
  appLog("Successfully entered amount : <b>"+amount+"</b>");

  await SelectPayFromAcc_SheduleBillPay();
}

async function SelectPayFromAcc_SheduleBillPay(){

  appLog("Intiated method to Select Payee From");

  await kony.automation.playback.waitFor(["frmPayABill","txtTransferFrom"],15000);
  kony.automation.widget.touch(["frmPayABill","txtTransferFrom"], [600,17],null,null);
  kony.automation.flexcontainer.click(["frmPayABill","segTransferFrom[0,0]","flxAccountListItem"]);

  appLog("Successfully selected Payee from the list");
}

async function selectfrequency_SheduledBillPay(freq){

  appLog("Intiated method to select freq : <b>"+freq+"</b>");
  await kony.automation.playback.waitFor(["frmPayABill","lbxFrequency"],15000);
  kony.automation.listbox.selectItem(["frmPayABill","lbxFrequency"],freq);
  appLog("Successfully selected freq : "+freq);
}

async function SelectDateRange_SheduledBillpay() {

  //new chnage in 202010
  //await kony.automation.playback.wait(5000);
  appLog("Intiated method to select DateRange");
  await kony.automation.playback.waitFor(["frmPayABill","lbxForHowLong"],15000);
  kony.automation.listbox.selectItem(["frmPayABill","lbxForHowLong"], "ON_SPECIFIC_DATE");

  await kony.automation.playback.waitFor(["frmPayABill","calSendOn"],15000);
  kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
  appLog("Successfully selected sendOn Date");
  await kony.automation.playback.waitFor(["frmPayABill","calEndingOn"],15000);
  kony.automation.calendar.selectDate(["frmPayABill","calEndingOn"], [11,21,2021]);
  appLog("Successfully selected EndOn Date");
}

async function SelectSendOnDate_SheduledBillpay() {

  await kony.automation.playback.waitFor(["frmPayABill","calSendOn"],15000);
  kony.automation.calendar.selectDate(["frmPayABill","calSendOn"], [11,11,2021]);
  appLog("Successfully selected sendOn Date");
}

async function SelectOccurences_SheduledBillPay(occurences) {
  //new chnage in 202010
  appLog("Intiated method to select N.of Occurences");
  await kony.automation.playback.waitFor(["frmPayABill","lbxForHowLong"],15000);
  kony.automation.listbox.selectItem(["frmPayABill","lbxForHowLong"], "NO_OF_RECURRENCES");
  await kony.automation.playback.waitFor(["frmPayABill","txtEndingOn"],15000);
  kony.automation.textbox.enterText(["frmPayABill","txtEndingOn"],occurences);
  appLog("Successfully selected Occurences : <b>"+occurences+"</b>");
}

async function EnterNoteValue_SheduledBillPay(notes) {

  appLog("Intiated method to enter note value");
  await kony.automation.playback.waitFor(["frmPayABill","txtNotes"],15000);
  kony.automation.textbox.enterText(["frmPayABill","txtNotes"],notes);
  appLog("Successfully entered Note value : <b>"+notes+"</b>");

  appLog("Intiated method to click on Confirm button");
  await kony.automation.playback.waitFor(["frmPayABill","btnConfirm"],15000);
  kony.automation.button.click(["frmPayABill","btnConfirm"]);
  appLog("Successfully clicked on Confirm button");
}

async function confirmSheduledBillpay(){

  appLog("Intiated method to Confirm Sheduled BillPayment");

  await kony.automation.playback.waitFor(["frmPayBillConfirm","flxImgCheckBox"],15000);
  kony.automation.flexcontainer.click(["frmPayBillConfirm","flxImgCheckBox"]);
  appLog("Successfully accepted terms check box");

  await kony.automation.playback.waitFor(["frmPayBillConfirm","btnConfirm"],15000);
  kony.automation.button.click(["frmPayBillConfirm","btnConfirm"]);
  appLog("Successfully clicked on Confirm button");
}

async function cancelSheduledBillPay(){

  appLog("Intiated method to CANCEL Sheduled BillPayment");

  await kony.automation.playback.waitFor(["frmPayBillConfirm","btnCancel"],15000);
  kony.automation.button.click(["frmPayBillConfirm","btnCancel"]);
  appLog("Successfully clicked on Cancel button");

  await kony.automation.playback.waitFor(["frmPayBillConfirm","CancelPopup","lblPopupMessage"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmPayBillConfirm","CancelPopup","lblPopupMessage"],"text")).toEqual("Are you sure you want to cancel this transaction?");

  await kony.automation.playback.waitFor(["frmPayBillConfirm","CancelPopup","btnYes"],15000);
  kony.automation.button.click(["frmPayBillConfirm","CancelPopup","btnYes"]);
  appLog("Successfully clicked on YES button");

  await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],15000);
  kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
  appLog("Successfully MovedBack to Account DashBoard");
}

async function verifySheduledBillpaySuccessMsg(){

  appLog("Intiated method to verify Sheduled BillPay SuccessMsg");
  
  await kony.automation.playback.wait(5000);
  var Success= await kony.automation.playback.waitFor(["frmPayBillAcknowledgement"],30000);
  
  if(Success){
  //expect(kony.automation.widget.getWidgetProperty(["frmPayBillAcknowledgement","lblSuccessMessage"],"text")).not.toBe('');
  await kony.automation.playback.waitFor(["frmPayBillAcknowledgement","customheadernew","flxAccounts"],15000);
  kony.automation.flexcontainer.click(["frmPayBillAcknowledgement","customheadernew","flxAccounts"]);
  appLog("Successfully MovedBack to Account DashBoard");
  }else if(await kony.automation.playback.waitFor(["frmPayABill","rtxDowntimeWarning"],15000)){
    //Checking for exception message
    //Move back to dashboard again there is an exception message
    appLog("Exception while performing a Sheduled BillPay");
    await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],15000);
    kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
    await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
    expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
    
    //appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmPayABill","rtxDowntimeWarning"],"text"));
    //fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmPayABill","rtxDowntimeWarning"],"text"));
    appLog("Failed : Unable to Perform Successfull Transcation. Failed with rtxDowntimeWarning");
    fail("Failed : Unable to Perform Successfull Transcation. Failed with rtxDowntimeWarning");
  }else{
     appLog("Unable to verify Success Message");
  }

}

async function navigateToSheduledBillPay(){

  await navigateToBillPay();
  await kony.automation.playback.waitFor(["frmBulkPayees","btnScheduled"],15000);
  kony.automation.button.click(["frmBulkPayees","btnScheduled"]);
  appLog("Successfully clicked on Sheduled tab");
  await kony.automation.playback.wait(5000);
}

async function clickOnEditButton_SheduledBillPayment(){

  appLog("Intiated method to click on Edit button");
  await kony.automation.playback.waitFor(["frmBillPayScheduled","segmentBillpay"],15000);
  kony.automation.button.click(["frmBillPayScheduled","segmentBillpay[0]","btnEdit"])
  appLog("Successfully clicked on Edit button");
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmPayABill","lblPayABill"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmPayABill","lblPayABill"],"text")).toEqual("Pay a Bill");

}

async function UpdatedSheduledBillPayment(notes){

  await SelectPayFromAcc_SheduleBillPay();
  await selectfrequency_SheduledBillPay("Once");
  await EnterNoteValue_SheduledBillPay(notes);
  await confirmSheduledBillpay();

}
async function EditSheduledBillPay(notes){

  var nopayments=await kony.automation.playback.waitFor(["frmBillPayScheduled","rtxNoPaymentMessage"],15000);
  if(nopayments){
    appLog("There are no sheduled payments");
    //Move back to accounts
    await kony.automation.playback.waitFor(["frmBillPayScheduled","customheadernew","flxAccounts"],15000);
    kony.automation.button.click(["frmBillPayScheduled","customheadernew","flxAccounts"]);
    appLog("Successfully MovedBack to Account DashBoard");
  }else{

    appLog("There are few sheduled payments");
    await clickOnEditButton_SheduledBillPayment();
    await UpdatedSheduledBillPayment(notes);
    var warning=await kony.automation.playback.waitFor(["frmPayABill","rtxDowntimeWarning"],15000);
    if(warning){
      await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],15000);
      kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
      await verifyAccountsLandingScreen();
      appLog("Successfully MovedBack to Account DashBoard");
      //fail("Custom Message :: Amount Greater than Allowed Maximum Deposit");
      appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmPayABill","rtxDowntimeWarning"],"text"));
      fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmPayABill","rtxDowntimeWarning"],"text"));

    }else{
      await verifySheduledBillpaySuccessMsg();
      await verifyAccountsLandingScreen();
    }  

  }
}

async function clickOnAddPayeeLink(){

  appLog("Intiated method to click on Add payee link");
  await kony.automation.playback.waitFor(["frmBulkPayees","flxAddPayee"],15000);
  kony.automation.flexcontainer.click(["frmBulkPayees","flxAddPayee"]);
  appLog("Successfully Clicked on addPayee link");
}

async function enterPayeeDetails_UsingPayeeinfo(payeeName,address1,address2,city,zipcode,accno,note){

  appLog("Intiated method to Add Payee Details");

  await kony.automation.playback.waitFor(["frmAddPayee1","btnEnterPayeeInfo"],15000);
  kony.automation.button.click(["frmAddPayee1","btnEnterPayeeInfo"]);
  appLog("Successfully Clicked on EnterPayeeInfo Tab");
  await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterName"],15000);
  kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterName"],payeeName);
  appLog("Successfully Entered Payee name as : <b>"+payeeName+"</b>");
  await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterAddress"],15000);
  kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterAddress"],address1);
  appLog("Successfully Entered Address Line1 as : <b>"+address1+"</b>");
  await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterAddressLine2"],15000);
  kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterAddressLine2"],address2);
  appLog("Successfully Entered Address Line2 as : <b>"+address2+"</b>");
  await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxCity"],15000);
  kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxCity"],city);
  appLog("Successfully Entered CityName as : <b>"+city+"</b>");
  await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterZipCode"],15000);
  kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterZipCode"],zipcode);
  appLog("Successfully Entered Zipcode as : <b>"+zipcode+"</b>");
  await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxEnterAccountNmber"],15000);
  kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxEnterAccountNmber"],accno);
  appLog("Successfully Entered account number as : <b>"+accno+"</b>");
  await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxConfirmAccNumber"],15000);
  kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxConfirmAccNumber"],accno);
  appLog("Successfully Re-Entered account number as : <b>"+accno+"</b>");
  
//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","flxClick"],15000);
//   kony.automation.flexcontainer.click(["frmAddPayeeInformation","flxClick"]);
//   await kony.automation.playback.waitFor(["frmAddPayeeInformation","tbxAdditionalNote"],15000);
//   kony.automation.textbox.enterText(["frmAddPayeeInformation","tbxAdditionalNote"],note);
//   appLog("Successfully Entered Note value as : <b>"+note+"</b>");
  
  await kony.automation.playback.waitFor(["frmAddPayeeInformation","btnReset"],15000);
  kony.automation.button.click(["frmAddPayeeInformation","btnReset"]);
  appLog("Successfully Clicked on Next button ");
}

async function clickOnNextButton_payeeDetails(){

  appLog("Intiated method verify Payee Details");
  await kony.automation.playback.waitFor(["frmPayeeDetails","btnDetailsConfirm"],15000);
  kony.automation.button.click(["frmPayeeDetails","btnDetailsConfirm"]);
  appLog("Successfully Clicked on Next button ");

  await linkPayee();
}

async function SelectPayeeBankingType_payeeDetails(BankingType){

  appLog("Intiated method to click on AddRecepientContinue");
  var btnAddRecepient=await kony.automation.playback.waitFor(["frmPayeeDetails","btnAddRecepientContinue"],15000);
  if(btnAddRecepient){
    kony.automation.button.click(["frmPayeeDetails","btnAddRecepientContinue"]);
    appLog("Successfully Clicked on AddRecepientContinue button ");
    await kony.automation.playback.wait(5000);
  }else{
    appLog("Selecting Banking type screen is not available");
  }

}

async function linkPayee(){

  var linkreciptent=await kony.automation.playback.waitFor(["frmPayeeDetails","contractList","lblHeader"],15000);

  if(linkreciptent){
    kony.automation.widget.touch(["frmPayeeDetails","contractList","lblCheckBoxSelectAll"], [8,6],null,null);
    appLog("Successfully selected select All CheckBox");
    await kony.automation.playback.waitFor(["frmPayeeDetails","contractList","flxCol4"],15000);
    kony.automation.flexcontainer.click(["frmPayeeDetails","contractList","flxCol4"]);
    await kony.automation.playback.waitFor(["frmPayeeDetails","contractList","btnAction6"],15000);
    kony.automation.button.click(["frmPayeeDetails","contractList","btnAction6"]);
    appLog("Successfully Clicked on Link Reciptent Continue Button");

  }
}

async function clickOnConfirmButton_verifyPayee(){

  appLog("Intiated method to confirm Payee Details");
  await kony.automation.playback.waitFor(["frmVerifyPayee","btnConfirm"],15000);
  kony.automation.button.click(["frmVerifyPayee","btnConfirm"]);
  appLog("Successfully Clicked on Confirm button ");
}

async function verifyAddPayeeSuccessMsg(){

  appLog("Intiated method to verify Add payee SuccessMsg");
  await kony.automation.playback.wait(5000);
  var success=await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],30000);
  
  if(success){
    await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],15000);
    expect(kony.automation.widget.getWidgetProperty(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],"text")).not.toBe('');
    await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"],15000);
    kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
    appLog("Successfully Moved back to Accounts dashboard");
    
  }else if(await kony.automation.playback.waitFor(["frmAddPayeeInformation","rtxDowntimeWarning"],5000)){
    
    appLog("Intiated method to verify DowntimeWarning");
    await kony.automation.playback.waitFor(["frmAddPayeeInformation","customheadernew","flxAccounts"],15000);
    kony.automation.flexcontainer.click(["frmAddPayeeInformation","customheadernew","flxAccounts"]);
    
    appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmAddPayeeInformation","rtxDowntimeWarning"],"text"));
    fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmAddPayeeInformation","rtxDowntimeWarning"],"text"));

  }else{
    appLog("Unable to add Payee");
  }

}


async function expandPayee_ManagePayee(){

  appLog("Intiated method to Expand payee from Manage payee");
  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","segmentBillPay"],15000);
  kony.automation.flexcontainer.click(["frmManagePayees","manageBiller","segmentBillPay[0]","flxDropdown"]);
  appLog("Successfully clicked on Manage Payees dropdown arrow");
}

async function MoveBackToDashBoard_ManagePayees(){

  await kony.automation.playback.waitFor(["frmManagePayees","customheadernew","flxAccounts"],15000);
  kony.automation.flexcontainer.click(["frmManagePayees","customheadernew","flxAccounts"]);
  appLog("Successfully Moved back to Accounts dashboard");
}
async function clickOnEditBtn_ManagePayees(){

  await expandPayee_ManagePayee();
  appLog("Intiated method to Edit Biller");
  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","segmentBillPay"],15000);
  kony.automation.button.click(["frmManagePayees","manageBiller","segmentBillPay[0]","btn3"]);
  appLog("Successfully clicked on Editbutton under manage payee");
}

async function deletePayee_ManagePayee(){

  appLog("Intiated method to Delete Payee");
  await expandPayee_ManagePayee();
  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","segmentBillPay"],15000);
  kony.automation.button.click(["frmManagePayees","manageBiller","segmentBillPay[0]","btn4"]);
  appLog("Successfully clicked on Delete button under manage payee");
  await kony.automation.playback.waitFor(["frmManagePayees","btnYesIC"],15000);
  kony.automation.button.click(["frmManagePayees","btnYesIC"]);
  appLog("Successfully clicked on YES button on delete biller");
  await kony.automation.playback.wait(5000);
  await MoveBackToDashBoard_ManagePayees();
}

async function EditPayee_ManagePayee(){

  appLog("Intiated method to Edit Payee");

  await clickOnEditBtn_ManagePayees();

  appLog("Intiated method to updated biller Zipcode");
  await kony.automation.playback.waitFor(["frmManagePayees","tbxZipCode"],15000);
  kony.automation.textbox.enterText(["frmManagePayees","tbxZipCode"],"123456");
  appLog("Successfully Updated biller zipcode");

  appLog("Intiated method to click on Continue button");
  await kony.automation.playback.waitFor(["frmManagePayees","btnContinue"],15000);
  kony.automation.button.click(["frmManagePayees","btnContinue"]);
  appLog("Successfully Clicked on Continue button")

  appLog("Intiated method to click on Savelink Continue button");
  await kony.automation.playback.waitFor(["frmManagePayees","contractList","btnAction6"],15000);
  kony.automation.button.click(["frmManagePayees","contractList","btnAction6"]);
  await kony.automation.playback.wait(5000);
  appLog("Successfully Clicked on Savelink Continue button");

  await verifyUpdatePayeeSuccessMsg();

}


async function verifyUpdatePayeeSuccessMsg(){

  appLog("Intiated method to verify Update payee SuccessMsg");
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],30000);
  expect(kony.automation.widget.getWidgetProperty(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],"text")).not.toBe('');
  await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","customheadernew","flxAccounts"],15000);
  kony.automation.flexcontainer.click(["frmPayeeAcknowledgement","customheadernew","flxAccounts"]);
  appLog("Successfully Moved back to Accounts dashboard");

}


async function navigateToPastBillPay(){

  appLog("Intiated method to navigate to Billpay History");
  await navigateToBillPay();
  await kony.automation.playback.waitFor(["frmBulkPayees","btnHistory"],15000);
  kony.automation.button.click(["frmBulkPayees","btnHistory"]);
  appLog("Successfully clicked on History tab");
  await kony.automation.playback.wait(5000);
}

async function clickonRepeatButton_PastBillpay(){

  appLog("Intiated method to click on Repeat button");
  await kony.automation.playback.waitFor(["frmBillPayHistory","segmentBillpay"],15000);
  kony.automation.button.click(["frmBillPayHistory","segmentBillpay[0]","btnRepeat"]);
  appLog("Successfully clicked on Repeat tab");
  await kony.automation.playback.wait(5000);
}

async function repeatPastBillPayment(note){

  appLog("Intiated method to Repeat a BillPay");

  var nopayments=await kony.automation.playback.waitFor(["frmBillPayHistory","rtxNoPaymentMessage"],15000);

  if(nopayments){
    appLog("There are no History payments");
    //Move back to accounts
    await kony.automation.playback.waitFor(["frmBillPayHistory","customheadernew","flxAccounts"],15000);
    kony.automation.button.click(["frmBillPayHistory","customheadernew","flxAccounts"]);
    appLog("Successfully Moved back to Accounts dashboard");
  }else{

    await clickonRepeatButton_PastBillpay();
    await SelectPayFromAcc_SheduleBillPay();
    await EnterNoteValue_SheduledBillPay(note);
    await confirmSheduledBillpay();

    var warning=await kony.automation.playback.waitFor(["frmPayABill","rtxDowntimeWarning"],15000);
    if(warning){
      await kony.automation.playback.waitFor(["frmPayABill","customheadernew","flxAccounts"],15000);
      kony.automation.flexcontainer.click(["frmPayABill","customheadernew","flxAccounts"]);
      await verifyAccountsLandingScreen();
      appLog("Successfully Moved back to Accounts dashboard");
      //fail("Custom Message :: Amount Greater than Allowed Maximum Deposit");
      appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmPayABill","rtxDowntimeWarning"],"text"));
      fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmPayABill","rtxDowntimeWarning"],"text"));


    }else{
      await verifySheduledBillpaySuccessMsg();
      await verifyAccountsLandingScreen();
      appLog("Successfully Moved back to Accounts dashboard");
    }

  }
}

async function clickOnAllpayeesTab(){

  appLog("Intiated method to click on Allpayees tab");
  await kony.automation.playback.waitFor(["frmBulkPayees","btnAllPayees"],15000);
  kony.automation.button.click(["frmBulkPayees","btnAllPayees"]);
  appLog("Successfully clicked on Allpayees tab");
  await kony.automation.playback.wait(5000);
}

async function verifyAllPayeesList(){

  appLog("Intiated method to verify Allpayees List");

  var PayeeList=await kony.automation.playback.waitFor(["frmBulkPayees","segmentBillpay"],15000);

  if(PayeeList){
    kony.automation.flexcontainer.click(["frmBulkPayees","segmentBillpay[0]","flxDropdown"]);
    appLog("Successfully verified on Allpayees List");
  }else if(await kony.automation.playback.waitFor(["frmBulkPayees","rtxNoPaymentMessage"],5000)){

    appLog("Failed : "+kony.automation.widget.getWidgetProperty(["frmBulkPayees","rtxNoPaymentMessage"],"text"));
    fail('Failed : '+kony.automation.widget.getWidgetProperty(["frmBulkPayees","rtxNoPaymentMessage"],"text"));

  }else {
    appLog("Unable to verify Allpayees List");
  }

}

async function MoveBackToDashBoard_AllPayees(){

  await kony.automation.playback.waitFor(["frmBulkPayees","customheadernew","flxAccounts"],15000);
  kony.automation.flexcontainer.click(["frmBulkPayees","customheadernew","flxAccounts"]);
}

async function clickOnSavePayeeButton_OneTimePay(){

  appLog("Intiated method to Save Payee from OneTime Payment");

  await kony.automation.playback.waitFor(["frmOneTimePaymentAcknowledgement","btnSavePayee"],15000);
  kony.automation.button.click(["frmOneTimePaymentAcknowledgement","btnSavePayee"]);
  appLog("Successfully Clicked on Save button");

  //Continue Button
  await kony.automation.playback.waitFor(["frmPayeeDetails","btnDetailsConfirm"],15000);
  kony.automation.button.click(["frmPayeeDetails","btnDetailsConfirm"]);
  appLog("Successfully Clicked on Continue button");

  //Confirm Button
  await kony.automation.playback.waitFor(["frmVerifyPayee","btnConfirm"],15000);
  kony.automation.button.click(["frmVerifyPayee","btnConfirm"]);
  appLog("Successfully Clicked on Confirm button");

  await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAddPayee"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmPayeeAcknowledgement","lblAddPayee"],"text")).toEqual("Add Payee");


  await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmPayeeAcknowledgement","lblAcknowledgementMessage"],"text")).toContain("has been added.");
  appLog("Successfully verified Added payee");

  await kony.automation.playback.waitFor(["frmPayeeAcknowledgement","btnViewAllPayees"],15000);
  kony.automation.button.click(["frmPayeeAcknowledgement","btnViewAllPayees"]);
  appLog("Successfully clicked on ViewAll payees button");

}

async function activateBillPayTermsconditions(){

  appLog("Intiated method to Activate Billpay TC's");

  var warning=await kony.automation.playback.waitFor(["frmBillPayActivation","lblWarning"],15000);
  if(warning){
    //expect(kony.automation.widget.getWidgetProperty(["frmBillPayActivation","lblWarning"], "text")).toEqual("Please activate My Bills.");
    await kony.automation.playback.waitFor(["frmBillPayActivation","listbxAccountType"],15000);
    kony.automation.listbox.selectItem(["frmBillPayActivation","listbxAccountType"], "160128223241511");
    appLog("Successfully Selected Default BillPay Acc");
    await kony.automation.playback.waitFor(["frmBillPayActivation","lblFavoriteEmailCheckBox"],15000);
    kony.automation.widget.touch(["frmBillPayActivation","lblFavoriteEmailCheckBox"], null,null,[14,13]);
    appLog("Successfully accepted checkbox");
    await kony.automation.playback.waitFor(["frmBillPayActivation","flxAgree"],15000);
    kony.automation.flexcontainer.click(["frmBillPayActivation","flxAgree"]);
    appLog("Successfully clicked on AgreeFlex");
    await kony.automation.playback.waitFor(["frmBillPayActivation","btnProceed"],15000);
    kony.automation.button.click(["frmBillPayActivation","btnProceed"]);
    appLog("Successfully clicked on Proceed button");
    var error=await kony.automation.playback.waitFor(["frmBillPayActivation","rtxErrorMessage"],15000);
    if(error){
      //expect(kony.automation.widget.getWidgetProperty(["frmBillPayActivation","rtxErrorMessage"], "text")).toEqual("Update operation failed on  customerpreference:No value specified for parameter 2");
      await kony.automation.playback.waitFor(["frmBillPayActivation","customheadernew","flxAccounts"],15000);
      kony.automation.flexcontainer.click(["frmBillPayActivation","customheadernew","flxAccounts"]);
    }
  }else{
    appLog("Already accepted billpay activation..Moveback to dashboard");
    await MoveBackToDashBoard_AllPayees();
  }
}

async function activateNewlyAddedpayee(){

  appLog('Intiated method to activate Newly Added Payee');

  await kony.automation.playback.waitFor(["frmManagePayees","manageBiller","segmentBillPay"],15000);

  var ButtonName=kony.automation.widget.getWidgetProperty(["frmManagePayees","manageBiller","segmentBillPay[0]","btnAction"], "text");

  //appLog('Button Name is : '+ButtonName);

  if(ButtonName==='Activate ebill'){

    kony.automation.button.click(["frmManagePayees","manageBiller","segmentBillPay[0]","btnAction"]);
    appLog('Successfully clicked on activate button');

    var activate=await kony.automation.playback.waitFor(["frmManagePayees","lblWarningOneIC"],15000);
    if(activate){
      await kony.automation.playback.waitFor(["frmManagePayees","btnProceedIC"],15000);
      kony.automation.button.click(["frmManagePayees","btnProceedIC"]);
      appLog('Successfully clicked on YES button');
      await kony.automation.playback.wait(10000);
      await MoveBackToDashBoard_ManagePayees();
    }else {
      appLog('Failed : Unable to Activate Added Payee');
      fail('Failed : Unable to Activate Added Payee');
    }
  }else {
    appLog('Payee Already activated');
  }


}