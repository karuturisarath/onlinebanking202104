async function navigateToMakePayments(){

  appLog("Intiated method to Navigate Payments Screen");

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransferMoney"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransferMoney"]);
  await kony.automation.playback.wait(15000);

  await VerifyPaymentsScreen();

}

async function navigateToTransfers(){

  appLog("Intiated method to Navigate Transfers Screen");

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"],15000);
  //kony.automation.widget.touch(["frmDashboard","customheader","topmenu","flxTransfersAndPay"], [105,12],null,null);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxPayBills"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxPayBills"]);
  await kony.automation.playback.wait(10000);

  await VerifyTransfersScreen();

}

async function VerifyTransfersScreen(){

  await kony.automation.playback.waitFor(["frmMakePayment","lblTransfers"],30000);
  var Status=kony.automation.widget.getWidgetProperty(["frmMakePayment","lblTransfers"], "text");
  expect(Status).toEqual("Transfers","Failed to Navigate to Transfers Screen");
  appLog("Successfully Navigated to Transfers Screen");
}

async function VerifyPaymentsScreen(){

  await kony.automation.playback.waitFor(["frmMakePayment","lblTransfers"],30000);
  var Status=kony.automation.widget.getWidgetProperty(["frmMakePayment","lblTransfers"], "text");
  expect(Status).toEqual("Payments","Failed to Navigate to Payments Screen");
  appLog("Successfully Navigated to MakePayment Screen");
}

async function SelectFromAccount(fromAcc){

  appLog("Intiated method to Select From Account :: <b>"+fromAcc+"</b>");
  await kony.automation.playback.waitFor(["frmMakePayment","txtTransferFrom"],30000);
  kony.automation.widget.touch(["frmMakePayment","txtTransferFrom"], [241,25],null,null);
  kony.automation.textbox.enterText(["frmMakePayment","txtTransferFrom"],fromAcc);
  await kony.automation.playback.wait(3000);
  await kony.automation.playback.waitFor(["frmMakePayment","segTransferFrom"],30000);
  kony.automation.flexcontainer.click(["frmMakePayment","segTransferFrom[0,0]","flxAmount"]);
  appLog("Successfully Selected From Account from List");
}

async function ReSelectFromAccount(fromAcc){

  appLog("Intiated method to Re-Select From Account :: <b>"+fromAcc+"</b>");
  await kony.automation.playback.waitFor(["frmMakePayment","flxDropdown"],30000);
  kony.automation.flexcontainer.click(["frmMakePayment","flxDropdown"]);

  await kony.automation.playback.waitFor(["frmMakePayment","txtTransferFrom"],30000);
  kony.automation.widget.touch(["frmMakePayment","txtTransferFrom"], [189,19],null,null);
  await kony.automation.playback.waitFor(["frmMakePayment","flxMainWrapper"],30000);
  kony.automation.flexcontainer.click(["frmMakePayment","flxMainWrapper"]);
  kony.automation.textbox.enterText(["frmMakePayment","txtTransferFrom"],fromAcc);
  kony.automation.flexcontainer.click(["frmMakePayment","segTransferFrom[0,0]","flxAmount"]);

  appLog("Successfully Selected From Account from List");
}

async function SelectToAccount(ToAccReciptent){

  appLog("Intiated method to Select To Account :: <b>"+ToAccReciptent+"</b>");
  await kony.automation.playback.waitFor(["frmMakePayment","txtTransferTo"],15000);
  //kony.automation.widget.touch(["frmMakePayment","txtTransferTo"], [150,23],null,null);
  kony.automation.textbox.enterText(["frmMakePayment","txtTransferTo"],ToAccReciptent);
  await kony.automation.playback.wait(3000);
  await kony.automation.playback.waitFor(["frmMakePayment","segTransferTo"],30000);
  kony.automation.flexcontainer.click(["frmMakePayment","segTransferTo[0]","flxAccountListItemWrapper"]);
  appLog("Successfully Selected To Account from List");
  // To laod dynamic data after selecting To account
  await kony.automation.playback.wait(5000);

}

async function SelectOwnTransferToAccount(ToAccReciptent){

  appLog("Intiated method to Select To Account :: <b>"+ToAccReciptent+"</b>");

  await kony.automation.playback.waitFor(["frmMakePayment","txtTransferTo"],15000);
  //kony.automation.widget.touch(["frmMakePayment","txtTransferTo"], [150,23],null,null);
  kony.automation.textbox.enterText(["frmMakePayment","txtTransferTo"],ToAccReciptent);
  await kony.automation.playback.waitFor(["frmMakePayment","segTransferTo"],30000);
  kony.automation.flexcontainer.click(["frmMakePayment","segTransferTo[0,0]","flxAccountListItemWrapper"]);

  appLog("Successfully Selected To Account from List");
  // To laod dynamic data after selecting To account
  await kony.automation.playback.wait(5000);

}

async function verifyExistingSameBanBeneficiaryDetails(){


}

async function verifyExistingDomesticBeneficiaryDetails(){


}

async function verifyExistingInternationalBeneficiaryDetails(){

  await kony.automation.playback.waitFor(["frmMakePayment","txtSwift"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmMakePayment","txtSwift"], "text")).not.toBe("");
  //   await kony.automation.playback.waitFor(["frmMakePayment","txtAddressLine01"],15000);
  //   expect(kony.automation.widget.getWidgetProperty(["frmMakePayment","txtAddressLine01"], "text")).not.toBe("");
  //   await kony.automation.playback.waitFor(["frmMakePayment","txtCity"],15000);
  //   expect(kony.automation.widget.getWidgetProperty(["frmMakePayment","txtCity"], "text")).not.toBe("");
  //   await kony.automation.playback.waitFor(["frmMakePayment","txtPostCode"],15000);
  //   expect(kony.automation.widget.getWidgetProperty(["frmMakePayment","txtPostCode"], "text")).not.toBe("");

}


async function EnterAmount(amountValue) {

  await kony.automation.playback.waitFor(["frmMakePayment","txtAmount"],15000);
  kony.automation.textbox.enterText(["frmMakePayment","txtAmount"],amountValue);
  appLog("Successfully Entered Amount as : <b>"+amountValue+"</b>");
  await kony.automation.scrollToWidget(["frmMakePayment","customfooternew","btnFaqs"]);
}

async function SelectFrequency(freqValue) {

  //kony.automation.flexcontainer.click(["frmFastTransfers","flxContainer4"]);
  await kony.automation.playback.waitFor(["frmMakePayment","lbxFrequency"],15000);
  kony.automation.listbox.selectItem(["frmMakePayment","lbxFrequency"], freqValue);
  appLog("Successfully Selected Freq as : <b>"+freqValue+"</b>");
}

async function SelectDateRange() {

  //var today = new Date();
  //kony.automation.calendar.selectDate(["frmConsolidatedStatements","calFromDate"], [(today.getDate()-2),(today.getMonth()+1),today.getFullYear()]);
  await kony.automation.playback.waitFor(["frmMakePayment","calSendOnNew"],15000);
  kony.automation.calendar.selectDate(["frmMakePayment","calSendOnNew"], [4,25,2021]);
  await kony.automation.playback.waitFor(["frmMakePayment","calEndingOnNew"],15000);
  kony.automation.calendar.selectDate(["frmMakePayment","calEndingOnNew"], [4,25,2022]);
  appLog("Successfully Selected DateRange");
}

async function SelectSendOnDate() {

  await kony.automation.playback.waitFor(["frmMakePayment","calSendOnNew"],15000);
  kony.automation.calendar.selectDate(["frmMakePayment","calSendOnNew"], [4,25,2021]);
  appLog("Successfully Selected SendOn Date");
}

async function selectNormalPaymentRadio(){

  appLog("Intiated method to select Payment mode as Normal");
  await kony.automation.playback.waitFor(["frmMakePayment","flxRadioBtn5"],15000);
  kony.automation.flexcontainer.click(["frmMakePayment","flxRadioBtn5"]);
  appLog("Successfully Selected Payment mode as Normal");
}

async function selectFeePaidRadio(){

  // sync issue where it's not selecting properly some times
  appLog("Intiated method to Selected Fee Pay Radio");
//   await kony.automation.playback.waitFor(["frmMakePayment","flxtooltipFeesImg"],30000);
//   kony.automation.flexcontainer.click(["frmMakePayment","flxtooltipFeesImg"]);
  // - flxShared is t0 just come out of cursor
  await kony.automation.playback.waitFor(["frmMakePayment","flxShared"],30000);
  kony.automation.flexcontainer.click(["frmMakePayment","flxShared"]);

  //await kony.automation.scrollToWidget(["frmMakePayment","lblRadioBtn3"]);
  //kony.automation.widget.touch(["frmMakePayment","lblRadioBtn3"], null,null,[8,11]);
  var isRadio=await kony.automation.playback.waitFor(["frmMakePayment","flxRadioBtn3"],45000);
  expect(isRadio).toBe(true,"Failed to find Fee-Paid Radio");
  //await kony.automation.scrollToWidget(["frmMakePayment","flxRadioBtn3"]);
  kony.automation.flexcontainer.click(["frmMakePayment","flxRadioBtn3"]);
  appLog("Successfully Selected Fee Pay Radio");
}

async function EnterNoteValue(notes) {

  await kony.automation.playback.waitFor(["frmMakePayment","txtPaymentReference"],15000);
  kony.automation.textbox.enterText(["frmMakePayment","txtPaymentReference"],notes);
  appLog("Successfully entered Note value as : <b>"+notes+"</b>");

  await kony.automation.playback.waitFor(["frmMakePayment","btnConfirm"],15000);
  var isEnable=kony.automation.widget.getWidgetProperty(["frmMakePayment","btnConfirm"], "enable");
  if(isEnable){
    appLog('Intiated method to click on Continue button');
    await kony.automation.scrollToWidget(["frmMakePayment","btnConfirm"]);
    kony.automation.button.click(["frmMakePayment","btnConfirm"]);
    await kony.automation.playback.wait(5000);
    appLog('Successfully Clicked on Continue button');

    var Confirm=await kony.automation.playback.waitFor(["frmConfirmEuro","lblHeading"],45000);
    if(Confirm){
      appLog('Custom Message : Successfully moved to frmConfirmEuro Screen');
    }else if(await kony.automation.playback.waitFor(["frmMakePayment","rtxMakeTransferError"],5000)){
      appLog('Custom Message : Failed with rtxMakeTransferError');
      fail('Custom Message :'+kony.automation.widget.getWidgetProperty(["frmMakePayment","rtxMakeTransferError"], "text"));
      await MoveBackToLandingScreen_Transfers();
    }else{
      appLog('Custom Message : Failed to Navigate to frmConfirmEuro Screen');
      fail('Custom Message : Failed to Navigate to frmConfirmEuro Screen');
      await MoveBackToLandingScreen_Transfers();
    }
  }else{
    appLog('Custom Message : CONTINUE button is not enabled');
  }

}

async function ValidatePaymentField_OwnTransfers(){

  var Status=	await kony.automation.playback.waitFor(["frmConfirmEuro","lblPaymentMethodKey"],15000);
  expect(Status).toBe(false,"Payment Method is not expected for Transfers");
}

async function ValidateSwiftCodeDetails_SameBankBenefeciary(){

  var Status=	await kony.automation.playback.waitFor(["frmConfirmEuro","lblSWIFTBICKey"],15000);
  expect(Status).toBe(false,"SWIFT Details are not expected for SameBank Benefeciary");

}


async function ConfirmTransfer() {

  appLog("Intiated method to Confirm Transfer Details");

  await kony.automation.playback.waitFor(["frmConfirmEuro","btnContinue"],30000);
  kony.automation.button.click(["frmConfirmEuro","btnContinue"]);
  await kony.automation.playback.wait(5000);
  appLog("Successfully Clicked on Confirm Button");

}

async function ClickOnModifyButton(){

  await kony.automation.playback.waitFor(["frmConfirmEuro","btnModify"],15000);
  kony.automation.button.click(["frmConfirmEuro","btnModify"]);
  appLog("Successfully Clicked on btnModify Button");
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmMakePayment","lblTransfers"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmMakePayment","lblTransfers"], "text")).not.toBe("");
}

async function VerifyTransferSuccessMessage() {

  //await kony.automation.playback.wait(5000);
  var success=await kony.automation.playback.waitFor(["frmAcknowledgementEuro","lblSuccessMessage"],90000);

  if(success){
    appLog("Intiated method to Verify Transfer SuccessMessage");
    //await kony.automation.playback.waitFor(["frmAcknowledgementEuro","lblSuccessMessage"],15000);
    //expect(kony.automation.widget.getWidgetProperty(["frmAcknowledgementEuro","lblSuccessMessage"], "text")).not.toBe("");
    await kony.automation.playback.waitFor(["frmAcknowledgementEuro","customheadernew","flxAccounts"],15000);
    kony.automation.flexcontainer.click(["frmAcknowledgementEuro","customheadernew","flxAccounts"]);
    appLog("Successfully Clicked on Accounts Button");
  }else if(await kony.automation.playback.waitFor(["frmMakePayment","rtxMakeTransferError"],5000)){
    //expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","rtxMakeTransferError"], "text")).toEqual("Transaction cannot be executed. Update your organization's approval matrix and re-submit the transaction.");
    appLog("Failed with : rtxMakeTransferError");
    fail("Failed with : "+kony.automation.widget.getWidgetProperty(["frmFastTransfers","rtxMakeTransferError"], "text"));

    await MoveBackToLandingScreen_Transfers();

  }else{

    // This is the condition for use cases where it won't throw error on UI but struck at same screen
    appLog("Unable to perform Successfull Transcation");
    fail("Unable to perform Successfull Transcation");
  }

}

async function verifyDataCutOff_Ackform(){

  //await kony.automation.playback.wait(5000);
  var success=await kony.automation.playback.waitFor(["frmAcknowledgementEuro"],60000);

  if(success){
    appLog("Intiated method to Verify Transfer SuccessMessage");
    await kony.automation.playback.waitFor(["frmAcknowledgementEuro","lblFrequencyValue"],15000);
    expect(kony.automation.widget.getWidgetProperty(["frmAcknowledgementEuro","lblFrequencyValue"], "text")).not.toBe("");
    await kony.automation.playback.waitFor(["frmAcknowledgementEuro","lblPaymentReferenceValue"],15000);
    expect(kony.automation.widget.getWidgetProperty(["frmAcknowledgementEuro","lblPaymentReferenceValue"], "text")).not.toBe("");

    await kony.automation.playback.waitFor(["frmAcknowledgementEuro","customheadernew","flxAccounts"],15000);
    kony.automation.flexcontainer.click(["frmAcknowledgementEuro","customheadernew","flxAccounts"]);
    appLog("Successfully Clicked on Accounts Button");

  }else if(await kony.automation.playback.waitFor(["frmMakePayment","rtxMakeTransferError"],5000)){
    //expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","rtxMakeTransferError"], "text")).toEqual("Transaction cannot be executed. Update your organization's approval matrix and re-submit the transaction.");
    appLog("Failed with : rtxMakeTransferError");
    fail("Failed with : "+kony.automation.widget.getWidgetProperty(["frmFastTransfers","rtxMakeTransferError"], "text"));

    await MoveBackToLandingScreen_Transfers();

  }else{

    // This is the condition for use cases where it won't throw error on UI but struck at same screen
    appLog("Unable to perform Successfull Transcation");
    fail("Unable to perform Successfull Transcation");
  }
}

async function MoveBackToLandingScreen_Transfers(){

  //Move back to landing Screen
  appLog("Intiated method to move from frmMakePayment screen");
  await kony.automation.playback.waitFor(["frmMakePayment","customheadernew","flxAccounts"],15000);
  kony.automation.flexcontainer.click(["frmMakePayment","customheadernew","flxAccounts"]);
  var DashBoard=await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
  expect(DashBoard).toBe(true,"Failed to navigate to DashBoard");
  appLog("Successfully Moved back to Accounts dashboard");
}

async function MoveBackToLandingScreen_TransferConfirm(){

  //Move back to landing Screen
  appLog("Intiated method to move from frmFastTransfers screen");
  await kony.automation.playback.waitFor(["frmConfirmEuro","customheadernew","flxAccounts"],15000);
  kony.automation.flexcontainer.click(["frmConfirmEuro","customheadernew","flxAccounts"]);
  var DashBoard=await kony.automation.playback.waitFor(["frmDashboard","lblShowing"],15000);
  expect(DashBoard).toBe(true,"Failed to navigate to DashBoard");
  appLog("Successfully Moved back to Accounts dashboard");
}


async function EnterNewToAccountName(ToAccReciptent){

  appLog("Intiated method to enter new To Account :: <b>"+ToAccReciptent+"</b>");

  await kony.automation.playback.waitFor(["frmMakePayment","txtTransferTo"],15000);
  kony.automation.widget.touch(["frmMakePayment","txtTransferTo"], [150,23],null,null);
  kony.automation.textbox.enterText(["frmMakePayment","txtTransferTo"],ToAccReciptent);
  appLog("Successfully entered New To acc name  as : <b>"+ToAccReciptent+"</b>");
  // To laod dynamic data after selecting To account
  //await kony.automation.playback.wait(5000);

}

async function clickOnNewButton_OneTimePay(){

  await kony.automation.playback.waitFor(["frmMakePayment","lblNew"],15000);
  kony.automation.widget.touch(["frmMakePayment","lblNew"], null,null,[12,11]);
  await kony.automation.playback.waitFor(["frmMakePayment","flxCancelFilterTo"],15000);
  kony.automation.flexcontainer.click(["frmMakePayment","flxCancelFilterTo"]);
  await kony.automation.playback.wait(5000);
  appLog("Successfully clicked on new button for OneTimePay");

}

async function selectSameBankRadioBtn(){

  await kony.automation.playback.waitFor(["frmMakePayment","flxBankOption1"],15000);
  kony.automation.flexcontainer.click(["frmMakePayment","flxBankOption1"]);
  appLog("Successfully clicked on Radio button - This account is with us");
}

async function selectOtherBankRadioBtn(){

  await kony.automation.playback.waitFor(["frmMakePayment","flxBankOption2"],15000);
  kony.automation.flexcontainer.click(["frmMakePayment","flxBankOption2"]);
  appLog("Successfully clicked on Radio button - This account is Other bank");
}

async function enterOneTimePaymentDetails_SameBank(Accno,amount){

  await kony.automation.playback.waitFor(["frmMakePayment","txtAccountNumber"],15000);
  kony.automation.textbox.enterText(["frmMakePayment","txtAccountNumber"],Accno);
  appLog("Successfully entered acc no as : <b>"+Accno+"</b>");
  // To laod dynamic data after selecting To account
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmMakePayment","txtAmount"],15000);
  kony.automation.textbox.enterText(["frmMakePayment","txtAmount"],amount);
  appLog("Successfully entered amount as : <b>"+amount+"</b>");
}

async function enterOneTimePaymentDetails_Domestic(IBAN,amount){

  await kony.automation.playback.waitFor(["frmMakePayment","txtAccountNumber"],15000);
  kony.automation.textbox.enterText(["frmMakePayment","txtAccountNumber"],IBAN);
  appLog("Successfully entered acc no as : <b>"+IBAN+"</b>");
  // To laod dynamic data after selecting To account
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmMakePayment","txtAmount"],15000);
  kony.automation.textbox.enterText(["frmMakePayment","txtAmount"],amount);
  appLog("Successfully entered amount as : <b>"+amount+"</b>");
}
async function enterOneTimePaymentDetails_International(Accno,Swift,amount){

  await kony.automation.playback.waitFor(["frmMakePayment","txtAccountNumber"],15000);
  kony.automation.textbox.enterText(["frmMakePayment","txtAccountNumber"],Accno);
  appLog("Successfully entered acc no as : <b>"+Accno+"</b>");
  // To laod dynamic data after selecting To account
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmMakePayment","txtSwift"],15000);
  kony.automation.textbox.enterText(["frmMakePayment","txtSwift"],Swift);
  appLog("Successfully entered SWIFT as : <b>"+Swift+"</b>");
  await kony.automation.playback.waitFor(["frmMakePayment","txtAmount"],15000);
  kony.automation.textbox.enterText(["frmMakePayment","txtAmount"],amount);
  appLog("Successfully entered amount as : <b>"+amount+"</b>");
}

async function VerifyOneTimePaymentSuccessMessage(){

  var success=await kony.automation.playback.waitFor(["frmAcknowledgementEuro"],60000);

  if(success){
    appLog("Intiated method to Verify OneTimePaymentSuccessMessage");
    await kony.automation.playback.waitFor(["frmAcknowledgementEuro","customheadernew","flxAccounts"],15000);
    kony.automation.flexcontainer.click(["frmAcknowledgementEuro","customheadernew","flxAccounts"]);
    appLog("Successfully Clicked on Accounts Button");
  }else if(await kony.automation.playback.waitFor(["frmMakePayment","rtxMakeTransferError"],5000)){
    //expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","rtxMakeTransferError"], "text")).toEqual("Transaction cannot be executed. Update your organization's approval matrix and re-submit the transaction.");
    appLog("Failed with : rtxMakeTransferError");
    fail("Failed with : "+kony.automation.widget.getWidgetProperty(["frmMakePayment","rtxMakeTransferError"], "text"));

    await MoveBackToLandingScreen_Transfers();

  }else{

    // This is the condition for use cases where it won't throw error on UI but struck at same screen
    appLog("Unable to perform Successfull Transcation");
    fail("Unable to perform Successfull Transcation");
  }

}

async function SaveOneTimePaymentBenefeciary(){

  var success=await kony.automation.playback.waitFor(["frmAcknowledgementEuro","btnSaveBeneficiary"],60000);

  if(success){
    kony.automation.button.click(["frmAcknowledgementEuro","btnSaveBeneficiary"]);
    appLog("Successfully clicked on btnSaveBeneficiary");
    if(await kony.automation.playback.waitFor(["frmAcknowledgementEuro","lblSuccessmesg"],10000)){
      //expect(kony.automation.widget.getWidgetProperty(["frmAcknowledgementEuro","lblSuccessmesg"], "text")).not.toBe("");
      appLog("Info : Benefeciary is Saved Successfully");
    }else{
      await kony.automation.playback.waitFor(["frmAcknowledgementEuro","lblFailureMsg"],5000)
      //expect(kony.automation.widget.getWidgetProperty(["frmAcknowledgementEuro","lblFailureMsg"], "text")).toEqual("jndj");
      appLog("Warning : Same Benefeciary is Already Saved previousely");
    }
    await kony.automation.playback.waitFor(["frmAcknowledgementEuro","customheadernew","flxAccounts"],15000);
    kony.automation.flexcontainer.click(["frmAcknowledgementEuro","customheadernew","flxAccounts"]);
    appLog("Successfully Clicked on Accounts Button");

  }else if(await kony.automation.playback.waitFor(["frmMakePayment","rtxMakeTransferError"],5000)){
    //expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","rtxMakeTransferError"], "text")).toEqual("Transaction cannot be executed. Update your organization's approval matrix and re-submit the transaction.");
    appLog("Failed with : rtxMakeTransferError");
    fail("Failed with : rtxMakeTransferError");

    await MoveBackToLandingScreen_Transfers();

  }else{

    // This is the condition for use cases where it won't throw error on UI but struck at same screen
    appLog("Unable to perform Successfull Transcation");
    fail("Unable to perform Successfull Transcation");
  }

}

