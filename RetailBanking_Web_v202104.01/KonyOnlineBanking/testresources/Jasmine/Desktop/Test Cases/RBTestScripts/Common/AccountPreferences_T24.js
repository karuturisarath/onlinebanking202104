async function navigateToAccountPreferences(){

  appLog("Intiated method to Navigate AccountPreferences Screen");

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","Settings2flxMyAccounts"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","Settings2flxMyAccounts"]);
  await kony.automation.playback.wait(10000);
  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAccountsHeader"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAccountsHeader"], "text")).toContain("Accounts");
  var Status=await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAccounts"],45000);
  expect(Status).toBe(true,"FAILED to load Accounts segment");
  appLog("Successfully Navigated to AccountPreferences Screen");
}

async function clickonEditButton(){

  appLog("Intiated method to click on Edit button");

  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAccounts"],45000);
  // Even segAccounts is visible unable to click EDIT as there is a AppLoader
  await kony.automation.playback.waitFor(["frmProfileManagement","settings","segAccounts[0,0]","btnEdit"],15000);
  kony.automation.button.click(["frmProfileManagement","settings","segAccounts[0,0]","btnEdit"]);
  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblEditAccountsHeader"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblEditAccountsHeader"], "text")).toContain("Edit Account");
  await kony.automation.playback.wait(5000);
  appLog("Successfully clicked on Edit button");
}

async function EnableEStatement(){

  appLog("Intiated method to Enable e-Statements");
  
  var Status=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblFavoriteEmailCheckBox"],"text");

  if(Status==='D'){
    appLog("Intiated method to click on enable e-Statements");
    await kony.automation.playback.waitFor(["frmProfileManagement","settings","flximgEnableEStatementsCheckBox"],15000);
    kony.automation.flexcontainer.click(["frmProfileManagement","settings","flximgEnableEStatementsCheckBox"]);
    appLog("Successfully Clicked on ENABLE e-Statement CheckBox");
    // Accept Terms and conditions after enabling e-Statement
	//await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblIAccept"],15000);
	//expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblIAccept"], "text")).not.toBe("");
	var isTerms=await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxTCContentsCheckbox"],15000);
    if(isTerms){
      kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxTCContentsCheckbox"]);
      appLog("Successfully accepted ENABLE e-Statement terms and conditions");
    }
	
  }else{
    appLog("e-Statement is already enabled");
  }
}

async function DisableEStatement(){

  appLog("Intiated method to Disable e-Statements");
  
  var Status=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblFavoriteEmailCheckBox"],"text");

  if(Status==='C'){
    appLog("Intiated method to click on disable e-Statements");
    await kony.automation.playback.waitFor(["frmProfileManagement","settings","flximgEnableEStatementsCheckBox"],10000);
    kony.automation.flexcontainer.click(["frmProfileManagement","settings","flximgEnableEStatementsCheckBox"]);
    appLog("Successfully Clicked on DISABLE e-Statement CheckBox");
    // Accept Terms and conditions after enabling e-Statement
	//await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblIAccept"],15000);
	//expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblIAccept"], "text")).not.toBe("");
	var isTerms=await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxTCContentsCheckbox"],15000);
    if(isTerms){
      kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxTCContentsCheckbox"]);
      appLog("Successfully accepted ENABLE e-Statement terms and conditions");
    }
	
  }else{
    appLog("e-Statement is already disabled");
  }

}

async function clickOnSaveButton(){

  appLog("Intiated method to click on SAVE button");
  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditAccountsSave"],30000);
  await kony.automation.scrollToWidget(["frmProfileManagement","settings","btnEditAccountsSave"]);
  kony.automation.button.click(["frmProfileManagement","settings","btnEditAccountsSave"]);
  await kony.automation.playback.wait(5000);
  appLog("Successfully clicked on SAVE button");
  //await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAccountsHeader"],15000);
  //expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAccountsHeader"], "text")).toContain("Accounts");
  
}

async function MoveBackToDashBoard_ProfileManagement(){

  // Move back to base state
  await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],15000);
  kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
  appLog("Successfully Moved back to Accounts dashboard");
}