async function navigateToConsentmanagement(){

  appLog("Intiated method to Navigate Consentmanagement Screen");

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"]);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","Settings5flxMyAccounts"]);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","Settings5flxMyAccounts"]);
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
  await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblflxConsentManagementHeader"]);
  expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblflxConsentManagementHeader"], "text")).toEqual("Your Consent");

  appLog("Successfully Navigated to Consentmanagement Screen");
}

async function VerifyDisableCheckbox_ViewMode(){

  appLog("Intiated method to verify edit view mode");
  var Status=await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditConsent"],15000);
  expect(Status).toBe(true,"Failed to dispaly View Mode");
}

async function EditConsent(){

  appLog("Intiated method to Edit consent");
  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditConsent"],15000);
  kony.automation.button.click(["frmProfileManagement","settings","btnEditConsent"]);
  appLog("Successfully clicked on Edit button");
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxCloneRow0lblCheckBox"],15000);
  kony.automation.widget.touch(["frmProfileManagement","settings","flxCloneRow0lblCheckBox"], [9,11],null,null);
  await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxCloneRow0flxProfileManagementConsentOptions"],15000);
  kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxCloneRow0flxProfileManagementConsentOptions"]);
  appLog("Successfully clicked on Check Box");
  await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnConsentManagementSave"],15000);
  await kony.automation.scrollToWidget(["frmProfileManagement","settings","btnConsentManagementSave"]);
  kony.automation.button.click(["frmProfileManagement","settings","btnConsentManagementSave"]);
  appLog("Successfully clicked on Save button");
  //await kony.automation.playback.wait(10000);
  await kony.automation.playback.waitForLoadingScreenToDismiss(20000);
  await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"],15000);
  kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
  
}
