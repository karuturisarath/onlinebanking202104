it("MonthlySheduledTransfer-Occurences", async function() {
  
  await navigateToUnifiedTransfers();
  await ClickonMakeTransfrBtn("International");
  await SelectUTFFromAccount(UTFPayments.International.FromAcc);
  await SelectUTFToAccount(UTFPayments.International.ToAcc);
  await Enter_CCY_AmountValue(UTFPayments.International.Amount);
  await SelectUTFFrequency("Monthly");
  await SelectUTFSendOnDate();
  await SelectUTFOccurences();
  await EnterUTFNoteValue("International-MonthlySheduledTransfer-Occurences");
  await clickonUTFConfirmBtn();
  await VerifyUTFTransferSuccessMsg();
  
 },TimeOuts.UnifiedTransfers.Transfers);