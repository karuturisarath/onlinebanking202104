it("AddNewInternationalAcc", async function() {
  
  var AccountNumber=getRandomNumber(8);
  var SwiftCode=UTFManageBeneficiary.International.SWIFT;
  var BeneficiaryName=UTFManageBeneficiary.International.BeneficiaryName;
  var Nickname=UTFManageBeneficiary.International.Nickname;
//   var Address1=UTFManageBeneficiary.International.Address1;
//   var Address2=UTFManageBeneficiary.International.Address2;
//   var city=UTFManageBeneficiary.International.city;
//   var zipcode=UTFManageBeneficiary.International.zipcode;
  
  await NavigateToManageBeneficiary();
  if(await isBenefeciaryAlreadyAdded(SwiftCode)){
    await MoveBackFrom_ManageBeneficiaries();
  }else{
  await MoveBackFrom_ManageBeneficiaries();
  await navigateToUnifiedTransfers();
  await clickOnAddNewAccountBtn_International();
  await EnterInternationalAccDetails(BeneficiaryName,AccountNumber,SwiftCode,Nickname);
  await ClickonConfirmButton();
  await VerifyAddNewAccSuccessMsg();
  }
  
},TimeOuts.UnifiedTransfers.AddNewAccount);