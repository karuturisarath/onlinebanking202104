it("SavingAcc_Navigate_Statement", async function() {
  
  await SelectContextualOnDashBoard(AllAccounts.Saving.Shortaccno);
  await selectContextMenuOption(AllAccounts.Saving.MenuOptions[2].option);
  await verifyVivewStatementsHeader();
  await MoveBackToLandingScreen_AccDetails();
  
},TimeOuts.Accounts.Timeout);