it("SavingAcc_Navigate_AccountAlerts", async function() {
  
  await SelectContextualOnDashBoard(AllAccounts.Saving.Shortaccno);
  await selectContextMenuOption(AllAccounts.Saving.MenuOptions[7].option);
  await VerifyAccountAlertScreen();
  await MoveBack_Accounts_ProfileManagement();
  
},TimeOuts.Accounts.Timeout);