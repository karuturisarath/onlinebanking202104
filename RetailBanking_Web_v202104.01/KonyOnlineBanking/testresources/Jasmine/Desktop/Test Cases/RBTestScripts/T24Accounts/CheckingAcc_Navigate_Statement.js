it("CheckingAcc_Navigate_Statement", async function() {
  
  await SelectContextualOnDashBoard(AllAccounts.Current.Shortaccno);
  await selectContextMenuOption(AllAccounts.Current.MenuOptions[2].option);
  await verifyVivewStatementsHeader();
  await MoveBackToLandingScreen_AccDetails();
  
},TimeOuts.Accounts.Timeout);