it("VerifyTransfers_Occurences", async function() {
  
  await navigateToTransfers();
  await SelectFromAccount(Payments.OwnAcc.FromAcc);
  await SelectOwnTransferToAccount(Payments.OwnAcc.ToAcc);
  await EnterAmount(Payments.OwnAcc.Amount);
  await SelectFrequency("Monthly");
  await SelectDateRange();
  await EnterNoteValue("VerifyTransfers_Occurences");
  await ConfirmTransfer();
  await VerifyTransferSuccessMessage();
  
},TimeOuts.OwnPayments.Payment);