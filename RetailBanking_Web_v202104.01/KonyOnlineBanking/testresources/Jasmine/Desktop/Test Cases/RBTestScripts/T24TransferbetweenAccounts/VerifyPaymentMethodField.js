it("VerifyPaymentMethodField", async function() {
  
  await navigateToTransfers();
  await SelectFromAccount(Payments.OwnAcc.FromAcc);
  await SelectOwnTransferToAccount(Payments.OwnAcc.ToAcc);
  await EnterAmount(Payments.OwnAcc.Amount);
  await EnterNoteValue("VerifyPaymentMethodField");
  await ValidatePaymentField_OwnTransfers();
  await MoveBackToLandingScreen_TransferConfirm();
  
},TimeOuts.OwnPayments.Payment);