it("ModifyTransfersDetails", async function() {
  
  await navigateToTransfers();
  await SelectFromAccount(Payments.OwnAcc.FromAcc);
  await SelectOwnTransferToAccount(Payments.OwnAcc.ToAcc);
  await EnterAmount(Payments.OwnAcc.Amount);
  await EnterNoteValue("ModifyTransfersDetails");
  await ClickOnModifyButton();
  await EnterNoteValue("Update-ModifyTransfersDetails");
  await ConfirmTransfer();
  await VerifyTransferSuccessMessage();
  
},TimeOuts.OwnPayments.Payment);