it("CombinedStatement_CSV", async function() {
  
  await navigateToCombinedStatements();
  await clickOnGenarateNewStatement();
  await selectFromDate();
  await selectAccountforStatement();
  await clickOnCombinedStatementContinueButton();
  await selectFormatAsCSV();
  await clickOnCreateStatementButton();
  await verifyStatementCreationPopupMsg();
  await MoveBackToLandingScreen_AccDetails();
  
},120000);