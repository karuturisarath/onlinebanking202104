it("VerifyCurrencyField", async function() {
  
  await navigateToUnifiedTransfers();
  await ClickonMakeTransfrBtn("Domestic");
  await SelectUTFFromAccount(UTFPayments.Domestic.FromAcc);
  await SelectUTFToAccount(UTFPayments.Domestic.ToAcc);
  await Enter_CCY_AmountValue(UTFPayments.Domestic.Amount);
  await EnterUTFNoteValue("Domestic-VerifyCurrency");
  await VerifyCurrencyField_Domestic();
  
},TimeOuts.UnifiedTransfers.Transfers);