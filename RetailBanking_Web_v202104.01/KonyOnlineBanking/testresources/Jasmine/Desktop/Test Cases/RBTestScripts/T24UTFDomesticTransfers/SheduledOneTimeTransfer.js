it("SheduledOneTimeTransfer", async function() {
  
  await navigateToUnifiedTransfers();
  await ClickonMakeTransfrBtn("Domestic");
  await SelectUTFFromAccount(UTFPayments.Domestic.FromAcc);
  await SelectUTFToAccount(UTFPayments.Domestic.ToAcc);
  await Enter_CCY_AmountValue(UTFPayments.Domestic.Amount);
  await SelectUTFSendOnDate();
  await EnterUTFNoteValue("Domestic-OneTimeTransfers");
  await clickonUTFConfirmBtn();
  await VerifyUTFTransferSuccessMsg();
  
},TimeOuts.UnifiedTransfers.Transfers);