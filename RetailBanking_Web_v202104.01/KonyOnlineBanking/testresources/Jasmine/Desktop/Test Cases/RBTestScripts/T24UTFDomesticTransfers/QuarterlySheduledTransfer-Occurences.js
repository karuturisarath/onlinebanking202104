it("QuarterlySheduledTransfer-Occurences", async function() {
  
  await navigateToUnifiedTransfers();
  await ClickonMakeTransfrBtn("Domestic");
  await SelectUTFFromAccount(UTFPayments.Domestic.FromAcc);
  await SelectUTFToAccount(UTFPayments.Domestic.ToAcc);
  await Enter_CCY_AmountValue(UTFPayments.Domestic.Amount);
  await SelectUTFFrequency("Qtrly");
  await SelectUTFSendOnDate();
  await SelectUTFOccurences();
  await EnterUTFNoteValue("Domestic-QtrlySheduledTransfer-Occurences");
  await clickonUTFConfirmBtn();
  await VerifyUTFTransferSuccessMsg();
  
 },TimeOuts.UnifiedTransfers.Transfers);