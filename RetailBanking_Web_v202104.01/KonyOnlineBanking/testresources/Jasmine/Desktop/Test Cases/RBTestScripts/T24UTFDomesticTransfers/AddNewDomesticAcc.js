it("AddNewDomesticAcc", async function() {
  
  var IBAN=UTFManageBeneficiary.Domestic.IBANList[0].IBAN;
  var BeneficiaryName=UTFManageBeneficiary.Domestic.BeneficiaryName;
  var Nickname=UTFManageBeneficiary.Domestic.Nickname;
  var Address1=UTFManageBeneficiary.Domestic.Address1;
  var Address2=UTFManageBeneficiary.Domestic.Address2;
  var city=UTFManageBeneficiary.Domestic.city;
  var zipcode=UTFManageBeneficiary.Domestic.zipcode;
  
  await NavigateToManageBeneficiary();
  if(await isBenefeciaryAlreadyAdded(IBAN)){
    await MoveBackFrom_ManageBeneficiaries();
  }else{
  await MoveBackFrom_ManageBeneficiaries();
  await navigateToUnifiedTransfers();
  await clickOnAddNewAccountBtn_Domestic();
  await EnterDomesticAccDetails(BeneficiaryName,IBAN,Nickname);
  await ClickonConfirmButton();
  await VerifyAddNewAccSuccessMsg();
  }
  
},TimeOuts.UnifiedTransfers.AddNewAccount);