it("WeeklySheduledTransfer-DateRange", async function() {
  
  await navigateToUnifiedTransfers();
  await ClickonMakeTransfrBtn("Domestic");
  await SelectUTFFromAccount(UTFPayments.Domestic.FromAcc);
  await SelectUTFToAccount(UTFPayments.Domestic.ToAcc);
  await Enter_CCY_AmountValue(UTFPayments.Domestic.Amount);
  await SelectUTFFrequency("Weekly");
  await SelectUTFDateRange();
  await EnterUTFNoteValue("Domestic-WeeklySheduledTransfer-DateRange");
  await clickonUTFConfirmBtn();
  await VerifyUTFTransferSuccessMsg();
  
},TimeOuts.UnifiedTransfers.Transfers);