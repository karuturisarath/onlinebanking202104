it("VerifyOneTimeWireTransfer", async function() {
  
  var RecipientName=WireTransers.DomesticRecipitentDetails.BeneficiaryName;
  var AddressLine1=WireTransers.DomesticRecipitentDetails.Address1;
  var AddressLine2=WireTransers.DomesticRecipitentDetails.Address2;
  var City=WireTransers.DomesticRecipitentDetails.city;
  var ZipCode=WireTransers.DomesticRecipitentDetails.zipcode;
  
  var SwiftCode=WireTransers.InternationalBankdetails.SWIFT;
  var AccNumber=WireTransers.DomesticBankdetails.AccNumber+getRandomNumber(7);
  var NickName=WireTransers.DomesticBankdetails.Nickname;
  var BankName=WireTransers.DomesticBankdetails.BankName;
  var BankAddressLine1=WireTransers.DomesticBankdetails.BankAddressLine1;
  var BankAddressLine2=WireTransers.DomesticBankdetails.BankAddressLine2;
  var BankCity=WireTransers.DomesticBankdetails.BankCity;
  var BankZipcode=WireTransers.DomesticBankdetails.BankZipcode;
  
  await NavigateToWireOneTimePayment();
  await EnterOneTimeDomesticRecipitentDetails(RecipientName,AddressLine1,AddressLine2,City,ZipCode);
  await EnterOneTimeDomesticBankDetails(SwiftCode,AccNumber,NickName,BankName,BankAddressLine1,BankAddressLine2,BankCity,BankZipcode);
  await MakeOneTimeWireTransfer();
  await VerifyOneTimeDomesticWireTransferSuccessMsg();
  
},TimeOuts.WireTransfers.Payment);