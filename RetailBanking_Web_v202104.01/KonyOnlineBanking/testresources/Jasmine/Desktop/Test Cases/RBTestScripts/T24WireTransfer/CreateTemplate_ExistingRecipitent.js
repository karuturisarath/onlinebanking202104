it("CreateTemplate_ExistingRecipitent", async function() {
  
  await NavigatetoCreateNewTemplate();
  await EnterTemplateDetails("Temp_ExistingRecipitent"+getRandomString(5));
  await selectExistingRecipitentOption();
  await CreateNewTemplate_Existingrecipitent();
  
},TimeOuts.WireTransfers.Payment);