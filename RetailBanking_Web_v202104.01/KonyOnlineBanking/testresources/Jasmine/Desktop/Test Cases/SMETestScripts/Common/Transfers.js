async function navigateToTransfers(){

  appLog("Intiated method to Navigate FastTransfers Screen");

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransferMoney"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransferMoney"]);
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmFastTransfers","lblTransfers"],15000);
  expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","lblTransfers"], "text")).toEqual("Transfers");
  appLog("Successfully Navigated to FastTransfers Screen");
}

async function SelectFromAccount(fromAcc){

  appLog("Intiated method to Select From Account");
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmFastTransfers","txtTransferFrom"],15000);
  kony.automation.widget.touch(["frmFastTransfers","txtTransferFrom"], [230,25],null,null);
  kony.automation.textbox.enterText(["frmFastTransfers","txtTransferFrom"],fromAcc);
  appLog("Successfully Entered From Account");
  await kony.automation.playback.wait(5000);
  kony.automation.flexcontainer.click(["frmFastTransfers","segTransferFrom[0,0]","flxAmount"]);
  appLog("Successfully Selected From Account from List");
}

async function SelectToAccount(ToAccReciptent){

  appLog("Intiated method to Select To Account :: <b>"+ToAccReciptent+"</b>");

  await kony.automation.playback.waitFor(["frmFastTransfers","txtTransferTo"],15000);
  kony.automation.widget.touch(["frmFastTransfers","txtTransferTo"], [72,9],null,null);

  //   if(ToAccReciptent==='OwnAcc'){
  //     kony.automation.textbox.enterText(["frmFastTransfers","txtTransferTo"],"Saving");
  //     appLog("Successfully Entered Default To Account : ");
  //     await kony.automation.playback.wait(5000);
  //     expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","segTransferTo[0,0]","lblAccountName"], "text")).not.toBe('');
  //   }else{
  //     kony.automation.textbox.enterText(["frmFastTransfers","txtTransferTo"],ToAccReciptent);
  //     appLog("Successfully Entered To Account : <b>"+ToAccReciptent+"</b>");
  //     await kony.automation.playback.wait(5000);
  //     expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","segTransferTo[0,0]","lblAccountName"], "text")).not.toBe('');
  //   }

  kony.automation.textbox.enterText(["frmFastTransfers","txtTransferTo"],ToAccReciptent);
  appLog("Successfully Entered To Account : <b>"+ToAccReciptent+"</b>");
  await kony.automation.playback.wait(5000);
  expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","segTransferTo[0,0]","lblAccountName"], "text")).not.toBe('');

  kony.automation.flexcontainer.click(["frmFastTransfers","segTransferTo[0,0]","flxAmount"]);
  appLog("Successfully Selected To Account from List");

}

async function EnterAmount(amountValue) {

  await kony.automation.playback.waitFor(["frmFastTransfers","tbxAmount"],15000);
  kony.automation.textbox.enterText(["frmFastTransfers","tbxAmount"],amountValue);
  appLog("Successfully Entered Amount as : <b>"+amountValue+"</b>");
  await kony.automation.scrollToWidget(["frmFastTransfers","customfooternew","btnFaqs"]);
}

async function SelectFrequency(freqValue) {

  kony.automation.flexcontainer.click(["frmFastTransfers","flxContainer4"]);
  kony.automation.listbox.selectItem(["frmFastTransfers","lbxFrequency"], freqValue);
  appLog("Successfully Selected Freq as : <b>"+freqValue+"</b>");
}

async function SelectDateRange() {

  kony.automation.calendar.selectDate(["frmFastTransfers","calSendOnNew"], [10,25,2021]);
  kony.automation.calendar.selectDate(["frmFastTransfers","calEndingOnNew"], [11,25,2021]);
  appLog("Successfully Selected DateRange");
}

async function SelectSendOnDate() {

  kony.automation.calendar.selectDate(["frmFastTransfers","calSendOnNew"], [10,25,2021]);
  appLog("Successfully Selected SendOn Date");
}

async function SelectOccurences(occurences) {

  kony.automation.listbox.selectItem(["frmFastTransfers","lbxForHowLong"], "NO_OF_RECURRENCES");
  kony.automation.textbox.enterText(["frmFastTransfers","tbxNoOfRecurrences"],occurences);
  appLog("Successfully Selected Occurences as <b>"+occurences+"</b>");
  kony.automation.calendar.selectDate(["frmFastTransfers","calSendOnNew"], [10,25,2021]);
  appLog("Successfully Selected SendOn Date");
}
async function EnterNoteValue(notes) {

  await kony.automation.playback.waitFor(["frmFastTransfers","txtNotes"],10000);
  kony.automation.textbox.enterText(["frmFastTransfers","txtNotes"],notes);
  appLog("Successfully entered Note value as : <b>"+notes+"</b>");
  await kony.automation.playback.waitFor(["frmFastTransfers","btnConfirm"],10000);
  await kony.automation.scrollToWidget(["frmFastTransfers","btnConfirm"]);
  kony.automation.button.click(["frmFastTransfers","btnConfirm"]);
  appLog("Successfully Clicked on Continue Button");
}

async function ConfirmTransfer() {

  appLog("Intiated method to Confirm Transfer Details");

  await kony.automation.playback.waitFor(["frmReview","btnConfirm"],15000);
  kony.automation.button.click(["frmReview","btnConfirm"]);
  appLog("Successfully Clicked on Confirm Button");

}

async function VerifyTransferSuccessMessage() {

  appLog("Intiated method to Verify Transfer SuccessMessage ");

  await kony.automation.playback.wait(5000);
  var success=await kony.automation.playback.waitFor(["frmConfirmTransfer"],30000);

  if(success){
    //expect(kony.automation.widget.getWidgetProperty(["frmConfirmTransfer","lblTransactionMessage"],"text")).toContain("We successfully");
    //   expect(kony.automation.widget.getWidgetProperty(["frmConfirmTransfer","lblTransactionMessage"],"text")).not.toBe('');
    //   await kony.automation.playback.waitFor(["frmConfirmTransfer","btnSavePayee"],15000);
    //   kony.automation.button.click(["frmConfirmTransfer","btnSavePayee"]);
    await kony.automation.playback.waitFor(["frmConfirmTransfer","customheadernew","flxAccounts"],15000);
    kony.automation.flexcontainer.click(["frmConfirmTransfer","customheadernew","flxAccounts"]);
    appLog("Successfully Clicked on Accounts Button");
  }else if(await kony.automation.playback.waitFor(["frmFastTransfers","rtxMakeTransferError"],5000)){
    //expect(kony.automation.widget.getWidgetProperty(["frmFastTransfers","rtxMakeTransferError"], "text")).toEqual("Transaction cannot be executed. Update your organization's approval matrix and re-submit the transaction.");
    appLog("Failed with : rtxMakeTransferError");
    fail("Failed with : rtxMakeTransferError");

    await MoveBackToLandingScreen_Transfers();

  }else{

    // This is the condition for use cases where it won't throw error on UI but struck at same screen
    appLog("Unable to perform Successfull Transcation");
    fail("Unable to perform Successfull Transcation");
  }

}

async function CancelTransfer() {

  //await kony.automation.playback.wait(5000);

  await kony.automation.playback.waitFor(["frmReview","btnCancel"],15000);
  kony.automation.button.click(["frmReview","btnCancel"]);
  appLog("Successfully Clicked on CANCEL Button");
  await kony.automation.playback.waitFor(["frmReview","CustomPopup","btnYes"],15000);
  kony.automation.button.click(["frmReview","CustomPopup","btnYes"]);
  appLog("Successfully Clicked on YES Button");
}

async function navigateToTransferActivities(){

  appLog("Intiated method to navigate to Transfer Activities");

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxPayBills"],15000);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxPayBills"]);
  await kony.automation.playback.wait(5000);
  appLog("Successfully Navigated to TransferActivities");
}

async function navigateToPastTransfersTab(){

  appLog("Intiated method to navigate to PastTransfer Tab");
  await kony.automation.playback.waitFor(["frmFastTransfersActivites","btnRecent"],15000);
  kony.automation.button.click(["frmFastTransfersActivites","btnRecent"]);
  await kony.automation.playback.wait(5000);
  appLog("Successfully navigated to PastTransfer Tab");
}

async function verifyRepeatTransferFunctionality(note){

  appLog("Intiated method verify Repeat Transfer Functionality");

  var noTransfers=await kony.automation.playback.waitFor(["frmFastTransfersActivites","rtxNoPaymentMessage"],10000);

  if(noTransfers){

    appLog('There are No Transactions Found');
    await kony.automation.playback.waitFor(["frmFastTransfersActivites","customheadernew","flxAccounts"],15000);
    kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
    await kony.automation.playback.wait(5000); 
    appLog("Successfully Moved back to Accounts dashboard");
  }else{

    //await kony.automation.playback.waitFor(["frmFastTransfersActivites","segmentTransfers"],15000);
    var noReapeatBtn= await kony.automation.playback.waitFor(["frmFastTransfersActivites","segmentTransfers[0]","btnAction"],10000);

    if(noReapeatBtn){
      appLog('Intiating Repeat Transfer');
      kony.automation.button.click(["frmFastTransfersActivites","segmentTransfers[0]","btnAction"]);
      await kony.automation.playback.wait(5000);
      appLog("Successfully Clicked on Repeat Button");
      await EnterNoteValue(note);
      await ConfirmTransfer();
      await VerifyTransferSuccessMessage();
    }else{
      appLog('No Repeat Transfers available');
      await kony.automation.playback.waitFor(["frmFastTransfersActivites","customheadernew","flxAccounts"],15000);
      kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
      await kony.automation.playback.wait(5000); 
      appLog("Successfully Moved back to Accounts dashboard");
    }
  }


}

async function VerifyTranxUnderActivities(){

  appLog("Intiated method verify Transfer under Activities");

  var noTransfers=await kony.automation.playback.waitFor(["frmFastTransfersActivites","rtxNoPaymentMessage"],15000);

  if(noTransfers){
    appLog('There are No Transactions Found');
    await kony.automation.playback.waitFor(["frmFastTransfersActivites","customheadernew","flxAccounts"],15000);
    kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
    await kony.automation.playback.wait(5000);
    await verifyAccountsLandingScreen();
    appLog("Successfully Moved back to Accounts dashboard");
  }else{

    //await kony.automation.playback.waitFor(["frmFastTransfersActivites","segmentTransfers"],15000);
    var noTranxBtn= await kony.automation.playback.waitFor(["frmFastTransfersActivites","segmentTransfers[0]","flxDropdown"],10000);
    if(noTranxBtn){

      appLog('Intiating to verify Transfer Activity');
      await kony.automation.playback.waitFor(["frmFastTransfersActivites","segmentTransfers"],15000);
      kony.automation.flexcontainer.click(["frmFastTransfersActivites","segmentTransfers[0]","flxDropdown"]);
      appLog("Successfully Clicked on first Sheduled Transfer");

      //No garuntee that same note will be there, other users also will perform Tranx
      //expect(kony.automation.widget.getWidgetProperty(["frmFastTransfersActivites","segmentTransfers[0]","flxFastPastTransfersSelected","lblNotesValue1"],"text")).toEqual(notevalue);

      await kony.automation.playback.waitFor(["frmFastTransfersActivites","customheadernew","flxAccounts"],15000);
      kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
      await verifyAccountsLandingScreen();
      appLog("Successfully Moved back to Accounts dashboard");
    }else{

      appLog('No Transfers activities available');
      await kony.automation.playback.waitFor(["frmFastTransfersActivites","customheadernew","flxAccounts"],15000);
      kony.automation.flexcontainer.click(["frmFastTransfersActivites","customheadernew","flxAccounts"]);
      await kony.automation.playback.wait(5000); 
      appLog("Successfully Moved back to Accounts dashboard");
    }

  }
}

async function MoveBackToLandingScreen_Transfers(){

  //Move back to landing Screen
  appLog("Intiated method to move from frmFastTransfers screen");
  await kony.automation.playback.waitFor(["frmFastTransfers","customheadernew","flxAccounts"],15000);
  kony.automation.flexcontainer.click(["frmFastTransfers","customheadernew","flxAccounts"]);
  appLog("Successfully Moved back to Accounts dashboard");
}
