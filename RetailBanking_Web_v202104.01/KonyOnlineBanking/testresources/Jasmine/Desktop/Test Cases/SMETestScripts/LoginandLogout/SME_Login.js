it("SME_202007_Login", async function() {
	
  //var userName='testuat52';
  var userName=LoginDetails.username;
  //var userName='adithyasai.kovuru';
  var passWord=LoginDetails.password;
  
  kony.print('Inside Login TestCase');

 // Check for MayBeLater
  await verifyMayBeLater();
  
  await verifyLoginFunctionality(userName,passWord);
  
  //Check for terms and conditions
  await verifyTermsandConditions();
  
  await verifyAccountsLandingScreen();
  
},120000);