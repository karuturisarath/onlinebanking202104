it("CustomRole_TS007_AcknowledgementScreen", async function() {

  //Scenario 7 :  Custome Roles - Verify creation of custom role from create user - Acknowledgement screen

  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.wait(5000);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.wait(2000);
  await kony.automation.scrollToWidget(["frmBBAccountsLanding","customheader","customhamburger","UserManagementflxAccountsMenu"]);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","UserManagementflxAccountsMenu"]);
  await kony.automation.playback.wait(2000);
  await kony.automation.scrollToWidget(["frmBBAccountsLanding","customheader","customhamburger","UserManagement3flxMyAccounts"]);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","UserManagement3flxMyAccounts"]);
  await kony.automation.playback.waitFor(["frmPermissionsTemplate","flxCopy"]);
  kony.automation.flexcontainer.click(["frmPermissionsTemplate","flxCopy"]);
  await kony.automation.playback.wait(3000);

  var today = new Date();
  var date = today.getFullYear()+""+(today.getMonth()+1)+""+today.getDate();
  var time = today.getHours() + "" + today.getMinutes() + "" + today.getSeconds();

  kony.automation.textbox.enterText(["frmPermissionsTemplate","tbxRoleName"],"SMEROLE"+date+time);
  await kony.automation.playback.wait(2000);
  kony.automation.button.click(["frmPermissionsTemplate","btnCheckAvailability"]);
  await kony.automation.playback.wait(5000);
  kony.automation.flexcontainer.click(["frmPermissionsTemplate","flxUsers"]);
  await kony.automation.playback.waitFor(["frmPermissionsTemplate","flxExistingUsersExpand"]);
  kony.automation.flexcontainer.click(["frmPermissionsTemplate","flxExistingUsersExpand"]);
  await kony.automation.playback.waitFor(["frmPermissionsTemplate","segUserNames"]);
  kony.automation.flexcontainer.click(["frmPermissionsTemplate","segUserNames[0]","flxSelectRole"]);
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmPermissionsTemplate","btnProceedRoles"]);
  await kony.automation.scrollToWidget(["frmPermissionsTemplate","btnProceedRoles"]);
  kony.automation.button.click(["frmPermissionsTemplate","btnProceedRoles"]);
  await kony.automation.playback.waitFor(["frmPermissionsTemplate","btnSaveAndProceed"]);
  await kony.automation.scrollToWidget(["frmPermissionsTemplate","btnSaveAndProceed"]);
  kony.automation.button.click(["frmPermissionsTemplate","btnSaveAndProceed"]);
  await kony.automation.playback.waitFor(["frmPermissionsTemplate","btnCancelAddServices"]);
  kony.automation.button.click(["frmPermissionsTemplate","btnCancelAddServices"]);

  // Verifying the Custom Role Creation from Acknowledgement Screen
 
  kony.automation.textbox.enterText(["frmPermissionsTemplate","tbxRoleName"],"SMEROLE"+time+date);
  await kony.automation.playback.wait(2000);
  kony.automation.button.click(["frmPermissionsTemplate","btnCheckAvailability"]);
  await kony.automation.playback.wait(5000);
  kony.automation.flexcontainer.click(["frmPermissionsTemplate","flxUsers"]);
  await kony.automation.playback.waitFor(["frmPermissionsTemplate","flxExistingUsersExpand"]);
  await kony.automation.playback.wait(3000);
  kony.automation.flexcontainer.click(["frmPermissionsTemplate","flxExistingUsersExpand"]);
  await kony.automation.playback.waitFor(["frmPermissionsTemplate","segUserNames"]);
  await kony.automation.playback.wait(2000);
  kony.automation.flexcontainer.click(["frmPermissionsTemplate","segUserNames[0]","flxSelectRole"]);
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmPermissionsTemplate","btnProceedRoles"]);
  await kony.automation.scrollToWidget(["frmPermissionsTemplate","btnProceedRoles"]);
  await kony.automation.playback.wait(2000);
  kony.automation.button.click(["frmPermissionsTemplate","btnProceedRoles"]);
  await kony.automation.playback.waitFor(["frmPermissionsTemplate","btnSaveAndProceed"]);
  await kony.automation.scrollToWidget(["frmPermissionsTemplate","btnSaveAndProceed"]);
  await kony.automation.playback.wait(2000);
  kony.automation.button.click(["frmPermissionsTemplate","btnSaveAndProceed"]);
  await kony.automation.playback.waitFor(["frmPermissionsTemplate","btnCancelAddServices"]);
  await kony.automation.playback.wait(5000);
  kony.automation.flexcontainer.click(["frmPermissionsTemplate","customheader","topmenu","flxaccounts"]);
  await kony.automation.playback.wait(5000);
},100000);