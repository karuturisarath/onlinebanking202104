it("CustomRole_TS005_SearchCustomRole", async function() {

  //Scenario 5 : Custom Roles - Verify search functionality in custom role

  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.wait(3000);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","customhamburger","UserManagementflxAccountsMenu"]);
  await kony.automation.scrollToWidget(["frmBBAccountsLanding","customheader","customhamburger","UserManagementflxAccountsMenu"]);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","UserManagementflxAccountsMenu"]);
  await kony.automation.playback.wait(2000);
  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","customhamburger","UserManagement1flxMyAccounts"]);
  await kony.automation.scrollToWidget(["frmBBAccountsLanding","customheader","customhamburger","UserManagement1flxMyAccounts"]);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","UserManagement1flxMyAccounts"]);
  await kony.automation.playback.waitFor(["frmBBUsersDashboard","TabPane","TabSearchBarNew","tbxSearch"]);
  await kony.automation.playback.wait(3000);
  kony.automation.textbox.enterText(["frmBBUsersDashboard","TabPane","TabSearchBarNew","tbxSearch"],"SME");
  await kony.automation.playback.wait(3000);
  kony.automation.widget.touch(["frmBBUsersDashboard"], null,null,[129,294]);
  await kony.automation.playback.wait(3000);
  kony.automation.flexcontainer.click(["frmBBUsersDashboard","TabPane","TabSearchBarNew","flxSearchimg"]);
  await kony.automation.playback.wait(3000);
  kony.automation.widget.touch(["frmBBUsersDashboard"], null,null,[130,294]);
  await kony.automation.playback.wait(3000);
  kony.automation.flexcontainer.click(["frmBBUsersDashboard","TabPane","TabSearchBarNew","flxSearchimg"]);
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmBBUsersDashboard","TabPane","TabSearchBarNew","tbxSearch"]);
  await kony.automation.playback.wait(3000);
  kony.automation.textbox.enterText(["frmBBUsersDashboard","TabPane","TabSearchBarNew","tbxSearch"],"CROLE");
  await kony.automation.playback.wait(3000);
  kony.automation.widget.touch(["frmBBUsersDashboard"], null,null,[129,294]);
  await kony.automation.playback.wait(3000);
  kony.automation.flexcontainer.click(["frmBBUsersDashboard","TabPane","TabSearchBarNew","flxSearchimg"]);
  await kony.automation.playback.wait(3000);
  kony.automation.widget.touch(["frmBBUsersDashboard"], null,null,[130,294]);
  await kony.automation.playback.wait(3000);
  kony.automation.flexcontainer.click(["frmBBUsersDashboard","TabPane","TabSearchBarNew","flxSearchimg"]);
  await kony.automation.playback.wait(10000);
  kony.automation.flexcontainer.click(["frmBBUsersDashboard","customheader","topmenu","flxaccounts"]);
},80000);