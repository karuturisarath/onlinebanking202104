it("CustomRole_TS002_VerifyListofCustomRoles", async function() {

  //Scenario 2  : Custom Roles - Verify list of users & custom roles in user roles tab

  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.wait(3000);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","customhamburger","UserManagementflxAccountsCollapse"]);
  await kony.automation.scrollToWidget(["frmBBAccountsLanding","customheader","customhamburger","UserManagementflxAccountsCollapse"]);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","UserManagementflxAccountsCollapse"]);
  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","customhamburger","UserManagement1flxMyAccounts"]);
  await kony.automation.scrollToWidget(["frmBBAccountsLanding","customheader","customhamburger","UserManagement1flxMyAccounts"]);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","UserManagement1flxMyAccounts"]);
  await kony.automation.playback.wait(3000);
  await kony.automation.playback.waitFor(["frmBBUsersDashboard","TabPane","TabsHeaderNew","btnTab1"]);
  kony.automation.button.click(["frmBBUsersDashboard","TabPane","TabsHeaderNew","btnTab1"]);
  await kony.automation.playback.wait(3000);
  kony.automation.button.click(["frmBBUsersDashboard","TabPane","TabsHeaderNew","btnTab2"]);
  kony.automation.flexcontainer.click(["frmBBUsersDashboard","customheader","topmenu","flxaccounts"]);
  await kony.automation.playback.wait(5000);
},30000);