it("CustomRole_TS003_UserCreationWithCustomRole", async function() {

  //Scenario 3 : Custom Roles - Verify creation of user by copying permissions from custom role

  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.wait(3000);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","customhamburger","UserManagementflxAccountsMenu"]);
  await kony.automation.scrollToWidget(["frmBBAccountsLanding","customheader","customhamburger","UserManagementflxAccountsMenu"]);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","UserManagementflxAccountsMenu"]);
  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","customhamburger","UserManagement2flxMyAccounts"]);
  await kony.automation.scrollToWidget(["frmBBAccountsLanding","customheader","customhamburger","UserManagement2flxMyAccounts"]);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","UserManagement2flxMyAccounts"]);
  await kony.automation.playback.waitFor(["frmUserManagement","tbxName"]);
  await kony.automation.playback.wait(3000);
  kony.automation.textbox.enterText(["frmUserManagement","tbxName"],"Custom");
  await kony.automation.playback.wait(1000);
  kony.automation.textbox.enterText(["frmUserManagement","tbLastName"],"User");

  var today = new Date();
  var date = today.getFullYear()+""+(today.getMonth()+1)+""+today.getDate();
  var time = today.getHours() + "" + today.getMinutes() + "" + today.getSeconds();

  await kony.automation.playback.wait(1000);
  kony.automation.textbox.enterText(["frmUserManagement","CustomDate","tbxDateInputKA"],"01/01/1980");
  await kony.automation.playback.wait(1000);
  kony.automation.textbox.enterText(["frmUserManagement","tbxEmail"],"temenos@temenos.com");
  kony.automation.textbox.enterText(["frmUserManagement","tbxPhoneNum"],"123456789");
  await kony.automation.playback.wait(1000);
  kony.automation.textbox.enterText(["frmUserManagement","tbxSSN"],date+time);
  await kony.automation.playback.wait(1000);
  await kony.automation.scrollToWidget(["frmUserManagement","tbxDriversLicense"]);
  kony.automation.textbox.enterText(["frmUserManagement","tbxDriversLicense"],time);
  await kony.automation.playback.wait(1000);
  await kony.automation.scrollToWidget(["frmUserManagement","tbxUsername"]);
  kony.automation.textbox.enterText(["frmUserManagement","tbxUsername"],"CUSER@"+date+time);
  await kony.automation.playback.wait(1000);
  kony.automation.button.click(["frmUserManagement","btnCheckAvailability"]);
  await kony.automation.playback.wait(7000);
  await kony.automation.playback.waitFor(["frmUserManagement","btnProceedCreate"]);
  await kony.automation.scrollToWidget(["frmUserManagement","btnProceedCreate"]);
  kony.automation.button.click(["frmUserManagement","btnProceedCreate"]);
  await kony.automation.playback.waitFor(["frmUserManagement","flxCustomRoleExpand"]);
  kony.automation.flexcontainer.click(["frmUserManagement","flxCustomRoleExpand"]);
  await kony.automation.playback.wait(5000);
  kony.automation.flexcontainer.click(["frmUserManagement","segRoleNames[0]","flxSelectRole"]);
  await kony.automation.playback.wait(5000);
  await kony.automation.scrollToWidget(["frmUserManagement","btnProceedRoles"]);
  kony.automation.button.click(["frmUserManagement","btnProceedRoles"]);
  await kony.automation.playback.waitFor(["frmUserManagement","btnSaveAndProceed"]);
  await kony.automation.scrollToWidget(["frmUserManagement","btnSaveAndProceed"]);
  kony.automation.button.click(["frmUserManagement","btnSaveAndProceed"]);
  await kony.automation.playback.waitFor(["frmUserManagement","customheader","topmenu","flxaccounts"]);
  await kony.automation.playback.wait(5000);
  kony.automation.flexcontainer.click(["frmUserManagement","customheader","topmenu","flxaccounts"]);
  await kony.automation.playback.wait(5000);
},60000);