it("VerifyUpdatingNewEmailID", async function() {
  
  var updatedemailid=Settings.email.updatedemailid;
  
  await NavigateToProfileSettings();
  await selectProfileSettings_EmailAddress();
  await ProfileSettings_UpdateEmailAddress(updatedemailid);
  await MoveBackToDashBoard_ProfileManagement();
  await verifyAccountsLandingScreen();

//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMenu"]);

//   await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","SettingsflxAccountsMenu"]);

//   await kony.automation.playback.waitFor(["frmDashboard","customheader","customhamburger","Settings0flxMyAccounts"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","customhamburger","Settings0flxMyAccounts"]);

//   await kony.automation.playback.wait(5000);
//   kony.automation.flexcontainer.click(["frmProfileManagement","settings","lblPersonalDetailsHeading"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblPersonalDetailsHeading"], "text")).toEqual("Personal Details");


//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","flxEmail"]);
//   kony.automation.flexcontainer.click(["frmProfileManagement","settings","flxEmail"]);

//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblEmailHeading"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblEmailHeading"], "text")).toEqual("Email");

//   // Add new email ID
//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnAddNewEmail"]);
//   kony.automation.button.click(["frmProfileManagement","settings","btnAddNewEmail"]);
//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblAddNewEmailHeading"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","lblAddNewEmailHeading"], "text")).toEqual("Add New Email");
//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxEmailId"]);
//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","lblMarkAsPrimaryEmailCheckBox"]);
//   kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxEmailId"],"testautomation@dbx.com");
//   kony.automation.button.click(["frmProfileManagement","settings","btnAddEmailIdAdd"]);
//   await kony.automation.playback.wait(5000);

//   // Update email ID
//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds"]);
//   var accounts_Size1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds"],"data");

//   var segLength1=accounts_Size1.length;
//   for(var x = 0; x <segLength1; x++) {
//     var seg="segEmailIds["+x+"]";
//     await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg,"flxProfileManagementEmail","lblEmail"]);
//     var email=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg,"flxProfileManagementEmail","lblEmail"], "text");

//     if(email==="testautomation@dbx.com"){

//       await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds"]);
//       kony.automation.button.click(["frmProfileManagement","settings",seg,"btnEdit"]);
//       await kony.automation.playback.waitFor(["frmProfileManagement","settings","tbxEditEmailId"]);
//       kony.automation.textbox.enterText(["frmProfileManagement","settings","tbxEditEmailId"],"testautomation123@dbx.com");
//       await kony.automation.playback.waitFor(["frmProfileManagement","settings","btnEditEmailIdSave"]);
//       kony.automation.button.click(["frmProfileManagement","settings","btnEditEmailIdSave"]);
//       await kony.automation.playback.wait(5000);
//       break;
//     }
//   }


//   // Delete added email
//   await kony.automation.playback.waitFor(["frmProfileManagement","settings","segEmailIds"]);
//   var accounts_Size=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings","segEmailIds"],"data");

//   var segLength=accounts_Size.length;
//   for(var y = 0; y <segLength; y++) {
//     var seg1="segEmailIds["+y+"]";
//     await kony.automation.playback.waitFor(["frmProfileManagement","settings",seg1,"flxProfileManagementEmail","lblEmail"]);
//     var email1=kony.automation.widget.getWidgetProperty(["frmProfileManagement","settings",seg1,"flxProfileManagementEmail","lblEmail"], "text");

//     if(email1==="testautomation123@dbx.com"){

//       kony.automation.button.click(["frmProfileManagement","settings",seg1,"btnDelete"]);
//       await kony.automation.playback.waitFor(["frmProfileManagement","btnDeleteYes"]);
//       kony.automation.button.click(["frmProfileManagement","btnDeleteYes"]);
//       await kony.automation.playback.wait(5000);
//       break;
//     }
//   }


//   // Move back to base state
//   await kony.automation.playback.waitFor(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
//   kony.automation.flexcontainer.click(["frmProfileManagement","customheader","topmenu","flxaccounts"]);
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");


},120000);