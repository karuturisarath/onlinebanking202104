it("DeleteExternalBankRecipitent", async function() {

 // Add a recipitent and Then delete the same recipitent
  
  var Routingno=ManageRecipients.externalAccount.Routingno;
  var Accno="0"+new Date().getTime();
  var unique_RecipitentName=ManageRecipients.externalAccount.unique_RecipitentName+getRandomString(5);
  
  await NavigateToManageRecipitents();
  await clickonAddExternalAccounttab();
  await enterExternalBankAccountDetails(Routingno,Accno,unique_RecipitentName);
  await verifyAddingNewReciptientSuccessMsg();
  await verifyAccountsLandingScreen();
  
  //Delete Added Recipitent
  
  await NavigateToManageRecipitents();
  await clickOnExternalRecipitentsTab();
  await SearchforPayee_External(unique_RecipitentName);
  await DeleteReciptent();
  await MoveBacktoDashboard_ManageRecipitent();
  
//   var unique_RecipitentName="TestExtAcc_"+new Date().getTime();
  
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");

//   await kony.automation.playback.waitFor(["frmFastManagePayee","flxAddKonyAccount"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","flxAddKonyAccount"]);
//   await kony.automation.playback.wait(5000);
  
//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","tbxRoutingNumberKA"]);
//   kony.automation.textbox.enterText(["frmFastAddExternalAccount","tbxRoutingNumberKA"],"1234567890");
//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","tbxAccountNumberKA"]);
//   kony.automation.textbox.enterText(["frmFastAddExternalAccount","tbxAccountNumberKA"],"1234567890");
//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","tbxAccountNumberAgainKA"]);
//   kony.automation.textbox.enterText(["frmFastAddExternalAccount","tbxAccountNumberAgainKA"],"1234567890");
//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","tbxBeneficiaryNameKA"]);
//   kony.automation.textbox.enterText(["frmFastAddExternalAccount","tbxBeneficiaryNameKA"],unique_RecipitentName);
//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","tbxAccountNickNameKA"]);
//   kony.automation.textbox.enterText(["frmFastAddExternalAccount","tbxAccountNickNameKA"],unique_RecipitentName);
//   await kony.automation.playback.waitFor(["frmFastAddExternalAccount","btnAddAccountKA"]);
//   kony.automation.button.click(["frmFastAddExternalAccount","btnAddAccountKA"]);
//   await kony.automation.playback.waitFor(["frmFastAddExternalAccountConfirm","btnConfirm"]);
//   kony.automation.button.click(["frmFastAddExternalAccountConfirm","btnConfirm"]);

//   await kony.automation.playback.waitFor(["frmFastAddExternalAccountAcknowledgement","lblSuccessMessage"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastAddExternalAccountAcknowledgement","lblSuccessMessage"],"text")).toContain("has been added.");

//   await kony.automation.playback.waitFor(["frmFastAddExternalAccountAcknowledgement","customheadernew","flxAccounts"]);
//   kony.automation.flexcontainer.click(["frmFastAddExternalAccountAcknowledgement","customheadernew","flxAccounts"]);

//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
  
//    //Delete Ext Bank Recipitent
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxTransfersAndPay"]);
//   await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxSendMoney"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","lblManageRecipients"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","lblManageRecipients"],"text")).toEqual("Manage Recipients");
//   await kony.automation.playback.waitFor(["frmFastManagePayee","btnExternalAccounts"]);
//   kony.automation.button.click(["frmFastManagePayee","btnExternalAccounts"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","txtSearch"]);
//   kony.automation.textbox.enterText(["frmFastManagePayee","Search","txtSearch"],unique_RecipitentName);
//   await kony.automation.playback.waitFor(["frmFastManagePayee","Search","btnConfirm"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","Search","btnConfirm"]);
//   await kony.automation.playback.wait(5000);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","segmentTransfers[0]","flxDropdown"]);
//   await kony.automation.playback.waitFor(["frmFastManagePayee","segmentTransfers"]);
//   kony.automation.button.click(["frmFastManagePayee","segmentTransfers[0]","btnRemoveRecipient"]);

//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup","lblPopupMessage"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmFastManagePayee","CustomPopup","lblPopupMessage"],"text")).toEqual("Are you sure you want to delete this account?");

//   await kony.automation.playback.waitFor(["frmFastManagePayee","CustomPopup","btnYes"]);
//   kony.automation.button.click(["frmFastManagePayee","CustomPopup","btnYes"]);
//   await kony.automation.playback.wait(5000);
  
//   var error= await kony.automation.playback.waitFor(["frmFastManagePayee","rtxMakeTransferErro"],5000);
  
//   if(error){
//     fail("There was a technical delay. Please try again.");
//   }

//   await kony.automation.playback.waitFor(["frmFastManagePayee","customheadernew","flxAccounts"]);
//   kony.automation.flexcontainer.click(["frmFastManagePayee","customheadernew","flxAccounts"]);
//   await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
//   expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

},120000);