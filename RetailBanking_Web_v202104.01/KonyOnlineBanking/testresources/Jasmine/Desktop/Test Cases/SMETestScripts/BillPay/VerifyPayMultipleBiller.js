it("VerifyPayMultipleBiller", async function() {

  await kony.automation.playback.waitFor(["frmDashboard","lblShowing"]);
  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");
  
  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMyBills"]);
  kony.automation.flexcontainer.click(["frmDashboard","customheader","topmenu","flxMyBills"]);

  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmBulkPayees","segmentBillpay"]);
  kony.automation.textbox.enterText(["frmBulkPayees","segmentBillpay[0]","txtAmount"],"1");
  kony.automation.textbox.enterText(["frmBulkPayees","segmentBillpay[1]","txtAmount"],"2");

  await kony.automation.playback.waitFor(["frmBulkPayees","flxContent"]);
  kony.automation.flexcontainer.click(["frmBulkPayees","flxContent"]);

  await kony.automation.playback.waitFor(["frmBulkPayees","btnBulkConfirm"]);
  kony.automation.button.click(["frmBulkPayees","btnBulkConfirm"]);

  await kony.automation.playback.waitFor(["frmBulkBillPayConfirm","lblAddPayee"]);
  expect(kony.automation.widget.getWidgetProperty(["frmBulkBillPayConfirm","lblAddPayee"], "text")).toEqual("Confirm My Bills");

  await kony.automation.playback.waitFor(["frmBulkBillPayConfirm","lblCheckBoxIcon"]);
  kony.automation.widget.touch(["frmBulkBillPayConfirm","lblCheckBoxIcon"], null,null,[20,11]);
  kony.automation.flexcontainer.click(["frmBulkBillPayConfirm","flxCheckBoxTnC"]);

  await kony.automation.playback.waitFor(["frmBulkBillPayConfirm","btnConfirm"]);
  kony.automation.button.click(["frmBulkBillPayConfirm","btnConfirm"]);

  await kony.automation.playback.waitFor(["frmBulkBillPayAcknowledgement","lblBulkBillPayAcknowledgement"]);
  expect(kony.automation.widget.getWidgetProperty(["frmBulkBillPayAcknowledgement","lblBulkBillPayAcknowledgement"], "text")).toEqual("My Bills - Acknowledgment");

  //Move back to base state
  await kony.automation.playback.waitFor(["frmBulkBillPayAcknowledgement","customheadernew","flxAccounts"]);
  kony.automation.flexcontainer.click(["frmBulkBillPayAcknowledgement","customheadernew","flxAccounts"]);

  await kony.automation.playback.waitFor(["frmDashboard","customheader","topmenu","flxMenu"]);
  expect(kony.automation.widget.getWidgetProperty(["frmDashboard","lblShowing"], "text")).toContain("Show");

},120000);