var LoginDetails={
  //username:'0407828283',
  username:'testeu04',
  password:'Kony@1234'
};

var TimeOuts={

  Accounts : {
    Timeout : "180000",
  },
  CustomView : {
    Timeout : "120000",
  },
  ManageBenefeciary : {

    AddBenefeciary : "180000",
    EditBenefeciary : "180000",
    DeleteBenefeciary: "120000",
    PaymentBenefeciary: "180000",
    SeachBenefeciary: "120000"
  },
  DomesticPayments : {

    Payment : "180000",
    OneTimepay : "180000"
  },
  InternationalPayments : {

    Payment : "180000",
    OneTimepay : "180000"
  },
  SameBankPayments : {

    Payment : "180000",
    OneTimepay : "180000"
  },
  OwnPayments : {

    Payment : "180000",
    OneTimepay : "180000"
  },
  TrasferActivities : {

    PaymentActivity : "180000",
    ActivitiesList : "180000",
    SearchActivities : "180000"
  },
  BillPay : {

    Payment : "180000"
  },  
  WireTransfers : {

    Payment : "180000",
    activateWireTransfer : "90000"
  },
  UnifiedTransfers : {
    AddNewAccount : "120000",
    Transfers : "120000",
  }

};

var AllAccounts={

  Checking : {

    accType : "Checking",
    accno : "106399",
    Shortaccno : "6399",

    "MenuOptions":
    [
      { "option": "Transfer Money From"},
      { "option": "Pay Money From"},
      { "option": "View Statements"},
      { "option": "Send Money"},
      { "option": "Pay a Bill from"},
      { "option": "Request Check Book"},
      { "option": "Stop Check Payment"},
      { "option": "My Checks"},
      { "option": "Manage Cards"},
      { "option": "Account Alerts"},
      { "option": "Set as favorite"}
    ]
  },
  Current : {

    accType : "Checking",
    accno : "118613",
    Shortaccno : "8613",

    "MenuOptions":
    [
      { "option": "Transfer Money From"},
      { "option": "Pay Money From"},
      { "option": "View Statements"},
      { "option": "Send Money"},
      { "option": "Pay a Bill from"},
      { "option": "Request Check Book"},
      { "option": "Stop Check Payment"},
      { "option": "My Checks"},
      { "option": "Manage Cards"},
      { "option": "Account Alerts"},
      { "option": "Set as favorite"}
    ]
  },
  Saving : {
    accType : "Savings",
    accno : "105139",
    Shortaccno : "5139",

    "MenuOptions":
    [
      { "option": "Transfer Money From"},
      { "option": "Pay Money From" },
      { "option": "View Statements" },
      { "option": "Request Check Book"},
      { "option": "Stop Check Payment" },
      { "option": "My Checks" },
      { "option": "Manage Cards"},
      { "option": "Account Alerts"},
      { "option": "Set as favorite"}
    ]
  },
  Deposit : {
    accType : "Deposit",
    accno : "108901",
    Shortaccno : "8901",

    "MenuOptions":
    [
      { "option": "View Statements" },
      { "option": "Update Account Settings" },
      { "option": "Account Alerts" }
    ]
  },
  Loan : {
    accType : "Loan",
    accno : "107425",
    Shortaccno : "7425",

    "MenuOptions":
    [
      { "option": "Pay Due Amount" },
      { "option": "View Statements" },
      { "option": "Update Account Settings" },
      { "option": "Account Alerts" },
      { "option": "Pay Off Loan" }
    ]
  }
};
var SearchTranscation={
  TransactionType:'Transfers',
  amountRange1:'1',
  amountRange2:'100',
  keyword:'Transfer',
  debitEntry:'Person-To-Person Debits',
  creditEntry:'Person-To-Person Credits'
};

var SearchBeneficiary={
  Name:'jasmine',
  AccNo:'10'
};

var SearchTransferActivities={
  Name:'jasmine',
  AccNo:'10'
};

var ManageBeneficiary={

  SameBank : {

    Nickname : "Samebankaccjasmine",
    Address1 : "Miyapur",
    Address2 : "alwyn",
    city : "Hyderabad",
    zipcode : "500055",
    updateCity :"Nellore",

    "AccountNumberList":
    [
      { "accno": "108812" },
      { "accno": "100048" },
      { "accno": "100037" },
      { "accno": "105635" }
    ]

  },
  Domestic : {

    BeneficiaryName : "Domesticbeneficiaryjasmine",
    Nickname : "Domesticaccjasmine",
    Address1 : "Miyapur",
    Address2 : "alwyn",
    city : "Hyderabad",
    zipcode : "500055",
    updateCity :"Nellore",

    "IBANList":
    [
      { "IBAN": "GB04BARC20474473160944" },
      { "IBAN": "GB04BARC20474473160944" },
      { "IBAN": "GB04BARC20474473160944" },
      { "IBAN": "GB04BARC20474473160944" },
      { "IBAN": "GB04BARC20474473160944" },
      { "IBAN": "GB04BARC20474473160944" },
      { "IBAN": "GB04BARC20474473160944" },
      { "IBAN": "GB04BARC20474473160944" }
    ]
  },
  International : {

    SWIFT : "UNBKILIT",
    BeneficiaryName : "Interbeneficiaryjasmine",
    Nickname : "Interaccjasmine",
    Address1 : "Miyapur",
    Address2 : "alwyn",
    city : "Hyderabad",
    zipcode : "500055",
    updateCity :"Nellore"
  },

};

var UTFManageBeneficiary={

  // Name should not be more than 20 chars
  SameBank : {
    BeneficiaryName : "SameBankbenjasmine",
    Nickname : "Samebankaccjasmine",
    Address1 : "Miyapur",
    Address2 : "alwyn",
    city : "Hyderabad",
    zipcode : "500055",
    updateCity :"Nellore",

    "AccountNumberList":
    [
      { "accno": "108812" },
      { "accno": "100048" },
      { "accno": "100037" },
      { "accno": "105635" }
    ]

  },
  Domestic : {

    BeneficiaryName : "Domesticbenjasmine",
    Nickname : "Domesticaccjasmine",
    Address1 : "Miyapur",
    Address2 : "alwyn",
    city : "Hyderabad",
    zipcode : "500055",
    updateCity :"Nellore",

    "IBANList":
    [
      { "IBAN": "GB04BARC20474473160944" },
      { "IBAN": "GB04BARC20474473160944" },
      { "IBAN": "GB04BARC20474473160944" },
      { "IBAN": "GB04BARC20474473160944" },
      { "IBAN": "GB04BARC20474473160944" },
      { "IBAN": "GB04BARC20474473160944" },
      { "IBAN": "GB04BARC20474473160944" },
      { "IBAN": "GB04BARC20474473160944" }
    ]
  },
  International : {

    SWIFT : "UNBKILIT",
    BeneficiaryName : "Interbenjasmine",
    Nickname : "Interaccjasmine",
    Address1 : "Miyapur",
    Address2 : "alwyn",
    city : "Hyderabad",
    zipcode : "500055",
    updateCity :"Nellore"
  },

};

var Payments={

  SameBank : {
    FromAcc : AllAccounts.Current.accno,
    ToAcc : ManageBeneficiary.SameBank.Nickname,
    Amount : "1.5"
  },
  Domestic : {
    FromAcc : AllAccounts.Current.accno,
    ToAcc : ManageBeneficiary.Domestic.Nickname,
    Amount : "1.5"
  },
  International : {
    FromAcc : AllAccounts.Current.accno,
    ToAcc : ManageBeneficiary.International.Nickname,
    Amount : "1.5"
  },
  OwnAcc : {
    FromAcc : AllAccounts.Current.accno,
    ToAcc : AllAccounts.Saving.accno,
    Amount : "1.5"
  }
};

var UTFPayments={

  SameBank : {
    FromAcc : AllAccounts.Current.Shortaccno,
    ToAcc : UTFManageBeneficiary.SameBank.BeneficiaryName,
    Amount : "1.5"
  },
  Domestic : {
    FromAcc : AllAccounts.Current.Shortaccno,
    ToAcc : UTFManageBeneficiary.Domestic.BeneficiaryName,
    Amount : "1.5"
  },
  International : {
    FromAcc : AllAccounts.Current.Shortaccno,
    ToAcc : UTFManageBeneficiary.International.BeneficiaryName,
    Amount : "1.5"
  },
  OwnAcc : {
    FromAcc : AllAccounts.Current.Shortaccno,
    ToAcc : AllAccounts.Saving.Shortaccno,
    Amount : "1.5"
  }
};

var OneTimePayment={

  SameBank : {
    FromAcc : AllAccounts.Current.accno,
    ToAcc : "SameBankOneTimePay",
    Amount : "1.5",

    "AccountNumberList":
    [
      { "accno": "108812" },
      { "accno": "100048" },
      { "accno": "100037" },
      { "accno": "105635" }
    ]

  },

  Domestic : {
    FromAcc : AllAccounts.Current.accno,
    ToAcc : "DomesticOneTimePay",
    Amount : "1.5",

    "IBANList":
    [
      { "IBAN": "GB04BARC20474473160944" },
      { "IBAN": "GB04BARC20474473160944" },
      { "IBAN": "GB04BARC20474473160944" },
      { "IBAN": "GB04BARC20474473160944" },
      { "IBAN": "GB04BARC20474473160944" },
      { "IBAN": "GB04BARC20474473160944" },
      { "IBAN": "GB04BARC20474473160944" },
      { "IBAN": "GB04BARC20474473160944" }
    ]
  },
  International : {
    FromAcc : AllAccounts.Current.accno,
    ToAcc : "InternationalOneTimePay",
    SWIFT : ManageBeneficiary.International.SWIFT,
    Amount : "1.5"
  }
};

var BillPay={

  oneTimePay : {
    payeeName : "A",
    zipcode : "500055",
    accno : "1234567890",
    accnoAgain : "1234567890",
    mobileno : "1234567890",
    amountValue : "1"
  },
  addPayee : {
    unique_RecipitentName : "ABCJasmine",
    address1 : "LR PALLI",
    address2 : "ATMAKUR",
    city : "NELLORE",
    zipcode : "500055"
  },
  deletePayee : {
    delete_RecipitentName : "Test Automation",
    address1 : "LR PALLI",
    address2 : "ATMAKUR",
    city : "NELLORE",
    zipcode : "500055"
  },
  schedulePay : {
    payeeName : "ABCJasmine",
    amountValue : "1.2"
  }
};

var Settings={

  address : {
    addressLine1 : "Miyapur",
    addressLine2 : "alwyn",
    zipcode : "500055",
    primaryAddressLine1 : "MiyapurPrimary",
    primaryAddressLine2 : "alwynPrimary",
    primaryZipcode : "500049",
    updatedZip : "500011"
  },
  phone : {
    phoneNumber : "888456",
    primaryPhoneNumber : "888456",
    updatedPhonenum : "1234567"

  },
  email : {
    emailAddress : "dbxjasmine",
    primaryEmailAddress : "dbxjasminePrimary",
    updatedemailid :"dbxjasmineUpdate"

  },
};

var WireTransers={

  DomesticRecipitentDetails: {

    BeneficiaryName : "Domesticbeneficiaryjasmine",
    Address1 : "Miyapur",
    Address2 : "alwyn",
    city : "Hyderabad",
    zipcode : "500055"
  },

  DomesticBankdetails : {

    RoutingNumber : "123",
    AccNumber : "123",
    Nickname : "Domesticjasmine",
    BankName : "SBI",
    BankAddressLine1 : "Miyapur",
    BankAddressLine2 : "Alwyn",
    BankCity : "Hyd",
    BankZipcode :"500055"
  },

  InternationalRecipitentDetails: {

    BeneficiaryName : "Interbeneficiaryjasmine",
    Address1 : "Miyapur",
    Address2 : "alwyn",
    city : "Hyderabad",
    zipcode : "500055"
  },

  InternationalBankdetails : {

    SWIFT : "UNBKILIT",
    IBAN : "123",
    AccNumber : "123",
    Nickname : "Domesticjasmine",
    BankName : "SBI",
    BankAddressLine1 : "Miyapur",
    BankAddressLine2 : "Alwyn",
    BankCity : "Hyd",
    BankZipcode :"500055"
  },


};

var CustomViewDetails={

  ViewName:'JasmineView',
  AccountName:'Saving',
  AccountNumber:'105139'
};
