require(["Test Suites/RBTestSuites/LoginSuite"], function() {
	require(["Test Suites/RBTestSuites/T24_ManageBeneficiarySuite"], function() {
		require(["Test Suites/RBTestSuites/T24_SearchBeneficiariesSuite"], function() {
			require(["Test Suites/RBTestSuites/T24_DomesticPaymentsSuite"], function() {
				require(["Test Suites/RBTestSuites/T24_InternationalPaymentsSuite"], function() {
					require(["Test Suites/RBTestSuites/T24_SameBankPaymentSuite"], function() {
						require(["Test Suites/RBTestSuites/T24_TransferbetweenAccountsSuite"], function() {
														jasmine.getEnv().execute();
						});
					});
				});
			});
		});
	});
});