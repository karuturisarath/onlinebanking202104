require(["Test Suites/SMETestSuites/SME_PreLoginSuite"], function() {
	require(["Test Suites/SMETestSuites/SME_LoginSuite"], function() {
		require(["Test Suites/SMETestSuites/SME_MiniManageRecipitent"], function() {
			require(["Test Suites/SMETestSuites/SME_MiniTransferSuite"], function() {
				require(["Test Suites/SMETestSuites/SME_AccountsSuite"], function() {
					require(["Test Suites/SMETestSuites/SME_MessagesSuite"], function() {
						require(["Test Suites/SMETestSuites/SME_PostLoginSuite"], function() {
														jasmine.getEnv().execute();
						});
					});
				});
			});
		});
	});
});