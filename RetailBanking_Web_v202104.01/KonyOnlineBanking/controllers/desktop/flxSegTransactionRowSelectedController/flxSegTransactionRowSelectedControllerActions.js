define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnPrint **/
    AS_Button_bd7df62d4f58457fa4995a6339890852: function AS_Button_bd7df62d4f58457fa4995a6339890852(eventobject, context) {
        var self = this;
        kony.print("test");
    },
    /** onClick defined for btnRepeat **/
    AS_Button_c3ca2f123d7c45a986a41cd0a53ba885: function AS_Button_c3ca2f123d7c45a986a41cd0a53ba885(eventobject, context) {
        var self = this;
        this.showEditRule();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_db434477d2dd4006bc588388b42bdd73: function AS_FlexContainer_db434477d2dd4006bc588388b42bdd73(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    },
    /** onClick defined for flxRememberCategory **/
    AS_FlexContainer_f094efe1df294916ac839c5ea3df2958: function AS_FlexContainer_f094efe1df294916ac839c5ea3df2958(eventobject, context) {
        var self = this;
        this.rememberCategory();
    }
});