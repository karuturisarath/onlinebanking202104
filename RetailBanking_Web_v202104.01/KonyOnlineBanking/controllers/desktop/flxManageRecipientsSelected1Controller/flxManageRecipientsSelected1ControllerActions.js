define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnAction **/
    AS_Button_bd72ead795954508ba7fc0a70696537d: function AS_Button_bd72ead795954508ba7fc0a70696537d(eventobject, context) {
        var self = this;
        this.showOneTimeTransfer();
        this.view.oneTimeTransfer.lblHeader.text = "EDIT RECIPIENT";
        this.view.oneTimeTransfer.flxAccountType.setVisibility(false);
        this.setOneTimeTransferStep(1);
        this.view.breadcrumb.btnBreadcrumb2.text = "EDIT RECIPIENT";
        this.detailsJson.isDomesticVar = "false";
        this.detailsJson.isInternationalVar = "true";
    },
    /** onClick defined for btnDelete **/
    AS_Button_c4c9eb523afe48d89e125bb95c7ba88f: function AS_Button_c4c9eb523afe48d89e125bb95c7ba88f(eventobject, context) {
        var self = this;
        this.DeleteAction();
    },
    /** onClick defined for btnVewActivity **/
    AS_Button_e56c75988b924e4fa0b904981eefeea5: function AS_Button_e56c75988b924e4fa0b904981eefeea5(eventobject, context) {
        var self = this;
        this.showWireTransferActivityScreen();
    },
    /** onClick defined for btnMakeTransfer **/
    AS_Button_hcb3f2b3207c40f6a785f196ba328291: function AS_Button_hcb3f2b3207c40f6a785f196ba328291(eventobject, context) {
        var self = this;
        this.MakeTransferAction();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_e127e08949064dedb0bfceb8e44e3036: function AS_FlexContainer_e127e08949064dedb0bfceb8e44e3036(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});