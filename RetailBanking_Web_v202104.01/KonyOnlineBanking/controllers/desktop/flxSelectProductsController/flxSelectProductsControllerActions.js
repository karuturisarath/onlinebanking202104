define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnKnowMore **/
    AS_Button_g413108cbdaf4439928dcef97cd9d84f: function AS_Button_g413108cbdaf4439928dcef97cd9d84f(eventobject, context) {
        var self = this;
        this.KnowMoreAction();
    },
    /** onClick defined for flxCheckBox **/
    AS_FlexContainer_e239ce8cd41540fe800a1d50ad5caa16: function AS_FlexContainer_e239ce8cd41540fe800a1d50ad5caa16(eventobject, context) {
        var self = this;
        this.toggleCheckBox();
    }
});