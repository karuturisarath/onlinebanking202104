define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for flxCheckBox **/
    AS_FlexContainer_ee0e911757844dde875bc202e35ebe7d: function AS_FlexContainer_ee0e911757844dde875bc202e35ebe7d(eventobject, context) {
        var self = this;
        this.toggleCheckBox(eventobject, context);
    },
    /** onTouchStart defined for lblDropDown **/
    AS_Label_bd5286784faa4866a99b06d6fa3165df: function AS_Label_bd5286784faa4866a99b06d6fa3165df(eventobject, x, y, context) {
        var self = this;
        this.toggleSegment(eventobject, context)
    }
});