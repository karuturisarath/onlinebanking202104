define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** postShow defined for frmCreateCustomRoleName **/
    AS_Form_becf4afd14134b10a7c47b7048f223f8: function AS_Form_becf4afd14134b10a7c47b7048f223f8(eventobject) {
        var self = this;
        this.postShow();
    },
    /** preShow defined for frmCreateCustomRoleName **/
    AS_Form_g97c363b51ef4fb6adcc91b871ba7a5e: function AS_Form_g97c363b51ef4fb6adcc91b871ba7a5e(eventobject) {
        var self = this;
        this.preShow();
    },
    /** init defined for frmCreateCustomRoleName **/
    AS_Form_heada92c2cb24bbba3d659f6cb1a1343: function AS_Form_heada92c2cb24bbba3d659f6cb1a1343(eventobject) {
        var self = this;
        this.initActions();
    }
});