define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnYes **/
    AS_Button_c226fb7b57314d28894f20c40ee1ff1b: function AS_Button_c226fb7b57314d28894f20c40ee1ff1b(eventobject) {
        var self = this;
        this.btnYesTakeSurvey();
    },
    /** onClick defined for btnNo **/
    AS_Button_fa0c887a1cb1471cbd4cf406d78c0655: function AS_Button_fa0c887a1cb1471cbd4cf406d78c0655(eventobject) {
        var self = this;
        this.btnNoTakeSurvey();
    },
    /** onClick defined for flxCross **/
    AS_FlexContainer_c3c3e8028f404228abd717613254d512: function AS_FlexContainer_c3c3e8028f404228abd717613254d512(eventobject) {
        var self = this;
        this.onFeedbackCrossClick();
    },
    /** postShow defined for frmUserManagement **/
    AS_Form_ad32ac2c9fc945d1952af4fa86d0b342: function AS_Form_ad32ac2c9fc945d1952af4fa86d0b342(eventobject) {
        var self = this;
        this.postShow();
    },
    /** preShow defined for frmUserManagement **/
    AS_Form_cea29091ea9c456490f5eb5fe8a1536e: function AS_Form_cea29091ea9c456490f5eb5fe8a1536e(eventobject) {
        var self = this;
        this.preShow();
    },
    /** onBreakpointChange defined for frmUserManagement **/
    AS_Form_fd82d79546e740189dec92050d92e24c: function AS_Form_fd82d79546e740189dec92050d92e24c(eventobject, breakpoint) {
        var self = this;
        return self.onBreakpointChange.call(this, breakpoint);
    },
    /** onTouchEnd defined for frmUserManagement **/
    AS_Form_gb81458364d744b79959ccf1d375df64: function AS_Form_gb81458364d744b79959ccf1d375df64(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** init defined for frmUserManagement **/
    AS_Form_i7d826f1f6ee43d781bcb3838edffd6c: function AS_Form_i7d826f1f6ee43d781bcb3838edffd6c(eventobject) {
        var self = this;
        this.initActions();
    }
});