define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** init defined for frmConfirmAndAck **/
    AS_Form_d5d624b9b00c4e0e8c8f660b00fd4ff5: function AS_Form_d5d624b9b00c4e0e8c8f660b00fd4ff5(eventobject) {
        var self = this;
        this.initActions();
    },
    /** postShow defined for frmConfirmAndAck **/
    AS_Form_dd93f599a9684059a04d01a647335301: function AS_Form_dd93f599a9684059a04d01a647335301(eventobject) {
        var self = this;
        this.postShow();
    },
    /** preShow defined for frmConfirmAndAck **/
    AS_Form_jf442681c02546e5b603508765fc347a: function AS_Form_jf442681c02546e5b603508765fc347a(eventobject) {
        var self = this;
        this.preShow();
    }
});