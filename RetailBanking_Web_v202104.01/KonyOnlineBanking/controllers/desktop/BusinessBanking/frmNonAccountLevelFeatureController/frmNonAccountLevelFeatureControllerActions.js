define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** init defined for frmNonAccountLevelFeature **/
    AS_Form_d9fb4e66ed9d423ca6417addb9439fdc: function AS_Form_d9fb4e66ed9d423ca6417addb9439fdc(eventobject) {
        var self = this;
        return self.initActions.call(this);
    },
    /** onBreakpointChange defined for frmNonAccountLevelFeature **/
    AS_Form_fd82d79546e740189dec92050d92e24c: function AS_Form_fd82d79546e740189dec92050d92e24c(eventobject, breakpoint) {
        var self = this;
        return self.onBreakpointChange.call(this, breakpoint);
    }
});