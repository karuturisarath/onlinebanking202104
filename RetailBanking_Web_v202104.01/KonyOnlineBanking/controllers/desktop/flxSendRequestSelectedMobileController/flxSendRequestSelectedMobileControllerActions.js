define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnSend **/
    AS_Button_a8141ef3c6054c819ac6e7ba5bc99b22: function AS_Button_a8141ef3c6054c819ac6e7ba5bc99b22(eventobject, context) {
        var self = this;
        this.showSendMoney();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_e990ee5c94a5411b933f5df79f1acea7: function AS_FlexContainer_e990ee5c94a5411b933f5df79f1acea7(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});