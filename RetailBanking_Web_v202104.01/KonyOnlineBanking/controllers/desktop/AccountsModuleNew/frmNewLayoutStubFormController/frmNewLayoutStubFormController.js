define([], function() {
    return {
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onBreakpointChange = this.onBreakpointChange;
        },
        preShow: function() {},
        postShow: function() {},
        onBreakpointChange: function() {}
    }
});