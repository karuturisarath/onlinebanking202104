define(['FormControllerUtility'],function(FormControllerUtility){

  return {

    onNavigate: function(params) {
      var scope = this;
      scope.view.securityQuestions.setContext(params);
      scope.view.securityQuestions.showSecurityQuestions(params);
      scope.initActions();
    },
     /**
     * preShow
     * @api : preShow    
     * @return : NA
     */
   preShow : function(){
     FormControllerUtility.updateWidgetsHeightInInfo(this.view, ['flxHeader', 'flxFooter','flxMain','flxLogout']);
   },

    initActions: function() {
      var scope = this;
      var frmName;
      scope.view.securityQuestions.onSuccessCallback = function(params) {
        if(params.serviceName === "INTERNATIONAL_ACCOUNT_FUND_TRANSFER_CREATE")
          frmName = "UnifiedTransfersFlow/frmInternationalTransferAcknowledgement";
        else if(params.serviceName === "INTER_BANK_ACCOUNT_FUND_TRANSFER_CREATE")
          frmName = "UnifiedTransfersFlow/frmDomesticTransferAcknowledgement";
        else if(params.serviceName === "TRANSFER_BETWEEN_OWN_ACCOUNT_CREATE" || 
                params.serviceName === "INTRA_BANK_FUND_TRANSFER_CREATE")
          frmName = "UnifiedTransfersFlow/frmSameBankTransferAcknowledgement";
        var navMan = applicationManager.getNavigationManager();
        navMan.navigateTo(frmName,false,params);
      };
      scope.view.securityQuestions.onFailureCallback = function(response, error) {

      };
    },
    updateFormUI: function(viewModel) {
      if (viewModel.isLoading === true) {
        FormControllerUtility.showProgressBar(this.view);
      } else if (viewModel.isLoading === false) {
        FormControllerUtility.hideProgressBar(this.view);
      }
    }
  };

});