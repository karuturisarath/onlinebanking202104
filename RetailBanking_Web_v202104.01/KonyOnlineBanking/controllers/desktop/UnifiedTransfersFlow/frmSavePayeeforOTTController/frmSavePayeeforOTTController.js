define(['FormControllerUtility'],function(FormControllerUtility){

  return {
    onNavigate:function(params) {
      var scope = this;
      this.view.SavePayee.setContext(params,scope); 
    },
     /**
     * preShow
     * @api : preShow    
     * @return : NA
     */
   preShow : function(){
     FormControllerUtility.updateWidgetsHeightInInfo(this.view, ['flxHeader', 'flxFooter','flxMain','flxLogout']);
   },
    cancelSaveYes: function() {
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedTransfersFlow/frmLanding");
    },

    newTransfer: function() {
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("UnifiedTransfersFlow/frmLanding");
    },

    listAccounts: function() {
      var navMan = applicationManager.getNavigationManager();
      navMan.navigateTo("frmDashboard");
    },
    updateFormUI: function(viewModel) {
      if (viewModel.isLoading === true) {
        FormControllerUtility.showProgressBar(this.view);
      } else if (viewModel.isLoading === false) {
        FormControllerUtility.hideProgressBar(this.view);
      }
    }

  };
});

