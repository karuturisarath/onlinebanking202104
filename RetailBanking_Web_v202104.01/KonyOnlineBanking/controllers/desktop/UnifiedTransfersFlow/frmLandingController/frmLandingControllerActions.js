define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** postShow defined for frmLanding **/
    AS_Form_a365d8d0df2042bca461dd8309a4d5c0: function AS_Form_a365d8d0df2042bca461dd8309a4d5c0(eventobject) {
        var self = this;
        return self.postShow.call(this);
    },
    /** preShow defined for frmLanding **/
    AS_Form_de8e7a2d8fb9464489ad6ff8b6c7c4d7: function AS_Form_de8e7a2d8fb9464489ad6ff8b6c7c4d7(eventobject) {
        var self = this;
        this.preShow();
    },
    /** onDeviceBack defined for frmLanding **/
    AS_Form_e8734b2b9b524eaea74f68fdb77e34bf: function AS_Form_e8734b2b9b524eaea74f68fdb77e34bf(eventobject) {
        var self = this;
        kony.print("Back Navigation Disabled");
    }
});