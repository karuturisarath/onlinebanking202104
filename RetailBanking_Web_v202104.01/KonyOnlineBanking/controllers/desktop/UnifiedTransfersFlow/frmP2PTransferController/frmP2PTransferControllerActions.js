define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onDeviceBack defined for frmP2PTransfer **/
    AS_Form_b684b5ed35ef4267ba3bc770ae4aa0f5: function AS_Form_b684b5ed35ef4267ba3bc770ae4aa0f5(eventobject) {
        var self = this;
        kony.print("Back Navigation Disabled");
    },
    /** preShow defined for frmP2PTransfer **/
    AS_Form_ee80a45e5e904c6d82c756a91615812f: function AS_Form_ee80a45e5e904c6d82c756a91615812f(eventobject) {
        var self = this;
        this.preshow();
    },
    /** postShow defined for frmP2PTransfer **/
    AS_Form_j9cca5088bf3474f8423d63ec41f3286: function AS_Form_j9cca5088bf3474f8423d63ec41f3286(eventobject) {
        var self = this;
        return self.postshow.call(this);
    }
});