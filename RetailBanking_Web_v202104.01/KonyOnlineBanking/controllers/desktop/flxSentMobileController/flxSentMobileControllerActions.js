define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnModify **/
    AS_Button_faf64605d02a4e8f8295cc296131c2ba: function AS_Button_faf64605d02a4e8f8295cc296131c2ba(eventobject, context) {
        var self = this;
        this.showSendMoney();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_f2c18f70f96f4cbe87d1d1b7693afc48: function AS_FlexContainer_f2c18f70f96f4cbe87d1d1b7693afc48(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});