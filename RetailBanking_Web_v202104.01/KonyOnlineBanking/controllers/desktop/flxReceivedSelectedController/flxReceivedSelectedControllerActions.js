define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnAction **/
    AS_Button_efdebdd229f34729b80ddcdd06cec3e3: function AS_Button_efdebdd229f34729b80ddcdd06cec3e3(eventobject, context) {
        var self = this;
        this.showRequestMoney();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_h9776b7dd72d4998932ffc6fd6c335f6: function AS_FlexContainer_h9776b7dd72d4998932ffc6fd6c335f6(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});