define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for flxContent **/
    AS_FlexContainer_c3ba7965892440cc9d4a7f5aae0dd69a: function AS_FlexContainer_c3ba7965892440cc9d4a7f5aae0dd69a(eventobject, context) {
        var self = this;
        this.accountPressed(context);
    },
    /** onClick defined for flxFavorite **/
    AS_FlexContainer_cdb10ba0443b4637bbdd03a3cc9eb6b4: function AS_FlexContainer_cdb10ba0443b4637bbdd03a3cc9eb6b4(eventobject, context) {
        var self = this;
        event.stopPropagation();
        this.imgPressed(context);
    },
    /** onTouchStart defined for flxMenu **/
    AS_FlexContainer_eddabd2827d142e781edcb38358b310b: function AS_FlexContainer_eddabd2827d142e781edcb38358b310b(eventobject, x, y, context) {
        var self = this;
        this.setClickOrigin();
    },
    /** onClick defined for flxMenu **/
    AS_FlexContainer_j68e1e4184f9457dae4a73b672400fa8: function AS_FlexContainer_j68e1e4184f9457dae4a73b672400fa8(eventobject, context) {
        var self = this;
        this.menuPressed();
    }
});