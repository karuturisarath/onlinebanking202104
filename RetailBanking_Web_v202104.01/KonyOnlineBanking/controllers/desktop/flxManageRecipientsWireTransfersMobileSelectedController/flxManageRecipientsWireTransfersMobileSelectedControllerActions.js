define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnAction **/
    AS_Button_c086465b8ada4ee8b1f99b4b8a9b9e16: function AS_Button_c086465b8ada4ee8b1f99b4b8a9b9e16(eventobject, context) {
        var self = this;
        this.showOneTimeTransfer();
        this.view.oneTimeTransfer.lblHeader.text = "EDIT RECIPIENT";
        this.view.oneTimeTransfer.flxAccountType.setVisibility(false);
        this.setOneTimeTransferStep(1);
        this.view.breadcrumb.btnBreadcrumb2.text = "EDIT RECIPIENT";
        this.detailsJson.isDomesticVar = "false";
        this.detailsJson.isInternationalVar = "true";
    },
    /** onClick defined for btnDelete **/
    AS_Button_g287639d2bf6431bb6ee735832178a67: function AS_Button_g287639d2bf6431bb6ee735832178a67(eventobject, context) {
        var self = this;
        this.DeleteAction();
    },
    /** onClick defined for btnMakeTransfer **/
    AS_Button_g3bbf93c6239439f9e1156c2a9159665: function AS_Button_g3bbf93c6239439f9e1156c2a9159665(eventobject, context) {
        var self = this;
        this.MakeTransferAction();
    },
    /** onClick defined for btnVewActivity **/
    AS_Button_h4e2b06aee1c4fbd9f427b7970c75721: function AS_Button_h4e2b06aee1c4fbd9f427b7970c75721(eventobject, context) {
        var self = this;
        this.showWireTransferActivityScreen();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_a629545695d84e70afc04815f4e3b9be: function AS_FlexContainer_a629545695d84e70afc04815f4e3b9be(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});