define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onTouchEnd defined for flxContent **/
    AS_FlexContainer_c85ea4e2a7674b3dad323f47f457ca8e: function AS_FlexContainer_c85ea4e2a7674b3dad323f47f457ca8e(eventobject, x, y, context) {
        var self = this;
        this.accountPressed(context);
    },
    /** onClick defined for flxMenu **/
    AS_FlexContainer_eddabcfb4d4a46708364c4ee8b47273c: function AS_FlexContainer_eddabcfb4d4a46708364c4ee8b47273c(eventobject, context) {
        var self = this;
        this.menuPressed();
    },
    /** onClick defined for flxFavorite **/
    AS_FlexContainer_h3008a9cd9724030911c58712f40b347: function AS_FlexContainer_h3008a9cd9724030911c58712f40b347(eventobject, context) {
        var self = this;
        this.imgPressed();
    },
    /** onTouchStart defined for flxMenu **/
    AS_FlexContainer_h497d69300f547cfb0c63905ace270d8: function AS_FlexContainer_h497d69300f547cfb0c63905ace270d8(eventobject, x, y, context) {
        var self = this;
        this.setClickOrigin();
    }
});