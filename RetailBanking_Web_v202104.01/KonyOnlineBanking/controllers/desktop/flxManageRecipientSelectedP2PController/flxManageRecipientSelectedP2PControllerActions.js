define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnSendMoney **/
    AS_Button_b188615d77a74b5cb415f14b59baabac: function AS_Button_b188615d77a74b5cb415f14b59baabac(eventobject, context) {
        var self = this;
        this.showSendMoney();
    },
    /** onClick defined for btnDelete **/
    AS_Button_bdd00c9239b94c8698868a9bffea93ea: function AS_Button_bdd00c9239b94c8698868a9bffea93ea(eventobject, context) {
        var self = this;
        //adding this comment due to platform bug.
    },
    /** onClick defined for btnViewActivity **/
    AS_Button_c1e859547fde452eb4a18f5a37b339d2: function AS_Button_c1e859547fde452eb4a18f5a37b339d2(eventobject, context) {
        var self = this;
        //adding this comment due to platform bug.
    },
    /** onClick defined for btnEdit **/
    AS_Button_e79465538fb847eeb8fe352d307c8524: function AS_Button_e79465538fb847eeb8fe352d307c8524(eventobject, context) {
        var self = this;
        //adding this comment due to platform bug.
    },
    /** onClick defined for btnRequestMoney **/
    AS_Button_f8a80dda437c4dd4b1f7eed8aacaeb20: function AS_Button_f8a80dda437c4dd4b1f7eed8aacaeb20(eventobject, context) {
        var self = this;
        this.showRequestMoney();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_h1ab1c72abbb443d808a1aa1264beb70: function AS_FlexContainer_h1ab1c72abbb443d808a1aa1264beb70(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});