define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnViewActivity **/
    AS_Button_d1cacdea79494f118f27d1a674b3b9f0: function AS_Button_d1cacdea79494f118f27d1a674b3b9f0(eventobject, context) {
        var self = this;
        //adding this comment due to platform bug.
    },
    /** onClick defined for btnSendMoney **/
    AS_Button_d4807f66dcf14cdcbfc576277bb297f8: function AS_Button_d4807f66dcf14cdcbfc576277bb297f8(eventobject, context) {
        var self = this;
        this.showSendMoney();
    },
    /** onClick defined for btnDelete **/
    AS_Button_e5b8f3262a4b4a80b62198115cfbcec8: function AS_Button_e5b8f3262a4b4a80b62198115cfbcec8(eventobject, context) {
        var self = this;
        //adding this comment due to platform bug.
    },
    /** onClick defined for btnEdit **/
    AS_Button_f79d4f3c9c074c0287b516da1bb36737: function AS_Button_f79d4f3c9c074c0287b516da1bb36737(eventobject, context) {
        var self = this;
        //adding this comment due to platform bug.
    },
    /** onClick defined for btnRequestMoney **/
    AS_Button_i68219564987434780371f2ccbf70a5a: function AS_Button_i68219564987434780371f2ccbf70a5a(eventobject, context) {
        var self = this;
        this.showRequestMoney();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_d205e53e36c04a57a69cbc8ac7e164f7: function AS_FlexContainer_d205e53e36c04a57a69cbc8ac7e164f7(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});