define({
    showSelectedRow: function() {
        var previousIndex;
        var index = kony.application.getCurrentForm().MyRequestsTabs.segTransactions.selectedIndex;
        var rowIndex = index[1];
        var data = kony.application.getCurrentForm().MyRequestsTabs.segTransactions.data;
        for (i = 0; i < data.length; i++) {
            if (i == rowIndex) {
                kony.print("index:" + index);
                data[i].imgDropdown = "arrow_up.png";
                data[i].template = "flxDisputedChecksSelectedWrapper";
            } else {
                data[i].imgDropdown = "arrow_down.png";
                data[i].template = "flxDisputedChecksRowWrapper";
            }
        }
        kony.application.getCurrentForm().MyRequestsTabs.segTransactions.setData(data);
    }
});