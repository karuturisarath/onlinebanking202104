define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnDelete **/
    AS_Button_ab007c47521b4d319d871cd04688b0cf: function AS_Button_ab007c47521b4d319d871cd04688b0cf(eventobject, context) {
        var self = this;
        return self.showDeletePopup.call(this);
    },
    /** onClick defined for btnEdit **/
    AS_Button_b1f71e9fb41d49f4b659cbdb7fa8db4b: function AS_Button_b1f71e9fb41d49f4b659cbdb7fa8db4b(eventobject, context) {
        var self = this;
        return self.showEditEmail.call(this);
    }
});