define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxResetUserImg **/
    AS_FlexContainer_a3b26dac4b37456d97e818c8e80b9d4f: function AS_FlexContainer_a3b26dac4b37456d97e818c8e80b9d4f(eventobject) {
        var self = this;
        this.showUserActions();
    },
    /** onClick defined for flxUserId **/
    AS_FlexContainer_f7d4e2f09c4a4c88bb976bec8d703945: function AS_FlexContainer_f7d4e2f09c4a4c88bb976bec8d703945(eventobject) {
        var self = this;
        this.showUserActions();
    },
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_ec37e21b368c488f97df235f248b2e3f: function AS_FlexContainer_ec37e21b368c488f97df235f248b2e3f(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onClick defined for flxClose **/
    AS_FlexContainer_a230838887774f149fcb65becd6bfb06: function AS_FlexContainer_a230838887774f149fcb65becd6bfb06(eventobject) {
        var self = this;
        //this.closeHamburgerMenu();
    },
    /** onClick defined for btnBreadcrumb1 **/
    AS_Button_f7f7391b045847d095dda487a74198d6: function AS_Button_f7f7391b045847d095dda487a74198d6(eventobject) {
        var self = this;
        this.navigateForm();
    },
    /** onClick defined for btnBreadcrumb2 **/
    AS_Button_j9e209711dbc4cf5b243fd89187bdd2c: function AS_Button_j9e209711dbc4cf5b243fd89187bdd2c(eventobject) {
        var self = this;
        this.navigateForm();
    },
    /** onSelection defined for lbxFrequencymod **/
    AS_ListBox_d631f66ba55f4f61ba2f7f9d6641a7ff: function AS_ListBox_d631f66ba55f4f61ba2f7f9d6641a7ff(eventobject) {
        var self = this;
        this.getFrequencyAndFormLayout();
    },
    /** onSelection defined for lbxForHowLongmod **/
    AS_ListBox_c6ac88a71ff54430845d81e97e05a4b8: function AS_ListBox_c6ac88a71ff54430845d81e97e05a4b8(eventobject) {
        var self = this;
        this.getForHowLongandFormLayout();
    },
    /** onClick defined for btnbacktopayeelist **/
    AS_Button_b0f4eda906184d61a9d992d9fac29378: function AS_Button_b0f4eda906184d61a9d992d9fac29378(eventobject) {
        var self = this;
        this.presenter.getExternalAccounts();
    },
    /** onClick defined for flxAddNonKonyAccount **/
    AS_FlexContainer_a295f2b05c8040f5bed89e08dabfe64c: function AS_FlexContainer_a295f2b05c8040f5bed89e08dabfe64c(eventobject) {
        var self = this;
        //this.addExternalAccount();
        this.presenter.showDomesticAccounts();
    },
    /** onTouchStart defined for imgViewCVVCode **/
    AS_Image_a2e5b4fd2a2e45d1b1d47439533e8fb1: function AS_Image_a2e5b4fd2a2e45d1b1d47439533e8fb1(eventobject, x, y) {
        var self = this;
        this.showPassword();
    },
    /** onTouchEnd defined for imgViewCVVCode **/
    AS_Image_bcb918d064664cc084bbbdf545fee454: function AS_Image_bcb918d064664cc084bbbdf545fee454(eventobject, x, y) {
        var self = this;
        this.hidePassword();
    },
    /** onEndEditing defined for tbxAnswers1 **/
    AS_TextField_hd2919a53bba4cd5b97e2c1d7ce28435: function AS_TextField_hd2919a53bba4cd5b97e2c1d7ce28435(eventobject, changedtext) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** onKeyUp defined for tbxAnswers1 **/
    AS_TextField_bb5712a0acbf4be185c037837e4c1149: function AS_TextField_bb5712a0acbf4be185c037837e4c1149(eventobject) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** onEndEditing defined for tbxAnswers2 **/
    AS_TextField_e15285f05a50489aa812b54ac4a5489b: function AS_TextField_e15285f05a50489aa812b54ac4a5489b(eventobject, changedtext) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** onKeyUp defined for tbxAnswers2 **/
    AS_TextField_ce88b736ffcb43828056b276292472b0: function AS_TextField_ce88b736ffcb43828056b276292472b0(eventobject) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** onClick defined for btnLocateUs **/
    AS_Button_jefc7472c7a14f1fb8e090dab2a1b5ad: function AS_Button_jefc7472c7a14f1fb8e090dab2a1b5ad(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** onClick defined for btnContactUs **/
    AS_Button_bcde7b654ba14263975183cdd52a2ba7: function AS_Button_bcde7b654ba14263975183cdd52a2ba7(eventobject) {
        var self = this;
        this.showContactUsPage();
    },
    /** onClick defined for btnPrivacy **/
    AS_Button_i99dc23b65ec4f5195d666b4800b159a: function AS_Button_i99dc23b65ec4f5195d666b4800b159a(eventobject) {
        var self = this;
        this.showPrivacyPolicyPage();
    },
    /** onClick defined for btnTermsAndConditions **/
    AS_Button_i64b0bdb0f114e5ea5d35135da0f5a30: function AS_Button_i64b0bdb0f114e5ea5d35135da0f5a30(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    },
    /** onClick defined for btnFaqs **/
    AS_Button_i9271988eee941ba96e62962f94e2104: function AS_Button_i9271988eee941ba96e62962f94e2104(eventobject) {
        var self = this;
        this.showFAQs();
    },
    /** onClick defined for btnClose **/
    AS_Button_jb9de4a0de6b4c54ac4876b9a2e0a1f8: function AS_Button_jb9de4a0de6b4c54ac4876b9a2e0a1f8(eventobject) {
        var self = this;
        this.closeViewReport();
    },
    /** onClick defined for btnLocateUs **/
    AS_Button_e794db8749e843b48bb5b70cf2c3ad50: function AS_Button_e794db8749e843b48bb5b70cf2c3ad50(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** onClick defined for btnContactUs **/
    AS_Button_a179fb3179244e9f84d3fdfdd3aa8065: function AS_Button_a179fb3179244e9f84d3fdfdd3aa8065(eventobject) {
        var self = this;
        this.showContactUsPage();
    },
    /** onClick defined for btnPrivacy **/
    AS_Button_hecf96cf6a0f451b80805cf234d8f3af: function AS_Button_hecf96cf6a0f451b80805cf234d8f3af(eventobject) {
        var self = this;
        this.showPrivacyPolicyPage();
    },
    /** onClick defined for btnTermsAndConditions **/
    AS_Button_f3fda4d6c0d241b99c854fcd4dde887b: function AS_Button_f3fda4d6c0d241b99c854fcd4dde887b(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    },
    /** onClick defined for btnFaqs **/
    AS_Button_e80fc731fcdc42a4bca07bb39ca7fb1b: function AS_Button_e80fc731fcdc42a4bca07bb39ca7fb1b(eventobject) {
        var self = this;
        this.showFAQs();
    },
    /** init defined for frmTransfersEur **/
    AS_Form_f09ba16e3fc94953ad96928f30b9d3ab: function AS_Form_f09ba16e3fc94953ad96928f30b9d3ab(eventobject) {
        var self = this;
        this.initActions();
    },
    /** preShow defined for frmTransfersEur **/
    AS_Form_ff8db6f76b904852b8bfd0a2c657c0a8: function AS_Form_ff8db6f76b904852b8bfd0a2c657c0a8(eventobject) {
        var self = this;
        this.initTabsActions();
    },
    /** postShow defined for frmTransfersEur **/
    AS_Form_cf4081a2f395449487485c721bf6cb41: function AS_Form_cf4081a2f395449487485c721bf6cb41(eventobject) {
        var self = this;
        this.postShowtransfers();
    },
    /** onDeviceBack defined for frmTransfersEur **/
    AS_Form_h1ffb5e1446d47b0a253693d4b82a868: function AS_Form_h1ffb5e1446d47b0a253693d4b82a868(eventobject) {
        var self = this;
        //Need to Consolidate
        kony.print("Back button pressed");
    },
    /** onTouchEnd defined for frmTransfersEur **/
    AS_Form_f819dcae48864d2a89f30890ea9bd9f1: function AS_Form_f819dcae48864d2a89f30890ea9bd9f1(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});