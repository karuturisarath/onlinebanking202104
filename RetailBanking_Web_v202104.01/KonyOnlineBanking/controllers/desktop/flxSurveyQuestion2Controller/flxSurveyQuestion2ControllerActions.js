define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for flxcheckbox1 **/
    AS_FlexContainer_a272ee6884654bb18b36bb34dd44900e: function AS_FlexContainer_a272ee6884654bb18b36bb34dd44900e(eventobject, context) {
        var self = this;
        this.toggleSurveyQuestionTransfersCheckBox();
    },
    /** onClick defined for flxcheckbox2 **/
    AS_FlexContainer_a2f65689ba37449f97dc589e1df25778: function AS_FlexContainer_a2f65689ba37449f97dc589e1df25778(eventobject, context) {
        var self = this;
        this.toggleSurveyQuestionBillPayCheckBox();
    },
    /** onClick defined for flxcheckbox4 **/
    AS_FlexContainer_c6381cee98fb4343a809b3320207fb4d: function AS_FlexContainer_c6381cee98fb4343a809b3320207fb4d(eventobject, context) {
        var self = this;
        this.toggleSurveyQuestionNotificationMsgsCheckBox();
    },
    /** onClick defined for flxcheckbox3 **/
    AS_FlexContainer_f9cbd14d4ff8413f8b682a331ad2b5af: function AS_FlexContainer_f9cbd14d4ff8413f8b682a331ad2b5af(eventobject, context) {
        var self = this;
        this.toggleSurveyQuestionSecuritySettingCheckBox();
    }
});