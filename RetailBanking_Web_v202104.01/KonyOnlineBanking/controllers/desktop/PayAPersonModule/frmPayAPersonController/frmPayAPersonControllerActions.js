define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnCancel **/
    AS_Button_ac1a4efd4f454da28fea56106db1d5b8: function AS_Button_ac1a4efd4f454da28fea56106db1d5b8(eventobject) {
        var self = this;
        this.showTableView();
        this.setMyRequestsSegmentData();
    },
    /** onClick defined for btnCancel **/
    AS_Button_ae116ba841b6460a859dcd014219ffc4: function AS_Button_ae116ba841b6460a859dcd014219ffc4(eventobject) {
        var self = this;
        this.showNoRecipient();
    },
    /** onClick defined for btnSendMoney **/
    AS_Button_b05e0e2be56d495689746791cc29250d: function AS_Button_b05e0e2be56d495689746791cc29250d(eventobject) {
        var self = this;
        this.showSendMoney();
    },
    /** onClick defined for btnCancel **/
    AS_Button_b1baf167a1f8468f9f304b29ba082bb6: function AS_Button_b1baf167a1f8468f9f304b29ba082bb6(eventobject) {
        var self = this;
        this.showOptionsAndProceed();
    },
    /** onClick defined for btnSendMoneyNewRecipient **/
    AS_Button_b3386fadd8d84740b3b460f2a3cc94c6: function AS_Button_b3386fadd8d84740b3b460f2a3cc94c6(eventobject) {
        var self = this;
        this.showAddRecipient();
    },
    /** onClick defined for btnConfirm **/
    AS_Button_bc9ce113cc5441418e419f3346a5a8b0: function AS_Button_bc9ce113cc5441418e419f3346a5a8b0(eventobject) {
        var self = this;
        this.presenter.loadPayAPersonflx();
    },
    /** onClick defined for btnSendReminderCancel **/
    AS_Button_d539105ebd1949e4a31dec04ae027bc4: function AS_Button_d539105ebd1949e4a31dec04ae027bc4(eventobject) {
        var self = this;
        this.showTableView();
        this.setMyRequestsSegmentData();
    },
    /** onClick defined for btnAddRecipient **/
    AS_Button_d7c58f00bfd7410494adb15272b5df34: function AS_Button_d7c58f00bfd7410494adb15272b5df34(eventobject) {
        var self = this;
        this.showAddRecipient();
    },
    /** onClick defined for btnRequestMoneySendRequest **/
    AS_Button_d82d0ca6b46545538ad44879c31cdec5: function AS_Button_d82d0ca6b46545538ad44879c31cdec5(eventobject) {
        var self = this;
        this.showConfirmationRequestMoney();
    },
    /** onClick defined for btnRequestMoneyCancel **/
    AS_Button_def90096ce974f84aac11ec33f3f589c: function AS_Button_def90096ce974f84aac11ec33f3f589c(eventobject) {
        var self = this;
        //this.showBillPay();
        this.setSendRequestSegmentData();
    },
    /** onClick defined for btnSendReminder **/
    AS_Button_e0b46bb8b2b14ac5b79e53d66d1f947d: function AS_Button_e0b46bb8b2b14ac5b79e53d66d1f947d(eventobject) {
        var self = this;
        this.showAcknowledgementRequestMoney();
    },
    /** onClick defined for btnSendMoney **/
    AS_Button_e1cbffd627b2483889b8e10e1008ff3d: function AS_Button_e1cbffd627b2483889b8e10e1008ff3d(eventobject) {
        var self = this;
        this.showConfirmationSendMoney();
        kony.print("here");
    },
    /** onClick defined for btnProceed **/
    AS_Button_ebda37155f7c486bb195079c32d50a07: function AS_Button_ebda37155f7c486bb195079c32d50a07(eventobject) {
        var self = this;
        this.presenter.loadP2PActivationScreen();
    },
    /** onClick defined for btnRequestMoney **/
    AS_Button_g40cd76140124e21ad8c1265e49260a6: function AS_Button_g40cd76140124e21ad8c1265e49260a6(eventobject) {
        var self = this;
        this.showRequestMoney();
    },
    /** onClick defined for btnNo **/
    AS_Button_h2a642d5e767401594af0e60168af5cf: function AS_Button_h2a642d5e767401594af0e60168af5cf(eventobject) {
        var self = this;
        this.closeQuitScreen();
    },
    /** onClick defined for btnAddRecipient **/
    AS_Button_h638f37beec842c9abc8583bb5ff4f6a: function AS_Button_h638f37beec842c9abc8583bb5ff4f6a(eventobject) {
        var self = this;
        this.showAddRecipient();
    },
    /** onClick defined for btnAdd **/
    AS_Button_i87958957ad14f06a17b8c76c814d327: function AS_Button_i87958957ad14f06a17b8c76c814d327(eventobject) {
        var self = this;
        this.showConfirmationAddReceipient();
    },
    /** onClick defined for flxSearch **/
    AS_FlexContainer_d66daefec3674fa39cd486bc7ae22bad: function AS_FlexContainer_d66daefec3674fa39cd486bc7ae22bad(eventobject) {
        var self = this;
        this.toggleSearch();
    },
    /** onClick defined for flximgInfoIcon **/
    AS_FlexContainer_e2dd270f119e4cb2a19cbbc6d761ffd5: function AS_FlexContainer_e2dd270f119e4cb2a19cbbc6d761ffd5(eventobject) {
        var self = this;
    },
    /** onClick defined for flxCross **/
    AS_FlexContainer_gb0f1b87bc3d4460adf3acd1f2344e6a: function AS_FlexContainer_gb0f1b87bc3d4460adf3acd1f2344e6a(eventobject) {
        var self = this;
        this.closeQuitScreen();
    },
    /** onDeviceBack defined for frmPayAPerson **/
    AS_Form_acfdd77e696e4530a1cc4358ea8fcfe8: function AS_Form_acfdd77e696e4530a1cc4358ea8fcfe8(eventobject) {
        var self = this;
        //Need to Consolidate
        kony.print("Back button pressed");
    },
    /** init defined for frmPayAPerson **/
    AS_Form_c0d98d725a574962a4d6fd9372f3a2a7: function AS_Form_c0d98d725a574962a4d6fd9372f3a2a7(eventobject) {
        var self = this;
        this.init();
    },
    /** postShow defined for frmPayAPerson **/
    AS_Form_f3978356bbdd46948b22681fa8d49513: function AS_Form_f3978356bbdd46948b22681fa8d49513(eventobject) {
        var self = this;
        this.postShowPayaPerson();
    },
    /** onTouchEnd defined for frmPayAPerson **/
    AS_Form_h0af8f37d58440bcab6be4ba079485ba: function AS_Form_h0af8f37d58440bcab6be4ba079485ba(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** preShow defined for frmPayAPerson **/
    AS_Form_ib32241007544d1ab114963588c793d3: function AS_Form_ib32241007544d1ab114963588c793d3(eventobject) {
        var self = this;
        this.frmPayAPersonPreShow();
    }
});