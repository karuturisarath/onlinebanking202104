define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnSend **/
    AS_Button_f2d4cd4ba02a4579aaaae5720372313d: function AS_Button_f2d4cd4ba02a4579aaaae5720372313d(eventobject, context) {
        var self = this;
        this.showSendMoney();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_jdd807a1ba7c4215adadcc8b27606f25: function AS_FlexContainer_jdd807a1ba7c4215adadcc8b27606f25(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});