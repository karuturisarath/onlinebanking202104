define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onTextChange defined for tbxDailyTransactionAmt **/
    AS_TextField_e0821e9cf3b14e53a0445e7941d20f64: function AS_TextField_e0821e9cf3b14e53a0445e7941d20f64(eventobject, changedtext, context) {
        var self = this;
        return self.onLimitChanged.call(this, eventobject, context);
    },
    /** onTextChange defined for tbxWeeklyTransactionAmt **/
    AS_TextField_e6fdec0c134647d6a6d99401e1a9d044: function AS_TextField_e6fdec0c134647d6a6d99401e1a9d044(eventobject, changedtext, context) {
        var self = this;
        return self.onLimitChanged.call(this, eventobject, context);
    },
    /** onTextChange defined for tbxPerTransactionAmt **/
    AS_TextField_hee80d89b38f46dd9028aac5e40f77d6: function AS_TextField_hee80d89b38f46dd9028aac5e40f77d6(eventobject, changedtext, context) {
        var self = this;
        return self.onLimitChanged.call(this, eventobject, context);
    }
});