define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnCancel **/
    AS_Button_c86fbf7b80404cfbb38374a55f9ab7c1: function AS_Button_c86fbf7b80404cfbb38374a55f9ab7c1(eventobject) {
        var self = this;
        this.onActivationCancel();
    },
    /** onClick defined for btnStepContinue **/
    AS_Button_d6256b4674094bb2ac57ea09e65f9516: function AS_Button_d6256b4674094bb2ac57ea09e65f9516(eventobject) {
        var self = this;
        this.onMakeTransferContinue();
    },
    /** onClick defined for btnProceed **/
    AS_Button_ef24b1158ee84170b96c48412cc32a5f: function AS_Button_ef24b1158ee84170b96c48412cc32a5f(eventobject) {
        var self = this;
        this.onActivationContinue();
    },
    /** onClick defined for flxAddKonyAccount **/
    AS_FlexContainer_a4e5a713566840db979d5ff8dc1bd3ad: function AS_FlexContainer_a4e5a713566840db979d5ff8dc1bd3ad(eventobject) {
        var self = this;
        this.ShowDomesticAccount();
    },
    /** onClick defined for flxAddInternationalAccount **/
    AS_FlexContainer_bc41c3b8bc1d4576b53acb90a8ef149d: function AS_FlexContainer_bc41c3b8bc1d4576b53acb90a8ef149d(eventobject) {
        var self = this;
        this.showOneTimeTransfer();
    },
    /** onClick defined for flxAddNonKonyAccount **/
    AS_FlexContainer_e8548f1abafa4153806d165548994d3e: function AS_FlexContainer_e8548f1abafa4153806d165548994d3e(eventobject) {
        var self = this;
        this.showInternationalAccount();
    },
    /** onTouchEnd defined for frmWireTransfer **/
    AS_Form_a3f85960f6584452949f79666367e4e9: function AS_Form_a3f85960f6584452949f79666367e4e9(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** postShow defined for frmWireTransfer **/
    AS_Form_bd9ead963a0d4aa88c5a3391d95fe53b: function AS_Form_bd9ead963a0d4aa88c5a3391d95fe53b(eventobject) {
        var self = this;
        this.postShow();
    },
    /** onScrollEnd defined for frmWireTransfer **/
    AS_Form_c03c3f4a12a34a9fa832ec4b9d0de8f0: function AS_Form_c03c3f4a12a34a9fa832ec4b9d0de8f0(eventobject) {
        var self = this;
        this.closePopups();
    },
    /** preShow defined for frmWireTransfer **/
    AS_Form_c3ba9435f751460a9db3346daf50c19d: function AS_Form_c3ba9435f751460a9db3346daf50c19d(eventobject) {
        var self = this;
        this.formPreshow();
    },
    /** init defined for frmWireTransfer **/
    AS_Form_e1d54df39cfb480cbec529d293acf68b: function AS_Form_e1d54df39cfb480cbec529d293acf68b(eventobject) {
        var self = this;
        this.init();
    },
    /** onDeviceBack defined for frmWireTransfer **/
    AS_Form_i940b60fb529478d80eecd31abca40a1: function AS_Form_i940b60fb529478d80eecd31abca40a1(eventobject) {
        var self = this;
        kony.print("on device back");
    }
});