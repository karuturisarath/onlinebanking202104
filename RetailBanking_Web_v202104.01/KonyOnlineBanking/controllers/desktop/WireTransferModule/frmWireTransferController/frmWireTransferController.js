define(['CommonUtilities', 'OLBConstants', 'IBANUtils', 'FormControllerUtility', 'ViewConstants'], function(CommonUtilities, OLBConstants, IBANUtils, FormControllerUtility, ViewConstants) {
    var ADD_RECIPIENTS_STEPS = {
        RECIPIENT_DETAILS: 1,
        RECIPIENT_ACCOUNT_DETAILS: 2
    }
    var ONE_TIME_TRANSFER_STEPS = {
        RECIPIENT_DETAILS: 1,
        RECIPIENT_ACCOUNT_DETAILS: 2,
        TRANSACTION_DETAILS: 3
    }
    var orientationHandler = new OrientationHandler();
    return {
        /**
         * @param {context} context Context for the UI
         */
        updateFormUI: function(context) {
            if (context.isLoading) {
                FormControllerUtility.showProgressBar(this.view);
            } else {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (context.activationScreen) {
                this.showActivationScreen(context.activationScreen)
            }
            if (context.resetForm) {
                this.resetUI();
            }
            if (context.makeTransferRecipients) {
                this.showMakeTransferRecipients(context.makeTransferRecipients);
            }
            if (context.noMoreRecords) {
                this.showNoMoreRecords();
            }
            if (context.manageRecipients) {
                this.showManageTransferRecipients(context.manageRecipients);
            }
            if (context.wireTransactions) {
                this.showRecentTransactions(context.wireTransactions);
            }
            if (context.makeTransfer) {
                this.showMakeTransferForPayee(context.makeTransfer);
            }
            if (context.wireTransferAcknowledgement) {
                this.showWireTransferAcknowledgement(context.wireTransferAcknowledgement);
            }
            if (context.addPayee) {
                this.showAddPayee(context.addPayee)
            }
            if (context.states) {
                this.populateStates(context.states);
            }
            if (context.addRecipientAcknowledgement) {
                this.showAddAccountAcknowledgement(context.addRecipientAcknowledgement);
            }
            if (context.oneTimeTransfer) {
                this.showOneTimeTransfer(context.oneTimeTransfer);
            }
            if (context.editPayee) {
                this.showEditRecipientUI(context.editPayee);
            }
            if (context.accountDetailsForInbound) {
                this.showCheckingAccountsForInbound(context.accountDetailsForInbound.checkingAccounts);
            }
            if (context.recipientActivity) {
                this.showRecipientActivity(context.recipientActivity.recipient, context.recipientActivity.transactions);
            }
            if (context.oneTimeTransferAcknowledgement) {
                this.showOneTimeTransferAcknowledgement(context.oneTimeTransferAcknowledgement);
            }
            if (context.serverError) {
                this.showServerError(context.serverError);
            }
            if (context.saveRecipientServerError) {
                this.showSaveRecipientServerError(context.saveRecipientServerError);
            }
            if (context.wireTransferError) {
                this.showMakeTransferUI();
                this.showServerError(context.wireTransferError.errorMessage);
            }
            if (context.oneTimeTransferError) {
                this.showOneTimeTransferServerError(context.oneTimeTransferError);
            }
            if (context.TnCcontent) {
                this.bindTnCData(context.TnCcontent);
            }
            if (context.TnCFailure) {
                this.disableTnC();
            }
            if (context.editRecipientSuccessMsg) {
                this.showEditRecipientSuccessMessage(context.editRecipientSuccessMsg);
            }
        },

        bindTnCData: function(TnCcontent) {
            CommonUtilities.disableButton(this.view.confirmDetails.btnConfirm);
            this.view.confirmDetails.lblFavoriteEmailCheckBox.text = OLBConstants.FONT_ICONS.CHECBOX_UNSELECTED;
            this.view.confirmDetails.flxAgree.setVisibility(true);
            if (TnCcontent.contentTypeId === OLBConstants.TERMS_AND_CONDITIONS_URL) {
                this.view.confirmDetails.btnTandC.onClick = function() {
                    window.open(TnCcontent.termsAndConditionsContent);
                }
            } else {
                this.view.confirmDetails.btnTandC.onClick = this.showTermsAndConditionPopUp;
                this.setTnCDATASection(TnCcontent.termsAndConditionsContent);
                FormControllerUtility.setHtmlToBrowserWidget(this, this.view.brwBodyTnC, TnCcontent.termsAndConditionsContent);
            }
            this.view.flxClose.onClick = this.hideTermsAndConditionPopUp;
            this.view.confirmDetails.lblFavoriteEmailCheckBox.onTouchEnd = this.toggleTnC.bind(this, this.view.confirmDetails.lblFavoriteEmailCheckBox);
        },
        yes: function() {
            console.log("WireTransfer:Yes")
        },
        no: function() {
            console.log("WireTransfer:No")
        },

        showTermsAndConditionPopUp: function() {
            var height = this.view.flxHeader.info.frame.height + this.view.flxMain.info.frame.height + this.view.flxFooter.info.frame.height;
            this.view.flxTermsAndConditionsPopUp.height = height + "dp";
            this.view.flxTermsAndConditionsPopUp.setVisibility(true);
            this.view.forceLayout();
        },
        setTnCDATASection: function(content) {
            this.view.rtxTC.text = content;
        },
        hideTermsAndConditionPopUp: function() {
            this.view.flxTermsAndConditionsPopUp.setVisibility(false);
        },

        toggleTnC: function(widget) {
            CommonUtilities.toggleFontCheckbox(widget);
            if (widget.text === OLBConstants.FONT_ICONS.CHECBOX_UNSELECTED)
                CommonUtilities.disableButton(this.view.confirmDetails.btnConfirm);
            else
                CommonUtilities.enableButton(this.view.confirmDetails.btnConfirm);
        },
        /**
         * Preshow of the form - Various and Initialization and UI Resets
         */
        formPreshow: function() {
            this.view.customheader.forceCloseHamburger();
            var scopeObj = this;
            this.view.onBreakpointChange = function() {
                scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
            };
            applicationManager.getLoggerManager().setCustomMetrics(this, false, "Wire Transfer");
            if (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) {
                this.view.customheader.lblHeaderMobile.isVisible = true;
            } else {
                this.view.customheader.lblHeaderMobile.isVisible = false;
            }
            this.view.customheader.lblHeaderMobile.text = this.view.lblAddAccountHeading.text;
            this.view.customheader.topmenu.flxMenu.skin = "slFbox";
            this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
            this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
            this.view.oneTimeTransfer.lbxStep3Currency.masterData = [];
            if (applicationManager.getConfigurationManager().isInternationalWireTransferEnabled === "false") {
                this.view.flxAddNonKonyAccount.setVisibility(false);
            }
            if (applicationManager.getConfigurationManager().isDomesticWireTransferEnabled === "false") {
                this.view.flxAddKonyAccount.setVisibility(false);
            }
            this.makeTransferAmountField = FormControllerUtility.wrapAmountField(this.view.MakeTransfer.tbxAmount)
                .onKeyUp(this.checkMakeTransferForm.bind(this));
            this.oneTimeTransferAmountField = FormControllerUtility.wrapAmountField(this.view.oneTimeTransfer.tbxAmount)
                .onKeyUp(this.checkOTTTransactionForm.bind(this));
            FormControllerUtility.disableButton(this.view.oneTimeTransfer.btnProceed);
            this.setDomesticAddFormValidationActions();
            this.setStateForDomesticInternationalTabButton();
            this.setInitialActions();
            this.view.customheader.customhamburger.activateMenu("WIRE TRANSFER");
            applicationManager.getNavigationManager().applyUpdates(this);
            FormControllerUtility.updateWidgetsHeightInInfo(this, ['flxHeader', 'flxFooter', 'flxMain', 'confirmDetails.flxBankDetails', 'confirmDetails.flxRecipientDetails', 'customheader']);
        },
        closePopups: function() {
            this.view.AllFormsConfirmDetails.isVisible = false;
        },
        /**
         * Handle server error, shows serverFlex
         * @param {object} serverError error
         */
        showServerError: function(serverError) {
            this.view.flxDowntimeWarning.setVisibility(true);
            this.view.rtxDowntimeWarning.setVisibility(true);
            this.view.rtxDowntimeWarning.text = serverError;
            this.AdjustScreen();
        },
        /**
         * showNoMoreRecords - Handles zero records scenario in navigation.
         */
        showNoMoreRecords: function() {
            var paginationComponent = this.view.WireTransferContainer.tablePagination;
            paginationComponent.imgPaginationNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_INACTIVE;
            paginationComponent.flxPaginationNext.setEnabled(false);
            kony.ui.Alert(kony.i18n.getLocalizedString("i18n.navigation.norecordsfound"));
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Shows Add Button based on Configurations
         */
        setStateForDomesticInternationalTabButton: function() {
            if (applicationManager.getConfigurationManager().isInternationalWireTransferEnabled === "false") {
                this.view.AddRecipientAccount.btnInternationalAccount.skin = "sknbtnfbfbfbBottomBordere3e3e3csr";
                this.view.AddRecipientAccount.btnInternationalAccount.setEnabled(false);
            } else {
                this.view.AddRecipientAccount.btnInternationalAccount.skin = "sknBtnAccountSummarySelected";
                this.view.AddRecipientAccount.btnInternationalAccount.setEnabled(true);
            }
            if (applicationManager.getConfigurationManager().isDomesticWireTransferEnabled === "false") {
                this.view.AddRecipientAccount.btnDomesticAccount.skin = "sknbtnfbfbfbBottomBordere3e3e3csr";
                this.view.AddRecipientAccount.btnDomesticAccount.setEnabled(false);
            } else {
                this.view.AddRecipientAccount.btnDomesticAccount.skin = "sknBtnAccountSummarySelected";
                this.view.AddRecipientAccount.btnDomesticAccount.setEnabled(true);
            }
        },

        init: function() {
            FormControllerUtility.setRequestUrlConfig(this.view.brwBodyTnC);
        },

        /**
         * Binds initial widget actions - Should be called from pre show
         */
        setInitialActions: function() {
            var scopeObj = this;
            this.view.flxAddKonyAccount.onClick = this.onAddDomesticAccount.bind(this);
            this.view.flxAddNonKonyAccount.onClick = this.onAddInternationalAccount.bind(this);
            this.view.flxAccountInfoForAccountTransfer.onClick = this.onInboundInfoClick.bind(this);
            this.view.AddRecipientAccount.btnDomesticAccount.onClick = this.onAddDomesticAccount.bind(this);
            this.view.AddRecipientAccount.btnInternationalAccount.onClick = this.onAddInternationalAccount.bind(this);
            this.view.NoRecipientsWindow.btnRequestMoney.onClick = this.showOneTimeTransfer; //No-Recipients
            this.view.WireTransferContainer.btnRecent.onClick = this.onRecentTabClick.bind(this); //Recent - Wire Transfer
            this.view.WireTransferContainer.btnManageRecipient.onClick = this.onManageTabClick.bind(this);
            this.view.WireTransferContainer.btnMakeTransfer.onClick = this.onMakeTransferTabClick.bind(this);
            this.view.flxAddInternationalAccount.onClick = this.onOneTimeTransferActionClick.bind(this);
            this.setDomesticAddFormValidationActions();
            this.view.flxInfo.onClick = this.showOneTimeTransferInfo.bind(this);
            this.view.AllFormsConfirmDetails.flxCross.onClick = function() {
                scopeObj.view.AllFormsConfirmDetails.setVisibility(false);
            };
            this.view.confirmDetails.flxTransctionFee.onClick = this.showTransactionFeeInfo.bind(this);
        },
        onFromAccountChanged: function(component, accounts) {
            var fromAccount = this.getFromAccount(accounts, component.lbxStep3From.selectedKey);
            if (component === this.view.MakeTransfer) {
                this.view.MakeTransfer.lblCurrencySymbol.text = applicationManager.getFormatUtilManager().getCurrencySymbol(fromAccount.currencyCode);
            } else if (component === this.view.oneTimeTransfer) {
                this.view.oneTimeTransfer.lblCurrencySymbol.text = applicationManager.getFormatUtilManager().getCurrencySymbol(fromAccount.currencyCode);
            }
            if (this.view.oneTimeTransfer.flxStep3Currency.isVisible) {
                component.lbxStep3Currency.selectedKey = applicationManager.getFormatUtilManager().getCurrencySymbol(fromAccount.currencyCode);
            }
        },
        /**
         * Show the expanded row
         * @param {object} segment Reference of segment object
         * @param {string} expandedTemplate Name of expanded template
         */
        showExpandedRow: function(segment, expandedTemplate, expandedMobileTemplate) {
            if (this.expandedRow !== null && this.expandedRow !== undefined && typeof this.expandedRow === "number") {
                var data = this.view.WireTransferContainer.segWireTransfers.data[this.expandedRow];
                data.imgDropdown = "arrow_up.png"
                data.template = (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) ? expandedMobileTemplate : expandedTemplate;
                segment.setDataAt(data, this.expandedRow)
                this.expandedRow = null;
            }
        },
        /**
         * Sets the expanded row for history
         * @param {number} rowIndex Row Index for expansion
         */
        setExpandedRow: function(rowIndex) {
            this.expandedRow = rowIndex;
        },
        /**
         * Shows I-icon for one time transfer
         */
        showOneTimeTransferInfo: function() {
            var scopeObj = this;
            if (scopeObj.view.AllFormsConfirmDetails.isVisible === false) {
                scopeObj.view.AllFormsConfirmDetails.isVisible = true;
                scopeObj.view.AllFormsConfirmDetails.left = scopeObj.view.flxRightBar.info.frame.x + scopeObj.view.flxInfo.info.frame.x - 135 + "dp";
                scopeObj.view.AllFormsConfirmDetails.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.WireTransfer.msgInfo2OneTime");
                if (scopeObj.view.flxAccountInfoForAccountTransfer.isVisible === true) scopeObj.view.AllFormsConfirmDetails.top = scopeObj.view.flxRightBar.info.frame.height - 80 + "dp";
                else scopeObj.view.AllFormsConfirmDetails.top = scopeObj.view.flxRightBar.info.frame.height - 10 + "dp";
                scopeObj.view.forceLayout();
            } else scopeObj.view.AllFormsConfirmDetails.isVisible = false;
        },
        /**
         * Shows I-icon for confirm details transfer
         */
        showTransactionFeeInfo: function() {
            var scopeObj = this;
            if (scopeObj.view.AllFormsConfirmDetails.isVisible === false) {
                scopeObj.view.AllFormsConfirmDetails.isVisible = true;
                scopeObj.view.AllFormsConfirmDetails.left = "87dp";
                scopeObj.view.AllFormsConfirmDetails.RichTextInfo.text = kony.i18n.getLocalizedString("i18n.Wiretranfer.infoIcon");
                if (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) {
                    if (this.view.flxSuccessMessage.isVisible == true)
                        scopeObj.view.AllFormsConfirmDetails.top = scopeObj.view.confirmDetails.flxTransactionDetailsRow3.info.frame.y + 78 + 330 + "dp";
                    else
                        scopeObj.view.AllFormsConfirmDetails.top = scopeObj.view.confirmDetails.flxTransactionDetailsRow3.info.frame.y + 78 + "dp";
                } else scopeObj.view.AllFormsConfirmDetails.top = scopeObj.view.confirmDetails.flxTransactionDetailsRow3.info.frame.y + 78 + "dp";

            } else scopeObj.view.AllFormsConfirmDetails.isVisible = false;
        },
        /**
         * Calls when Account Info button on right bar is clicked
         */
        onInboundInfoClick: function() {
            this.getWireTransferModule()
                .presentationController
                .showAccountDetailsForInboundTransfers();
        },
        /**
         * Starts One Time Transfer
         */
        onOneTimeTransferActionClick: function() {
            this.getWireTransferModule().presentationController.showOneTimeTransfer();
        },
        /**
         * Binds Field Validation actions for add recipient scenario
         */
        setDomesticAddFormValidationActions: function() {
            this.view.AddRecipientAccount.tbxRecipientName.onKeyUp = this.checkAddRecipientDetailForm.bind(this);
            this.view.AddRecipientAccount.tbxAddressLine1.onKeyUp = this.checkAddRecipientDetailForm.bind(this);
            this.view.AddRecipientAccount.tbxAddressLine2.onKeyUp = this.checkAddRecipientDetailForm.bind(this);
            this.view.AddRecipientAccount.tbxCity.onKeyUp = this.checkAddRecipientDetailForm.bind(this);
            this.view.AddRecipientAccount.tbxZipcode.onKeyUp = this.checkAddRecipientDetailForm.bind(this);
            this.view.AddRecipientAccount.tbxSwiftCode.onKeyUp = this.checkAddRecipientAccountDetailForm.bind(this);
            this.view.AddRecipientAccount.tbxAccountNumber.onKeyUp = this.checkAddRecipientAccountDetailForm.bind(this);
            this.view.AddRecipientAccount.tbxReAccountNumber.onKeyUp = this.checkAddRecipientAccountDetailForm.bind(this);
            this.view.AddRecipientAccount.tbxNickName.onKeyUp = this.checkAddRecipientAccountDetailForm.bind(this);
            this.view.AddRecipientAccount.tbxBankName.onKeyUp = this.checkAddRecipientAccountDetailForm.bind(this);
            this.view.AddRecipientAccount.tbxBankAddressLine1.onKeyUp = this.checkAddRecipientAccountDetailForm.bind(this);
            this.view.AddRecipientAccount.tbxBankAddressLine2.onKeyUp = this.checkAddRecipientAccountDetailForm.bind(this);
            this.view.AddRecipientAccount.tbxBankCity.onKeyUp = this.checkAddRecipientAccountDetailForm.bind(this);
            this.view.AddRecipientAccount.tbxBankZipcode.onKeyUp = this.checkAddRecipientAccountDetailForm.bind(this);
            this.view.AddRecipientAccount.tbxIBANOrIRC.onKeyUp = this.checkAddRecipientAccountDetailForm.bind(this);
        },
        /**
         * Returns the complete data for recipient
         * @returns {object} - Containing details of recipient  details
         */
        getFullRecipientData: function() {
            return {
                recipientDetails: this.getRecipientFormData(),
                recipientAccountDetails: this.getRecipientAccountFormData()
            }
        },
        /**
         * Returns the account details of recipient in key value pairs.
         * @returns {Object} details of account of recipient
         */
        getRecipientAccountFormData: function() {
            return {
                payeeNickName: this.view.AddRecipientAccount.tbxNickName.text.trim(),
                routingCode: this.view.AddRecipientAccount.tbxSwiftCode.text.trim(),
                swiftCode: this.view.AddRecipientAccount.tbxSwiftCode.text.trim(),
                IBAN: this.view.AddRecipientAccount.tbxIBANOrIRC.text.trim(),
                internationalRoutingCode: this.view.AddRecipientAccount.tbxIBANOrIRC.text.trim(),
                payeeAccountNumber: this.view.AddRecipientAccount.tbxAccountNumber.text.trim(),
                accountNumberConfirm: this.view.AddRecipientAccount.tbxReAccountNumber.text.trim(),
                bankName: this.view.AddRecipientAccount.tbxBankName.text.trim(),
                bankAddressLine1: this.view.AddRecipientAccount.tbxBankAddressLine1.text.trim(),
                bankAddressLine2: this.view.AddRecipientAccount.tbxBankAddressLine2.text.trim(),
                bankCity: this.view.AddRecipientAccount.tbxBankCity.text.trim(),
                bankState: this.view.AddRecipientAccount.lbxBankState.selectedKeyValue[1],
                bankZip: this.view.AddRecipientAccount.tbxBankZipcode.text.trim()
            }
        },
        /**
         * Returns the form data for recipient details
         * @returns {Object} Key Value Pairs of the recipientd data
         */
        getRecipientFormData: function() {
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            var country = this.view.AddRecipientAccount.flxCountry.isVisible ?
                this.view.AddRecipientAccount.lbxCountry.selectedKeyValue[1] :
                OLBConstants.WireTransferConstants.DOMESTIC_COUNTRY_NAME;
            return {
                payeeName: this.view.AddRecipientAccount.tbxRecipientName.text.trim(),
                type: this.view.AddRecipientAccount.imgRadioBtnRecipientType1.text === ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO ? OLBConstants.WireTransferConstants.RECIPIENT_INDIVIDUAL : OLBConstants.WireTransferConstants.RECIPIENT_BUSINESS,
                addressLine1: this.view.AddRecipientAccount.tbxAddressLine1.text.trim(),
                addressLine2: this.view.AddRecipientAccount.tbxAddressLine2.text.trim(),
                cityName: this.view.AddRecipientAccount.tbxCity.text.trim(),
                state: this.view.AddRecipientAccount.lbxState.selectedKeyValue[1],
                country: country,
                zipCode: this.view.AddRecipientAccount.tbxZipcode.text.trim()
            };
        },
        /**
         * Validates domestic recipient account details form
         */
        checkAddRecipientAccountDetailForm: function() {
            var formData = this.getRecipientAccountFormData();
            if (formData.swiftCode === "" ||
                formData.payeeAccountNumber === "" ||
                formData.accountNumberConfirm === "" ||
                formData.payeeNickName === "" ||
                formData.bankName === "" ||
                formData.bankAddressLine1 === "" ||
                formData.bankCity === "" ||
                formData.bankZip === "" ||
                (this.view.AddRecipientAccount.tbxIBANOrIRC.isVisible && formData.IBAN === "")) {
                FormControllerUtility.disableButton(this.view.AddRecipientAccount.btnAddRecipent);
            } else {
                FormControllerUtility.enableButton(this.view.AddRecipientAccount.btnAddRecipent);
            }
        },
        /**
         * Validates domestic recipient details form
         */
        checkAddRecipientDetailForm: function() {
            var formData = this.getRecipientFormData();
            if (formData.payeeName === "" ||
                formData.addressLine1 === "" ||
                formData.cityName === "" ||
                formData.zipCode === "") {
                FormControllerUtility.disableButton(this.view.AddRecipientAccount.btnProceed);
            } else {
                FormControllerUtility.enableButton(this.view.AddRecipientAccount.btnProceed);
            }
        },
        /**
         * Called when ActivateWireTransferWindow.flxCheckbox onClick is triggerd.
         * Toggles the image checkbox and enable/disable activate proceed button,
         */
        onActivationCheckboxPressed: function() {
            FormControllerUtility.toggleFontCheckbox(this.view.ActivateWireTransferWindow.lblFavoriteEmailCheckBox);
            if (FormControllerUtility.isFontIconChecked(this.view.ActivateWireTransferWindow.lblFavoriteEmailCheckBox)) {
                FormControllerUtility.enableButton(this.view.ActivateWireTransferWindow.btnProceed);
            } else {
                FormControllerUtility.disableButton(this.view.ActivateWireTransferWindow.btnProceed);
            }
        },
        /**
         * Called when ActivateWireTransferWindow.btnProceed onClick is triggered.
         */
        onActivationContinue: function() {
            this.getWireTransferModule().presentationController.activateWireTransferForUser(this.view.ActivateWireTransferWindow.lbxDefaultAccountForSending.selectedKey);
        },
        /**
         * Called when ActivateWireTransferWindow.btnCancel onClick is triggered.
         */
        onActivationCancel: function() {
            this.getWireTransferModule().presentationController.cancelWireTransferActivation();
        },
        /**
         * View Entry Point Method for Activation Screen
         * @param {object} activationScreenData Data for activation screen data
         * @param {Account[]} activationScreenData.checkingAccounts Accounts with type checking
         */
        showActivationScreen: function(activationScreenData) {
            var scopeObj = this;
            if (activationScreenData.checkingAccounts.length === 0) {
                this.showNoCheckingAccountUI();
                return;
            }
            this.showActivationScreenUI();
            FormControllerUtility.setLblCheckboxState(false, this.view.ActivateWireTransferWindow.lblFavoriteEmailCheckBox);
            FormControllerUtility.disableButton(this.view.ActivateWireTransferWindow.btnProceed);
            this.view.btnCancel.onClick = function() {
                scopeObj.view.flxTermsAndConditionsPopUp.setVisibility(false);
            };
            this.view.flxClose.onClick = function() {
                scopeObj.view.flxTermsAndConditionsPopUp.setVisibility(false);
            };
            this.view.btnSave.onClick = function() {
                FormControllerUtility.setLblCheckboxState(FormControllerUtility.isFontIconChecked(scopeObj.view.lblFavoriteEmailCheckBox), scopeObj.view.ActivateWireTransferWindow.lblFavoriteEmailCheckBox);
                if (FormControllerUtility.isFontIconChecked(scopeObj.view.ActivateWireTransferWindow.lblFavoriteEmailCheckBox)) {
                    FormControllerUtility.enableButton(scopeObj.view.ActivateWireTransferWindow.btnProceed);
                } else {
                    FormControllerUtility.disableButton(scopeObj.view.ActivateWireTransferWindow.btnProceed);
                }
                scopeObj.view.flxTermsAndConditionsPopUp.setVisibility(false);
            };
            this.view.flxTCContentsCheckbox.onClick = function() {
                FormControllerUtility.toggleFontCheckbox(scopeObj.view.lblFavoriteEmailCheckBox);
            };
            this.view.ActivateWireTransferWindow.btnTermsAndConditions.onClick = function() {
                FormControllerUtility.setLblCheckboxState(FormControllerUtility.isFontIconChecked(scopeObj.view.ActivateWireTransferWindow.lblFavoriteEmailCheckBox), scopeObj.view.lblFavoriteEmailCheckBox);
                scopeObj.view.flxTermsAndConditionsPopUp.height = scopeObj.view.flxHeader.info.frame.height + scopeObj.view.flxMain.info.frame.height + scopeObj.view.flxFooter.info.frame.height;
                scopeObj.view.flxTermsAndConditionsPopUp.setVisibility(true);
            };
            this.view.ActivateWireTransferWindow.flxCheckbox.onClick = function() {
                FormControllerUtility.toggleFontCheckbox(scopeObj.view.ActivateWireTransferWindow.lblFavoriteEmailCheckBox);
                if (FormControllerUtility.isFontIconChecked(scopeObj.view.ActivateWireTransferWindow.lblFavoriteEmailCheckBox) && !CommonUtilities.isCSRMode()) {
                    FormControllerUtility.enableButton(scopeObj.view.ActivateWireTransferWindow.btnProceed);
                } else {
                    FormControllerUtility.disableButton(scopeObj.view.ActivateWireTransferWindow.btnProceed);
                }
            };
            if (CommonUtilities.isCSRMode()) {
                this.view.ActivateWireTransferWindow.btnProceed.onClick = FormControllerUtility.disableButtonActionForCSRMode();
                this.view.ActivateWireTransferWindow.btnProceed.skin = FormControllerUtility.disableButtonSkinForCSRMode();
            } else {
                this.view.ActivateWireTransferWindow.btnProceed.onClick = this.activateWireTransfer;
            }
            this.view.ActivateWireTransferWindow.btnCancel.onClick = this.NavigateToTransfers;
            this.mapCheckingAccountsForActivation(activationScreenData.checkingAccounts);
        },
        activateWireTransfer: function() {
            var defaultAccountID = this.view.ActivateWireTransferWindow.lbxDefaultAccountForSending.selectedKey;
            this.getWireTransferModule().presentationController.activateWireTransfer(defaultAccountID);
        },
        /**
         * Map Checking Acccounts for activation in list box
         * @param {Account[]} checkingAccounts Accounts with type checking
         */
        mapCheckingAccountsForActivation: function(checkingAccounts) {
            var listBoxData = FormControllerUtility.getListBoxDataFromObjects(checkingAccounts, "accountID", CommonUtilities.getAccountDisplayName);
            this.view.ActivateWireTransferWindow.lbxDefaultAccountForSending.masterData = listBoxData;
        },
        /**
         * Shows the UI component for Activation screen.
         */
        showActivationScreenUI: function() {
            this.resetUI();
            this.view.breadcrumb.setBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.transfers.wireTransfer")
            }, {
                text: kony.i18n.getLocalizedString("i18n.WireTransfer.ActivateWireTransfer")
            }]);
            this.view.customheader.customhamburger.activateMenu("WIRE TRANSFER");
            this.view.flxActivateWireTransfer.setVisibility(true);
            this.view.lblAddAccountHeading.text = "Activate Wire Transfer";
            this.view.customheader.lblHeaderMobile.text = this.view.lblAddAccountHeading.text;
            this.AdjustScreen();
        },
        /**
         * Resets the UI - Hide all major UI Components
         */
        resetUI: function() {
            this.view.flxPrint.setVisibility(false);
            this.view.flxDownload.setVisibility(false);
            this.view.flxDowntimeWarning.setVisibility(false);
            this.view.rtxDowntimeWarning.setVisibility(false);
            this.view.WireTransferContainer.tablePagination.setVisibility(false);
            this.view.flxWireTransfersWindow.setVisibility(false);
            this.view.flxWireTransferActivityWindow.setVisibility(false);
            this.view.flxAddRecipientsWindow.setVisibility(false);
            this.view.flxNoRecipients.setVisibility(false);
            this.view.WireTransferContainer.flxNoTransactions.setVisibility(false);
            this.view.flxActivateWireTransfer.setVisibility(false);
            this.view.flxConfirmDetails.setVisibility(false);
            this.view.flxRightBar.setVisibility(false);
            this.view.flxOneTimeTransfer.setVisibility(false);
            this.view.flxMakeTransfer.setVisibility(false);
            this.view.flxInboundTransfer.setVisibility(false);
            this.view.frmNotEligibleToTransfer.setVisibility(false);
        },
        /**
         * Returns the wire Transfer Module
         * @returns {object} Reference to wire transfer module.
         */
        getWireTransferModule: function() {
            return kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('WireTransferModule');
        },
        /**
         * To return Bank Address
         * @param {Payee} payeeObject recipient list
         * @param {boolean} withName -to specify whether Address required with name or not
         * @returns {string} formatted address
         */
        returnBankAddress: function(payeeObject, withName) {
            var strings = [payeeObject.bankAddressLine1, payeeObject.bankAddressLine2, payeeObject.bankCity, payeeObject.bankState, payeeObject.bankZip];
            var address = strings.filter(function(string) {
                if (string) {
                    return true
                };
                return false;
            }).join(', ');
            if (withName) {
                address = [payeeObject.bankName, address].filter(function(string) {
                    if (string) {
                        return true;
                    }
                    return false;
                }).join(', ');
            }
            return address;
        },
        /**
         * Return recipient address
         * @param {Payee} dataItem - Reciepient Object
         * @returns {string} Formatted Address of the payee
         */
        returnRecipientAddress: function(dataItem) {
            var strings = [dataItem.addressLine1, dataItem.addressLine2, dataItem.cityName, dataItem.state, dataItem.country, dataItem.zipCode];
            var address = strings.filter(function(string) {
                if (string) {
                    return true;
                }
                return false;
            }).join(', ');
            return address;
        },
        /**
         * Display No Recipient Screen - UI logic
         */
        showNoRecipientsScreen: function() {
            this.resetUI();
            this.view.flxNoRecipients.setVisibility(true);
            //No-Recipients Actions
            var text1 = kony.i18n.getLocalizedString("i18n.transfers.wireTransfer");
            var text2 = kony.i18n.getLocalizedString("i18n.PayAPerson.AddRecipient");
            this.view.breadcrumb.setBreadcrumbData([{
                text: text1
            }, {
                text: text2
            }]);
            this.view.lblAddAccountHeading.text = "Wire Transfer";
            this.view.customheader.lblHeaderMobile.text = this.view.lblAddAccountHeading.text;
            this.view.NoRecipientsWindow.btnSendMoney.onClick = this.onAddDomesticAccount;
            this.view.NoRecipientsWindow.btnRequestMoney.onClick = this.onOneTimeTransferActionClick;
            this.showRightBar("noRecipient");
            this.AdjustScreen();
            this.view.forceLayout();
        },
        /**
         * Show No Search results - UI logic
         */
        showNoSearchResults: function() {
            this.view.WireTransferContainer.tablePagination.setVisibility(false);
            this.view.WireTransferContainer.segWireTransfers.setVisibility(false);
            this.view.WireTransferContainer.flxNoTransactions.setVisibility(true);
            this.view.WireTransferContainer.lblScheduleAPayment.setVisibility(false);
            this.view.WireTransferContainer.rtxNoPaymentMessage.text = kony.i18n.getLocalizedString("i18n.onlineHelp.noSearchResults");
            this.view.forceLayout();
        },
        /**
         * Show Make Transfer Recipients in Make Transfer Tab
         * @param {Payee[]} recipients Array of Payee Object
         * @param {object} config Searching|Sorting|Pagination Config
         */
        showMakeTransferRecipients: function(context) {
            if (context.recipients.length === 0) {
                if (context.searchString) {
                    this.showNoSearchResults();
                    return;
                }
                this.showNoRecipientsScreen();
                return;
            }
            this.showMakeTransferTabUI();
            var scopeObj = this;
            var accountSpecificData;
            var sortMap = [{
                    name: 'nickName',
                    imageFlx: this.view.WireTransferContainer.imgSortAccountName,
                    clickContainer: this.view.WireTransferContainer.flxAccountName
                },
                {
                    name: 'bankName',
                    imageFlx: this.view.WireTransferContainer.imgSortBankName,
                    clickContainer: this.view.WireTransferContainer.flxBankName
                }
            ];
            var break_point = kony.application.getCurrentBreakpoint();
            var MakeTransferData = {
                "btnAction": "btnAction",
                "flxAccountName": "flxAccountName",
                "flxAccountType": "flxAccountType",
                "flxBankName": "flxBankName",
                "flxDropdown": "flxDropdown",
                "flxMakeTransfersTransfersUnselected": "flxMakeTransfersTransfersUnselected",
                "flxRow": "flxRow",
                "imgDropdown": "imgDropdown",
                "lblAccountName": "lblAccountName",
                "lblAccountType": "lblAccountType",
                "lblBankName": "lblBankName",
                "lblSeparator": "lblSeparator",
                "flxWireTransferMakeTransfersSelected": "flxWireTransferMakeTransfersSelected",
                "lblIdentifier": "lblIdentifier",
                "flxIdentifier": "flxIdentifier",
                "lblAccountHolder": "lblAccountHolder",
                "lblAccountNumber": "lblAccountNumber",
                "lblRoutingNumber": "lblRoutingNumber",
                "lblAccountNumberValue": "lblAccountNumberValue",
                "lblAccountTypeValue": "lblAccountTypeValue",
                "lblAddedOnValue": "lblAddedOnValue",
                "lblSwiftCode": "lblSwiftCode",
                "lblIBANNo": "lblIBANNo",
                "lblBankDetailsTitle": "lblBankDetailsTitle",
                "lblRoutingNumberValue": "lblRoutingNumberValue",
                "lblIBANNumber": "lblIBANNumber",
                "lblBankAddressValue": "lblBankAddressValue",
                "lblRowSeperator": "lblRowSeperator"
            };
            var segData = context.recipients.map(function(dataItem) {
                var data = {
                    "lblAccountHolder": kony.i18n.getLocalizedString("i18n.WireTransfer.AccountHolder"),
                    "lblAccountNumberValue": dataItem.payeeName,
                    "lblAccountNumber": kony.i18n.getLocalizedString("i18n.common.accountNumber"),
                    "lblAccountTypeValue": dataItem.accountNumber,
                    "btnAction": scopeObj.setSegmentButtonsBasedOnEntitlements({
                        "isVisible": true,
                        "text": kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer"),
                        "toolTip": kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer"),
                        "onClick": scopeObj.onMakeTransferActionClick.bind(scopeObj, dataItem, null),
                        "type": dataItem.wireAccountType
                    }),
                    "imgDropdown": ViewConstants.IMAGES.ARRAOW_DOWN,
                    "lblAccountName": dataItem.payeeNickName,
                    "lblAccountType": dataItem.wireAccountType,
                    "lblBankName": dataItem.bankName,
                    "lblSeparator": " ",
                    "lblIdentifier": " ",
                    "lblBankDetailsTitle": "",
                    "lblBankAddressValue": "",
                    "lblSwiftCode": kony.i18n.getLocalizedString("i18n.transfers.bankDetails"),
                    "lblRoutingNumberValue": scopeObj.returnBankAddress(dataItem),
                    "template": (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) ? "flxMakeTransferWireTransfersMobile" : "flxMakeTransfersTransfersUnselected",
                };
                if (dataItem.wireAccountType === applicationManager.getConfigurationManager().OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC) {
                    accountSpecificData = {
                        "lblRoutingNumber": kony.i18n.getLocalizedString("i18n.accounts.routingNumber"),
                        "lblAddedOnValue": dataItem.routingCode,
                        "lblIBANNo": "",
                        "lblIBANNumber": ""
                    };
                } else {
                    accountSpecificData = {
                        "lblRoutingNumber": (IBANUtils.isCountrySupportsIBAN(dataItem.country)) ? kony.i18n.getLocalizedString("i18n.WireTransfer.IBAN") : kony.i18n.getLocalizedString("i18n.wireTransfer.IRC"),
                        "lblAddedOnValue": (IBANUtils.isCountrySupportsIBAN(dataItem.country)) ? dataItem.IBAN : dataItem.internationalRoutingCode,
                        "lblIBANNo": kony.i18n.getLocalizedString("i18n.accounts.swiftCode"),
                        "lblIBANNumber": dataItem.swiftCode
                    };
                }
                var dataKeys = Object.keys(accountSpecificData);
                dataKeys.forEach(function(key) {
                    data[key] = accountSpecificData[key];
                });
                return data;
            });
            var presentationController = this.getWireTransferModule().presentationController;
            this.view.WireTransferContainer.segWireTransfers.widgetDataMap = MakeTransferData;
            //             if(break_point == 640){
            //       			for(var i=0;i<segData.length;i++){
            //       			segData[i].template = "flxMakeTransferWireTransfersMobile";
            //       			}
            //             }
            this.view.WireTransferContainer.segWireTransfers.setData(segData);
            var showPagination = context.searchString === "" || context.searchString === null || context.searchString === undefined;
            if (showPagination) {
                this.updatePaginationValue(context.config, "i18n.transfers.external_accounts",
                    presentationController.getNextMakeTransferRecipients.bind(presentationController),
                    presentationController.getPrevioustMakeTransferRecipients.bind(presentationController)
                );
            } else {
                this.view.WireTransferContainer.tablePagination.setVisibility(false);
            }
            FormControllerUtility.setSortingHandlers(sortMap, this.makeTransferRecipientsSortingHandler.bind(this), this);
            FormControllerUtility.updateSortFlex(sortMap, context.config);
            this.configureSearch(context.searchString, presentationController.showMakeTransferRecipientList.bind(presentationController))
            this.AdjustScreen();
        },
        /**
         * Sorting Handler for make transfer recipients
         */
        makeTransferRecipientsSortingHandler: function(event, data) {
            this.getWireTransferModule().presentationController.showMakeTransferRecipientList(data);
        },
        /**
         * Show Make Transfer Recipients in Make Transfer Tab
         * @param {Payee[]} recipients Array of Payee Object
         * @param {object} config Searching|Sorting|Pagination Config
         */
        showManageTransferRecipients: function(context) {
            if (context.recipients.length === 0) {
                if (context.searchString) {
                    this.showNoSearchResults();
                    return;
                }
                this.showNoRecipientsScreen();
                return;
            }
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            this.showManageRecipientUI();
            var sortMap = [{
                    name: 'nickName',
                    imageFlx: this.view.WireTransferContainer.imgSortAccountName,
                    clickContainer: this.view.WireTransferContainer.flxAccountName
                },
                {
                    name: 'bankName',
                    imageFlx: this.view.WireTransferContainer.imgSortBankName,
                    clickContainer: this.view.WireTransferContainer.flxBankName
                }
            ];
            var scopeObj = this;
            var accountSpecificData;
            var break_point = kony.application.getCurrentBreakpoint();
            var ManageRecipientData = {
                "btnAction": "btnAction",
                "btnMakeTransfer": "btnMakeTransfer",
                "btnVewActivity": "btnVewActivity",
                "btnDelete": "btnDelete",
                "flxAccountName": "flxAccountName",
                "flxAccountType": "flxAccountType",
                "flxBankName": "flxBankName",
                "flxDropdown": "flxDropdown",
                "flxManageRecipientsSelected1": "flxManageRecipientsSelected1",
                "flxManageRecipientsUnselected1": "flxManageRecipientsUnselected1",
                "flxRow": "flxRow",
                "imgDropdown": "imgDropdown",
                "lblAccountName": "lblAccountName",
                "lblAccountType": "lblAccountType",
                "lblBankName": "lblBankName",
                "lblSeparator": "lblSeparator",
                "flxWireTransferMakeTransfersSelected": "flxWireTransferMakeTransfersSelected",
                "lblIdentifier": "lblIdentifier",
                "flxIdentifier": "flxIdentifier",
                "lblAccountHolder": "lblAccountHolder",
                "lblAccountNumber": "lblAccountNumber",
                "lblRoutingNumber": "lblRoutingNumber",
                "lblAccountNumberValue": "lblAccountNumberValue",
                "lblAccountTypeValue": "lblAccountTypeValue",
                "lblAddedOnValue": "lblAddedOnValue",
                "lblSwiftCode": "lblSwiftCode",
                "lblIBANNo": "lblIBANNo",
                "lblBankDetailsTitle": "lblBankDetailsTitle",
                "lblRoutingNumberValue": "lblRoutingNumberValue",
                "lblIBANNumber": "lblIBANNumber",
                "lblBankAddressValue": "lblBankAddressValue",
                "lblRowSeperator": "lblRowSeperator"
            };
            var segData = context.recipients.map(function(recipient, rowIndex) {
                var data = {
                    "lblAccountHolder": kony.i18n.getLocalizedString("i18n.WireTransfer.AccountHolder"),
                    "lblAccountNumberValue": recipient.payeeName,
                    "lblAccountNumber": kony.i18n.getLocalizedString("i18n.common.accountNumber"),
                    "lblAccountTypeValue": recipient.accountNumber,
                    "btnAction": scopeObj.setSegmentButtonsBasedOnEntitlements({
                        "isVisible": true,
                        "text": kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                        "toolTip": kony.i18n.getLocalizedString("i18n.PayAPerson.EditRecipientDetails"),
                        "onClick": scopeObj.onEditPayeeAction.bind(this, recipient, rowIndex),
                        "type": recipient.wireAccountType
                    }),
                    "btnMakeTransfer": scopeObj.setSegmentButtonsBasedOnEntitlements({
                        "isVisible": true,
                        "text": kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer"),
                        "toolTip": kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer"),
                        "onClick": scopeObj.onMakeTransferActionClick.bind(scopeObj, recipient, null, rowIndex),
                        "type": recipient.wireAccountType
                    }),
                    "btnVewActivity": scopeObj.setSegmentButtonsBasedOnEntitlements({
                        "isVisible": true,
                        "text": kony.i18n.getLocalizedString("i18n.transfers.viewActivity"),
                        "toolTip": kony.i18n.getLocalizedString("i18n.transfers.viewActivity"),
                        "onClick": scopeObj.fetchViewActivity.bind(scopeObj, recipient, rowIndex),
                        "type": recipient.wireAccountType
                    }),
                    "btnDelete": scopeObj.setSegmentButtonsBasedOnEntitlements({
                        "isVisible": true,
                        "text": kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                        "toolTip": kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                        "onClick": function() {
                            scopeObj.showConfirmDialog("i18n.transfers.deleteExternalAccount",
                                "i18n.PayAPerson.DeleteRecipientMessage",
                                scopeObj.deleteRecipient.bind(scopeObj, recipient, config),
                                "i18n.common.noDontDelete", "i18n.common.deleteRecipient");
                        },
                        "type": recipient.wireAccountType
                    }),
                    "imgDropdown": ViewConstants.IMAGES.ARRAOW_DOWN,
                    "lblAccountName": recipient.payeeNickName,
                    "lblAccountType": recipient.wireAccountType,
                    "lblBankName": recipient.bankName,
                    "lblSeparator": " ",
                    "lblIdentifier": " ",
                    "lblBankDetailsTitle": "",
                    "lblBankAddressValue": "",
                    "lblSwiftCode": kony.i18n.getLocalizedString("i18n.transfers.bankDetails"),
                    "lblRoutingNumberValue": scopeObj.returnBankAddress(recipient),
                    "template": "flxManageRecipientsUnselected1"
                };
                if (CommonUtilities.isCSRMode()) {
                    data.btnDelete.onClick = CommonUtilities.disableButtonActionForCSRMode();
                    data.btnDelete.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(13);
                }
                if (recipient.wireAccountType === OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC) {
                    accountSpecificData = {
                        "lblRoutingNumber": kony.i18n.getLocalizedString("i18n.accounts.routingNumber"),
                        "lblAddedOnValue": recipient.routingCode,
                        "lblIBANNo": "",
                        "lblIBANNumber": ""
                    };
                } else {
                    accountSpecificData = {
                        "lblRoutingNumber": (IBANUtils.isCountrySupportsIBAN(recipient.country)) ? kony.i18n.getLocalizedString("i18n.WireTransfer.IBAN") : kony.i18n.getLocalizedString("i18n.wireTransfer.IRC"),
                        "lblAddedOnValue": (IBANUtils.isCountrySupportsIBAN(recipient.country)) ? recipient.IBAN : recipient.internationalRoutingCode,
                        "lblIBANNo": kony.i18n.getLocalizedString("i18n.accounts.swiftCode"),
                        "lblIBANNumber": recipient.swiftCode
                    };
                }
                var dataKeys = Object.keys(accountSpecificData);
                dataKeys.forEach(function(key) {
                    data[key] = accountSpecificData[key];
                });
                return data;
            });
            this.view.WireTransferContainer.segWireTransfers.widgetDataMap = ManageRecipientData;
            if (break_point == 640 || orientationHandler.isMobile) {
                for (var i = 0; i < segData.length; i++) {
                    segData[i].template = "flxManageRecipientsWireTransfersMobile";
                }
            }
            this.view.WireTransferContainer.segWireTransfers.setData(segData);
            this.showExpandedRow(this.view.WireTransferContainer.segWireTransfers, "flxManageRecipientsSelected1", "flxManageRecipientsWireTransfersMobileSelected");
            // CommonUtilities.Sorting.setSortingHandlers(sortMap, this.onManageRecipientSorting.bind(this), this);
            FormControllerUtility.setSortingHandlers(sortMap, this.manageTransferRecipientsSortingHandler.bind(this), this);
            FormControllerUtility.updateSortFlex(sortMap, context.config);
            var showPagination = context.searchString === "" || context.searchString === null || context.searchString === undefined;
            var presentationController = this.getWireTransferModule().presentationController;
            if (showPagination) {
                this.updatePaginationValue(context.config, "i18n.transfers.external_accounts",
                    presentationController.getNextManageRecipients.bind(presentationController),
                    presentationController.getPreviousManageRecipients.bind(presentationController)
                );
            } else {
                this.view.WireTransferContainer.tablePagination.setVisibility(false);
            }
            this.configureSearch(context.searchString, presentationController.showManageRecipientList.bind(presentationController))
            this.AdjustScreen();
        },
        /**
         * Called when user clicks yes on confirm delete dialog
         */
        deleteRecipient: function(recipient) {
            this.getWireTransferModule().presentationController.deleteRecipient(recipient);
        },
        /**
         * Manage Transfer Recipient List Sorting Handler
         */
        manageTransferRecipientsSortingHandler: function(event, data) {
            this.getWireTransferModule().presentationController.showManageRecipientList(data);
        },
        /**
         * onClick Listener of Make Transfer Action on Make Transfer Segment
         * @param {Payee} payeeObject Payee Object Selected from segment
         */
        onMakeTransferActionClick: function(payeeObject, transactionObject, rowHistory) {
            if (typeof rowHistory === "number") {
                this.setExpandedRow(rowHistory)
            }
            this.getWireTransferModule().presentationController.showMakeTransferForPayee(payeeObject, transactionObject);
        },
        /**
         * onClick Listener of Make Transfer Action on Make Transfer Segment
         * @param {Payee} payeeObject Payee Object Selected from segment
         */
        onEditPayeeAction: function(payeeObject, row) {
            this.setExpandedRow(row);
            this.getWireTransferModule().presentationController.showEditPayee(payeeObject);
        },
        /**
         * Shows the UI of the make transfer tab
         */
        showMakeTransferTabUI: function() {
            this.resetUI();
            this.view.customheader.customhamburger.activateMenu("WIRE TRANSFER", "Make Transfer");
            //this.view.lblAddAccountHeading.setVisibility(false);
            this.view.flxWireTransfersWindow.setVisibility(true);
            this.view.lblAddAccountHeading.text = "Wire Transfer";
            this.view.customheader.lblHeaderMobile.text = this.view.lblAddAccountHeading.text;
            var text1 = kony.i18n.getLocalizedString("i18n.transfers.wireTransfer");
            var text2 = kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer");
            this.view.breadcrumb.setBreadcrumbData([{
                text: text1
            }, {
                text: text2
            }]);
            this.view.WireTransferContainer.btnMakeTransfer.skin = "sknBtnAccountSummarySelected";
            this.view.WireTransferContainer.btnRecent.skin = "sknBtnAccountSummaryUnselected";
            this.view.WireTransferContainer.btnManageRecipient.skin = "sknBtnAccountSummaryUnselected";
            this.showAllSeperatorsWireTransfer();
            this.view.WireTransferContainer.segWireTransfers.setVisibility(true);
            this.view.WireTransferContainer.flxTabsSeperator4.setVisibility(false);
            this.view.WireTransferContainer.flxSortMakeTransfers.setVisibility(true);
            this.view.WireTransferContainer.flxSortRecent.setVisibility(false);
            this.view.WireTransferContainer.flxSearchImage.setVisibility(true);
            this.view.flxTermsAndConditions.setVisibility(true);
            this.showRightBar("WireTransfersWindow");
        },
        /**
         * UI logic to manage recipient tab
         */
        showManageRecipientUI: function() {
            this.resetUI();
            var scopeObj = this;
            this.view.flxWireTransfersWindow.setVisibility(true);
            this.view.lblAddAccountHeading.text = "Wire Transfer";
            this.view.customheader.lblHeaderMobile.text = this.view.lblAddAccountHeading.text;
            this.view.customheader.customhamburger.activateMenu("WIRE TRANSFER", "My Recipients");
            //this.view.lblAddAccountHeading.setVisibility(false);
            scopeObj.view.breadcrumb.setBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.transfers.wireTransfer")
            }, {
                text: kony.i18n.getLocalizedString("i18n.PayAPerson.ManageRecipient")
            }]);
            this.view.WireTransferContainer.btnManageRecipient.skin = "sknBtnAccountSummarySelected";
            this.view.WireTransferContainer.btnRecent.skin = "sknBtnAccountSummaryUnselected";
            this.view.WireTransferContainer.btnMakeTransfer.skin = "sknBtnAccountSummaryUnselected";
            this.showAllSeperatorsWireTransfer();
            this.view.WireTransferContainer.segWireTransfers.setVisibility(true);
            this.view.WireTransferContainer.flxTabsSeperator1.setVisibility(false);
            this.view.WireTransferContainer.flxSortMakeTransfers.setVisibility(true);
            this.view.WireTransferContainer.flxSortRecent.setVisibility(false);
            this.view.WireTransferContainer.flxSearchImage.setVisibility(true);
            this.view.flxTermsAndConditions.setVisibility(true);
            this.showRightBar("WireTransfersWindow");
        },
        /**
         * UI logic to Change UI of right bar based on flow
         * @param {string} screenName The screen identifier
         */
        showRightBar: function(screenName) {
            var break_point = kony.application.getCurrentBreakpoint();
            if (break_point == 640 || break_point == 1024) {
                this.view.flxRightBar.setVisibility(false);
            } else {
                this.view.flxRightBar.setVisibility(true);
            }
            if (screenName === "noRecipient") {
                this.view.flxRightBar.top = "0dp";
                this.configureRightActionButton(['add', 'oneTimeTransfer']);
            } else if (screenName === "oneTimeTransfer") {
                this.view.flxRightBar.top = "0dp";
                this.configureRightActionButton(['add'])
            } else if (screenName === "addRecipient") {
                this.view.flxRightBar.top = "0dp";
                this.configureRightActionButton(['oneTimeTransfer'])
            } else if (screenName === "WireTransfersWindow") {
                this.view.flxRightBar.top = "0dp";
                this.configureRightActionButton(['add', 'oneTimeTransfer', 'inbound'])
            } else if (screenName === "inbound") {
                this.view.flxRightBar.top = "0dp";
                this.configureRightActionButton(['add', 'oneTimeTransfer'])
            }
        },
        /**
         * @param {object} data Data of the row
         * Configure the segment action buttons based on entitlements
         * @returns {Object} Returns the segment data for button
         */
        setSegmentButtonsBasedOnEntitlements: function(data) {
            var height = 13;
            if (data.type === applicationManager.getConfigurationManager().OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC) {
                if (applicationManager.getConfigurationManager().isDomesticWireTransferEnabled === "true") {
                    data.skin = "sknBtn1a98ffSSP13px";
                    return data;
                } else {
                    data.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(height);
                    data.onClick = function() {};
                    return data;
                }
            } else {
                if (applicationManager.getConfigurationManager().isInternationalWireTransferEnabled === "true") {
                    data.skin = "sknBtn1a98ffSSP13px";
                    return data;
                } else {
                    data.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(height);
                    data.onClick = function() {};
                    return data;
                }
            }
        },
        /**
         * Configure right action button based on a config
         * @param {string[]} config  config for buttons
         */
        configureRightActionButton: function(config) {
            if (config.constructor === Array) {
                var showAddButtons = config.indexOf('add') >= 0;
                var showMakeOneTimeTransfer = config.indexOf('oneTimeTransfer') >= 0;
                var showInboundButton = config.indexOf('inbound') >= 0;
                if (applicationManager.getConfigurationManager().isInternationalWireTransferEnabled === "false") {
                    this.view.flxAddNonKonyAccount.setVisibility(false);
                } else {
                    this.view.flxAddNonKonyAccount.setVisibility(showAddButtons);
                }
                if (applicationManager.getConfigurationManager().isDomesticWireTransferEnabled === "false") {
                    this.view.flxAddKonyAccount.setVisibility(false);
                } else {
                    this.view.flxAddKonyAccount.setVisibility(showAddButtons);
                }
                this.view.flxAddInternationalAccount.setVisibility(showMakeOneTimeTransfer);
                this.view.lblSeparator3.setVisibility(showMakeOneTimeTransfer);
                this.view.flxAccountInfoForAccountTransfer.setVisibility(showInboundButton);
            }
        },
        /**
         * Shows no account checking UI.
         */
        showNoCheckingAccountUI: function() {
            var userPreferencesManager = applicationManager.getUserPreferencesManager();
            this.view.NotEligibleToTransfer.txtMessage.text = kony.i18n.getLocalizedString("i18n.WireTransfer.Hi") +
                " " +
                userPreferencesManager.getUserName() +
                ", " +
                kony.i18n.getLocalizedString("i18n.WireTransfer.NotEligibleToTransfer");
            this.view.frmNotEligibleToTransfer.setVisibility(true);
            this.view.NotEligibleToTransfer.btnRequestMoney.onClick = function() {
                var naoModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("NAOModule");
                naoModule.presentationController.showNewAccountOpening();
            };
            this.view.NotEligibleToTransfer.btnNoThanks.onClick = this.NavigateToTransfers;
        },
        /**
         * Shows the seperators of tabs
         */
        showAllSeperatorsWireTransfer: function() {
            // this.view.WireTransferContainer.flxTabsSeperator1.setVisibility(true);
            // this.view.WireTransferContainer.flxTabsSeperator2.setVisibility(true);
            // this.view.WireTransferContainer.flxTabsSeperator3.setVisibility(true);
            this.view.WireTransferContainer.flxTabsSeperator4.setVisibility(false);
        },
        /**
         * Make Transfer Entry point for a payee
         * @param {Payee} payeeObject payeeObject for which transfer needs to be done
         * @param {Accounts[]} checkingAccounts Checking Accounts of the user.
         */
        showMakeTransferForPayee: function(context) {
            this.resetMakeTransferForm();
            this.fillPayeeDetailsForMakeTransfer(context.payee || context.transactionObject);
            this.selectedPayeeObject = context.payee || context.transactionObject;
            var makeTransfer = this.view.MakeTransfer;
            var listBoxData = FormControllerUtility.getListBoxDataFromObjects(context.checkingAccounts, "accountID", CommonUtilities.getAccountDisplayNameWithBalance);
            makeTransfer.lbxStep3From.masterData = listBoxData;
            makeTransfer.lbxStep3From.selectedKey = listBoxData[0][0];
            makeTransfer.lbxStep3Currency.masterData = FormControllerUtility.getListBoxDataFromObjects(this.getCurrency(), "symbol", "name");
            makeTransfer.lbxStep3Currency.selectedKey = this.returnKeyForListboxFromValue(makeTransfer.lbxStep3Currency, applicationManager.getConfigurationManager().OLBConstants.CURRENCY_NAME);
            makeTransfer.lbxStep3From.onSelection = this.onFromAccountChanged.bind(this, this.view.MakeTransfer, context.checkingAccounts);
            this.onFromAccountChanged(this.view.MakeTransfer, context.checkingAccounts);
            makeTransfer.tbxNote.onKeyUp = this.checkMakeTransferForm;
            makeTransfer.btnStepContinue.onClick = this.onMakeTransferContinue.bind(this);
            makeTransfer.btnStepCancel.onClick = this.onMakeTransferCancel.bind(this, context.metaData);
            makeTransfer.lbxStep3Currency.onSelection = this.onCurrencyChanged.bind(this, this.view.MakeTransfer.lblCurrencySymbol);
            this.onCurrencyChanged(this.view.MakeTransfer.lblCurrencySymbol, makeTransfer.lbxStep3Currency)
            this.showMakeTransferUI();
            if (context.transactionObject) {
                this.fillTransactionDetails(context.transactionObject);
            }
            this.checkMakeTransferForm();
        },
        onCurrencyChanged: function(label, listbox) {
            label.text = listbox.selectedKeyValue ? listbox.selectedKeyValue[0] : listbox.masterData[0][0];
        },
        /**
         * Show Make Transfer Repeat Details
         * @param {Object} transactionObject Transaction  Object for repeat
         * @param {Object} payee Payee  Object for repeat
         */
        fillTransactionDetails: function(transactionObject) {
            var makeTransfer = this.view.MakeTransfer;
            this.view.MakeTransfer.lbxStep3From.selectedKey = transactionObject.fromAccountNumber;
            makeTransfer.lbxStep3Currency.selectedKey = this.returnKeyForListboxFromValue(makeTransfer.lbxStep3Currency, transactionObject.payeeCurrency);
            makeTransfer.tbxAmount.text = CommonUtilities.formatCurrencyWithCommas(transactionObject.amount, true);
            makeTransfer.tbxNote.text = transactionObject.transactionsNotes;
            this.onCurrencyChanged(this.view.MakeTransfer.lblCurrencySymbol, makeTransfer.lbxStep3Currency);
        },
        /**
         * Make Transfer Cancel.
         * @param {object} config Pagination/Sorting Config
         */
        onMakeTransferCancel: function(config) {
            var scopeObj = this;
            var selectedTab = this.returnSelectedTab();
            if (selectedTab === "recent") {
                this.showConfirmDialog("i18n.PayAPerson.Quit", "I18n.billPay.QuitTransactionMsg", function() {
                    scopeObj.getWireTransferModule().presentationController.fetchWireTransactions(config);
                }, "i18n.common.dontCancelTransaction", "i18n.transfers.deleteTransfer");
            } else if (selectedTab === "manageRecipients") {
                this.showConfirmDialog("i18n.PayAPerson.Quit", "I18n.billPay.QuitTransactionMsg", function() {
                    scopeObj.getWireTransferModule().presentationController.fetchManageRecipientList(config);
                }, "i18n.common.dontCancelTransaction", "i18n.transfers.deleteTransfer");
            } else if (selectedTab === "makeTransfer") {
                this.showConfirmDialog("i18n.PayAPerson.Quit", "I18n.billPay.QuitTransactionMsg", function() {
                    scopeObj.getWireTransferModule().presentationController.fetchMakeTransferRecipientList(config);
                }, "i18n.common.dontCancelTransaction", "i18n.transfers.deleteTransfer");
            }
        },
        /**
         * Called when make transfer continue button is pressed.
         */
        onMakeTransferContinue: function() {
            this.enableTnC();
            this.showMakeTransferConfirmation(this.getWireTransferData());
        },
        /**
         * Checks and Returns selected tab
         * @returns {String} selectedTab
         */
        returnSelectedTab: function() {
            if (this.view.WireTransferContainer.btnRecent.skin === "sknBtnAccountSummarySelected") {
                return "recent";
            } else if (this.view.WireTransferContainer.btnManageRecipient.skin === "sknBtnAccountSummarySelected") {
                return "manageRecipients";
            } else {
                return "makeTransfer";
            }
        },
        /**
         * Returns the data for a Wire Transfer
         * @returns {object} Key value data for wire transfer
         */
        getWireTransferData: function() {
            this.view.MakeTransfer.lbxStep3Currency.selectedKey = this.view.MakeTransfer.lblCurrencySymbol.text;
            return {
                payeeObject: this.selectedPayeeObject,
                fromAccountName: this.view.MakeTransfer.lbxStep3From.selectedKeyValue[1],
                fromAccountNumber: this.view.MakeTransfer.lbxStep3From.selectedKeyValue[0],
                currencySymbol: this.view.MakeTransfer.lbxStep3Currency.selectedKeyValue[0],
                currency: this.view.MakeTransfer.lbxStep3Currency.selectedKeyValue[1],
                amount: this.makeTransferAmountField.getAmount(),
                transactionFee: applicationManager.getConfigurationManager().wireTranferFees,
                reasonForTransfer: this.view.MakeTransfer.tbxNote.text.trim()
            }
        },
        /**
         * Formats the amount label for confirmation for a given currency symbol
         * @param {string} currencySymbol Currency Symbol
         * @returns {string} Label for amount
         */
        getAmountLabelText: function(currencySymbol) {
            return kony.i18n.getLocalizedString("i18n.transfers.amountlabel") + "(" + currencySymbol + ") :";
        },
        /**
         * Checks the Wire Transfer form for completion and enable/disable the continue button.
         */
        checkMakeTransferForm: function() {
            if (this.makeTransferAmountField.isValidAmount() &&
                this.getWireTransferData().reasonForTransfer.length > 0 && this.makeTransferAmountField.getAmountValue() > 0
            ) {
                FormControllerUtility.enableButton(this.view.MakeTransfer.btnStepContinue);
            } else {
                FormControllerUtility.disableButton(this.view.MakeTransfer.btnStepContinue);
            }
        },
        /**
         * For a given payee object it tells if currency should be visible or not
         * @param {Payee} payeeObject payeeObject
         * @returns {boolean} Returns true if currency needs to be shown.
         */
        isCurrencyVisible: function(payeeObject) {
            return true; //multi-currency
            // var OLBConstants =  applicationManager.getConfigurationManager().OLBConstants;
            // return payeeObject.wireAccountType === OLBConstants.WireTransferConstants.ACCOUNT_INTERNATIONAL;
        },
        /**
         * Shows the make transfer confirmation
         * @param {object} makeTransferData Object containing data for tranfer
         */
        showMakeTransferConfirmation: function(makeTransferData) {
            var scopeObj = this;
            scopeObj.getWireTransferModule().presentationController.getTnC(makeTransferData.payeeObject.wireAccountType);
            this.showMakeTransferConfirmationUI();
            var payeeObject = makeTransferData.payeeObject;
            // Hiding Currency if payee is domestic
            this.view.confirmDetails.lblCurrencyKey.setVisibility(this.isCurrencyVisible(payeeObject));
            this.view.confirmDetails.rtxCurrencyValue.setVisibility(this.isCurrencyVisible(payeeObject));
            this.view.confirmDetails.lblFromAccountKey.text = kony.i18n.getLocalizedString("i18n.transfers.fromAccount") + ":";
            this.view.confirmDetails.rtxFromAccountValue.text = makeTransferData.fromAccountName;
            this.view.confirmDetails.lblToAccountKey.text = kony.i18n.getLocalizedString("i18n.transfers.lblTo") + ":";
            this.view.confirmDetails.rtxToAccountValue.text = payeeObject.payeeNickName;
            this.view.confirmDetails.lblAmountKey.text = this.getAmountLabelText(makeTransferData.currencySymbol);
            this.view.confirmDetails.rtxAmountValue.text = CommonUtilities.formatCurrencyWithCommas(makeTransferData.amount, true);
            this.view.confirmDetails.lblCurrencyKey.text = kony.i18n.getLocalizedString("i18n.common.Currency") + ":";
            this.view.confirmDetails.rtxCurrencyValue.text = makeTransferData.currency;
            this.view.confirmDetails.lblTransactionFeeKey.text = kony.i18n.getLocalizedString("i18n.WireTransfer.TransactionFee") + ":";
            this.view.confirmDetails.rtxTransactionFeeValue.text = CommonUtilities.formatCurrencyWithCommas(applicationManager.getConfigurationManager().wireTranferFees, false, makeTransferData.currencySymbol);
            this.view.confirmDetails.lblNoteKey.text = kony.i18n.getLocalizedString("i18n.transfers.Description") + ":";
            this.view.confirmDetails.rtxNoteValue.text = makeTransferData.reasonForTransfer;
            //recipient details
            this.view.confirmDetails.lblDetailsKey1.text = kony.i18n.getLocalizedString("i18n.transfers.benificiaryName") + " :";
            this.view.confirmDetails.rtxDetailsValue1.text = payeeObject.payeeName;
            this.view.confirmDetails.lblDetailsKey2.text = kony.i18n.getLocalizedString("i18n.WireTransfer.recipientType") + " :";
            this.view.confirmDetails.rtxDetailsValue2.text = payeeObject.type || payeeObject.payeeType;
            this.view.confirmDetails.lblDetailsKey3.text = kony.i18n.getLocalizedString("i18n.transfers.accountType") + " :";
            this.view.confirmDetails.rtxDetailsValue3.text = payeeObject.wireAccountType;
            this.view.confirmDetails.lblDetailsKey4.text = kony.i18n.getLocalizedString("i18n.WireTransfer.RecipientAddress") + " :";
            this.view.confirmDetails.rtxDetailsValue4.text = scopeObj.returnRecipientAddress(payeeObject);
            //bank details
            this.view.confirmDetails.lblBankDetailsKey3.text = kony.i18n.getLocalizedString("i18n.WireTransfer.recipientAccountNumber") + " :";
            this.view.confirmDetails.rtxBankDetailsValue3.text = payeeObject.accountNumber || payeeObject.payeeAccountNumber;
            this.view.confirmDetails.lblBankDetailsKey5.text = kony.i18n.getLocalizedString("i18n.transfers.accountNickName") + " :";
            this.view.confirmDetails.rtxBankDetailsValue5.text = payeeObject.payeeNickName;
            this.view.confirmDetails.lblBankDetailsKey6.text = kony.i18n.getLocalizedString("i18n.WireTransfer.recipientBank&Address") + " :";
            this.view.confirmDetails.rtxBankDetailsValue6.text = scopeObj.returnBankAddress(payeeObject, true);
            //location specific bank details
            if (payeeObject.wireAccountType === applicationManager.getConfigurationManager().OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC) {
                this.view.confirmDetails.lblBankDetailsKey1.text = kony.i18n.getLocalizedString("i18n.accounts.routingNumber") + " :";
                this.view.confirmDetails.rtxBankDetailsValue1.text = payeeObject.routingCode || payeeObject.routingNumber;
                this.view.confirmDetails.flxRow2.setVisibility(false);
            } else if (payeeObject.wireAccountType === applicationManager.getConfigurationManager().OLBConstants.WireTransferConstants.ACCOUNT_INTERNATIONAL) {
                this.view.confirmDetails.flxRow2.setVisibility(true);
                if (IBANUtils.isCountrySupportsIBAN(payeeObject.country)) {
                    this.view.confirmDetails.lblBankDetailsKey2.text = kony.i18n.getLocalizedString("i18n.WireTransfer.IBAN");
                    this.view.confirmDetails.rtxBankDetailsValue2.text = payeeObject.IBAN;
                } else {
                    this.view.confirmDetails.lblBankDetailsKey2.text = kony.i18n.getLocalizedString("i18n.WireTransfer.internationalRoutingCode") + " :";
                    this.view.confirmDetails.rtxBankDetailsValue2.text = payeeObject.internationalRoutingCode
                }
                this.view.confirmDetails.lblBankDetailsKey1.text = kony.i18n.getLocalizedString("i18n.accounts.swiftCode") + " :";
                this.view.confirmDetails.rtxBankDetailsValue1.text = payeeObject.swiftCode;
            }
            this.view.confirmDetails.btnCancel.onClick = this.onMakeTransferCancel.bind(this);
            this.view.confirmDetails.btnCancel.toolTip = kony.i18n.getLocalizedString('i18n.transfers.Cancel');
            this.view.confirmDetails.btnModify.onClick = this.modifyWireTransfer.bind(this);
            this.view.confirmDetails.btnModify.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Modify");
            this.view.confirmDetails.btnConfirm.onClick = this.onMakeTransfer;
            this.view.confirmDetails.btnConfirm.toolTip = kony.i18n.getLocalizedString('i18n.common.confirmTransaction');
            this.AdjustScreen();
        },

        disableTnC: function() {
            this.view.confirmDetails.flxAgree.setVisibility(false);
            CommonUtilities.enableButton(this.view.confirmDetails.btnConfirm);
        },
        enableTnC: function() {
            CommonUtilities.disableButton(this.view.confirmDetails.btnConfirm);
            this.view.confirmDetails.flxAgree.setVisibility(true);
        },
        /**
         * Modify Wire Transfer
         */
        modifyWireTransfer: function() {
            this.disableTnC();
            this.showMakeTransferUI();
            this.checkMakeTransferForm();
        },
        /**
         * onClick listener for make transfer on confirmation flex
         */
        onMakeTransfer: function() {
            this.disableTnC();
            var transferData = this.getWireTransferData();
            this.getWireTransferModule().presentationController.authorizeWireTransfer(transferData);
        },
        /**
         * UI logic for showing confirmation screen for make transfer
         */
        showMakeTransferConfirmationUI: function() {
            this.resetUI();
            this.view.confirmDetails.flxRow4.setVisibility(false);
            this.view.confirmDetails.flxTransactionDetails.setVisibility(true);
            this.view.flxSuccessMessage.setVisibility(true);
            this.view.flxPrint.setVisibility(false);
            this.view.flxDownload.setVisibility(false);
            this.view.confirmDetails.lblConfirmHeader.text = kony.i18n.getLocalizedString("i18n.billPay.TransactionDetails");
            var text1 = kony.i18n.getLocalizedString("i18n.transfers.wireTransfer");
            var text2 = kony.i18n.getLocalizedString("i18n.WireTransfer.MAKETRANSFERCONFIRMATION");
            this.view.breadcrumb.setBreadcrumbData([{
                text: text1
            }, {
                text: text2
            }]);
            //this.view.lblAddAccountHeading.setVisibility(true);
            this.view.flxSuccessMessage.setVisibility(false);
            this.view.flxbuttons.setVisibility(false);
            this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString("i18n.WireTransfer.makeTransferConfirm");
            this.view.customheader.lblHeaderMobile.text = this.view.lblAddAccountHeading.text;
            this.view.confirmDetails.btnModify.setVisibility(true);
            this.view.confirmDetails.btnModify.text = kony.i18n.getLocalizedString('i18n.transfers.Modify');
            this.view.confirmDetails.flxStep2Buttons.setVisibility(true);
            // this.view.confirmDetails.btnCancel.right = "510dp";
            this.view.confirmDetails.btnCancel.setVisibility(true);
            this.view.flxTermsAndConditions.setVisibility(false);
            this.view.flxSuccessMessage.setVisibility(false);
            this.view.flxConfirmDetails.setVisibility(true);
            this.view.flxHeader.setFocus(true);
            this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString("i18n.WireTransfer.makeTransferConfirm");
            this.view.customheader.lblHeaderMobile.text = this.view.lblAddAccountHeading.text;
            this.view.confirmDetails.flxRecipientDetails.info.frame.height = this.view.confirmDetails.flxBankDetails.info.frame.height;
            this.AdjustScreen();
        },
        /**
         * Resets the make transfer form.
         */
        resetMakeTransferForm: function() {
            var makeTransfer = this.view.MakeTransfer;
            FormControllerUtility.disableButton(makeTransfer.btnStepContinue);
            makeTransfer.tbxAmount.text = "";
            makeTransfer.tbxNote.text = "";
        },
        /**
         * Fills the payee details in make transfer form
         * @param {Payee} payeeObject Payee object for transfer
         */
        fillPayeeDetailsForMakeTransfer: function(payeeObject) {
            var makeTransfer = this.view.MakeTransfer;
            makeTransfer.lblStep3To.text = kony.i18n.getLocalizedString("i18n.transfers.lblTo") + " : " + payeeObject.payeeNickName;
            makeTransfer.lblStep3Bank.text = kony.i18n.getLocalizedString("i18n.CheckImages.Bank") + " : " + payeeObject.bankName;
            makeTransfer.flxStep3Currency.isVisible = (payeeObject.wireAccountType === applicationManager.getConfigurationManager().OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC) ? false : true;
        },
        /**
         * Ui logic for showing make transfer form.
         */
        showMakeTransferUI: function() {
            this.resetUI();
            this.view.customheader.customhamburger.activateMenu("WIRE TRANSFER");
            //this.view.lblAddAccountHeading.setVisibility(false);
            this.showRightBar("WireTransfersWindow");
            var makeTransfer = this.view.MakeTransfer;
            this.view.flxTermsAndConditions.setVisibility(true);
            // makeTransfer.height = "preferred";
            var text1 = kony.i18n.getLocalizedString("i18n.transfers.wireTransfer");
            var text2 = kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer");
            this.view.breadcrumb.setBreadcrumbData([{
                text: text1
            }, {
                text: text2
            }]);
            this.view.flxMakeTransfer.setVisibility(true);
            this.view.flxHeader.setFocus(true);
            this.view.lblAddAccountHeading.text = "Wire Transfer";
            this.view.customheader.lblHeaderMobile.text = this.view.lblAddAccountHeading.text;
            makeTransfer.tbxNote.placeholder = kony.i18n.getLocalizedString("i18n.wireTransfer.enterReasonForTransfer");
            this.AdjustScreen();
        },
        /**
         * Show wire transfer acknowledgement screen
         * @param {string} acknowledgementData -Acknowledgement Data to show
         * @param {string} acknowledgementData.referenceId - referenceId of the transaction
         */
        showWireTransferAcknowledgement: function(acknowledgementData) {
            this.view.lblRefrenceNumber.setVisibility(true);
            this.view.lblRefrenceNumberValue.setVisibility(true);
            this.view.lblRefrenceNumberValue.text = acknowledgementData.referenceId;
            this.view.btnMakeAnotherWireTransfer.text = kony.i18n.getLocalizedString("i18n.WireTranfers.MakeAnotherWireTransfer");
            this.view.btnMakeAnotherWireTransfer.toolTip = kony.i18n.getLocalizedString("i18n.WireTranfers.MakeAnotherWireTransfer");
            this.view.btnViewSentTransactions.text = kony.i18n.getLocalizedString("i18n.payaperson.viewSentTrasactions");
            this.view.btnViewSentTransactions.toolTip = kony.i18n.getLocalizedString("i18n.common.viewRecentTransaction");
            this.view.confirmDetails.flxStep2Buttons.setVisibility(false);
            this.view.flxbuttons.setVisibility(true);
            this.view.btnSaveRecipient.setVisibility(false);
            //  this.view.lblAddAccountHeading.setVisibility(true);
            this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString("i18n.wiretransfers.makeTransferAck");
            this.view.customheader.lblHeaderMobile.text = this.view.lblAddAccountHeading.text;
            if (CommonUtilities.isPrintEnabled()) {
                this.view.flxPrint.setVisibility(true);
                this.view.lblPrintfontIconConfirm.onTouchStart = this.onClickPrint;
            } else {
                this.view.flxPrint.setVisibility(false);
            }
            this.view.flxSuccessMessage.setVisibility(true);
            this.view.breadcrumb.setBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.transfers.wireTransfer")
            }, {
                text: kony.i18n.getLocalizedString("i18n.wireTransfer.MAKETRANSFERACK")
            }]);
            this.view.confirmDetails.flxBankDetails.info.frame.height = this.view.confirmDetails.flxRecipientDetails.info.frame.height;
            this.view.btnViewSentTransactions.onClick = this.onRecentTabClick.bind(this);
            this.view.btnMakeAnotherWireTransfer.onClick = this.onMakeTransferTabClick.bind(this);
            this.view.lblSuccessAcknowledgement.text = kony.i18n.getLocalizedString("i18n.transfers.AcknowledgementMessage");
            this.view.flxHeader.setFocus(true);
            this.AdjustScreen();
        },
        /**
         * Entry Point method for One time transfer acknowledgement
         * @param {object} response - Data of the transfer
         */
        showOneTimeTransferAcknowledgement: function(response) {
            var scopeObj = this;
            var data = response.transferData;
            this.showOneTimeTransferAcknowledgementUI(data);
            this.view.lblRefrenceNumberValue.text = response.referenceId;
            this.view.btnMakeAnotherWireTransfer.onClick = this.onOneTimeTransferActionClick.bind(this);
            this.view.btnSaveRecipient.onClick = function() {
                this.confirmAddRecipient(data, data.recipientDetails.wireAccountType, function() {
                    this.getWireTransferModule().presentationController.showWireTransfer();
                }.bind(this), this.onSavePayeeAfterWireTransfer.bind(this, response.referenceId, data), true);
            }.bind(this);
            this.view.btnViewSentTransactions.onClick = function() {
                scopeObj.getWireTransferModule().presentationController.fetchWireTransactions();
            };
            this.AdjustScreen();
        },
        /**
         * Save Payee After Wire Transfer
         */
        onSavePayeeAfterWireTransfer: function(transactionId, data) {
            this.getWireTransferModule().presentationController.savePayeeAfterTransfer(transactionId, data);
        },
        /**
         * Show One time transfer ackonwledgement UI - Sets heading text and breadcrumb
         * @member frmWireTransferController
         * @param {object} data - Updated Config
         */
        showOneTimeTransferAcknowledgementUI: function() {
            // this.view.lblAddAccountHeading.setVisibility(true);
            if (kony.application.getCurrentBreakpoint() == 640 || kony.application.getCurrentBreakpoint() == 1024) {
                this.view.flxPrint.setVisibility(false);
            } else {
                this.view.flxPrint.setVisibility(true);
            }
            // this.view.flxDownload.setVisibility(true);
            this.view.flxSuccessMessage.setVisibility(true);
            this.view.btnSaveRecipient.setVisibility(true);
            this.view.btnSaveRecipient.toolTip = kony.i18n.getLocalizedString('i18n.common.saveThisRecipient');
            this.view.btnMakeAnotherWireTransfer.text = kony.i18n.getLocalizedString('i18n.WireTranfers.MakeAnotherWireTransfer');
            this.view.btnMakeAnotherWireTransfer.toolTip = kony.i18n.getLocalizedString('i18n.WireTranfers.MakeAnotherWireTransfer');
            this.view.btnViewSentTransactions.text = kony.i18n.getLocalizedString('i18n.payaperson.viewSentTrasactions');
            this.view.btnViewSentTransactions.toolTip = kony.i18n.getLocalizedString("i18n.common.viewRecentTransaction");
            this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString('i18n.wiretransfers.makeTransferAck');
            this.view.customheader.lblHeaderMobile.text = this.view.lblAddAccountHeading.text;
            this.view.breadcrumb.setBreadcrumbData([{
                text: kony.i18n.getLocalizedString("iupdateHamburgerMenu18n.WireTransfer.WireTransfer")
            }, {
                text: kony.i18n.getLocalizedString('i18n.wireTransfers.makeOneTimeTransferAck')
            }]);
            this.view.lblSuccessAcknowledgement.text = kony.i18n.getLocalizedString("i18n.transfers.AcknowledgementMessage");
            this.view.confirmDetails.lblConfirmHeader.text = kony.i18n.getLocalizedString('i18n.transfers.Acknowledgement');
            this.view.lblRefrenceNumber.setVisibility(true);
            this.view.lblRefrenceNumberValue.setVisibility(true);
            this.view.flxbuttons.setVisibility(true);
            this.view.confirmDetails.flxStep2Buttons.setVisibility(false);
            this.view.confirmDetails.flxRecipientDetails.info.frame.height = this.view.confirmDetails.flxBankDetails.info.frame.height;
            this.view.flxHeader.setFocus(true);
            this.view.forceLayout();
        },
        /**
         * Called on Click of Recent Tab
         */
        onRecentTabClick: function() {
            this.getWireTransferModule().presentationController.showRecentTransactions();
            this.view.WireTransferContainer.lblSortType.text = kony.i18n.getLocalizedString('i18n.transfers.amountlabel');
            //this.view.WireTransferContainer.Search.txtSearch.setFocus(true);
        },
        /**
         * Called on Click of Recent Tab
         */
        onManageTabClick: function() {
            this.getWireTransferModule().presentationController.showManageRecipientList();
            //this.view.WireTransferContainer.Search.txtSearch.setFocus(true);
        },
        /**
         * Called on Click of Make Transfer Tab
         */
        onMakeTransferTabClick: function() {
            this.getWireTransferModule().presentationController.showMakeTransferRecipientList();
            //this.view.WireTransferContainer.Search.txtSearch.setFocus(true);
        },
        /**
         * Bind segment data for Recent tab
         * @param {Transaction[]} transactions Array of transaction Object
         * @param {object} config Searching|Sorting|Pagination Config
         */
        showRecentTransactions: function(context) {
            if (context.transactions.length === 0) {
                this.showNoTransactionsScreen();
                return;
            }
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            this.showRecentTabUI();
            var sortMap = [{
                    name: 'transactionDate',
                    imageFlx: this.view.WireTransferContainer.imgSortDate,
                    clickContainer: this.view.WireTransferContainer.flxSortDate
                },
                {
                    name: 'nickName',
                    imageFlx: this.view.WireTransferContainer.imgSortSentTo,
                    clickContainer: this.view.WireTransferContainer.flxSortSentTo
                },
                {
                    name: 'amount',
                    imageFlx: this.view.WireTransferContainer.imgSortType,
                    clickContainer: this.view.WireTransferContainer.flxSortAmount
                }
            ];
            var scopeObj = this;
            var break_point = kony.application.getCurrentBreakpoint();
            var widgetDataMap = {
                "btnAction": "btnAction",
                "flxAmount": "flxAmount",
                "flxDate": "flxDate",
                "flxDropdown": "flxDropdown",
                "flxRecentWireTransfers": "flxRecentWireTransfers",
                "flxRow": "flxRow",
                "flxSendTo": "flxSendTo",
                "flxIdentifier": "flxIdentifier",
                "lblIdentifier": "lblIdentifier",
                "imgDropdown": "imgDropdown",
                "lblAmount": "lblAmount",
                "lblDate": "lblDate",
                "lblSendTo": "lblSendTo",
                "lblFrom": "lblFrom",
                "lblReferenceNumberValue": "lblReferenceNumberValue",
                "lblReferenceNumber": "lblReferenceNumber",
                "lblFromAccountValue": "lblFromAccountValue",
                "lblRoutingNumber": "lblRoutingNumber",
                "lblStatusValue": "lblStatusValue",
                "lblAccountNumber": "lblAccountNumber",
                "lblRecurrenceValue": "lblRecurrenceValue",
                "lblAccountType": "lblAccountType",
                "lblTransactionFee": "lblTransactionFee",
                "lblTransferCurrency": "lblTransferCurrency",
                "lblAmountReceived": "lblAmountReceived",
                "lblTransactionFeeSymbol": "lblTransactionFeeSymbol",
                "lblTransactionFeeValue": "lblTransactionFeeValue",
                "lblTransferCurrencyValue": "lblTransferCurrencyValue",
                "lblCurrencySymbol": "lblCurrencySymbol",
                "lblCurrencyValue": "lblCurrencyValue",
                "lblNoteValue": "lblNoteValue",
                "lblBankDetailsTitle": "lblBankDetailsTitle",
                "lblBankAddressValue": "lblBankAddressValue",
                "lblSeparator": "lblSeparator",
                "lblSeparator1": "lblSeparator1"
            };
            var data = context.transactions.map(function(dataItem) {
                return {
                    "btnAction": scopeObj.setSegmentButtonsBasedOnEntitlements({
                        "isVisible": (dataItem.isPayeeDeleted === "true") ? false : true,
                        "text": kony.i18n.getLocalizedString("i18n.accounts.repeat"),
                        "toolTip": kony.i18n.getLocalizedString("i18n.common.repeatThisTransaction"),
                        "onClick": scopeObj.onMakeTransferActionClick.bind(this, null, dataItem),
                        "type": dataItem.wireAccountType
                    }),
                    "imgDropdown": ViewConstants.IMAGES.ARRAOW_DOWN,
                    "lblDate": CommonUtilities.getFrontendDateString(dataItem.transactionDate),
                    "lblSendTo": dataItem.payeeNickName ? dataItem.payeeNickName : dataItem.payeeName,
                    "lblAmount": CommonUtilities.formatCurrencyWithCommas(dataItem.amount, false, dataItem.currencyCode),
                    "lblFrom": kony.i18n.getLocalizedString("i18n.transfers.lblFrom"),
                    "lblReferenceNumberValue": CommonUtilities.getAccountDisplayName({
                        name: dataItem.fromAccountName,
                        accountID: dataItem.fromAccountNumber,
                        nickName: dataItem.fromNickName,
                        Account_id: dataItem.fromAccountNumber
                    }),
                    "lblReferenceNumber": kony.i18n.getLocalizedString('i18n.transfers.RefrenceNumber'),
                    "lblFromAccountValue": dataItem.transactionId,
                    "lblRoutingNumber": (dataItem.wireAccountType === OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC) ? kony.i18n.getLocalizedString('i18n.accounts.routingNumber') : (IBANUtils.isCountrySupportsIBAN(dataItem.country)) ? kony.i18n.getLocalizedString("i18n.WireTransfer.IBAN") : kony.i18n.getLocalizedString("i18n.wireTransfer.IRC"),
                    "lblStatusValue": (dataItem.wireAccountType === OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC) ? dataItem.routingNumber : (IBANUtils.isCountrySupportsIBAN(dataItem.country)) ? dataItem.IBAN : dataItem.internationalRoutingCode,
                    "lblAccountNumber": kony.i18n.getLocalizedString("i18n.common.accountNumber"),
                    "lblRecurrenceValue": dataItem.fromAccountNumber,
                    "lblAccountType": kony.i18n.getLocalizedString('i18n.transfers.accountType'),
                    "lblNoteValue": dataItem.wireAccountType,
                    "lblBankDetailsTitle": kony.i18n.getLocalizedString("i18n.transfers.bankDetails"),
                    "lblBankAddressValue": scopeObj.returnBankAddress(dataItem),
                    "lblSeparator": ".",
                    "lblTransactionFee": kony.i18n.getLocalizedString("i18n.WireTransfer.TransactionFee"),
                    "lblTransferCurrency": {
                        "isVisible": scopeObj.isCurrencyVisible(dataItem),
                        "text": kony.i18n.getLocalizedString("i18n.WireTransfer.TransferCurrency")
                    },
                    "lblAmountReceived": {
                        "isVisible": scopeObj.isCurrencyVisible(dataItem),
                        "text": kony.i18n.getLocalizedString("i18n.WireTransfer.AmountReceived")
                    },
                    "lblTransactionFeeSymbol": applicationManager.getConfigurationManager().getCurrencyCode(),
                    "lblTransactionFeeValue": CommonUtilities.formatCurrencyWithCommas(dataItem.fee, true),
                    "lblTransferCurrencyValue": {
                        "isVisible": scopeObj.isCurrencyVisible(dataItem),
                        "text": dataItem.payeeCurrency
                    },
                    "lblIdentifier": ".",
                    "lblCurrencySymbol": {
                        "isVisible": scopeObj.isCurrencyVisible(dataItem),
                        "text": scopeObj.returnCurrencySymbolFromValue(scopeObj.getCurrency(), dataItem.payeeCurrency)
                    },
                    "lblCurrencyValue": {
                        "isVisible": scopeObj.isCurrencyVisible(dataItem),
                        "text": CommonUtilities.formatCurrencyWithCommas(dataItem.amountRecieved, true)
                    },
                    "template": "flxRecentWireTransfers"
                }
            });
            this.view.WireTransferContainer.segWireTransfers.widgetDataMap = widgetDataMap;
            if (break_point == 640) {
                for (var i = 0; i < data.length; i++) {
                    data[i].template = "flxRecentsWireTransfersMobile";
                }
            }
            this.view.WireTransferContainer.segWireTransfers.setData(data);
            FormControllerUtility.setSortingHandlers(sortMap, this.recentTransactionSortingHandler.bind(this), this);
            FormControllerUtility.updateSortFlex(sortMap, context.config);
            var presentationController = this.getWireTransferModule().presentationController;
            this.updatePaginationValue(
                context.config,
                "i18n.common.transactions",
                presentationController.getNextWireTransactions.bind(presentationController),
                presentationController.getPreviousWireTransactions.bind(presentationController)
            )
            //this.view.WireTransferContainer.flxSearch.setVisibility(false);
            this.AdjustScreen();
        },
        /**
         * Display No Transaction Screen - UI logic
         * @member frmWireTransferController
         * @returns {void} - None
         * @throws {void} - None
         */
        showNoTransactionsScreen: function() {
            this.showRecentTabUI();
            this.view.WireTransferContainer.flxSearch.setVisibility(false);
            this.view.WireTransferContainer.segWireTransfers.setVisibility(false);
            this.view.WireTransferContainer.rtxNoPaymentMessage.text = kony.i18n.getLocalizedString('i18n.transfers.noTransactions');
            this.view.WireTransferContainer.flxNoTransactions.setVisibility(true);
            this.view.WireTransferContainer.lblScheduleAPayment.onTouchEnd = function() {
                this.getWireTransferModule().presentationController.showWireTransfer();
            }.bind(this);
            this.view.forceLayout();
        },
        /**
         * Recent Transaction Sorting Handler
         */
        recentTransactionSortingHandler: function(event, data) {
            this.getWireTransferModule().presentationController.showRecentTransactions(data);
        },
        /**
         * UI logic to show recent tab
         */
        showRecentTabUI: function() {
            this.resetUI();
            this.view.customheader.customhamburger.activateMenu("WIRE TRANSFER", "History");
            //this.view.lblAddAccountHeading.setVisibility(false);
            this.view.flxWireTransfersWindow.setVisibility(true);
            this.view.lblAddAccountHeading.text = "Wire Transfer";
            this.view.customheader.lblHeaderMobile.text = this.view.lblAddAccountHeading.text;
            this.view.breadcrumb.setBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.transfers.wireTransfer")
            }, {
                text: kony.i18n.getLocalizedString("i18n.transfers.recent")
            }]);
            this.view.WireTransferContainer.btnRecent.skin = "sknBtnAccountSummarySelected";
            this.view.WireTransferContainer.btnMakeTransfer.skin = "sknBtnAccountSummaryUnselected";
            this.view.WireTransferContainer.btnManageRecipient.skin = "sknBtnAccountSummaryUnselected";
            this.showAllSeperatorsWireTransfer();
            this.view.flxTermsAndConditions.setVisibility(true);
            this.view.WireTransferContainer.segWireTransfers.setVisibility(true);
            this.view.WireTransferContainer.flxTabsSeperator4.setVisibility(false);
            this.view.WireTransferContainer.flxTabsSeperator1.setVisibility(false);
            this.view.WireTransferContainer.flxSortMakeTransfers.setVisibility(false);
            this.view.WireTransferContainer.flxSortRecent.setVisibility(true);
            this.view.WireTransferContainer.flxSearchImage.setVisibility(false);
            this.showRightBar("WireTransfersWindow");
        },
        /**
         * Save Recipient Server error
         * @param {object} viewModel Data from server
         */
        showSaveRecipientServerError: function(viewModel) {
            this.modifyAddRecipient(viewModel.type);
            this.showServerError(viewModel.errorMessage);
        },
        /**
         * Shows Add Payee
         * @param {Object} addPayeeData Initial Data needed for showing add payee
         * @param {Object[]} addPayeeData.states List of states
         * @param {Object[]} addPayeeData.countries List of Countries. Only Needed for international Recipient
         */
        showAddPayee: function(addPayeeData) {
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            if (addPayeeData.type === OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC) {
                this.showDomesticAddForm(addPayeeData.states);
            } else {
                this.showInternationalAddForm(addPayeeData.states, addPayeeData.countries);
            }
        },
        /**
         * Shows Domestic recipient add form
         * @param {Object[]} states List of states
         * @param {function} onCancel for cancel button
         */
        showDomesticAddForm: function(states, onCancel) {
            var scopeObj = this;
            if (onCancel === undefined || typeof onCancel !== "function") {
                onCancel = function() {
                    scopeObj.getWireTransferModule().presentationController.showWireTransfer();
                };
            }
            this.view.AddRecipientAccount.btnCancel.onClick = onCancel;
            this.ShowDomesticAccountUI();
            this.resetAddRecipientForms();
            var stateListBoxData = FormControllerUtility.getListBoxDataFromObjects(states, 'region_Name', 'region_Name')
            this.view.AddRecipientAccount.lbxState.masterData = stateListBoxData;
            this.view.AddRecipientAccount.lbxBankState.masterData = stateListBoxData;
            this.view.AddRecipientAccount.flxTypeRadio1.onClick = function() {
                scopeObj.RadioBtnAction(scopeObj.view.AddRecipientAccount.imgRadioBtnRecipientType1, scopeObj.view.AddRecipientAccount.imgRadioBtnRecipientType2);
            };
            this.view.AddRecipientAccount.flxTypeRadio2.onClick = function() {
                scopeObj.RadioBtnAction(scopeObj.view.AddRecipientAccount.imgRadioBtnRecipientType2, scopeObj.view.AddRecipientAccount.imgRadioBtnRecipientType1);
            };
            this.view.AddRecipientAccount.btnCancel.onClick = this.showConfirmDialog.bind(this, "i18n.PayAPerson.Quit", "i18n.wireTransfers.cancelAddRecipient", onCancel, "i18n.Wiretranfer.no", "i18n.Wiretranfer.yes");
            this.view.AddRecipientAccount.btnProceed.onClick = this.showDomesticAccountDetailsForm.bind(this, onCancel);
        },
        /**
         * Shows Domestic recipient add form - step 2
         * @param {function} [onCancel] Optional Listener for cancewl button
         */
        showDomesticAccountDetailsForm: function(onCancel) {
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            this.setAddRecipientsStep(ADD_RECIPIENTS_STEPS.RECIPIENT_ACCOUNT_DETAILS);
            this.hideErrorFlexAddRecipient();
            this.view.AddRecipientAccount.tbxAccountNumber.onBeginEditing = this.hideErrorFlexAddRecipient.bind(this);
            this.view.AddRecipientAccount.tbxReAccountNumber.onBeginEditing = this.hideErrorFlexAddRecipient.bind(this);
            this.view.AddRecipientAccount.btnBack.onClick = this.setAddRecipientsStep.bind(this, ADD_RECIPIENTS_STEPS.RECIPIENT_DETAILS);
            this.view.AddRecipientAccount.btnStep2Cancel.onClick = this.showConfirmDialog.bind(this, "i18n.PayAPerson.Quit", "i18n.wireTransfers.cancelAddRecipient", onCancel, "i18n.common.noDontCancel", "i18n.transfers.Cancel");
            this.view.AddRecipientAccount.btnAddRecipent.onClick = function() {
                if (this.validateAddRecipientForm()) {
                    this.confirmAddRecipient(this.getFullRecipientData(), OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC, onCancel);
                }
            }.bind(this);
        },
        /**
         * UI logic for  add domestic recipient form
         */
        ShowDomesticAccountUI: function() {
            this.showAddRecipientsScreen();
            this.showRoutingCodeField();
            this.view.AddRecipientAccount.btnDomesticAccount.skin = "sknBtnAccountSummarySelected";
            if (applicationManager.getConfigurationManager().isInternationalWireTransferEnabled !== "false") {
                this.view.AddRecipientAccount.btnInternationalAccount.skin = "sknBtnAccountSummaryUnselected";
            }
            this.ShowAllSeperators();
            this.view.breadcrumb.setBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.transfers.wireTransfer")
            }, {
                text: kony.i18n.getLocalizedString("i18n.WireTransfers.AddRecipientDomestic")
            }]);
            this.view.AddRecipientAccount.flxTabsSeperator3.setVisibility(false);
            this.view.lblAddAccountHeading.text = "Add Recipient";
            this.view.customheader.lblHeaderMobile.text = this.view.lblAddAccountHeading.text;
            //this.view.lblAddAccountHeading.setVisibility(false);
            this.view.AddRecipientAccount.flxCountry.setVisibility(false);
            this.view.AddRecipientAccount.lblIBANOrIRC.setVisibility(false);
            this.view.AddRecipientAccount.tbxIBANOrIRC.setVisibility(false);
            this.setAddRecipientsStep(ADD_RECIPIENTS_STEPS.RECIPIENT_DETAILS);
        },
        /**
         * UI logic for  add  recipient form
         */
        showAddRecipientsScreen: function() {
            this.resetUI();
            this.view.customheader.customhamburger.activateMenu("WIRE TRANSFER", "Add Recipient");
            this.view.flxAddRecipientsWindow.setVisibility(true);
            this.view.flxAddKonyAccount.setVisibility(false);
            this.view.flxAddNonKonyAccount.setVisibility(false);
            this.view.flxTermsAndConditions.setVisibility(true);
            this.view.flxAddInternationalAccount.setVisibility(true);
            this.showRightBar("addRecipient");
            this.view.lblAddAccountHeading.text = "Add Recipient";
            this.view.customheader.lblHeaderMobile.text = this.view.lblAddAccountHeading.text;
        },
        /**
         * Toggles between steps of add recipient form
         * @param {number} step - Step Number 1 or 2
         */
        setAddRecipientsStep: function(step) {
            this.view.AddRecipientAccount.flxStep1.setVisibility(false);
            this.view.AddRecipientAccount.flxStep2.setVisibility(false);
            this.view.AddRecipientAccount["flxStep" + step].setVisibility(true);
            this.AdjustScreen();
        },
        /**
         * Resets the fields of add recipient form.
         */
        resetAddRecipientForms: function() {
            this.view.AddRecipientAccount.tbxRecipientName.text = "";
            this.view.AddRecipientAccount.imgRadioBtnRecipientType1.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO;
            this.view.AddRecipientAccount.imgRadioBtnRecipientType2.text = 'L';
            this.view.AddRecipientAccount.tbxAddressLine1.text = ""
            this.view.AddRecipientAccount.tbxAddressLine2.text = ""
            this.view.AddRecipientAccount.tbxCity.text = ""
            this.view.AddRecipientAccount.tbxZipcode.text = "";
            this.view.AddRecipientAccount.tbxSwiftCode.text = "";
            this.view.AddRecipientAccount.tbxIBANOrIRC.text = "";
            this.view.AddRecipientAccount.tbxAccountNumber.text = "";
            this.view.AddRecipientAccount.tbxReAccountNumber.text = "";
            this.view.AddRecipientAccount.tbxNickName.text = "";
            this.view.AddRecipientAccount.tbxBankName.text = "";
            this.view.AddRecipientAccount.tbxBankAddressLine1.text = "";
            this.view.AddRecipientAccount.tbxBankAddressLine2.text = "";
            this.view.AddRecipientAccount.tbxBankCity.text = "";
            this.view.AddRecipientAccount.tbxBankZipcode.text = "";
            FormControllerUtility.disableButton(this.view.AddRecipientAccount.btnProceed);
            FormControllerUtility.disableButton(this.view.AddRecipientAccount.btnAddRecipent);
        },
        /**
         * Toggles the visibility of Row in account details page based data its passes
         * @param {object}  rowObject in format {rowId: 'id of the row', left: {key: 'i18n', value: 'value'}, right: {key: 'i18n', value: 'value'}}
         * @return {void} None
         */
        validateAddRecipientForm: function() {
            var data = this.getRecipientAccountFormData();
            if (data.payeeAccountNumber !== data.accountNumberConfirm) {
                this.showErrorForAccountFields();
                return false;
            }
            this.hideErrorFlexAddRecipient();
            return true;
        },
        /**
         * Toggles the Radio Button selection state for Image Labels rendered by font icons
         * @param {object} RadioBtn1 Refernce to image widget 1
         * @param {object} RadioBtn2 Refernce to image widget 2
         */
        RadioBtnAction: function(RadioBtn1, RadioBtn2) {
            if (RadioBtn1.text === ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO) {
                RadioBtn1.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED_NUO;
                RadioBtn1.skin = ViewConstants.SKINS.RADIOBTN_UNSELECTED_FONT;
                RadioBtn2.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO;
                RadioBtn2.skin = ViewConstants.SKINS.RADIOBTN_SELECTED;
                //                 RadioBtn1.height="20dp";
                //                 RadioBtn2.height="25dp";
            } else {
                RadioBtn2.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED_NUO;
                RadioBtn2.skin = ViewConstants.SKINS.RADIOBTN_UNSELECTED_FONT;
                RadioBtn1.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO;
                //                 RadioBtn1.height="25dp";
                //                 RadioBtn2.height="20dp";
                RadioBtn1.skin = ViewConstants.SKINS.RADIOBTN_SELECTED;
            }
        },
        /**
         * Entry point for international add form
         * @param {Object[]} states - List of states
         * @param {Object[]} countries - List of countries
         * @param {function} [onCancel] - Optional Call back for cancel button
         */
        showInternationalAddForm: function(states, countries, onCancel) {
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            var scopeObj = this;
            if (onCancel === undefined || typeof onCancel !== "function") {
                onCancel = function() {
                    scopeObj.getWireTransferModule().presentationController.showWireTransfer();
                };
            }
            this.showInternationalAccountUI();
            this.resetAddRecipientForms();
            var stateListBoxData = FormControllerUtility.getListBoxDataFromObjects(states, 'region_Name', 'region_Name')
            this.view.AddRecipientAccount.lbxState.masterData = stateListBoxData;
            var countryListBoxData = FormControllerUtility.getListBoxDataFromObjects(countries, 'Name', 'Name')
            this.view.AddRecipientAccount.lbxCountry.masterData = countryListBoxData;
            this.view.AddRecipientAccount.lbxCountry.selectedKey = OLBConstants.WireTransferConstants.DOMESTIC_COUNTRY_NAME;
            this.view.AddRecipientAccount.lbxBankState.masterData = stateListBoxData;
            this.view.AddRecipientAccount.flxTypeRadio1.onClick = function() {
                scopeObj.RadioBtnAction(scopeObj.view.AddRecipientAccount.imgRadioBtnRecipientType1, scopeObj.view.AddRecipientAccount.imgRadioBtnRecipientType2);
            };
            this.view.AddRecipientAccount.flxTypeRadio2.onClick = function() {
                scopeObj.RadioBtnAction(scopeObj.view.AddRecipientAccount.imgRadioBtnRecipientType2, scopeObj.view.AddRecipientAccount.imgRadioBtnRecipientType1);
            };
            this.view.AddRecipientAccount.btnCancel.onClick = this.showConfirmDialog.bind(this, "i18n.PayAPerson.Quit", "i18n.wireTransfers.cancelAddRecipient", onCancel, "i18n.common.noDontCancel", "i18n.transfers.Cancel");
            this.view.AddRecipientAccount.btnProceed.onClick = this.showInternationalAccountDetailsForm.bind(this, onCancel);
            this.view.AddRecipientAccount.lbxCountry.onSelection = this.onCountryChanged.bind(this);
        },
        /**
         * Fetch states when country is changed on list box
         * @param {Object} widget - Reference to listbox widget
         */
        onCountryChanged: function(widget) {
            this.getWireTransferModule().presentationController.fetchStates(widget.selectedKey);
        },
        /**
         * Shows the UI for international add
         * @param {void} - None
         * @returns {void} - None
         */
        showInternationalAccountUI: function() {
            this.showAddRecipientsScreen();
            this.showSwiftCodeField();
            this.view.AddRecipientAccount.btnInternationalAccount.skin = "sknBtnAccountSummarySelected";
            if (applicationManager.getConfigurationManager().isDomesticWireTransferEnabled !== "false") {
                this.view.AddRecipientAccount.btnDomesticAccount.skin = "sknBtnAccountSummaryUnselected";
            }
            this.ShowAllSeperators();
            //this.view.lblAddAccountHeading.setVisibility(false);
            this.view.AddRecipientAccount.flxTabsSeperator1.setVisibility(false);
            var text1 = kony.i18n.getLocalizedString("i18n.transfers.wireTransfer");
            var text2 = kony.i18n.getLocalizedString("i18n.WireTransfers.AddRecipientInternational");
            this.view.breadcrumb.setBreadcrumbData([{
                text: text1
            }, {
                text: text2
            }]);
            this.setAddRecipientsStep(ADD_RECIPIENTS_STEPS.RECIPIENT_DETAILS);
            this.view.AddRecipientAccount.flxCountry.setVisibility(true);
            this.view.AddRecipientAccount.lblIBANOrIRC.setVisibility(true);
            this.view.AddRecipientAccount.tbxIBANOrIRC.setVisibility(true);
            this.AdjustScreen();
        },
        /**
         * Show Iban field
         */
        showIBANField: function() {
            this.view.AddRecipientAccount.lblIBANOrIRC.text = kony.i18n.getLocalizedString("i18n.WireTransfers.InternationalBankAccountNumberBAN");
            this.view.AddRecipientAccount.tbxIBANOrIRC.placeholder = kony.i18n.getLocalizedString("i18n.WireTransfers.EnterInternationalBankAccountNumber");
        },
        /**
         * Show International routing code field.
         */
        showIRCField: function() {
            this.view.AddRecipientAccount.lblIBANOrIRC.text = kony.i18n.getLocalizedString("i18n.WireTransfer.internationalRoutingCode");
            this.view.AddRecipientAccount.tbxIBANOrIRC.placeholder = kony.i18n.getLocalizedString("i18n.WireTransfers.EnterInternationalRoutingCode");
        },
        /**
         * Shows step 2 for international add form.
         * @param {function} [onCancel]  Optional Listener for on cancel
         */
        showInternationalAccountDetailsForm: function(onCancel) {
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            this.setAddRecipientsStep(ADD_RECIPIENTS_STEPS.RECIPIENT_ACCOUNT_DETAILS);
            this.hideErrorFlexAddRecipient();
            this.view.AddRecipientAccount.tbxAccountNumber.onBeginEditing = this.hideErrorFlexAddRecipient.bind(this);
            this.view.AddRecipientAccount.tbxReAccountNumber.onBeginEditing = this.hideErrorFlexAddRecipient.bind(this);
            if (IBANUtils.isCountrySupportsIBAN(this.getRecipientFormData().country)) {
                this.showIBANField();
            } else {
                this.showIRCField();
            }
            this.view.AddRecipientAccount.btnStep2Cancel.onClick = this.showConfirmDialog.bind(this, "i18n.PayAPerson.Quit", "i18n.wireTransfers.cancelAddRecipient", onCancel, "i18n.common.noDontCancel", "i18n.transfers.Cancel");
            this.view.AddRecipientAccount.btnBack.onClick = this.setAddRecipientsStep.bind(this, ADD_RECIPIENTS_STEPS.RECIPIENT_DETAILS);
            this.view.AddRecipientAccount.btnAddRecipent.onClick = function() {
                if (this.validateAddRecipientForm()) {
                    this.confirmAddRecipient(this.getFullRecipientData(), OLBConstants.WireTransferConstants.ACCOUNT_INTERNATIONAL, onCancel);
                }
            }.bind(this);
        },
        /**
         * Shows data forconfirmation UI
         * @param {Object}  data of add recipient form
         * @param {string}  type of recipient 'domestic'/'international'
         * @param {function} [onCancel] Optional Listener for cancel
         * @param {function} [onSave] Optional Listener for on save
         * @param {boolean}  disableModify If modify needs to be disabled
         * @returns {void} - None
         */
        confirmAddRecipient: function(data, type, onCancel, onSave, disableModify) {
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            //recipient details
            this.showAddRecipientConfirmUI(type);
            this.view.confirmDetails.lblDetailsKey1.text = kony.i18n.getLocalizedString("i18n.transfers.benificiaryName") + " :";
            this.view.confirmDetails.rtxDetailsValue1.text = data.recipientDetails.payeeName;
            this.view.confirmDetails.lblDetailsKey2.text = kony.i18n.getLocalizedString('i18n.WireTransfer.recipientType') + " :";
            this.view.confirmDetails.rtxDetailsValue2.text = data.recipientDetails.type;
            this.view.confirmDetails.lblDetailsKey3.text = kony.i18n.getLocalizedString('i18n.transfers.accountType') + " :";
            this.view.confirmDetails.rtxDetailsValue3.text = type;
            this.view.confirmDetails.lblDetailsKey4.text = kony.i18n.getLocalizedString('i18n.WireTransfer.RecipientAddress') + " :";
            this.view.confirmDetails.rtxDetailsValue4.text = this.returnRecipientAddress(data.recipientDetails)
            //bank details
            this.view.confirmDetails.lblBankDetailsKey3.text = kony.i18n.getLocalizedString('i18n.WireTransfer.recipientAccountNumber') + " :";
            this.view.confirmDetails.rtxBankDetailsValue3.text = data.recipientAccountDetails.payeeAccountNumber;
            this.view.confirmDetails.lblBankDetailsKey5.text = kony.i18n.getLocalizedString('i18n.transfers.accountNickName') + " :";
            this.view.confirmDetails.rtxBankDetailsValue5.text = data.recipientAccountDetails.payeeNickName;
            this.view.confirmDetails.lblBankDetailsKey6.text = kony.i18n.getLocalizedString('i18n.WireTransfer.recipientBank&Address') + " :";
            this.view.confirmDetails.rtxBankDetailsValue6.text = data.recipientAccountDetails.bankAddressLine1;
            this.view.confirmDetails.rtxBankDetailsValue6.text = this.returnBankAddress(data.recipientAccountDetails, true);
            //location specific bank details
            if (type === OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC) {
                this.view.confirmDetails.lblBankDetailsKey1.text = kony.i18n.getLocalizedString('i18n.accounts.routingNumber') + " :";
                this.view.confirmDetails.rtxBankDetailsValue1.text = data.recipientAccountDetails.routingCode;
            } else if (type === OLBConstants.WireTransferConstants.ACCOUNT_INTERNATIONAL) {
                if (IBANUtils.isCountrySupportsIBAN(data.recipientDetails.country)) {
                    this.view.confirmDetails.lblBankDetailsKey2.text = kony.i18n.getLocalizedString('i18n.Accounts.IBAN');
                    this.view.confirmDetails.rtxBankDetailsValue2.text = data.recipientAccountDetails.IBAN
                } else {
                    this.view.confirmDetails.lblBankDetailsKey2.text = kony.i18n.getLocalizedString('i18n.WireTransfer.internationalRoutingCode') + " :";
                    this.view.confirmDetails.rtxBankDetailsValue2.text = data.recipientAccountDetails.internationalRoutingCode
                }
                this.view.confirmDetails.lblBankDetailsKey1.text = kony.i18n.getLocalizedString('i18n.accounts.swiftCode') + " :";
                this.view.confirmDetails.rtxBankDetailsValue1.text = data.recipientAccountDetails.swiftCode;
            }
            this.view.confirmDetails.btnCancel.toolTip = kony.i18n.getLocalizedString('i18n.transfers.Cancel');
            this.view.confirmDetails.btnConfirm.toolTip = kony.i18n.getLocalizedString('i18n.common.confirmAndAddRecipient');
            if (disableModify) {
                this.view.confirmDetails.btnCancel.setVisibility(false);
                this.view.confirmDetails.btnModify.onClick = this.showConfirmDialog.bind(this, "i18n.PayAPerson.Quit", "i18n.wireTransfers.cancelAddRecipient", onCancel, "i18n.common.noDontCancel", "i18n.transfers.Cancel");
                this.view.confirmDetails.btnModify.toolTip = kony.i18n.getLocalizedString('i18n.transfers.Cancel');
                this.view.confirmDetails.btnModify.text = kony.i18n.getLocalizedString('i18n.transfers.Cancel');
            } else {
                this.view.confirmDetails.btnModify.text = kony.i18n.getLocalizedString('i18n.transfers.Modify');
                this.view.confirmDetails.btnModify.toolTip = kony.i18n.getLocalizedString('i18n.common.modifyRecipientDetail');
                this.view.confirmDetails.btnCancel.onClick = this.showConfirmDialog.bind(this, "i18n.PayAPerson.Quit", "i18n.wireTransfers.cancelAddRecipient", onCancel, "i18n.common.noDontCancel", "i18n.transfers.Cancel");
                this.view.confirmDetails.btnModify.onClick = this.modifyAddRecipient.bind(this, type);
            }
            if (CommonUtilities.isCSRMode()) {
                this.view.confirmDetails.btnConfirm.onClick = FormControllerUtility.disableButtonActionForCSRMode();
                this.view.confirmDetails.btnConfirm.skin = FormControllerUtility.disableButtonSkinForCSRMode();
                this.view.confirmDetails.btnConfirm.hoverSkin = FormControllerUtility.disableButtonSkinForCSRMode();
            } else {
                this.view.confirmDetails.btnConfirm.onClick = onSave ? function() {
                    onSave(data)
                } : this.saveRecipient.bind(this, type);
            }
            this.AdjustScreen();
        },
        /**
         * Calls business logic to save recipient.
         * @param {string} type - type of recipient to save domestic/international
         */
        saveRecipient: function(type) {
            this.getWireTransferModule().presentationController.saveWireTransferPayee(this.getFullRecipientData(), type);
        },
        /**
         * Modify Add Recipient - Resets UI and takes back to Form - from confirm page
         * @param {type} type - Type of Recipient International / Domestic
         */
        modifyAddRecipient: function(type) {
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            if (type === OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC) {
                this.ShowDomesticAccountUI();
            } else {
                this.showInternationalAccountUI();
            }
            this.setAddRecipientsStep(ADD_RECIPIENTS_STEPS.RECIPIENT_DETAILS);
            this.checkAddRecipientDetailForm();
        },
        /**
         * Shows UI for add recipient Confirmation
         * @param {string} type Type of Recipient International/Domestic
         */
        showAddRecipientConfirmUI: function(type) {
            this.resetUI();
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            this.view.flxConfirmDetails.setVisibility(true);
            this.view.flxHeader.setFocus(true);
            this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString("i18n.PayAPerson.AddRecipientConfirmation");
            this.view.customheader.lblHeaderMobile.text = this.view.lblAddAccountHeading.text;
            this.view.confirmDetails.lblConfirmHeader.text = kony.i18n.getLocalizedString('i18n.wiretransfers.confirmDetails');
            this.view.confirmDetails.flxTransactionDetails.setVisibility(false);
            this.view.flxTermsAndConditions.setVisibility(false);
            this.view.flxSuccessMessage.setVisibility(false);
            this.view.flxbuttons.setVisibility(false);
            this.view.confirmDetails.flxRow4.setVisibility(false);
            this.view.confirmDetails.flxStep2Buttons.setVisibility(true);
            // this.view.lblAddAccountHeading.setVisibility(true);
            this.view.flxPrint.setVisibility(false);
            this.view.flxDownload.setVisibility(false);
            this.view.confirmDetails.flxSeparator.setVisibility(false);
            if (type === OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC) {
                this.view.confirmDetails.flxRow2.setVisibility(false);
                this.view.breadcrumb.setBreadcrumbData([{
                    text: kony.i18n.getLocalizedString("i18n.transfers.wireTransfer")
                }, {
                    text: kony.i18n.getLocalizedString("i18n.WireTransfers.AddRecipientDomesticConfirmation")
                }]);
            } else {
                this.view.confirmDetails.flxRow2.setVisibility(true);
                this.view.breadcrumb.setBreadcrumbData([{
                    text: kony.i18n.getLocalizedString("i18n.transfers.wireTransfer")
                }, {
                    text: kony.i18n.getLocalizedString("i18n.WireTransfers.AddRecipientInternationalConfirmation")
                }]);
            }
            this.view.confirmDetails.flxRow4.setVisibility(false);
            this.view.confirmDetails.flxRecipientDetails.info.frame.height = this.view.confirmDetails.flxBankDetails.info.frame.height;
        },
        /**
         * Get standard currencies name with their symbols
         * @returns {objetc[]} - array of JSONs with currency name and symbol
         */
        getCurrency: function() {
            return applicationManager.getConfigurationManager().OLBConstants.WireTransferConstants.CURRENCIES;
        },
        /**
         * Return key corresponding to value for Listbox
         * @param {widget} listbox -widget Id
         * @param {string} value -value for which key need to be searched
         * @returns {string} Key of the listbox
         */
        returnKeyForListboxFromValue: function(listbox, value) {
            var data = listbox.masterData;
            data = data.filter(function(item) {
                return item[1] === value
            });
            return data[0] ? data[0][0] : listbox.masterData[0][0];
        },
        /**
         * Return account object corresponding to account number for Listbox
         * @param {widget} listbox -widget Id
         * @param {string} value -value for which account object needs to be searched
         * @returns {string} Key of the listbox
         */
        getFromAccount: function(accounts, value) {
            var data = accounts;
            data = data.filter(function(item) {
                return item.accountID === value
            });
            return data[0];
        },
        /**
         * Updates the pagination values
         * @param {object} values Values containing pagination details
         * @param {string} paginationTextKey i18nKey for pagination widget
         * @param {function} onNext Listener for next button
         * @param {function} onPrevious Listener for previous button
         */
        updatePaginationValue: function(values, paginationTextKey, onNext, onPrevious) {
            var paginationComponent = this.view.WireTransferContainer.tablePagination;
            paginationComponent.setVisibility(true);
            paginationComponent.lblPagination.text = (values.offset + 1) + " - " + (values.offset + values.limit) + " " + kony.i18n.getLocalizedString(paginationTextKey);
            if (values.offset >= values.paginationRowLimit) {
                paginationComponent.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_ACTIVE;
                paginationComponent.flxPaginationPrevious.setEnabled(true);
                paginationComponent.flxPaginationPrevious.onClick = onPrevious;
            } else {
                paginationComponent.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_INACTIVE;
                paginationComponent.flxPaginationPrevious.setEnabled(false);
            }
            if (values.limit < values.paginationRowLimit) {
                paginationComponent.imgPaginationNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_INACTIVE;
                paginationComponent.flxPaginationNext.setEnabled(false);
            } else {
                paginationComponent.imgPaginationNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_ACTIVE;
                paginationComponent.flxPaginationNext.setEnabled(true);
                paginationComponent.flxPaginationNext.onClick = onNext;
            }
        },
        /**
         * Show Confirm Dialog
         * @param {string} headingKey  - i18n of heading
         * @param {string} messageKey  - i18n of message
         * @param {function} onYesPressed  - callback for yes button
         * @param {string} noBtnTooltip  - i18n for tooltip
         * @param {string} yesBtnTooltip - i18n of tooltip
         */
        showConfirmDialog: function(headingKey, messageKey, onYesPressed, noBtnTooltip, yesBtnTooltip) {
            this.view.flxPopup.setVisibility(true);
            var height = this.view.flxHeader.info.frame.height + this.view.flxMain.info.frame.height + this.view.flxFooter.info.frame.height;
            this.view.flxPopup.height = height + "dp";
            this.view.CustomPopup.setFocus(true);
            if (noBtnTooltip) {
                this.view.CustomPopup.btnNo.toolTip = kony.i18n.getLocalizedString(noBtnTooltip);
            } else {
                this.view.CustomPopup.btnNo.toolTip = kony.i18n.getLocalizedString("i18n.common.no");
            }
            if (yesBtnTooltip) {
                this.view.CustomPopup.btnYes.toolTip = kony.i18n.getLocalizedString(yesBtnTooltip);
            } else {
                this.view.CustomPopup.btnYes.toolTip = kony.i18n.getLocalizedString("i18n.common.yes");
            }
            this.view.CustomPopup.lblHeading.text = kony.i18n.getLocalizedString(headingKey);
            this.view.CustomPopup.lblPopupMessage.text = kony.i18n.getLocalizedString(messageKey);
            this.view.forceLayout();
            //Quit Pop-Up Message Box
            this.view.CustomPopup.flxCross.onClick = function() {
                this.view.flxPopup.setVisibility(false);
            }.bind(this);
            this.view.CustomPopup.btnNo.onClick = function() {
                this.view.flxPopup.setVisibility(false);
            }.bind(this);
            this.view.CustomPopup.btnYes.onClick = function() {
                this.view.flxPopup.setVisibility(false);
                onYesPressed();
                this.disableTnC();
            }.bind(this);
        },
        /**
         * Show Error for account fields for add recipient
         */
        showErrorForAccountFields: function() {
            this.view.AddRecipientAccount.flxError.setVisibility(true);
            this.view.AddRecipientAccount.tbxAccountNumber.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;
            this.view.AddRecipientAccount.tbxReAccountNumber.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;
            this.AdjustScreen();
        },
        /**
         * Hides Error for account fields for add recipient
         */
        hideErrorFlexAddRecipient: function() {
            this.view.AddRecipientAccount.tbxAccountNumber.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            this.view.AddRecipientAccount.tbxReAccountNumber.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            this.view.AddRecipientAccount.flxError.setVisibility(false);
            this.AdjustScreen();
        },
        /**
         * Show routing Code field.
         * @returns {void} - None
         * @param {void} - None
         */
        showRoutingCodeField: function() {
            this.view.AddRecipientAccount.lblSwiftCode.text = kony.i18n.getLocalizedString("i18n.accounts.routingNumber");
            this.view.AddRecipientAccount.tbxSwiftCode.placeholder = kony.i18n.getLocalizedString("i18n.AddExternalAccount.EnterRoutingNumber");
        },
        /**
         * Show swift code field
         * @returns {void} - None
         * @param {void} - None
         */
        showSwiftCodeField: function() {
            this.view.AddRecipientAccount.lblSwiftCode.text = kony.i18n.getLocalizedString("i18n.accounts.swiftCode");
            this.view.AddRecipientAccount.tbxSwiftCode.placeholder = kony.i18n.getLocalizedString("i18n.WireTransfers.EnterSwiftCode");
        },
        /**
         * Shows recipient data on acknowledgement screen
         * @param {JSON} data - data of add recipient form
         */
        showAddAccountAcknowledgement: function(data) {
            var self = this;
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            this.showAddAccountAcknowledgementUI(data);
            this.view.lblSuccessAcknowledgement.text = data.payeeNickName + " " + kony.i18n.getLocalizedString("i18n.common.HasBeenAddedSuccessfully");
            this.view.btnMakeAnotherWireTransfer.onClick = function() {
                self.onMakeTransferActionClick(data);
            }
            this.view.btnViewSentTransactions.onClick = function() {
                if (data.wireAccountType === OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC) {
                    self.onAddDomesticAccount();
                } else {
                    self.onAddInternationalAccount();
                }
            }
            this.AdjustScreen();
        },
        /**
         * onClick action of print button
         * @returns {void} - None
         */
        onClickPrint: function() {
            var scopeObj = this;
            var subData = [],
                tableList = [];
            if (this.view.lblRefrenceNumber.isVisible) {
                subData.push({
                    key: kony.i18n.getLocalizedString("i18n.common.status"),
                    value: this.view.lblSuccessAcknowledgement.text
                });
                subData.push({
                    key: this.view.lblRefrenceNumber.text,
                    value: this.view.lblRefrenceNumberValue.text
                });
                subData.push({
                    key: this.view.confirmDetails.lblFromAccountKey.text,
                    value: this.view.confirmDetails.rtxToAccountValue.text
                });
                subData.push({
                    key: this.view.confirmDetails.lblToAccountKey.text,
                    value: this.view.confirmDetails.rtxToAccountValue.text
                });
                subData.push({
                    key: this.view.confirmDetails.lblAmountKey.text,
                    value: this.view.confirmDetails.rtxAmountValue.text
                });
                subData.push({
                    key: this.view.confirmDetails.lblTransactionFeeKey.text,
                    value: this.view.confirmDetails.rtxTransactionFeeValue.text
                });
                subData.push({
                    key: this.view.confirmDetails.lblNoteKey.text,
                    value: this.view.confirmDetails.rtxNoteValue.text
                });
                tableList.push({
                    tableHeader: this.view.confirmDetails.lblConfirmHeader.text,
                    tableRows: subData
                });
            }
            subData = [];
            if (!this.view.lblRefrenceNumber.isVisible) {
                subData.push({
                    key: kony.i18n.getLocalizedString("i18n.common.status"),
                    value: this.view.lblSuccessAcknowledgement.text
                });
            }
            subData.push({
                key: this.view.confirmDetails.lblDetailsKey1.text,
                value: this.view.confirmDetails.rtxDetailsValue1.text
            });
            subData.push({
                key: this.view.confirmDetails.lblDetailsKey2.text,
                value: this.view.confirmDetails.rtxDetailsValue2.text
            });
            subData.push({
                key: this.view.confirmDetails.lblDetailsKey3.text,
                value: this.view.confirmDetails.rtxDetailsValue3.text
            });
            subData.push({
                key: this.view.confirmDetails.lblDetailsKey4.text,
                value: this.view.confirmDetails.rtxDetailsValue4.text
            });
            tableList.push({
                tableHeader: this.view.confirmDetails.lblRecipientDetails.text,
                tableRows: subData
            });
            subData = [];
            subData.push({
                key: this.view.confirmDetails.lblBankDetailsKey1.text,
                value: this.view.confirmDetails.rtxBankDetailsValue1.text
            });
            subData.push({
                key: this.view.confirmDetails.lblBankDetailsKey3.text,
                value: this.view.confirmDetails.rtxBankDetailsValue3.text
            });
            subData.push({
                key: this.view.confirmDetails.lblBankDetailsKey5.text,
                value: this.view.confirmDetails.rtxBankDetailsValue5.text
            });
            subData.push({
                key: this.view.confirmDetails.lblBankDetailsKey6.text,
                value: this.view.confirmDetails.rtxBankDetailsValue6.text
            });
            tableList.push({
                tableHeader: this.view.confirmDetails.lblBankDetails.text,
                tableRows: subData
            });
            this.getWireTransferModule().presentationController.showPrintPage({
                printKeyValueGroupModel: {
                    "moduleHeader": scopeObj.view.lblAddAccountHeading.text,
                    "printCallback": scopeObj.navigateToAcknowledgement,
                    "tableList": tableList
                }
            });
        },
        /**
         * callback function binded to cancel button of print view
         * @returns {void} - None
         */
        navigateToAcknowledgement: function() {
            applicationManager.getNavigationManager().navigateTo("frmWireTransfer");
            applicationManager.getNavigationManager().updateForm({}, "frmWireTransfer");
        },
        NavigateToTransfers: function() {
            if (applicationManager.getConfigurationManager().getConfigurationValue('isFastTransferEnabled') === "true")
                applicationManager.getModulesPresentationController("TransferFastModule").showTransferScreen();
            else
                applicationManager.getModulesPresentationController("TransferModule").showTransferScreen();
        },
        /**
         * Shows UI for add account acknowledgement
         * @param {Object} data - data of add recipient form
         * @returns {void} - None
         */
        showAddAccountAcknowledgementUI: function(data) {
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            //this.view.lblAddAccountHeading.setVisibility(true);
            if (CommonUtilities.isPrintEnabled()) {
                if (kony.application.getCurrentBreakpoint() == 640 || kony.application.getCurrentBreakpoint() == 1024) {
                    this.view.flxPrint.setVisibility(false);
                } else {
                    this.view.flxPrint.setVisibility(true);
                }
                this.view.lblPrintfontIconConfirm.onTouchStart = this.onClickPrint;
            } else {
                this.view.flxPrint.setVisibility(false);
            }
            // this.view.flxDownload.setVisibility(true);
            this.view.flxSuccessMessage.setVisibility(true);
            this.view.btnSaveRecipient.setVisibility(false);
            this.view.btnMakeAnotherWireTransfer.text = kony.i18n.getLocalizedString('i18n.WireTransfer.MakeWireTransfer');
            this.view.btnMakeAnotherWireTransfer.toolTip = kony.i18n.getLocalizedString('i18n.common.initiateWireTransfer');
            this.view.btnViewSentTransactions.text = kony.i18n.getLocalizedString('i18n.PayAPerson.AddAnotherRecipient');
            this.view.btnViewSentTransactions.toolTip = kony.i18n.getLocalizedString("i18n.PayAPerson.AddAnotherRecipient");
            this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString('i18n.PayAPerson.AddRecipientAcknowledgement');
            this.view.customheader.lblHeaderMobile.text = this.view.lblAddAccountHeading.text;
            var text2 = "";
            if (data.wireAccountType === OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC) {
                text2 = kony.i18n.getLocalizedString('i18n.addRecipient.domestAck');
            } else {
                text2 = kony.i18n.getLocalizedString('i18n.addRecipient.intAck');
            }
            var text1 = kony.i18n.getLocalizedString("i18n.transfers.wireTransfer");
            this.view.breadcrumb.setBreadcrumbData([{
                text: text1
            }, {
                text: text2
            }]);
            this.view.lblRefrenceNumber.setVisibility(false);
            this.view.lblRefrenceNumberValue.setVisibility(false);
            this.view.flxbuttons.setVisibility(true);
            this.view.confirmDetails.flxStep2Buttons.setVisibility(false);
            this.view.confirmDetails.flxRecipientDetails.info.frame.height = this.view.confirmDetails.flxBankDetails.info.frame.height;
            this.view.flxHeader.setFocus(true);
            this.view.forceLayout();
        },
        /**
         * Return currency symbol corresponding to currency name
         * @param {Array} data -currency masterdata
         * @param {string} value -value for which currency name need to be searched
         * @returns {string} Currency Symbol
         */
        returnCurrencySymbolFromValue: function(data, value) {
            var currObj = data.filter(function(item) {
                if (item.name === value) {
                    return true;
                }
                return false;
            });
            return data[0].symbol;
        },
        /**
         * Right Bar Add Domestic Account Listener
         */
        onAddDomesticAccount: function() {
            this.getWireTransferModule().presentationController.showAddPayee('Domestic');
            this.view.AddRecipientAccount.imgRadioBtnRecipientType1.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO;
            this.view.AddRecipientAccount.imgRadioBtnRecipientType1.skin = ViewConstants.SKINS.RADIOBTN_SELECTED;
            this.view.AddRecipientAccount.imgRadioBtnRecipientType2.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED_NUO;
            this.view.AddRecipientAccount.imgRadioBtnRecipientType2.skin = ViewConstants.SKINS.RADIOBTN_UNSELECTED_FONT;
            this.AdjustScreen();
            //         	this.view.AddRecipientAccount.imgRadioBtnRecipientType1.height="25dp";
            //             this.view.AddRecipientAccount.imgRadioBtnRecipientType2.height="20dp";
        },
        /**
         * Right Bar Add International Account Listener
         */
        onAddInternationalAccount: function() {
            this.getWireTransferModule().presentationController.showAddPayee('International');
            this.view.AddRecipientAccount.imgRadioBtnRecipientType1.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO;
            this.view.AddRecipientAccount.imgRadioBtnRecipientType1.skin = ViewConstants.SKINS.RADIOBTN_SELECTED;
            this.view.AddRecipientAccount.imgRadioBtnRecipientType2.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED_NUO;
            this.view.AddRecipientAccount.imgRadioBtnRecipientType2.skin = ViewConstants.SKINS.RADIOBTN_UNSELECTED_FONT;
            this.AdjustScreen();
            //           	this.view.AddRecipientAccount.imgRadioBtnRecipientType1.height="25dp";
            //             this.view.AddRecipientAccount.imgRadioBtnRecipientType2.height="20dp";
        },
        /**
         * Populate List of states in state listbox
         * @param {Object[]} states List of state objects
         * @param {boolean} isEditFlow To mark a edit flow.
         */
        populateStates: function(states, isEditFlow) {
            var data = FormControllerUtility.getListBoxDataFromObjects(states, 'region_Name', 'region_Name');
            this.view.AddRecipientAccount.lbxState.masterData = data;
            this.view.oneTimeTransfer.lbxState.masterData = data;
            this.view.oneTimeTransfer.lbxBankState.masterData = data;
            this.view.AddRecipientAccount.lbxBankState.masterData = data;
            // if(isEditFlow){
            //     this.view.oneTimeTransfer.lbxState.selectedKey=this.returnKeyForListboxFromValue(this.view.oneTimeTransfer.lbxState,this.stateForEdit);
            // }
        },
        /**
         * Adjusts the footer. Needs to be called after every Major UI reset
         */
        AdjustScreen: function() {
            this.view.forceLayout();
            var mainheight = 0;
            var screenheight = kony.os.deviceInfo().screenHeight;
            mainheight = this.view.customheader.info.frame.height + this.view.flxMain.info.frame.height;
            var diff = screenheight - mainheight;
            if (mainheight < screenheight) {
                diff = diff - this.view.flxFooter.info.frame.height;
                if (diff > 0) {
                    this.view.flxFooter.top = mainheight + diff + "dp";
                } else {
                    this.view.flxFooter.top = mainheight + "dp";
                }
            } else {
                this.view.flxFooter.top = mainheight + "dp";
            }
            this.view.forceLayout();
            this.initializeResponsiveViews();
        },
        /**
         * Entry point method for one time transfer
         * @param {object} data  - Data for One Time Transfer
         * @param {function} onCancel  - Optional callback for cancel button
         */
        showOneTimeTransfer: function(data, onCancel) {
            this.resetOneTimeTransferForms();
            this.setOneTimeTransferStep(ONE_TIME_TRANSFER_STEPS.RECIPIENT_DETAILS);
            this.setOneTimeTransferValidationActions();
            this.view.oneTimeTransfer.lbxCountry.setEnabled(true);
            this.view.oneTimeTransfer.lbxCountry.skin = "sknLbxSSP42424215PxBorder727272";
            this.view.oneTimeTransfer.lbxCountry.focusSkin = "sknLbxSSP42424215PxBorder727272";
            this.view.oneTimeTransfer.lbxCountry.hoverSkin = "sknLbxSSP42424215PxBorder727272";
            var stateListBoxData = FormControllerUtility.getListBoxDataFromObjects(data.states, 'region_Name', 'region_Name')
            this.view.oneTimeTransfer.lbxBankState.masterData = stateListBoxData;
            this.setCurrencyData();
            if (onCancel === undefined || typeof onCancel !== "function") {
                onCancel = function() {
                    this.getWireTransferModule().presentationController.showWireTransfer();
                }.bind(this);
            }
            this.showOneTimeTransferUI();
            this.configureAccountTypeRadioButtons(data.states, data.countries);
            this.view.oneTimeTransfer.flxTypeRadio1.onClick = function() {
                this.RadioBtnAction(this.view.oneTimeTransfer.imgRadioBtnRecipient1, this.view.oneTimeTransfer.imgRadioBtnRecipient2);
            }.bind(this);
            this.view.oneTimeTransfer.flxTypeRadio2.onClick = function() {
                this.RadioBtnAction(this.view.oneTimeTransfer.imgRadioBtnRecipient2, this.view.oneTimeTransfer.imgRadioBtnRecipient1);
            }.bind(this);
            this.view.oneTimeTransfer.btnCancel.onClick = this.showConfirmDialog.bind(this, "i18n.PayAPerson.Quit", "i18n.wireTransfer.cancelOneTimeTransfer", onCancel, "i18n.common.noDontCancel", "i18n.transfers.Cancel");
            this.view.oneTimeTransfer.btnCancel.toolTip = kony.i18n.getLocalizedString("i18n.transfers.deleteTransfer");
            this.view.oneTimeTransfer.btnProceed.onClick = this.showOneTimeTransferAccountDetailsForm.bind(this, data, onCancel);
            this.view.oneTimeTransfer.btnProceed.toolTip = kony.i18n.getLocalizedString("i18n.common.proceed");
            this.checkAndShowCountryFieldinOTT(data.states, data.countries);
            this.view.oneTimeTransfer.lbxCountry.onSelection = this.onCountryChanged.bind(this);
            this.view.customheader.customhamburger.activateMenu("Wire Transfer", "Make One Time Payment");
            this.checkOTTRecipientDetailForm();
            this.AdjustScreen();
        },
        /**
         * One Time Transfer Server error
         */
        showOneTimeTransferServerError: function(context) {
            this.showOneTimeTransferUI();
            this.view.customheader.customhamburger.activateMenu("WIRE TRANSFER", "Make One Time Payment");
            this.setOneTimeTransferStep(3);
            this.checkOTTRecipientDetailForm();
            this.showServerError(context.errorMessage);
        },
        /**
         * Check Radio box value and show/ hide country field in OTT
         * @param {object[]} states array of state objects
         * @param {object[]} countries array of country objects
         */
        checkAndShowCountryFieldinOTT: function(states, countries) {
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            var showCountryField = this.view.oneTimeTransfer.imgRadioBtnAccountType2.text === ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO;
            this.view.oneTimeTransfer.flxCountry.setVisibility(showCountryField);
            if (showCountryField) {
                var countryListBoxData = FormControllerUtility.getListBoxDataFromObjects(countries, 'Name', 'Name')
                this.view.oneTimeTransfer.lbxCountry.masterData = countryListBoxData;
                this.view.oneTimeTransfer.lbxCountry.selectedKey = OLBConstants.WireTransferConstants.DOMESTIC_COUNTRY_NAME;
            } else {
                var stateListBoxData = FormControllerUtility.getListBoxDataFromObjects(states, 'region_Name', 'region_Name')
                this.view.oneTimeTransfer.lbxState.masterData = stateListBoxData;
            }
            this.AdjustScreen();
        },
        /**
         * Binds initial validations actions on text fields of OTT - Should be called from pre show
         */
        setOneTimeTransferValidationActions: function() {
            this.view.oneTimeTransfer.tbxRecipientName.onKeyUp = this.checkOTTRecipientDetailForm.bind(this);
            this.view.oneTimeTransfer.tbxAddressLine1.onKeyUp = this.checkOTTRecipientDetailForm.bind(this);
            this.view.oneTimeTransfer.tbxAddressLine2.onKeyUp = this.checkOTTRecipientDetailForm.bind(this);
            this.view.oneTimeTransfer.tbxCity.onKeyUp = this.checkOTTRecipientDetailForm.bind(this);
            this.view.oneTimeTransfer.tbxZipcode.onKeyUp = this.checkOTTRecipientDetailForm.bind(this);
            this.view.oneTimeTransfer.tbxSwiftCode.onKeyUp = this.checkOTTRecipientAccountDetailForm.bind(this);
            this.view.oneTimeTransfer.tbxAccountNumber.onKeyUp = this.checkOTTRecipientAccountDetailForm.bind(this);
            this.view.oneTimeTransfer.tbxReAccountNumber.onKeyUp = this.checkOTTRecipientAccountDetailForm.bind(this);
            this.view.oneTimeTransfer.tbxNickName.onKeyUp = this.checkOTTRecipientAccountDetailForm.bind(this);
            this.view.oneTimeTransfer.tbxBankName.onKeyUp = this.checkOTTRecipientAccountDetailForm.bind(this);
            this.view.oneTimeTransfer.tbxBankAddressLine1.onKeyUp = this.checkOTTRecipientAccountDetailForm.bind(this);
            this.view.oneTimeTransfer.tbxBankAddressLine2.onKeyUp = this.checkOTTRecipientAccountDetailForm.bind(this);
            this.view.oneTimeTransfer.tbxBankCity.onKeyUp = this.checkOTTRecipientAccountDetailForm.bind(this);
            this.view.oneTimeTransfer.tbxBankZipcode.onKeyUp = this.checkOTTRecipientAccountDetailForm.bind(this);
            this.view.oneTimeTransfer.tbxIBANOrIRC.onKeyUp = this.checkOTTRecipientAccountDetailForm.bind(this);
            this.view.oneTimeTransfer.tbxNote.onKeyUp = this.checkOTTTransactionForm.bind(this);
        },
        /**
         * Resets form Show one time transfer UI
         */
        showOneTimeTransferUI: function() {
            this.resetUI();
            this.view.oneTimeTransfer.flxAccountType.setVisibility(true);
            this.view.oneTimeTransfer.flxReason.setVisibility(false);
            this.view.flxOneTimeTransfer.setVisibility(true);
            this.view.lblAddAccountHeading.text = "One Time Transfer";
            this.view.customheader.lblHeaderMobile.text = this.view.lblAddAccountHeading.text;
            this.view.breadcrumb.setBreadcrumbData([{
                    text: kony.i18n.getLocalizedString("i18n.transfers.wireTransfer")
                },
                {
                    text: kony.i18n.getLocalizedString('i18n.WireTransfer.MakeOneTimeTransfer')
                }
            ])
            this.view.flxTermsAndConditions.setVisibility(true);
            FormControllerUtility.disableButton(this.view.oneTimeTransfer.btnProceed);
            this.view.oneTimeTransfer.lblHeader.text = kony.i18n.getLocalizedString("i18n.WireTransfer.MakeOneTimeTransfer");
            this.view.oneTimeTransfer.flxAccountType.setVisibility(true);
            //this.view.lblAddAccountHeading.setVisibility(false);
            this.showRightBar("oneTimeTransfer");
            this.view.forceLayout();
        },
        /**
         * Resets one time transfer form.
         */
        resetOneTimeTransferForms: function() {
            this.view.oneTimeTransfer.tbxRecipientName.text = "";
            this.view.oneTimeTransfer.imgRadioBtnAccountType1.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO;
            this.view.oneTimeTransfer.imgRadioBtnAccountType2.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED_NUO;
            this.view.oneTimeTransfer.imgRadioBtnRecipient1.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO;
            this.view.oneTimeTransfer.imgRadioBtnRecipient2.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED_NUO;
            this.view.oneTimeTransfer.tbxAddressLine1.text = ""
            this.view.oneTimeTransfer.tbxAddressLine2.text = ""
            this.view.oneTimeTransfer.tbxCity.text = ""
            this.view.oneTimeTransfer.tbxZipcode.text = "";
            this.view.oneTimeTransfer.tbxSwiftCode.text = "";
            this.view.oneTimeTransfer.tbxIBANOrIRC.text = "";
            this.view.oneTimeTransfer.tbxAccountNumber.text = "";
            this.view.oneTimeTransfer.tbxReason.text = "";
            this.view.oneTimeTransfer.tbxReAccountNumber.text = "";
            this.view.oneTimeTransfer.tbxNickName.text = "";
            this.view.oneTimeTransfer.tbxBankName.text = "";
            this.view.oneTimeTransfer.tbxBankAddressLine1.text = "";
            this.view.oneTimeTransfer.tbxBankAddressLine2.text = "";
            this.view.oneTimeTransfer.tbxBankCity.text = "";
            this.view.oneTimeTransfer.tbxBankZipcode.text = "";
            this.view.oneTimeTransfer.tbxAmount.text = "";
            this.view.oneTimeTransfer.tbxNote.text = "";
            FormControllerUtility.disableButton(this.view.oneTimeTransfer.btnProceed);
            FormControllerUtility.disableButton(this.view.oneTimeTransfer.btnStep2Proceed);
            FormControllerUtility.disableButton(this.view.oneTimeTransfer.btnStep3MakeTransfer);
        },
        /**
         * Resets UI Actions
         */
        resetOneTimeTransferValidationActions: function() {
            this.view.oneTimeTransfer.tbxRecipientName.onKeyUp = null;
            this.view.oneTimeTransfer.tbxAddressLine1.onKeyUp = null;
            this.view.oneTimeTransfer.tbxAddressLine2.onKeyUp = null;
            this.view.oneTimeTransfer.tbxCity.onKeyUp = null;
            this.view.oneTimeTransfer.tbxZipcode.onKeyUp = null;
            this.view.oneTimeTransfer.tbxSwiftCode.onKeyUp = null;
            this.view.oneTimeTransfer.tbxAccountNumber.onKeyUp = null;
            this.view.oneTimeTransfer.tbxReAccountNumber.onKeyUp = null;
            this.view.oneTimeTransfer.tbxNickName.onKeyUp = null;
            this.view.oneTimeTransfer.tbxBankName.onKeyUp = null;
            this.view.oneTimeTransfer.tbxBankAddressLine1.onKeyUp = null;
            this.view.oneTimeTransfer.tbxBankAddressLine2.onKeyUp = null;
            this.view.oneTimeTransfer.tbxBankCity.onKeyUp = null;
            this.view.oneTimeTransfer.tbxBankZipcode.onKeyUp = null;
            this.view.oneTimeTransfer.tbxIBANOrIRC.onKeyUp = null;
        },
        /**
         * Sets Currency Values for One time transfer
         */
        setCurrencyData: function() {
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            this.view.oneTimeTransfer.lbxStep3Currency.masterData = FormControllerUtility.getListBoxDataFromObjects(this.getCurrency(), "symbol", "name");
            this.view.oneTimeTransfer.lbxStep3Currency.selectedKey = this.returnKeyForListboxFromValue(this.view.oneTimeTransfer.lbxStep3Currency, OLBConstants.CURRENCY_NAME); //To set Dollar BY-DEFAULT
            this.onCurrencyChanged(this.view.oneTimeTransfer.lblCurrencySymbol, this.view.oneTimeTransfer.lbxStep3Currency);
        },
        /**
         * Toggles between steps of one time transfer
         * @member frmWireTransferController
         * @param {number} step - Number of step from 1 to 3
         * @returns {void} - None
         */
        setOneTimeTransferStep: function(step) {
            var TOTAL_STEPS = 4;
            if (step > 0 && step < TOTAL_STEPS) {
                this.view.oneTimeTransfer.flxStep1.setVisibility(false);
                this.view.oneTimeTransfer.flxStep2.setVisibility(false);
                this.view.oneTimeTransfer.flxStep3.setVisibility(false);
                this.view.oneTimeTransfer["flxStep" + step].setVisibility(true);
            }
            this.AdjustScreen();
        },
        /**
         * Configure Account Type Radio button based on configurations
         *  @param {Object[]} states List of states
         * @param {object[]} countries List of countries
         */
        configureAccountTypeRadioButtons: function(states, countries) {
            if (applicationManager.getConfigurationManager().isDomesticWireTransferEnabled === "false") {
                this.view.oneTimeTransfer.lblAccountTypeRadio1.setVisibility(false);
                this.view.oneTimeTransfer.flxAccountTypeRadio1.setVisibility(false);
            } else {
                this.view.oneTimeTransfer.lblAccountTypeRadio1.setVisibility(true);
                this.view.oneTimeTransfer.flxAccountTypeRadio1.setVisibility(true);
                this.view.oneTimeTransfer.flxAccountTypeRadio1.onClick = function() {
                    this.setCurrencyData();
                    this.RadioBtnAction(this.view.oneTimeTransfer.imgRadioBtnAccountType1, this.view.oneTimeTransfer.imgRadioBtnAccountType2);
                    this.checkAndShowCountryFieldinOTT(states, countries);
                }.bind(this);
            }
            if (applicationManager.getConfigurationManager().isInternationalWireTransferEnabled === "false") {
                this.view.oneTimeTransfer.flxAccountTypeRadio2.setVisibility(false);
                this.view.oneTimeTransfer.lblAccountTypeRadio2.setVisibility(false);
            } else {
                this.view.oneTimeTransfer.flxAccountTypeRadio2.setVisibility(true);
                this.view.oneTimeTransfer.lblAccountTypeRadio2.setVisibility(true);
                this.view.oneTimeTransfer.flxAccountTypeRadio2.onClick = function() {
                    this.setCurrencyData();
                    this.RadioBtnAction(this.view.oneTimeTransfer.imgRadioBtnAccountType2, this.view.oneTimeTransfer.imgRadioBtnAccountType1);
                    this.checkAndShowCountryFieldinOTT(states, countries);
                }.bind(this);
            }
            this.view.forceLayout();
        },
        /**
         * Validates domestic recipient details form in OTT
         */
        checkOTTRecipientDetailForm: function() {
            var formData = this.getOTTRecipientFormData();
            if (formData.payeeName === "" ||
                formData.addressLine1 === "" ||
                formData.cityName === "" ||
                formData.zipCode === "") {
                FormControllerUtility.disableButton(this.view.oneTimeTransfer.btnProceed);
            } else {
                FormControllerUtility.enableButton(this.view.oneTimeTransfer.btnProceed);
            }
        },
        /**
         * Validates recipient account details form in OTT
         */
        checkOTTRecipientAccountDetailForm: function() {
            var formData = this.getOTTRecipientAccountFormData();
            if (formData.swiftCode === "" ||
                formData.payeeAccountNumber === "" ||
                formData.accountNumberConfirm === "" ||
                formData.payeeNickName === "" ||
                formData.bankName === "" ||
                formData.bankAddressLine1 === "" ||
                formData.bankCity === "" ||
                formData.bankZip === "" ||
                (this.view.oneTimeTransfer.tbxIBANOrIRC.isVisible && formData.IBAN === "")) {
                FormControllerUtility.disableButton(this.view.oneTimeTransfer.btnStep2Proceed);
            } else {
                FormControllerUtility.enableButton(this.view.oneTimeTransfer.btnStep2Proceed);
            }
        },
        /**
         * Validates transaction  details form in OTT
         */
        checkOTTTransactionForm: function() {
            var formData = this.getOTTTransactionDetails();
            if (formData.reasonForTransfer === "" || !this.oneTimeTransferAmountField.isValidAmount() || this.oneTimeTransferAmountField.getAmountValue() === 0) {
                FormControllerUtility.disableButton(this.view.oneTimeTransfer.btnStep3MakeTransfer);
            } else {
                FormControllerUtility.enableButton(this.view.oneTimeTransfer.btnStep3MakeTransfer);
            }
        },
        /**
         * Get Recipient Account Details for One time transfer (Step 2 data)
         * @member frmWireTransferController
         * @returns {JSON} - JSON for data
         */
        getOTTRecipientAccountFormData: function() {
            return {
                payeeNickName: this.view.oneTimeTransfer.tbxNickName.text.trim(),
                routingCode: this.view.oneTimeTransfer.tbxSwiftCode.text.trim(),
                swiftCode: this.view.oneTimeTransfer.tbxSwiftCode.text.trim(),
                IBAN: this.view.oneTimeTransfer.tbxIBANOrIRC.text.trim(),
                internationalRoutingCode: this.view.oneTimeTransfer.tbxIBANOrIRC.text.trim(),
                payeeAccountNumber: this.view.oneTimeTransfer.tbxAccountNumber.text.trim(),
                accountNumberConfirm: this.view.oneTimeTransfer.tbxReAccountNumber.text.trim(),
                recipientReasonForTransfer: this.view.oneTimeTransfer.tbxReason.text.trim(),
                bankName: this.view.oneTimeTransfer.tbxBankName.text.trim(),
                bankAddressLine1: this.view.oneTimeTransfer.tbxBankAddressLine1.text.trim(),
                bankAddressLine2: this.view.oneTimeTransfer.tbxBankAddressLine2.text.trim(),
                bankCity: this.view.oneTimeTransfer.tbxBankCity.text.trim(),
                bankState: this.view.oneTimeTransfer.lbxBankState.selectedKeyValue[1],
                bankZip: this.view.oneTimeTransfer.tbxBankZipcode.text.trim()
            }
        },
        /**
         * Get Recipient  Details for One time transfer (Step 1 data)
         * @returns {Object} - JSON for data
         */
        getOTTRecipientFormData: function() {
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            return {
                wireAccountType: this.view.oneTimeTransfer.imgRadioBtnAccountType1.text === ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO ? OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC : OLBConstants.WireTransferConstants.ACCOUNT_INTERNATIONAL,
                payeeName: this.view.oneTimeTransfer.tbxRecipientName.text.trim(),
                type: this.view.oneTimeTransfer.imgRadioBtnRecipient1.src === ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO ? OLBConstants.WireTransferConstants.RECIPIENT_INDIVIDUAL : OLBConstants.WireTransferConstants.RECIPIENT_BUSINESS,
                addressLine1: this.view.oneTimeTransfer.tbxAddressLine1.text.trim(),
                addressLine2: this.view.oneTimeTransfer.tbxAddressLine2.text.trim(),
                cityName: this.view.oneTimeTransfer.tbxCity.text.trim(),
                state: this.view.oneTimeTransfer.lbxState.selectedKeyValue[1],
                country: this.view.oneTimeTransfer.lbxCountry.selectedKeyValue[1],
                zipCode: this.view.oneTimeTransfer.tbxZipcode.text.trim()
            };
        },
        /**
         * Get Transaction  Details for One time transfer (Step 3 data)
         * @returns {JSON} - JSON for data
         */
        getOTTTransactionDetails: function() {
            this.view.oneTimeTransfer.lbxStep3Currency.selectedKey = this.view.oneTimeTransfer.lblCurrencySymbol.text;
            return {
                fromAccountNumber: this.view.oneTimeTransfer.lbxStep3From.selectedKey,
                fromAccountName: this.view.oneTimeTransfer.lbxStep3From.selectedKeyValue[1],
                currencySymbol: this.view.oneTimeTransfer.lbxStep3Currency.selectedKeyValue ? this.view.oneTimeTransfer.lbxStep3Currency.selectedKeyValue[0] : "",
                currency: this.view.oneTimeTransfer.lbxStep3Currency.selectedKeyValue ? this.view.oneTimeTransfer.lbxStep3Currency.selectedKeyValue[1] : "",
                amount: this.oneTimeTransferAmountField.getAmount(),
                transactionFee: applicationManager.getConfigurationManager().wireTranferFees,
                reasonForTransfer: this.view.oneTimeTransfer.tbxNote.text.trim()
            }
        },
        /**
         * Show One time transfer step 2
         * @param {function} [onCancel] Optional Listener for on cancel
         */
        showOneTimeTransferAccountDetailsForm: function(data, onCancel) {
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            this.setOneTimeTransferStep(ONE_TIME_TRANSFER_STEPS.RECIPIENT_ACCOUNT_DETAILS);
            this.ShowAllStep2();
            this.view.oneTimeTransfer.tbxAccountNumber.onBeginEditing = this.hideErrorFlexOTT.bind(this);
            this.view.oneTimeTransfer.tbxReAccountNumber.onBeginEditing = this.hideErrorFlexOTT.bind(this);
            var step1Data = this.getOTTRecipientFormData();
            if (step1Data.wireAccountType === OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC) {
                this.view.oneTimeTransfer.lblSwiftCode.text = kony.i18n.getLocalizedString("i18n.accounts.routingNumber")
                this.view.oneTimeTransfer.tbxSwiftCode.placeholder = kony.i18n.getLocalizedString("i18n.AddExternalAccount.EnterRoutingNumber")
                this.view.oneTimeTransfer.lblIBANOrIRC.setVisibility(false);
                this.view.oneTimeTransfer.tbxIBANOrIRC.setVisibility(false);
            } else {
                this.view.oneTimeTransfer.lblSwiftCode.text = kony.i18n.getLocalizedString("i18n.accounts.swiftCode");;
                this.view.oneTimeTransfer.tbxSwiftCode.placeholder = kony.i18n.getLocalizedString("i18n.WireTransfers.EnterSwiftCode")
                this.view.oneTimeTransfer.lblIBANOrIRC.setVisibility(true);
                this.view.oneTimeTransfer.tbxIBANOrIRC.setVisibility(true);
                if (IBANUtils.isCountrySupportsIBAN(step1Data.country)) {
                    this.view.oneTimeTransfer.lblIBANOrIRC.setVisibility(true);
                    this.view.oneTimeTransfer.tbxIBANOrIRC.setVisibility(true);
                    this.view.oneTimeTransfer.lblIBANOrIRC.text = kony.i18n.getLocalizedString("i18n.WireTransfers.InternationalBankAccountNumberBAN")
                    this.view.oneTimeTransfer.tbxIBANOrIRC.placeholder = kony.i18n.getLocalizedString("i18n.WireTransfers.EnterInternationalBankAccountNumber")
                } else {
                    this.view.oneTimeTransfer.lblIBANOrIRC.text = kony.i18n.getLocalizedString("i18n.WireTransfer.internationalRoutingCode"); //TODO i18n
                    this.view.oneTimeTransfer.tbxIBANOrIRC.placeholder = kony.i18n.getLocalizedString("i18n.WireTransfers.EnterInternationalRoutingCode"); //TODO i18n
                }
            }
            this.view.oneTimeTransfer.btnStep2Proceed.onClick = function() {
                if (this.validateOTTForm()) {
                    this.showOneTimeTransferTransactionDetails(data, onCancel);
                }
            }.bind(this);
            this.view.oneTimeTransfer.btnBack.onClick = this.setOneTimeTransferStep.bind(this, ONE_TIME_TRANSFER_STEPS.RECIPIENT_DETAILS);
            this.view.oneTimeTransfer.btnStep2Cancel.onClick = this.showConfirmDialog.bind(this, "i18n.PayAPerson.Quit", "i18n.wireTransfer.cancelOneTimeTransfer", onCancel, "i18n.common.noDontCancel", "i18n.transfers.Cancel");
            this.checkOTTRecipientAccountDetailForm();
            this.AdjustScreen();
        },
        /**
         * Get Full  Details for One time transfer (All Step  data)
         * @returns {JSON} - JSON for data
         */
        getFullOTTDetails: function() {
            return {
                recipientAccountDetails: this.getOTTRecipientAccountFormData(),
                recipientDetails: this.getOTTRecipientFormData(),
                transactionDetails: this.getOTTTransactionDetails()
            }
        },
        /**
         * Toggles the visibility of Row in account details page based data its passes
         * @return {boolean} True if vlid and false will not valid
         */
        validateOTTForm: function() {
            var data = this.getOTTRecipientAccountFormData();
            if (data.payeeAccountNumber !== data.accountNumberConfirm) {
                this.showErrorForAccountFieldsOTT();
                return false;
            }
            this.hideErrorFlexOTT();
            return true;
        },
        /**
         * Show errros for account number in OTT
         */
        showErrorForAccountFieldsOTT: function() {
            this.view.oneTimeTransfer.flxError.setVisibility(true);
            this.view.oneTimeTransfer.tbxAccountNumber.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;
            this.view.oneTimeTransfer.tbxReAccountNumber.skin = ViewConstants.SKINS.SKNTXTSSP424242BORDERFF0000OP100RADIUS2PX;
            this.AdjustScreen();
        },
        /**
         * Hide errros for account number in OTT
         */
        hideErrorFlexOTT: function() {
            this.view.oneTimeTransfer.tbxAccountNumber.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            this.view.oneTimeTransfer.tbxReAccountNumber.skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
            this.view.oneTimeTransfer.flxError.setVisibility(false);
            this.AdjustScreen();
        },
        /**
         * Shows transactions details form for one time transfer and binds actions
         * @member frmWireTransferController
         * @param  {object} data  - Data from backend
         * @param  {function} [onCancel]  - Optional callback for cancel button
         */
        showOneTimeTransferTransactionDetails: function(data, onCancel) {
            this.setOneTimeTransferStep(ONE_TIME_TRANSFER_STEPS.TRANSACTION_DETAILS);
            var step2Data = this.getOTTRecipientAccountFormData();
            var step1Data = this.getOTTRecipientFormData();
            var listBoxData = FormControllerUtility.getListBoxDataFromObjects(data.checkingAccounts, "accountID", CommonUtilities.getAccountDisplayNameWithBalance);
            this.view.oneTimeTransfer.lbxStep3From.masterData = listBoxData;
            this.setCurrencyData();
            this.configureCurrencyField(step1Data.wireAccountType);
            this.view.oneTimeTransfer.lbxStep3From.onSelection = this.onFromAccountChanged.bind(this, this.view.oneTimeTransfer, data.checkingAccounts);
            this.onFromAccountChanged(this.view.oneTimeTransfer, data.checkingAccounts);
            this.view.oneTimeTransfer.lblToValue.text = step2Data.payeeNickName;
            this.view.oneTimeTransfer.lblBankValue.text = step2Data.bankName;
            this.view.oneTimeTransfer.btnStep3Back.onClick = this.setOneTimeTransferStep.bind(this, ONE_TIME_TRANSFER_STEPS.RECIPIENT_ACCOUNT_DETAILS);
            this.view.oneTimeTransfer.btnStep3Cancel.onClick = this.showConfirmDialog.bind(this, "i18n.PayAPerson.Quit", "i18n.wireTransfer.cancelOneTimeTransfer", onCancel, "i18n.common.noDontCancel", "i18n.transfers.Cancel");
            this.view.oneTimeTransfer.btnStep3MakeTransfer.onClick = this.confirmOneTimeTransfer.bind(this, onCancel);
            this.AdjustScreen();
        },
        /**
         * Confgures the currency field in OTT
         * @param {string} accountType - DOmestic/International
         */
        configureCurrencyField: function(accountType) {
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            if (accountType === OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC) {
                this.view.oneTimeTransfer.flxStep3Currency.setVisibility(false);
            } else {
                this.view.oneTimeTransfer.flxStep3Currency.setVisibility(true);
                this.view.oneTimeTransfer.lbxStep3Currency.onSelection = this.onCurrencyChanged.bind(this, this.view.oneTimeTransfer.lblCurrencySymbol);
            }
        },
        /**
         * Entry Point for Confirm One Time Transfer
         * @param {function} onCancel  Optional call back for cancel button
         */
        confirmOneTimeTransfer: function(onCancel) {
            this.enableTnC();
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            this.getWireTransferModule().presentationController.getTnC("One-Time");
            var data = this.getFullOTTDetails();
            this.view.confirmDetails.lblDetailsKey1.text = kony.i18n.getLocalizedString("i18n.transfers.benificiaryName") + " :";
            this.view.confirmDetails.rtxDetailsValue1.text = data.recipientDetails.payeeName;
            this.view.confirmDetails.lblDetailsKey2.text = kony.i18n.getLocalizedString("i18n.WireTransfer.recipientType") + " :";
            this.view.confirmDetails.rtxDetailsValue2.text = data.recipientDetails.type;
            this.view.confirmDetails.lblDetailsKey3.text = kony.i18n.getLocalizedString("i18n.transfers.accountType") + " :";
            this.view.confirmDetails.rtxDetailsValue3.text = data.recipientDetails.wireAccountType;
            this.view.confirmDetails.lblDetailsKey4.text = kony.i18n.getLocalizedString("i18n.WireTransfer.RecipientAddress") + " :";
            this.view.confirmDetails.rtxDetailsValue4.text = this.returnRecipientAddress(data.recipientDetails);
            //bank details
            this.view.confirmDetails.lblBankDetailsKey3.text = kony.i18n.getLocalizedString("i18n.WireTransfer.recipientAccountNumber") + " :";
            this.view.confirmDetails.rtxBankDetailsValue3.text = data.recipientAccountDetails.payeeAccountNumber;
            this.view.confirmDetails.lblBankDetailsKey5.text = kony.i18n.getLocalizedString("i18n.transfers.accountNickName") + " :";
            this.view.confirmDetails.rtxBankDetailsValue5.text = data.recipientAccountDetails.payeeNickName;
            this.view.confirmDetails.lblBankDetailsKey6.text = kony.i18n.getLocalizedString("i18n.WireTransfer.recipientBank&Address") + " :";
            this.view.confirmDetails.rtxBankDetailsValue6.text = this.returnBankAddress(data.recipientAccountDetails);
            //location specific bank details
            if (data.recipientDetails.wireAccountType === OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC) {
                this.view.confirmDetails.lblBankDetailsKey1.text = kony.i18n.getLocalizedString("i18n.accounts.routingNumber") + " :"
                this.view.confirmDetails.rtxBankDetailsValue1.text = data.recipientAccountDetails.routingCode;
            } else if (data.recipientDetails.wireAccountType === OLBConstants.WireTransferConstants.ACCOUNT_INTERNATIONAL) {
                if (IBANUtils.isCountrySupportsIBAN(data.recipientDetails.country)) {
                    this.view.confirmDetails.lblBankDetailsKey2.text = kony.i18n.getLocalizedString("i18n.WireTransfer.IBAN") + " :";
                    this.view.confirmDetails.rtxBankDetailsValue2.text = data.recipientAccountDetails.IBAN
                } else {
                    this.view.confirmDetails.lblBankDetailsKey2.text = kony.i18n.getLocalizedString("i18n.WireTransfer.internationalRoutingCode") + " :";
                    this.view.confirmDetails.rtxBankDetailsValue2.text = data.recipientAccountDetails.internationalRoutingCode
                }
                this.view.confirmDetails.lblBankDetailsKey1.text = kony.i18n.getLocalizedString("i18n.accounts.swiftCode") + " :";
                this.view.confirmDetails.rtxBankDetailsValue1.text = data.recipientAccountDetails.swiftCode;
            }
            //  Transaction Details
            this.view.confirmDetails.rtxFromAccountValue.text = data.transactionDetails.fromAccountName;
            this.view.confirmDetails.rtxToAccountValue.text = data.recipientAccountDetails.payeeNickName;
            this.view.confirmDetails.lblAmountKey.text = this.getAmountLabelText(data.transactionDetails.currencySymbol);
            this.view.confirmDetails.rtxAmountValue.text = CommonUtilities.formatCurrencyWithCommas(data.transactionDetails.amount, true);
            this.view.confirmDetails.rtxTransactionFeeValue.text = CommonUtilities.formatCurrencyWithCommas(applicationManager.getConfigurationManager().wireTranferFees);
            this.view.confirmDetails.rtxNoteValue.text = data.transactionDetails.reasonForTransfer;
            this.view.confirmDetails.lblCurrencyKey.isVisible = data.recipientDetails.recipientAccountType === OLBConstants.WireTransferConstants.ACCOUNT_INTERNATIONAL;
            this.view.confirmDetails.rtxCurrencyValue.isVisible = data.recipientDetails.recipientAccountType === OLBConstants.WireTransferConstants.ACCOUNT_INTERNATIONAL;
            this.view.confirmDetails.rtxCurrencyValue.text = data.transactionDetails.currency;
            this.view.confirmDetails.btnCancel.onClick = this.showConfirmDialog.bind(this, "i18n.PayAPerson.Quit", "i18n.wireTransfer.cancelOneTimeTransfer", onCancel, "i18n.common.noDontCancel", "i18n.transfers.Cancel");
            this.view.confirmDetails.btnModify.onClick = this.modifyOneTimeTransfer.bind(this);
            this.view.confirmDetails.btnConfirm.toolTip = kony.i18n.getLocalizedString('i18n.common.confirmTransaction');
            if (CommonUtilities.isCSRMode()) {
                this.view.confirmDetails.btnConfirm.onClick = FormControllerUtility.disableButtonActionForCSRMode();
                this.view.confirmDetails.btnConfirm.skin = FormControllerUtility.disableButtonSkinForCSRMode();
                this.view.confirmDetails.btnConfirm.hoverSkin = FormControllerUtility.disableButtonSkinForCSRMode();
            } else {
                this.view.confirmDetails.btnConfirm.onClick = this.createOneTimeTransfer.bind(this);
            }
            this.showOneTimeTransferConfirmUI(data.recipientDetails.wireAccountType);
            this.AdjustScreen();
        },
        /**
         * Modify One time transfer - Resets UI and takes back to Form - from confirm page
         */
        modifyOneTimeTransfer: function() {
            this.disableTnC();
            this.showOneTimeTransferUI();
            this.setOneTimeTransferStep(ONE_TIME_TRANSFER_STEPS.RECIPIENT_DETAILS);
            this.checkOTTRecipientDetailForm();
        },
        /**
         * Create One Time Transfer
         */
        createOneTimeTransfer: function() {
            this.disableTnC();
            var data = this.getFullOTTDetails();
            this.getWireTransferModule().presentationController.authorizeOneTimeTransfer(data);
        },
        /**
         * Show One Time Transfer Confirm UI
         * @param {string} type - International / Domestic
         */
        showOneTimeTransferConfirmUI: function(type) {
            this.resetUI();
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            this.view.flxConfirmDetails.setVisibility(true);
            this.view.flxHeader.setFocus(true);
            this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString("i18n.WireTransfer.makeTransferConfirm");
            this.view.customheader.lblHeaderMobile.text = this.view.lblAddAccountHeading.text;
            this.view.confirmDetails.flxTransactionDetails.setVisibility(true);
            this.view.flxTermsAndConditions.setVisibility(false);
            this.view.flxSuccessMessage.setVisibility(false);
            this.view.confirmDetails.flxRow4.setVisibility(false);
            this.view.flxbuttons.setVisibility(false);
            this.view.confirmDetails.flxStep2Buttons.setVisibility(true);
            //this.view.lblAddAccountHeading.setVisibility(true);
            this.view.flxPrint.setVisibility(false);
            this.view.flxDownload.setVisibility(false);
            this.view.confirmDetails.flxSeparator.setVisibility(false);
            if (type === OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC) {
                this.view.confirmDetails.flxRow2.setVisibility(false);
            } else {
                this.view.confirmDetails.flxRow2.setVisibility(true);
            }
            this.view.breadcrumb.setBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.transfers.wireTransfer")
            }, {
                text: kony.i18n.getLocalizedString('i18n.wireTransfer.makeOneTimeTransferConfirmation')
            }]);
            this.view.confirmDetails.flxRecipientDetails.info.frame.height = this.view.confirmDetails.flxBankDetails.info.frame.height;
            this.view.confirmDetails.btnModify.text = kony.i18n.getLocalizedString('i18n.transfers.Modify');
            this.view.confirmDetails.btnModify.toolTip = kony.i18n.getLocalizedString('i18n.transfers.Modify');
            this.view.confirmDetails.btnCancel.toolTip = kony.i18n.getLocalizedString('i18n.transfers.Cancel');
            this.view.confirmDetails.btnCancel.setVisibility(true)
            this.AdjustScreen();
        },
        /**
         * Show Edit Recipient UI
         * @param {Object} context - data
         */
        showEditRecipientUI: function(context) {
            var payee = context.payee;
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            var scopeObj = this;
            scopeObj.resetUI();
            scopeObj.setOneTimeTransferStep(ONE_TIME_TRANSFER_STEPS.RECIPIENT_DETAILS);
            scopeObj.resetOneTimeTransferValidationActions();
            scopeObj.view.oneTimeTransfer.flxStep1.setVisibility(true);
            this.view.oneTimeTransfer.lbxCountry.setEnabled(true); //User cannot change country while Editing
            this.view.oneTimeTransfer.lbxCountry.skin = "sknDisableF7F7F7lst";
            this.view.oneTimeTransfer.lbxCountry.focusSkin = "sknDisableF7F7F7lst";
            this.view.oneTimeTransfer.lbxCountry.hoverSkin = "sknDisableF7F7F7lst";
            this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString('i18n.PayAPerson.EditRecipient');
            this.view.customheader.lblHeaderMobile.text = this.view.lblAddAccountHeading.text;
            this.view.oneTimeTransfer.btnProceed.toolTip = kony.i18n.getLocalizedString("i18n.common.saveChanges");
            this.view.oneTimeTransfer.btnCancel.toolTip = kony.i18n.getLocalizedString("i18n.common.cancelUpdate");
            this.view.oneTimeTransfer.btnCancel.onClick = function() {
                this.getWireTransferModule().presentationController.fetchManageRecipientList();
            }.bind(this);
            this.view.oneTimeTransfer.flxTypeRadio1.onClick = function() {
                this.RadioBtnAction(this.view.oneTimeTransfer.imgRadioBtnRecipient1, this.view.oneTimeTransfer.imgRadioBtnRecipient2);
            }.bind(this);
            this.view.oneTimeTransfer.flxTypeRadio2.onClick = function() {
                this.RadioBtnAction(this.view.oneTimeTransfer.imgRadioBtnRecipient2, this.view.oneTimeTransfer.imgRadioBtnRecipient1);
            }.bind(this);
            var countryShow = payee.wireAccountType === OLBConstants.WireTransferConstants.ACCOUNT_INTERNATIONAL
            if (countryShow) {
                this.view.oneTimeTransfer.lbxCountry.masterData = FormControllerUtility.getListBoxDataFromObjects(context.countries, "Name", "Name");
                this.view.oneTimeTransfer.lbxCountry.selectedKey = OLBConstants.WireTransferConstants.DOMESTIC_COUNTRY_NAME;
                this.view.oneTimeTransfer.lbxCountry.onSelection = this.onCountryChanged.bind(this);
                this.view.oneTimeTransfer.lbxCountry.selectedKey = scopeObj.returnKeyForListboxFromValue(this.view.oneTimeTransfer.lbxCountry, payee.country);
            }
            scopeObj.view.oneTimeTransfer.lbxState.masterData = FormControllerUtility.getListBoxDataFromObjects(context.states, "region_Name", "region_Name");
            scopeObj.view.oneTimeTransfer.lbxState.selectedKey = this.returnKeyForListboxFromValue(this.view.oneTimeTransfer.lbxState, payee.state);
            scopeObj.view.flxOneTimeTransfer.setVisibility(true);
            scopeObj.showRightBar("addRecipient");
            scopeObj.view.oneTimeTransfer.lblHeader.text = kony.i18n.getLocalizedString("i18n.PayAPerson.EditRecipient");
            scopeObj.view.oneTimeTransfer.flxAccountType.setVisibility(false);
            scopeObj.view.oneTimeTransfer.flxCountry.setVisibility(countryShow);
            scopeObj.view.flxAddKonyAccount.setVisibility(false);
            scopeObj.view.flxAddNonKonyAccount.setVisibility(false);
            FormControllerUtility.enableButton(this.view.oneTimeTransfer.btnProceed);
            if (CommonUtilities.isCSRMode()) {
                this.view.oneTimeTransfer.btnProceed.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.oneTimeTransfer.btnProceed.skin = CommonUtilities.disableButtonSkinForCSRMode();
                this.view.oneTimeTransfer.btnProceed.hoverSkin = CommonUtilities.disableButtonSkinForCSRMode();
                this.view.oneTimeTransfer.btnProceed.focusSkin = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                this.view.oneTimeTransfer.btnProceed.onClick = this.editRecipientAccountDetails.bind(scopeObj, context, payee.payeeId);
            }
            this.view.oneTimeTransfer.tbxRecipientName.text = payee.payeeName;
            if (payee.type === OLBConstants.WireTransferConstants.RECIPIENT_INDIVIDUAL) {
                this.view.oneTimeTransfer.imgRadioBtnRecipient2.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED_NUO;
                this.view.oneTimeTransfer.imgRadioBtnRecipient1.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO;
            } else {
                this.view.oneTimeTransfer.imgRadioBtnRecipient2.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO;
                this.view.oneTimeTransfer.imgRadioBtnRecipient1.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED_NUO;
            }
            this.view.oneTimeTransfer.tbxAddressLine1.text = payee.addressLine1;
            this.view.oneTimeTransfer.tbxAddressLine2.text = payee.addressLine2;
            this.view.oneTimeTransfer.tbxCity.text = payee.cityName;
            this.view.oneTimeTransfer.tbxZipcode.text = payee.zipCode;
            this.AdjustScreen();
        },
        /**
         * Edit Recipient
         * @param {string} payeeId -payeeID
         * @param {object} config -pagination config
         */
        editRecipient: function(payeeId, context) {
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            return updateRecipientData = {
                "payeeName": this.view.oneTimeTransfer.tbxRecipientName.text.trim(),
                "zipCode": this.view.oneTimeTransfer.tbxZipcode.text.trim(),
                "cityName": this.view.oneTimeTransfer.tbxCity.text.trim(),
                "state": this.view.oneTimeTransfer.lbxState.selectedKey,
                "addressLine1": this.view.oneTimeTransfer.tbxAddressLine1.text.trim(),
                "addressLine2": this.view.oneTimeTransfer.tbxAddressLine2.text ? this.view.oneTimeTransfer.tbxAddressLine2.text.trim() : "",
                "type": this.view.oneTimeTransfer.imgRadioBtnRecipient1.text === ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO ? OLBConstants.WireTransferConstants.RECIPIENT_INDIVIDUAL : OLBConstants.WireTransferConstants.RECIPIENT_BUSINESS,
                "country": this.view.oneTimeTransfer.lbxCountry.selectedKeyValue[1],
                "payeeId": payeeId,
                "wireAccountType": context.payee.wireAccountType
            };
        },

        editRecipientAccountDetails: function(context, payeeId) {
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            var scopeObj = this;
            scopeObj.view.oneTimeTransfer.flxStep1.setVisibility(false);
            scopeObj.view.oneTimeTransfer.flxStep2.setVisibility(true);
            this.view.oneTimeTransfer.flxHeader.setVisibility(false);
            scopeObj.view.oneTimeTransfer.lblStep2.text = kony.i18n.getLocalizedString("i18n.WireTransfer.Step2RecipientAccountDetails");
            if (context.payee.wireAccountType === OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC) {
                this.view.oneTimeTransfer.lblSwiftCode.text = kony.i18n.getLocalizedString("i18n.accounts.routingNumber");
                this.view.oneTimeTransfer.tbxSwiftCode.placeholder = kony.i18n.getLocalizedString("i18n.AddExternalAccount.EnterRoutingNumber");
                this.view.oneTimeTransfer.tbxSwiftCode.text = context.payee.routingCode;
                this.view.oneTimeTransfer.lblIBANOrIRC.setVisibility(false);
                this.view.oneTimeTransfer.tbxIBANOrIRC.setVisibility(false);
            } else {
                this.view.oneTimeTransfer.lblSwiftCode.text = kony.i18n.getLocalizedString("i18n.accounts.swiftCode");
                this.view.oneTimeTransfer.tbxSwiftCode.placeholder = kony.i18n.getLocalizedString("i18n.WireTransfers.EnterSwiftCode");
                this.view.oneTimeTransfer.tbxSwiftCode.text = context.payee.swiftCode;
                this.view.oneTimeTransfer.lblIBANOrIRC.setVisibility(true);
                this.view.oneTimeTransfer.tbxIBANOrIRC.setVisibility(true);
                this.view.oneTimeTransfer.tbxIBANOrIRC.text = context.payee.internationalRoutingCode;
            }
            this.view.oneTimeTransfer.tbxAccountNumber.text = context.payee.accountNumber;
            this.view.oneTimeTransfer.tbxReAccountNumber.text = context.payee.accountNumber;
            this.view.oneTimeTransfer.tbxNickName.text = context.payee.payeeNickName;
            this.view.oneTimeTransfer.tbxBankName.text = context.payee.bankName;
            this.view.oneTimeTransfer.tbxBankAddressLine1.text = context.payee.bankAddressLine1;
            this.view.oneTimeTransfer.tbxBankAddressLine2.text = context.payee.bankAddressLine2;
            this.view.oneTimeTransfer.tbxBankCity.text = context.payee.bankCity;
            this.view.oneTimeTransfer.lbxBankState.text = context.payee.bankState;
            this.view.oneTimeTransfer.tbxBankZipcode.text = context.payee.bankZip;
            this.view.oneTimeTransfer.btnStep2Cancel.onClick = function() {
                this.getWireTransferModule().presentationController.fetchManageRecipientList();
            }.bind(this);
            this.view.oneTimeTransfer.btnBack.onClick = this.showEditRecipientUI.bind(this, context);
            this.view.oneTimeTransfer.btnStep2Proceed.text = kony.i18n.getLocalizedString("i18n.payaperson.savereciepient");
            var editRecipient = this.editRecipient(payeeId, context);
            this.view.oneTimeTransfer.btnStep2Proceed.onClick = function() {
                var recipientAccountDetails = scopeObj.getRecipientAccountDetails();
                scopeObj.editRecipientConfirmation(editRecipient, recipientAccountDetails, context);
            };
            this.AdjustScreen();
        },

        getRecipientAccountDetails: function() {
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            return recipientAccountDetailsData = {
                "routingCode": this.view.oneTimeTransfer.tbxSwiftCode.text ? this.view.oneTimeTransfer.tbxSwiftCode.text.trim() : "",
                "swiftCode": this.view.oneTimeTransfer.tbxSwiftCode.text ? this.view.oneTimeTransfer.tbxSwiftCode.text.trim() : "",
                "internationalRoutingCode": this.view.oneTimeTransfer.tbxIBANOrIRC.text ? this.view.oneTimeTransfer.tbxIBANOrIRC.text.trim() : "",
                "accountNumber": this.view.oneTimeTransfer.tbxAccountNumber.text.trim(),
                "payeeNickName": this.view.oneTimeTransfer.tbxNickName.text.trim(),
                "bankName": this.view.oneTimeTransfer.tbxBankName.text.trim(),
                "bankAddressLine1": this.view.oneTimeTransfer.tbxBankAddressLine1.text.trim(),
                "bankAddressLine2": this.view.oneTimeTransfer.tbxBankAddressLine2.text ? this.view.oneTimeTransfer.tbxBankAddressLine2.text.trim() : "",
                "bankCity": this.view.oneTimeTransfer.tbxBankCity.text.trim(),
                "bankState": this.view.oneTimeTransfer.lbxBankState.selectedKeyValue[1],
                "bankZip": this.view.oneTimeTransfer.tbxBankZipcode.text.trim()
            };
        },

        editRecipientConfirmation: function(editRecipient, recipientAccountDetails, context) {
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            var scopeObj = this;
            scopeObj.view.flxSuccessMessage.setVisibility(false);
            scopeObj.view.flxOneTimeTransfer.setVisibility(false);
            scopeObj.view.flxbuttons.setVisibility(false);
            scopeObj.view.flxRightBar.setVisibility(false);
            scopeObj.view.flxConfirmDetails.setVisibility(true);
            scopeObj.view.confirmDetails.flxStep2Buttons.setVisibility(true);
            scopeObj.view.confirmDetails.flxTransactionDetails.setVisibility(false);
            scopeObj.view.confirmDetails.flxSeparator.setVisibility(false);
            scopeObj.view.confirmDetails.flxRow4.setVisibility(false);
            scopeObj.view.confirmDetails.flxAgree.setVisibility(false);
            this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString("i18n.PayAPerson.EditRecipientConfirmation");
            this.view.confirmDetails.lblConfirmHeader.text = kony.i18n.getLocalizedString("i18n.wiretransfers.confirmDetails");
            this.view.confirmDetails.lblDetailsKey1.text = kony.i18n.getLocalizedString("i18n.PayPerson.recipientName") + " :";
            this.view.confirmDetails.rtxDetailsValue1.text = editRecipient.payeeName;
            this.view.confirmDetails.lblDetailsKey2.text = kony.i18n.getLocalizedString("i18n.WireTransfer.recipientType") + " :";
            this.view.confirmDetails.rtxDetailsValue2.text = editRecipient.type;
            this.view.confirmDetails.lblDetailsKey3.text = kony.i18n.getLocalizedString("i18n.transfers.accountType") + " :";
            this.view.confirmDetails.rtxDetailsValue3.text = editRecipient.wireAccountType;
            this.view.confirmDetails.lblDetailsKey4.text = kony.i18n.getLocalizedString("i18n.WireTransfer.RecipientAddress") + " :";
            this.view.confirmDetails.rtxDetailsValue4.text = editRecipient.addressLine1 + editRecipient.addressLine2 + editRecipient.cityName + editRecipient.state + editRecipient.zipCode + editRecipient.country;
            if (context.payee.wireAccountType === OLBConstants.WireTransferConstants.ACCOUNT_DOMESTIC) {
                this.view.confirmDetails.lblBankDetailsKey1.text = kony.i18n.getLocalizedString("i18n.accounts.routingNumber") + " :";
                this.view.confirmDetails.rtxBankDetailsValue1.text = recipientAccountDetails.routingCode;
                this.view.confirmDetails.flxRow2.setVisibility(false);
            } else {
                this.view.confirmDetails.lblBankDetailsKey1.text = kony.i18n.getLocalizedString("i18n.accounts.swiftCode") + " :";
                this.view.confirmDetails.rtxBankDetailsValue1.text = recipientAccountDetails.swiftCode;
                this.view.confirmDetails.flxRow2.setVisibility(true);
                this.view.confirmDetails.lblBankDetailsKey2.text = kony.i18n.getLocalizedString("i18n.WireTransfer.internationalRoutingCode") + " :";
                this.view.confirmDetails.rtxBankDetailsValue2.text = recipientAccountDetails.internationalRoutingCode;
            }
            this.view.confirmDetails.lblBankDetailsKey3.text = kony.i18n.getLocalizedString("i18n.WireTransfer.recipientAccountNumber") + " :";
            this.view.confirmDetails.rtxBankDetailsValue3.text = recipientAccountDetails.accountNumber;
            this.view.confirmDetails.lblBankDetailsKey5.text = kony.i18n.getLocalizedString("i18n.transfers.accountNickName") + " :";
            this.view.confirmDetails.rtxBankDetailsValue5.text = recipientAccountDetails.payeeNickName;
            this.view.confirmDetails.lblBankDetailsKey6.text = kony.i18n.getLocalizedString("i18n.WireTransfer.recipientBank&Address") + " :";
            this.view.confirmDetails.rtxBankDetailsValue6.text = recipientAccountDetails.bankName + recipientAccountDetails.bankAddressLine1 + recipientAccountDetails.bankAddressLine2 + recipientAccountDetails.bankCity + recipientAccountDetails.bankState + recipientAccountDetails.bankZip;
            this.view.confirmDetails.btnCancel.onClick = function() {
                this.getWireTransferModule().presentationController.fetchManageRecipientList();
            }.bind(this);
            this.view.confirmDetails.btnModify.onClick = this.showEditRecipientUI.bind(this, context);
            this.view.confirmDetails.btnConfirm.onclick = function() {
                scopeObj.editRecipientAcknowledgement(editRecipient, recipientAccountDetails);
            };
            this.AdjustScreen();
        },

        editRecipientAcknowledgement: function(editRecipient, recipientAccountDetails, config) {
            this.getWireTransferModule().presentationController.updateRecipient(editRecipient, recipientAccountDetails, config);
        },

        showEditRecipientSuccessMessage: function(data) {
            var self = this;
            var scopeObj = this;
            this.view.confirmDetails.flxStep2Buttons.setVisibility(false);
            this.view.flxRightBar.setVisibility(false);
            this.view.flxSuccessMessage.setVisibility(true);
            this.view.flxbuttons.setVisibility(true);
            this.view.lblRefrenceNumber.setVisibility(false);
            this.view.lblRefrenceNumberValue.setVisibility(false);
            this.view.btnSaveRecipient.setVisibility(false);
            this.view.lblAddAccountHeading.text = kony.i18n.getLocalizedString("i18n.PayAPerson.EditRecipientAcknowledgement");
            this.view.confirmDetails.lblConfirmHeader.text = kony.i18n.getLocalizedString("i18n.transfers.YourTransactionDetails");
            this.view.lblSuccessAcknowledgement.text = data.payeeNickName + " " + "has been edited succesfully!";
            this.view.btnMakeAnotherWireTransfer.text = kony.i18n.getLocalizedString('i18n.WireTranfers.MakeAnotherWireTransfer');
            this.view.btnMakeAnotherWireTransfer.toolTip = kony.i18n.getLocalizedString('i18n.WireTranfers.MakeAnotherWireTransfer');
            this.view.btnViewSentTransactions.text = kony.i18n.getLocalizedString("i18n.PayAPerson.EditAnotherRecipient");
            this.view.btnViewSentTransactions.toolTip = kony.i18n.getLocalizedString("i18n.PayAPerson.EditAnotherRecipient");
            this.view.btnMakeAnotherWireTransfer.onClick = function() {
                self.onMakeTransferActionClick(data);
            };
            this.view.btnViewSentTransactions.onClick = function() {
                scopeObj.getWireTransferModule().presentationController.fetchManageRecipientList();
            };
            this.AdjustScreen();
        },
        /**
         * Resets all the fields of step 2 OTT
         */
        ShowAllStep2: function() {
            this.view.oneTimeTransfer.flxSwiftCode.setVisibility(true);
            this.view.oneTimeTransfer.flxAccountNumber.setVisibility(true);
            this.view.oneTimeTransfer.flxReAccountNumber.setVisibility(true);
            this.view.oneTimeTransfer.flxNickName.setVisibility(true);
            this.view.oneTimeTransfer.flxBankName.setVisibility(true);
            this.view.oneTimeTransfer.flxBankAddress1.setVisibility(true);
            this.view.oneTimeTransfer.flxBankCityStateZipcode.setVisibility(true);
        },
        /**
         * Show Checking Accounts For Details for Inbound Transfer. Binds On Selection and trigger details method
         * @member frmWireTransferController
         * @param {array}  checkingAccounts Checking Accounts
         */
        showCheckingAccountsForInbound: function(checkingAccounts) {
            function listBoxMapper(checkingAccount) {
                return [checkingAccount.accountID, CommonUtilities.getAccountDisplayName(checkingAccount)];
            }
            this.view.inboundTransfer.lbxAccountNumber.masterData = checkingAccounts.map(listBoxMapper);
            this.showInboundDetails(checkingAccounts[0])
            this.view.inboundTransfer.lbxAccountNumber.onSelection = function() {
                var selectedId = this.view.inboundTransfer.lbxAccountNumber.selectedKey;
                this.showInboundDetails(checkingAccounts.filter(function(account) {
                    return account.accountID === selectedId
                })[0]);
            }.bind(this);
            this.showAccountDetailsForInboundTransferViewUI();
        },
        /**
         * Show Details of Account Object for Inbound Transfer
         * @member frmWireTransferController
         * @param {object}  accountObject Account Model Object
         */
        showInboundDetails: function(accountObject) {
            var userObject = applicationManager.getUserPreferencesManager().getUserObj();
            var OLBConstants = applicationManager.getConfigurationManager().OLBConstants;
            this.inboundConfigureRow({
                rowId: 'accountInfoRow1',
                left: {
                    key: "i18n.common.accountNumber",
                    value: accountObject.accountID
                }
            })
            this.inboundConfigureRow({
                rowId: 'accountInfoRow2',
                left: {
                    key: 'i18n.inboundTransfer.AccountOwnersName',
                    value: userObject.userfirstname + " " + userObject.userlastname
                }
            })
            this.inboundConfigureRow({
                rowId: 'accountInfoRow3',
                left: {
                    key: 'i18n.inboundTransfer.JointAccountHolder1',
                    value: accountObject.jointAccountHolder1
                },
                right: {
                    key: 'i18n.inboundTransfer.JointAccountHolder2',
                    value: accountObject.jointAccountHolder2
                }
            })
            this.inboundConfigureRow({
                rowId: 'bankDetailsRow1',
                left: {
                    key: "i18n.transfers.bankName",
                    value: userObject.bankName
                },
                right: {
                    key: 'i18n.inboundTransfer.IntermediaryBankName',
                    value: accountObject.intermediaryBankName
                }
            })
            this.inboundConfigureRow({
                rowId: 'bankDetailsRow2',
                left: {
                    key: "i18n.transfers.bankAddress",
                    value: accountObject.bankAddress
                },
                right: {
                    key: 'i18n.inboundTransfer.IntermediaryBankAddress',
                    value: accountObject.intermediaryBankAddress
                }
            })
            this.inboundConfigureRow({
                rowId: 'bankDetailsRow3',
                left: {
                    key: "i18n.accounts.routingNumber",
                    value: accountObject.routingNumber
                },
                right: {
                    key: 'i18n.inboundTransfer.IntermediaryBankSWIFTCode',
                    value: accountObject.intermediaryBankSwiftCode
                }
            })
            this.inboundConfigureRow({
                rowId: 'bankDetailsRow4',
                left: {
                    key: "i18n.accounts.swiftCode",
                    value: accountObject.swiftCode
                }
            })
            this.inboundConfigureRow({
                rowId: 'bankDetailsRow5',
                left: {
                    key: 'i18n.inboundTransfer.AccountCurrency',
                    value: accountObject.accountCurrency || OLBConstants.CURRENCY_NAME
                }
            })
            this.inboundConfigureRow({
                rowId: 'bankDetailsRow6',
                left: {
                    key: 'i18n.inboundTransfer.ServiceFeesapplicable',
                    value: CommonUtilities.formatCurrencyWithCommas(applicationManager.getConfigurationManager().wireTranferFees)
                }
            })
        },
        /**
         * Toggles the visibility of Row in account details page based data its passes
         * @param {object}  rowObject in format {rowId: 'id of the row', left: {key: 'i18n', value: 'value'}, right: {key: 'i18n', value: 'value'}}
         */
        inboundConfigureRow: function(rowObject) {
            var view = this.view.inboundTransfer;
            var rowView = view[rowObject.rowId]
            var leftVisible = false,
                rightVisible = false;
            if (rowObject.left && rowObject.left.value) {
                this.view.inboundTransfer[rowObject.rowId].flxLeftKeyValue.setVisibility(true);
                rowView.lblLeftKey.text = kony.i18n.getLocalizedString(rowObject.left.key);
                rowView.rtxLeftValue.text = rowObject.left.value;
                leftVisible = true;
            } else {
                this.view.inboundTransfer[rowObject.rowId].flxLeftKeyValue.setVisibility(false);
            }
            if (rowObject.right && rowObject.right.value) {
                this.view.inboundTransfer[rowObject.rowId].flxRightKeyValue.setVisibility(true);
                this.view.inboundTransfer[rowObject.rowId].lblRightKey.text = kony.i18n.getLocalizedString(rowObject.right.key);
                this.view.inboundTransfer[rowObject.rowId].rtxRightValue.text = rowObject.right.value;
                rightVisible = true;
            } else {
                this.view.inboundTransfer[rowObject.rowId].flxRightKeyValue.setVisibility(false);
            }
            rowView.parent.setVisibility(leftVisible || rightVisible);
        },
        /**
         * Shows UI for Account Details for Inbound Transfer
         * @member frmWireTransferController
         * @return {void} None
         *  @throws {void} - None
         */
        showAccountDetailsForInboundTransferViewUI: function() {
            this.resetUI();
            this.view.flxInboundTransfer.setVisibility(true);
            this.view.lblAddAccountHeading.text = "Account Info for Inbound Transfer";
            this.view.customheader.lblHeaderMobile.text = this.view.lblAddAccountHeading.text;
            this.view.flxTermsAndConditions.setVisibility(true);
            this.showRightBar("inbound");
            this.view.customheader.customhamburger.activateMenu("WIRE TRANSFER");
            this.view.breadcrumb.setBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.transfers.wireTransfer")
            }, {
                text: kony.i18n.getLocalizedString('i18n.inboundTransfer.ACCOUNTINFOFORINBOUNDTRANSFER')
            }]);
            this.view.inboundTransfer.flxButtons.setVisibility(false);
            this.AdjustScreen();
        },
        /**
         * Fetch View activity
         * @param {object} recipient Recipient Object for which view activity needs to be fetched.
         */
        fetchViewActivity: function(recipient, row) {
            this.setExpandedRow(row);
            this.getWireTransferModule().presentationController.fetchRecipientTransactions(recipient);
        },
        /**
         * Shows The recipient activity
         * @param {object} recipient Recipient Object
         * @param {object[]} transactions List of transactions
         */
        showRecipientActivity: function(recipient, transactions) {
            var scopeObj = this;
            this.view.WireTransferActivityWindow.lblHeader.text = kony.i18n.getLocalizedString("i18n.billPay.PaymentActivity");
            this.view.WireTransferActivityWindow.lblAccountName.text = recipient.payeeNickName;
            this.view.WireTransferActivityWindow.lblAccountHolder.text = kony.i18n.getLocalizedString("i18n.common.accountNumber") + " : " + recipient.accountNumber;
            this.view.WireTransferActivityWindow.lblAmountDeducted.text = CommonUtilities.formatCurrencyWithCommas(0);
            this.view.WireTransferActivityWindow.btnbacktopayeelist.onClick = function() {
                scopeObj.getWireTransferModule().presentationController.fetchManageRecipientList();
            };
            var text1 = kony.i18n.getLocalizedString("i18n.transfers.wireTransfer");
            var text2 = kony.i18n.getLocalizedString("i18n.PayAPerson.ManageRecipient");
            var text3 = kony.i18n.getLocalizedString("i18n.transfers.viewActivity");
            scopeObj.view.breadcrumb.setBreadcrumbData([{
                text: text1
            }, {
                text: text2,
                callback: function() {
                    scopeObj.getWireTransferModule().presentationController.showManageRecipientList();
                }
            }, {
                text: text3
            }]);
            scopeObj.wireTransferActivity(transactions);
        },
        /**
         * Show view activity based on data from backend
         * @param {JSON} viewActivityRecipientTransactions -Activities of particular recipient
         */
        wireTransferActivity: function(viewActivityRecipientTransactions) {
            var scopeObj = this;
            if (viewActivityRecipientTransactions.length > 0) {
                scopeObj.showWireTransferActivityScreen();
                scopeObj.setDataViewActivity(viewActivityRecipientTransactions);
            } else {
                scopeObj.noWireTransferActivity();
            }
        },
        /**
         * Shows the wire transfer activity screen
         */
        showWireTransferActivityScreen: function() {
            this.resetUI();
            this.view.flxWireTransferActivityWindow.setVisibility(true);
            this.view.WireTransferActivityWindow.flxSegmentBillPay.setVisibility(true);
            this.view.WireTransferActivityWindow.flxNoRecords.setVisibility(false);
            this.showRightBar("WireTransfersWindow");
            this.view.lblAddAccountHeading.text = "Wire Transfer";
            this.view.customheader.lblHeaderMobile.text = this.view.lblAddAccountHeading.text;
            this.AdjustScreen();
        },
        /**
         * Show No activity for recipient screen
         */
        noWireTransferActivity: function() {
            this.showWireTransferActivityScreen();
            this.view.WireTransferActivityWindow.flxSegmentBillPay.setVisibility(false);
            this.view.WireTransferActivityWindow.flxNoRecords.setVisibility(true);
            this.AdjustScreen();
        },
        /**
         * Bind data for activities for view activities
         * @member frmWireTransferController
         * @param {JSON} viewActivityRecipientTransactions -Activities of particular recipient
         * @returns {void} - None
         * @throws {void} - None
         */
        setDataViewActivity: function(viewActivityRecipientTransactions) {
            this.view.WireTransferActivityWindow.lblHeader.text = kony.i18n.getLocalizedString("i18n.billPay.PaymentActivity");
            this.view.WireTransferActivityWindow.lblAccountName.text = viewActivityRecipientTransactions[0].payeeNickName;
            this.view.WireTransferActivityWindow.lblAmountDeducted.text = CommonUtilities.formatCurrencyWithCommas(viewActivityRecipientTransactions[0].amountTransferedTillNow, false, viewActivityRecipientTransactions[0].currencyCode);
            var break_point = kony.application.getCurrentBreakpoint();
            var dataMap = {
                "lblpaiddate": "lblpaiddate",
                "lblFromAccount": "lblFromAccount",
                "lblAmount": "lblAmount",
                "lblAmount1": "lblAmount1",
                "lblStatus": "lblStatus",
                "lblAmountHeader": "lblAmountHeader",
                "lblFromHeader": "lblFromHeader"
            };
            viewActivityRecipientTransactions = viewActivityRecipientTransactions.map(function(dataItem) {
                return {
                    "lblpaiddate": CommonUtilities.getFrontendDateString(dataItem.transactionDate),
                    "lblFromAccount": dataItem.fromNickName,
                    "lblAmount": CommonUtilities.formatCurrencyWithCommas(dataItem.amount, false, dataItem.currencyCode),
                    "lblAmount1": CommonUtilities.formatCurrencyWithCommas(dataItem.amount, false, dataItem.currencyCode),
                    "lblStatus": dataItem.statusDescription,
                    "lblAmountHeader": "Running Balance",
                    "lblFromHeader": "From:",
                    "template": "flxSortWireTransferActivity"
                };
            });
            this.view.WireTransferActivityWindow.segBillPayActivity.setVisibility(true);
            this.view.WireTransferActivityWindow.segBillPayActivity.widgetDataMap = dataMap;
            if (break_point == 640 || orientationHandler.isMobile) {
                for (var i = 0; i < viewActivityRecipientTransactions.length; i++) {
                    viewActivityRecipientTransactions[i].template = "flxWireTransferActivityMobile";
                }
            }
            this.view.WireTransferActivityWindow.segBillPayActivity.setData(viewActivityRecipientTransactions);
            this.AdjustScreen();
        },
        /**
         * Shows The Seperator of tabs
         */
        ShowAllSeperators: function() {
            // this.view.AddRecipientAccount.flxTabsSeperator1.setVisibility(true);
            // this.view.AddRecipientAccount.flxTabsSeperator2.setVisibility(true);
            // this.view.AddRecipientAccount.flxTabsSeperator3.setVisibility(true);
            this.view.forceLayout();
        },
        /**
         * Checks and Returns selected tab
         * @returns {string} selectedTab
         */
        getSelectedTab: function() {
            if (this.view.WireTransferContainer.btnRecent.skin === "sknBtnAccountSummarySelected") {
                return "recent";
            } else if (this.view.WireTransferContainer.btnManageRecipient.skin === "sknBtnAccountSummarySelected") {
                return "manageRecipients";
            } else {
                return "makeTransfer";
            }
        },
        responsiveViews: {},
        initializeResponsiveViews: function() {
            this.responsiveViews["flxAddRecipientsWindow"] = this.isViewVisible("flxAddRecipientsWindow");
            this.responsiveViews["flxOneTimeTransfer"] = this.isViewVisible("flxOneTimeTransfer");
            this.responsiveViews["flxActivateWireTransfer"] = this.isViewVisible("flxActivateWireTransfer");
            this.responsiveViews["flxNoRecipients"] = this.isViewVisible("flxNoRecipients");
            this.responsiveViews["flxWireTransfersWindow"] = this.isViewVisible("flxWireTransfersWindow");
            this.responsiveViews["flxInboundTransfer"] = this.isViewVisible("flxInboundTransfer");
            this.responsiveViews["flxMakeTransfer"] = this.isViewVisible("flxMakeTransfer");
            this.responsiveViews["flxWireTransferActivityWindow"] = this.isViewVisible("flxWireTransferActivityWindow");
            this.responsiveViews["flxConfirmDetails"] = this.isViewVisible("flxConfirmDetails");
            this.responsiveViews["frmNotEligibleToTransfer"] = this.isViewVisible("frmNotEligibleToTransfer");
            this.responsiveViews["flxRightBar"] = this.isViewVisible("flxRightBar");
            this.responsiveViews["flxTermsAndConditions"] = this.isViewVisible("flxTermsAndConditions");
            this.responsiveViews["flxPopup"] = this.isViewVisible("flxPopup");
            this.responsiveViews["flxLoading"] = this.isViewVisible("flxLoading");
            this.responsiveViews["flxLogout"] = this.isViewVisible("flxLogout");
            this.responsiveViews["flxTermsAndConditionsPopUp"] = this.isViewVisible("flxTermsAndConditionsPopUp");
        },
        isViewVisible: function(container) {
            if (this.view[container].isVisible) {
                return true;
            } else {
                return false;
            }
        },
        //UI Code
        /**
         * onBreakpointChange : Handles ui changes on .
         * @member of {frmWireTransferController}
         * @param {integer} width - current browser width
         * @return {}
         * @throws {}
         */
        onBreakpointChange: function(width) {
            var scope = this;
            this.view.CustomPopup.onBreakpointChangeComponent(scope.view.CustomPopup, width);
            this.view.CustomPopupLogout.onBreakpointChangeComponent(scope.view.CustomPopupLogout, width);
            orientationHandler.onOrientationChange(this.onBreakpointChange);
            this.view.customheader.onBreakpointChangeComponent(width);
            this.setupFormOnTouchEnd(width);
            this.view.customheader.forceCloseHamburger();
            var scope = this;
            var mobileTemplates = {
                "flxMakeTransfersTransfersUnselected": "flxMakeTransferWireTransfersMobile",
                "flxWireTransferMakeTransfersSelected": "flxMakeTransferWireTransfersMobileSelected",
                "flxManageRecipientsUnselected1": "flxManageRecipientsWireTransfersMobile",
                "flxManageRecipientsSelected1": "flxManageRecipientsWireTransfersMobileSelected",
                "flxRecentWireTransfers": "flxRecentsWireTransfersMobile",
                "flxRecentsWireTransfersSelected": "flxRecentsWireTransfersMobileSelected",
                "flxSortWireTransferActivity": "flxWireTransferActivityMobile",
                "flxManageRecipient": "flxManageRecipientMobile",
                "flxManageRecipientSelected": "flxManageRecipientSelectedMobile"
            };
            var desktopTemplates = {
                "flxMakeTransferWireTransfersMobile": "flxMakeTransfersTransfersUnselected",
                "flxMakeTransferWireTransfersMobileSelected": "flxWireTransferMakeTransfersSelected",
                "flxManageRecipientsWireTransfersMobile": "flxManageRecipientsUnselected1",
                "flxManageRecipientsWireTransfersMobileSelected": "flxManageRecipientsSelected1",
                "flxRecentsWireTransfersMobile": "flxRecentWireTransfers",
                "flxRecentsWireTransfersMobileSelected": "flxRecentsWireTransfersSelected",
                "flxWireTransferActivityMobile": "flxSortWireTransferActivity",
                "flxManageRecipientMobile": "flxManageRecipient",
                "flxManageRecipientSelectedMobile": "flxManageRecipientSelected"
            };
            var views;
            var data;
            if (width === 640 || orientationHandler.isMobile) {
                views = Object.keys(this.responsiveViews);
                views.forEach(function(e) {
                    scope.view[e].isVisible = scope.responsiveViews[e];
                });
                this.view.AllFormsActivateWireTransfer.top = this.view.ActivateWireTransferWindow.flxDefaultAccountForSending.info.frame.y + this.view.ActivateWireTransferWindow.flxDetails.info.frame.y + 50 + "dp";
                this.view.AllFormsActivateWireTransfer.left = this.view.ActivateWireTransferWindow.flxDefaultAccountForSendingInfo.info.frame.x - 131 + "dp";
                this.view.flxMain.top = "51dp";
                this.view.WireTransferContainer.segWireTransfers.skin = "sknFlxffffffShadowdddcdc";
                this.view.lblAddAccountHeading.isVisible = false;
                this.view.customheader.lblHeaderMobile.isVisible = true;
                this.view.WireTransferContainer.flxMainContainerTable.skin = "slFbox";
                this.view.WireTransferContainer.flxTabs.skin = "sknFlxffffffShadowdddcdc";
                this.view.WireTransferContainer.lblScheduleAPayment.skin = "sknLabelSSP0273e313Pxold";
                this.view.MakeTransfer.flxButtons.height = "160dp";
                this.view.NoRecipientsWindow.btnRequestMoney.left = "10dp";
                this.view.NoRecipientsWindow.btnRequestMoney.right = "10dp";
                this.view.NoRecipientsWindow.btnRequestMoney.width = "";
                data = this.view.WireTransferContainer.segWireTransfers.data;
                if (data == undefined) return;
                data.map(function(e) {
                    if (mobileTemplates[e.template] == undefined)
                        return;
                    return e.template = mobileTemplates[e.template];
                });
                this.view.WireTransferContainer.segWireTransfers.setData(data);
            } else {
                views = Object.keys(this.responsiveViews);
                views.forEach(function(e) {
                    scope.view[e].isVisible = scope.responsiveViews[e];
                });
                data = this.view.WireTransferContainer.segWireTransfers.data;
                if (data == undefined) return;
                data.map(function(e) {
                    if (desktopTemplates[e.template] == undefined)
                        return;
                    return e.template = desktopTemplates[e.template];
                });
                this.view.WireTransferContainer.segWireTransfers.setData(data);
                this.view.customheader.lblHeaderMobile.isVisible = false;
                this.view.MakeTransfer.flxButtons.height = "110dp";
                this.view.flxMain.top = "120dp";
                this.view.NoRecipientsWindow.btnRequestMoney.width = "32.49%"
                this.view.NoRecipientsWindow.btnRequestMoney.right = "37%";
                this.view.NoRecipientsWindow.btnRequestMoney.left = "";
                this.view.WireTransferContainer.lblScheduleAPayment.skin = "sknLabelSSP0273e315px";
                this.view.WireTransferContainer.segWireTransfers.setData(data);
                this.view.customheader.lblHeaderMobile.text = "";
                this.view.lblAddAccountHeading.isVisible = true;
                this.view.WireTransferContainer.flxMainContainerTable.skin = "sknFlxffffffShadowdddcdc";
                this.view.WireTransferContainer.segWireTransfers.skin = "slFbox";
                this.view.WireTransferContainer.flxTabs.skin = "slFbox";
            }
            this.AdjustScreen();
        },
        setupFormOnTouchEnd: function(width) {
            if (width == 640) {
                this.view.onTouchEnd = function() {}
                this.nullifyPopupOnTouchStart();
            } else {
                if (width == 1024) {
                    this.view.onTouchEnd = function() {}
                    this.nullifyPopupOnTouchStart();
                } else {
                    this.view.onTouchEnd = function() {
                        hidePopups();
                    }
                }
                var userAgent = kony.os.deviceInfo().userAgent;
                if (userAgent.indexOf("iPad") != -1) {
                    this.view.onTouchEnd = function() {}
                    this.nullifyPopupOnTouchStart();
                } else if (userAgent.indexOf("Android") != -1 && userAgent.indexOf("Mobile") == -1) {
                    this.view.onTouchEnd = function() {}
                    this.nullifyPopupOnTouchStart();
                }
            }
        },
        nullifyPopupOnTouchStart: function() {},
        configureSearch: function(searchText, onSearch) {
            this.view.WireTransferContainer.flxSearch.setVisibility(true);
            this.view.WireTransferContainer.Search.txtSearch.text = searchText || "";
            this.checkSearchForm();
            this.view.WireTransferContainer.Search.txtSearch.onDone = function() {
                onSearch({
                    searchString: this.view.WireTransferContainer.Search.txtSearch.text.trim()
                });
            }.bind(this);
            this.view.WireTransferContainer.Search.txtSearch.onKeyUp = this.checkSearchForm.bind(this);
            this.view.WireTransferContainer.Search.flxClearBtn.onClick = function() {
                onSearch({
                    searchString: ""
                });
                this.resetSearchForm();
            }.bind(this)
        },
        checkSearchForm: function() {
            if (this.view.WireTransferContainer.Search.txtSearch.text.trim() === "") {
                this.view.WireTransferContainer.Search.flxClearBtn.setVisibility(false);
            } else {
                this.view.WireTransferContainer.Search.flxClearBtn.setVisibility(true);
            }
            this.view.WireTransferContainer.Search.forceLayout();
        },
        resetSearchForm: function() {
            this.view.WireTransferContainer.Search.txtSearch.text = "";
        },
        postShow: function() {
            var scopeObj = this;
            this.view.ActivateWireTransferWindow.flxDefaultAccountForSendingInfo.onClick = function() {
                if (scopeObj.view.AllFormsActivateWireTransfer.isVisible === true) {
                    scopeObj.view.AllFormsActivateWireTransfer.isVisible = false;
                } else {
                    scopeObj.view.AllFormsActivateWireTransfer.isVisible = true;
                    if (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) {
                        scopeObj.view.AllFormsActivateWireTransfer.left = "15%"
                    } else {
                        scopeObj.view.AllFormsActivateWireTransfer.left = "35%";
                    }
                    scopeObj.view.AllFormsActivateWireTransfer.top = scopeObj.view.ActivateWireTransferWindow.flxDefaultAccountForSending.info.frame.y + 216 + "px";
                    scopeObj.view.AllFormsActivateWireTransfer.imgCross.onTouchEnd = function() {
                        scopeObj.view.AllFormsActivateWireTransfer.isVisible = false;
                    }
                }
            };
            this.view.AllFormsActivateWireTransfer.flxCross.onClick = function() {
                scopeObj.view.AllFormsActivateWireTransfer.setVisibility(false);
            };
            this.onBreakpointChange(kony.application.getCurrentBreakpoint());
            this.AdjustScreen();
            applicationManager.executeAuthorizationFramework(this);
        }
    }
});