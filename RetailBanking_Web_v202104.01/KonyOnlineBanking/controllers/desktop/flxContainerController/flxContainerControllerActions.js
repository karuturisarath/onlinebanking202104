define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnRepeat **/
    AS_Button_dd50ce8727e54c398f4a9e452c99ed88: function AS_Button_dd50ce8727e54c398f4a9e452c99ed88(eventobject, context) {
        var self = this;
        this.showPayABill();
    },
    /** onClick defined for btnEdit **/
    AS_Button_g2981ca29ab34bf8bee33719bfd30d5a: function AS_Button_g2981ca29ab34bf8bee33719bfd30d5a(eventobject, context) {
        var self = this;
        this.showPayABill();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_c8a6db06ee144a399da63a6319004e0b: function AS_FlexContainer_c8a6db06ee144a399da63a6319004e0b(eventobject, context) {
        var self = this;
        this.segmentHistoryRowClick();
    }
});