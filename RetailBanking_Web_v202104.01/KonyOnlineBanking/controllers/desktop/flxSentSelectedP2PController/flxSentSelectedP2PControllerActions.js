define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnModify **/
    AS_Button_d8a74b663cec44ddb1d0c0c8813c806d: function AS_Button_d8a74b663cec44ddb1d0c0c8813c806d(eventobject, context) {
        var self = this;
        this.showSendMoney();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_d99fc8900da14e1a8862de32da32aeb3: function AS_FlexContainer_d99fc8900da14e1a8862de32da32aeb3(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});