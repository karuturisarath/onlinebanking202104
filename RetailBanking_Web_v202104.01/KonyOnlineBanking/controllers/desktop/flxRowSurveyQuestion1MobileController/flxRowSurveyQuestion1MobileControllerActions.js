define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxRating1 **/
    AS_FlexContainer_a2f534a41ffa4991b53be6535ee778fc: function AS_FlexContainer_a2f534a41ffa4991b53be6535ee778fc(eventobject, context) {
        var self = this;
        this.showRatingActionCircle(1);
    },
    /** onClick defined for flxRating2 **/
    AS_FlexContainer_c2ae3d0aaaeb4dd58ed510923b58582a: function AS_FlexContainer_c2ae3d0aaaeb4dd58ed510923b58582a(eventobject, context) {
        var self = this;
        this.showRatingActionCircle(2);
    },
    /** onClick defined for flxRating3 **/
    AS_FlexContainer_efb97841a97a4d70944a2a3d5a0c671d: function AS_FlexContainer_efb97841a97a4d70944a2a3d5a0c671d(eventobject, context) {
        var self = this;
        this.showRatingActionCircle(3);
    },
    /** onClick defined for flxRating4 **/
    AS_FlexContainer_ab184e78c3e641fb94a6ec3d3561a90b: function AS_FlexContainer_ab184e78c3e641fb94a6ec3d3561a90b(eventobject, context) {
        var self = this;
        this.showRatingActionCircle(4);
    },
    /** onClick defined for flxRating5 **/
    AS_FlexContainer_ae2ce52da83b4efa93b1e3c4c83d2183: function AS_FlexContainer_ae2ce52da83b4efa93b1e3c4c83d2183(eventobject, context) {
        var self = this;
        this.showRatingActionCircle(5);
    }
});