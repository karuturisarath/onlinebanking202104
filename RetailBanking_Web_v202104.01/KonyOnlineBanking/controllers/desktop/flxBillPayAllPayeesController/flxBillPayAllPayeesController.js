define(['FormControllerUtility'], function(FormControllerUtility) {
    return {
        segmentAllPayeesRowClick: function() {
            var currForm = kony.application.getCurrentForm();
            var index = currForm.tableView.segmentBillpay.selectedRowIndex[1];
            var data = currForm.tableView.segmentBillpay.data;
            for (i = 0; i < data.length; i++) {
                if (i == index) {
                    data[i].imgDropdown = "chevron_up.png";
                    data[i].imgError = "error_yellow.png";
                    data[i].lblError = "Verify payment amount and available account balance";
                    data[i].template = "flxBillPayAllPayeesSelected";
                } else {
                    data[i].imgDropdown = "arrow_down.png";
                    data[i].imgError = "error_yellow.png";
                    // data[i].lblError = "lblError";
                    if (kony.application.getCurrentBreakpoint() == 640) {
                        data[i].template = "flxBillPayAllPayeesMobile";
                    } else {
                        data[i].template = "flxBillPayAllPayees";
                    }
                }
            }
            currForm.tableView.segmentBillpay.setData(data);
            data = currForm.tableView.segmentBillpay.data;
            // data[index].calSendOn.context = {
            //     "widget":currForm.tableView.segmentBillpay.clonedTemplates[index].calSendOn,
            //     "anchor":"bottom"
            // }
            // data[index].calDeliverBy.context = {
            //   "widget":currForm.tableView.segmentBillpay.clonedTemplates[index].calDeliverBy,
            //   "anchor":"bottom"
            // }
            currForm.tableView.segmentBillpay.setDataAt(data[index], index);
            this.AdjustScreen(321);
            //kony.timer.schedule("mytimer12",this.timerFunc, 0, false);
        },
        timerFunc: function() {
            kony.application.getCurrentForm().tableView.segmentBillpay.selectedRowIndex = kony.application.getCurrentForm().tableView.segmentBillpay.selectedRowIndex;
            this.AdjustScreen(30);
        },
        viewEBill: function() {
            var currForm = kony.application.getCurrentForm();
            FormControllerUtility.updateWidgetsHeightInInfo(currForm, ['flxContainer']);
            var height_to_set = 140 + currForm.flxContainer.info.frame.height;
            currForm.flxViewEbill.height = height_to_set + "dp";
            currForm.flxViewEbill.isVisible = true;
            this.AdjustScreen(30);
            currForm.forceLayout();
        },
        //UI Code
        AdjustScreen: function(data) {
            var currForm = kony.application.getCurrentForm();
            if (data !== undefined) {} else {
                data = 0;
            }
            var footerTop = currForm.flxFooter.info.frame.y;
            footerTop += data;
            currForm.flxFooter.top = footerTop + "dp";
            currForm.forceLayout();
            return;
        },
        /* calculateTopForCustomsListBox:function(index)
         {
           var currForm = kony.application.getCurrentForm();
           var topCalculated=314,topCalculated_category;
           if(currForm.tableView.Search.isVisible===true)
            {
              topCalculated=topCalculated+101;
            }
           topCalculated = topCalculated+(270*index);
           topCalculated_category=topCalculated+81;
           currForm.flxCustomListBoxContainerPayFrom.top=topCalculated+"dp";
           currForm.flxCustomListBoxContainerCategory.top=topCalculated_category+"dp";
         }
           */
    };
});