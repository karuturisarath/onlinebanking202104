/**
 * Description of Module representing a New Account Opening form.
 * @module frmNAOController
 */
define("CampaignManagement/userfrmShortTermDepositController", ['CommonUtilities', 'OLBConstants', 'FormControllerUtility', 'ViewConstants'], function(CommonUtilities, OLBConstants, FormControllerUtility, ViewConstants) {
    var orientationHandler = new OrientationHandler();
    var internalData = {};
    var depositData = {};
    return /** @alias module:frmNAOController */ {
        /** updates the present Form based on required function.
         * @param {uiDataMap[]} uiDataMap list of functions
         */
        updateFormUI: function(uiDataMap) {
            var self = this;
            if (uiDataMap.showLoadingIndicator) {
                if (uiDataMap.showLoadingIndicator.status === true) {
                    FormControllerUtility.showProgressBar(this.view);
                } else {
                    FormControllerUtility.hideProgressBar(this.view);
                }
            }
            if (uiDataMap.enableDownTimeWarning) {
                this.view.rtxDowntimeWarning.text = uiDataMap.enableDownTimeWarning;
                this.view.flxDownTimeWarning.setVisibility(true);
                this.AdjustScreen();
            }
            if (uiDataMap.enableflxAccounts) {
                this.hideAll();
                this.view.ShortTermDeposit.lblFavoriteEmailCheckBox.text = "C";
                this.view.flxAccounts.setVisibility(true);
                this.AdjustScreen();
            }
            if (uiDataMap.enableflxDeposits) {
                self.setFromAccounts(uiDataMap.data);
                self.enableSkins();
            }
            if (uiDataMap.enableflxReviewApplication) {
                this.hideAll();
                this.depositData = uiDataMap.depositData;
                this.setReviewApplicationData(uiDataMap.depositData);
                this.view.ShortTermDeposit.flxError.setVisibility(false);
                this.view.ShortTermDeposit.tbxLoanPayOffAmount.skin = "skntxt727272";
                this.view.flxReviewApplication.setVisibility(true);
                this.AdjustScreen();
            }
            if (uiDataMap.modifyflxReviewApplication) {
                this.hideAll();
                this.populateLoansData(this.depositData);
                this.view.flxDeposits.setVisibility(true);
                this.AdjustScreen();
            }
            if (uiDataMap.enableAcknowledgement) {
                this.hideAll();
                this.populateAcknowledgementData(this.depositData);
                //this.view.flxMainLeftContainers.width = "87%";       
                this.view.flxNAOAcknowledgement.setVisibility(true);
                this.AdjustScreen();
                this.view.forceLayout();
            }
        },
        /**
         * used to set the actions to the  NAO On clicks
         */
        setFlowActions: function() {
            var self = this;
            this.view.onBreakpointChange = function() {
                self.onBreakpointChange(kony.application.getCurrentBreakpoint());
            };
            this.view.CampaignDescription.btnAccountsDescriptionCancel.onClick = function() {
                self.NavigateToAccountsLandingForm();
            };
            this.view.CampaignDescription.btnAccountsDescriptionProceed.onClick = function() {
                var CampaignManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('CampaignManagement');
                CampaignManagementModule.presentationController.getFromAccounts();
            };
            this.view.ShortTermDeposit.btnCancel.onClick = function() {
                var context = {
                    "enableflxAccounts": true
                };
                self.updateFormUI(context);
            }
            this.view.ShortTermDeposit.btnloanDetailsCancel.onClick = function() {
                self.NavigateToAccountsLandingForm();
            }
            this.view.ShortTermDeposit.btnConfirm.onClick = function() {
                var amt = Number(self.view.ShortTermDeposit.tbxLoanPayOffAmount.text.replace(/[^0-9.]/g, ''));
                var accBalance = Number(self.view.ShortTermDeposit.lbxsavingsAccount.selectedKey.split("!!")[1].replace(/[^0-9.]/g, ''));
                self.validateForm(amt, accBalance);
                //var context = {"enableflxReviewApplication":true,"enteredData":self.enteredData()};
                //self.updateFormUI(context);
            };
            this.view.confirmDialog.confirmButtons.btnModify.onClick = function() {
                var context = {
                    "modifyflxReviewApplication": true
                };
                self.updateFormUI(context);
            };
            this.view.confirmDialog.confirmButtons.btnCancel.onClick = function() {
                self.view.flxDownTimeWarning.setVisibility(false);
                self.view.flxSubmitApplication.setVisibility(true);
                self.AdjustScreen();
            };
            this.view.SubmitApplication.btnNo.onClick = function() {
                self.view.flxSubmitApplication.setVisibility(false);
                self.AdjustScreen();
            };
            this.view.SubmitApplication.btnYes.onClick = function() {
                self.NavigateToAccountsLandingForm();
                self.view.flxSubmitApplication.setVisibility(false);
            };
            this.view.SubmitApplication.flxCross.onClick = function() {
                self.view.flxSubmitApplication.setVisibility(false);
            };
            this.view.confirmDialog.confirmButtons.btnConfirm.onClick = function() {
                var params = self.getdataForCreateDeposit(self.depositData);
                var CampaignManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('CampaignManagement');
                CampaignManagementModule.presentationController.createDeposit(params);
            };
            this.view.ShortTermDeposit.lblFavoriteEmailCheckBox.onTouchStart = function() {
                self.toggleProductSelectionCheckbox();
            };
            this.view.ShortTermDeposit.imgMaturityInfo.onTouchMove = function() {
                self.view.ShortTermDeposit.flxInformation.setVisibility(true);
                self.AdjustScreen();
            };
            this.view.ShortTermDeposit.tbxLoanPayOffAmount.onKeyUp = function() {
                self.populateOnDepositAmountChange();
                self.enableSkins();
            };
            this.view.AcknowledgementCM.btnAckGoToAccounts.onClick = function() {
                self.NavigateToAccountsLandingForm();
            };
            this.view.ShortTermDeposit.btnTermsAndConditions.onClick = function() {
                self.view.flxTermsAndConditions.setVisibility(true);
                self.AdjustScreen();
            };
            this.view.imgClose.onTouchEnd = function() {
                self.view.flxTermsAndConditions.setVisibility(false);
            };
            this.view.ShortTermDeposit.imgMaturityInfo.onTouchMove = function() {
                self.view.ShortTermDeposit.flxa.setVisibility(true);
                self.AdjustScreen();
                self.view.forceLayout();
            };
            this.view.ShortTermDeposit.imgFlxCross.onTouchEnd = function() {
                self.view.ShortTermDeposit.flxa.setVisibility(false);
            };
            this.view.ShortTermDeposit.lbxsavingsAccount.onSelection = function() {
                self.enableSkins();
            };
        },
        /*Populates the data in Loan Details form*/
        populateLoansData: function(depositData) {
            this.view.ShortTermDeposit.lbxsavingsAccount.selectedKey.text = depositData.fromAccount;
            this.view.ShortTermDeposit.tbxLoanPayOffAmount.text = depositData.initialDepositAmt;
            this.view.ShortTermDeposit.lblLoanEndDate.text = depositData.duration;
            this.view.ShortTermDeposit.lblInterestRate.text = depositData.interestRate;
            this.view.ShortTermDeposit.lblIntrestAmt.text = depositData.interestAmt;
            this.view.ShortTermDeposit.lblTotalRepaymenyAmt.text = depositData.totalMaturityAmt;
            this.view.ShortTermDeposit.lbxMaturityInstructions.selectedKey.text = depositData.maturityInstructions;
            this.view.ShortTermDeposit.lbxCreditAccountNumber.selectedKey.text = depositData.creditAccountNo;
            this.view.ShortTermDeposit.tbxOptional.text = depositData.reason;
        },
        /*Populates the data in reView form*/
        setReviewApplicationData: function(depositData) {
            this.view.confirmDialog.keyValueFrom.lblValue.text = CommonUtilities.getMaskedAccountNumber(depositData.fromAccount);
            this.view.confirmDialog.keyValueTo.lblValue.text = depositData.initialDepositAmt;
            this.view.confirmDialog.keyValueAmount.lblValue.text = depositData.duration;
            this.view.confirmDialog.keyValuePaymentDate.lblValue.text = depositData.interestRate;
            this.view.confirmDialog.keyValueFrequency.lblValue.text = depositData.interestAmt;
            this.view.confirmDialog.keyValueFrequencyType.lblValue.text = depositData.totalMaturityAmt;
            this.view.confirmDialog.keyValueNote.lblValue.text = depositData.maturityInstructions;
            this.view.confirmDialog.keyValue.lblValue.text = CommonUtilities.getMaskedAccountNumber(depositData.creditAccountNo);
            this.view.confirmDialog.keyValue1.lblValue.text = depositData.reason;
        },
        calculateInterestAmount: function(rateOfInterest, amount) {
            var principalAmt = Number(amount);
            var interest = Number(rateOfInterest);
            var intAmount = (principalAmt * (interest / 100)).toFixed(2);
            return intAmount;

        },
        calculateTotalAmount: function(amount, interestAmount) {
            var principalAmt = Number(amount);
            principalAmt = Number(principalAmt);
            return (Number(principalAmt) + Number(interestAmount)).toFixed(2);
        },
        populateOnDepositAmountChange: function() {
            var depositAmount = this.view.ShortTermDeposit.tbxLoanPayOffAmount.text.replace(/,/g, "");
            var interestRate = this.view.ShortTermDeposit.lblInterestRate.text;
            var currCurrency = applicationManager.getConfigurationManager().getCurrencyCode();
            interestRate = interestRate.replace("%", "");
            depositAmount = depositAmount.replace(currCurrency, "");
            var interestAmount = this.calculateInterestAmount(interestRate, depositAmount);
            this.view.ShortTermDeposit.lblIntrestAmt.text = currCurrency + interestAmount.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            var totalRepayAmount = this.calculateTotalAmount(depositAmount, interestAmount);
            this.view.ShortTermDeposit.lblTotalRepaymenyAmt.text = currCurrency + totalRepayAmount.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            this.view.ShortTermDeposit.tbxLoanPayOffAmount.text = currCurrency + depositAmount.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },
        /*Populates the data in Acknowledgement form*/
        populateAcknowledgementData: function(depositData) {
            this.view.AcknowledgementCM.confirmDialog.lblValue.text = CommonUtilities.getMaskedAccountNumber(depositData.fromAccount);
            this.view.AcknowledgementCM.confirmDialog.lblValueTo.text = depositData.initialDepositAmt;
            this.view.AcknowledgementCM.confirmDialog.RichText0bfcfe81705e240.text = depositData.duration;
            this.view.AcknowledgementCM.confirmDialog.CopyRichText0j287807d04b44c.text = depositData.interestRate;
            this.view.AcknowledgementCM.confirmDialog.lblValueDescription.text = CommonUtilities.getMaskedAccountNumber(depositData.interestAmt);
            this.view.AcknowledgementCM.confirmDialog.CopylblValueDescription0e7e996a47e5744.text = depositData.totalMaturityAmt;
            this.view.AcknowledgementCM.confirmDialog.CopylblValueDescription0b68a3d2d9c5942.text = depositData.maturityInstructions;
            this.view.AcknowledgementCM.confirmDialog.CopylblValueDescription0j4d08d9160fb4d.text = CommonUtilities.getMaskedAccountNumber(depositData.creditAccountNo);
        },
        /*Forms JSON for the entered data in flxDeposits flex*/
        enteredData: function() {
            var data = {
                fromAccount: this.view.ShortTermDeposit.lbxsavingsAccount.selectedKey.split("!!")[0],
                initialDepositAmt: this.view.ShortTermDeposit.tbxLoanPayOffAmount.text,
                duration: this.view.ShortTermDeposit.lblLoanEndDate.text,
                interestRate: this.view.ShortTermDeposit.lblInterestRate.text,
                interestAmt: this.view.ShortTermDeposit.lblIntrestAmt.text,
                totalMaturityAmt: this.view.ShortTermDeposit.lblTotalRepaymenyAmt.text,
                maturityInstructions: this.view.ShortTermDeposit.lbxMaturityInstructions.selectedKey,
                creditAccountNo: this.view.ShortTermDeposit.lbxCreditAccountNumber.selectedKey.split("!!")[0],
                availableBalance: this.view.ShortTermDeposit.lbxsavingsAccount.selectedKey.split("!!")[1],
            };
            return data;
        },
        getdataForCreateDeposit: function(depositData) {
            var amount = Number(depositData.initialDepositAmt.replace(/[^0-9.]/g, ''));
            var params = {
                "amount": amount,
                "productId": "PRODUCT9",
                "payingAccount": depositData.fromAccount,
                "campaignId": "",
                "payoutAccount": depositData.creditAccountNo,
                "userId": "",
                "productType": ""
            };
            return params;
        },
        /**
         * Method to load and return NAO Module.
         * @returns {object} NAO Module object.
         */
        loadNAOModule: function() {
            return kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("NAOModule");
        },
        /**
         * Form preShow action handler
         */
        preshow: function() {
            var scopeObj = this;
            this.hideAll();
            this.view.flxAccounts.setVisibility(true);
            applicationManager.getNavigationManager().applyUpdates(this);
            this.setFlowActions();
            this.view.ShortTermDeposit.lbxsavingsAccount.selectedValue = "Savings Account Nicknaame";
            this.view.ShortTermDeposit.lbxMaturityInstructions.selectedValue = "Renew Principal";
            this.view.ShortTermDeposit.lbxCreditAccountNumber.selectedValue = "Account Number - X 8760";
            this.view.ShortTermDeposit.lblFavoriteEmailCheckBox.text = "D";
            this.view.ShortTermDeposit.tbxLoanPayOffAmount.text = applicationManager.getConfigurationManager().getCurrencyCode() + "10,000.00";
            this.view.ShortTermDeposit.lblIntrestAmt.text = applicationManager.getConfigurationManager().getCurrencyCode() + "1500.00";
            this.view.ShortTermDeposit.lblTotalRepaymenyAmt.text = applicationManager.getConfigurationManager().getCurrencyCode() + "1500.00";
            this.populateOnDepositAmountChange();
            FormControllerUtility.disableButton(this.view.ShortTermDeposit.btnConfirm);
            this.view.customheader.forceCloseHamburger();
            applicationManager.getLoggerManager().setCustomMetrics(this, false, "Campaign Management");
            FormControllerUtility.updateWidgetsHeightInInfo(this.view, ['flxFooter', 'flxHeader', 'flxContainer','flxFormContent']);
            //this.loadNAOModule().presentationController.getCampaignsNAO();
        },
        showReviewApplication: function(products) {
            if (kony.application.getCurrentBreakpoint() === 640) {
                this.view.customheader.lblHeaderMobile.text = "Review Application";
            } else {
                this.view.customheader.lblHeaderMobile.text = "";
            }
            this.hideAll();
            // this.view.flxMainLeftContainers.width = "87%";      
            this.view.flxReviewApplication.setVisibility(true);
            this.view.ReviewApplicationNAO.flxColorLine.setFocus(true);
            this.setUserData();
            this.setSelectedAccountSegmentData(products);
            this.view.ReviewApplicationNAO.btnEdit.onClick = this.showSelectedProductsList;
            this.view.ReviewApplicationNAO.btnReviewApplicationSubmit.onClick = this.performCreditCheck.bind(this, this.saveProducts.bind(this, products));
            this.AdjustScreen();
        },
        /** 
         * get the total form height
         * @returns {string} height
         */
        getTotalFormHeight: function() {
            return this.view.flxHeader.frame.height + this.view.flxContainer.frame.height + this.view.flxFooter.frame.height + "dp";
        },
        /**
         * Toggle Select Product Checkbox of Select Products Segment
         * @param {object}  index of row
         */
        toggleProductSelectionCheckbox: function() {
            if (this.view.ShortTermDeposit.lblFavoriteEmailCheckBox.text === "C") {
                this.view.ShortTermDeposit.lblFavoriteEmailCheckBox.text = "D";
                FormControllerUtility.disableButton(this.view.ShortTermDeposit.btnConfirm);
            } else {
                this.view.ShortTermDeposit.lblFavoriteEmailCheckBox.text = "C";
                FormControllerUtility.enableButton(this.view.ShortTermDeposit.btnConfirm);
            }
        },
        /**
         * shows the acknowledgement scrren
         * @param {object} acknowledgementView acknowledgementViewModel
         */
        showAcknowledgement: function(acknowledgementView) {
            this.showAcknowledgementUI();
            this.setSelectedProductsData(acknowledgementView.selectedProducts)
            this.view.AcknowledgementNAO.btnAckFundAccounts.onClick = function() {
                var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                accountModule.presentationController.showAccountsDashboard();
            }
            if (kony.application.getCurrentBreakpoint() == 640) {
                this.view.customheader.lblHeaderMobile.text = kony.i18n.getLocalizedString("i18n.transfers.Acknowledgement");
                this.view.customheader.forceLayout();
            } else {
                this.view.customheader.lblHeaderMobile.text = "";
            }
        },
        /**
         * shows the AcknowledgementUI
         */
        showAcknowledgementUI: function() {
            this.hideAll();
            this.view.flxNAOAcknowledgement.setVisibility(true);
            this.view.AcknowledgementNAO.btnAckGoToAccounts.setVisibility(false);
            this.view.AcknowledgementNAO.btnAckFundAccounts.text = kony.i18n.getLocalizedString("i18n.NAO.GoToAccounts");
            this.view.AcknowledgementNAO.btnAckFundAccounts.toolTip = kony.i18n.getLocalizedString("i18n.NAO.GoToAccounts");
            this.AdjustScreen();
        },
        /**
         * hides the all UI
         */
        hideAll: function() {
            this.view.flxDownTimeWarning.setVisibility(false);
            this.view.flxAccounts.setVisibility(false);
            this.view.flxNAOAcknowledgement.setVisibility(false);
            this.view.flxReviewApplication.setVisibility(false);
            this.view.flxDeposits.setVisibility(false);
        },
        /**
         * post show of the form
         */
        postshowfrmNAO: function() {
            this.AdjustScreen();
        },
        /**
         * used to navigate the account landing form
         */
        NavigateToAccountsLandingForm: function() {
            var frmName;
            var configurationManager = applicationManager.getConfigurationManager();
            if (configurationManager.isSMEUser === "true")
                frmName = "frmDashboard";
            else if (configurationManager.isCombinedUser === "true")
                frmName = "frmDashboard";
            else
                frmName = "frmDashboard";
            applicationManager.getNavigationManager().navigateTo(frmName);
        },
        /**
         *AdjustScreen- function to adjust the footer
         */
        AdjustScreen: function() {
            this.view.forceLayout();
            var mainheight = 0;
            var screenheight = kony.os.deviceInfo().screenHeight;
            mainheight = this.view.customheader.frame.height + this.view.flxContainer.frame.height;
            var diff = screenheight - mainheight;
            if (mainheight < screenheight) {
                diff = diff - this.view.flxFooter.frame.height;
                if (diff > 0) {
                    this.view.flxFooter.top = mainheight + diff + "dp";
                } else {
                    this.view.flxFooter.top = mainheight + "dp";
                }
            } else {
                this.view.flxFooter.top = mainheight + "dp";
            }
            this.view.forceLayout();
        },
        /**
         * onBreakpointChange : Handles ui changes on .
         * @member of {frmNAOController}
         * @param {integer} width - current browser width
         * @return {} 
         * @throws {}
         */
        onBreakpointChange: function(width) {
            var scope = this;
            this.view.CustomPopupLogout.onBreakpointChangeComponent(scope.view.CustomPopupLogout, width)
            this.AdjustScreen();
        },
        FromAccountsMapper: function(accountsData) {
            if (accountsData.nickName === undefined || accountsData.nickName === null || accountsData.nickName === "") {
                return [accountsData.accountID + "!!" + accountsData.availableBalance, accountsData.accountName];
            } else {
                return [accountsData.accountID + "!!" + accountsData.availableBalance, accountsData.nickName + " - " + CommonUtilities.getMaskedAccountNumber(accountsData.accountID) + "(" + applicationManager.getConfigurationManager().getCurrencyCode() + this.numberWithCommas(accountsData.availableBalance) + ")"];
            }
        },
        creditAccountsMapper: function(accountsData) {
            return [accountsData.accountID + "!!" + accountsData.availableBalance, "Account Number - " + CommonUtilities.getMaskedAccountNumber(accountsData.accountID)];
        },
        setFromAccounts: function(internalAccountsdata) {
            this.view.ShortTermDeposit.lbxsavingsAccount.masterData = internalAccountsdata.map(this.FromAccountsMapper);
            this.view.ShortTermDeposit.lbxCreditAccountNumber.masterData = internalAccountsdata.map(this.creditAccountsMapper);
            this.hideAll();
            this.view.ShortTermDeposit.lblFavoriteEmailCheckBox.text = "D";
            FormControllerUtility.disableButton(this.view.ShortTermDeposit.btnConfirm);
            this.view.flxDeposits.setVisibility(true);
            this.view.forceLayout();
        },
        validateForm: function(amt, bal) {
            if (bal < amt) {
                this.view.ShortTermDeposit.tbxLoanPayOffAmount.skin = "skntxtSSP424242BorderFF0000Op100Radius2px";
                this.view.ShortTermDeposit.lblError.text = "Deposit amount must be less than account balance";
                this.view.ShortTermDeposit.flxError.setVisibility(true);
            } else {
                var context = {
                    "enableflxReviewApplication": true,
                    "depositData": this.enteredData()
                };
                this.updateFormUI(context);
            }
        },
        enableSkins: function() {
            this.view.ShortTermDeposit.flxError.setVisibility(false);
            this.view.ShortTermDeposit.tbxLoanPayOffAmount.skin = "skntxt727272";
            if (this.view.ShortTermDeposit.lblFavoriteEmailCheckBox.text === "C") {
                FormControllerUtility.enableButton(this.view.ShortTermDeposit.btnConfirm);
                this.AdjustScreen();
            } else {
                FormControllerUtility.disableButton(this.view.ShortTermDeposit.btnConfirm);
                this.AdjustScreen();
            }
        },
        numberWithCommas: function(x) {
            x = x.toString();
            var pattern = /(-?\d+)(\d{3})/;
            while (pattern.test(x))
                x = x.replace(pattern, "$1,$2");
            return x;
        },
    };
});