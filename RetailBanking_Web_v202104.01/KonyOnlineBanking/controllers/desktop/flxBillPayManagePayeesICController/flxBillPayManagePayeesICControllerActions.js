define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnEbill **/
    AS_Button_cc7c9b6a6b65410db404fcb485b7c33c: function AS_Button_cc7c9b6a6b65410db404fcb485b7c33c(eventobject, context) {
        var self = this;
        this.viewEBill();
    },
    /** onClick defined for btnAction **/
    AS_Button_h5113687294340a2ab53c4d4806cda67: function AS_Button_h5113687294340a2ab53c4d4806cda67(eventobject, context) {
        var self = this;
        //Just adding comment to register action, will set in form controller
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_g7726ef40a524f3aa4ae7e5c8bb14ad3: function AS_FlexContainer_g7726ef40a524f3aa4ae7e5c8bb14ad3(eventobject, context) {
        var self = this;
        this.segmentManagePayeesRowClick();
    }
});