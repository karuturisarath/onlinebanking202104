define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for CopybtnCancel0f5022f26d6cb40 **/
    AS_Button_c86fbf7b80404cfbb38374a55f9ab7c1: function AS_Button_c86fbf7b80404cfbb38374a55f9ab7c1(eventobject) {
        var self = this;
        this.onActivationCancel();
    },
    /** onClick defined for btnProceed **/
    AS_Button_ef24b1158ee84170b96c48412cc32a5f: function AS_Button_ef24b1158ee84170b96c48412cc32a5f(eventobject) {
        var self = this;
        this.onActivationContinue();
    },
    /** init defined for frmActivateWireTransfer **/
    AS_Form_f10b013bf6544cd2b7010131cf07404f: function AS_Form_f10b013bf6544cd2b7010131cf07404f(eventobject) {
        var self = this;
        this.init();
    }
});