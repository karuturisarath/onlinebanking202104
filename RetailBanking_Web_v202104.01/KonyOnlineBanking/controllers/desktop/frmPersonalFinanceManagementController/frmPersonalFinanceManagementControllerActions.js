define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onClick defined for btnBreadcrumb1 **/
    AS_Button_fb5a0d943cbd487ea26c5488b18a8748: function AS_Button_fb5a0d943cbd487ea26c5488b18a8748(eventobject) {
        var self = this;
        var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
        accountModule.presentationController.showAccountsDashboard();
    },
    /** init defined for frmPersonalFinanceManagement **/
    AS_Form_ef3509b60474425dbf5352bd5df3569c: function AS_Form_ef3509b60474425dbf5352bd5df3569c(eventobject) {
        var self = this;
        this.initActions();
    },
    /** preShow defined for frmPersonalFinanceManagement **/
    AS_Form_fd9b9dde468744cfb9ec015b69f1543f: function AS_Form_fd9b9dde468744cfb9ec015b69f1543f(eventobject) {
        var self = this;
        this.preShow();
    },
    /** postShow defined for frmPersonalFinanceManagement **/
    AS_Form_j76ffebeb9504dd99c56d3766e15f0bb: function AS_Form_j76ffebeb9504dd99c56d3766e15f0bb(eventobject) {
        var self = this;
        this.postShow();
    },
    /** onDeviceBack defined for frmPersonalFinanceManagement **/
    AS_Form_g11cf0172e714728bb68cfa47b9be073: function AS_Form_g11cf0172e714728bb68cfa47b9be073(eventobject) {
        var self = this;
        kony.print("");
    },
    /** onTouchEnd defined for frmPersonalFinanceManagement **/
    AS_Form_a71b3588cef84880b7449832947fe27c: function AS_Form_a71b3588cef84880b7449832947fe27c(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});