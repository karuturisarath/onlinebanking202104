define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onSelection defined for lstType **/
    AS_ListBox_jaa01a743ea746369350e390869e7174: function AS_ListBox_jaa01a743ea746369350e390869e7174(eventobject, context) {
        var self = this;
        return self.onTypeSelection.call(this, eventobject, context);
    },
    /** onTextChange defined for tbxLowerLimit **/
    AS_TextField_b04dfa97611340b3a013f866e6bb666a: function AS_TextField_b04dfa97611340b3a013f866e6bb666a(eventobject, changedtext, context) {
        var self = this;
        return self.onLimitChanged.call(this, eventobject, changedtext);
    },
    /** onKeyUp defined for tbxUpperLimit **/
    AS_TextField_b3b5f9f5842d481ebf95f02c934c6748: function AS_TextField_b3b5f9f5842d481ebf95f02c934c6748(eventobject, context) {
        var self = this;
        return self.onLimitValueChange.call(this, eventobject, context);
    },
    /** onTextChange defined for tbxUpperLimit **/
    AS_TextField_c14539085b654f3db1c068ec55386766: function AS_TextField_c14539085b654f3db1c068ec55386766(eventobject, changedtext, context) {
        var self = this;
        return self.onLimitChanged.call(this, eventobject, changedtext);
    },
    /** onKeyUp defined for tbxLowerLimit **/
    AS_TextField_f90cfadf9acc46fb80dff4291910b8ac: function AS_TextField_f90cfadf9acc46fb80dff4291910b8ac(eventobject, context) {
        var self = this;
        return self.onLimitValueChange.call(this, eventobject, context);
    }
});