define({
    showSelectedRow: function() {
        var currForm = kony.application.getCurrentForm();
        var index = currForm.transactions.segTransactions.selectedIndex[1];
        var data = currForm.transactions.segTransactions.data;
        for (i = 0; i < data.length; i++) {
            if (i === index) {
                data[i].imgDropdown.src = "chevron_up.png";
                data[i].template = "flxSegCheckImages";
            } else {
                data[i].imgDropdown.src = "arrow_down.png";
                data[i].template = "flxCheckImageUnSelected";
            }
        }
        currForm.transactions.segTransactions.setData(data);
        this.AdjustScreen(268);
    },
    AdjustScreen: function(data) {
        var currentForm = kony.application.getCurrentForm();
        var mainheight = 0;
        var screenheight = kony.os.deviceInfo().screenHeight;
        mainheight = currentForm.customheader.frame.height + currentForm.flxMainWrapper.frame.height;
        var diff = screenheight - mainheight;
        if (mainheight < screenheight) {
            diff = diff - currentForm.flxFooter.frame.height;
            if (diff > 0)
                currentForm.flxFooter.top = mainheight + diff + data + "dp";
            else
                currentForm.flxFooter.top = mainheight + data + "dp";
            currentForm.forceLayout();
        } else {
            currentForm.flxFooter.top = mainheight + data + "dp";
            currentForm.forceLayout();
        }
    },
});