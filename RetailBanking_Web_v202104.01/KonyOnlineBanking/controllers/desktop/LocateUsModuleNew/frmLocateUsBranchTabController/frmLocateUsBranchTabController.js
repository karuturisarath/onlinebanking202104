define(['commonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(commonUtilities, OLBConstants, viewConstants, FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();
    return {
        /**
         * Update form UI
         */
        updateFormUI: function(locateUsViewModel) {
            if (!locateUsViewModel) return;
            if (locateUsViewModel.showProgressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (locateUsViewModel.showProgressBar === false) {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (locateUsViewModel.inFormError) {
                this.view.rtxMessageWarning.text = viewPropertiesMap.inFormError.errorMessage;
                this.view.flxWarning.setVisibility(true);
            }
            if (locateUsViewModel.preLoginView) {
                this.showPreLoginView();
            }
            if (locateUsViewModel.postLoginView) {
                this.showPostLoginView();
            }
            if (locateUsViewModel.getBranchOrATMListSuccess) {
                this.getBranchOrATMListSuccessCallback(locateUsViewModel.getBranchOrATMListSuccess);
            }
            if (locateUsViewModel.getBranchOrATMListFailure) {
                this.getBranchOrATMListErrorCallback();
            }
            if (locateUsViewModel.getAtmorBranchDetailsSuccess) {
                this.getAtmOrBranchDetailsSuccessCallback(locateUsViewModel.getAtmorBranchDetailsSuccess);
            }
            if (locateUsViewModel.getAtmorBranchDetailsFailure) {
                this.getAtmOrBranchDetailsErrorCallback();
            }
            if (locateUsViewModel.getSearchBranchOrATMListSuccess) {
                this.getSearchBranchOrATMListSuccessCallback(locateUsViewModel.getSearchBranchOrATMListSuccess);
            }
            if (locateUsViewModel.getSearchBranchOrATMListFailure) {
                this.getSearchBranchOrATMListErrorCallback();
            }
            if (locateUsViewModel.geoLocationError) {
                this.noSearchResultUI();
                this.view.lblInfoErrorMessage.text = kony.i18n.getLocalizedString("i18n.LocateUs.geolocationNoSearchData");
                FormControllerUtility.hideProgressBar(this.view);
            }
        },

        /**
         * Search data success callback
         * @param {Object} data: search results
         */
        getSearchBranchOrATMListSuccessCallback: function(data) {
            if (data.length === 0) {
                this.getSearchBranchOrATMListErrorCallback();
            } else {
                this.searchResultData = data;
                this.filterSearchData();
            }
        },
        /**
         * Search data error callback
         */
        getSearchBranchOrATMListErrorCallback: function() {
            this.noSearchResultUI();
        },

        /**
         * Clears the search text and make a service call
         */
        searchTextClearAndRefresh: function() {
            var locateUsPresenter = this.getLocateUsPresentationController();
            locateUsPresenter.getBranchOrATMList();
            this.view.tbxSearch.text = "";
        },

        /**
         * Makes service call based on serach text
         */
        searchData: function(searchText) {
            var scopeObj = this;
            var queryParams = {
                "query": searchText.trim()
            };
            var presenter = scopeObj.getLocateUsPresentationController();
            presenter.getSearchBranchOrATMList(queryParams);
        },

        /**
         * it perform the search
         */
        performSearch: function() {
            var searchText = this.view.tbxSearch.text.trim();
            if (searchText !== null && searchText !== '' && searchText.length > 0) {
                this.searchData(searchText);
            } else {
                this.searchTextClearAndRefresh();
            }
        },

        /**
         * Sets filter chips in search
         */
        segSetFilters: function() {
            var filterlblWidget;
            var filterCloseBtn;
            var filterNum = 1;
            this.selectedServicesList = [];
            let data = this.view.segFilters.data;
            for (let i = 0; i < data.length; i++) {
                if (data[i].lblCheckbox.text === viewConstants.FONT_ICONS.CHECBOX_SELECTED) {
                    filterlblWidget = "lblFilterName" + filterNum;
                    filterCloseBtn = "flxCancelFilter" + filterNum;
                    var lbl = data[i].lblOption;
                    this.selectedServicesList.push(lbl);
                    this.view[filterlblWidget].text = lbl;
                    this.view[filterlblWidget].setVisibility(true);
                    this.view[filterCloseBtn].setVisibility(true);
                    filterNum++;
                }
            }
            this.view.flxFiltersRow1.setVisibility(false)
            this.view.flxFiltersRow2.setVisibility(false)
            this.view.flxFiltersRow3.setVisibility(false)
            if (filterNum - 1 > 0) {
                this.view.flxFiltersRow1.setVisibility(true)
                this.view.flxFilterWrapper.height = "50dp";
            }
            if (filterNum - 1 > 2) {
                this.view.flxFiltersRow2.setVisibility(true)
                this.view.flxFilterWrapper.height = "80dp";
            }
            if (filterNum - 1 > 4) {
                this.view.flxFiltersRow3.setVisibility(true)
                this.view.flxFilterWrapper.height = "100dp";
            }

            for (i = filterNum; i < 6; i++) {
                filterlblWidget = "lblFilterName" + i;
                filterCloseBtn = "flxCancelFilter" + i;
                this.view[filterlblWidget].setVisibility(false);
                this.view[filterCloseBtn].setVisibility(false);
            }
            this.view.flxFilterWrapper.setVisibility(filterNum > 1);
            this.view.flxBranchDetails.setVisibility(false);
        },


        pinToSegment: function(dataItem) {
            var scopeObj = this;
            var locationId = dataItem.locationId;
            var informationTitle = dataItem.informationTitle ? dataItem.informationTitle : dataItem.lblName;
            var data = scopeObj.view.segLoacteUsSearchResults.data;
            var index = -1;
            data.forEach(function(item, i) {
                if (item.locationId === locationId && item.lblName === informationTitle) {
                    index = i;
                }
            });
            if (index !== -1) {
                scopeObj.view.LocateUs.segResults.selectedRowIndex = [0, index];
                scopeObj.changeResultsRowSkin();
            }
        },

        /**
         * to apply the filter
         */
        applyFilters: function() {
            this.performSearch();
            this.segSetFilters();
            this.view.flxFilters.setVisibility(false);
            this.view.flxMapOverlay.setVisibility(false);
        },

        /**
         * return the services
         */
        getServices: function() {
            return [kony.i18n.getLocalizedString("i18n.LocateUs.DriveUpATM"), kony.i18n.getLocalizedString("i18n.LocateUs.SurchargeFreeATM"), kony.i18n.getLocalizedString("i18n.LocateUs.DepositTakingATM"), kony.i18n.getLocalizedString("i18n.LocateUs.CoOpSharedBranch"), kony.i18n.getLocalizedString("i18n.LocateUs.SafeDepositBox")];
        },

        /**
         * Sets Filters data
         */
        setViewsAndFilterSegmentData: function() {
            var scopeObj = this;
            var services = scopeObj.getServices();
            var dataMap = {
                "flximg": "flximg",
                "flxOption": "flxOption",
                "lblCheckbox": "lblCheckbox",
                "lblOption": "lblOption",
                "lblSepartor": "lblSepartor"
            };
            var data = services.map(function(item) {
                return {
                    "flximg": {
                        "onClick": scopeObj.segToggleCheckBox,
                        "skin": "skncursor"
                    },
                    "lblCheckbox": {
                        "text": viewConstants.FONT_ICONS.CHECBOX_UNSELECTED,
                    },
                    "lblOption": item,
                    "lblSepartor": "."
                };
            });
            this.view.segFilters.widgetDataMap = dataMap;
            this.view.segFilters.setData(data);
        },

        /**
         * Toggle filters checkbox
         */
        segToggleCheckBox: function() {
            var index = this.view.segFilters.selectedRowIndex;
            var rowIndex = index[1];
            var data = this.view.segFilters.data;
            if (data[rowIndex].lblCheckbox.text === viewConstants.FONT_ICONS.CHECBOX_UNSELECTED) {
                data[rowIndex].lblCheckbox.text = viewConstants.FONT_ICONS.CHECBOX_SELECTED;
            } else {
                data[rowIndex].lblCheckbox.text = viewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
            }
            this.view.segFilters.setDataAt(data[rowIndex], rowIndex);
        },

        /**
         * Failed to fetch branch details
         */
        getAtmOrBranchDetailsErrorCallback: function() {
            this.view.lblBranchName.text = kony.i18n.getLocalizedString("i18n.locateus.detailsNA");
            this.view.lblDistanceAndTimeFromUser.text = "";
            this.view.lblAddressLine2.text = "";
            this.view.lblAddressLine1.text = "";
            this.view.lblPhoneNumber1.text = "";
            this.view.lblPhoneNumber2.text = "";
            this.view.segDayAndTime.setData([]);
            this.view.segBranchService.setData([]);
            this.view.lblBranchName2.text = "";
        },

        /**
         * Sets result row skin on click
         */
        changeResultsRowSkin: function() {
            var index = this.view.segLoacteUsSearchResults.selectedRowIndex;
            var rowIndex = index[1];
            var data = this.view.segLoacteUsSearchResults.data;
            for (var i = 0; i < data.length; i++) {
                data[i].flxSearchResults.skin = viewConstants.SKINS.SKNFLXFFFFFFBORDER0;
                data[i].imgIndicator.isVisible = false;
                data[i].flxIndicator.skin = viewConstants.SKINS.BLANK_SKIN_FLEX
            }
            data[rowIndex].flxSearchResults.skin = viewConstants.SKINS.SKNFLEXF9F9F9;
            data[rowIndex].imgIndicator.isVisible = false;
            data[rowIndex].flxIndicator.skin = viewConstants.SKINS.PFM_LBLIDENTIFIER
            this.view.segLoacteUsSearchResults.setData(data);
        },

        /**
         * Show branch details section
         */
        showBranchDetails: function() {
            if (!responsiveUtils.isMobile)
                this.view.flxBranchDetails.setVisibility(true);
        },

        /**
         * Populates branch details section
         * @param {Object} data : contains branch details of click row
         */
        getAtmOrBranchDetailsSuccessCallback: function(data) {
            this.view.lblDistanceAndTimeFromUser.text = ""
            this.view.lblPhoneNumber2.text = "";
            if (data.addressLine2) {
                this.view.lblAddressLine2.text = data.addressLine2.trim();
            }
            if (data.addressLine1) {
                this.view.lblBranchName.text = data.addressLine1.trim();
                this.view.lblBranchName2.text = data.addressLine1.trim();
                this.view.lblAddressLine1.text = data.addressLine1.trim();
            }
            if (data.phoneNumber) {
                this.view.lblPhoneNumber1.text = data.phoneNumber.trim();
            } else {
                this.view.lblPhoneNumber1.text = "N/A";
            }
            if (data.services) {
                let services = data.services.split('||');
                let serviceData = services.map(function(dataItem) {
                    return {
                        "lblService": dataItem.trim(),
                        "imgBullet": viewConstants.IMAGES.PAGEOFFDOT
                    }
                });
                this.view.segBranchService.widgetDataMap = {
                    "lblService": "lblService",
                    "imgBulltet": "imgBullet"
                };
                this.view.segBranchService.setData(serviceData);
            }
            if (data.workingHours) {
                let workingHours = data.workingHours.split("||");
                let dateData = workingHours.map(function(dataItem) {
                    return {
                        "lblTimings": dataItem.replace(/\?/g, " to ")
                    };
                });
                this.view.segDayAndTime.widgetDataMap = {
                    "lblTimings": "lblTimings"
                };
                this.view.segDayAndTime.setData(dateData);
            } else {
                this.view.segDayAndTime.setData([]);
            }
        },

        /**
         * failure callback of getBranchOrATMList
         */
        getBranchOrATMListErrorCallback: function() {
            this.noSearchResultUI();
        },

        noSearchResultUI: function() {
            this.showNoMapPins();
            this.view.segLoacteUsSearchResults.setVisibility(false);
            this.view.flxNoSearchResult.setVisibility(true);
            this.view.lblInfoErrorMessage.text = kony.i18n.getLocalizedString("i18n.LocateUs.NosearchresultfoundPleasechangethesearchcriteria");
            this.view.forceLayout();
        },

        /**
         * it shows the single pin which represents current location
         */
        showNoMapPins: function() {
            var scopeObj = this;
            if (!this.globalMapLat) {
                var presenter = this.getLocateUsPresentationController();
                this.globalMapLat = presenter.globalLat;
                this.globalMapLong = presenter.globalLon;
            }
            var locationData = {
                lat: scopeObj.globalMapLat,
                lon: scopeObj.globalMapLong,
                showCallout: false
            };
            var data = [];
            data.push(locationData);
            this.bindMapData(data);
        },
        /**
         * success callback of getBrachOrATMList
         */
        getBranchOrATMListSuccessCallback: function(data) {
            this.searchResultData = data;
            this.updateSearchSegmentResult(data);
            var presenter = this.getLocateUsPresentationController();
            this.globalMapLat = presenter.globalLat;
            this.globalMapLong = presenter.globalLon;
            this.filterSearchData();
        },

        filterSearchData: function() {
            var scopeObj = this;
            var data = scopeObj.searchResultData;
            var filterData = scopeObj.filterDataByView(data);
            data = scopeObj.filterDataByServices(filterData);
            if (data)
                scopeObj.updateSearchSegmentResult(data);
            else
                scopeObj.noSearchResultUI();
        },
        filterDataByView: function(data) {
            return data.filter(function(row) {
                if (row.type === "BRANCH")
                    return true;
                else
                    return false;
            });
        },
        filterDataByServices: function(data) {
            var scopeObj = this;
            var filter = scopeObj.selectedServicesList;
            if (filter.length > 0) {
                //TO DO-filter data
                //As of now no criteria for how to filter
                //if any of the filter is selected-Showing no data is found
                return null
            }
            return data;
        },

        /**
         * update the seach data in segment
         * @param {Object} locationListViewModel
         */
        updateSearchSegmentResult: function(locationListViewModel) {
            var scopeObj = this;
            if (locationListViewModel.length === 0 || locationListViewModel[0].latitude === null) {
                scopeObj.noSearchResultUI();
            } else {
                var data = locationListViewModel.map(function(dataItem) {
                    return {
                        "lat": dataItem.latitude,
                        "lon": dataItem.longitude,
                        "locationId": dataItem.locationId,
                        "showCallout": true,
                        "image": dataItem.type === "BRANCH" ? viewConstants.IMAGES.BANK_ICON_BLUE : viewConstants.IMAGES.ATM_ICON_BLUE,
                        "calloutData": {
                            "lblBranchName": dataItem.informationTitle,
                            "lblBranchAddressOneLine": dataItem.addressLine1,
                            "lblBranchAddress2": dataItem.addressLine2,
                            "lblOpen": {
                                "text": "Open",
                                "isVisible": dataItem.status === "OPEN" ? true : false
                            },
                            "lblClosed": {
                                "text": "Closed",
                                "isVisible": dataItem.status === "OPEN" ? false : true
                            },
                            "imgGo": {
                                "text": "Q"
                            },
                            "flxLocationDetails": {
                                "onClick": function() {
                                    scopeObj.view.mapLocateUs.dismissCallout();
                                    scopeObj.onRowClickSegment(dataItem);
                                    scopeObj.pinToSegment(dataItem);
                                    scopeObj.showBranchDetails();
                                }
                            }
                        },
                        "imgBuildingType": dataItem.type === "BRANCH" ? viewConstants.IMAGES.BANK_ICON : viewConstants.IMAGES.TRANSACTION_TYPE_WITHDRAWL,
                        "imgGo": {
                            "text": "Q"
                        },
                        "imgIndicator": {
                            "src": viewConstants.IMAGES.ACCOUNTS_SIDEBAR_BLUE,
                            "isVisible": false,
                        },
                        "lblAddress": dataItem.addressLine2,
                        "lblClosed": {
                            "text": "Closed",
                            "isVisible": dataItem.status === "OPEN" ? false : true
                        },
                        "lblName": dataItem.informationTitle,
                        "lblOpen": {
                            "text": "Open",
                            "isVisible": dataItem.status === "OPEN" ? true : false
                        },
                        "lblSeperator": " ",
                        "flxSearchResults": {
                            "skin": viewConstants.SKINS.SKNFLXFFFFFFBORDER0,
                            "hoverSkin": viewConstants.SKINS.SKNSEGF9F9F9HOVER
                        },
                        "flxIndicator": {
                            "skin": viewConstants.SKINS.BLANK_SKIN_FLEX,
                        },
                        "template": "flxSearchResults",
                    };
                });
                this.bindSearchSegmentData(data);
                this.bindMapData(data);
                this.view.segLoacteUsSearchResults.setVisibility(true);
                this.view.flxNoSearchResult.setVisibility(false);
            }
        },

        /**
         * Binds the search data to the segment
         */
        bindSearchSegmentData: function(data) {
            var segmentDataMap = {
                "flxDetails": "flxDetails",
                "flxDistanceAndGo": "flxDistanceAndGo",
                "flxIndicator": "flxIndicator",
                "flxSearchResults": "flxSearchResults",
                "imgBuildingType": "imgBuildingType",
                "imgGo": "imgGo",
                "imgIndicator": "imgIndicator",
                "lblAddress": "lblAddress",
                "lblClosed": "lblClosed",
                "lblName": "lblName",
                "lblOpen": "lblOpen",
                "lblSeperator": "lblSeperator"
            };
            this.view.segLoacteUsSearchResults.widgetDataMap = segmentDataMap;
            this.view.segLoacteUsSearchResults.setData(data);
        },

        /**
         * it bind the data to the map
         */
        bindMapData: function(data) {
            var scopeObj = this;
            var mapData = data;
            if (mapData.length > 1) {
                mapData = JSON.parse(JSON.stringify(data));
                mapData.map(function(dataItem) {
                    dataItem.calloutData.flxLocationDetails.onClick = function() {
                        scopeObj.view.mapLocateUs.dismissCallout();
                        scopeObj.onRowClickSegment(dataItem);
                        scopeObj.showBranchDetails();
                        scopeObj.pinToSegment(dataItem);
                    };
                    return dataItem;
                });
                mapData.push(this.getCurrentLocationData());
            }
            this.view.mapLocateUs.calloutTemplate = "flxDistanceDetails";
            this.view.mapLocateUs.zoomLevel = 15;
            this.view.mapLocateUs.widgetDataMapForCallout = {
                "lblBranchName": "lblBranchName",
                "lblBranchAddressOneLine": "lblBranchAddressOneLine",
                "lblBranchAddress2": "lblBranchAddress2",
                "lblOpen": "lblOpen",
                "lblClosed": "lblClosed",
                "imgGo": "imgGo",
                "flxLocationDetails": "flxLocationDetails",
            };
            this.view.mapLocateUs.locationData = mapData;
            this.gotoSearchLocation();
        },

        gotoSearchLocation: function() {
            if (this.searchResultData && this.searchResultData.length > 0) {
                this.view.mapLocateUs.zoomLevel = 15;
                this.view.mapLocateUs.navigateToLocation({
                    lat: this.searchResultData[0].latitude,
                    lon: this.searchResultData[0].longitude
                }, false, false);
                this.view.forceLayout();
            }
        },

        /**
         * it returns the current location data
         */
        getCurrentLocationData: function() {
            return {
                "lat": this.globalMapLat,
                "lon": this.globalMapLong,
                "locationId": "",
                "showCallout": false,
                "calloutData": {
                    "lblBranchName": "",
                    "lblBranchAddressOneLine": "",
                    "lblBranchAddress2": "",
                    "lblOpen": "",
                    "lblClosed": "",
                    "imgGo": "",
                    "flxLocationDetails": {
                        "onClick": function() {},
                    },
                },
                "imgBuildingType": viewConstants.IMAGES.BANK_ICON,
                "imgGo": "",
                "imgIndicator": {
                    "src": viewConstants.IMAGES.ACCOUNTS_SIDEBAR_BLUE,
                    "isVisible": false,
                },
                "lblAddress": "",
                "lblClosed": "",
                "lblName": "",
                "lblOpen": "",
                "lblSeperator": " ",
                "flxSearchResults": {
                    "skin": viewConstants.SKINS.SKNFLEXF9F9F9,
                    "hoverSkin": viewConstants.SKINS.SKNSEGF9F9F9HOVER
                },
                "template": "flxSearchResults",
            };
        },

        /**
         * Shows prelogin view of the form
         */
        showPreLoginView: function() {
            this.view.customheadernew.showPreLoginView();
        },

        /**
         * Shows postLogin view of the form
         */
        showPostLoginView: function() {
            this.view.customheadernew.showPostLoginView();
        },

        /**
         * to navigate the map to current location
         */
        gotoCurrentLocation: function() {
            this.view.mapLocateUs.zoomLevel = 15;
            var presenter = this.getLocateUsPresentationController();
            this.view.mapLocateUs.navigateToLocation({
                lat: presenter.globalLat,
                lon: presenter.globalLon
            }, false, false);
        },

        /**
         * this is helper function to get the presentationcontroller of Locate Us
         */
        getLocateUsPresentationController: function() {
            var locateUsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("LocateUsModuleNew");
            if (locateUsModule) {
                return locateUsModule.presentationController;
            } else {
                return null;
            }
        },

        /**
         * shows the filters
         */
        viewFilters: function(visible) {
            let scopeObj = this;
            return function() {
                scopeObj.view.flxFilters.setVisibility(visible);
                scopeObj.view.flxMapOverlay.setVisibility(visible);
                scopeObj.setViewsAndFilterSegmentData();
            }
        },

        /**
         * On row click handler of segLoacteUsSearchResults
         * @param {JSON} obj : Object containing data of clicked row
         */
        onRowClickSegment: function(obj) {
            this.view.flxBranchDetails.setVisibility(true);
            var params = {
                "type": "details",
                "placeID": obj.locationId
            };
            this.getLocateUsPresentationController().getAtmorBranchDetails(params, responsiveUtils.isMobile || kony.application.getCurrentBreakpoint() == 640);
        },

        /**
         * Removes selected filter
         * @param {String} filterName 
         */
        removeFilter: function(filterName) {
            // let data = this.searchResultData;
            // this.view.segLoacteUsSearchResults.setData(data);

            let segData = this.view.segFilters.data;
            for (let i = 0; i < segData.length; i++) {
                if (segData[i]["lblOption"] === filterName.trim()) {
                    segData[i]["lblCheckbox"].text = viewConstants.FONT_ICONS.CHECBOX_UNSELECTED;
                }
            }
            this.segSetFilters();
            this.filterSearchData();
        },

        /**
         * Toggles skins of map and list view buttons in mobile
         */
        toggleListMap: function(key) {
            if (key === "list") {
                this.view.flxButtonsWrapper.skin = "sknflxGradient0273e3ffffff";
                this.view.lblListViewIcon.skin = viewConstants.SKINS.FONT_ICON_WHITE;
                this.view.lblListView.skin = viewConstants.SKINS.FEEDBACK_TOP_MENU_BLANK;
                this.view.lblMapView.skin = "sknLabelSSP0273e315px";
                this.view.lblMapViewIcon.skin = viewConstants.SKINS.FONT_ICON_BLUE;
            } else {
                this.view.flxButtonsWrapper.skin = "sknflxGradientffffff0273e3";
                this.view.lblListViewIcon.skin = viewConstants.SKINS.FONT_ICON_BLUE;
                this.view.lblListView.skin = "sknLabelSSP0273e315px";
                this.view.lblMapView.skin = viewConstants.SKINS.FEEDBACK_TOP_MENU_BLANK;
                this.view.lblMapViewIcon.skin = viewConstants.SKINS.FONT_ICON_WHITE;
            }
        },

        /**
         * Init lifecycle function
         */
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            var scopeObj = this;
            this.view.onBreakpointChange = function() {
                scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint())
            };
            this.view.flxCloseWarning.onClick = function() {
                scopeObj.view.flxWarning.setVisibility(false);
            };

            this.view.btnATMAll.onClick = function() {
                scopeObj.getLocateUsPresentationController().showLocateUsPage("ATMTab")
            };

            this.view.btnAllTab.onClick = function() {
                scopeObj.getLocateUsPresentationController().showLocateUsPage("AllTab")
            };

            this.view.imgMyLocation.onTouchEnd = function() {
                scopeObj.gotoCurrentLocation();
            };
            this.view.flxZoomIn.onClick = function() {
                var zoom = scopeObj.view.mapLocateUs.zoomLevel;
                scopeObj.view.mapLocateUs.zoomLevel = --zoom;
            };
            this.view.flxZoomOut.onClick = function() {
                var zoom = scopeObj.view.mapLocateUs.zoomLevel;
                scopeObj.view.mapLocateUs.zoomLevel = ++zoom;
            };
            this.view.flxFiltersIcon.onClick = this.viewFilters(true);
            this.view.flxCloseFilter.onClick = this.viewFilters(false);
            this.view.btnCancelFilters.onClick = this.viewFilters(false);
            this.view.segLoacteUsSearchResults.onRowClick = function(obj) {
                scopeObj.onRowClickSegment(obj.selectedRowItems[0]);
                scopeObj.showBranchDetails();
                scopeObj.changeResultsRowSkin();
            };
            this.view.btnBackToMap.onClick = function() {
                scopeObj.view.flxBranchDetails.setVisibility(false);
            };
            this.view.btnApplyFilters.onClick = this.applyFilters;
            this.view.flxCancelFilter1.onClick = function() {
                scopeObj.removeFilter(scopeObj.view.lblFilterName1.text);
            };
            this.view.flxCancelFilter2.onClick = function() {
                scopeObj.removeFilter(scopeObj.view.lblFilterName2.text);
            };
            this.view.flxCancelFilter3.onClick = function() {
                scopeObj.removeFilter(scopeObj.view.lblFilterName3.text);
            };
            this.view.flxCancelFilter4.onClick = function() {
                scopeObj.removeFilter(scopeObj.view.lblFilterName4.text);
            };
            this.view.flxCancelFilter5.onClick = function() {
                scopeObj.removeFilter(scopeObj.view.lblFilterName5.text);
            };
            this.view.btnClearAll.onClick = function() {
                scopeObj.searchTextClearAndRefresh();
                scopeObj.segSetFilters();
                scopeObj.selectedServicesList = [];
                scopeObj.filterSearchData();
                scopeObj.view.flxFilterWrapper.setVisibility(false);
            }
            this.view.flxSearchIcon.onClick = function() {
                scopeObj.performSearch();
            };
            this.view.flxShowMapView.onClick = function() {
                scopeObj.view.flxRightContent.setVisibility(true);
                scopeObj.toggleListMap("map");
            }
            this.view.flxShowListView.onClick = function() {
                scopeObj.view.flxRightContent.setVisibility(false);
                scopeObj.toggleListMap("list");
            }
            this.view.tbxSearch.onKeyUp = function() {
                scopeObj.view.flxClearSearch.setVisibility(scopeObj.view.tbxSearch.text.length > 0);
                scopeObj.view.forceLayout();
            };
            this.view.flxClearSearch.onTouchEnd = function() {
                scopeObj.view.tbxSearch.text = "";
                scopeObj.view.flxClearSearch.setVisibility(false);
                scopeObj.getLocateUsPresentationController().getBranchOrATMList();
            };
        },

        /**
         * PreShow lifecycle event
         */
        preShow: function() {
            this.view.customheadernew.activateMenu("About Us", "Locate Us");
        },

        /**
         * PostShow lifecycle event
         */
        postShow: function() {
            this.getLocateUsPresentationController().getBranchOrATMList();
            this.selectedServicesList = [];
            this.setViewsAndFilterSegmentData();
            applicationManager.getNavigationManager().applyUpdates(this);
        },

        /**
         * onBreakpoint change handler
         */
        onBreakpointChange: function(width) {
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
            if (responsiveUtils.isMobile || kony.application.getCurrentBreakpoint() == 640) {
                let tabHeight = kony.os.deviceInfo().screenHeight - parseInt(this.view.flxHeader.height.slice(0, -2));
                this.view.flxTabContent.height = tabHeight + "dp";
                this.view.flxMapAndListButtons.bottom = "0dp";
                this.view.segLoacteUsSearchResults.height = tabHeight - 140 + "dp";
                this.view.flxLeftContent.height = tabHeight - 50 + "dp"
            } else {
                this.view.flxLeftContent.height = "600dp"
                this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
                this.view.flxTabContent.height = "650dp";
                this.view.segLoacteUsSearchResults.height = "510dp";
            }
            this.view.forceLayout();
        }
    }

});