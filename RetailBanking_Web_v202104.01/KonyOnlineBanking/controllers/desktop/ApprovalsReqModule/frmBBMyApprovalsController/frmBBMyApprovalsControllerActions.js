define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onDeviceBack defined for frmBBMyApprovals **/
    AS_Form_d03f24dcd90f42008a83858765af4431: function AS_Form_d03f24dcd90f42008a83858765af4431(eventobject) {
        var self = this;
        kony.print("Back Button Pressed");
    },
    /** postShow defined for frmBBMyApprovals **/
    AS_Form_d81a01d597374bd793d281e4f6cb1c31: function AS_Form_d81a01d597374bd793d281e4f6cb1c31(eventobject) {
        var self = this;
        applicationManager.getNavigationManager().applyUpdates(this);
        this.adjustScreen(0);
    },
    /** onTouchEnd defined for frmBBMyApprovals **/
    AS_Form_f18198ec20184dd9ba83026759fe8fde: function AS_Form_f18198ec20184dd9ba83026759fe8fde(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** preShow defined for frmBBMyApprovals **/
    AS_Form_f54c13358f204b24b7b8c374c84ae096: function AS_Form_f54c13358f204b24b7b8c374c84ae096(eventobject) {
        var self = this;
        this.onPreShow();
    },
    /** init defined for frmBBMyApprovals **/
    AS_Form_ffbcc144c4f445289d92affc86c635bc: function AS_Form_ffbcc144c4f445289d92affc86c635bc(eventobject) {
        var self = this;
        this.onInit();
    }
});