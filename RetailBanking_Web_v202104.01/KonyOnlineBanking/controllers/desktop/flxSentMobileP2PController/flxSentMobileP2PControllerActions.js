define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnModify **/
    AS_Button_e703f7afbd904fc7ad1aea23f9a2f344: function AS_Button_e703f7afbd904fc7ad1aea23f9a2f344(eventobject, context) {
        var self = this;
        this.showSendMoney();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_d0a3b807b9be4e9c99a2946204b330f0: function AS_FlexContainer_d0a3b807b9be4e9c99a2946204b330f0(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});