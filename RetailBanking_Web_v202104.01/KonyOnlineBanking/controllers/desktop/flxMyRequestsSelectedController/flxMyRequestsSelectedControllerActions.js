define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnReminder **/
    AS_Button_dd831c6802fd4329b57882908fd96ff5: function AS_Button_dd831c6802fd4329b57882908fd96ff5(eventobject, context) {
        var self = this;
        this.showSendReminder();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_i70047e9b8af4650bd919dc4031d7621: function AS_FlexContainer_i70047e9b8af4650bd919dc4031d7621(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});