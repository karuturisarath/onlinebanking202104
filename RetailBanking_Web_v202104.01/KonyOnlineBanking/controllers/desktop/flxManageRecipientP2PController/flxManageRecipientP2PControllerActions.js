define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnDelete **/
    AS_Button_cfccea8cb45a4c05a0bc3d22aa6381fe: function AS_Button_cfccea8cb45a4c05a0bc3d22aa6381fe(eventobject, context) {
        var self = this;
        //adding this comment due to platform bug.
    },
    /** onClick defined for btnEdit **/
    AS_Button_e259283aa31f459c8b76dd5939460776: function AS_Button_e259283aa31f459c8b76dd5939460776(eventobject, context) {
        var self = this;
        //adding this comment due to platform bug.
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_f0cdd76c878b4efba032f70d9f5ca511: function AS_FlexContainer_f0cdd76c878b4efba032f70d9f5ca511(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});