define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onDeviceBack defined for frmViewApprovalMatrix **/
    AS_Form_a7761342fc774c139eaf59293027526e: function AS_Form_a7761342fc774c139eaf59293027526e(eventobject) {
        var self = this;
        this.doLogout("Logout");
    },
    /** postShow defined for frmViewApprovalMatrix **/
    AS_Form_bdfb70e405454fe69ec4e5c485d260a1: function AS_Form_bdfb70e405454fe69ec4e5c485d260a1(eventobject) {
        var self = this;
        return self.frmPostShow.call(this);
    },
    /** init defined for frmViewApprovalMatrix **/
    AS_Form_c1265919a0cc4ff5b63cafba3a4ee5da: function AS_Form_c1265919a0cc4ff5b63cafba3a4ee5da(eventobject) {
        var self = this;
        return self.doInit.call(this);
    },
    /** preShow defined for frmViewApprovalMatrix **/
    AS_Form_fc7615d24a224b289b804c51d22a6957: function AS_Form_fc7615d24a224b289b804c51d22a6957(eventobject) {
        var self = this;
        return self.frmPreShow.call(this);
    }
});