define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onTouchStart defined for calSendOn **/
    AS_Calendar_dd8fed32590d47d68c898e15132436a3: function AS_Calendar_dd8fed32590d47d68c898e15132436a3(eventobject, x, y, context) {
        var self = this;
        this.position(eventobject);
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_c88e895a86ea472295423e6b33e2aac3: function AS_FlexContainer_c88e895a86ea472295423e6b33e2aac3(eventobject, context) {
        var self = this;
        this.showUnselectedRow();
    }
});