define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for flxRemoveAttachment **/
    AS_FlexContainer_e0044c0bd34e4ce880f2bdce2bcacfe7: function AS_FlexContainer_e0044c0bd34e4ce880f2bdce2bcacfe7(eventobject, context) {
        var self = this;
        return self.removeClickedInTransfers.call(this);
    },
    /** onTouchEnd defined for lblAttachedDocument **/
    AS_Label_f78e7540ef2941308e5d88962c824b0c: function AS_Label_f78e7540ef2941308e5d88962c824b0c(eventobject, x, y, context) {
        var self = this;
        return self.showDownloadPopup.call(this);
    }
});