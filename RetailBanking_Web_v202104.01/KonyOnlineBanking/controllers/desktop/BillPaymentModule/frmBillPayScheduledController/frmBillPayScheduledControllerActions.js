define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnNo **/
    AS_Button_cbee0bd746ff4ee288d2dce411c8c467: function AS_Button_cbee0bd746ff4ee288d2dce411c8c467(eventobject) {
        var self = this;
        this.hideCancelPopup();
    },
    /** onClick defined for btnYes **/
    AS_Button_iae5b1281c13440ab2a25f8876a30c89: function AS_Button_iae5b1281c13440ab2a25f8876a30c89(eventobject) {
        var self = this;
        var obj1 = {
            "tabname": "payees"
        };
        var navObj = new kony.mvc.Navigation("frmBillPay");
        navObj.navigate(obj1);
    },
    /** onClick defined for flxCross **/
    AS_FlexContainer_be5eeb309f5c4587b6f7f712ac282909: function AS_FlexContainer_be5eeb309f5c4587b6f7f712ac282909(eventobject) {
        var self = this;
        this.hideCancelPopup();
    },
    /** init defined for frmBillPayScheduled **/
    AS_Form_gec72a3a939e468f85d4f97004d6de3f: function AS_Form_gec72a3a939e468f85d4f97004d6de3f(eventobject) {
        var self = this;
        this.init();
    }
});