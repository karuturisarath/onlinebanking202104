define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnAction **/
    AS_Button_hd550aad15ba41d9a2225e61d4681d23: function AS_Button_hd550aad15ba41d9a2225e61d4681d23(eventobject, context) {
        var self = this;
        this.executeOnParent("viewTransactionReport");
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_dddb1baf6d3b425d8fc41bfcaf9ea87f: function AS_FlexContainer_dddb1baf6d3b425d8fc41bfcaf9ea87f(eventobject, context) {
        var self = this;
        this.showUnselectedRow();
    }
});