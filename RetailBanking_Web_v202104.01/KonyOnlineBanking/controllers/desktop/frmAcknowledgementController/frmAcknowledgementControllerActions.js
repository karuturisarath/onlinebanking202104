define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** init defined for frmAcknowledgement **/
    AS_Form_ja2a733aa3444cedb63684f03f21b757: function AS_Form_ja2a733aa3444cedb63684f03f21b757(eventobject) {
        var self = this;
        this.initActions();
    },
    /** postShow defined for frmAcknowledgement **/
    AS_Form_a073e73befc14f33bde760ef659e0348: function AS_Form_a073e73befc14f33bde760ef659e0348(eventobject) {
        var self = this;
        this.postShowFrmAcknowledgement();
        if (this.presenter && this.presenter.loadHamburgerTransfers) {
            this.presenter.loadHamburgerTransfers("frmAcknowledgement");
        }
        if (this.presenter && this.presenter.loadHamburgerBillPay) {
            this.presenter.loadHamburgerBillPay("frmAcknowledgement");
        }
    },
    /** onDeviceBack defined for frmAcknowledgement **/
    AS_Form_ca5e7672bb1c460c813efe0e09aa2013: function AS_Form_ca5e7672bb1c460c813efe0e09aa2013(eventobject) {
        var self = this;
        //Have to Consolidate
        kony.print("Back Button Pressed");
    },
    /** onTouchEnd defined for frmAcknowledgement **/
    AS_Form_iba601c0c9e84c68bc9bc1a202118d14: function AS_Form_iba601c0c9e84c68bc9bc1a202118d14(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});