define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onDeviceBack defined for frmPayaPersonAddBeneficiary **/
    AS_Form_eb965238baa14786bda5862ad948720e: function AS_Form_eb965238baa14786bda5862ad948720e(eventobject) {
        var self = this;
        kony.print("Back Button Pressed");
    },
    /** onBreakpointChange defined for frmPayaPersonAddBeneficiary **/
    AS_Form_f4c84352f2e2480d8cee18608607d7d0: function AS_Form_f4c84352f2e2480d8cee18608607d7d0(eventobject, breakpoint) {
        var self = this;
        return self.preShow.call(this);
    },
    /** postShow defined for frmPayaPersonAddBeneficiary **/
    AS_Form_f6b1221b53ac42149376293ec70c347c: function AS_Form_f6b1221b53ac42149376293ec70c347c(eventobject) {
        var self = this;
        return self.postShow.call(this);
    },
    /** preShow defined for frmPayaPersonAddBeneficiary **/
    AS_Form_ge940f461aa74ccd855f1c5006c434a7: function AS_Form_ge940f461aa74ccd855f1c5006c434a7(eventobject) {
        var self = this;
        return self.preShow.call(this);
    }
});