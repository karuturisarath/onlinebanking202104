define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onBreakpointChange defined for frmPayaPersonAddBeneficiaryConfirm **/
    AS_Form_cf72b1461e8647f99627671294ce7e6f: function AS_Form_cf72b1461e8647f99627671294ce7e6f(eventobject, breakpoint) {
        var self = this;
        return self.onBreakPointChange.call(this);
    },
    /** onDeviceBack defined for frmPayaPersonAddBeneficiaryConfirm **/
    AS_Form_dcbcd84a59fc4c978e4034d392b25285: function AS_Form_dcbcd84a59fc4c978e4034d392b25285(eventobject) {
        var self = this;
        kony.print("Back Button Pressed");
    },
    /** preShow defined for frmPayaPersonAddBeneficiaryConfirm **/
    AS_Form_i80e8802e8b247ba95fe527adb74c7b0: function AS_Form_i80e8802e8b247ba95fe527adb74c7b0(eventobject) {
        var self = this;
        return self.preShow.call(this);
    }
});