define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxPermissionClickable **/
    AS_FlexContainer_iee29ae3ac234f3eb3fe6ad418e5f55f: function AS_FlexContainer_iee29ae3ac234f3eb3fe6ad418e5f55f(eventobject, context) {
        var self = this;
        this.selectOrUnselectEntireFeature(eventobject, context);
    },
    /** onTouchStart defined for flxTooltip **/
    AS_FlexContainer_efc6c1aabcfb4b4c97204753f57fa48b: function AS_FlexContainer_efc6c1aabcfb4b4c97204753f57fa48b(eventobject, x, y, context) {
        var self = this;
        this.onInfoTouchStart(eventobject, context);
    },
    /** onTouchEnd defined for flxTooltip **/
    AS_FlexContainer_af17794166da420e8eb514ccdd3df78e: function AS_FlexContainer_af17794166da420e8eb514ccdd3df78e(eventobject, x, y, context) {
        var self = this;
        this.onInfoTouchEnd(eventobject, context);
    },
    /** onClick defined for flxClickAction1 **/
    AS_FlexContainer_dbbc4929b5ce43978a5075182ed13e06: function AS_FlexContainer_dbbc4929b5ce43978a5075182ed13e06(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction2 **/
    AS_FlexContainer_d400222bc87b46a6bc194cf89c0632b7: function AS_FlexContainer_d400222bc87b46a6bc194cf89c0632b7(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction3 **/
    AS_FlexContainer_e463e1e113b940678f8d8d4e81665f89: function AS_FlexContainer_e463e1e113b940678f8d8d4e81665f89(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction4 **/
    AS_FlexContainer_g5183e28b9134417b8132df3334cba3d: function AS_FlexContainer_g5183e28b9134417b8132df3334cba3d(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction5 **/
    AS_FlexContainer_dd72a313ef2544eb861d33d76c34f24a: function AS_FlexContainer_dd72a313ef2544eb861d33d76c34f24a(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction6 **/
    AS_FlexContainer_bc0299c913804c56a09b1b09d58d66a1: function AS_FlexContainer_bc0299c913804c56a09b1b09d58d66a1(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction7 **/
    AS_FlexContainer_b59ff9412c874fa2beaa4fbd0d854910: function AS_FlexContainer_b59ff9412c874fa2beaa4fbd0d854910(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction8 **/
    AS_FlexContainer_hb94eaa8f9e3440d8d08f1422278767b: function AS_FlexContainer_hb94eaa8f9e3440d8d08f1422278767b(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction9 **/
    AS_FlexContainer_bceced93c9c342e4966c7df6a28abcb0: function AS_FlexContainer_bceced93c9c342e4966c7df6a28abcb0(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction10 **/
    AS_FlexContainer_ae20dd771ea6495381e9bd8571148e7e: function AS_FlexContainer_ae20dd771ea6495381e9bd8571148e7e(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction11 **/
    AS_FlexContainer_f91f085d18e14bd08a572564bd992cb3: function AS_FlexContainer_f91f085d18e14bd08a572564bd992cb3(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction12 **/
    AS_FlexContainer_e960c20507f04637b2038e5d641d5209: function AS_FlexContainer_e960c20507f04637b2038e5d641d5209(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction13 **/
    AS_FlexContainer_e0080fe854754da9b50056d8ac20de39: function AS_FlexContainer_e0080fe854754da9b50056d8ac20de39(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction14 **/
    AS_FlexContainer_c5a430dd0b364427ad34f9fd5a1f8207: function AS_FlexContainer_c5a430dd0b364427ad34f9fd5a1f8207(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction15 **/
    AS_FlexContainer_f1851386d3ff4a9ab28e2b2c9d2d13a4: function AS_FlexContainer_f1851386d3ff4a9ab28e2b2c9d2d13a4(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction16 **/
    AS_FlexContainer_f3d6f9a1c13f4a86a2d128327a1a8198: function AS_FlexContainer_f3d6f9a1c13f4a86a2d128327a1a8198(eventobject, context) {
        var self = this;
        return self.selectOrUnselectParentFeature.call(this, null, null);
    },
    /** onClick defined for flxClickAction17 **/
    AS_FlexContainer_fbaa2c1a4aaf4413b5c006a2450ddf6e: function AS_FlexContainer_fbaa2c1a4aaf4413b5c006a2450ddf6e(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction18 **/
    AS_FlexContainer_d4e5ba2d765c4d9a822a3084e0e2d3c9: function AS_FlexContainer_d4e5ba2d765c4d9a822a3084e0e2d3c9(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction19 **/
    AS_FlexContainer_a98d9127c67049c4bdacf1ff4ba66bdf: function AS_FlexContainer_a98d9127c67049c4bdacf1ff4ba66bdf(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction20 **/
    AS_FlexContainer_e95e637b5dc24e4d84f0a5cdc775ec64: function AS_FlexContainer_e95e637b5dc24e4d84f0a5cdc775ec64(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    }
});