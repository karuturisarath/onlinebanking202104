define({
    showUnselectedRow: function() {
        var index = kony.application.getCurrentForm().transactions.segTransactions.selectedIndex;
        var rowIndex = index[1];
        var data = kony.application.getCurrentForm().transactions.segTransactions.data;
        data[rowIndex].imgDropdown.src = "O";
        data[rowIndex].template = "flxCheckImageUnSelected";
        kony.application.getCurrentForm().transactions.segTransactions.setDataAt(data[rowIndex], rowIndex);
        this.AdjustScreen(-268);
    },
    showCheckImage: function() {
        var currForm = kony.application.getCurrentForm();
        currForm.flxCheckImage.setVisibility(true);
        this.AdjustScreen(30);
    },
    rememberCategory: function() {
        var index = kony.application.getCurrentForm().transactions.segTransactions.selectedIndex;
        var sectionIndex = index[0];
        var rowIndex = index[1];
        var data = kony.application.getCurrentForm().transactions.segTransactions.data;
        if (data[sectionIndex][1][rowIndex].imgRememberCategory.src == "unchecked_box.png") {
            data[sectionIndex][1][rowIndex].imgRememberCategory.src = "checked_box.png";
        } else {
            data[sectionIndex][1][rowIndex].imgRememberCategory.src = "unchecked_box.png";
        }
        kony.application.getCurrentForm().transactions.segTransactions.setDataAt(data[sectionIndex][1][rowIndex], rowIndex, sectionIndex);
        this.AdjustScreen(30);
    },
    AdjustScreen: function(data) {
        var currentForm = kony.application.getCurrentForm();
        var mainheight = 0;
        var screenheight = kony.os.deviceInfo().screenHeight;
        mainheight = currentForm.customheader.frame.height + currentForm.flxMainWrapper.frame.height;
        var diff = screenheight - mainheight;
        if (mainheight < screenheight) {
            diff = diff - currentForm.flxFooter.frame.height;
            if (diff > 0)
                currentForm.flxFooter.top = mainheight + diff + data + "dp";
            else
                currentForm.flxFooter.top = mainheight + data + "dp";
            currentForm.forceLayout();
        } else {
            currentForm.flxFooter.top = mainheight + data + "dp";
            currentForm.forceLayout();
        }
    },
});