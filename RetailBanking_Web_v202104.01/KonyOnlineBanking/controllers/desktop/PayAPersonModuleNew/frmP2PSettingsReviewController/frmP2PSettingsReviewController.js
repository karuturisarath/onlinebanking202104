define(['commonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(commonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();
    var userJSON = null;
    var paymentAccounts = null;
    return {
        /**
         * Update form UI
         */
        updateFormUI: function(viewPropertiesMap) {
            if (viewPropertiesMap.showProgressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewPropertiesMap.showProgressBar === false) {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (viewPropertiesMap.p2pSettings) {
                this.userJSON = viewPropertiesMap.userJSON;
                this.paymentAccounts = viewPropertiesMap.paymentAccounts;
                this.setPayeeData(this.userJSON, this.paymentAccounts);
            }
            if (viewPropertiesMap.inFormError) {
                this.view.rtxMessageWarning.text = viewPropertiesMap.inFormError.errorMessage;
                this.view.flxWarning.setVisibility(true);
            }
        },

        /**
         * Sets payee data in review screen
         * @param  {Object} userJSON - contains info like userfirstname, userLastName, phone and email.
         * @param {object} paymentAccounts - contians the list of transactions.
         */
        setPayeeData: function(userJSON, paymentAccounts) {
            var p2pAccounts = this.showAccountsForSelectionP2PSettings(paymentAccounts);
            var defaultToAccount = this.getMaskedAccountNameForID(p2pAccounts, kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.getDefaultP2PToAccount());
            var defaultFromAccount = this.getMaskedAccountNameForID(p2pAccounts, kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.getDefaultP2PFromAccount());
            this.view.lblYourNameValue.text = userJSON.userName;
            this.view.lblRegisteredPhoneValue.text = userJSON.phone ? userJSON.phone : kony.i18n.getLocalizedString("i18n.common.none");
            this.view.lblRegisteredEmailValue.text = userJSON.email ? userJSON.email : kony.i18n.getLocalizedString("i18n.common.none");
            this.view.lblDefaultToAccountValue.text = defaultToAccount;
            this.view.lblDefaultFromAccountValue.text = defaultFromAccount;
        },

        /**
         * This method is used to get the payemnt accounts to show in pay a person settings page.
         * @param {Object} presentAccounts - contains the list of accounts.
         */
        showAccountsForSelectionP2PSettings: function(presentAccounts) {
            var list = [];
            for (var i = 0; i < presentAccounts.length; i++) {
                var tempList = [];
                tempList.push(presentAccounts[i].accountID);
                var tempAccountNumber = presentAccounts[i].accountID;
                tempList.push(presentAccounts[i].accountName + " ..." + tempAccountNumber.slice(-4));
                list.push(tempList);
            }
            return list;
        },

        /**
         * This method is used to get the maskedAccount Name.
         * @param {Objects} accounts - contains the list of accounts.
         * @param {Number} accountID - contains the accountID.
         */
        getMaskedAccountNameForID: function(accounts, accountID) {
            var paymentAccount;
            for (var index in accounts) {
                if (accounts[index][0] === accountID) {
                    paymentAccount = accounts[index][1];
                    break;
                }
            }
            return paymentAccount;
        },

        /**
         * Toggles notify me checkbox
         */
        notifyMeCheckToggle: function() {
            if (this.view.imgIAgree.src === ViewConstants.IMAGES.UNCHECKED_IMAGE)
                this.view.imgIAgree.src = ViewConstants.IMAGES.CHECKED_IMAGE
            else
                this.view.imgIAgree.src = ViewConstants.IMAGES.UNCHECKED_IMAGE
        },

        /**
         * Toggle info popup of default account for sending
         */
        infoDefaultAccForToggle: function() {
            this.view.AllForms.setVisibility(!this.view.AllForms.isVisible);
        },

        /** 
         * Init lifecycle function
         */
        init: function() {
            let scopeObj = this;
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.view.flxCloseWarning.onClick = function() {
                scopeObj.view.flxWarning.setVisibility(false);
            };

            this.view.btnBack.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showSendMoneyTabView();
            };
            this.view.flxIAgree.onClick = this.notifyMeCheckToggle;
            this.view.flxInfoDefaultSending.onClick = this.infoDefaultAccForToggle;
            this.view.btnEdit.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.editP2PSettingsScreen(scopeObj.userJSON, scopeObj.paymentAccounts);
            }
        },

        /**
         * PreShow lifecycle event
         */
        preShow: function() {
            // @todoP2P
            // Change the active menu for P2P  
            this.view.customheadernew.activateMenu("FASTTRANSFERS", "Transfer Money");
        },

        /**
         * PostShow lifecycle event
         */
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
            applicationManager.getNavigationManager().applyUpdates(this);
            this.resetUI();
        },

        /**
         * Resets the UI
         */
        resetUI: function() {
            this.view.imgIAgree.src = ViewConstants.IMAGES.UNCHECKED_IMAGE;
            this.view.AllForms.setVisibility(false);
        },

        /**
         * onBreakpoint change handler
         */
        onBreakpointChange: function(ref, width) {
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
        }
    }
});