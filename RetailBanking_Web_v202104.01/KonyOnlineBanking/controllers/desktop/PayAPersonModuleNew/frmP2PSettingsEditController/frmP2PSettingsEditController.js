define(['commonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(commonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();
    return {
        /**
         * Update form UI
         */
        updateFormUI: function(viewPropertiesMap) {
            if (viewPropertiesMap.showProgressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewPropertiesMap.showProgressBar === false) {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (viewPropertiesMap.p2pSettingsEdit) {
                this.setFormData(viewPropertiesMap.userJSON, viewPropertiesMap.paymentAccounts, "updateP2PPreferencesForUser");
            }
            if (viewPropertiesMap.activateP2P) {
                this.setFormData(viewPropertiesMap.userJSON, viewPropertiesMap.paymentAccounts, "ActivateP2P");
            }
            if (viewPropertiesMap.inFormError) {
                this.view.rtxMessageWarning.text = viewPropertiesMap.inFormError.errorMessage;
                this.view.flxWarning.setVisibility(true);
            }
        },

        /**
         * Sets data for editing
         * @param {JSON} userJSON - payee information
         * @param {JSON} paymentAccounts - payee accounts
         */
        setFormData: function(userJSON, paymentAccounts, mode) {
            let self = this;
            this.view.tbxName.text = userJSON.userName;
            this.view.tbxName.setEnabled(false);
            this.view.tbxRegisteredPhone.text = userJSON.phone;
            this.view.tbxRegisteredPhone.setEnabled(false);
            this.view.tbxRegisteredEmail.text = userJSON.email;
            this.view.tbxRegisteredEmail.setEnabled(false);
            this.view.lbxDefaultAccountDeposit.masterData = this.showAccountsForSelectionP2PSettings(paymentAccounts);
            this.view.lbxDefaultAccountSeding.masterData = this.showAccountsForSelectionP2PSettings(paymentAccounts);
            if (commonUtilities.isCSRMode()) {
                this.view.btnCreateNewAccount.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.btnCreateNewAccount.skin = CommonUtilities.disableButtonSkinForCSRMode();
                this.view.btnCreateNewAccount.hoverSkin = CommonUtilities.disableButtonSkinForCSRMode();
                this.view.btnCreateNewAccount.focusSkin = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                this.view.btnConfirm.onClick = function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController[mode]({
                        "defaultFromAccount": self.view.lbxDefaultAccountSeding.selectedKey,
                        "defaultToAccount": self.view.lbxDefaultAccountDeposit.selectedKey,
                    });
                };
            }
            if (mode === "updateP2PPreferencesForUser") {
                this.view.lblHeading.text = kony.i18n.getLocalizedString("i18n.p2p.PayAPerson");
                this.view.btnCancel.onClick = function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showSendMoneyTabView();
                };
            } else {
                this.view.lblHeading.text = kony.i18n.getLocalizedString("i18n.PayAPerson.ActivateP2PPaymentService");
                this.view.btnCancel.onClick = function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('AccountsModule').presentationController.showAccountsDashboard();
                };
            }
        },

        /**
         * This method is used to get the payemnt accounts to show in pay a person settings page.
         * @param {Object} presentAccounts - contains the list of accounts.
         */
        showAccountsForSelectionP2PSettings: function(presentAccounts) {
            var list = [];
            for (var i = 0; i < presentAccounts.length; i++) {
                var tempList = [];
                tempList.push(presentAccounts[i].accountID);
                var tempAccountNumber = presentAccounts[i].accountID;
                tempList.push(presentAccounts[i].accountName + " ..." + tempAccountNumber.slice(-4));
                list.push(tempList);
            }
            return list;
        },

        /**
         * Toggles notify me checkbox
         */
        notifyMeCheckToggle: function() {
            if (this.view.imgIAgree.src === ViewConstants.IMAGES.UNCHECKED_IMAGE)
                this.view.imgIAgree.src = ViewConstants.IMAGES.CHECKED_IMAGE
            else
                this.view.imgIAgree.src = ViewConstants.IMAGES.UNCHECKED_IMAGE
        },

        /**
         * Toggle info popup of default account for sending
         */
        infoDefaultAccForToggle: function() {
            this.view.AllForms.setVisibility(!this.view.AllForms.isVisible);
        },

        /** 
         * Init lifecycle function
         */
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            var scopeObj = this;
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.view.flxCloseWarning.onClick = function() {
                scopeObj.view.flxWarning.setVisibility(false);
            };
            this.view.flxIAgree.onClick = this.notifyMeCheckToggle;
            this.view.flxInfoDefaultSending.onClick = this.infoDefaultAccForToggle;
            this.view.btnCancel.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showSendMoneyTabView();
            };
        },

        /**
         * PreShow lifecycle event
         */
        preShow: function() {
            // @todoP2P
            // Change the active menu for P2P  
            this.view.customheadernew.activateMenu("FASTTRANSFERS", "Transfer Money");
        },

        /**
         * PostShow lifecycle event
         */
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
            applicationManager.getNavigationManager().applyUpdates(this);
            this.resetUI();
        },

        /**
         * Resets the UI
         */
        resetUI: function() {
            this.view.imgIAgree.src = ViewConstants.IMAGES.UNCHECKED_IMAGE;
            this.view.AllForms.setVisibility(false);
        },

        /**
         * onBreakpoint change handler
         */
        onBreakpointChange: function(ref, width) {
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
        }
    }
});