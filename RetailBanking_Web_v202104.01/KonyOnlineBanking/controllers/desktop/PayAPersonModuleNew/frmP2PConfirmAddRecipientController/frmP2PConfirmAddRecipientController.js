define(['commonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(commonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();
    var payPersonJSON = null;
    return {
        /**
         * Update form UI
         */
        updateFormUI: function(viewPropertiesMap) {
            if (viewPropertiesMap.showProgressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewPropertiesMap.showProgressBar === false) {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (viewPropertiesMap.addRecipientAcknowledgement) {
                this.payPersonJSON = viewPropertiesMap.addRecipientAcknowledgement.payPersonJSON;
                this.showAcknowledgement(viewPropertiesMap.addRecipientAcknowledgement.payPersonJSON, viewPropertiesMap.addRecipientAcknowledgement.result);
            }
            if (viewPropertiesMap.editRecipientAcknowledgement) {
                this.payPersonJSON = viewPropertiesMap.editRecipientAcknowledgement;
                this.showAcknowledgement(viewPropertiesMap.editRecipientAcknowledgement);
                this.showEditAcknowledgement();
            }
            if (viewPropertiesMap.deactivationAcknowledgement) {
                this.showDeactivationView(viewPropertiesMap.deactivationAcknowledgement);
            }
            if (viewPropertiesMap.inFormError) {
                this.view.rtxMessageWarning.text = viewPropertiesMap.inFormError.errorMessage;
                this.view.flxWarning.setVisibility(true);
            }
        },

        /**
         * Shows deactivated P2P view
         * @param {JSON} payeeData
         */
        showDeactivationView: function(payeeData) {
            let details = {};
            this.view.btnAddAnotherRecipient.text = kony.i18n.getLocalizedString("i18n.login.Done");
            this.view.btnSendMoney.setVisibility(false);
            this.view.btnAddAnotherRecipient.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('AccountsModule').presentationController.showAccountsDashboard();
            };
            details["i18n.PayPerson.defaultAccountForDeposit"] = payeeData["defaultAccountForDeposit"];
            details["i18n.WireTransfer.DefaultAccountForSending"] = payeeData["defaultAccountForSending"];
            details["i18n.PayPerson.phoneNumbers"] = payeeData["phone"];
            details["i18n.BillPay.BillerName"] = payeeData["name"];
            details["i18n.payaAPerson.emailAddress"] = payeeData["email"];
            this.view.keyValueList.setData(details);
        },

        /**
         * Shows edit recipient acknowledgement view
         */
        showEditAcknowledgement: function() {
            this.view.lblAckMessage.text = kony.i18n.getLocalizedString("i18n.PayAPerson.EditRecipientAckMessage");
            this.view.btnAddAnotherRecipient.text = kony.i18n.getLocalizedString("i18n.FastTransfers.BackToManageRecipient");
            this.view.btnAddAnotherRecipient.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showPayAPerson("ManageRecipients");
            }
        },

        /**
         * Sets acknowlegement data
         * @param {JSON} payPersonJSON - data of added payee
         * @param {JSON} result - added recipient details
         */
        showAcknowledgement: function(payPersonJSON, result) {
            let details = {};
            details["i18n.BillPay.BillerName"] = payPersonJSON["name"];
            details["i18n.PayPerson.nickName"] = payPersonJSON["nickName"];
            details["i18n.PayAPerson.PrimaryContactKey"] = payPersonJSON["primaryContactForSending"];
            details["i18n.PayPerson.phoneNumbers"] = payPersonJSON["phone"];
            details["i18n.payaAPerson.emailAddress"] = payPersonJSON["email"];
            if (payPersonJSON["secondaryEmail"])
                details["i18n.payaAPerson.emailAddress"] += "<br>" + payPersonJSON["secondaryEmail"];
            if (payPersonJSON["secondaryPhoneNumber"])
                details["i18n.PayPerson.phoneNumbers"] += "<br>" + payPersonJSON["secondaryPhoneNumber"];
            this.view.lblAckMessage.text = kony.i18n.getLocalizedString("i18n.PayAPerson.AddRecipientSuccessMessage");
            this.view.ImgAcknowledged.src = ViewConstants.IMAGES.SUCCESS_GREEN;
            this.view.keyValueList.setData(details);
        },

        /**
         * Send money onClick
         */
        sendMoney: function() {
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.onSendMoney(this.payPersonJSON);
        },

        /** 
         * Init lifecycle function
         */
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            var scopeObj = this;
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.view.flxCloseWarning.onClick = function() {
                scopeObj.view.flxWarning.setVisibility(false);
            };
            this.view.btnAddAnotherRecipient.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showAddRecipientView();
            };
            this.view.btnSendMoney.onClick = this.sendMoney;
        },

        /**
         * PreShow lifecycle event
         */
        preShow: function() {
            // @todoP2P
            // Change the active menu for P2P  
            this.view.customheadernew.activateMenu("FASTTRANSFERS", "Transfer Money");
        },

        /**
         * PostShow lifecycle event
         */
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
            applicationManager.getNavigationManager().applyUpdates(this);
            this.resetUI();
        },

        /**
         * resets the UI
         */
        resetUI: function() {
            this.view.btnSendMoney.setVisibility(true);
        },

        /**
         * onBreakpoint change handler
         */
        onBreakpointChange: function(ref, width) {
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
            this.view.keyValueList.onBreakpointChange();
        }
    }
});