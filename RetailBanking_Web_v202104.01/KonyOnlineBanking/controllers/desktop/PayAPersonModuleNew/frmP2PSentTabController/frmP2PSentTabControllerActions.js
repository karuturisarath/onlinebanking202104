define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnAddRecipient **/
    AS_Button_b3050d2f27044a75a15be44c85b33902: function AS_Button_b3050d2f27044a75a15be44c85b33902(eventobject) {
        var self = this;
        this.showAddRecipient();
    },
    /** onClick defined for btnSendMoneyNewRecipient **/
    AS_Button_i54db45a1af241148141bac268ffd09a: function AS_Button_i54db45a1af241148141bac268ffd09a(eventobject) {
        var self = this;
        this.showAddRecipient();
    },
    /** init defined for frmP2PSentTab **/
    AS_Form_a2142a95bee94689876b3ad6951a234f: function AS_Form_a2142a95bee94689876b3ad6951a234f(eventobject) {
        var self = this;
        return self.init.call(this);
    }
});