define(['commonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(commonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();
    return {
        /**
         * Update form UI
         */
        updateFormUI: function(viewPropertiesMap) {
            if (viewPropertiesMap.showProgressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewPropertiesMap.showProgressBar === false) {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (viewPropertiesMap.sentTransactions) {
                this.setSentSegmentData(viewPropertiesMap.sentTransactions, viewPropertiesMap.pagination);
                this.sentSortMap = [{
                        name: 'nickName',
                        imageFlx: this.view.imgSortTo,
                        clickContainer: this.view.flxSortTo
                    },
                    {
                        name: 'transactionDate',
                        imageFlx: this.view.imgSortDueDate,
                        clickContainer: this.view.flxSortDueDate
                    },
                    {
                        name: 'amount',
                        imageFlx: this.view.imgSortAmount,
                        clickContainer: this.view.flxSortAmount
                    }
                ];
                FormControllerUtility.setSortingHandlers(this.sentSortMap, this.onSentSortClickHandler, this);
                FormControllerUtility.updateSortFlex(this.sentSortMap, viewPropertiesMap.pagination);
            }
            if (viewPropertiesMap.noMoreRecords) {
                this.noRecords();
            }
            if (viewPropertiesMap.inFormError) {
                this.view.rtxMessageWarning.text = viewPropertiesMap.inFormError.errorMessage;
                this.view.flxWarning.setVisibility(true);
            }
        },

        /**
         * Show no further records view
         */
        noRecords() {
            this.view.imgNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_INACTIVE;
            this.view.flxNext.setEnabled(false);
        },

        /**
         * Sets sent transaction data
         * @param transaction
         */
        setSentSegmentData: function(transactions, paginationValues) {
            let dataMap = {
                "imgDropdown": "imgDropdown",
                "imgRowSelected": "imgRowSelected",
                "lblTo": "lblTo",
                "lblDate": "lblDate",
                "lblUsing": "lblUsing",
                "lblAmount": "lblAmount",
                "btnModify": "btnModify",
                "lblReferenceNo": "lblReferenceNo",
                "lblReferenceNumber1": "lblReferenceNumber1",
                "lblDeliveredOn1": "lblDeliveredOn1",
                "lblDeliveredOn": "lblDeliveredOn",
                "lblNote": "lblNote",
                "lblNote1": "lblNote1",
                "lblHeaderTitle": "lblHeaderTitle",
                "lblSeperator": "lblSeperator",
                "lblSeperatorBottom": "lblSeperatorBottom",
                "lblIdentifier": "lblIdentifier"
            };

            // sorting pending and posted transactions
            var pending = [];
            var posted = [];
            for (var index in transactions) {
                transactions[index].onCancel = function() {
                    kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showSentTransactionsView();
                };
                if (transactions[index].statusDescription === "Successful") {
                    posted.push(transactions[index]);
                } else {
                    transactions[index].isEditFlow = true;
                    pending.push(transactions[index]);
                }
            }

            let postedHeader = {
                "lblHeaderTitle": kony.i18n.getLocalizedString("i18n.accounts.posted"),
                "template": "flxSentHeaderP2P"
            }

            let postedTransactions = posted.map(function(dataItem) {
                var repeatButtonVisible = true;
                var repeatButtonOnclick = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.onSendMoney.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController, dataItem);
                if (dataItem.isPaypersonDeleted === "true" || !dataItem.personId) {
                    repeatButtonVisible = false;
                    repeatButtonOnclick = null;
                }
                return {
                    "btnModify": {
                        "text": kony.i18n.getLocalizedString("i18n.accounts.repeat"),
                        "toolTip": kony.i18n.getLocalizedString("i18n.accounts.repeat"),
                        "onClick": repeatButtonOnclick,
                        "isVisible": repeatButtonVisible,
                    },
                    "lblAmount": dataItem.amount ? kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.formatCurrency(dataItem.amount, false, applicationManager.getFormatUtilManager().getCurrencySymbol(dataItem.transactionCurrency)) : kony.i18n.getLocalizedString("i18n.common.NA"),
                    "lblDate": dataItem.transactionDate ? kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.getFormattedDateString(dataItem.transactionDate) : kony.i18n.getLocalizedString("i18n.common.NA"),
                    "lblDeliveredOn1": dataItem.transactionDate ? kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.getFormattedDateString(dataItem.transactionDate) : kony.i18n.getLocalizedString("i18n.common.NA"),
                    "lblNote1": dataItem.transactionsNotes ? dataItem.transactionsNotes : kony.i18n.getLocalizedString("i18n.common.none"),
                    "lblReferenceNumber1": dataItem.transactionId,
                    "lblTo": dataItem.payPersonName ? dataItem.payPersonName : kony.i18n.getLocalizedString("i18n.common.NA"),
                    "lblUsing": dataItem.p2pContact ? dataItem.p2pContact : kony.i18n.getLocalizedString("i18n.common.NA"),
                    "template": (kony.application.getCurrentBreakpoint() === 640 || responsiveUtils.isMobile) ? "flxSentMobileP2P" : "flxSentP2P",
                    "imgDropdown": {
                        "src": ViewConstants.IMAGES.ARRAOW_DOWN,
                        "accessibilityconfig": {
                            "a11yLabel": "View Transaction Details"
                        }
                    },
                    "imgRowSelected": ViewConstants.IMAGES.ARRAOW_UP,
                    "lblIdentifier": " ",
                    "lblSeperatorBottom": " ",
                    "lblSeperator": " ",
                    "lblReferenceNo": kony.i18n.getLocalizedString("i18n.PayAPerson.ReferenceNumber"),
                    "lblDeliveredOn": kony.i18n.getLocalizedString("i18n.PayAPerson.DeliveredOn"),
                    "lblNote": kony.i18n.getLocalizedString("i18n.PayAPerson.Note"),
                    "lblFrequencyTitle": kony.i18n.getLocalizedString("i18n.transfers.lblFrequency"),
                    "lblFrequencyValue": dataItem.frequencyType,
                    "lblReccurrenceTitle": kony.i18n.getLocalizedString("i18n.accounts.recurrence"),
                    "lblReccurrenceValue": dataItem.recurrenceDesc ? dataItem.recurrenceDesc : "-",
                };
            });

            let pendingHeader = {
                "lblHeaderTitle": {
                    "text": kony.i18n.getLocalizedString("i18n.billPay.scheduled"),
                    "skin": "sknLblE5690B15pxSSP"
                },
                "template": "flxSentHeaderP2P"
            }
            let pendingTransactions = pending.map(function(dataItem) {
                var btnCancelSeriesVisible = dataItem.frequencyType === configurationManager.OLBConstants.TRANSACTION_RECURRENCE.ONCE ? false : true;
                // var isMobileAndVisible = btnCancelSeriesVisible && (kony.application.getCurrentBreakpoint() === 640 && orientationHandler.isMobile);
                var data = {
                    "btnModify": {
                        "text": kony.i18n.getLocalizedString("i18n.transfers.Modify"),
                        "onClick": kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.onSendMoney.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController, dataItem),
                        "toolTip": kony.i18n.getLocalizedString("i18n.transfers.Modify"),
                    },
                    "lblAmount": dataItem.amount ? kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.formatCurrency(dataItem.amount, false, applicationManager.getFormatUtilManager().getCurrencySymbol(dataItem.transactionCurrency)) : kony.i18n.getLocalizedString("i18n.common.NA"),
                    "lblDate": dataItem.transactionDate ? kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.getFormattedDateString(dataItem.transactionDate) : kony.i18n.getLocalizedString("i18n.common.NA"),
                    "lblDeliveredOn1": dataItem.scheduledDate ? kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.getFormattedDateString(dataItem.scheduledDate) : kony.i18n.getLocalizedString("i18n.common.NA"),
                    "lblNote1": dataItem.transactionsNotes ? dataItem.transactionsNotes : kony.i18n.getLocalizedString("i18n.common.none"),
                    "lblReferenceNumber1": dataItem.transactionId,
                    "lblTo": dataItem.payPersonName ? dataItem.payPersonName : kony.i18n.getLocalizedString("i18n.common.NA"),
                    "lblUsing": dataItem.p2pContact ? dataItem.p2pContact : kony.i18n.getLocalizedString("i18n.common.NA"),
                    "template": (kony.application.getCurrentBreakpoint() === 640 || responsiveUtils.isMobile) ? "flxSentMobileP2P" : "flxSentP2P",
                    "imgDropdown": {
                        "src": ViewConstants.IMAGES.ARRAOW_DOWN,
                        "accessibilityconfig": {
                            "a11yLabel": "View Transaction Details"
                        }
                    },
                    "imgRowSelected": ViewConstants.IMAGES.ARRAOW_UP,
                    "lblIdentifier": " ",
                    "lblSeperatorBottom": " ",
                    "lblSeperator": " ",
                    "lblReferenceNo": kony.i18n.getLocalizedString("i18n.PayAPerson.ReferenceNumber"),
                    "lblDeliveredOn": kony.i18n.getLocalizedString("i18n.PayAPerson.DeliveredOn"),
                    "lblNote": kony.i18n.getLocalizedString("i18n.PayAPerson.Note"),
                    "lblFrequencyTitle": kony.i18n.getLocalizedString("i18n.transfers.lblFrequency"),
                    "lblFrequencyValue": dataItem.frequencyType,
                    "lblReccurrenceTitle": kony.i18n.getLocalizedString("i18n.accounts.recurrence"),
                    "lblReccurrenceValue": dataItem.recurrenceDesc ? dataItem.recurrenceDesc : "-",
                };
                //         if (isMobileAndVisible) {
                //           data.btnCancelThisOccurrence.left = "0";
                //           data.btnCancelThisOccurrence.width = "50%";
                //           data.btnCancelSeries.left = "50%";
                //           data.btnCancelSeries.width = "50%";
                //         }
                return data;
            });

            if (kony.application.getCurrentBreakpoint() === 640 || responsiveUtils.isMobile) {
                postedHeader["flxSentHeaderP2P"] = {
                    "skin": "sknFlxInnerShadow424242"
                }
                pendingHeader["flxSentHeaderP2P"] = {
                    "skin": "sknFlxInnerShadow424242"
                }
            }

            let rowData = [];
            this.view.segTransactions.widgetDataMap = dataMap;
            rowData = rowData.concat(pendingHeader).concat(pendingTransactions).concat(postedHeader).concat(postedTransactions);
            this.view.segTransactions.setData(rowData);
            this.view.flxNext.onClick = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.fetchNextSentTransactions.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController);
            this.view.flxPrevious.onClick = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.fetchPreviousSentTransactions.bind(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController);
            paginationValues.limit = transactions.length;
            this.updatePaginationValue(paginationValues, "Transactions");
        },

        /**
         * This method is sued as an onclick handler for sent transactions tab.
         */
        onSentSortClickHandler: function(event, data) {
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showSentTransactionsView(data);
        },

        /**
         * This method is used to update the pagination widget values.
         * @param{Object} values - it contains the set of values like offset, limit.
         * @param{String} paginationText - String representing paginated attribute
         */
        updatePaginationValue: function(values, paginationText) {
            this.view.lblPagination.text = (values.offset + 1) + " - " + (values.offset + values.limit) + " " + paginationText;
            if (values.offset >= values.paginationRowLimit) {
                this.view.imgPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_ACTIVE;
                this.view.flxPrevious.setEnabled(true);
            } else {
                this.view.imgPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_INACTIVE;
                this.view.flxPrevious.setEnabled(false);
            }
            if (values.limit < values.paginationRowLimit) {
                this.view.imgNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_INACTIVE;
                this.view.flxNext.setEnabled(false);
            } else {
                this.view.imgNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_ACTIVE;
                this.view.flxNext.setEnabled(true);
            }
        },

        /**
         * Contorls secondary action dropdown
         */
        showSecondaryActions: function() {
            this.view.imgDropdown.src = (this.view.imgDropdown.src === ViewConstants.IMAGES.ARROW_DOWN) ? ViewConstants.IMAGES.CHEVRON_UP : ViewConstants.IMAGES.ARROW_DOWN;
            this.view.secondaryActions.setVisibility(!this.view.secondaryActions.isVisible);
        },

        /**
         * Set up navigation on clicks for
         */
        secondaryActionsNavigation: function() {
            let index = this.view.secondaryActions.segAccountTypes.selectedRowIndex[1]
            if (index === 1)
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.loadP2PSettingsScreen();
            else
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showDeactivateP2P();
        },

        /**
         * Init lifecycle function
         */
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            var scopeObj = this;
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.view.flxCloseWarning.onClick = function() {
                scopeObj.view.flxWarning.setVisibility(false);
            };

            this.view.flxWhatElse.onClick = this.showSecondaryActions;
            this.view.secondaryActions.segAccountTypes.onRowClick = this.secondaryActionsNavigation;

            this.view.btnSendMoneyTab.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showSendMoneyTabView();
            };
            this.view.btnManageRecipientsTab.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("PayAPersonModuleNew").presentationController.showManageRecipientsView();
            };
        },

        /**
         * PreShow lifecycle event
         */
        preShow: function() {
            // @todoP2P
            // Change the active menu for P2P  
            this.view.customheadernew.activateMenu("FASTTRANSFERS", "Transfer Money");
        },

        /**
         * PostShow lifecycle event
         */
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
            applicationManager.getNavigationManager().applyUpdates(this);
        },

        /**
         * onBreakpoint change handler
         */
        onBreakpointChange: function(ref, width) {
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
            this.changeSwitchSegTempalte(width);
        },

        /**
         * to set expanded data in the segment
         */
        expandRow: function() {
            var rowIndex = this.view.segTransactions.selectedRowIndex[1];
            var data = this.view.segTransactions.data;
            if (data[rowIndex].template === "flxSentHeaderP2P")
                return;
            for (let i = 0; i < data.length; i++) {
                if (data[i].template === "flxSentHeaderP2P")
                    continue;
                if (i === rowIndex) {
                    data[i].imgDropdown = "chevron_up.png";
                    if (kony.application.getCurrentBreakpoint() === 640)
                        data[i].template = "flxSentSelectedMobileP2P"
                    else
                        data[i].template = "flxSentSelectedP2P";
                } else {
                    data[i].imgDropdown = "arrow_down.png";
                    if (kony.application.getCurrentBreakpoint() === 640)
                        data[i].template = "flxSentMobileP2P";
                    else
                        data[i].template = "flxSentP2P";
                }
            }
            this.view.segTransactions.setData(data);
        },

        /**
         * to collpase expanded row
         */
        collapseRow: function() {
            var rowIndex = this.view.segTransactions.selectedRowIndex[1];
            var data = this.view.segTransactions.data;
            if (kony.application.getCurrentBreakpoint() === 640) {
                data[rowIndex].template = "flxSentMobileP2P";
            } else {
                data[rowIndex].template = "flxSentP2P";
            }
            data[rowIndex].imgDropdown = "arrow_down.png";
            this.view.segTransactions.setDataAt(data[rowIndex], rowIndex);
        },

        /**
         * Function to change mobile and desktop tempalte of segement
         */
        changeSwitchSegTempalte: function(width) {
            let data = this.view.segTransactions.data;
            let template = (width === 640 || responsiveUtils.isMobile) ? "flxSentMobileP2P" : "flxSentP2P";
            if (data) {
                data.map(function(elem) {
                    if (elem.template === "flxSentHeaderP2P") {
                        if (width === 640 || responsiveUtils.isMobile)
                            elem["flxSentHeaderP2P"] = {
                                "skin": "sknFlxInnerShadow424242"
                            }
                        else
                            elem["flxSentHeaderP2P"] = {
                                "skin": "bbSKnFlxffffff"
                            }
                        return elem
                    } else
                        return elem.template = template;
                });
                this.view.segTransactions.setData(data);
            }

        }
    }

});