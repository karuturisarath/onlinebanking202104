define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for flxCheckBox **/
    AS_FlexContainer_d75ffbc5cda143018b15b6d320460822: function AS_FlexContainer_d75ffbc5cda143018b15b6d320460822(eventobject, context) {
        var self = this;
        this.toggleCheckBox(eventobject, context);
    },
    /** onTouchStart defined for lblDropDown **/
    AS_Label_faad85eb471a4ce8940c391552790ae2: function AS_Label_faad85eb471a4ce8940c391552790ae2(eventobject, x, y, context) {
        var self = this;
        this.toggleSegment(eventobject, context)
    }
});