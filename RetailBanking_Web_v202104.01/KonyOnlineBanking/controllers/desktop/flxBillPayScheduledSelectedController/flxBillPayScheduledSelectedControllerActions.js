define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnEdit **/
    AS_Button_ac00c0ab7b114403b083f7a1fbd8fe66: function AS_Button_ac00c0ab7b114403b083f7a1fbd8fe66(eventobject, context) {
        var self = this;
        this.showPayABill();
    },
    /** onClick defined for btnCancel **/
    AS_Button_b5779952aa294a979448c06633462fd0: function AS_Button_b5779952aa294a979448c06633462fd0(eventobject, context) {
        var self = this;
        this.showPayABill();
    },
    /** onClick defined for btnEbill **/
    AS_Button_ec6eed88c4d14a12adf093c94b0e84b5: function AS_Button_ec6eed88c4d14a12adf093c94b0e84b5(eventobject, context) {
        var self = this;
        this.showPayABill();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_j688cc733620430ca5e8d758ec95efb7: function AS_FlexContainer_j688cc733620430ca5e8d758ec95efb7(eventobject, context) {
        var self = this;
        this.segmentHistoryRowClick();
    }
});