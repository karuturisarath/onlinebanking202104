define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnViewActivity **/
    AS_Button_e041ae9ff764495690f0536e2aafc7b7: function AS_Button_e041ae9ff764495690f0536e2aafc7b7(eventobject, context) {
        var self = this;
        kony.print("Empty Action for View Activity due to platform defect");
    },
    /** onClick defined for btnModify **/
    AS_Button_e98995ed03f243d29996db05b63277dc: function AS_Button_e98995ed03f243d29996db05b63277dc(eventobject, context) {
        var self = this;
        this.showSendMoney();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_g8156f1d74524702aaa4be59a294cfe3: function AS_FlexContainer_g8156f1d74524702aaa4be59a294cfe3(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    }
});