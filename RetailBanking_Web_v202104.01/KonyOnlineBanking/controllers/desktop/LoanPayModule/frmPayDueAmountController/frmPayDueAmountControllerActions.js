define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnConfirm **/
    AS_Button_f5d7c3ed4af04f9285d2926333ea33da: function AS_Button_f5d7c3ed4af04f9285d2926333ea33da(eventobject) {
        var self = this;
        this.loanDataAfterConfirmation();
    },
    /** onClick defined for flxRadioPayOtherAmount **/
    AS_FlexContainer_aad3f837eca547f8831feaf8be188f57: function AS_FlexContainer_aad3f837eca547f8831feaf8be188f57(eventobject) {
        var self = this;
        var self = this;
        this.payDueAmountRadioButton("Other");
    },
    /** onClick defined for flxRadioPayDueAmount **/
    AS_FlexContainer_ca4b7f6e10c54ad1b93328f3ba667fa7: function AS_FlexContainer_ca4b7f6e10c54ad1b93328f3ba667fa7(eventobject) {
        var self = this;
        var self = this;
        this.payDueAmountRadioButton("Due");
    },
    /** onClick defined for flxCross **/
    AS_FlexContainer_f1159265018c413fb008a253c33c686f: function AS_FlexContainer_f1159265018c413fb008a253c33c686f(eventobject) {
        var self = this;
        this.quitPopUpNo();
    },
    /** onDeviceBack defined for frmPayDueAmount **/
    AS_Form_b0e7160a6b374e60be051b61112ea7d0: function AS_Form_b0e7160a6b374e60be051b61112ea7d0(eventobject) {
        var self = this;
        //Need to Consolidate
        kony.print("Back button pressed");
    },
    /** postShow defined for frmPayDueAmount **/
    AS_Form_cd94e734981b49cdbeb5ab4d34f4f525: function AS_Form_cd94e734981b49cdbeb5ab4d34f4f525(eventobject) {
        var self = this;
        this.postShowPayDueAmount();
    },
    /** onTouchEnd defined for frmPayDueAmount **/
    AS_Form_ced0930a179e4de8b1c410fe3ae6d697: function AS_Form_ced0930a179e4de8b1c410fe3ae6d697(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** preShow defined for frmPayDueAmount **/
    AS_Form_g4f57f598ab34e358b6b3294048f52f5: function AS_Form_g4f57f598ab34e358b6b3294048f52f5(eventobject) {
        var self = this;
        this.frmPayDueAmountPreShow();
    }
});