define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for flxCheckBox **/
    AS_FlexContainer_bdc9bab73f6748508efd1beea76e70c3: function AS_FlexContainer_bdc9bab73f6748508efd1beea76e70c3(eventobject, context) {
        var self = this;
        this.toggleSegRowCheckBox(eventobject, context);
    },
    /** onClick defined for flxRightCheckBox **/
    AS_FlexContainer_cb11bf4f9f4244da976da8bcbc15fe1f: function AS_FlexContainer_cb11bf4f9f4244da976da8bcbc15fe1f(eventobject, context) {
        var self = this;
        this.toggleSegRowCheckBox(eventobject, context, true);
    }
});