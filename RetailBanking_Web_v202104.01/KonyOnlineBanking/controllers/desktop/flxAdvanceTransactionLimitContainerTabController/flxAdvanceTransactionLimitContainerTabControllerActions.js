define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onEndEditing defined for tbxWeeklyTransDenialAmount **/
    AS_TextField_ac19972c89644b47bd18fab7536985dd: function AS_TextField_ac19972c89644b47bd18fab7536985dd(eventobject, changedtext, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimitsOnEndEditing.call(self, eventobject, changedtext, context);
    },
    /** onKeyUp defined for tbxWeeklyTransDenialAmount **/
    AS_TextField_b96c069819854b16ad6371610db5956f: function AS_TextField_b96c069819854b16ad6371610db5956f(eventobject, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimits.call(this, eventobject, context);
    },
    /** onKeyUp defined for tbxPerTransApproveAmount **/
    AS_TextField_c1d4b27ce9d04854aaeb56914a43133c: function AS_TextField_c1d4b27ce9d04854aaeb56914a43133c(eventobject, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimits.call(this, eventobject, context);
    },
    /** onKeyUp defined for tbxDailyTransDenialAmount **/
    AS_TextField_caecd92dc63c477a909553c67d916091: function AS_TextField_caecd92dc63c477a909553c67d916091(eventobject, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimits.call(this, eventobject, context);
    },
    /** onEndEditing defined for tbxWeeklyTransApproveAmount **/
    AS_TextField_d18763fe8503452785630ec71356b3de: function AS_TextField_d18763fe8503452785630ec71356b3de(eventobject, changedtext, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimitsOnEndEditing.call(self, eventobject, changedtext, context);
    },
    /** onEndEditing defined for tbxPerTransDenialAmount **/
    AS_TextField_d4d69bd22a3c4bbdbb305e0fc271a872: function AS_TextField_d4d69bd22a3c4bbdbb305e0fc271a872(eventobject, changedtext, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimitsOnEndEditing.call(self, eventobject, changedtext, context);
    },
    /** onEndEditing defined for tbxDailyTransDenialAmount **/
    AS_TextField_d80f6ea48bf241a7b9bde392748470a5: function AS_TextField_d80f6ea48bf241a7b9bde392748470a5(eventobject, changedtext, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimitsOnEndEditing.call(self, eventobject, changedtext, context);
    },
    /** onTextChange defined for tbxPerTransApproveAmount **/
    AS_TextField_e309285a5e9c40cdbbffacc8e4611422: function AS_TextField_e309285a5e9c40cdbbffacc8e4611422(eventobject, changedtext, context) {
        var self = this;
        return self.onLimitChanged.call(this, eventobject, context);
    },
    /** onEndEditing defined for tbxDailyTransApproveAmount **/
    AS_TextField_e6e75dc4bf9041a0b942115da4e1e69b: function AS_TextField_e6e75dc4bf9041a0b942115da4e1e69b(eventobject, changedtext, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimitsOnEndEditing.call(self, eventobject, changedtext, context);
    },
    /** onTextChange defined for tbxWeeklyTransDenialAmount **/
    AS_TextField_f03761efb5a949af973d3d0dcb80868c: function AS_TextField_f03761efb5a949af973d3d0dcb80868c(eventobject, changedtext, context) {
        var self = this;
        return self.onLimitChanged.call(this, eventobject, context);
    },
    /** onTextChange defined for tbxWeeklyTransApproveAmount **/
    AS_TextField_f09538da7feb4f338f6d5e0e66147489: function AS_TextField_f09538da7feb4f338f6d5e0e66147489(eventobject, changedtext, context) {
        var self = this;
        return self.onLimitChanged.call(this, eventobject, context);
    },
    /** onTextChange defined for tbxDailyTransApproveAmount **/
    AS_TextField_f72cd4cb0962493aa8f8c04ffebe76f7: function AS_TextField_f72cd4cb0962493aa8f8c04ffebe76f7(eventobject, changedtext, context) {
        var self = this;
        return self.onLimitChanged.call(this, eventobject, context);
    },
    /** onKeyUp defined for tbxDailyTransApproveAmount **/
    AS_TextField_fc92cc82d6ab4b2ea9f3265ca9ed2590: function AS_TextField_fc92cc82d6ab4b2ea9f3265ca9ed2590(eventobject, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimits.call(this, eventobject, context);
    },
    /** onTextChange defined for tbxDailyTransDenialAmount **/
    AS_TextField_g78183d83df64e26a8fd9f9f9385418e: function AS_TextField_g78183d83df64e26a8fd9f9f9385418e(eventobject, changedtext, context) {
        var self = this;
        return self.onLimitChanged.call(this, eventobject, context);
    },
    /** onKeyUp defined for tbxWeeklyTransApproveAmount **/
    AS_TextField_h3ed55645fe94eda8d0523f43a7a00ac: function AS_TextField_h3ed55645fe94eda8d0523f43a7a00ac(eventobject, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimits.call(this, eventobject, context);
    },
    /** onEndEditing defined for tbxPerTransApproveAmount **/
    AS_TextField_h57ebe916ce543be952bdd2e720c6f1a: function AS_TextField_h57ebe916ce543be952bdd2e720c6f1a(eventobject, changedtext, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimitsOnEndEditing.call(self, eventobject, changedtext, context);
    },
    /** onKeyUp defined for tbxPerTransDenialAmount **/
    AS_TextField_j0b1001c55794fccad8936286df0f8cd: function AS_TextField_j0b1001c55794fccad8936286df0f8cd(eventobject, context) {
        var self = this;
        var self = this;
        return self.validateTransactionLimits.call(this, eventobject, context);
    },
    /** onTextChange defined for tbxPerTransDenialAmount **/
    AS_TextField_jf94bbcd2e5b44ecb33e5c771d0cf586: function AS_TextField_jf94bbcd2e5b44ecb33e5c771d0cf586(eventobject, changedtext, context) {
        var self = this;
        return self.onLimitChanged.call(this, eventobject, context);
    }
});