define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_e986845eb762471fae3f4b896a73332e: function AS_FlexContainer_e986845eb762471fae3f4b896a73332e(eventobject, context) {
        this.segmentAllPayeesRowClick();
    },
    /** onClick defined for btnEbill **/
    AS_Button_c3188a5cccd34d228033f0570e7f348e: function AS_Button_c3188a5cccd34d228033f0570e7f348e(eventobject, context) {
        this.viewEBill();
    },
});