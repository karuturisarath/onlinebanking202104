define("CardManagementModuleNew/userfrmCardManagementChangePinController", ['CommonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(CommonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();
    return {
        selectedOption: "",
        billingAddress: "",
        updateFormUI: function(viewPropertiesMap) {
            if (viewPropertiesMap.progressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewPropertiesMap.progressBar === false) {
                CommonUtilities.hideProgressBar(this.view);
            }
            if (viewPropertiesMap.serverError) {
                CommonUtilities.hideProgressBar(this.view);
                this.showServerError(viewPropertiesMap.serverError);
            } else {
                this.hideServerError();
            }
            if (viewPropertiesMap.serverDown) {
                CommonUtilities.hideProgressBar(this.view);
                CommonUtilities.showServerDownScreen();
            }
            if (viewPropertiesMap.changePin) {
                this.setUpChangePin(viewPropertiesMap.changePin)
            }
        },
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.initActions();
        },
        preShow: function() {
            var scopeObj = this;
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            CommonUtilities.setText(this.view.WarningMessage.rtxWarningText1, kony.i18n.getLocalizedString("i18n.CardManagement.OfflineChangePinGuideline1"), accessibilityConfig);
            CommonUtilities.setText(this.view.WarningMessage.rtxWarningText2, kony.i18n.getLocalizedString("i18n.CardManagement.OfflineChangePinGuideline2"), accessibilityConfig);
            var reasonsMasterData = [];
            reasonsMasterData.push([OLBConstants.CARD_CHANGE_PIN_REASON.PIN_COMPROMISED, OLBConstants.CARD_CHANGE_PIN_REASON.PIN_COMPROMISED]);
            reasonsMasterData.push([OLBConstants.CARD_CHANGE_PIN_REASON.FORGOT_PIN, OLBConstants.CARD_CHANGE_PIN_REASON.FORGOT_PIN]);
            reasonsMasterData.push([OLBConstants.CARD_CHANGE_PIN_REASON.OTHER, OLBConstants.CARD_CHANGE_PIN_REASON.OTHER]);
            this.view.lbxReasonPinChange.masterData = reasonsMasterData;
            this.view.lbxReasonPinChange.selectedKey = OLBConstants.CARD_CHANGE_PIN_REASON.PIN_COMPROMISED;
            this.view.tbxNotePinChange.text = "";
            this.view.tbxNotePinChange.maxTextLength = OLBConstants.NOTES_LENGTH;
            CommonUtilities.setText(this.view.lblOption1, kony.i18n.getLocalizedString("i18n.ProfileManagement.EmailId"), accessibilityConfig);
            CommonUtilities.setText(this.view.lblOption2, kony.i18n.getLocalizedString("i18n.ProfileManagement.PhoneNumbers"), accessibilityConfig);
            CommonUtilities.setText(this.view.lblOption3, kony.i18n.getLocalizedString("i18n.ProfileManagement.postalAddress"), accessibilityConfig);
            this.view.lblCheckBox1.skin = ViewConstants.SKINS.CARD_RADIOBTN_LABEL_SELECTED;
            this.view.lblCheckBox1.text = "M";
            this.view.lblCheckBox2.skin = ViewConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
            this.view.lblCheckBox2.text = "L";
            this.view.lblCheckBox3.skin = ViewConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
            this.view.lblCheckBox3.text = "L";
            this.view.rtxRegisteredOption.text = kony.i18n.getLocalizedString("i18n.CardManagement.RegisteredEmailID") + " " + applicationManager.getUserPreferencesManager().getUserEmail();;
            CommonUtilities.setText(this.view.btnConfirm, kony.i18n.getLocalizedString('i18n.common.proceed'), accessibilityConfig);
            this.view.btnConfirm.toolTip = kony.i18n.getLocalizedString('i18n.common.proceed');
            CommonUtilities.setText(this.view.btnModify, kony.i18n.getLocalizedString("i18n.transfers.Cancel"), accessibilityConfig);
            this.view.btnModify.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
            applicationManager.getNavigationManager().applyUpdates(this);
        },
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
        },
        onBreakpointChange: function(form, width) {
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
            this.view.cardDetails.onBreakpointChange(width);
            var responsiveFonts = new ResponsiveFonts();
            if (responsiveUtils.isMobile) {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, kony.i18n.getLocalizedString("i18n.CardManagement.ChangePin"), CommonUtilities.getaccessibilityConfig());
                responsiveFonts.setMobileFonts();
            } else {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, "", CommonUtilities.getaccessibilityConfig());
                responsiveFonts.setDesktopFonts();
            }
        },
        initActions: function() {
            var self = this;
            this.view.flxCheckBox1.onTouchEnd = function() {
                var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
                self.view.lblCheckBox1.skin = ViewConstants.SKINS.CARD_RADIOBTN_LABEL_SELECTED;
                self.view.lblCheckBox1.text = "M";
                self.view.lblCheckBox2.skin = ViewConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
                self.view.lblCheckBox2.text = "L";
                self.view.lblCheckBox3.skin = ViewConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
                self.view.lblCheckBox3.text = "L";
                self.selectedOption = "E-mail ID";
                self.view.rtxRegisteredOption.text = kony.i18n.getLocalizedString("i18n.CardManagement.RegisteredEmailID") + " " + applicationManager.getUserPreferencesManager().getUserEmail();;
            };
            this.view.flxCheckBox2.onTouchEnd = function() {
                var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
                self.view.lblCheckBox1.skin = ViewConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
                self.view.lblCheckBox1.text = "L";
                self.view.lblCheckBox2.skin = ViewConstants.SKINS.CARD_RADIOBTN_LABEL_SELECTED;
                self.view.lblCheckBox2.text = "M";
                self.view.lblCheckBox3.skin = ViewConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
                self.view.lblCheckBox3.text = "L";
                self.selectedOption = "Phone No";
                self.view.rtxRegisteredOption.text = kony.i18n.getLocalizedString("i18n.CardManagement.RegisteredPhoneNo") + " " + applicationManager.getUserPreferencesManager().getUserPhone();
            };
            this.view.flxCheckBox3.onTouchEnd = function() {
                var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
                self.view.lblCheckBox1.skin = ViewConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
                self.view.lblCheckBox1.text = "L";
                self.view.lblCheckBox2.skin = ViewConstants.SKINS.CARDS_RADIOBTN_LABEL_UNSELECTED;
                self.view.lblCheckBox2.text = "L";
                self.view.lblCheckBox3.skin = ViewConstants.SKINS.CARD_RADIOBTN_LABEL_SELECTED;
                self.view.lblCheckBox3.text = "M";
                self.selectedOption = 'Postal Address';
                self.view.rtxRegisteredOption.text = kony.i18n.getLocalizedString("i18n.CardManagement.RegisteredAddress") + " " + self.billingAddress;
            };
            this.view.btnModify.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.navigateToManageCards();
            };
        },
        setUpChangePin: function(card) {
            this.view.cardDetails.setData(card);
            var self = this;
            this.billingAddress = card.billingAddress;
            this.selectedOption = "E-mail ID";
            FormControllerUtility.enableButton(this.view.btnConfirm);
            if (CommonUtilities.isCSRMode()) {
                this.view.btnConfirm.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.btnConfirm.skin = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                this.view.btnConfirm.onClick = function() {
                    var params = {
                        'card': card,
                        'CardAccountNumber': card.maskedCardNumber,
                        'CardAccountName': card.productName,
                        'RequestReason': self.view.lbxReasonPinChange.selectedKey,
                        'AdditionalNotes': self.view.tbxNotePinChange.text,
                        'AccountType': 'CARD',
                        'RequestCode': 'NEW_PIN',
                        'Channel': OLBConstants.Channel,
                        'userName': kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.getUserName()
                    };
                    if (this.selectedOption === 'Postal Address') {
                        params['Address_id'] = self.getSelectedAddressId();
                    } else if (this.selectedOption === "Phone No") {
                        var contactNumbers = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.fetchUserPhoneNumbers();
                        params['communication_id'] = contactNumbers.filter(function(item) {
                            return item.isPrimary === "true"
                        })[0].id;
                    } else {
                        var emailids = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.fetchUserEmailIds();
                        params['communication_id'] = emailids.filter(function(item) {
                            return item.isPrimary === "true"
                        })[0].id;
                    }
                    self.initMFAFlow(params, "Offline_Change_Pin");
                }.bind(this);
            }
        },
        initMFAFlow: function(params, action) {
            CommonUtilities.showProgressBar(this.view);
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.verifySecureAccessCodeSuccess(params, action);
        },
        /**
         *  Method to show error flex.
         * @param {String} - Error message to be displayed.
         */
        showServerError: function(errorMsg) {
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            this.view.flxDowntimeWarning.setVisibility(true);
            if (errorMsg.errorMessage && errorMsg.errorMessage != "") {
                CommonUtilities.setText(this.view.rtxDowntimeWarning, errorMsg.errorMessage, accessibilityConfig);
            } else {
                CommonUtilities.setText(this.view.rtxDowntimeWarning, errorMsg, accessibilityConfig);
            }
            this.view.rtxDowntimeWarning.setFocus(true);
        },
        /**
         * Method to hide error flex.
         */
        hideServerError: function() {
            this.view.flxDowntimeWarning.setVisibility(false);
        },
    };
});