define("CardManagementModuleNew/userfrmCardManagementConfirmPinController", ['CommonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(CommonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();
    return {
        updateFormUI: function(viewPropertiesMap) {
            if (viewPropertiesMap.progressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewPropertiesMap.progressBar === false) {
                CommonUtilities.hideProgressBar(this.view);
            }
            if (viewPropertiesMap.serverError) {
                CommonUtilities.hideProgressBar(this.view);
                this.showServerError(viewPropertiesMap.serverError);
            } else {
                this.hideServerError();
            }
            if (viewPropertiesMap.serverDown) {
                CommonUtilities.hideProgressBar(this.view);
                CommonUtilities.showServerDownScreen();
            }
            if (viewPropertiesMap.changePin) {
                this.setUpChangePin(viewPropertiesMap.changePin)
            }
        },
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.initActions();
        },
        preShow: function() {
            var scopeObj = this;
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            CommonUtilities.setText(this.view.WarningMessage.rtxWarningText1, kony.i18n.getLocalizedString("i18n.CardManagement.OnlineChangePinGuideline1"), accessibilityConfig);
            CommonUtilities.setText(this.view.WarningMessage.rtxWarningText2, kony.i18n.getLocalizedString("i18n.CardManagement.OnlineChangePinGuideline2"), accessibilityConfig);
            this.view.lblReason.setVisibility(false);
            this.view.lbxReason.setVisibility(false);
            this.view.tbxCurrentPIN.text = "";
            this.view.tbxNewPIN.text = "";
            this.view.imgNewPIN.setVisibility(false);
            this.view.tbxConfirmPIN.text = "";
            this.view.imgConfirmPIN.setVisibility(false);
            this.view.tbxNote.text = "";
            this.view.tbxNote.maxTextLength = OLBConstants.NOTES_LENGTH;
            this.view.tbxConfirmPIN.secureTextEntry = false;
            FormControllerUtility.disableButton(this.view.btnConfirm);
            CommonUtilities.setText(this.view.btnConfirm, kony.i18n.getLocalizedString('i18n.common.proceed'), accessibilityConfig);
            this.view.btnConfirm.toolTip = kony.i18n.getLocalizedString('i18n.common.proceed');
            CommonUtilities.setText(this.view.btnModify, kony.i18n.getLocalizedString("i18n.transfers.Cancel"), accessibilityConfig);
            this.view.btnModify.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
            applicationManager.getNavigationManager().applyUpdates(this);
        },
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
        },
        onBreakpointChange: function(form, width) {
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
            this.view.cardDetails.onBreakpointChange(width);
            var responsiveFonts = new ResponsiveFonts();
            if (responsiveUtils.isMobile) {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, kony.i18n.getLocalizedString("i18n.CardManagement.ChangePin"), CommonUtilities.getaccessibilityConfig());
                responsiveFonts.setMobileFonts();
            } else {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, "", CommonUtilities.getaccessibilityConfig());
                responsiveFonts.setDesktopFonts();
            }
        },
        initActions: function() {
            var self = this;
            this.view.btnModify.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.navigateToManageCards();
            };
            if (CommonUtilities.isCSRMode()) {
                this.view.btnConfirm.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.btnConfirm.skin = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                this.view.tbxCurrentPIN.onKeyUp = function() {
                    var enteredPin = self.view.tbxCurrentPIN.text;
                    if (self.isValidPin(enteredPin) && self.isValidPin(self.view.tbxNewPIN.text) && self.view.tbxNewPIN.text === self.view.tbxConfirmPIN.text) {
                        FormControllerUtility.enableButton(self.view.btnConfirm);
                    } else {
                        FormControllerUtility.disableButton(self.view.btnConfirm);
                    }
                }.bind(this);
                this.view.tbxNewPIN.onKeyUp = function() {
                    var enteredPin = self.view.tbxNewPIN.text;
                    if (self.isValidPin(enteredPin) && self.isValidPin(self.view.tbxCurrentPIN.text) && enteredPin === self.view.tbxConfirmPIN.text) {
                        FormControllerUtility.enableButton(self.view.btnConfirm);
                    } else {
                        FormControllerUtility.disableButton(self.view.btnConfirm);
                    }
                    if (self.isValidPin(enteredPin) && enteredPin === self.view.tbxConfirmPIN.text) {
                        self.view.imgConfirmPIN.setVisibility(true);
                        self.view.imgConfirmPIN.src = 'success_green.png';
                        self.view.forceLayout();
                    } else {
                        self.view.imgConfirmPIN.setVisibility(false);
                    }
                    if (self.isValidPin(enteredPin)) {
                        self.view.imgNewPIN.setVisibility(true);
                        self.view.imgNewPIN.src = 'success_green.png';
                        self.view.forceLayout();
                    } else {
                        self.view.imgNewPIN.setVisibility(false);
                    }
                }.bind(this);
                this.view.tbxConfirmPIN.onKeyUp = function() {
                    var enteredPin = self.view.tbxConfirmPIN.text;
                    if (self.isValidPin(enteredPin) && self.isValidPin(self.view.tbxCurrentPIN.text) && enteredPin === self.view.tbxNewPIN.text) {
                        FormControllerUtility.enableButton(self.view.btnConfirm);
                    } else {
                        FormControllerUtility.disableButton(self.view.btnConfirm);
                    }
                    if (self.isValidPin(enteredPin) && enteredPin === self.view.tbxNewPIN.text) {
                        self.view.imgConfirmPIN.setVisibility(true);
                        self.view.imgConfirmPIN.src = 'success_green.png';
                        self.view.forceLayout();
                    } else {
                        self.view.imgConfirmPIN.setVisibility(false);
                    }
                }.bind(this);
            }
        },
        setUpChangePin: function(card) {
            this.view.cardDetails.setData(card);
            var self = this;
            if (!CommonUtilities.isCSRMode()) {
                this.view.btnConfirm.onClick = function() {
                    var params = {
                        'card': card,
                        'notes': self.view.tbxNote.text,
                        'newPin': self.view.tbxConfirmPIN.text
                    };
                    self.initMFAFlow(params, kony.i18n.getLocalizedString("i18n.CardManagement.ChangePin"));
                }.bind(this);
            }
        },
        /**
         * Method used to check if the entered pin is valid or not.
         * @param {String} pin - contains the entered pin.
         */
        isValidPin: function(pin) {
            var regex = new RegExp('^[0-9]{4,4}$');
            if (regex.test(pin)) {
                for (var i = 1; i < pin.length; i++) {
                    if (Number(pin[i]) - 1 !== Number(pin[i - 1])) {
                        return true;
                    }
                }
            }
            return false;
        },
        initMFAFlow: function(params, action) {
            CommonUtilities.showProgressBar(this.view);
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.verifySecureAccessCodeSuccess(params, action);
        },
        /**
         *  Method to show error flex.
         * @param {String} - Error message to be displayed.
         */
        showServerError: function(errorMsg) {
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            this.view.flxDowntimeWarning.setVisibility(true);
            if (errorMsg.errorMessage && errorMsg.errorMessage != "") {
                CommonUtilities.setText(this.view.rtxDowntimeWarning, errorMsg.errorMessage, accessibilityConfig);
            } else {
                CommonUtilities.setText(this.view.rtxDowntimeWarning, errorMsg, accessibilityConfig);
            }
            this.view.rtxDowntimeWarning.setFocus(true);
        },
        /**
         * Method to hide error flex.
         */
        hideServerError: function() {
            this.view.flxDowntimeWarning.setVisibility(false);
        },
    };
});