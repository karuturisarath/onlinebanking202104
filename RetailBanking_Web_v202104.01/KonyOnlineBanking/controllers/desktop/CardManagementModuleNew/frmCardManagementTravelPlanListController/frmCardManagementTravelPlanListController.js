define("CardManagementModuleNew/userfrmCardManagementTravelPlanListController", ['CommonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(CommonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();

    return {
        /**
         * Globals for storing travel-notification,card-image, status-skin mappings.
         */
        notificationObject: {},
        statusSkinsLandingScreen: {},
        travelNotificationDataMap: {},

        updateFormUI: function(viewPropertiesMap) {
            if (viewPropertiesMap.progressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewPropertiesMap.progressBar === false) {
                CommonUtilities.hideProgressBar(this.view);
            }
            if (viewPropertiesMap.serverError) {
                CommonUtilities.hideProgressBar(this.view);
                this.showServerError(viewPropertiesMap.serverError);
            } else {
                this.hideServerError();
            }
            if (viewPropertiesMap.serverDown) {
                CommonUtilities.hideProgressBar(this.view);
                CommonUtilities.showServerDownScreen();
            }
            if (viewPropertiesMap.travelNotificationsList) {
                applicationManager.executeAuthorizationFramework(this, "updateTravelNotifications");
                this.setTravelNotificationsData(viewPropertiesMap.travelNotificationsList.TravelRequests);
            }
        },
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.initActions();
        },
        preShow: function() {
            var scopeObj = this;
            this.initializeCards();
            applicationManager.getLoggerManager().setCustomMetrics(this, false, "Cards");
            applicationManager.getNavigationManager().applyUpdates(this);
        },
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
            applicationManager.executeAuthorizationFramework(this);
        },
        onBreakpointChange: function(form, width) {
            var scope = this;
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
            var responsiveFonts = new ResponsiveFonts();
            if (responsiveUtils.isMobile) {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, "My Cards", CommonUtilities.getaccessibilityConfig());
                responsiveFonts.setMobileFonts();
            } else {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, "", CommonUtilities.getaccessibilityConfig());
                responsiveFonts.setDesktopFonts();
            }
        },
        initActions: function() {
            var scopeObj = this;
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();

            CommonUtilities.setText(this.view.lblManageTravelPlans, kony.i18n.getLocalizedString('i18n.CardManagement.AddNewTravelPlan'), accessibilityConfig);
            this.view.lblManageTravelPlans.toolTip = kony.i18n.getLocalizedString('i18n.CardManagement.AddNewTravelPlan');
            this.view.flxMangeTravelPlans.onClick = function() {
                FormControllerUtility.showProgressBar(this.view);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.AddNewTravelPlan();
            }
            CommonUtilities.setText(this.view.lblContactUs, kony.i18n.getLocalizedString("i18n.footer.contactUs"), accessibilityConfig);
            this.view.lblContactUs.toolTip = kony.i18n.getLocalizedString("i18n.footer.contactUs");
            this.view.flxContactUs.onClick = function() {
                var informationContentModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("InformationContentModule");
                informationContentModule.presentationController.showContactUsPage();
            };
        },
        showServerError: function(errorMsg) {
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            this.view.flxDowntimeWarning.setVisibility(true);
            if (errorMsg.errorMessage && errorMsg.errorMessage != "") {
                CommonUtilities.setText(this.view.rtxDowntimeWarning, errorMsg.errorMessage, accessibilityConfig);
            } else {
                CommonUtilities.setText(this.view.rtxDowntimeWarning, errorMsg, accessibilityConfig);
            }
            this.view.rtxDowntimeWarning.setFocus(true);
            this.view.flxMyCardsView.setVisibility(false);
        },
        hideServerError: function() {
            this.view.flxDowntimeWarning.setVisibility(false);
        },
        initializeCards: function() {
            this.initializeStatusSkins();
            this.setTravelNotificationDataMap();
        },
        initializeStatusSkins: function() {
            this.statusSkinsLandingScreen['Active'] = ViewConstants.SKINS.CARDS_ACTIVE_STATUS_LANDING;
            this.statusSkinsLandingScreen['Issued'] = ViewConstants.SKINS.CARDS_ACTIVE_STATUS_LANDING;
            this.statusSkinsLandingScreen['Locked'] = ViewConstants.SKINS.CARDS_LOCKED_STATUS_LANDING;
            this.statusSkinsLandingScreen['Reported Lost'] = ViewConstants.SKINS.CARDS_REPORTED_LOST_STATUS_LANDING;
            this.statusSkinsLandingScreen['Replace Request Sent'] = ViewConstants.SKINS.CARDS_REPLACE_REQUEST_SENT_STATUS_LANDING;
            this.statusSkinsLandingScreen['Replaced'] = ViewConstants.SKINS.CARDS_REPLACE_REQUEST_SENT_STATUS_LANDING;
            this.statusSkinsLandingScreen['Cancel Request Sent'] = ViewConstants.SKINS.CARDS_CANCEL_REQUEST_SENT_STATUS_LANDING;
            this.statusSkinsLandingScreen['Cancelled'] = ViewConstants.SKINS.CARDS_CANCELLED_STATUS_LANDING;
            this.statusSkinsLandingScreen['Inactive'] = ViewConstants.SKINS.CARDS_INACTIVE_STATUS_LANDING;
        },
        setTravelNotificationDataMap: function() {
            this.travelNotificationDataMap = {
                "flxActions": "flxActions",
                "lblSeparator1": "lblSeparator1",
                "lblCardHeader": "lblCardHeader",
                "lblCardId": "lblCardId",
                "lblCardStatus": "lblCardStatus",
                "lblIdentifier": "lblIdentifier",
                "flxCollapse": "flxCollapse",
                "imgCollapse": "imgCollapse",
                "imgChevron": "imgChevron",
                "lblKey1": "lblKey1",
                "rtxValue1": "rtxValue1",
                "lblKey2": "lblKey2",
                "rtxValue2": "rtxValue2",
                "flxDestination1": "flxDestination1",
                "flxDestination2": "flxDestination2",
                "flxDestination3": "flxDestination3",
                "flxDestination4": "flxDestination4",
                "flxDestination5": "flxDestination5",
                "lblDestination1": "lblDestination1",
                "rtxDestination1": "rtxDestination1",
                "lblDestination2": "lblDestination2",
                "rtxDestination2": "rtxDestination2",
                "lblDestination3": "lblDestination3",
                "rtxDestination3": "rtxDestination3",
                "lblDestination4": "lblDestination4",
                "rtxDestination4": "rtxDestination4",
                "lblDestination5": "lblDestination5",
                "rtxDestination5": "rtxDestination5",
                "lblKey4": "lblKey4",
                "rtxValue4": "rtxValue4",
                "lblKey5": "lblKey5",
                "rtxValue5": "rtxValue5",
                "lblKey6": "lblKey6",
                "rtxValueA": "rtxValueA",
                "btnAction1": "btnAction1",
                "btnAction2": "btnAction2"
            };
        },
        setTravelNotificationsData: function(travelNotifications) {
            var self = this;
            if (travelNotifications == undefined || travelNotifications.lengh == 0) {
                self.showNoTravelNotificationScreen();
            } else {
                var widgetDataMap = this.travelNotificationDataMap;
                var flxCardSkin = (kony.application.getCurrentBreakpoint() === 640) ? "sknFlxffffffRoundedBorder" : "sknFlxffffffShadowdddcdc";
                var segData = travelNotifications.map(function(dataItem) {
                    var destinations = self.returnDestinationsArray(dataItem.destinations);
                    return {
                        "flxActions": {
                            "isVisible": true
                        },
                        "lblSeparator1": " ",
                        "lblIdentifier": {
                            "text": " ",
                            "accessibilityconfig": {
                                "a11yLabel": " "
                            }
                        },
                        "lblCardHeader": {
                            "text": kony.i18n.getLocalizedString("i18n.CardManagement.requestId"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.CardManagement.requestId")
                            }
                        },
                        "lblCardId": {
                            "text": dataItem.notificationId,
                            "accessibilityconfig": {
                                "a11yLabel": dataItem.notificationId
                            }
                        },
                        "lblCardStatus": {
                            "text": dataItem.status,
                            "skin": self.statusSkinsLandingScreen[dataItem.status],
                            "accessibilityconfig": {
                                "a11yLabel": dataItem.status
                            }
                        },
                        "flxCollapse": {
                            "onClick": self.changeNotificationRowTemplate
                        },
                        "imgCollapse": {
                            "src": ViewConstants.IMAGES.ARRAOW_DOWN,
                            "accessibilityconfig": {
                                "a11yLabel": "View Transaction Details"
                            }
                        },
                        "lblKey1": {
                            "text": kony.i18n.getLocalizedString("i18n.CardManagement.travelStartDate"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.CardManagement.travelStartDate")
                            }
                        },
                        "rtxValue1": {
                            "text": self.returnFrontendDate(dataItem.startDate),
                            "accessibilityconfig": {
                                "a11yLabel": self.returnFrontendDate(dataItem.startDate)
                            }
                        },
                        "lblKey2": {
                            "text": kony.i18n.getLocalizedString("i18n.CardManagement.travelEndDate"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.CardManagement.travelEndDate")
                            }
                        },
                        "rtxValue2": {
                            "text": self.returnFrontendDate(dataItem.endDate),
                            "accessibilityconfig": {
                                "a11yLabel": self.returnFrontendDate(dataItem.endDate)
                            }
                        },
                        "flxDestination1": {
                            "isVisible": destinations[0] ? true : false
                        },
                        "lblDestination1": {
                            "text": kony.i18n.getLocalizedString("i18n.CardManagement.destination1"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.CardManagement.destination1")
                            }
                        },
                        "rtxDestination1": destinations[0],
                        "flxDestination2": {
                            "isVisible": destinations[1] ? true : false
                        },
                        "lblDestination2": {
                            "text": kony.i18n.getLocalizedString("i18n.CardManagement.destination2"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.CardManagement.destination2")
                            }
                        },
                        "rtxDestination2": destinations[1],
                        "flxDestination3": {
                            "isVisible": destinations[2] ? true : false
                        },
                        "lblDestination3": {
                            "text": kony.i18n.getLocalizedString("i18n.CardManagement.destination3"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.CardManagement.destination3")
                            }
                        },
                        "rtxDestination3": destinations[2],
                        "flxDestination4": {
                            "isVisible": destinations[3] ? true : false
                        },
                        "lblDestination4": {
                            "text": kony.i18n.getLocalizedString("i18n.CardManagement.destination4"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.CardManagement.destination4")
                            }
                        },
                        "rtxDestination4": destinations[3],
                        "flxDestination5": {
                            "isVisible": destinations[4] ? true : false
                        },
                        "lblDestination5": {
                            "text": kony.i18n.getLocalizedString("i18n.CardManagement.destination5"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.CardManagement.destination5")
                            }
                        },
                        "rtxDestination5": destinations[4],
                        "lblKey4": {
                            "text": kony.i18n.getLocalizedString("i18n.ProfileManagement.PhoneNumber"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.ProfileManagement.PhoneNumber")
                            }
                        },
                        "rtxValue4": {
                            "text": dataItem.contactNumber,
                            "accessibilityconfig": {
                                "a11yLabel": dataItem.contactNumber
                            }
                        },
                        "lblKey5": {
                            "text": kony.i18n.getLocalizedString("i18n.CardManagement.additionalInfo"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.CardManagement.additionalInfo")
                            }
                        },
                        "rtxValue5": {
                            "text": dataItem.additionalNotes.length >= 1 ? dataItem.additionalNotes : kony.i18n.getLocalizedString("i18n.common.none"),
                            "accessibilityconfig": {
                                "a11yLabel": dataItem.additionalNotes.length >= 1 ? dataItem.additionalNotes : kony.i18n.getLocalizedString("i18n.common.none")
                            }
                        },
                        "lblKey6": {
                            "text": kony.i18n.getLocalizedString("i18n.CardManagement.selectedCards"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.CardManagement.selectedCards")
                            }
                        },
                        "rtxValueA": {
                            "text": self.returnCardDisplayName(dataItem.cardNumber),
                            "accessibilityconfig": {
                                "a11yLabel": self.returnCardDisplayName(dataItem.cardNumber)
                            }
                        },
                        "btnAction1": {
                            "skin": dataItem.status === 'Expired' ? "sknBtnSSP3343A813PxBg0CSR" : "sknBtnSSP3343a813px",
                            "text": kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            "onClick": dataItem.status === 'Expired' ? null : self.editTravelNotification.bind(self, dataItem)
                        },
                        "btnAction2": {
                            "skin": dataItem.status === 'Expired' ? "sknBtnSSP3343A813PxBg0CSR" : "sknBtnSSP3343a813px",
                            "text": kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"),
                            "onClick": dataItem.status === 'Expired' ? null : self.deleteNotification.bind(self, dataItem.notificationId)
                        },
                        "flxMyCards": {
                            "clipBounds": false,
                            "skin": flxCardSkin,
                            "onClick": self.viewCardDetailsMobile
                        },
                        "imgChevron": "arrow_left_grey.png",
                        "template": "flxTravelNotificationsCollapsed"
                    };
                });
                this.view.segMyCards.widgetDataMap = widgetDataMap;
                this.view.segMyCards.setData(segData);
            }
            CommonUtilities.hideProgressBar(this.view);
        },
        returnDestinationsArray: function(destinations) {
            var array = [];
            destinations.split('-').forEach(function(destination) {
                array.push(destination);
            })
            return array;
        },
        changeNotificationRowTemplate: function() {
            var index = this.view.segMyCards.selectedRowIndex;
            var rowIndex = index[1];
            var data = this.view.segMyCards.data;
            for (var i = 0; i < data.length; i++) {
                if (i === rowIndex) {
                    if (data[i].template === "flxTravelNotificationsCollapsed") {
                        data[i].imgCollapse = {
                            "src": ViewConstants.IMAGES.ARRAOW_UP,
                            "accessibilityconfig": {
                                "a11yLabel": "View Details"
                            }
                        };
                        data[i].template = "flxTravelNotificationsExpanded";
                    } else {
                        data[i].imgCollapse = {
                            "src": ViewConstants.IMAGES.ARRAOW_DOWN,
                            "accessibilityconfig": {
                                "a11yLabel": "View Details"
                            }
                        };
                        data[i].template = "flxTravelNotificationsCollapsed";
                    }
                } else {
                    data[i].imgCollapse = {
                        "src": ViewConstants.IMAGES.ARRAOW_DOWN,
                        "accessibilityconfig": {
                            "a11yLabel": "View Details"
                        }
                    };
                    data[i].template = "flxTravelNotificationsCollapsed";
                }
            }
            this.view.segMyCards.setData(data);
            this.view.forceLayout();
        },
        showNoTravelNotificationScreen: function() {
            this.view.segMyCards.setVisibility(false);
            this.view.flxNoError.setVisibility(true);
            this.view.flxNoError.skin = "sknFlxffffffShadowdddcdc";
            this.view.flxNoCardsError.setVisibility(true);
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            CommonUtilities.setText(this.view.lblNoCardsError, kony.i18n.getLocalizedString('i18n.CardsManagement.NoTravelNotificationError'), accessibilityConfig);
            CommonUtilities.setText(this.view.btnApplyForCard, kony.i18n.getLocalizedString("i18n.CardManagement.BackToCards"), accessibilityConfig);
            this.view.flxApplyForCards.setVisibility(true);
            this.view.btnApplyForCard.toolTip = kony.i18n.getLocalizedString("i18n.CardManagement.BackToCards");
            this.view.btnApplyForCard.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.navigateToManageCards();
            }
        },
        returnFrontendDate: function(date) {
            var dateString = CommonUtilities.getFrontendDateString(date);
            return dateString;
        },
        returnCardDisplayName: function(cards) {
            return cards.replace(/,/g, "<br/>");
        },
        editTravelNotification: function(data) {
            var self = this;
            this.notificationObject.requestId = data.notificationId;
            this.notificationObject.isEditFlow = true;

            //todo: pass cardnumber and isEdit flow:
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.AddNewTravelPlan();


            self.getBackendCards(data.cardNumber);
            this.view.lblRequestID.setVisibility(true);
            this.view.lblRequestNo.setVisibility(true);
            this.view.segDestinations.setVisibility(true);
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            CommonUtilities.setText(this.view.lblRequestID, kony.i18n.getLocalizedString('i18n.CardManagement.requestId'), accessibilityConfig);
            CommonUtilities.setText(this.view.lblRequestNo, data.notificationId, accessibilityConfig);
            this.view.calFrom.dateComponents = this.getDateComponent(self.returnFrontendDate(data.startDate));
            this.view.calTo.dateComponents = this.getDateComponent(self.returnFrontendDate(data.endDate));
            this.view.txtPhoneNumber.text = data.contactNumber;
            this.view.txtareaUserComments.text = data.additionalNotes;
            this.setDestinations(data.destinations);
        },
        /**
         * Method to get date component
         * @param {string} - Date string
         * @returns {Object} - dateComponent Object
         */
        getDateComponent: function(dateString) {
            var dateObj = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(dateString, (applicationManager.getFormatUtilManager().getDateFormat()).toUpperCase());
            return [dateObj.getDate(), dateObj.getMonth() + 1, dateObj.getFullYear()];
        },
        deleteNotification: function(notificationId) {
            var self = this;
            this.view.flxDialogs.setVisibility(true);
            this.view.flxAlert.setVisibility(true);
            var height = this.view.customheader.info.frame.height + this.view.flxMain.info.frame.height + this.view.flxFooter.info.frame.height;
            this.view.flxAlert.height = height + "dp";
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            CommonUtilities.setText(this.view.CustomAlertPopup.lblPopupMessage, kony.i18n.getLocalizedString("i18n.CardManagement.deleteTravelMsg") + " " + notificationId + " ?", accessibilityConfig);
            CommonUtilities.setText(this.view.CustomAlertPopup.lblHeading, kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount"), accessibilityConfig);
            this.view.CustomAlertPopup.lblHeading.setFocus(true);
            this.view.CustomAlertPopup.btnYes.onClick = function() {
                FormControllerUtility.showProgressBar(self.view);
                self.view.flxAlert.setVisibility(false);
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.deleteNotification(notificationId);
            };
            this.view.CustomAlertPopup.flxCross.onClick = function() {
                self.view.flxAlert.isVisible = false;
            };
            this.view.CustomAlertPopup.btnNo.onClick = function() {
                self.view.flxAlert.isVisible = false;
            };
            this.view.forceLayout();
        },
        deleteNotificationSuccess: function() {
            FormControllerUtility.showProgressBar(this.view);
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.fetchTravelNotifications();

        },

        setMobileHeader: function(text) {
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            if (responsiveUtils.isMobile) {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, text, accessibilityConfig);
            } else {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, "", accessibilityConfig);
            }
        }
    };
});