define("CardManagementModuleNew/userfrmCardManagementCardReplacementController", ['CommonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function(CommonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
    var responsiveUtils = new ResponsiveUtils();
    return {
        updateFormUI: function(viewPropertiesMap) {
            if (viewPropertiesMap.progressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewPropertiesMap.progressBar === false) {
                CommonUtilities.hideProgressBar(this.view);
            }
            if (viewPropertiesMap.serverError) {
                CommonUtilities.hideProgressBar(this.view);
                this.showServerError(viewPropertiesMap.serverError);
            } else {
                this.hideServerError();
            }
            if (viewPropertiesMap.serverDown) {
                CommonUtilities.hideProgressBar(this.view);
                CommonUtilities.showServerDownScreen();
            }
            if (viewPropertiesMap.replaceCard) {
                this.setUpReplaceCard(viewPropertiesMap.replaceCard);
            }
            if (viewPropertiesMap.reportLostCard) {
                this.setUpReportLostCard(viewPropertiesMap.reportLostCard);
            }
            if (viewPropertiesMap.cancelCard) {
                this.setUpCancelCard(viewPropertiesMap.cancelCard);
            }
            if (viewPropertiesMap.TndCSuccessCancelCard) {
                this.showTermsAndConditionsSuccessScreenCancelCard(viewPropertiesMap.TndCSuccessCancelCard);
            }
        },
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.initActions();
        },
        preShow: function() {
            var scopeObj = this;
            applicationManager.getNavigationManager().applyUpdates(this);
        },
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - parseInt(this.view.flxHeader.height.slice(0, -2)) - parseInt(this.view.flxFooter.height.slice(0, -2)) + "dp";
        },
        onBreakpointChange: function(form, width) {
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
            this.view.cardDetails.onBreakpointChange(width);
            var responsiveFonts = new ResponsiveFonts();
            if (responsiveUtils.isMobile) {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, "My Cards", CommonUtilities.getaccessibilityConfig());
                responsiveFonts.setMobileFonts();
            } else {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, "", CommonUtilities.getaccessibilityConfig());
                responsiveFonts.setDesktopFonts();
            }
        },
        initActions: function() {
            var self = this;
            this.view.btnModify.onClick = function() {
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.navigateToManageCards();
            };
            this.view.WarningMessage.flxCheckbox.onTouchEnd = function() {
                CommonUtilities.toggleCheckBox(self.view.WarningMessage.imgChecbox);
                if (CommonUtilities.isChecked(self.view.WarningMessage.imgChecbox)) {
                    FormControllerUtility.enableButton(self.view.btnConfirm);
                    CommonUtilities.setCheckboxState(true, self.view.WarningMessage.imgChecbox);
                } else {
                    FormControllerUtility.disableButton(self.view.btnConfirm);
                    CommonUtilities.setCheckboxState(false, self.view.WarningMessage.imgChecbox);
                }
            };
            this.view.WarningMessage.btnTermsAndConditions.onClick = function() {
                self.view.flxTermsAndConditionsPopUp.height = (self.view.flxMain.frame.height + 355) + "dp";
                self.view.flxTC.top = "100dp";
                self.view.flxTermsAndConditionsPopUp.setVisibility(true);
                self.view.flxDialogs.setVisibility(true);
                self.view.lblTermsAndConditions.setFocus(true);
                if (CommonUtilities.isChecked(self.view.WarningMessage.imgChecbox)) {
                    CommonUtilities.setLblCheckboxState(true, self.view.lblTCContentsCheckboxIcon);
                } else {
                    CommonUtilities.setLblCheckboxState(false, self.view.lblTCContentsCheckboxIcon);
                }
                kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.showTermsAndConditionsCancelCard();
            };
            this.view.btnCancel.onClick = function() {
                self.view.flxTermsAndConditionsPopUp.setVisibility(false);
                self.view.flxDialogs.setVisibility(false);
            };
            this.view.flxClose.onClick = function() {
                self.view.flxTermsAndConditionsPopUp.setVisibility(false);
                self.view.flxDialogs.setVisibility(false);
            };
            this.view.flxTCContentsCheckbox.onClick = function() {
                CommonUtilities.toggleFontCheckbox(self.view.lblTCContentsCheckboxIcon);
            };
            this.view.btnSave.onClick = function() {
                if (CommonUtilities.isFontIconChecked(self.view.lblTCContentsCheckboxIcon)) {
                    FormControllerUtility.enableButton(self.view.btnConfirm);
                    CommonUtilities.setCheckboxState(true, self.view.WarningMessage.imgChecbox);
                } else {
                    FormControllerUtility.disableButton(self.view.btnConfirm);
                    CommonUtilities.setCheckboxState(false, self.view.WarningMessage.imgChecbox);
                }
                self.view.flxTermsAndConditionsPopUp.setVisibility(false);
                self.view.flxDialogs.setVisibility(false);
            };
        },
        setUpReplaceCard: function(card) {
            var self = this;
            this.setMobileHeader(kony.i18n.getLocalizedString("i18n.CardManagement.cardReplacement"));
            this.view.lblUpgrade.setVisibility(false);
            this.view.WarningMessage.flxIAgree.setVisibility(false);
            this.view.cardDetails.setData(card);
            var isPrimaryAvailable = false;
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            CommonUtilities.setText(this.view.lblHeading, kony.i18n.getLocalizedString("i18n.CardManagement.cardReplacement"), accessibilityConfig);
            CommonUtilities.setText(this.view.WarningMessage.rtxWarningText1, kony.i18n.getLocalizedString("i18n.CardManagement.replaceCardGuidline1"), accessibilityConfig);
            CommonUtilities.setText(this.view.WarningMessage.rtxWarningText2, kony.i18n.getLocalizedString("i18n.CardManagement.replaceCardGuidline2"), accessibilityConfig);
            CommonUtilities.setText(this.view.WarningMessage.rtxWarningText3, kony.i18n.getLocalizedString("i18n.CardManagement.replaceCardGuidline3"), accessibilityConfig);
            this.view.WarningMessage.flxWarningText4.setVisibility(false);
            this.view.flxAddresslabel.setVisibility(true);
            this.view.flxAddress.setVisibility(true);
            this.view.tbxNoteOptional.text = "";
            this.view.tbxNoteOptional.maxTextLength = OLBConstants.NOTES_LENGTH;
            this.view.lblReason2.setVisibility(true);
            this.view.lbxReason2.setVisibility(true);
            CommonUtilities.setText(this.view.lblReason2, kony.i18n.getLocalizedString("i18n.CardsManagement.replaceReason"), accessibilityConfig);
            var reasonMasterData = [];
            reasonMasterData.push(["Reason1", kony.i18n.getLocalizedString("i18n.CardsManagement.replaceReason1")]);
            reasonMasterData.push(["Reason2", kony.i18n.getLocalizedString("i18n.CardsManagement.damageCard")]);
            reasonMasterData.push(["Reason3", kony.i18n.getLocalizedString("i18n.cardsManagement.others")]);
            this.view.lbxReason2.masterData = reasonMasterData;
            this.view.lbxReason2.selectedKey = "Reason1";
            CommonUtilities.setText(this.view.lblAddress, kony.i18n.getLocalizedString("i18n.CardManagement.addressforcards"), accessibilityConfig);
            var addressObject = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.fetchUserAddresses();
            var addressArray = [];
            for (var addressIndex = 0; addressIndex < 3; addressIndex = addressIndex + 1) { //To reset all the radio buttons to off state.
                this.view["lblAddressCheckBox" + (addressIndex + 1)].text = "L";
                this.view["lblAddressCheckBox" + (addressIndex + 1)].skin = "sknC0C0C020pxolbfonticons";
            }
            if (addressObject) {
                addressObject.forEach(function(dataItem) {
                    addressArray.push(dataItem.AddressLine1 + ", " + dataItem.AddressLine2 + ", " + dataItem.CityName + ", " + dataItem.CountryName + ", " + dataItem.ZipCode);
                });
            }
            if (addressObject) {
                for (var addressIndex = 0; addressIndex < addressObject.length && addressIndex < 3; addressIndex = addressIndex + 1) {
                    if (addressObject[addressIndex].isPrimary == "true") {
                        isPrimaryAvailable = true;
                        this.view["lblAddressCheckBox" + (addressIndex + 1)].text = "M";
                        this.view["lblAddressCheckBox" + (addressIndex + 1)].skin = "sknLblFontTypeIcon3343e820pxMOD";
                        this.view["lblAddressCheckBox" + (addressIndex + 1)].toolTip = kony.i18n.getLocalizedString("i18n.CardManagement.PrimaryAddress");
                    } else {
                        this.view["lblAddressCheckBox" + (addressIndex + 1)].text = "L";
                        this.view["lblAddressCheckBox" + (addressIndex + 1)].skin = "sknC0C0C020pxolbfonticons";
                        this.view["lblAddressCheckBox" + (addressIndex + 1)].toolTip = kony.i18n.getLocalizedString("i18n.CardManagement.SecondaryAddress");
                    }
                }
            }
            if (!isPrimaryAvailable) {
                this.view.lblAddressCheckBox1.text = "M";
                this.view.lblAddressCheckBox1.skin = "sknLblFontTypeIcon3343e820pxMOD";
            }
            if (addressArray[0]) {
                this.view.rtxAddress1.setVisibility(true);
                this.view.flxAddressCheckbox1.setVisibility(true);
                CommonUtilities.setText(this.view.rtxAddress1, addressArray[0], accessibilityConfig);
            } else {
                this.view.flxAddress.setVisibility(false);
                this.view.flxAddresslabel.setVisibility(false);
                this.view.rtxAddress1.setVisibility(false);
                this.view.flxAddressCheckbox1.setVisibility(false);
            }
            if (addressArray[1]) {
                this.view.rtxAddress2.setVisibility(true);
                this.view.flxAddressCheckbox2.setVisibility(true);
                CommonUtilities.setText(this.view.rtxAddress2, addressArray[1], accessibilityConfig);
            } else {
                this.view.flxAddressCheckbox2.setVisibility(false);
                this.view.rtxAddress2.setVisibility(false);
            }
            if (addressArray[2]) {
                this.view.rtxAddress3.setVisibility(true);
                this.view.flxAddressCheckbox3.setVisibility(true);
                CommonUtilities.setText(this.view.rtxAddress3, addressArray[2], accessibilityConfig);
            } else {
                this.view.flxAddressCheckbox3.setVisibility(false);
                this.view.rtxAddress3.setVisibility(false);
            }
            var checkBoxArray = [];
            checkBoxArray.push({
                'flex1': this.view.flxAddressCheckbox1,
                'flex2': this.view.flxAddressCheckbox2,
                'flex3': this.view.flxAddressCheckbox3
            });
            this.view.flxAddressCheckbox1.onTouchEnd = this.onRadioButtonSelection.bind(this, checkBoxArray);
            this.view.flxAddressCheckbox2.onTouchEnd = this.onRadioButtonSelection.bind(this, checkBoxArray);
            this.view.flxAddressCheckbox3.onTouchEnd = this.onRadioButtonSelection.bind(this, checkBoxArray);
            if (CommonUtilities.isCSRMode()) {
                this.view.btnConfirm.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.btnConfirm.skin = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                FormControllerUtility.enableButton(self.view.btnConfirm);
                this.view.btnConfirm.onClick = function() {
                    var params = {
                        'card': card,
                        'userName': kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.getUserName(),
                        'CardAccountNumber': card.maskedCardNumber,
                        'CardAccountName': card.productName,
                        'AccountType': 'CARD',
                        'RequestCode': 'REPLACEMENT',
                        'Channel': OLBConstants.Channel,
                        'Address_id': self.getSelectedAddressId(),
                        'AdditionalNotes': self.view.tbxNoteOptional.text,
                        'reason': self.view.lbxReason2.selectedKeyValue[1]
                    };
                    self.initMFAFlow.call(self, params, kony.i18n.getLocalizedString("i18n.Accounts.ContextualActions.requestReplaceCard"));
                }.bind(this);
            }
        },
        /**
         * Method used to change the radio button selection in the replace card flow.
         * @param {Object} radioButtons - contains the radio buttons list.
         * @param {Object} selectedradioButton - contains the raido button selected widget Object.
         */
        onRadioButtonSelection: function(radioButtons, selectedradioButton) {
            if (selectedradioButton.widgets()["0"].id === "lblAddressCheckBox1") {
                this.view.lblAddressCheckBox1.text = "M";
                this.view.lblAddressCheckBox1.skin = "sknLblFontTypeIcon3343e820pxMOD";
                this.view.lblAddressCheckBox2.text = "L";
                this.view.lblAddressCheckBox2.skin = "sknC0C0C020pxolbfonticons";
                this.view.lblAddressCheckBox3.text = "L";
                this.view.lblAddressCheckBox3.skin = "sknC0C0C020pxolbfonticons";
            }
            if (selectedradioButton.widgets()["0"].id === "lblAddressCheckBox2") {
                this.view.lblAddressCheckBox2.text = "M";
                this.view.lblAddressCheckBox2.skin = "sknLblFontTypeIcon3343e820pxMOD";
                this.view.lblAddressCheckBox1.text = "L";
                this.view.lblAddressCheckBox1.skin = "sknC0C0C020pxolbfonticons";
                this.view.lblAddressCheckBox3.text = "L";
                this.view.lblAddressCheckBox3.skin = "sknC0C0C020pxolbfonticons";
            }
            if (selectedradioButton.widgets()["0"].id === "lblAddressCheckBox3") {
                this.view.lblAddressCheckBox3.text = "M";
                this.view.lblAddressCheckBox3.skin = "sknLblFontTypeIcon3343e820pxMOD";
                this.view.lblAddressCheckBox1.text = "L";
                this.view.lblAddressCheckBox1.skin = "sknC0C0C020pxolbfonticons";
                this.view.lblAddressCheckBox2.text = "L";
                this.view.lblAddressCheckBox2.skin = "sknC0C0C020pxolbfonticons";
            }
        },
        getSelectedAddressId: function() {
            var i = 0;
            var addresses = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.fetchUserAddresses();
            if (this.view.lblAddressCheckBox1.text === "M") i = 0;
            if (this.view.lblAddressCheckBox2.text === "M") i = 1;
            if (this.view.lblAddressCheckBox3.text === "M") i = 2;
            return addresses[i].addressId;
        },
        setUpReportLostCard: function(card) {
            var self = this;
            this.setMobileHeader(kony.i18n.getLocalizedString("i18n.CardManagement.ReportLostOrStolen"));
            this.view.WarningMessage.flxIAgree.setVisibility(false);
            this.view.cardDetails.setData(card);
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            CommonUtilities.setText(this.view.lblHeading, kony.i18n.getLocalizedString("i18n.CardManagement.ReportLostOrStolen"), accessibilityConfig);
            CommonUtilities.setText(this.view.WarningMessage.rtxWarningText1, kony.i18n.getLocalizedString("i18n.CardManagement.ReportLostOrStolenGuideline1"), accessibilityConfig);
            CommonUtilities.setText(this.view.WarningMessage.rtxWarningText2, kony.i18n.getLocalizedString("i18n.CardManagement.ReportLostOrStolenGuideline2"), accessibilityConfig);
            CommonUtilities.setText(this.view.WarningMessage.rtxWarningText3, kony.i18n.getLocalizedString("i18n.CardManagement.ReportLostOrStolenGuideline3"), accessibilityConfig);
            this.view.WarningMessage.flxWarningText4.setVisibility(false);
            this.view.lblReason2.setVisibility(true);
            CommonUtilities.setText(this.view.lblReason2, kony.i18n.getLocalizedString("i18n.CardManagement.PleaseEnterTheReasonMessage"), accessibilityConfig);
            this.view.lbxReason2.setVisibility(true);
            var reasonsMasterData = [];
            reasonsMasterData.push([OLBConstants.CARD_REPORTLOST_REASON.LOST, OLBConstants.CARD_REPORTLOST_REASON.LOST], [OLBConstants.CARD_REPORTLOST_REASON.STOLEN, OLBConstants.CARD_REPORTLOST_REASON.STOLEN]);
            this.view.lbxReason2.masterData = reasonsMasterData;
            this.view.lbxReason2.selectedKey = OLBConstants.CARD_REPORTLOST_REASON.LOST;
            this.view.tbxNoteOptional.text = "";
            this.view.tbxNoteOptional.maxTextLength = OLBConstants.NOTES_LENGTH;
            this.view.lblUpgrade.setVisibility(false);
            this.view.flxAddresslabel.setVisibility(false);
            this.view.flxAddress.setVisibility(false);
            if (CommonUtilities.isCSRMode()) {
                this.view.btnConfirm.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.btnConfirm.skin = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                this.view.btnConfirm.onClick = function() {
                    var params = {
                        'card': card,
                        'Reason': self.view.lbxReason2.selectedKey,
                        'notes': self.view.tbxNoteOptional.text
                    };
                    self.initMFAFlow.call(self, params, kony.i18n.getLocalizedString("i18n.CardManagement.reportedLost"));
                }.bind(this);
            }
        },
        setUpCancelCard: function(card) {
            var self = this;
            this.setMobileHeader(kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard"));
            this.view.cardDetails.setData(card);
            this.view.lblUpgrade.setVisibility(false);
            this.view.flxAddresslabel.setVisibility(false);
            this.view.flxAddress.setVisibility(false);
            FormControllerUtility.disableButton(this.view.btnConfirm);
            CommonUtilities.setCheckboxState(false, this.view.WarningMessage.imgChecbox);
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            CommonUtilities.setText(this.view.lblHeading, kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard"), accessibilityConfig);
            CommonUtilities.setText(this.view.WarningMessage.rtxWarningText1, kony.i18n.getLocalizedString("i18n.cardsManagement.cancelWarn1"), accessibilityConfig);
            CommonUtilities.setText(this.view.WarningMessage.rtxWarningText2, kony.i18n.getLocalizedString("i18n.cardsManagement.cancelWarn2"), accessibilityConfig);
            CommonUtilities.setText(this.view.WarningMessage.rtxWarningText3, kony.i18n.getLocalizedString("i18n.cardsManagement.cancelWarn3"), accessibilityConfig);
            CommonUtilities.setText(this.view.WarningMessage.rtxWarningText4, kony.i18n.getLocalizedString("i18n.cardsManagement.cancelWarn4"), accessibilityConfig);
            this.view.lblReason2.setVisibility(true);
            this.view.lbxReason2.setVisibility(true);
            this.view.WarningMessage.flxIAgree.setVisibility(true);
            CommonUtilities.setText(this.view.lblReason2, kony.i18n.getLocalizedString("i18n.CardManagement.PleaseEnterTheReasonMessage"), accessibilityConfig);
            var reasonMasterData = [];
            reasonMasterData.push(["Reason1", kony.i18n.getLocalizedString("i18n.cardManagement.privacy")]);
            reasonMasterData.push(["Reason2", kony.i18n.getLocalizedString("i18n.cardsManagement.annualCharges")]);
            reasonMasterData.push(["Reason3", kony.i18n.getLocalizedString("i18n.cardsManagement.others")]);
            this.view.lbxReason2.masterData = reasonMasterData;
            this.view.lbxReason2.selectedKey = "Reason1";
            if (CommonUtilities.isCSRMode()) {
                this.view.btnConfirm.onClick = CommonUtilities.disableButtonActionForCSRMode();
                this.view.btnConfirm.skin = CommonUtilities.disableButtonSkinForCSRMode();
            } else {
                this.view.btnConfirm.onClick = function() {
                    var params = {
                        'card': card,
                        'Action': 'Cancel Card',
                        'Reason': self.view.lbxReason2.selectedKeyValue[1],
                        'notes': ""
                    };
                    self.initMFAFlow.call(self, params, kony.i18n.getLocalizedString("i18n.cardsManagement.cancelCard"));
                }.bind(this);
            }
        },
        initMFAFlow: function(params, action) {
            CommonUtilities.showProgressBar(this.view);
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardManagementModuleNew").presentationController.verifySecureAccessCodeSuccess(params, action);
        },
        showTermsAndConditionsSuccessScreenCancelCard: function(TnCcontent) {
            CommonUtilities.disableButton(this.view.btnConfirm);
            this.view.WarningMessage.flxIAgree.setVisibility(true);
            if (TnCcontent.contentTypeId === OLBConstants.TERMS_AND_CONDITIONS_URL) {
                this.view.WarningMessage.btnTermsAndConditions.onClick = function() {
                    window.open(TnCcontent.termsAndConditionsContent);
                }
            } else {
                this.setTnCDATASection(TnCcontent.termsAndConditionsContent);
            }
            this.view.flxClose.onClick = this.hideTermsAndConditionPopUp;
        },
        hideTermsAndConditionPopUp: function() {
            this.view.flxTermsAndConditionsPopUp.setVisibility(false);
            this.view.flxDialogs.setVisibility(false);
            document.getElementById("iframe_brwBodyTnC").contentWindow.document.getElementById("viewer").innerHTML = "";
        },
        setMobileHeader: function(text) {
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            if (responsiveUtils.isMobile) {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, text, accessibilityConfig);
            } else {
                CommonUtilities.setText(this.view.customheadernew.lblHeaderMobile, "", accessibilityConfig);
            }
        },
        /**
         *  Method to show error flex.
         * @param {String} - Error message to be displayed.
         */
        showServerError: function(errorMsg) {
            var accessibilityConfig = CommonUtilities.getaccessibilityConfig();
            this.view.flxDowntimeWarning.setVisibility(true);
            if (errorMsg.errorMessage && errorMsg.errorMessage != "") {
                CommonUtilities.setText(this.view.rtxDowntimeWarning, errorMsg.errorMessage, accessibilityConfig);
            } else {
                CommonUtilities.setText(this.view.rtxDowntimeWarning, errorMsg, accessibilityConfig);
            }
            this.view.rtxDowntimeWarning.setFocus(true);
        },
        /**
         * Method to hide error flex.
         */
        hideServerError: function() {
            this.view.flxDowntimeWarning.setVisibility(false);
        },
    };
});