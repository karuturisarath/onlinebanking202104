define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onClick defined for btnSearchPayee **/
    AS_Button_cfaaa40f8f5e4165bd5a8c26fa2156b7: function AS_Button_cfaaa40f8f5e4165bd5a8c26fa2156b7(eventobject) {
        var self = this;
        this.showFlxSearchPayee();
    },
    /** onClick defined for btnEnterPayeeInfo **/
    AS_Button_ad65eb7bf90343dcb8171e30616cbf58: function AS_Button_ad65eb7bf90343dcb8171e30616cbf58(eventobject) {
        var self = this;
        this.showFlxEnterPayeeInfo();
    },
    /** onRowClick defined for segPayeesName **/
    AS_Segment_b222580ae725475ba46a84bdba3e19b6: function AS_Segment_b222580ae725475ba46a84bdba3e19b6(eventobject, sectionNumber, rowNumber) {
        var self = this;
        this.selectPayeeName();
    },
    /** onClick defined for btnEnterManually **/
    AS_Button_c7fa3675ce074438b222094748ef6381: function AS_Button_c7fa3675ce074438b222094748ef6381(eventobject) {
        var self = this;
        this.showFlxEnterPayeeInfo();
    },
    /** onClick defined for btnResetPayeeInfo2 **/
    AS_Button_j492643e92cd4884a15045fb5a62fd8d: function AS_Button_j492643e92cd4884a15045fb5a62fd8d(eventobject) {
        var self = this;
        this.resetPayeeInformation();
    },
    /** onClick defined for btnNext2 **/
    AS_Button_c76755d388d44f3fb9d0f8331133a976: function AS_Button_c76755d388d44f3fb9d0f8331133a976(eventobject) {
        var self = this;
        this.goToSerchedBillerDetails();
    },
    /** onClick defined for flxClick **/
    AS_FlexContainer_f6470d9ef6f644448d1005e7cb8e3580: function AS_FlexContainer_f6470d9ef6f644448d1005e7cb8e3580(eventobject) {
        var self = this;
        this.rememberThis();
    },
    /** onClick defined for btnResetPayeeInfo **/
    AS_Button_d3b373073e25498e9ddbfd4817ec3a79: function AS_Button_d3b373073e25498e9ddbfd4817ec3a79(eventobject) {
        var self = this;
        this.resetPayeeInformation();
    },
    /** onClick defined for btnNext **/
    AS_Button_c4aefce0b6d2447da24db67181b4ea59: function AS_Button_c4aefce0b6d2447da24db67181b4ea59(eventobject) {
        var self = this;
        //this.showflxAddBillerDetailsInformation();
        this.goToAddBillerDetails();
    },
    /** onClick defined for btnViewPayActivities **/
    AS_Button_e28aed8da45b4803b1176647cbcfa59c: function AS_Button_e28aed8da45b4803b1176647cbcfa59c(eventobject) {
        var self = this;
        this.navigateToBillPayHistoryTab();
    },
    /** onClick defined for btnCancel **/
    AS_Button_f6a72cf3f8f5445ca6afb9223c694d3d: function AS_Button_f6a72cf3f8f5445ca6afb9223c694d3d(eventobject) {
        var self = this;
        this.showCancelPopup();
    },
    /** onClick defined for btnModify **/
    AS_Button_idc4ba21fd584b1292a125b5abdbc908: function AS_Button_idc4ba21fd584b1292a125b5abdbc908(eventobject) {
        var self = this;
        this.showMainWrapper();
    },
    /** onClick defined for btnConfirm **/
    AS_Button_j68fa86ee4584bfc82ef3208d65f336b: function AS_Button_j68fa86ee4584bfc82ef3208d65f336b(eventobject) {
        var self = this;
        //this.showFlxAddPayeeAck();
        this.goToAddPayeeAcknowledgement();
    },
    /** onClick defined for btnDetailsCancel **/
    AS_Button_c1a7542a12e54a7d8bffc9acc7031554: function AS_Button_c1a7542a12e54a7d8bffc9acc7031554(eventobject) {
        var self = this;
        this.showCancelPopup();
    },
    /** onClick defined for btnDetailsBack **/
    AS_Button_a025ccca7f214a0fba894cc4d313eb08: function AS_Button_a025ccca7f214a0fba894cc4d313eb08(eventobject) {
        var self = this;
        this.showMainWrapper();
    },
    /** onClick defined for btnDetailsConfirm **/
    AS_Button_fb3e71efb4354769a751443f06bcbd4d: function AS_Button_fb3e71efb4354769a751443f06bcbd4d(eventobject) {
        var self = this;
        //this.showFlxVerifyPayeeInformation();
        this.goToConfirmPayeeDetails();
    },
    /** onClick defined for btnViewAllPayees **/
    AS_Button_e11613a5b95e4545977208b7bc27bfe0: function AS_Button_e11613a5b95e4545977208b7bc27bfe0(eventobject) {
        var self = this;
        this.viewallpayeesbtnaction();
    },
    /** onClick defined for btnMakeBillPay **/
    AS_Button_e144fb208b124f19855d502845d9bb6f: function AS_Button_e144fb208b124f19855d502845d9bb6f(eventobject) {
        var self = this;
        var self = this;
        this.presenter.makePayment();
    },
    /** onClick defined for btnYes **/
    AS_Button_ia36577772ca4e88bb4f2efdefa62149: function AS_Button_ia36577772ca4e88bb4f2efdefa62149(eventobject) {
        var self = this;
        var obj1 = {
            "tabname": "payees"
        };
        applicationManager.getNavigationManager().navigateTo("frmBillPay");
    },
    /** onClick defined for btnNo **/
    AS_Button_e5c4090ca2bc4c0194a457a7cd6d93c8: function AS_Button_e5c4090ca2bc4c0194a457a7cd6d93c8(eventobject) {
        var self = this;
        this.hideCancelPopup();
    },
    /** onClick defined for flxCross **/
    AS_FlexContainer_d6ce4657c00049ebb3e9f1d86f824c79: function AS_FlexContainer_d6ce4657c00049ebb3e9f1d86f824c79(eventobject) {
        var self = this;
        this.hideCancelPopup();
    },
    /** preShow defined for frmAddPayee **/
    AS_Form_f4f339585fcf453b8d40ec04e56098ae: function AS_Form_f4f339585fcf453b8d40ec04e56098ae(eventobject) {
        var self = this;
        this.alterBreadcrumb();
        //this.preshowFrmAddPayee();
    },
    /** postShow defined for frmAddPayee **/
    AS_Form_b8da2250420344f28de685fb8c76e84c: function AS_Form_b8da2250420344f28de685fb8c76e84c(eventobject) {
        var self = this;
        this.postShowAddPayee();
    },
    /** onTouchEnd defined for frmAddPayee **/
    AS_Form_j64eab3f72284373a6975594b47b2b64: function AS_Form_j64eab3f72284373a6975594b47b2b64(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});