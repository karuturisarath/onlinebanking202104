define(['commonUtilities', 'FormControllerUtility', 'OLBConstants', 'ViewConstants'], function(commonUtilities, FormControllerUtility, OLBConstants, ViewConstants) {
    var responsiveUtils = new ResponsiveUtils();
    return /** @alias module:frmNotificationsAndMessagesController */ {
        isDeletedTab: false,
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.view.flxFormContent.doLayout = this.sesetMinHeight;
        },
        preShow: function() {
            var scopeObj = this;
            this.setActions();
            FormControllerUtility.showProgressBar(scopeObj.view);
            scopeObj.loadsMessagesNewModule().presentationController.showRequests({});
            this.view.flxReply.isVisible = false;
            this.view.flxMainMessages.bottom = "80dp";
            this.view.btnSendReply.toolTip = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.replyButton");
            this.view.btnCancelReply.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Cancel");
            if (commonUtilities.isCSRMode()) {
                this.view.btnSendReply.onClick = FormControllerUtility.disableButtonActionForCSRMode();
                this.view.btnSendReply.skin = FormControllerUtility.disableButtonSkinForCSRMode();
                this.view.btnSendReply.focusSkin = FormControllerUtility.disableButtonSkinForCSRMode();
            }
            this.view.CustomPopup1.flxCross.onCl
        },
        postShow: function() {
            this.setMinHeight();
        },
        loadsMessagesNewModule: function() {
            return kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("MessagesModuleNew");
        },
        screenHeightGbl: 0,
        setMinHeight: function() {
            if (this.screenHeightGbl === kony.os.deviceInfo().screenHeight) {
                return;
            }
            this.screenHeightGbl = kony.os.deviceInfo().screenHeight;
            var minHeightForMsg = 0;
            // screenheight - headerheight - footer height - address top - address bottom
            if (responsiveUtils.isMobile || kony.application.getCurrentBreakpoint() === 640) {
                this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - 200 + "dp";
            } else {
                this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - 270 + "dp"; //270 = header 120 + footer 150
                minHeightForMsg = kony.os.deviceInfo().screenHeight - 270;
                if (minHeightForMsg < 700) {
                    minHeightForMsg = 700;
                }
                this.view.flxContainer.minHeight = minHeightForMsg + "dp";
            }
        },
        updateFormUI: function(context) {
            if (context.showProgressBar) {
                FormControllerUtility.showProgressBar(this.view);
            }
            if (context.hideProgressBar) {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (context.showRequestsView) {
                this.showRequestsView(context.showRequestsView);
            }
            if (context.unreadMessagesCountView) {
                this.showUnreadMessagesCount(context.unreadMessagesCountView);
            }
            if (context.showMessagesView) {
                this.showMessagesView(context.showMessagesView);
            }
            if (context.updateMessageAsReadSuccessView) {
                this.updateMessageAsReadSuccessView(context.updateMessageAsReadSuccessView.readCount);
            }
            if (context.inFormError) {
                this.view.rtxMessageWarning.text = context.inFormError.errorMessage;
                this.view.flxWarning.setVisibility(true);
            }
            if (context.createNewRequestOrMessagesView) {
                this.showNewMessage(context.createNewRequestOrMessagesView);
            }
            if (context.createNewMessageError) {
                this.showNewMessageCreationError(context.createNewMessageError);
            }
            if (context.searchRequestsView) {
                this.showSearchRequestsView(context.searchRequestsView);
            }
        },
        setActions: function() {
            this.view.btnAlerts.onClick = this.navToAlerts;
            this.view.btnDeletedMessages.onClick = this.navToDeletedMessages;
            this.view.btnNewMessage.onClick = this.navToNewMessage;
            this.view.txtSearch.onKeyUp = this.showOrHideSearchCrossImage;
            this.view.txtSearch.onDone = this.OnMessageSearchClick.bind(this);
            this.view.flxSearchIcon.onClick = this.OnMessageSearchClick.bind(this);
            this.view.lblSearch.onTouchEnd = this.OnMessageSearchClick.bind(this);
            this.view.flxClearSearch.onClick = this.OnMessageSearchClick.bind(this, true);
            this.view.flxDelete.onClick = this.deleteMessage;
            this.view.txtAreaReply.onKeyUp = this.enableSendReplyButton.bind(this);
            this.view.btnCancelReply.onClick = this.hideReplyView;
            this.view.btnReply.onClick = this.showReplyView;
        },
        navToAlerts: function() {
            this.loadsMessagesNewModule().presentationController.showAlertsPage();
        },
        navToNewMessage: function() {
            this.loadsMessagesNewModule().presentationController.showNewMessagePage();
        },
        navToDeletedMessages: function() {
            this.loadsMessagesNewModule().presentationController.showDeletedMessagesPage();
        },
        showNewMessage: function() {
            this.navToNewMessage();
        },
        showOrHideSearchCrossImage: function() {
            var searchString = this.view.txtSearch.text;
            if (searchString && searchString.trim()) {
                this.view.flxClearSearch.setVisibility(true);
            } else {
                this.view.flxClearSearch.setVisibility(false);
            }
            this.view.forceLayout();
        },
        OnMessageSearchClick: function(clearSearch) {
            FormControllerUtility.showProgressBar(this.view);
            var searchString = this.view.txtSearch.text;
            if (clearSearch === true) {
                this.view.txtSearch.text = "";
                this.view.lblSearch.setVisibility(true);
                this.loadsMessagesNewModule().presentationController.showRequests();
                this.showOrHideSearchCrossImage();
            } else if (searchString && searchString.trim()) {
                this.view.lblSearch.setVisibility(true);
                this.view.flxClearSearch.setVisibility(false);
                this.loadsMessagesNewModule().presentationController.searchRequest(searchString);
                this.view.txtSearch.text = searchString;
                this.showOrHideSearchCrossImage();
            } else {
                this.loadsMessagesNewModule().presentationController.showRequests();
            }
        },
        //hideshow NOSearchResults
        /**
         * This function is used to display the unread messages Count in the "My Messages" tab
         *@param{object} unReadMsgsCountData -  consists of the unreadMessagesCount which is to be displayed
         */
        showUnreadMessagesCount: function(unReadMsgsCountData) {
            this.unreadMessagesCount = parseInt(unReadMsgsCountData.unReadMessagesCount);
            this.view.btnMyMessages.text = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Messages") + " (" + this.unreadMessagesCount + ")";
            this.updateAlertsIcon();
            this.view.flxMessagesMain.parent.forceLayout();
        },
        /**
         * updateAlertsIcon :This function is used to update the red Dot on the Alerts icon depending on whether there are any unread Messages/Notifications
         */
        updateAlertsIcon: function() {
            if (this.unreadMessagesCount !== undefined && this.unReadNotificationCount !== undefined) {
                applicationManager.getConfigurationManager().setUnreadMessageCount({
                    count: parseInt(this.unreadMessagesCount) + parseInt(this.unReadNotificationCount)
                });
                this.view.customheadernew.updateAlertIcon();
            }
        },
        /**
         * This function is executed when all  the messages for the request are retrieved
         *@param{object}  messagesData - is a JSON which consists of array of all the messsages for the particular request
         */
        showMessagesView: function(messagesData) {
            var widgetDataMap = this.processMessagesDataMap();
            var message = this.processMessagesData(messagesData.messages);
            this.view.segMessages.widgetDataMap = widgetDataMap;
            this.view.segMessages.setData(message);
            this.view.AllForms.setVisibility(false);
            this.view.flxMessagesMain.parent.forceLayout();
        },
        /**
         * This function is executed when the messages are  marked as read
         *@param{number}  readCount - which represents the number of messages read
         */
        updateMessageAsReadSuccessView: function(readCount) {
            this.unreadMessagesCount = this.unreadMessagesCount - parseInt(readCount);
            this.view.btnMyMessages.text = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Messages") + " (" + this.unreadMessagesCount + ")";
            if (this.unReadNotificationCount === 0 && this.unreadMessagesCount === 0) {
                this.view.customheadernew.lblNewMessages.isVisible = false;
            } else {
                this.view.customheadernew.lblNewMessages.isVisible = true;
            }
            this.view.flxMessagesMain.parent.forceLayout();
        },
        /**
         * This function is used to bind the search Result of the Requests to the Segment
         *@param{object} searchResult-is a JSON which  contains data of the Requests according to the Search String
         */
        showSearchRequestsView: function(searchResult) {
            var requests = searchResult.requests;
            if (requests.length === 0) {
                this.view.rtxNoSearchResults.text = kony.i18n.getLocalizedString("i18n.LocateUs.NosearchresultfoundPleasechangethesearchcriteria");
                this.view.flxNoSearchResult.isVisible = true;
                this.view.lblAccountRenewal.text = "";
                this.view.lblRequestIdValue.text = "";
                this.view.lblCategoryValue.text = "";
                this.view.lblCategoryValue2.text = "";
                this.view.segMessages.setData([]);
                this.view.segMessageAndNotification.setData([]);
                this.view.flxDelete.isVisible = false;
                this.view.btnSendReply.isVisible = false;
                this.view.btnCancelReply.setVisibility(false);
                this.view.lblWarningNotMoreThan5files.setVisibility(false);
                this.view.flxReplyImageAttachment.setVisibility(false);
                this.view.segMessageAndNotification.isVisible = false;
                FormControllerUtility.hideProgressBar(this.view);
            } else {
                this.unreadMessagesCount = parseInt(searchResult.unreadSearchMessagesCount);
                this.view.flxDelete.isVisible = true;
                this.view.flxNoSearchResult.isVisible = false;
                this.view.segMessageAndNotification.isVisible = true;
                this.view.btnMyMessages.text = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Messages") + " (" + this.unreadMessagesCount + ")";
                this.setMessagesSegmentData(searchResult);
            }
        },
        /**
         * processRequestsDataMap :This function is used for binding the each individual Request Data to the Segment fields
         * @returns {object} widget
         */
        processRequestsDataMap: function() {
            return {
                "flxNotificationsAndMessages": "flxNotificationsAndMessages",
                "imgAttachment": "imgAttachment",
                "imgBulletIcon": "imgBulletIcon",
                "imgCurrentOne": "imgCurrentOne",
                "lblDateAndTime": "lblDateAndTime",
                "lblSegDescription": "lblSegDescription",
                "lblSegHeading": "lblSegHeading",
                "lblRowSeperator": "lblRowSeperator",
                "flxArrow": "flxArrow",
                "segNotificationsAndMessages": "segNotificationsAndMessages",
                "lblRequestIdValue": "lblRequestIdValue",
                "lblCategoryValue": "lblCategoryValue"
            };
        },

        /**
         * processRequestsData :This function is used for assigning  individual Request Data to the Segment
         *@param{data}  data consists of all the details of the  requests which are to be mapped
         * @return {object} data
         */
        processRequestsData: function(data) {
            var self = this;
            if (!data) {
                return [];
            }
            data = data.map(function(dataItem) {
                return {
                    "imgCurrentOne": {
                        "src": ViewConstants.IMAGES.ACCOUNTS_SIDEBAR_BLUE,
                        "isVisible": false
                    },
                    "imgAttachment": { //if it has attachments depending on the data
                        "src": ViewConstants.IMAGES.ATTACHMENT_GREY,
                        "isVisible": dataItem.totalAttachments > 0 ? true : false
                    },
                    "imgBulletIcon": {
                        "isVisible": self.isReadMessage(dataItem.unreadmsgs)
                    },
                    "lblRowSeperator": {
                        "isVisible": true
                    },
                    "lblArrow": {
                        "skin": "sknLblrightArrowFontIcon0273E3",
                        "isVisible": true
                    },
                    "lblDateAndTime": {
                        "text": dataItem.recentMsgDate ? commonUtilities.getDateAndTime(dataItem.recentMsgDate) : ""
                    },
                    "lblSegDescription": {
                        "text": self.labelData(dataItem.firstMessage) ? self.labelData(dataItem.firstMessage) : ""
                    },
                    "flxNotificationsAndMessages": {
                        "skin": "sknFlxffffff",
                        "hoverSkin": ViewConstants.SKINS.SKNFLXF7F7F7
                    },
                    "lblSegHeading": {
                        "text": dataItem.unreadmsgs > 0 ? dataItem.requestsubject + " (" + dataItem.unreadmsgs + ")" : dataItem.requestsubject
                    },
                    "lblRequestIdValue": {
                        "text": dataItem.id
                    },
                    "lblCategoryValue": {
                        "text": dataItem.requestcategory_id
                    },
                    "requestId": dataItem.id,
                    "softdeleteflag": dataItem.softdeleteflag
                };
            });
            return data;
        },
        /**
         * This function is used to bind the Messages/Requests Data to the Segment and update view accordingly.
         *@param{object} requestsData-is a JSON which  contains data of the Requests/Messages and unread messages count.
         */
        setMessagesSegmentData: function(requestsData) {
            var requests = requestsData.requests; //Messages/Requests
            this.view.AllForms.setVisibility(false);
            this.view.segMessageAndNotification.onRowClick = this.onMessageRowSelection;
            if (requests && requests.length > 0) {
                this.view.flxRightMessages.flxMessagesHeader.flxDelete.setVisibility(true);
                this.view.segMessageAndNotification.setVisibility(true);
                this.view.flxNoSearchResult.setVisibility(false);
                var dataMap = this.processRequestsDataMap();
                requests = this.processRequestsData(requests);
                this.view.segMessageAndNotification.widgetDataMap = dataMap;
                this.view.segMessageAndNotification.setData(requests);
                if (requestsData.selectedRequestId) {
                    //set the selected Reqeuest in the segment
                    var index;
                    requests.forEach(function(dataItem, i) {
                        if (dataItem.requestId === requestsData.selectedRequestId) {
                            index = i;
                            return;
                        }
                    });
                    if (index >= 0) {
                        this.view.segMessageAndNotification.selectedIndex = [0, index];
                    }
                }
                if (!requestsData.createNewMessage) {
                    this.isDeletedTab = false;
                    this.onMessageRowSelection();
                    this.view.segMessageAndNotification.setVisibility(true);
                    var break_point = kony.application.getCurrentBreakpoint();
                    if (break_point === 640 || responsiveUtils.isMobile) {
                        this.view.flxRightMessages.setVisibility(false);
                    } else {
                        this.view.flxRightMessages.setVisibility(true);
                    }
                }
            } else {
                this.view.flxRightMessages.setVisibility(false);
                this.view.flxRightMessages.flxMessagesHeader.flxDelete.setVisibility(false);
                this.view.segMessageAndNotification.setVisibility(false);
                this.view.flxTabs.isVisible = true;
                if (requestsData.errorMsg) {
                    this.view.rtxNoSearchResults.text = requestsData.errorMsg;
                } else {
                    this.view.rtxNoSearchResults.text = kony.i18n.getLocalizedString("i18n.AccountsLanding.NoMessagesAtTheMoment");
                }
                this.view.flxNoSearchResult.setVisibility(true);
                this.view.flxVerticalSeparator.setVisibility(true);
                if (!requestsData.createNewMessage) {
                    FormControllerUtility.hideProgressBar(this.view);
                }
            }
            this.view.flxMessagesMain.parent.forceLayout();
        },
        /**
         * This function is executed when we want to display the complete details of the Request selected
         */
        onMessageRowSelection: function() {
            var self = this;
            var isSegClicked = false;
            FormControllerUtility.showProgressBar(self.view);
            self.hideReplyView();
            this.view.lblAccountRenewal.text = "";
            this.view.lblRequestIdValue.text = "";
            this.view.lblCategoryValue.text = "";
            this.view.lblCategoryValue2.text = "";
            this.view.segMessages.setData([]);
            if (this.isDeletedTab) {
                this.view.flxDelete.setVisibility(false);
            } else {
                this.view.flxDelete.setVisibility(true);
            }
            var index = 0;
            var data = this.view.segMessageAndNotification.data;
            if (this.view.segMessageAndNotification.selectedRowItems.length) {
                index = this.view.segMessageAndNotification.selectedRowIndex[1];
                isSegClicked = true;
            }
            var selectedRow = data[index];
            if (selectedRow.lblCategoryValue.text === "Dispute Transacation") {
                this.view.flxDelete.setVisibility(false);
            }
            if (!data[0].requestId) {
                data.splice(0, 1);
                index = index - 1;
                if (index === -1) {
                    index = 0;
                    selectedRow = data[0];
                    if (!selectedRow) {
                        //If there are no messages
                        this.hideCreateMessageView();
                        FormControllerUtility.hideProgressBar(this.view);
                        return;
                    }
                }
                this.view.segMessageAndNotification.setData(data);
            }
            var response = this.loadsMessagesNewModule().presentationController.getRequestsDetails(selectedRow.requestId, this.isDeletedTab);
            var prevIndex = 0;
            for (var i = 0; i < data.length; i++) {
                if (data[i].imgCurrentOne.isVisible) {
                    prevIndex = i;
                }
                data[i].flxNotificationsAndMessages = {
                    "skin": ViewConstants.SKINS.SKNFLXFFFFFF
                };
                data[i].imgCurrentOne.isVisible = false;
                data[i].imgBulletIcon.isVisible = false;
            }
            data[index].flxNotificationsAndMessages = {
                "skin": ViewConstants.SKINS.SKNFLXF7F7F7
            };
            data[index].lblArrow.isVisible = true;
            data[index].imgCurrentOne.isVisible = true;
            if (response && this.isReadMessage(response.unreadmsgs)) {
                var subject = data[index].lblSegHeading.text;
                data[index].lblSegHeading.text = subject.lastIndexOf(" \(") >= 0 ? subject.substr(0, subject.indexOf(" \(")) : subject;
                this.loadsMessagesNewModule().presentationController.updateMessageAsRead(data[index].requestId);
            }
            this.view.segMessageAndNotification.setDataAt(data[index], index);
            this.view.segMessageAndNotification.setDataAt(data[prevIndex], prevIndex);
            this.view.lblAccountRenewal.text = data[index].lblSegHeading.text;
            this.view.lblRequestIdValue.text = data[index].lblRequestIdValue.text;
            this.view.lblCategoryValue.text = data[index].lblCategoryValue.text;
            this.view.lblCategoryValue2.text = data[index].lblCategoryValue.text;
            if (selectedRow.softdeleteflag === "true") {
                if (commonUtilities.isCSRMode()) {
                    this.view.CustomPopup1.btnYes.onClick = FormControllerUtility.disableButtonActionForCSRMode();
                    this.view.CustomPopup1.btnYes.skin = FormControllerUtility.disableButtonSkinForCSRMode();
                } else {
                    this.view.CustomPopup1.btnYes.onClick = function() {
                        self.closeDismissPopup();
                        FormControllerUtility.showProgressBar(self.view);
                        self.loadsMessagesNewModule().presentationController.hardDeleteRequest(selectedRow.requestId);
                    };
                }
                this.view.CustomPopup1.btnNo.onClick = function() {
                    self.closeDismissPopup();
                };
                this.view.CustomPopup1.flxCross.onClick = function() {
                    self.closeDismissPopup();
                };
            } else {
                if (commonUtilities.isCSRMode()) {
                    this.view.CustomPopup1.btnYes.onClick = FormControllerUtility.disableButtonActionForCSRMode();
                    this.view.CustomPopup1.btnYes.skin = FormControllerUtility.disableButtonSkinForCSRMode();
                } else {
                    this.view.CustomPopup1.btnYes.onClick = function() {
                        self.closeDismissPopup();
                        FormControllerUtility.showProgressBar(self.view);
                        self.loadsMessagesNewModule().presentationController.softDeleteRequest(selectedRow.requestId);
                    };
                }
                this.view.CustomPopup1.btnNo.onClick = function() {
                    self.closeDismissPopup();
                };
                this.view.CustomPopup1.flxCross.onClick = function() {
                    self.closeDismissPopup();
                };
            }

            var break_point = kony.application.getCurrentBreakpoint();
            if (break_point === 640 || responsiveUtils.isMobile) {
                if (this.deletedflag === 1) {
                    this.deletedflag = 0;
                }
                if (isSegClicked) {
                    this.navToMessagesDetailsMobile({
                        show: "MessageDetail"
                    }, response);
                    this.loadsMessagesNewModule().presentationController.showMessages(selectedRow.requestId);
                }
            } else {
                this.loadsMessagesNewModule().presentationController.showMessages(selectedRow.requestId);
                this.view.segMessageAndNotification.setVisibility(true);
                this.view.flxTabs.setVisibility(true);
            }
            FormControllerUtility.hideProgressBar(this.view);
        },
        navToMessagesDetailsMobile: function(context, response) {
            this.loadsMessagesNewModule().presentationController.showMessageDetailsMobilePage(context, response);
        },
        /**
         * This function is exeecuted when the retrieval of the requests is Success which inturn calls the setMessagesSegmentData function which is used to bind the Requests to the Segment
         *@param{object} requestsData - is a JSON which  contains data of the Requests/Messages and un read messages count etc.
         *@param{object[]} requestsData.requests - array of requests/messages object.
         *@param{string} requestsData.unReadMessagesCount - un read request/messages count.
         *@param{string} [requestsData.selectedRequestId] - selected request id if any.
         *@param{string} [requestsData.createNewMessage] - create new message is required or not.
         */
        showRequestsView: function(requestsData) {
            this.view.segMessageAndNotification.setData([]);
            this.view.customheadernew.activateMenu("ALERTS AND MESSAGES", "My Messages");
            this.unreadMessagesCount = parseInt(requestsData.unReadMessagesCount);
            this.view.flxNoSearchResult.isVisible = false;
            this.view.flxReplyButton.isVisible = true;
            this.view.segMessageAndNotification.isVisible = true;
            this.view.btnMyMessages.text = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Messages") + " (" + this.unreadMessagesCount + ")";
            this.view.lblSearch.onTouchEnd = this.OnMessageSearchClick.bind(this);
            this.view.flxClearSearch.onClick = this.OnMessageSearchClick.bind(this, true);
            this.view.txtSearch.onDone = this.OnMessageSearchClick.bind(this);
            this.view.txtSearch.onKeyUp = this.showOrHideSearchCrossImage.bind(this);
            var searchString = this.view.txtSearch.text;
            if (searchString && searchString.trim()) {
                this.OnMessageSearchClick();
            } else {
                this.setMessagesSegmentData(requestsData);
            }
        },
        /**
         * showReplyView :This function is used to show the reply view once we click on Reply button
         */
        showReplyView: function() {
            var self = this;
            FormControllerUtility.disableButton(this.view.btnSendReply);
            this.view.lblWarningNotMoreThan5files.setVisibility(false);
            this.view.flxReplyButton.isVisible = false;
            this.view.flxReply.isVisible = true;
            this.view.flxMainMessages.bottom = "300dp";
            this.view.txtAreaReply.onKeyUp = this.enableSendReplyButton.bind(this);
            this.view.segAttachment.isVisible = true;
            if (kony.application.getCurrentBreakpoint() == 640 || responsiveUtils.isMobile) {
                //         this.view.flxReplyMessageButtons.isVisible = true;
                //         this.view.flxReplyImageAttachment.onClick = this.replyBrowseFiles.bind(this);
                //         this.view.lblWarningNotMoreThan5files.text = "";
                //         this.view.flxReplyImageAttachment.setVisibility(false);
                //         this.view.lblAttatchment.setVisibility(false);
                //Nothing
            }
            this.view.txtAreaReply.text = "";
            this.fileObject = [];
            this.view.segAttachment.data = [];
            this.view.flxReplyImageAttachment.onClick = this.replyBrowseFiles.bind(this);
            this.view.btnCancelReply.onClick = this.hideReplyView.bind(this);
            this.view.btnSendReply.onClick = function() {
                //if(!self.view.segMessages.data[0])return;
                if (kony.application.getCurrentBreakpoint() === 640 || responsiveUtils.isMobile)
                    self.view.lblWarningNotMoreThan5files.text = "";
                FormControllerUtility.showProgressBar(self.view);
                var requestParam = {
                    "files": self.fileObject,
                    "requestid": self.view.segMessages.data[0].requestId,
                    "description": self.view.txtAreaReply.text
                };
                self.loadsMessagesNewModule().presentationController.createNewRequestOrMessage(requestParam);
            };
            this.view.forceLayout();
            //this.view.flxReplyHeader.setFocus(true);
            this.view.txtAreaReply.setFocus(true);
        },
        /**
         * hideReplyView :This function is used to hide the reply view once we click on ReplyCancel button
         */
        hideReplyView: function() {
            this.view.flxReplyButton.isVisible = true;
            this.view.flxReply.isVisible = false;
            this.view.flxMainMessages.bottom = "80dp";
            FormControllerUtility.enableButton(this.view.btnSendReply);
            if (kony.application.getCurrentBreakpoint() === 640 || responsiveUtils.isMobile) {
                this.view.flxReplyMessageButtons.isVisible = false;
            }
            this.view.btnSendReply.onClick = this.showReplyView.bind(this);
            this.view.flxReplyButton.isVisible = true;
            this.view.forceLayout();
        },
        /**
         * enableSendReplyButton :This function is used to enable/Disable the reply button while replying to the existing request
         */
        enableSendReplyButton: function() {
            if (this.view.txtAreaReply.text.trim() === "" || commonUtilities.isCSRMode()) {
                FormControllerUtility.disableButton(this.view.btnSendReply);
            } else {
                FormControllerUtility.enableButton(this.view.btnSendReply);
            }
        },
        deleteMessage: function() {
            this.view.flxDialogs.isVisible = true;
            this.view.flxLogout.isVisible = false;
            this.view.FlxDismiss.isVisible = true;

        },
        /**
         * isReadMessage :This function checks whether there are any unread Messages
         * @param {boolean} status indicates the number of unread messages
         * @returns {boolean} status
         */
        isReadMessage: function(status) {
            if (status > 0) {
                return true;
            } else {
                return false;
            }
        },
        /**
         * labelData :This function is used to convert the html Data to normal String by removing the tags and assigning the text to a label
         * @param {object} data  html Data
         *@ return {text} normal text after converting from html to text
         */
        labelData: function(data) {
            var parser = new DOMParser();
            var res = parser.parseFromString(data, 'text/html');
            var html = res.body.textContent;
            var div = document.createElement("div");
            div.innerHTML = html;
            return div.innerText;
        },
        /**
         * processMessagesDataMap :This function is used for binding the each individual messages Data to the Segment fields
         * @returns {object} data
         */
        processMessagesDataMap: function() {
            return {
                "flxDummy": "flxDummy",
                "flxMessage": "flxMessage",
                "flxNameDate": "flxNameDate",
                "imgToolTip": "imgToolTip",
                "imgUser": "imgUser",
                "lblDate": "lblDate",
                "lblDummy": "lblDummy",
                "rtxMessage": "rtxMessage",
                "flxDocAttachment1": "flxDocAttachment1",
                "flxAttachmentName1": "flxAttachmentName1",
                "imgPDF1": "imgPDF1",
                "lblDocName1": "lblDocName1",
                "lblSize1": "lblSize1",
                "flxVerticalMiniSeparator1": "flxVerticalMiniSeparator1",
                "imgRemoveAttachment1": "imgRemoveAttachment1",
                "flxRemoveAttachment1": "flxRemoveAttachment1",
                "flxDownloadAttachment1": "flxDownloadAttachment1",
                "imgDownloadAttachment1": "imgDownloadAttachment1",
                "flxDocAttachment2": "flxDocAttachment2",
                "imgPDF2": "imgPDF2",
                "lblDocName2": "lblDocName2",
                "lblSize2": "lblSize2",
                "imgRemoveAttachment2": "imgRemoveAttachment2",
                "flxRemoveAttachment2": "flxRemoveAttachment2",
                "flxDownloadAttachment2": "flxDownloadAttachment2",
                "imgDownloadAttachment2": "imgDownloadAttachment2",
                "flxDocAttachment3": "flxDocAttachment3",
                "imgPDF3": "imgPDF3",
                "lblDocName3": "lblDocName3",
                "lblSize3": "lblSize3",
                "flxRemoveAttachment3": "flxRemoveAttachment3",
                "imgRemoveAttachment3": "imgRemoveAttachment3",
                "flxDownloadAttachment3": "flxDownloadAttachment3",
                "imgDownloadAttachment3": "imgDownloadAttachment3",
                "flxDocAttachment4": "flxDocAttachment4",
                "imgPDF4": "imgPDF4",
                "lblDocName4": "lblDocName4",
                "lblSize4": "lblSize4",
                "flxRemoveAttachment4": "flxRemoveAttachment4",
                "imgRemoveAttachment4": "imgRemoveAttachment4",
                "flxDownloadAttachment4": "flxDownloadAttachment4",
                "imgDownloadAttachment4": "imgDownloadAttachment4",
                "flxDocAttachment5": "flxDocAttachment5",
                "imgPDF5": "imgPDF5",
                "lblDocName5": "lblDocName5",
                "lblSize5": "lblSize5",
                "flxRemoveAttachment5": "flxRemoveAttachment5",
                "imgRemoveAttachment5": "imgRemoveAttachment5",
                "flxDownloadAttachment5": "flxDownloadAttachment5",
                "imgDownloadAttachment5": "imgDownloadAttachment5",
                "lblUser": "lblUser",
                "lblRowSeperator": "lblRowSeperator",
            };
        },
        /**
         * processMessagesData :This function is used for setting the messages data to the segment
         *@ param {data}  data consists of the array of the messages
         * @returns {object} data data
         */
        processMessagesData: function(data) {
            var self = this;
            var username = self.loadsMessagesNewModule().presentationController.getCurrentUserName();
            data = data.map(function(dataItem) {
                var totalAttachments = parseInt(dataItem.totalAttachments);
                var hasFirstAttachment = self.hasFirstAttachment(totalAttachments);
                var hasSecondAttachment = self.hasSecondAttachment(totalAttachments);
                var hasThirdAttachment = self.hasThirdAttachment(totalAttachments);
                var hasFourthAttachment = self.hasFourthAttachment(totalAttachments);
                var hasFifthAttachment = self.hasFifthAttachment(totalAttachments);
                return {
                    "requestId": dataItem.CustomerRequest_id,
                    "template": dataItem.createdby != username ? "flxMessagesLeft" : "flxMessagesRight",
                    "rtxMessage": dataItem.MessageDescription,
                    "lblDate": commonUtilities.getDateAndTime(dataItem.lastmodifiedts),
                    "flxDocAttachment1": {
                        "isVisible": hasFirstAttachment ? true : false
                    },
                    "lblDocName1": hasFirstAttachment ? dataItem.attachments[0].Name : "",
                    "lblSize1": hasFirstAttachment ? '(' + dataItem.attachments[0].Size.trim() + "KB" + ')' : "",
                    "imgPDF1": hasFirstAttachment ? self.getImageByType(dataItem.attachments[0].type) : "pdf_image.png",
                    "flxRemoveAttachment1": {
                        "isVisible": false
                    },
                    "flxDownloadAttachment1": {
                        "isVisible": true,
                        "onClick": function() {
                            var mediaId = dataItem.attachments[0].media_Id;
                            var fileName = dataItem.attachments[0].Name;
                            self.loadsMessagesNewModule().presentationController.downloadAttachment(mediaId, fileName);
                        }
                    },
                    "imgDownloadAttachment1": {
                        "src": ViewConstants.IMAGES.DOWNLOAD_BLUE,
                    },
                    "flxDocAttachment2": {
                        "isVisible": hasSecondAttachment ? true : false
                    },
                    "lblDocName2": hasSecondAttachment ? dataItem.attachments[1].Name : "",
                    "lblSize2": hasSecondAttachment ? '(' + dataItem.attachments[1].Size.trim() + "KB" + ')' : "",
                    "imgPDF2": hasSecondAttachment ? self.getImageByType(dataItem.attachments[1].type) : "pdf_image.png",
                    "flxRemoveAttachment2": {
                        "isVisible": false
                    },
                    "lblUser": dataItem.createdby,
                    "lblRowSeperator": {
                        "isVisible": true
                    },
                    "flxDownloadAttachment2": {
                        "isVisible": true,
                        "onClick": function() {
                            var mediaId = dataItem.attachments[1].media_Id;
                            var fileName = dataItem.attachments[1].Name;
                            self.loadsMessagesNewModule().presentationController.downloadAttachment(mediaId, fileName);
                        }
                    },
                    "imgDownloadAttachment2": {
                        "src": ViewConstants.IMAGES.DOWNLOAD_BLUE,
                        "isVisible": true
                    },
                    "flxDocAttachment3": {
                        "isVisible": hasThirdAttachment ? true : false
                    },
                    "lblDocName3": hasThirdAttachment ? dataItem.attachments[2].Name : "",
                    "lblSize3": hasThirdAttachment ? '(' + dataItem.attachments[2].Size.trim() + "KB" + ')' : "",
                    "imgPDF3": hasThirdAttachment ? self.getImageByType(dataItem.attachments[2].type) : "pdf_image.png",
                    "flxRemoveAttachment3": {
                        "isVisible": false
                    },
                    "flxDownloadAttachment3": {
                        "isVisible": true,
                        "onClick": function() {
                            var mediaId = dataItem.attachments[2].media_Id;
                            var fileName = dataItem.attachments[2].Name;
                            self.loadsMessagesNewModule().presentationController.downloadAttachment(mediaId, fileName);
                        }
                    },
                    "imgDownloadAttachment3": {
                        "src": ViewConstants.IMAGES.DOWNLOAD_BLUE,
                        "isVisible": true
                    },
                    "flxDocAttachment4": {
                        "isVisible": hasFourthAttachment ? true : false
                    },
                    "lblDocName4": hasFourthAttachment ? dataItem.attachments[3].Name : "",
                    "lblSize4": hasFourthAttachment ? '(' + dataItem.attachments[3].Size.trim() + "KB" + ')' : "",
                    "imgPDF4": hasFourthAttachment ? self.getImageByType(dataItem.attachments[3].type) : "pdf_image.png",
                    "flxRemoveAttachment4": {
                        "isVisible": false
                    },
                    "flxDownloadAttachment4": {
                        "isVisible": true,
                        "onClick": function() {
                            var mediaId = dataItem.attachments[3].media_Id;
                            var fileName = dataItem.attachments[3].Name;
                            self.loadsMessagesNewModule().presentationController.downloadAttachment(mediaId, fileName);
                        }
                    },
                    "imgDownloadAttachment4": {
                        "src": ViewConstants.IMAGES.DOWNLOAD_BLUE,
                        "isVisible": true
                    },
                    "flxDocAttachment5": {
                        "isVisible": hasFifthAttachment ? true : false
                    },
                    "lblDocName5": hasFifthAttachment ? dataItem.attachments[4].Name : "",
                    "lblSize5": hasFifthAttachment ? '(' + dataItem.attachments[4].Size.trim() + "KB" + ')' : "",
                    "imgPDF5": hasFifthAttachment ? self.getImageByType(dataItem.attachments[4].type) : "pdf_image.png",
                    "flxRemoveAttachment5": {
                        "isVisible": false
                    },
                    "flxDownloadAttachment5": {
                        "isVisible": true,
                        "onClick": function() {
                            var mediaId = dataItem.attachments[4].media_Id;
                            var fileName = dataItem.attachments[4].Name;
                            self.loadsMessagesNewModule().presentationController.downloadAttachment(mediaId, fileName);
                        }
                    },
                    "imgDownloadAttachment5": {
                        "src": ViewConstants.IMAGES.DOWNLOAD_BLUE,
                        "isVisible": true
                    },
                    "imgUser": dataItem.createdby !== username ? ViewConstants.IMAGES.BANK_CIRCLE_ICON_MOD : ViewConstants.IMAGES.USER_IMAGE,
                    "imgToolTip": dataItem.createdby !== username ? ViewConstants.IMAGES.REPLY_ARROWTIP : ViewConstants.IMAGES.REPLY_ARROWTIP_RIGHT
                };
            });
            return data;
        },
        /**
         * hasFirstAttachment :This function is used to check whether there is first Attachment or not
         * @param {object}  totalAttachments  is the total number of Attachments which are uploaded
         * @return {boolean} retuns true if there is an attachment or false if there is no first Attachment
         */
        hasFirstAttachment: function(totalAttachments) {
            var has = false;
            has = totalAttachments > 0 ? true : false;
            return has;
        },
        /**
         * hasSecondAttachment :This function is used to check whether there is second Attachment or not
         * @param {object}  totalAttachments  is the total number of Attachments which are uploaded
         * @return {boolean} retuns true if there is an attachment or false if there is no second Attachment
         */
        hasSecondAttachment: function(totalAttachments) {
            var has = false;
            has = totalAttachments > 1 ? true : false;
            return has;
        },
        /**
         * hasThirdAttachment :This function is used to check whether there is third Attachment or not
         * @param {object}  totalAttachments  is the total number of Attachments which are uploaded
         * @return {boolean} retuns true if there is an attachment or false if there is no third Attachment
         */
        hasThirdAttachment: function(totalAttachments) {
            var has = false;
            has = totalAttachments > 2 ? true : false;
            return has;
        },
        /**
         * hasFourthAttachment :This function is used to check whether there is fourth Attachment or not
         *@ param {totalAttachments}  totalAttachments  is the total number of Attachments which are uploaded
         * @return {boolean} retuns true if there is an attachment or false if there is no fourth Attachment
         */
        hasFourthAttachment: function(totalAttachments) {
            var has = false;
            has = totalAttachments > 3 ? true : false;
            return has;
        },
        /**
         * hasFifthAttachment :This function is used to check whether there is fifth Attachment or not
         *@ param {object}  totalAttachments  is the total number of Attachments which are uploaded
         * @return {boolean} retuns true if there is an attachment or false if there is no fifth Attachment
         */
        hasFifthAttachment: function(totalAttachments) {
            var has = false;
            has = totalAttachments > 4 ? true : false;
            return has;
        },
        /**
         * getImageByType :This function is used to get the image depending on the type of the document attached
         * @param {type}  type of the Attachment can be application/pdf, text/plain, image/jpeg, application/msword(DOC) or application/vnd.openxmlformats-officedocument.wordprocessingml.document(DOCX)
         * @return {image} returns the appropriate image based on type of the document
         */
        getImageByType: function(type) {
            var image;
            switch (type) {
                case "application/pdf":
                    image = ViewConstants.IMAGES.PDF_IMAGE;
                    break;
                case "text/plain":
                    image = ViewConstants.IMAGES.TXT_IMAGE;
                    break;
                case "image/jpeg":
                    image = ViewConstants.IMAGES.JPEG_IMAGE;
                    break;
                case "application/msword":
                    image = ViewConstants.IMAGES.DOC_IMAGE;
                    break;
                case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                    image = ViewConstants.IMAGES.DOCX_IMAGE;
                    break;
                case "image/png":
                    image = ViewConstants.IMAGES.PNG_IMAGE;
                    break;
            }
            return image;
        },
        closeDismissPopup: function() {
            this.view.flxDialogs.isVisible = false;
            this.view.flxLogout.isVisible = false;
            this.view.FlxDismiss.isVisible = false;
        },
        /**
         * hideCreateMessageView :This function is used to hide the create Message Template once we click on CancelCreatemessage button
         */
        hideCreateMessageView: function() {
            var messagesModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("MessagesModuleNew");
            messagesModule.presentationController.showAlertsPage("hamburgerMenu", {
                show: "Messages"
            });
            this.view.customheadernew.lblHeaderMobile.text = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.AlertsAndMessages");
            this.view.forceLayout();
        },


        /**
         * browseFiles :This function is used for opening the browser for uploading the files on click of the Attachment icon
         */
        browseFiles: function() {
            var config = this.getBrowseFilesConfig();
            this.view.lblWarningNewMessage.setVisibility(false);
            if (kony.application.getCurrentBreakpoint() == 640 || responsiveUtils.isMobile) {
                this.view.lblWarningNotMoreThan5files.setVisibility(false);
            }
            kony.io.FileSystem.browse(config, this.browseFilesCallback);
        },
        /**
         * getBrowseFilesConfig :This function is used to get the browser files config like which type of files to be uploaded
         * @return {config} returns the documents attached
         */
        getBrowseFilesConfig: function() {
            var config = {
                selectMultipleFiles: true,
                filter: ["image/png", "application/msword", "image/jpeg", "application/pdf", "text/plain", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"]
            };
            return config;
        },
        /**
         * replyBrowseFiles :This function displays the error message when more than five files are attached or invokes the konyAPI to browse the files
         */
        replyBrowseFiles: function() {
            var config = this.getBrowseFilesConfig();
            this.view.lblWarningNotMoreThan5files.setVisibility(false);
            if (kony.application.getCurrentBreakpoint() == 640 || responsiveUtils.isMobile) {
                this.view.lblWarningNotMoreThan5files.setVisibility(false);
            }
            kony.io.FileSystem.browse(config, this.replyBrowseFilesCallback);
        },
        /**
         * browseFilesCallback :This function executes once the files are uploded so that  the uploaded files are binded to the Segment while creating a new Message
         * @param {object}  event event
         * @param {object}   files is the data of the files which are uploaded
         */
        browseFilesCallback: function(event, files) {
            this.bindFilesToSegment(files, this.view.segAttachment, this.view.lblWarningNewMessage);
        },
        /**
         * replyBrowseFilesCallback :This function executes once the files are uploded so that  the uploaded files are binded to the Segment while replying to the request
         * @param {object}  event event
         * @param {object} files files is the data of the files which are uploaded
         */
        replyBrowseFilesCallback: function(event, files) {
            if (kony.application.getCurrentBreakpoint() == 640 || responsiveUtils.isMobile)
                this.bindFilesToSegment(files, this.view.segAttachment, this.view.lblWarningNotMoreThan5files);
            else
                this.bindFilesToSegment(files, this.view.segAttachment, this.view.lblWarningNotMoreThan5files);
        },
        /**
         * bindFilesToSegment :This function binds the attachment data to the segment
         * @param {object}  files is a  JSON which consists of the data of the files attached ,segment for which the files data is to be binded and the warning label which shows different kinds o warnings
         * @param {object} segment segment
         * @param {string} warningLabel warningLabel
         */
        bindFilesToSegment: function(files, segment, warningLabel) {
            var self = this;
            this.fileObject = this.fileObject || [];
            warningLabel.skin = "sknRtxSSPFF000015Px";
            if (this.fileObject.length + files.length > 5) {
                warningLabel.text = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.Maximum5AttachmentsAllowed");
                warningLabel.setVisibility(true);
                if (kony.application.getCurrentBreakpoint() == 640 || responsiveUtils.isMobile) {
                    warningLabel.left = "58dp";
                }
                this.view.flxSendNewMessage.forceLayout();
                return;
            } else if (this.isFileSizeExceeds(files)) {
                warningLabel.text = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.SizeExceeds1MB");
                warningLabel.setVisibility(true);
                if (kony.application.getCurrentBreakpoint() == 640 || responsiveUtils.isMobile) {
                    warningLabel.left = "58dp";
                }
                this.view.flxSendNewMessage.forceLayout();
                return;
            } else if (this.isFileAlreadyAdded(files)) {
                warningLabel.text = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.AlreadyFileAdded");
                warningLabel.setVisibility(true);
                if (kony.application.getCurrentBreakpoint() == 640 || responsiveUtils.isMobile) {
                    warningLabel.left = "58dp";
                }
                this.view.flxSendNewMessage.forceLayout();
                return;
            } else if (!this.isFileTypeSupported(files)) {
                warningLabel.text = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.InvalidFileType");
                warningLabel.setVisibility(true);
                if (kony.application.getCurrentBreakpoint() == 640 || responsiveUtils.isMobile) {
                    warningLabel.left = "58dp";
                    this.view.flxReplyMessageButtons.forceLayout();
                }
                this.view.flxSendNewMessage.forceLayout();
                return;
            } else {
                if (files && files.length > 0) {
                    files.forEach(function(item) {
                        self.fileObject.push(item);
                    });
                    var dataToAdd = files.map(function(dataItem) {
                        var break_point = kony.application.getCurrentBreakpoint();
                        if (break_point == 640 && responsiveUtils.isMobile) {
                            docSize = "(" + dataItem.file.size + "B)";
                        } else {
                            docSize = "(" + dataItem.file.size + " Bytes)";
                        }
                        return {
                            "lblDocSize": docSize,
                            "imgPDF": self.getImageByType(dataItem.file.type),
                            "flxRemoveAttachment": {
                                "onClick": function() {
                                    var index = segment.selectedIndex;
                                    var sectionIndex = index[0];
                                    var rowIndex = index[1];
                                    segment.removeAt(rowIndex, sectionIndex);
                                    self.fileObject.splice(rowIndex, 1);
                                    self.view.forceLayout();
                                    self.view.flxReplyMessageButtons.setFocus(true);
                                }
                            },
                            "imgRemoveAttachment": {
                                "src": ViewConstants.IMAGES.ICON_CLOSE_BLUE
                            },
                            "rtxDocName": dataItem.file.name
                        };
                    });
                    var data = segment.data;
                    if (data && data.length > 0) {
                        dataToAdd.forEach(function(item) {
                            data.push(item);
                        });
                    } else {
                        data = dataToAdd;
                    }
                    var dataMap = {
                        "lblDocSize": "lblDocSize",
                        "flxAddAttachmentMain": "flxAddAttachmentMain",
                        "flxAttachmentName": "flxAttachmentName",
                        "flxDocAttachment": "flxDocAttachment",
                        "flxRemoveAttachment": "flxRemoveAttachment",
                        "flxVerticalMiniSeparator": "flxVerticalMiniSeparator",
                        "imgPDF": "imgPDF",
                        "imgRemoveAttachment": "imgRemoveAttachment",
                        "rtxDocName": "rtxDocName"
                    };
                    segment.widgetDataMap = dataMap;
                    segment.setData(data);
                }
            }
            this.view.forceLayout();
        },
        /**
         * isFileSizeExceeds :This function is used to check if the size of the file exceeds more than 1MB
         * @param {files}   files which consists of the data of the files
         * @return {boolean} true if the size of the file exceeds false if the file size is less than 1MB
         */
        isFileSizeExceeds: function(files) {
            var temp = files.filter(function(file) {
                return file.size > 1048576;
            });
            return temp.length > 0;
        },
        /**
         * isFileAlreadyAdded :This function is used to check if the file is already added or not
         * @param {object}   files JSON which consists of the data of the files
         * @return {boolean} true if the same file is added again or  false if the new file is added
         */
        isFileAlreadyAdded: function(files) {
            var temp = this.fileObject.filter(function(file) {
                return files.filter(function(f) {
                    return f.name == file.name;
                }).length > 0;
            });
            return temp.length > 0;
        },
        /**
         * used to check file type supported or not
         * @param {object} files files is the data of the files which are uploaded
         * @param {boolean} status status
         */
        isFileTypeSupported: function(files) {
            filetype = files[0].file.type;
            if (filetype == "image/png" || filetype == "application/msword" || filetype == "image/jpeg" || filetype == "application/pdf" || filetype == "text/plain" || filetype == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") return true;
            return false;
        },
        /**
         * showNewMessageCreationError :This function is used to show the error when  the create New Message Fails
         * @param {string} errorMsg   error message
         */
        showNewMessageCreationError: function(errorMsg) {
            if (kony.application.getCurrentBreakpoint() == 640 || responsiveUtils.isMobile) {
                this.view.lblWarningNotMoreThan5files.text = errorMsg;
                this.view.lblWarningNotMoreThan5files.setVisibility(true);
            } else {
                this.view.lblWarningReplyMessage.text = errorMsg;
                this.view.lblWarningReplyMessage.setVisibility(true);
            }
            FormControllerUtility.hideProgressBar(this.view);
            this.view.flxSendMessage.forceLayout();
        },
        onBreakpointChange: function(eventObj, width) {
            var scope = this;
            kony.print('on breakpoint change');
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.CustomPopup.onBreakpointChangeComponent(scope.view.CustomPopup, width);
            this.view.CustomPopup1.onBreakpointChangeComponent(scope.view.CustomPopup1, width);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
        },
    };
});