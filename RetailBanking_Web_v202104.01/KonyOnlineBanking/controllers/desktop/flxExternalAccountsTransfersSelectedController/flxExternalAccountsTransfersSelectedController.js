define({
    showUnselectedRow: function() {
        var index = kony.application.getCurrentForm().transfermain.segmentTransfers.selectedRowIndex;
        var rowIndex = index[1];
        var data = kony.application.getCurrentForm().transfermain.segmentTransfers.data;
        data[rowIndex].imgDropdown = "arrow_down.png";
        data[rowIndex].template = "flxExternalAccountsTransfersUnselected";
        kony.application.getCurrentForm().transfermain.segmentTransfers.setDataAt(data[rowIndex], rowIndex);
        this.AdjustScreen(0);
    },
    viewBillPayActivityTransfers: function() {
        var currForm = kony.application.getCurrentForm();
        var height_to_set = 140 + currForm.flxMain.frame.height;
        currForm.flxTransfersViewBillActivity.height = height_to_set + "dp";
        currForm.transferActivity.height = height_to_set + "dp";
        currForm.flxTransfersViewBillActivity.setVisibility(true);
        this.AdjustScreen(30);
        currForm.forceLayout();
    },
    AdjustScreen: function(data) {
        var currentForm = kony.application.getCurrentForm();
        currentForm.forceLayout();
        var mainheight = 0;
        var screenheight = kony.os.deviceInfo().screenHeight;
        currentForm.forceLayout();
        mainheight = currentForm.customheader.frame.height + currentForm.flxMain.frame.height;
        var diff = screenheight - mainheight;
        if (mainheight < screenheight) {
            diff = diff - currentForm.flxFooter.frame.height;
            if (diff > 0)
                currentForm.flxFooter.top = mainheight + diff + data + "dp";
            else
                currentForm.flxFooter.top = mainheight + data + "dp";
            currentForm.forceLayout();
        } else {
            currentForm.flxFooter.top = mainheight + data + "dp";
            currentForm.forceLayout();
        }
    },
});