define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for flxImgCheckTransaction **/
    AS_FlexContainer_c79054c1ee344071ba8e1d3535fba8f1: function AS_FlexContainer_c79054c1ee344071ba8e1d3535fba8f1(eventobject, context) {
        var self = this;
        this.executeOnParent("checkUnCheckAccountTransaction");
    },
    /** onClick defined for flxImgCheckAccount **/
    AS_FlexContainer_dbe0f725c3a34cd9aeb255b03f1713f2: function AS_FlexContainer_dbe0f725c3a34cd9aeb255b03f1713f2(eventobject, context) {
        var self = this;
        this.executeOnParent("checkUnCheckAccountAccess");
    },
    /** onClick defined for flxImgCheckView **/
    AS_FlexContainer_e2ad2766944b4f1591b137a5a62bdc53: function AS_FlexContainer_e2ad2766944b4f1591b137a5a62bdc53(eventobject, context) {
        var self = this;
        this.executeOnParent("checkUnCheckAccountView");
    }
});