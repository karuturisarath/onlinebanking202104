define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for flxClickAction3 **/
    AS_FlexContainer_a2b7468f41bb486f92d434e0397a364b: function AS_FlexContainer_a2b7468f41bb486f92d434e0397a364b(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction14 **/
    AS_FlexContainer_aa754a627f2740309ce403387ae7c030: function AS_FlexContainer_aa754a627f2740309ce403387ae7c030(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction17 **/
    AS_FlexContainer_af26f25de24843ef8ffd36d62e0c80e6: function AS_FlexContainer_af26f25de24843ef8ffd36d62e0c80e6(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onTouchStart defined for flxTooltip **/
    AS_FlexContainer_b851d762c6954f1a979d6bfb26f79401: function AS_FlexContainer_b851d762c6954f1a979d6bfb26f79401(eventobject, x, y, context) {
        var self = this;
        this.onInfoTouchStart(eventobject, context);
    },
    /** onClick defined for flxClickAction12 **/
    AS_FlexContainer_b85f7d5160754fc6bcfd79a8a39e0f06: function AS_FlexContainer_b85f7d5160754fc6bcfd79a8a39e0f06(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction15 **/
    AS_FlexContainer_b910c43175364f36839cb24b2fe9bf1c: function AS_FlexContainer_b910c43175364f36839cb24b2fe9bf1c(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction13 **/
    AS_FlexContainer_bd863072cf264fc98d607db8a3bcc312: function AS_FlexContainer_bd863072cf264fc98d607db8a3bcc312(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction7 **/
    AS_FlexContainer_be704917718647aa8bef2b262ce81f00: function AS_FlexContainer_be704917718647aa8bef2b262ce81f00(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction5 **/
    AS_FlexContainer_c286443653cd4df5a4ca4a658140c7ff: function AS_FlexContainer_c286443653cd4df5a4ca4a658140c7ff(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction10 **/
    AS_FlexContainer_c715f29dbf3045aa8e97e8a5baaf3579: function AS_FlexContainer_c715f29dbf3045aa8e97e8a5baaf3579(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction16 **/
    AS_FlexContainer_d0525d265a73412aa45e4afc24b4c1e9: function AS_FlexContainer_d0525d265a73412aa45e4afc24b4c1e9(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction6 **/
    AS_FlexContainer_d633a593bc1a4b238534447d8da9294b: function AS_FlexContainer_d633a593bc1a4b238534447d8da9294b(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction4 **/
    AS_FlexContainer_e61660320e9f462ba9f8044a91ed192d: function AS_FlexContainer_e61660320e9f462ba9f8044a91ed192d(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction18 **/
    AS_FlexContainer_e74eb7f5c1f04358997bfd78a8a5177d: function AS_FlexContainer_e74eb7f5c1f04358997bfd78a8a5177d(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction11 **/
    AS_FlexContainer_ed4b1cb09ed94ab89d3c194ecca6ffc6: function AS_FlexContainer_ed4b1cb09ed94ab89d3c194ecca6ffc6(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction19 **/
    AS_FlexContainer_eeed0140e15b484bac36a14c6951f8d6: function AS_FlexContainer_eeed0140e15b484bac36a14c6951f8d6(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onTouchEnd defined for flxTooltip **/
    AS_FlexContainer_f13b5e0f68ab459a84b7a91b1cdb1abc: function AS_FlexContainer_f13b5e0f68ab459a84b7a91b1cdb1abc(eventobject, x, y, context) {
        var self = this;
        this.onInfoTouchEnd(eventobject, context);
    },
    /** onClick defined for flxPermissionClickable **/
    AS_FlexContainer_f29f6c87d20c491896c5fdc2cfaf7974: function AS_FlexContainer_f29f6c87d20c491896c5fdc2cfaf7974(eventobject, context) {
        var self = this;
        this.selectOrUnselectEntireFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction20 **/
    AS_FlexContainer_g0629ae4c61d4004bff840f08ee3d17e: function AS_FlexContainer_g0629ae4c61d4004bff840f08ee3d17e(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction8 **/
    AS_FlexContainer_g238d5aef63548f086d5275d950c995b: function AS_FlexContainer_g238d5aef63548f086d5275d950c995b(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction1 **/
    AS_FlexContainer_g6c6f4fd9001406fa3d219e255a2b178: function AS_FlexContainer_g6c6f4fd9001406fa3d219e255a2b178(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction2 **/
    AS_FlexContainer_ia902e6a25dd40a89a722053c4c56780: function AS_FlexContainer_ia902e6a25dd40a89a722053c4c56780(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    },
    /** onClick defined for flxClickAction9 **/
    AS_FlexContainer_je391c01e37b41459507d3603dca78bd: function AS_FlexContainer_je391c01e37b41459507d3603dca78bd(eventobject, context) {
        var self = this;
        this.selectOrUnselectParentFeature(eventobject, context);
    }
});