define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onDeviceBack defined for frmBulkPaymentsUploadFile **/
    AS_Form_c724b4439f244033b7d23e32387bcd71: function AS_Form_c724b4439f244033b7d23e32387bcd71(eventobject) {
        var self = this;
        kony.print("Back Button Pressed");
    },
    /** init defined for frmBulkPaymentsUploadFile **/
    AS_Form_d6a7396990c74ee784d72166bc9dc4bd: function AS_Form_d6a7396990c74ee784d72166bc9dc4bd(eventobject) {
        var self = this;
        self.onInit();
    },
    /** preShow defined for frmBulkPaymentsUploadFile **/
    AS_Form_h5d767e0849944aa98ba037ddd6c1385: function AS_Form_h5d767e0849944aa98ba037ddd6c1385(eventobject) {
        var self = this;
        return self.onPreShow();
    },
    /** postShow defined for frmBulkPaymentsUploadFile **/
    AS_Form_hc01c15c7b9c46a0ae080e47f401f16b: function AS_Form_hc01c15c7b9c46a0ae080e47f401f16b(eventobject) {
        var self = this;
        return self.onPostShow();
    },
    /** onTouchEnd defined for frmBulkPaymentsUploadFile **/
    AS_Form_jf02d943c0574a1fb42b1dfb1304a91d: function AS_Form_jf02d943c0574a1fb42b1dfb1304a91d(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** onSelection defined for lbxBatchMode **/
    AS_ListBox_a6cdd796abe94d0a85cce0b054f7cb8c: function AS_ListBox_a6cdd796abe94d0a85cce0b054f7cb8c(eventobject) {
        var self = this;
        return self.onBatchModeSelect.call(this);
    }
});