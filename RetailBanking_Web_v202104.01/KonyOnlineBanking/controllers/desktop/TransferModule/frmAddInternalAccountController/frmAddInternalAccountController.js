define(['commonUtilities', 'FormControllerUtility', 'OLBConstants', 'ViewConstants'], function(commonUtilities, FormControllerUtility, OLBConstants, ViewConstants) {
    return {
        /** Manages the upcomming flow
         * @param  {object} viewModel object consisting data based on which new flow has to drive
         */
        onNavigate: function() {
            applicationManager.getNavigationManager().applyUpdates(this);
        },
        updateFormUI: function(viewModel) {
            if (viewModel === undefined) {
                this.resetInternalAccount();
            } else {
                if (viewModel.isLoading) {
                    FormControllerUtility.showProgressBar(this.view);
                } else {
                    FormControllerUtility.hideProgressBar(this.view);
                }
                if (viewModel.internalAccount) {
                    this.view.internalAccount.flxDowntimeWarning.setVisibility(false);
                }
                if (viewModel.sameAccounts) {
                    this.resetInternalAccount();
                    this.view.internalAccount.flxDowntimeWarning.setVisibility(false);
                    this.setSameBankAccounts(viewModel.sameAccounts);
                }
                if (viewModel.serverError) {
                    this.resetInternalAccount();
                    this.view.internalAccount.rtxDowntimeWarning.text = viewModel.serverError;
                    this.view.internalAccount.flxDowntimeWarning.setVisibility(true);
                }
            }
            this.AdjustScreen();
        },
        /** Pre Show Actions
         */
        preshowFrmAddAccount: function() {
            var scopeObj = this;
            this.view.onBreakpointChange = function() {
                scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
            }
            //this.onBreakpointChange(kony.application.getCurrentBreakpoint());
            this.view.customheader.forceCloseHamburger();
            commonUtilities.setText(this.view.internalAccount.lblBankNameValue, applicationManager.getUserPreferencesManager().getBankName(), commonUtilities.getaccessibilityConfig());
            this.view.internalAccount.flxInternationalDetailsKA.setVisibility(false);
            this.view.customheader.topmenu.flxMenu.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX;
            this.view.customheader.topmenu.flxaccounts.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX;
            this.view.customheader.topmenu.flxTransfersAndPay.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU;
            this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
            this.view.customheader.topmenu.flxaccounts.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU_HOVER;
            this.view.breadcrumb.imgBreadcrumb2.setVisibility(false);
            this.view.breadcrumb.lblBreadcrumb3.setVisibility(false);
            this.view.customheader.customhamburger.activateMenu("TRANSFERS", "Add Infinity Accounts");
            this.view.breadcrumb.setBreadcrumbData([{
                    text: kony.i18n.getLocalizedString("i18n.hamburger.transfers")
                },
                {
                    text: kony.i18n.getLocalizedString("i18n.transfers.addKonyBankAccount")
                }
            ]);
            this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.hamburger.transfers");
            this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.transfers.addKonyBankAccount");
            this.view.internalAccount.flxAccountTypeKA.setVisibility(false);
            applicationManager.getNavigationManager().applyUpdates(this);
            this.view.forceLayout();
            FormControllerUtility.updateWidgetsHeightInInfo(this, ['customheader', 'flxHeader', 'flxMainContainer', 'flxFooter', 'internalAccount.flxAccountTypeKA', 'flxPreviouslyAddedAcc']);
        },
        /** Post Show Actions
         */
        postShowAddInternalAccount: function() {
            this.AdjustScreen();
            this.setFlowActions();
        },
        /** Adjust the Screens
         */
        AdjustScreen: function() {
            var mainheight = 0;
            var screenheight = kony.os.deviceInfo().screenHeight;
            mainheight = this.view.customheader.info.frame.height + this.view.flxMainContainer.info.frame.height;
            var diff = screenheight - mainheight;
            if (mainheight < screenheight) {
                diff = diff - this.view.flxFooter.info.frame.height;
                if (diff > 0) {
                    this.view.flxFooter.top = mainheight + diff + ViewConstants.POSITIONAL_VALUES.DP;
                } else {
                    this.view.flxFooter.top = mainheight + ViewConstants.POSITIONAL_VALUES.DP;
                }
            } else {
                this.view.flxFooter.top = mainheight + ViewConstants.POSITIONAL_VALUES.DP;
            }
            this.view.forceLayout();
        },
        /** Sets the basic Flow Actions
         */
        setFlowActions: function() {
            var scopeObj = this;
            this.view.internalAccount.flxInfoKA.onClick = function() {
                if (scopeObj.view.internalAccount.AllForms.isVisible === false) {
                    scopeObj.view.internalAccount.AllForms.setVisibility(true);
                    if (scopeObj.view.internalAccount.flxDowntimeWarning.isVisible === true) {
                        scopeObj.view.internalAccount.AllForms.top = scopeObj.view.internalAccount.flxAccountTypeKA.info.frame.y + 160 + ViewConstants.POSITIONAL_VALUES.DP;
                    } else {
                        scopeObj.view.internalAccount.AllForms.top = scopeObj.view.internalAccount.flxAccountTypeKA.info.frame.y + 80 + ViewConstants.POSITIONAL_VALUES.DP;
                    }
                } else {
                    scopeObj.view.internalAccount.AllForms.setVisibility(false);
                }
            };
            this.view.internalAccount.AllForms.flxCross.onClick = function() {
                scopeObj.view.internalAccount.AllForms.setVisibility(false);
            };
            scopeObj.view.AllForms.top = scopeObj.view.flxPreviouslyAddedAcc.info.frame.height - 10 + ViewConstants.POSITIONAL_VALUES.DP;
            this.view.previouslyAddedList.flxinfo.onClick = function() {
                if (scopeObj.view.AllForms.isVisible === false) {
                    scopeObj.view.AllForms.setVisibility(true);
                } else {
                    scopeObj.view.AllForms.setVisibility(false);
                }
            };
            this.view.AllForms.flxCross.onClick = function() {
                scopeObj.view.AllForms.setVisibility(false);
            };
            this.view.previouslyAddedList.flxaddkonyaccnt.onClick = function() {
                applicationManager.getModulesPresentationController("TransferModule").showDomesticAccounts();
            };
            this.view.internalAccount.btnCancelKA.onClick = function() {
                applicationManager.getModulesPresentationController("TransferModule").showTransferScreen();
            }
            scopeObj.view.forceLayout();
        },
        /** sets the bank Account Data
         * @param  {object} data data of the account
         */
        setSameBankAccounts: function(data) {
            var self = this;
            this.view.previouslyAddedList.segkonyaccounts.widgetDataMap = {
                "CopyLabel0a25bb187ff174b": "CopyLabel0a25bb187ff174b",
                "CopyLabel0f686ea9ea96c4e": "CopyLabel0f686ea9ea96c4e",
                "Label0j5951129f89142": "Label0j5951129f89142",
                "btnviewdetails": "btnviewdetails",
                "flxsegment": "flxsegment",
                "flxsegmentseperator": "flxsegmentseperator",
                "lblAccount": "lblAccount",
                "lblbank": "lblbank",
                "btnMakeTransfer": "btnMakeTransfer"
            };
            var segData = [];

            function getMapping(context) {
                if (context.btnMakeTransfer.isVerified === true || context.btnMakeTransfer.isVerified === "true") {
                    return {
                        "text": kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer"),
                        "accessibilityconfig": {
                            "a11yLabel": kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer")
                        },
                        "toolTip": kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer"),
                        "onClick": function() {
                            self.onBtnMakeTransfer();
                        }
                    };
                } else {
                    return {
                        "text": kony.i18n.getLocalizedString("i18n.accounts.pending"),
                        "accessibilityconfig": {
                            "a11yLabel": kony.i18n.getLocalizedString("i18n.accounts.pending")
                        },
                        "toolTip": kony.i18n.getLocalizedString("i18n.accounts.pending"),
                        "onClick": function() {},
                        "skin": ViewConstants.SKINS.RED_TEXT
                    };
                }
            }
            for (var i = 0; i < (data.length); i++) {
                if ((data[i] !== undefined) && (data[i].isSameBankAccount === "true")) {
                    segData.push({
                        "btnviewdetails": {
                            "text": kony.i18n.getLocalizedString("i18n.common.ViewDetails"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.common.ViewDetails")
                            },
                            "onClick": function() {
                                self.onBtnViewDetails();
                            }
                        },
                        "btnMakeTransfer": getMapping({
                            "btnMakeTransfer": data[i]
                        }),
                        "lblAccount": {
                            "text": data[i].nickName,
                            "accessibilityconfig": {
                                "a11yLabel": data[i].nickName,
                            },
                        },
                        "lblbank": {
                            "text": data[i].bankName,
                            "accessibilityconfig": {
                                "a11yLabel": data[i].bankName,
                            },
                        },
                        "CopyLabel0a25bb187ff174b": {
                            text: data[i].accountNumber,
                            "accessibilityconfig": {
                                "a11yLabel": data[i].accountNumber,
                            },
                            isVisible: true
                        },
                        "CopyLabel0f686ea9ea96c4e": {
                            text: data[i].accountType,
                            "accessibilityconfig": {
                                "a11yLabel": data[i].accountType,
                            },
                            isVisible: true
                        },
                        "Label0j5951129f89142": {
                            text: "wwq",
                            "accessibilityconfig": {
                                "a11yLabel": "wwq",
                            },
                            isVisible: true
                        }
                    });
                }
            }
            var len = segData.length;
            this.view.internalAccount.btnAddAccountKA.skin = ViewConstants.SKINS.BLOCKED;
            this.view.internalAccount.btnAddAccountKA.setEnabled(false);
            if (len !== 0) {
                this.view.previouslyAddedList.flxheader.setVisibility(true);
                this.view.previouslyAddedList.flxseperator.setVisibility(true);
                this.view.previouslyAddedList.segkonyaccounts.setVisibility(true);
                segData[len - 1].Label0j5951129f89142.isVisible = false;
                segData[len - 1].CopyLabel0f686ea9ea96c4e.isVisible = false;
                segData[len - 1].CopyLabel0a25bb187ff174b.isVisible = false;
                this.view.previouslyAddedList.segkonyaccounts.setData(segData);
                this.view.previouslyAddedList.flxaddkonyaccnt.top = "20" + ViewConstants.POSITIONAL_VALUES.PX;
            } else {
                this.view.previouslyAddedList.flxheader.setVisibility(false);
                this.view.previouslyAddedList.flxseperator.setVisibility(false);
                this.view.previouslyAddedList.segkonyaccounts.setVisibility(false);
                this.view.previouslyAddedList.flxaddkonyaccnt.top = "0" + ViewConstants.POSITIONAL_VALUES.PX;
            }
            this.view.forceLayout();
        },
        /** Sends the data to the next form
         */
        addInternalAccount: function() {
            var scopeObj = this;
            var data = {
                "bankName": this.view.internalAccount.lblBankNameValue.text.trim(),
                "accountType": this.view.internalAccount.lbxAccountTypeKA.selectedKeyValue[1],
                "accountNumber": this.view.internalAccount.tbxAccountNumberKA.text.trim(),
                "reAccountNumber": this.view.internalAccount.tbxAccountNumberAgainKA.text.trim(),
                "beneficiaryName": commonUtilities.changedataCase(this.view.internalAccount.tbxBeneficiaryNameKA.text.trim()),
                "nickName": this.view.internalAccount.tbxAccountNickNameKA.text.trim() ? commonUtilities.changedataCase(this.view.internalAccount.tbxAccountNickNameKA.text.trim()) : "",
                "isBusinessPayee": applicationManager.getConfigurationManager().isSMEUser === "true" ? "1" : "0"
            };
            var errMsg;
            if ((data.nickName === null || data.nickName === "") && (data.beneficiaryName !== null || data.beneficiaryName !== "")) {
                data.nickName = data.beneficiaryName;
            }
            if (!(data.accountNumber === data.reAccountNumber)) {
                this.view.internalAccount.tbxAccountNumberKA.skin = ViewConstants.SKINS.BORDER;
                this.view.internalAccount.tbxAccountNumberAgainKA.skin = ViewConstants.SKINS.BORDER;
                this.view.internalAccount.btnAddAccountKA.skin = ViewConstants.SKINS.BLOCKED;
                this.view.internalAccount.btnAddAccountKA.hoverSkin = ViewConstants.SKINS.BLOCKED;
                this.view.internalAccount.btnAddAccountKA.focusSkin = ViewConstants.SKINS.BLOCKED;
                this.view.internalAccount.btnAddAccountKA.setEnabled(false);
                errMsg = kony.i18n.getLocalizedString("i18n.transfers.accNoDoNotMatch");
                this.errorInternal({
                    "errorInternal": errMsg
                });
            } else {
                this.view.internalAccount.tbxAccountNumberKA.skin = ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX;
                this.view.internalAccount.tbxAccountNumberAgainKA.skin = ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX;
                this.view.internalAccount.btnAddAccountKA.skin = ViewConstants.SKINS.NORMAL;
                this.view.internalAccount.btnAddAccountKA.hoverSkin = ViewConstants.SKINS.HOVER;
                this.view.internalAccount.btnAddAccountKA.focusSkin = ViewConstants.SKINS.FOCUS;
                this.view.internalAccount.btnAddAccountKA.setEnabled(true);
                this.view.internalAccount.lblWarning.setVisibility(false);
                this.view.internalAccount.flxDowntimeWarning.setVisibility(false);
                applicationManager.getModulesPresentationController("TransferModule").addInternalAccount(data);
            }
        },
        /** Validates the internal Account fields
         */
        validateInternalError: function() {
            var errMsg;
            var data = {
                "bankName": this.view.internalAccount.lblBankNameValue.text.trim(),
                "accountNumber": this.view.internalAccount.tbxAccountNumberKA.text.trim(),
                "reAccountNumber": this.view.internalAccount.tbxAccountNumberAgainKA.text.trim(),
                "beneficiaryName": this.view.internalAccount.tbxBeneficiaryNameKA.text.trim(),
                "nickName": this.view.internalAccount.tbxAccountNickNameKA.text.trim()
            };
            if (data.accountNumber === null || data.beneficiaryName === null || data.bankName === null || data.accountNumber === "" || data.beneficiaryName === "" || data.bankName === "") {
                this.view.internalAccount.btnAddAccountKA.skin = ViewConstants.SKINS.BLOCKED;
                this.view.internalAccount.btnAddAccountKA.hoverSkin = ViewConstants.SKINS.BLOCKED;
                this.view.internalAccount.btnAddAccountKA.focusSkin = ViewConstants.SKINS.BLOCKED;
                this.view.internalAccount.btnAddAccountKA.setEnabled(false);
            } else {
                this.view.internalAccount.tbxAccountNumberKA.skin = ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX;
                this.view.internalAccount.tbxAccountNumberAgainKA.skin = ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX;
                this.view.internalAccount.btnAddAccountKA.skin = ViewConstants.SKINS.NORMAL;
                this.view.internalAccount.btnAddAccountKA.hoverSkin = ViewConstants.SKINS.HOVER;
                this.view.internalAccount.btnAddAccountKA.focusSkin = ViewConstants.SKINS.FOCUS;
                this.view.internalAccount.btnAddAccountKA.setEnabled(true);
            }
        },
        /** Shows the internal Account error
         * @param  {object} viewModel error response
         */
        errorInternal: function(viewModel) {
            this.view.internalAccount.flxDowntimeWarning.setVisibility(false);
            commonUtilities.setText(this.view.internalAccount.lblWarning, viewModel.errorInternal, commonUtilities.getaccessibilityConfig());
            this.view.internalAccount.lblWarning.setVisibility(true);
        },
        /** Resets the internal Account fields
         */
        resetInternalAccount: function() {
            this.view.internalAccount.tbxAccountNumberKA.skin = ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX;
            this.view.internalAccount.tbxAccountNumberAgainKA.skin = ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX;
            this.view.internalAccount.tbxAccountNumberKA.text = "";
            this.view.internalAccount.tbxBeneficiaryNameKA.text = "";
            this.view.internalAccount.tbxAccountNickNameKA.text = "";
            this.view.internalAccount.tbxAccountNumberAgainKA.text = "";
            this.view.internalAccount.lblWarning.setVisibility(false);
            this.view.internalAccount.btnAddAccountKA.skin = ViewConstants.SKINS.BLOCKED;
            this.view.internalAccount.btnAddAccountKA.hoverSkin = ViewConstants.SKINS.BLOCKED;
            this.view.internalAccount.btnAddAccountKA.focusSkin = ViewConstants.SKINS.BLOCKED;
            this.view.internalAccount.flxDowntimeWarning.setVisibility(false);
        },
        /** Manages widgets and Skins
         */
        addInternationalAccount: function() {
            this.view.externalAccount.flxDomesticDetailsKA.setVisibility(false);
            this.view.externalAccount.flxInternationalDetailsKA.setVisibility(true);
            this.view.externalAccount.btnInternationalAccountKA.skin = ViewConstants.SKINS.ACCOUNT_SUMMARY_SELETED;
            this.view.externalAccount.btnDomesticAccountKA.skin = ViewConstants.SKINS.ACCOUNT_TYPE_UNSELECTED;
            this.view.forceLayout();
        },
        /** Manages Widgets and Skins
         */
        addDomesticAccount: function() {
            this.view.externalAccount.flxDomesticDetailsKA.setVisibility(true);
            this.view.externalAccount.flxInternationalDetailsKA.setVisibility(false);
            this.view.externalAccount.btnInternationalAccountKA.skin = ViewConstants.SKINS.ACCOUNT_SUMMARY_SELETED;
            this.view.forceLayout();
        },
        /**Manages the Make trasfer flow
         */
        onBtnMakeTransfer: function() {
            var index = this.view.previouslyAddedList.segkonyaccounts.selectedRowIndex[1];
            var data = this.view.previouslyAddedList.segkonyaccounts.data[index];
            applicationManager.getModulesPresentationController("TransferModule").showTransferScreen({
                "accountTo": data.CopyLabel0a25bb187ff174b.text
            });
        },
        /** Shows Details of the Account
         */
        onBtnViewDetails: function() {
            var index = this.view.previouslyAddedList.segkonyaccounts.selectedRowIndex[1];
            var data = this.view.previouslyAddedList.segkonyaccounts.data[index];
            applicationManager.getModulesPresentationController("TransferModule").getSelectedExternalAccount(data.CopyLabel0a25bb187ff174b.text);
        },
        orientationHandler: null,
        onBreakpointChange: function(width) {
            var scope = this;
            kony.print('on breakpoint change');
            if (this.orientationHandler === null) {
                this.orientationHandler = new OrientationHandler();
            }
            this.orientationHandler.onOrientationChange(this.onBreakpointChange);
            this.view.customheader.onBreakpointChangeComponent(width);
            this.view.CustomPopup.onBreakpointChangeComponent(scope.view.CustomPopup, width);
            this.setupFormOnTouchEnd(width);
            var responsiveFonts = new ResponsiveFonts();
            if (width === 640) {
                commonUtilities.setText(this.view.customheader.lblHeaderMobile, "Add Internal Account", commonUtilities.getaccessibilityConfig());
                responsiveFonts.setMobileFonts();
                this.view.internalAccount.lblBankNameValue.skin = "sknSSP72727213Px";
                this.view.internalAccount.btnCancelKA.skin = "sknBtnffffffBorder0273e31pxRadius2px";
                this.view.internalAccount.btnIntCancelKA.skin = "sknBtnffffffBorder0273e31pxRadius2px";
            } else {
                commonUtilities.setText(this.view.customheader.lblHeaderMobile, "", commonUtilities.getaccessibilityConfig());
                responsiveFonts.setDesktopFonts();
            }
            this.AdjustScreen();
        },
        setupFormOnTouchEnd: function(width) {
            if (width == 640) {
                this.view.onTouchEnd = function() {}
                this.nullifyPopupOnTouchStart();
            } else {
                if (width == 1024) {
                    this.view.onTouchEnd = function() {}
                    this.nullifyPopupOnTouchStart();
                } else {
                    this.view.onTouchEnd = function() {
                        hidePopups();
                    }
                }
                var userAgent = kony.os.deviceInfo().userAgent;
                if (userAgent.indexOf("iPad") != -1) {
                    this.view.onTouchEnd = function() {}
                    this.nullifyPopupOnTouchStart();
                } else if (userAgent.indexOf("Android") != -1 && userAgent.indexOf("Mobile") == -1) {
                    this.view.onTouchEnd = function() {}
                    this.nullifyPopupOnTouchStart();
                }
            }
        },
        nullifyPopupOnTouchStart: function() {}
    };
});