define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onDeviceBack defined for frmTransfersGateway **/
    AS_Form_c19eed724b57466ea509c5803cc6ca2d: function AS_Form_c19eed724b57466ea509c5803cc6ca2d(eventobject) {
        var self = this;
        //Need to Consolidate
        kony.print("Back button pressed");
    },
    /** preShow defined for frmTransfersGateway **/
    AS_Form_f20e3dfe89634c188e2bac81e363ba03: function AS_Form_f20e3dfe89634c188e2bac81e363ba03(eventobject) {
        var self = this;
        this.initTabsActions();
    },
    /** init defined for frmTransfersGateway **/
    AS_Form_g8b999a5329045869cc51191e9b0dd76: function AS_Form_g8b999a5329045869cc51191e9b0dd76(eventobject) {
        var self = this;
        this.initActions();
    },
    /** onTouchEnd defined for frmTransfersGateway **/
    AS_Form_i364d65c6f574708903770999eb6e963: function AS_Form_i364d65c6f574708903770999eb6e963(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});