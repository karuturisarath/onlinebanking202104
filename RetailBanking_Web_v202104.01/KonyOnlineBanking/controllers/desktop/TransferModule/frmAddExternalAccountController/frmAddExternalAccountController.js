define(['commonUtilities', 'FormControllerUtility', 'OLBConstants', 'ViewConstants'], function(commonUtilities, FormControllerUtility, OLBConstants, ViewConstants) {
    var InternationalBankServiceName = "Internation Account to Account Fund Transfer";
    var domesticBankServiceName = "Interbank Account to Account Fund Transfer";
    return {
        /** Manages the upcomming flow
         * @param  {object} viewModel object consisting data based on which new flow has to drive
         */
        updateFormUI: function(viewModel) {
            if (!viewModel) {
                this.resetExternalAccount();
                return;
            }
            if (viewModel.isLoading) {
                FormControllerUtility.showProgressBar(this.view);
            } else {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (viewModel.updateBankName) {
                this.setUpdateBankName(viewModel)
            }
            if (viewModel.updateInternationalBankName) {
                this.setUpdateInternationalBankName(viewModel);
            }
            if (viewModel.serverInternationalError) {
                this.resetExternalAccount();
                this.view.externalAccount.rtxDowntimeWarningInternational.text = viewModel.serverInternationalError;
                this.view.externalAccount.flxDowntimeWarningDomestic.setVisibility(false);
                this.view.externalAccount.flxDowntimeWarningInternational.setVisibility(true);
            }
            if (viewModel.domesticAccounts) {
                this.setDomesticAccounts(viewModel.domesticAccounts);
                this.hideWarnings();
            }
            if (viewModel.internationalAccounts) {
                this.setInternationalAccounts(viewModel.internationalAccounts);
                this.hideWarnings();
            }
            if (viewModel.serverError) {
                commonUtilities.showServerDownScreen();
            }
            if (viewModel.serverDomesticError) {
                this.resetExternalAccount();
                this.view.externalAccount.rtxDowntimeWarningDomestic.text = viewModel.serverDomesticError;
                this.view.externalAccount.flxDowntimeWarningDomestic.setVisibility(true);
                this.view.externalAccount.flxDowntimeWarningInternational.setVisibility(false);
            }
            this.AdjustScreen();
        },
        /** Updates the bank name
         * @param  {object} viewModel bank details
         */
        setUpdateBankName: function(viewModel) {
            if (viewModel.updateBankName.bankName !== "") {
                this.view.externalAccount.lblWarning.isVisible = false;
                this.view.externalAccount.tbxBankNameKA.text = viewModel.updateBankName.bankName;
                commonUtilities.setText(this.view.externalAccount.lblBankNameValue, viewModel.updateBankName.bankName, commonUtilities.getaccessibilityConfig());
                this.view.externalAccount.tbxBankNameKA.setEnabled(false);
                FormControllerUtility.hideErrorForTextboxFields(this.view.externalAccount.tbxRoutingNumberKA, this.view.externalAccount.lblWarning);
            } else {
                this.view.externalAccount.tbxBankNameKA.text = "";
                commonUtilities.setText(this.view.externalAccount.lblBankNameValue, "", commonUtilities.getaccessibilityConfig());
                commonUtilities.setText(this.view.externalAccount.lblWarning, kony.i18n.getLocalizedString("i18n.transfers.routingNumberValidation"), commonUtilities.getaccessibilityConfig());
                FormControllerUtility.showErrorForTextboxFields(this.view.externalAccount.tbxRoutingNumberKA, this.view.externalAccount.lblWarning);
            }
        },
        /**Sets International bank name
         * @param  {object} viewModel bank name details
         */
        setUpdateInternationalBankName: function(viewModel) {
            if (viewModel.updateInternationalBankName.data !== "") {
                this.view.externalAccount.lblWarningInt.isVisible = false;
                this.view.externalAccount.tbxIntBankNameKA.text = viewModel.updateInternationalBankName.data;
                this.view.externalAccount.tbxIntBankNameKA.setEnabled(false);
                FormControllerUtility.hideErrorForTextboxFields(this.view.externalAccount.tbxIntSwiftCodeKA, this.view.externalAccount.lblWarningInt);
            } else {
                this.view.externalAccount.tbxIntBankNameKA.text = "";
                commonUtilities.setText(this.view.externalAccount.lblWarningInt, kony.i18n.getLocalizedString("i18n.transfers.swiftNumberValidation"), commonUtilities.getaccessibilityConfig());
                FormControllerUtility.showErrorForTextboxFields(this.view.externalAccount.tbxIntSwiftCodeKA, this.view.externalAccount.lblWarningInt);
            }
        },
        /** Hides the Warnings
         */
        hideWarnings: function() {
            this.view.externalAccount.flxDowntimeWarningDomestic.setVisibility(false);
            this.view.externalAccount.flxDowntimeWarningInternational.setVisibility(false);
        },
        /** Sets Domestic Account Data
         * @param  {object} data data of account
         */
        setDomesticAccounts: function(data) {
            var self = this;
            this.resetExternalAccount();
            this.view.breadcrumb.setBreadcrumbData([{
                    text: kony.i18n.getLocalizedString("i18n.hamburger.transfers")
                },
                {
                    text: kony.i18n.getLocalizedString(
                        "i18n.transfers.addNonKonyBankAccountDomestic"
                    )
                }
            ]);
            this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.hamburger.transfers");
            this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountDomestic");
            this.view.externalAccount.flxDomesticDetailsKA.setVisibility(true);
            this.view.externalAccount.flxInternationalDetailsKA.setVisibility(false);
            this.view.externalAccount.btnInternationalAccountKA.skin = ViewConstants.SKINS.ACCOUNT_SUMMARY_UNSELETED;
            this.view.externalAccount.btnInternationalAccountKA.hoverSkin = ViewConstants.SKINS.ACCOUNT_DETAILS_SUMMARY_UNSELECTED_HOVER;
            this.view.externalAccount.btnDomesticAccountKA.skin = ViewConstants.SKINS.ACCOUNT_SUMMARY_SELETED;
            this.view.externalAccount.btnDomesticAccountKA.hoverSkin = ViewConstants.SKINS.ACCOUNT_DETAILS_SUMMARY_SELECTED_HOVER;
            this.view.previouslyAddedList.segkonyaccounts.widgetDataMap = {
                CopyLabel0a25bb187ff174b: "CopyLabel0a25bb187ff174b",
                CopyLabel0f686ea9ea96c4e: "CopyLabel0f686ea9ea96c4e",
                Label0j5951129f89142: "Label0j5951129f89142",
                btnviewdetails: "btnviewdetails",
                flxsegment: "flxsegment",
                flxsegmentseperator: "flxsegmentseperator",
                lblAccount: "lblAccount",
                lblbank: "lblbank",
                btnMakeTransfer: "btnMakeTransfer"
            };
            var segData = [];

            function getMapping(context) {
                if (context.btnMakeTransfer.isVerified === "true") {
                    return {
                        text: kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer"),
                        toolTip: kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer"),
                        onClick: function() {
                            self.onBtnMakeTransfer();
                        }
                    };
                } else {
                    return {
                        text: kony.i18n.getLocalizedString("i18n.accounts.pending"),
                        onClick: function() {},
                        skin: ViewConstants.SKINS.RED_TEXT
                    };
                }
            }
            for (var i = 0; i < data.length; i++) {
                if (data[i] !== undefined && data[i].isInternationalAccount === "false" && data[i].isSameBankAccount === "false") {
                    segData.push({
                        btnviewdetails: {
                            text: kony.i18n.getLocalizedString("i18n.common.ViewDetails"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.common.ViewDetails")
                            },
                            onClick: function() {
                                self.onBtnViewDetails();
                            }
                        },
                        btnMakeTransfer: getMapping({
                            btnMakeTransfer: data[i]
                        }),
                        lblAccount: data[i].nickName,
                        lblbank: data[i].bankName,
                        CopyLabel0a25bb187ff174b: {
                            text: data[i].accountNumber,
                            isVisible: true
                        },
                        CopyLabel0f686ea9ea96c4e: {
                            text: data[i].accountType,
                            isVisible: true
                        },
                        Label0j5951129f89142: {
                            text: "wwq",
                            isVisible: true
                        }
                    });
                }
            }
            var len = segData.length;
            this.view.externalAccount.btnAddAccountKA.skin = ViewConstants.SKINS.BLOCKED;
            this.view.externalAccount.btnAddAccountKA.setEnabled(false);
            if (len !== 0) {
                this.view.previouslyAddedList.flxheader.setVisibility(true);
                this.view.previouslyAddedList.flxseperator.setVisibility(true);
                this.view.previouslyAddedList.segkonyaccounts.setVisibility(true);
                //hiding visiblity of separator for last account
                segData[len - 1].Label0j5951129f89142.isVisible = false;
                segData[len - 1].CopyLabel0f686ea9ea96c4e.isVisible = false;
                segData[len - 1].CopyLabel0a25bb187ff174b.isVisible = false;
                this.view.previouslyAddedList.segkonyaccounts.setData(segData);
                this.view.previouslyAddedList.flxaddkonyaccnt.top = "20" + ViewConstants.POSITIONAL_VALUES.PX;
            } else {
                this.view.previouslyAddedList.flxheader.setVisibility(false);
                this.view.previouslyAddedList.flxseperator.setVisibility(false);
                this.view.previouslyAddedList.segkonyaccounts.setVisibility(false);
                this.view.previouslyAddedList.flxaddkonyaccnt.top = "0" + ViewConstants.POSITIONAL_VALUES.PX;
            }
            this.view.forceLayout();
        },
        /** Manages make transfer flow
         */
        onBtnMakeTransfer: function() {
            var index = this.view.previouslyAddedList.segkonyaccounts.selectedRowIndex[1];
            var data = this.view.previouslyAddedList.segkonyaccounts.data[index];
            var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
            transferModule.presentationController.showTransferScreen({
                accountTo: data.CopyLabel0a25bb187ff174b.text
            });
        },
        /** Shows the Details
         */
        onBtnViewDetails: function() {
            var index = this.view.previouslyAddedList.segkonyaccounts.selectedRowIndex[1];
            var data = this.view.previouslyAddedList.segkonyaccounts.data[index];
            var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
            transferModule.presentationController.getSelectedExternalAccount(data.CopyLabel0a25bb187ff174b.text);
        },
        /** Sets International Data
         * @param {object} data data of the account
         */
        setInternationalAccounts: function(data) {
            var self = this;
            this.resetExternalAccount();
            this.view.breadcrumb.setBreadcrumbData([{
                    text: kony.i18n.getLocalizedString("i18n.hamburger.transfers"),
                    "accessibilityconfig": {
                        "a11yLabel": kony.i18n.getLocalizedString("i18n.hamburger.transfers")
                    },
                },
                {
                    text: kony.i18n.getLocalizedString(
                        "i18n.transfers.addNonKonyBankAccountInternational"
                    ),
                    "accessibilityconfig": {
                        "a11yLabel": kony.i18n.getLocalizedString(
                            "i18n.transfers.addNonKonyBankAccountInternational"
                        )
                    }
                }
            ]);
            this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.hamburger.transfers");
            this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountInternational");
            this.view.externalAccount.flxDomesticDetailsKA.setVisibility(false);
            this.view.externalAccount.flxInternationalDetailsKA.setVisibility(true);
            this.view.externalAccount.btnInternationalAccountKA.skin = ViewConstants.SKINS.ACCOUNT_SUMMARY_SELETED;
            this.view.externalAccount.btnInternationalAccountKA.hoverSkin = ViewConstants.SKINS.ACCOUNT_DETAILS_SUMMARY_SELECTED_HOVER;
            this.view.externalAccount.btnDomesticAccountKA.skin = ViewConstants.SKINS.ACCOUNT_SUMMARY_UNSELETED;
            this.view.externalAccount.btnDomesticAccountKA.hoverSkin = ViewConstants.SKINS.ACCOUNT_DETAILS_SUMMARY_UNSELECTED_HOVER;
            this.view.customheader.customhamburger.activateMenu("Transfers", "Add Non Kony Accounts");
            this.view.previouslyAddedList.segkonyaccounts.widgetDataMap = {
                CopyLabel0a25bb187ff174b: "CopyLabel0a25bb187ff174b",
                CopyLabel0f686ea9ea96c4e: "CopyLabel0f686ea9ea96c4e",
                Label0j5951129f89142: "Label0j5951129f89142",
                btnviewdetails: "btnviewdetails",
                flxsegment: "flxsegment",
                flxsegmentseperator: "flxsegmentseperator",
                lblAccount: "lblAccount",
                lblbank: "lblbank",
                btnMakeTransfer: "btnMakeTransfer"
            };
            var segData = [];

            function getMapping(context) {
                if (context.btnMakeTransfer.isVerified === "true") {
                    return {
                        text: kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer"),
                        "accessibilityconfig": {
                            "a11yLabel": kony.i18n.getLocalizedString("i18n.billPay.BillPayMakeTransfer")
                        },
                        onClick: function() {
                            self.onBtnMakeTransfer();
                        }
                    };
                } else {
                    return {
                        text: kony.i18n.getLocalizedString("i18n.accounts.pending"),
                        "accessibilityconfig": {
                            "a11yLabel": kony.i18n.getLocalizedString("i18n.accounts.pending")
                        },
                        onClick: function() {},
                        skin: ViewConstants.SKINS.RED_TEXT
                    };
                }
            }
            for (var i = 0; i < data.length; i++) {
                if (data[i] !== undefined && data[i].isInternationalAccount === "true" && data[i].isSameBankAccount !== "true") {
                    segData.push({
                        btnviewdetails: {
                            text: kony.i18n.getLocalizedString("i18n.common.ViewDetails"),
                            "accessibilityconfig": {
                                "a11yLabel": kony.i18n.getLocalizedString("i18n.common.ViewDetails"),
                            },
                            onClick: function() {
                                self.onBtnViewDetails();
                            }
                        },
                        btnMakeTransfer: getMapping({
                            btnMakeTransfer: data[i]
                        }),
                        lblAccount: data[i].nickName,
                        lblbank: data[i].bankName,
                        CopyLabel0a25bb187ff174b: {
                            text: data[i].accountNumber,
                            "accessibilityconfig": {
                                "a11yLabel": data[i].accountNumber,
                            },
                            isVisible: true
                        },
                        CopyLabel0f686ea9ea96c4e: {
                            text: data[i].accountType,
                            "accessibilityconfig": {
                                "a11yLabel": data[i].accountType,
                            },
                            isVisible: true
                        },
                        Label0j5951129f89142: {
                            text: "wwq",
                            "accessibilityconfig": {
                                "a11yLabel": "",
                            },
                            isVisible: true
                        }
                    });
                }
            }
            var len = segData.length;
            this.view.externalAccount.btnIntAddAccountKA.skin = ViewConstants.SKINS.BLOCKED;
            this.view.externalAccount.btnIntAddAccountKA.setEnabled(false);
            if (len !== 0) {
                this.view.previouslyAddedList.flxheader.setVisibility(true);
                this.view.previouslyAddedList.flxseperator.setVisibility(true);
                this.view.previouslyAddedList.segkonyaccounts.setVisibility(true);
                //hiding visiblity of separator for last account
                segData[len - 1].Label0j5951129f89142.isVisible = false;
                segData[len - 1].CopyLabel0f686ea9ea96c4e.isVisible = false;
                segData[len - 1].CopyLabel0a25bb187ff174b.isVisible = false;
                this.view.previouslyAddedList.segkonyaccounts.setData(segData);
                this.view.previouslyAddedList.flxaddkonyaccnt.top = "20" + ViewConstants.POSITIONAL_VALUES.PX;
            } else {
                this.view.previouslyAddedList.flxheader.setVisibility(false);
                this.view.previouslyAddedList.flxseperator.setVisibility(false);
                this.view.previouslyAddedList.segkonyaccounts.setVisibility(false);
                this.view.previouslyAddedList.flxaddkonyaccnt.top = "0" + ViewConstants.POSITIONAL_VALUES.PX;
            }
            this.view.forceLayout();
        },
        /** Resets all the fields
         */
        resetExternalAccount: function() {
            //for Domestic Accounts
            this.view.externalAccount.tbxAccountNumberKA.skin = ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX;
            this.view.externalAccount.tbxAccountNumberAgainKA.skin = ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX;
            this.view.externalAccount.btnAddAccountKA.skin = ViewConstants.SKINS.BLOCKED;
            this.view.externalAccount.btnAddAccountKA.setEnabled(false);
            this.view.externalAccount.tbxRoutingNumberKA.text = "";
            this.view.externalAccount.tbxBankNameKA.text = "";
            commonUtilities.setText(this.view.externalAccount.lblBankNameValue, "", commonUtilities.getaccessibilityConfig());
            this.view.externalAccount.tbxAccountNumberKA.text = "";
            this.view.externalAccount.tbxAccountNumberAgainKA.text = "";
            this.view.externalAccount.tbxBeneficiaryNameKA.text = "";
            this.view.externalAccount.tbxAccountNickNameKA.text = "";
            this.view.externalAccount.btnAddAccountKA.skin = ViewConstants.SKINS.BLOCKED;
            this.view.externalAccount.btnAddAccountKA.hoverSkin = ViewConstants.SKINS.BLOCKED;
            this.view.externalAccount.btnAddAccountKA.focusSkin = ViewConstants.SKINS.BLOCKED;
            this.view.externalAccount.flxDowntimeWarningDomestic.setVisibility(false);
            this.view.externalAccount.lblWarning.setVisibility(false);
            //for International Accounts
            this.view.externalAccount.tbxIntAccountNumberKA.skin = ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX;
            this.view.externalAccount.tbxIntAccountNumberAgainKA.skin = ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX;
            this.view.externalAccount.btnIntAddAccountKA.skin = ViewConstants.SKINS.BLOCKED;
            this.view.externalAccount.btnIntAddAccountKA.setEnabled(false);
            this.view.externalAccount.tbxIntSwiftCodeKA.text = "";
            this.view.externalAccount.tbxIntBankNameKA.text = "";
            this.view.externalAccount.tbxIntAccountNumberKA.text = "";
            this.view.externalAccount.tbxIntAccountNumberAgainKA.text = "";
            this.view.externalAccount.tbxIntBeneficiaryNameKA.text = "";
            this.view.externalAccount.tbxIntAccountNickNameKA.text = "";
            this.view.externalAccount.btnIntAddAccountKA.skin = ViewConstants.SKINS.BLOCKED;
            this.view.externalAccount.btnIntAddAccountKA.hoverSkin = ViewConstants.SKINS.BLOCKED;
            this.view.externalAccount.btnIntAddAccountKA.focusSkin = ViewConstants.SKINS.BLOCKED;
            this.view.externalAccount.flxDowntimeWarningInternational.setVisibility(false);
            this.view.externalAccount.lblWarningInt.setVisibility(false);
        },
        /** pre show Actions
         */
        preshowFrmAddAccount: function() {
            var scopeObj = this;
            this.view.onBreakpointChange = function() {
                scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
            }
            //this.onBreakpointChange(kony.application.getCurrentBreakpoint());
            this.view.customheader.forceCloseHamburger();
            scopeObj.disableTextBox(this.view.externalAccount.tbxBankNameKA);
            scopeObj.disableTextBox(this.view.externalAccount.tbxIntBankNameKA);
            if (applicationManager.getConfigurationManager().addExternalAccount === true) {
                this.view.externalAccount.lblchecked.text = OLBConstants.FONT_ICONS.CHECBOX_UNSELECTED;
                this.view.externalAccount.lblcheckboxchecked.text = OLBConstants.FONT_ICONS.UNCHECBOX_SELECTED;
                this.view.externalAccount.flxCheckboxKA.setVisibility(false);
                this.view.externalAccount.flxcheckboxinternational.setVisibility(false);
                this.view.externalAccount.flxIntSwiftCodeKA.top = "-40" + ViewConstants.POSITIONAL_VALUES.DP;
            }
            this.view.customheader.customhamburger.activateMenu("TRANSFERS", "Add Non Kony Accounts");
            this.view.customheader.topmenu.flxMenu.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX;
            this.view.customheader.topmenu.flxaccounts.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX;
            this.view.customheader.topmenu.flxTransfersAndPay.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU;
            this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
            this.view.customheader.topmenu.flxaccounts.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU_HOVER;
            this.view.externalAccount.flxAccountTypeKA.setVisibility(false);
            this.view.forceLayout();
            this.view.externalAccount.tbxRoutingNumberKA.onEndEditing = this.fetchBankDetails.bind(this);
            this.view.externalAccount.tbxIntSwiftCodeKA.onEndEditing = this.fetchBankDetailsForInternational.bind(this);
            applicationManager.getNavigationManager().applyUpdates(this);
            FormControllerUtility.updateWidgetsHeightInInfo(this, ['customheader',
                'flxHeader',
                'flxMainContainer',
                'flxFooter',
                'externalAccount.flxAccountTypeKA',
                'previouslyAddedList'
            ]);
        },
        /** Post show Actions
         */
        postshowAddExternalAccount: function() {
            this.AdjustScreen();
            this.setFlowActions();
        },
        /** UI code to Adjust Screen
         */
        AdjustScreen: function() {
            var mainheight = 0;
            var screenheight = kony.os.deviceInfo().screenHeight;
            mainheight = this.view.customheader.info.frame.height + this.view.flxMainContainer.info.frame.height;
            var diff = screenheight - mainheight;
            if (mainheight < screenheight) {
                diff = diff - this.view.flxFooter.info.frame.height;
                if (diff > 0) {
                    this.view.flxFooter.top = mainheight + diff + ViewConstants.POSITIONAL_VALUES.DP;
                } else {
                    this.view.flxFooter.top = mainheight + ViewConstants.POSITIONAL_VALUES.DP;
                }
            } else {
                this.view.flxFooter.top = mainheight + ViewConstants.POSITIONAL_VALUES.DP;
            }
            this.view.forceLayout();
        },
        /** Sets basic Flow Actions
         */
        setFlowActions: function() {
            var scopeObj = this;
            this.view.externalAccount.flxInfoKA.onClick = function() {
                if (scopeObj.view.externalAccount.AllForms.isVisible === false) {
                    scopeObj.view.forceLayout();
                    if (scopeObj.view.externalAccount.lblWarning.isVisible === true) {
                        scopeObj.view.externalAccount.AllForms.top = scopeObj.view.externalAccount.flxAccountTypeKA.info.frame.y + 125 + ViewConstants.POSITIONAL_VALUES.DP;
                    } else {
                        scopeObj.view.externalAccount.AllForms.top = scopeObj.view.externalAccount.flxAccountTypeKA.info.frame.y + 80 + ViewConstants.POSITIONAL_VALUES.DP;
                    }
                    scopeObj.view.externalAccount.AllForms.setVisibility(true);
                } else {
                    scopeObj.view.externalAccount.AllForms.setVisibility(false);
                }
            };
            this.view.externalAccount.AllForms.flxCross.onClick = function() {
                scopeObj.view.externalAccount.AllForms.setVisibility(false);
            };
            this.view.externalAccount.flxIntInfoKA.onClick = function() {
                if (scopeObj.view.externalAccount.AllForms.isVisible === false) {
                    scopeObj.view.externalAccount.AllForms.setVisibility(true);
                } else {
                    scopeObj.view.externalAccount.AllForms.setVisibility(false);
                }
            };
            this.view.previouslyAddedList.flxinfo.onClick = function() {
                if (scopeObj.view.AllForms.isVisible === false) {
                    scopeObj.view.AllForms.setVisibility(true);
                    scopeObj.view.AllForms.top = scopeObj.view.previouslyAddedList.info.frame.height - 10 + ViewConstants.POSITIONAL_VALUES.DP;
                } else {
                    scopeObj.view.AllForms.setVisibility(false);
                }
            };
            this.view.AllForms.flxCross.onClick = function() {
                scopeObj.view.AllForms.setVisibility(false);
            };
            this.view.previouslyAddedList.flxaddkonyaccnt.onClick = function() {
                var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
                transferModule.presentationController.showSameBankAccounts();
            };
            this.view.externalAccount.btnCancelKA.onClick = function() {
                var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
                transferModule.presentationController.showTransferScreen();
            };
            this.view.externalAccount.btnDomesticAccountKA.onClick = function() {
                var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
                transferModule.presentationController.showTransferScreen({
                    initialView: "addExternalAccounts"
                });
            }
            this.view.externalAccount.btnInternationalAccountKA.onClick = this.setInternationalAccounts.bind(this);
            scopeObj.view.forceLayout();
        },
        /**disables the textbox
         * @param {String} textbox Id of the textBox
         */
        disableTextBox: function(textbox) {
            textbox.skin = ViewConstants.SKINS.DISABLED;
            textbox.hoverSkin = ViewConstants.SKINS.DISABLED;
            textbox.focusSkin = ViewConstants.SKINS.DISABLED;
            textbox.setEnabled(false);
        },
        /** Fetches bank details of international bank
         */
        fetchBankDetailsForInternational: function() {
            FormControllerUtility.hideErrorForTextboxFields(this.view.externalAccount.tbxIntSwiftCodeKA, this.view.externalAccount.lblWarningInt);
            var swiftCode = this.view.externalAccount.tbxIntSwiftCodeKA.text;
            var serviceName = InternationalBankServiceName;
            if (swiftCode !== "") {
                var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
                transferModule.presentationController.fetchBankDetailsForInternationalTransfer(swiftCode, serviceName);
            }
        },
        /** fetches Bank details
         */
        fetchBankDetails: function() {
            FormControllerUtility.hideErrorForTextboxFields(this.view.externalAccount.tbxRoutingNumberKA, this.view.externalAccount.lblWarning);
            var routingNumber = this.view.externalAccount.tbxRoutingNumberKA.text;
            var serviceName = domesticBankServiceName;
            if (routingNumber !== "") {
                var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
                transferModule.presentationController.fetchBankDetails(routingNumber, serviceName);
            }
        },
        /**Validates Domestic Fields
         */
        validateDomesticFields: function() {
            var errMsg;
            var data = {
                checkBox: this.view.externalAccount.flxcheckboximg.src,
                routingNumber: this.view.externalAccount.tbxRoutingNumberKA.text.trim(),
                bankName: this.view.externalAccount.tbxBankNameKA.text.trim(),
                accountNumber: this.view.externalAccount.tbxAccountNumberKA.text.trim(),
                reAccountNumber: this.view.externalAccount.tbxAccountNumberAgainKA.text.trim(),
                beneficiaryName: this.view.externalAccount.tbxBeneficiaryNameKA.text.trim(),
                nickName: this.view.externalAccount.tbxAccountNickNameKA.text.trim(),
                ownerImage: this.view.externalAccount.lblchecked.text
            };
            if (data.accountNumber === null || data.beneficiaryName === null || data.bankName === null || data.routingNumber === null || data.accountNumber === "" || data.beneficiaryName === "" || data.bankName === "" || data.routingNumber === "") {
                FormControllerUtility.disableButton(this.view.externalAccount.btnAddAccountKA);
            } else {
                FormControllerUtility.enableButton(this.view.externalAccount.btnAddAccountKA);
            }
        },
        /** Validates International Fields
         */
        validateInternationalFields: function() {
            var errMsg;
            var data = {
                swiftCode: this.view.externalAccount.tbxIntSwiftCodeKA.text.trim(),
                bankName: this.view.externalAccount.tbxIntBankNameKA.text.trim(),
                accountNumber: this.view.externalAccount.tbxIntAccountNumberKA.text.trim(),
                reAccountNumber: this.view.externalAccount.tbxIntAccountNumberAgainKA.text.trim(),
                beneficiaryName: this.view.externalAccount.tbxIntBeneficiaryNameKA.text.trim(),
                nickName: this.view.externalAccount.tbxIntAccountNickNameKA.text.trim()
            };
            if (data.accountNumber === null || data.beneficiaryName === null || data.bankName === null || data.swiftCode === null || data.accountNumber === "" || data.beneficiaryName === "" || data.bankName === "" || data.swiftCode === "") {
                FormControllerUtility.disableButton(this.view.externalAccount.btnIntAddAccountKA);
            } else {
                FormControllerUtility.enableButton(this.view.externalAccount.btnIntAddAccountKA);
            }
        },
        /** Sends data to next Form
         */
        addInternationalAccount: function() {
            var scopeObj = this;
            var errMsg;
            var data = {
                ownerImage: this.view.externalAccount.lblcheckboxchecked.text,
                swiftCode: this.view.externalAccount.tbxIntSwiftCodeKA.text.trim(),
                bankName: this.view.externalAccount.tbxIntBankNameKA.text.trim(),
                accountType: this.view.externalAccount.lbxIntAccountTypeKA.selectedKeyValue[1],
                accountNumber: this.view.externalAccount.tbxIntAccountNumberKA.text.trim(),
                reAccountNumber: this.view.externalAccount.tbxIntAccountNumberAgainKA.text.trim(),
                beneficiaryName: this.view.externalAccount.tbxIntBeneficiaryNameKA.text.trim() ? commonUtilities.changedataCase(this.view.externalAccount.tbxIntBeneficiaryNameKA.text.trim()) : "",
                nickName: this.view.externalAccount.tbxIntAccountNickNameKA.text.trim() ? commonUtilities.changedataCase(this.view.externalAccount.tbxIntAccountNickNameKA.text.trim()) : "",
                isBusinessPayee: applicationManager.getConfigurationManager().isSMEUser === "true" ? "1" : "0"
            };
            if ((data.nickName === null || data.nickName === "") && (data.beneficiaryName !== null || data.beneficiaryName !== "")) {
                data.nickName = data.beneficiaryName;
            }
            if (!(data.accountNumber === data.reAccountNumber)) {
                this.setSkins('international');
                errMsg = kony.i18n.getLocalizedString("i18n.transfers.accNoDoNotMatch");
                this.errorInternational({
                    errorInternational: errMsg
                });
            } else {
                this.setFlowSkins();
                var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
                transferModule.presentationController.addInternationalAccount(data);
            }
        },
        /** Sends data to next form
         */
        addDomesticAccount: function() {
            var scopeObj = this;
            var data = {
                routingNumber: this.view.externalAccount.tbxRoutingNumberKA.text.trim(),
                bankName: this.view.externalAccount.tbxBankNameKA.text.trim(),
                accountType: this.view.externalAccount.lbxAccountTypeKA.selectedKeyValue[1],
                accountNumber: this.view.externalAccount.tbxAccountNumberKA.text.trim(),
                reAccountNumber: this.view.externalAccount.tbxAccountNumberAgainKA.text.trim(),
                beneficiaryName: this.view.externalAccount.tbxBeneficiaryNameKA.text.trim() ? commonUtilities.changedataCase(this.view.externalAccount.tbxBeneficiaryNameKA.text.trim()) : "",
                nickName: this.view.externalAccount.tbxAccountNickNameKA.text.trim() ? commonUtilities.changedataCase(this.view.externalAccount.tbxAccountNickNameKA.text.trim()) : "",
                ownerImage: this.view.externalAccount.lblchecked.text,
                isBusinessPayee: applicationManager.getConfigurationManager().isSMEUser === "true" ? "1" : "0"
            };
            if ((data.nickName === null || data.nickName === "") && (data.beneficiaryName !== null || data.beneficiaryName !== "")) {
                data.nickName = data.beneficiaryName;
            }
            if (!(data.accountNumber === data.reAccountNumber)) {
                this.setSkins('domestic');
                var errMsg = kony.i18n.getLocalizedString("i18n.transfers.accNoDoNotMatch");
                this.errorDomestic({
                    errorDomestic: errMsg
                });
            } else {
                this.setFlowSkins();
                var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
                transferModule.presentationController.addDomesticAccount(data);
            }
        },
        /** Sets the Skins
         */
        setFlowSkins: function() {
            this.view.externalAccount.tbxAccountNumberKA.skin = ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX;
            this.view.externalAccount.tbxAccountNumberAgainKA.skin = ViewConstants.SKINS.ACCOUNT_NUMBER_TEXTBOX;
            this.view.externalAccount.btnAddAccountKA.skin = ViewConstants.SKINS.NORMAL;
            this.view.externalAccount.btnAddAccountKA.hoverSkin = ViewConstants.SKINS.HOVER;
            this.view.externalAccount.btnAddAccountKA.focusSkin = ViewConstants.SKINS.FOCUS;
            this.view.externalAccount.btnAddAccountKA.setEnabled(true);
            this.view.externalAccount.lblWarning.setVisibility(false);
            this.view.externalAccount.flxDowntimeWarningDomestic.setVisibility(false);
            this.view.externalAccount.flxDowntimeWarningInternational.setVisibility(false);
        },
        /** Sets the Skins
         */
        setSkins: function(type) {
            if (type === "domestic") {
                this.view.externalAccount.tbxAccountNumberKA.skin = ViewConstants.SKINS.BORDER;
                this.view.externalAccount.tbxAccountNumberAgainKA.skin = ViewConstants.SKINS.BORDER;
                this.view.externalAccount.btnAddAccountKA.skin = ViewConstants.SKINS.BLOCKED;
                this.view.externalAccount.btnAddAccountKA.hoverSkin = ViewConstants.SKINS.BLOCKED;
                this.view.externalAccount.btnAddAccountKA.focusSkin = ViewConstants.SKINS.BLOCKED;
                this.view.externalAccount.btnAddAccountKA.setEnabled(false);
            } else {
                this.view.externalAccount.tbxIntAccountNumberKA.skin = ViewConstants.SKINS.BORDER;
                this.view.externalAccount.tbxIntAccountNumberAgainKA.skin = ViewConstants.SKINS.BORDER;
                this.view.externalAccount.btnIntAddAccountKA.skin = ViewConstants.SKINS.BLOCKED;
                this.view.externalAccount.btnIntAddAccountKA.hoverSkin = ViewConstants.SKINS.BLOCKED;
                this.view.externalAccount.btnIntAddAccountKA.focusSkin = ViewConstants.SKINS.BLOCKED;
                this.view.externalAccount.btnIntAddAccountKA.setEnabled(false);
            }
        },
        /** Sets error message while adding International Account
         * @param {object} viewModel error response form backend
         */
        errorInternational: function(viewModel) {
            this.view.externalAccount.btnIntAddAccountKA.skin = ViewConstants.SKINS.BLOCKED;
            this.view.externalAccount.flxDowntimeWarningDomestic.setVisibility(false);
            this.view.externalAccount.flxDowntimeWarningInternational.setVisibility(false);
            this.view.externalAccount.lblWarningInt.text = viewModel.errorInternational;
            this.view.externalAccount.lblWarningInt.setVisibility(true);
        },
        /** Sets error message while adding domestic Account
         * @param {object} viewModel error response form backend
         */
        errorDomestic: function(viewModel) {
            this.view.externalAccount.flxDowntimeWarningDomestic.setVisibility(false);
            this.view.externalAccount.flxDowntimeWarningInternational.setVisibility(false);
            this.view.externalAccount.lblWarning.text = viewModel.errorDomestic;
            this.view.externalAccount.lblWarning.setVisibility(true);
        },
        orientationHandler: null,
        onBreakpointChange: function(width) {
            var scope = this;
            kony.print('on breakpoint change');
            if (this.orientationHandler === null) {
                this.orientationHandler = new OrientationHandler();
            }
            this.orientationHandler.onOrientationChange(this.onBreakpointChange);
            this.view.customheader.onBreakpointChangeComponent(width);
            this.view.CustomPopup.onBreakpointChangeComponent(scope.view.CustomPopup, width);
            this.setupFormOnTouchEnd(width);
            this.AdjustScreen();
            var responsiveFonts = new ResponsiveFonts();
            if (width === 640) {
                commonUtilities.setText(this.view.customheader.lblHeaderMobile, "Add External Account", commonUtilities.getaccessibilityConfig());
                this.view.CustomPopup.width = "75%";
                responsiveFonts.setMobileFonts();
                this.view.externalAccount.lblBankNameValue.skin = "sknSSP72727213Px";
                this.view.externalAccount.btnCancelKA.skin = "sknBtnffffffBorder0273e31pxRadius2px";
                this.view.externalAccount.btnIntCancelKA.skin = "sknBtnffffffBorder0273e31pxRadius2px";
            } else {
                commonUtilities.setText(this.view.customheader.lblHeaderMobile, "", commonUtilities.getaccessibilityConfig());
                responsiveFonts.setDesktopFonts();
            }
            this.AdjustScreen();
        },
        setupFormOnTouchEnd: function(width) {
            if (width == 640) {
                this.view.onTouchEnd = function() {}
                this.nullifyPopupOnTouchStart();
            } else {
                if (width == 1024) {
                    this.view.onTouchEnd = function() {}
                    this.nullifyPopupOnTouchStart();
                } else {
                    this.view.onTouchEnd = function() {
                        hidePopups();
                    }
                }
                var userAgent = kony.os.deviceInfo().userAgent;
                if (userAgent.indexOf("iPad") != -1) {
                    this.view.onTouchEnd = function() {}
                    this.nullifyPopupOnTouchStart();
                } else if (userAgent.indexOf("Android") != -1 && userAgent.indexOf("Mobile") == -1) {
                    this.view.onTouchEnd = function() {}
                    this.nullifyPopupOnTouchStart();
                }
            }
        },
        nullifyPopupOnTouchStart: function() {}
    };
});