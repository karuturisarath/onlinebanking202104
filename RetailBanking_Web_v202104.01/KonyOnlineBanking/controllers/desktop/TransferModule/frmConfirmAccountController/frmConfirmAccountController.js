define(['commonUtilities', 'FormControllerUtility', 'OLBConstants', 'ViewConstants', 'CSRAssistUI'], function(commonUtilities, FormControllerUtility, OLBConstants, ViewConstants, CSRAssistUI) {
    var frmConfirmAccount = "frmConfirmAccount";
    return {
        /** Manages the upcomming flow
         * @param  {object} viewModel object consisting data based on which new flow has to drive
         */
        onNavigate: function() {
            applicationManager.getNavigationManager().applyUpdates(this);
        },
        updateFormUI: function(viewModel) {
            if (viewModel.isLoading) {
                FormControllerUtility.showProgressBar(this.view);
            } else {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (viewModel.internalAccount) {
                this.setInternalAccount(viewModel)
            }
            if (viewModel.domesticAccount) {
                this.setDomesticAccount(viewModel);
            }
            if (viewModel.internationalAccount) {
                this.setInternationalAccount(viewModel);
            }
            var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
            transferModule.presentationController.dataForNextForm(viewModel);
            this.AdjustScreen();
            this.view.forceLayout();
        },
        /** Sets the value for internal Account
         * @param {object} viewModel data to set
         */
        setInternalAccount: function(viewModel) {
            this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(false);
            commonUtilities.setText(this.view.lblAddAccountHeading, kony.i18n.getLocalizedString("i18n.transfers.add_kony_account"), commonUtilities.getaccessibilityConfig());
            this.view.breadcrumb.setBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.hamburger.transfers")
            }, {
                text: kony.i18n.getLocalizedString("i18n.transfers.addKonyBankAccountConfirm")
            }]);
            this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.transfers.addKonyBankAccountConfirm");
            this.mapDetails(viewModel.internalAccount);
            this.view.confirmDialogAccounts.confirmButtons.btnCancel.onClick = this.showCancelPopup.bind(this);
            this.view.confirmDialogAccounts.confirmButtons.btnModify.onClick = this.modifyAccount.bind(this, "internal");
            if (commonUtilities.isCSRMode()) {
                CSRAssistUI.setCSRAssistConfigurations(this, "frmConfirmAccount");
            } else {
                this.view.confirmDialogAccounts.confirmButtons.btnConfirm.onClick = this.addAccount.bind(this, viewModel);
            }
        },
        /** Sets the value for domestic Account
         * @param {object} viewModel data to set
         */
        setDomesticAccount: function(viewModel) {
            var data = viewModel.domesticAccount;
            this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(true);
            commonUtilities.setText(this.view.confirmDialogAccounts.keyValueRoutingNumber.lblKey, kony.i18n.getLocalizedString('i18n.accounts.routingNumber'), commonUtilities.getaccessibilityConfig());
            commonUtilities.setText(this.view.confirmDialogAccounts.keyValueRoutingNumber.lblValue, data.routingNumber, commonUtilities.getaccessibilityConfig());
            commonUtilities.setText(this.view.lblAddAccountHeading, kony.i18n.getLocalizedString('i18n.transfers.addNonKonyBankAccount'), commonUtilities.getaccessibilityConfig());
            this.view.breadcrumb.setBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.hamburger.transfers")
            }, {
                text: kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountConfirm")
            }]);
            this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountConfirm");
            this.mapDetails(viewModel.domesticAccount);
            this.view.confirmDialogAccounts.confirmButtons.btnCancel.onClick = this.showCancelPopup.bind(this);
            this.view.confirmDialogAccounts.confirmButtons.btnModify.onClick = this.modifyAccount.bind(this, "domestic");
            if (commonUtilities.isCSRMode()) {
                CSRAssistUI.setCSRAssistConfigurations(this, "frmConfirmAccount");
            } else {
                this.view.confirmDialogAccounts.confirmButtons.btnConfirm.onClick = this.addAccount.bind(this, viewModel);
            }
        },
        /** Sets the value for international Account
         * @param {object} viewModel data to set
         */
        setInternationalAccount: function(viewModel) {
            var data = viewModel.internationalAccount;
            this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(true);
            commonUtilities.setText(this.view.confirmDialogAccounts.keyValueRoutingNumber.lblKey, kony.i18n.getLocalizedString("i18n.accounts.swiftCode"), commonUtilities.getaccessibilityConfig());
            commonUtilities.setText(this.view.confirmDialogAccounts.keyValueRoutingNumber.lblValue, data.swiftCode, commonUtilities.getaccessibilityConfig());
            commonUtilities.setText(this.view.lblAddAccountHeading, kony.i18n.getLocalizedString('i18n.transfers.addNonKonyBankAccount'), commonUtilities.getaccessibilityConfig());
            this.view.breadcrumb.setBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.hamburger.transfers")
            }, {
                text: kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountConfirm")
            }]);
            this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.transfers.addNonKonyBankAccountConfirm");
            this.mapDetails(viewModel.internationalAccount);
            this.view.confirmDialogAccounts.confirmButtons.btnCancel.onClick = this.showCancelPopup.bind(this);
            this.view.confirmDialogAccounts.confirmButtons.btnModify.onClick = this.modifyAccount.bind(this, 'international');
            if (commonUtilities.isCSRMode()) {
                CSRAssistUI.setCSRAssistConfigurations(this, "frmConfirmAccount");
            } else {
                this.view.confirmDialogAccounts.confirmButtons.btnConfirm.onClick = this.addAccount.bind(this, viewModel);
            }
        },
        /** maps the dafault fields
         * @param {object} data data to set
         */
        mapDetails: function(data) {
            this.view.confirmDialogAccounts.flxDileveryBy.isVisible = false;
            this.view.flxEditHolisticAccount.isVisible = false;
            this.view.flxConfirmContainer.isVisible = true;
            this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.hamburger.transfers");
            commonUtilities.setText(this.view.confirmDialogAccounts.keyValueBankName.lblValue, data.bankName, commonUtilities.getaccessibilityConfig());
            commonUtilities.setText(this.view.confirmDialogAccounts.keyValueAccountType.lblValue, data.accountType, commonUtilities.getaccessibilityConfig());
            commonUtilities.setText(this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue, data.accountNumber, commonUtilities.getaccessibilityConfig());
            commonUtilities.setText(this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue, data.beneficiaryName, commonUtilities.getaccessibilityConfig());
            commonUtilities.setText(this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue, data.nickName, commonUtilities.getaccessibilityConfig());
            this.view.confirmDialogAccounts.keyValueCountryName.isVisible = false;
        },
        /** Actions to be done at preshow
         */
        preShowfrmConfirmAccount: function() {
            var scopeObj = this;
            this.view.onBreakpointChange = function() {
                scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
            }
            //this.onBreakpointChange(kony.application.getCurrentBreakpoint());
            this.view.customheader.topmenu.flxMenu.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX;
            this.view.customheader.topmenu.flxaccounts.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX;
            this.view.customheader.topmenu.flxTransfersAndPay.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU;
            this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
            this.view.customheader.topmenu.flxaccounts.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU_HOVER;
            this.view.confirmDialogAccounts.confirmButtons.btnConfirm.setEnabled(true);
            this.view.confirmDialogAccounts.confirmButtons.btnConfirm.toolTip = kony.i18n.getLocalizedString("i18n.transfers.confirmAccountDetails");
            this.view.confirmDialogAccounts.confirmButtons.btnModify.toolTip = kony.i18n.getLocalizedString('i18n.common.modifyAccountDetails');
            this.view.confirmDialogAccounts.confirmButtons.btnCancel.toolTip = kony.i18n.getLocalizedString('i18n.common.CancelAccount');
            this.view.flxCancelPopup.setVisibility(false);
            this.view.customheader.forceCloseHamburger();
            if (commonUtilities.isCSRMode()) {
                CSRAssistUI.setCSRAssistConfigurations(scopeObj, frmConfirmAccount);
            }
            applicationManager.getNavigationManager().applyUpdates(this);
            FormControllerUtility.updateWidgetsHeightInInfo(this, ['customheader', 'flxMainContainer', 'flxFooter', 'flxHeader']);
        },
        /** Methods to be done at post show
         */
        postShowfrmConfirmAccount: function() {
            this.AdjustScreen();
        },
        /** UI code to adjust screen
         */
        AdjustScreen: function() {
            this.view.forceLayout();
            var mainheight = 0;
            var screenheight = kony.os.deviceInfo().screenHeight;
            mainheight = this.view.customheader.info.frame.height + this.view.flxMainContainer.info.frame.height;
            var diff = screenheight - mainheight;
            if (mainheight < screenheight) {
                diff = diff - this.view.flxFooter.info.frame.height;
                if (diff > 0) {
                    this.view.flxFooter.top = mainheight + diff + ViewConstants.POSITIONAL_VALUES.DP;
                } else {
                    this.view.flxFooter.top = mainheight + ViewConstants.POSITIONAL_VALUES.DP;
                }
            } else {
                this.view.flxFooter.top = mainheight + ViewConstants.POSITIONAL_VALUES.DP;
            }
            this.view.forceLayout();
        },
        /** handles the cancel flow
         */
        showCancelPopup: function() {
            var heightToSet = 140 + this.view.flxMainContainer.frame.height + this.view.flxFooter.frame.height;
            this.view.flxCancelPopup.height = heightToSet + ViewConstants.POSITIONAL_VALUES.DP;
            this.view.flxCancelPopup.isVisible = true;
            this.view.flxCancelPopup.setFocus(true);
            this.view.CustomPopupCancel.btnYes.onClick = function() {
                this.cancelAddAccount();
            }.bind(this);
            this.view.forceLayout();
        },
        /** hides cancel popup
         */
        hideCancelPopup: function() {
            this.view.flxCancelPopup.isVisible = false;
            this.view.forceLayout();
        },
        orientationHandler: null,
        onBreakpointChange: function(width) {
            var scope = this;
            kony.print('on breakpoint change');
            //         if (this.orientationHandler === null) {
            //          this.orientationHandler = new OrientationHandler();
            //         }
            //         this.orientationHandler.onOrientationChange(this.onBreakpointChange);
            this.view.customheader.onBreakpointChangeComponent(width);
            this.view.CustomPopup.onBreakpointChangeComponent(scope.view.CustomPopup, width);
            this.view.CustomPopupCancel.onBreakpointChangeComponent(scope.view.CustomPopupCancel, width);
            this.setupFormOnTouchEnd(width);
            var responsiveFonts = new ResponsiveFonts();
            if (width === 640) {
                commonUtilities.setText(this.view.customheader.lblHeaderMobile, "Add a Infinity account", commonUtilities.getaccessibilityConfig());
                responsiveFonts.setMobileFonts();
            } else {
                commonUtilities.setText(this.view.customheader.lblHeaderMobile, "", commonUtilities.getaccessibilityConfig());
                responsiveFonts.setDesktopFonts();
            }
            this.AdjustScreen();
        },
        setupFormOnTouchEnd: function(width) {
            if (width == 640) {
                this.view.onTouchEnd = function() {}
                this.nullifyPopupOnTouchStart();
            } else {
                if (width == 1024) {
                    this.view.onTouchEnd = function() {}
                    this.nullifyPopupOnTouchStart();
                } else {
                    this.view.onTouchEnd = function() {
                        hidePopups();
                    }
                }
                var userAgent = kony.os.deviceInfo().userAgent;
                if (userAgent.indexOf("iPad") != -1) {
                    this.view.onTouchEnd = function() {}
                    this.nullifyPopupOnTouchStart();
                } else if (userAgent.indexOf("Android") != -1 && userAgent.indexOf("Mobile") == -1) {
                    this.view.onTouchEnd = function() {}
                    this.nullifyPopupOnTouchStart();
                }
            }
        },
        nullifyPopupOnTouchStart: function() {},
        /** populates the data to verify form
         */
        populateInVerify: function() {
            var newPayeeData = {
                "bankName": this.view.confirmDialogAccounts.keyValueBankName.lblValue.text,
                "accountType": this.view.confirmDialogAccounts.keyValueAccountType.lblValue.text,
                "accountNumber": this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text,
                "beneficiaryName": this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue.text,
                "nickName": this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue.text
            };
            var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
            transferModule.presentationController.createBankPayee(newPayeeData);
        },
        cancelAddAccount: function() {
            var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
            transferModule.presentationController.showTransferScreen();
        },
        modifyAccount: function(type) {
            var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
            transferModule.presentationController.modifyAccountInfo(type);
        },
        /** Adds Account
         */
        addAccount: function(data) {
            var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
            transferModule.presentationController.navigateToVerifyAccount(data);
        }
    };
});