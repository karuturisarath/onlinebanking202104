define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnCancel **/
    AS_Button_b98010e2b91e413f93439aee295a3cfc: function AS_Button_b98010e2b91e413f93439aee295a3cfc(eventobject) {
        var self = this;
        this.presenter.cancelTransaction(this);
    },
    /** onClick defined for btnConfirm **/
    AS_Button_ccc571982e3e405893be709160a38195: function AS_Button_ccc571982e3e405893be709160a38195(eventobject) {
        var self = this;
        this.checkVerificationOptions();
    },
    /** onClick defined for btnMakeTransfer **/
    AS_Button_f3c813bc89944fe69b18cd06f4b6eede: function AS_Button_f3c813bc89944fe69b18cd06f4b6eede(eventobject) {
        var self = this;
        this.makeNewTransfer();
    },
    /** onClick defined for btnAddAnotherAccount **/
    AS_Button_h74a797605f348ac987d3c3291395f9e: function AS_Button_h74a797605f348ac987d3c3291395f9e(eventobject) {
        var self = this;
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        transferModule.presentationController.addAnotherAccount();
    },
    /** onDeviceBack defined for frmVerifyAccount **/
    AS_Form_a916cb32c4a342fa8dc22af3484ba871: function AS_Form_a916cb32c4a342fa8dc22af3484ba871(eventobject) {
        var self = this;
        //Need to Consolidate
        kony.print("Back button pressed");
    },
    /** onTouchEnd defined for frmVerifyAccount **/
    AS_Form_b687723e9496482d955eeef3072402ac: function AS_Form_b687723e9496482d955eeef3072402ac(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** preShow defined for frmVerifyAccount **/
    AS_Form_ce8cbe4067d4480ea97f686e39ba8705: function AS_Form_ce8cbe4067d4480ea97f686e39ba8705(eventobject) {
        var self = this;
        this.preshowfrmVerifyAccount();
    },
    /** postShow defined for frmVerifyAccount **/
    AS_Form_d9ca2b89bff14759a2403fb22886fc81: function AS_Form_d9ca2b89bff14759a2403fb22886fc81(eventobject) {
        var self = this;
        this.postShowVerifyAccount();
    },
    /** init defined for frmVerifyAccount **/
    AS_Form_ff060438b71147f786f81f02749980c7: function AS_Form_ff060438b71147f786f81f02749980c7(eventobject) {
        var self = this;
        this.init();
    },
    /** onTouchEnd defined for lblRadioType1 **/
    AS_Label_bb051568637b44f7b01c079c19d2cefa: function AS_Label_bb051568637b44f7b01c079c19d2cefa(eventobject, x, y) {
        var self = this;
        this.setActionsOnCredential();
    },
    /** onTouchEnd defined for lblRadioType2 **/
    AS_Label_bc8ec096a97d4fa8864e233e441c5bbb: function AS_Label_bc8ec096a97d4fa8864e233e441c5bbb(eventobject, x, y) {
        var self = this;
        this.setActionsOnTrialDeposit();
    },
    /** onSelection defined for rbtVerifyByDeposits **/
    AS_RadioButtonGroup_h2da6337379e4989af06c18b2a3b5223: function AS_RadioButtonGroup_h2da6337379e4989af06c18b2a3b5223(eventobject) {
        var self = this;
        this.setActionsOnTrialDeposit();
    },
    /** onSelection defined for rbtVerifyBankCredential **/
    AS_RadioButtonGroup_i077d13867974af5a2257dff13d26461: function AS_RadioButtonGroup_i077d13867974af5a2257dff13d26461(eventobject) {
        var self = this;
        this.setActionsOnCredential();
    },
    /** onTextChange defined for tbxUsername **/
    AS_TextField_e93c2707b28445b0a93fd5d9e9126bab: function AS_TextField_e93c2707b28445b0a93fd5d9e9126bab(eventobject, changedtext) {
        var self = this;
        this.checkSkin();
    },
    /** onTextChange defined for tbxPassword **/
    AS_TextField_ib3741735390451dbd74ded877b18551: function AS_TextField_ib3741735390451dbd74ded877b18551(eventobject, changedtext) {
        var self = this;
        this.checkSkin();
    }
});