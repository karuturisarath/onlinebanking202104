define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnEdit **/
    AS_Button_dfc694c4a9554f95af5e9ef5f423281c: function AS_Button_dfc694c4a9554f95af5e9ef5f423281c(eventobject, context) {
        var self = this;
        this.showPayABill();
    },
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_d6f080e237c741fcbd4255e5901b5790: function AS_FlexContainer_d6f080e237c741fcbd4255e5901b5790(eventobject, context) {
        var self = this;
        this.segmentHistoryRowClick();
    }
});