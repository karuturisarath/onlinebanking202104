define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for flxSeparator **/
    AS_FlexContainer_be13d3f810c94becbf52529396816a29: function AS_FlexContainer_be13d3f810c94becbf52529396816a29(eventobject, context) {
        var self = this;
        this.menuPressed();
    },
    /** onClick defined for flxContent **/
    AS_FlexContainer_ca4e072f4f7e453582fa2525c3899256: function AS_FlexContainer_ca4e072f4f7e453582fa2525c3899256(eventobject, context) {
        var self = this;
        // var ntf = new kony.mvc.Navigation("frmAccountsDetails");
        // ntf.navigate();
        this.accountPressed();
    },
    /** onClick defined for flxAccountName **/
    AS_FlexContainer_d6d32d86ccbc4fd3b0e970143d950d66: function AS_FlexContainer_d6d32d86ccbc4fd3b0e970143d950d66(eventobject, context) {
        var self = this;
        // var ntf = new kony.mvc.Navigation("frmAccountsDetails");
        // ntf.navigate();
        this.accountPressed();
    },
    /** onClick defined for flxAvailableBalance **/
    AS_FlexContainer_e8e6b286f8c34fde8468093579b56427: function AS_FlexContainer_e8e6b286f8c34fde8468093579b56427(eventobject, context) {
        var self = this;
        // var ntf = new kony.mvc.Navigation("frmAccountsDetails");
        // ntf.navigate();
        this.accountPressed();
    },
    /** onClick defined for flxMenu **/
    AS_FlexContainer_f0b2f41edf0d43509048f5201e381bd0: function AS_FlexContainer_f0b2f41edf0d43509048f5201e381bd0(eventobject, context) {
        var self = this;
        this.menuPressed();
    },
    /** onClick defined for flxFavourite **/
    AS_FlexContainer_i01c63425ad645a8907c875a2313d1cb: function AS_FlexContainer_i01c63425ad645a8907c875a2313d1cb(eventobject, context) {
        var self = this;
        this.imgPressed();
    },
    /** onTouchStart defined for flxMenu **/
    AS_FlexContainer_j2b3ba8eadd3474888c4334d1399cbae: function AS_FlexContainer_j2b3ba8eadd3474888c4334d1399cbae(eventobject, x, y, context) {
        var self = this;
        this.setClickOrigin();
    }
});