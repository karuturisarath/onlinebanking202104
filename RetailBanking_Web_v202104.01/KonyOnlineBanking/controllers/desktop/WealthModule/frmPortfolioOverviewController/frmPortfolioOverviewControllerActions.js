define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** init defined for frmPortfolioOverview **/
    AS_Form_df5e413531994d989e771403adf86762: function AS_Form_df5e413531994d989e771403adf86762(eventobject) {
        var self = this;
        this.init();
    },
    /** onTouchEnd defined for lblConvertCurrency **/
    AS_Label_f94461d4896c462ea626b26be4eeceaf: function AS_Label_f94461d4896c462ea626b26be4eeceaf(eventobject, x, y) {
        var self = this;
        return self.navigateToConvertCurr.call(this);
    },
    /** onFilterChanged defined for investmentLineChart **/
    AS_UWI_d8e04d21d48f4cadad0d959293e8f5f6: function AS_UWI_d8e04d21d48f4cadad0d959293e8f5f6(filter) {
        var self = this;
        return self.onFilterChanged.call(this, filter);
    }
});