define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** init defined for frmBBAccountsLanding **/
    AS_Form_a0d20dcc2bd74e16a7c0a0c80ffcae0d: function AS_Form_a0d20dcc2bd74e16a7c0a0c80ffcae0d(eventobject) {
        var self = this;
        this.initializeAccountsDashboard();
    },
    /** onBreakpointChange defined for frmBBAccountsLanding **/
    AS_Form_dd20aada7ca74285ae3c907e951edfed: function AS_Form_dd20aada7ca74285ae3c907e951edfed(eventobject, breakpoint) {
        var self = this;
        this.onBreakpointChange(breakpoint);
    },
    /** preShow defined for frmBBAccountsLanding **/
    AS_Form_e35745da8b0b473ebcf3fbd94ec8e9d4: function AS_Form_e35745da8b0b473ebcf3fbd94ec8e9d4(eventobject) {
        var self = this;
        this.preShowFrmAccountsLanding();
        this.setAccountListData();
    },
    /** onTouchEnd defined for frmBBAccountsLanding **/
    AS_Form_h2b9916e616548118f434b03697fa3d7: function AS_Form_h2b9916e616548118f434b03697fa3d7(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** onDeviceBack defined for frmBBAccountsLanding **/
    AS_Form_h3520a5b179c41e488f9ff7eb063fd0a: function AS_Form_h3520a5b179c41e488f9ff7eb063fd0a(eventobject) {
        var self = this;
        kony.print("Back Button is clicked");
    },
    /** postShow defined for frmBBAccountsLanding **/
    AS_Form_j0d8ac2d8a184dfa84fd0d506ff9fb53: function AS_Form_j0d8ac2d8a184dfa84fd0d506ff9fb53(eventobject) {
        var self = this;
        this.onLoadChangePointer();
        this.setContextualMenuLeft();
    }
});