cust2 = {

    initializeWidget: function(parentNode, widgetModel) {
        //Assign custom DOM to parentNode to render this widget.
        parentNode.innerHTML = `
        <div style="postion: absolute; width: 100%; height: 100%; font-family: sans-serif;">
            <canvas id="canvas_${widgetModel.id}" class="cust1__canvas" height="300" width="300" style="width: 100%; position:absolute; top:0px">
            </canvas>
            <div style="position:absolute; bottom: 25px; width:100%; text-align: center; color: #424242; font-size: 30px; font-weight: 600;" id="cust1_label_${widgetModel.id}"></div>
            <div style="position:absolute; bottom: 0px; width:100%;text-align: center; color: #727272; font-size: 20px;">Complete</div>
        <div>
        `;
        cust1.modelChange(widgetModel, 'percentage', widgetModel.percentage)
    },

    modelChange: function(widgetModel, propertyChanged, propertyValue) {
        //Handle widget property changes to update widget's view and
        //trigger custom events based on widget state.
        if(propertyChanged == 'percentage'){
            document.querySelector(`#cust1_label_${widgetModel.id}`).innerHTML = parseInt(propertyValue) + "%";
            let canvas = document.querySelector(`#canvas_${widgetModel.id}`);
            let context = canvas.getContext('2d');
            let percent = parseInt(propertyValue);
            // let i = 0
            // var interval = setInterval(() => {
                // if(i>percent){
                    // clearInterval(interval);
                //     return
                // }
                context.beginPath();
                context.clearRect(0,0, canvas.width, canvas.height)
                console.log('called')
                context.arc(150, 150, 130, Math.PI, 0);
                context.lineWidth = 15;
                context.lineCap = 'round';
                context.strokeStyle = '#d0d0d090';
                context.stroke();
                context.closePath();
                
                // i++;
                // console.log(i)
                if(percent > 0 && percent <= 100){
                    context.beginPath();
                    context.strokeStyle = '#003e75ff';
                    context.arc(150, 150, 130, Math.PI, (Math.PI/100)*percent - Math.PI);
                    context.stroke();
                    context.closePath();
                }
                
            // }, 30);
        }
    }
};