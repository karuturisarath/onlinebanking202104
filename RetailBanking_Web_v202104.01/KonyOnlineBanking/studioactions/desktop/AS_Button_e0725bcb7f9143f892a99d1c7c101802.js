function AS_Button_e0725bcb7f9143f892a99d1c7c101802(eventobject) {
    var data = {
        "checkBox": this.view.externalAccount.flxcheckboximg.src,
        "swiftCode": this.view.externalAccount.tbxIntSwiftCodeKA.text,
        "bankName": this.view.externalAccount.tbxIntBankNameKA.text,
        "accountType": this.view.externalAccount.tbxIntAccountTypeKA.text,
        "AccountNumber": this.view.externalAccount.tbxIntAccountNumberKA.text,
        "reAccountNumber": this.view.externalAccount.tbxIntAccountNumberAgainKA.text,
        "beneficiaryName": this.view.externalAccount.tbxIntBeneficiaryNameKA.text,
        "nickName": this.view.externalAccount.tbxIntAccountNickNameKA.text
    }
    this.presenter.addInternationalAccount(this, data);
}