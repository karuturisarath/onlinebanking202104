function AS_Button_bd72ead795954508ba7fc0a70696537d(eventobject, context) {
    this.showOneTimeTransfer();
    this.view.oneTimeTransfer.lblHeader.text = "EDIT RECIPIENT";
    this.view.oneTimeTransfer.flxAccountType.setVisibility(false);
    this.setOneTimeTransferStep(1);
    this.view.breadcrumb.btnBreadcrumb2.text = "EDIT RECIPIENT";
    this.detailsJson.isDomesticVar = "false";
    this.detailsJson.isInternationalVar = "true";
}