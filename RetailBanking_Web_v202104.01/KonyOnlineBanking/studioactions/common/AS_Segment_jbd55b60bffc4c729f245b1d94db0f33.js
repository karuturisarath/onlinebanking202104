function AS_Segment_jbd55b60bffc4c729f245b1d94db0f33(eventobject, sectionNumber, rowNumber) {
    var self = this;
    return self.onTransferDurationSelection.call(this, this.view.segTransferDurationList, this.view.lblSelectedTransferDurationType, this.view.imgTransferDurationTypeDropdownIcon, this.view.flxTransferDurationTypeList);
}