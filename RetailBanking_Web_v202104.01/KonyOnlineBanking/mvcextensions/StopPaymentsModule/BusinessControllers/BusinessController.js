define([], function() {
    function StopPayments_BusinessController() {
        kony.mvc.Business.Controller.call(this);
    }
    inheritsFrom(StopPayments_BusinessController, kony.mvc.Business.Controller);
    StopPayments_BusinessController.prototype.initializeBusinessController = function() {};
    StopPayments_BusinessController.prototype.execute = function(command) {
        kony.mvc.Business.Controller.prototype.execute.call(this, command);
    };
    return StopPayments_BusinessController;
});