define([], function() {
    var frmLocateUsAllTab = "frmLocateUsAllTab";
    var frmLocateUsATMTab = "frmLocateUsATMTab";
    var frmLocateUsBranchTab = "frmLocateUsBranchTab";
    var frmLocateUsBranchDetailsMobile = "frmLocateUsBranchDetailsMobile";
    /**
     * User defined presentation controller
     * @constructor
     * @extends kony.mvc.Presentation.BasePresenter
     */
    function LocateUsNew_PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
        this.locateUsSearchModel = {
            "searchResult": null,
        };
        locateUsPresentationScope = this;
    }

    inheritsFrom(LocateUsNew_PresentationController, kony.mvc.Presentation.BasePresenter);
    var isLoggedin = false;
    var isMobile = false;
    /**
     * Overridden Method of kony.mvc.Presentation.BasePresenter
     * This method gets called when presentation controller gets initialized
     * @method
     */
    LocateUsNew_PresentationController.prototype.initializeLocateUsNew_PresentationController = function() {};
    /**
     * showLocateUsPage : Entry Function for Locate Us page
     * @param {Object} data: data to be passed to form
     * @param {String} form: view for locate us page
     */
    LocateUsNew_PresentationController.prototype.showLocateUsPage = function(view = "") {
        let data = {}
        if (!navigator.geolocation) {
            data = {
                "geoLocationError": "geoLocationError"
            };
            return;
        } else {
            var userObj = applicationManager.getUserPreferencesManager();
            isLoggedin = userObj.isUserLoggedin();
            if (!isLoggedin)
                data = {
                    "preLoginView": "preLoginView"
                };
            else
                data = {
                    "postLoginView": "postLoginView"
                };
            this.presentLocateUs(data, view === "" ? "AllTab" : view);
        }
    };

    /**
     * showATMTabView : Shows All tab view in Locate Us module
     * @param {Object} data : data to be passed to form
     */
    LocateUsNew_PresentationController.prototype.showBranchTabView = function(data) {
        let navigationManager = applicationManager.getNavigationManager();
        navigationManager.navigateTo(frmLocateUsBranchTab)
        navigationManager.updateForm(data)
    }

    /**
     * showATMTabView : Shows All tab view in Locate Us module
     * @param {Object} data : data to be passed to form
     */
    LocateUsNew_PresentationController.prototype.showATMTabView = function(data) {
        let navigationManager = applicationManager.getNavigationManager();
        navigationManager.navigateTo(frmLocateUsATMTab)
        navigationManager.updateForm(data)
    }

    /**
     * showAllTabView : Shows All tab view in Locate Us module
     * @param {Object} data : data to be passed to form
     */
    LocateUsNew_PresentationController.prototype.showAllTabView = function(data) {
        let navigationManager = applicationManager.getNavigationManager();
        navigationManager.navigateTo(frmLocateUsAllTab)
        navigationManager.updateForm(data)
    }

    /**
     * getSearchBranchOrATMList : Method to get the list of ATMs and Branches based on the search criteria
     * @param {String} queryParams - queryParams contains the search criteria
     */
    LocateUsNew_PresentationController.prototype.getSearchBranchOrATMList = function(queryParams) {
        var self = this;
        self.showProgressBar();
        var param = {
            "query": queryParams.query
        };
        var locateUsManager = applicationManager.getLocationManager();
        locateUsManager.fetchLocationsBySearch(queryParams.query, locateUsPresentationScope.fetchLocationsBySearchSuccess, locateUsPresentationScope.fetchLocationsBySearchFailure);
    };
    /**
     * fetchLocationsBySearchSuccess : success callback of fetchLocationsBySearch
     */
    LocateUsNew_PresentationController.prototype.fetchLocationsBySearchSuccess = function(data) {
        if (data && data.length) {
            locateUsPresentationScope.presentLocateUs({
                "getSearchBranchOrATMListSuccess": data
            });
        } else {
            locateUsPresentationScope.presentLocateUs({
                "getSearchBranchOrATMListFailure": "getSearchBranchOrATMListFailure"
            });
        }
    };
    /**
     * fetchLocationsBySearchFailure : failure callback of fetchLocationsBySearch
     */
    LocateUsNew_PresentationController.prototype.fetchLocationsBySearchFailure = function() {
        locateUsPresentationScope.presentLocateUs({
            "getSearchBranchOrATMListFailure": "getSearchBranchOrATMListFailure"
        });
    };

    /**
     * getAtmorBranchDetails : Method to get the Details of the ATM or Branch based on the Location Id
     * @param {Object} detailsJSON - contains the Type and Location Id
     */
    LocateUsNew_PresentationController.prototype.getAtmorBranchDetails = function(detailsJSON, mobile) {
        this.showProgressBar();
        var locateUsManager = applicationManager.getLocationManager();
        isMobile = mobile
        locateUsManager.fetchLocationDetails(detailsJSON.placeID, locateUsPresentationScope.fetchLocationDetailsSuccess, locateUsPresentationScope.fetchLocationDetailsFailure);
    };

    /**
     * ATMOrBranchDetailsMobileFaliue: success callback to display atm or branch details in mobile view
     * @param {Objeect} data: details of branch or atm
     */
    LocateUsNew_PresentationController.prototype.showBranchDetailsMobile = function(data) {
        let navigationManager = applicationManager.getNavigationManager()
        navigationManager.navigateTo(frmLocateUsBranchDetailsMobile);
        navigationManager.updateForm(data);
    };

    /**
     * fetchLocationDetailsSuccess : success callback of fetchLocationDetails
     * @param {Array} data - location details
     */
    LocateUsNew_PresentationController.prototype.fetchLocationDetailsSuccess = function(data) {
        if (data && data.length > 0) {
            let details = data[0];
            let updateData = {
                "getAtmorBranchDetailsSuccess": details
            };
            if (isMobile)
                locateUsPresentationScope.presentLocateUs(updateData, "BranchDetailsMobile")
            else
                locateUsPresentationScope.presentLocateUs(updateData);
        } else {
            let updateData = {
                "getAtmorBranchDetailsFailure": "getAtmorBranchDetailsFailure"
            };
            if (isMobile)
                locateUsPresentationScope.presentLocateUs(updateData, "BranchDetailsMobile")
            else
                locateUsPresentationScope.presentLocateUs(updateData);
        }
    };

    /**
     * fetchLocationDetailsFailure : failure callback of fetchLocationDetails
     */
    LocateUsNew_PresentationController.prototype.fetchLocationDetailsFailure = function() {
        if (isMobile)
            locateUsPresentationScope.presentLocateUs({
                "getAtmorBranchDetailsFailure": "getAtmorBranchDetailsFailure"
            }, "BranchDetailsMobile")
        else
            locateUsPresentationScope.presentLocateUs({
                "getAtmorBranchDetailsFailure": "getAtmorBranchDetailsFailure"
            });
    };

    /**
     * getBranchOrATMList : Get the list of Atms and Branches based on the current location
     */
    LocateUsNew_PresentationController.prototype.getBranchOrATMList = function() {
        var self = this;
        self.showProgressBar();
        var param = null;
        if (!navigator.geolocation) {
            self.presentLocateUs({
                "geoLocationError": "geoLocationError"
            });
            return;
        } else {
            navigator.geolocation.getCurrentPosition(geoSuccess, geoFailure);
        }

        function geoSuccess(position) {
            self.globalLat = position.coords.latitude;
            self.globalLon = position.coords.longitude;
            param = {
                "latitude": position.coords.latitude,
                "longitude": position.coords.longitude
            };
            var locateUsManager = applicationManager.getLocationManager();
            self.showProgressBar();
            locateUsManager.fetchNearByLocations(param, locateUsPresentationScope.fetchNearByLocationsSuccess, locateUsPresentationScope.fetchNearByLocationsFailure);
        }

        function geoFailure() {
            self.presentLocateUs({
                "geoLocationError": "geoLocationError"
            });
            return;
        }
    };

    /**
     * fetchNearByLocationsSuccess : success callback of fetchNearByLocations
     */
    LocateUsNew_PresentationController.prototype.fetchNearByLocationsSuccess = function(response) {
        var locationList = response;
        locateUsPresentationScope.presentLocateUs({
            "getBranchOrATMListSuccess": locationList
        });
    };

    /**
     * fetchNearByLocationsFailure : failure callback of fetchNearByLocations
     */
    LocateUsNew_PresentationController.prototype.fetchNearByLocationsFailure = function(error) {
        locateUsPresentationScope.presentLocateUs({
            "getBranchOrATMListFailure": "getBranchOrATMListFailure"
        });
    };

    /**
     * presentLocateUs : Method for navigate to LocateUs form
     * @param {Object} data - viewModel to be sent to the form
     */
    LocateUsNew_PresentationController.prototype.presentLocateUs = function(data = {}, view = "") {
        this.showProgressBar();
        let navigationManager = applicationManager.getNavigationManager();
        if (isLoggedin)
            data["postLoginView"] = "postLoginView";
        else
            data["preLoginView"] = "preLoginView";

        if (view == "AllTab")
            locateUsPresentationScope.showAllTabView(data);
        else if (view == "BranchDetailsMobile")
            locateUsPresentationScope.showBranchDetailsMobile(data);
        else if (view == "ATMTab")
            locateUsPresentationScope.showATMTabView(data);
        else if (view == "BranchTab")
            locateUsPresentationScope.showBranchTabView(data);
        else
            navigationManager.updateForm(data);
        this.hideProgressBar();
    };

    /**
     * showProgressBar : Method to show the progress bar
     */
    LocateUsNew_PresentationController.prototype.showProgressBar = function() {
        applicationManager.getNavigationManager().updateForm({
            "showProgressBar": true
        });
    };
    /**
     * hideProgressBar : Method to hide the progress bar
     */
    LocateUsNew_PresentationController.prototype.hideProgressBar = function() {
        applicationManager.getNavigationManager().updateForm({
            "showProgressBar": false
        });
    };
    return LocateUsNew_PresentationController;
});