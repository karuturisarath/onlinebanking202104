define(['CommonUtilities', 'OLBConstants'], function(CommonUtilities, OLBConstants) {
    var campaignData = {};
    var loanDetails = {
        "amount": "",
        "duration": ""
    };
    var MDABasePresenter = kony.mvc.Presentation.BasePresenter;

    function CampaignManagement_PresentationController() {
        MDABasePresenter.call(this);
    }
    this.card = "";
    this.action = "";
    inheritsFrom(CampaignManagement_PresentationController, MDABasePresenter);

    CampaignManagement_PresentationController.prototype.initializePresentationController = function() {

    };
    CampaignManagement_PresentationController.prototype.updateCampaignDetails = function(data) {
        campaignData = data[0];
    }
    /**
     * Shows Wire Transfer Activation Screen
     */
    CampaignManagement_PresentationController.prototype.getFromAccounts = function() {
        applicationManager.getNavigationManager().updateForm({
            showLoadingIndicator: {
                status: true
            }
        }, "frmShortTermDeposit");
        applicationManager.getAccountManager().fetchInternalAccounts(this.onCheckingAccountSuccess.bind(this), this.onCheckingAccountFailure.bind(this));
    };
    /**
     * @param {Account[]} checkingAccounts List of checking accounts
     * Called when fetching checking accounts is successfull for activation
     */
    CampaignManagement_PresentationController.prototype.onCheckingAccountSuccess = function(internalAccounts) {
        var data = [];
        internalAccounts = JSON.stringify(internalAccounts);
        internalAccounts = JSON.parse(internalAccounts);
        for (var i = 0; i < internalAccounts.length; i++) {
            if ((internalAccounts[i].accountType == "Savings" || internalAccounts[i].accountType == "Checkings") && internalAccounts[i].currencyCode == applicationManager.getFormatUtilManager().getCurrencySymbolCode(applicationManager.getConfigurationManager().getCurrencyCode())) {
                data.push(internalAccounts[i]);
            }
        }
        var context = {
            "enableflxDeposits": true,
            "data": data
        };
        var navManager = applicationManager.getNavigationManager();
        applicationManager.getNavigationManager().updateForm({
            showLoadingIndicator: {
                status: false
            }
        }, "frmShortTermDeposit");
        navManager.updateForm(context, "frmShortTermDeposit");
        // applicationManager.getPresentationUtility().getController('frmShortTermDeposit', true).setFromAccounts(checkingAccounts);
    };

    CampaignManagement_PresentationController.prototype.onCheckingAccountFailure = function(err) {
        var navManager = applicationManager.getNavigationManager();
        var context = {
            enableDownTimeWarning: err.errorMessage
        };
        applicationManager.getNavigationManager().updateForm({
            showLoadingIndicator: {
                status: false
            }
        }, "frmShortTermDeposit");
        navManager.updateForm(context, "frmShortTermDeposit");
    };

    CampaignManagement_PresentationController.prototype.updateCampaigning = function() {
        var userManager = applicationManager.getUserPreferencesManager();
        var userId = userManager.getUserId();
        var productsList = applicationManager.fetchAllProducts();
        var data = {
            "customerId": userId,
            "campaignId": campaignData.campaignId
        };
        applicationManager.getNavigationManager().updateForm({
            showLoadingIndicator: {
                status: true
            }
        }, "frmPersonalLoan");
        var campaignManagementManager = applicationManager.getCampaignManagementManager();
        campaignManagementManager.updateCampaigning(data, this.updateCampaigningSuccess.bind(this), this.updateCampaigningFailure.bind(this));
    };
    CampaignManagement_PresentationController.prototype.updateCampaigningSuccess = function(success) {
        var context = {
            "enableAcknowledgement": true
        };
        var navManager = applicationManager.getNavigationManager();
        applicationManager.getNavigationManager().updateForm({
            showLoadingIndicator: {
                status: false
            }
        }, "frmPersonalLoan");
        navManager.updateForm(context, "frmPersonalLoan");
    };
    CampaignManagement_PresentationController.prototype.updateCampaigningFailure = function(err) {
        var navManager = applicationManager.getNavigationManager();
        var context = {
            enableDownTimeWarning: err.errorMessage
        };
        applicationManager.getNavigationManager().updateForm({
            showLoadingIndicator: {
                status: false
            }
        }, "frmPersonalLoan");
        navManager.updateForm(context, "frmPersonalLoan");
    };

    CampaignManagement_PresentationController.prototype.createDeposit = function(data) {
        var userManager = applicationManager.getUserPreferencesManager();
        data.userId = userManager.getUserId();
        data.campaignId = campaignData.campaignId;
        applicationManager.getNavigationManager().updateForm({
            showLoadingIndicator: {
                status: true
            }
        }, "frmShortTermDeposit");
        var campaignManagementManager = applicationManager.getCampaignManagementManager();
        campaignManagementManager.createDeposit(data, this.createDepositSuccess.bind(this), this.createDepositFailure.bind(this));
    };
    CampaignManagement_PresentationController.prototype.createDepositSuccess = function(success) {
        var context = {
            "enableAcknowledgement": true
        };
        var navManager = applicationManager.getNavigationManager();
        applicationManager.getNavigationManager().updateForm({
            showLoadingIndicator: {
                status: false
            }
        }, "frmShortTermDeposit");
        navManager.updateForm(context, "frmShortTermDeposit");
    };
    CampaignManagement_PresentationController.prototype.createDepositFailure = function(err) {
        var navManager = applicationManager.getNavigationManager();
        var context = {
            enableDownTimeWarning: err.errorMessage
        };
        applicationManager.getNavigationManager().updateForm({
            showLoadingIndicator: {
                status: false
            }
        }, "frmShortTermDeposit");
        navManager.updateForm(context, "frmShortTermDeposit");
    };
    CampaignManagement_PresentationController.prototype.createLoanAccount = function(data) {
        var amount = this.cleanAmount(data.loanAmt);
        var param = {
            "amount": amount,
            "term": data.duration,
            "productType": OLBConstants.ACCOUNT_TYPE.LOAN,
            "currencyCode": applicationManager.getFormatUtilManager().getCurrencySymbolCode(applicationManager.getConfigurationManager().getCurrencyCode()),
            "campaignId": "",
            "userId": ""
        };
        var userManager = applicationManager.getUserPreferencesManager();
        param.userId = userManager.getUserId();
        param.campaignId = campaignData.campaignId;
        applicationManager.getNavigationManager().updateForm({
            showLoadingIndicator: {
                status: true
            }
        }, "frmPersonalLoan");
        var campaignManagementManager = applicationManager.getCampaignManagementManager();
        campaignManagementManager.createLoanAccount(param, this.createLoanAccountSuccess.bind(this), this.createLoanAccountFailure.bind(this));
    };
    CampaignManagement_PresentationController.prototype.createLoanAccountSuccess = function(success) {
        var context = {
            "enableAcknowledgement": true
        };
        var navManager = applicationManager.getNavigationManager();
        applicationManager.getNavigationManager().updateForm({
            showLoadingIndicator: {
                status: false
            }
        }, "frmPersonalLoan");
        navManager.updateForm(context, "frmPersonalLoan");
    };
    CampaignManagement_PresentationController.prototype.createLoanAccountFailure = function(err) {
        var navManager = applicationManager.getNavigationManager();
        var context = {
            enableDownTimeWarning: err.errorMessage
        };
        applicationManager.getNavigationManager().updateForm({
            showLoadingIndicator: {
                status: false
            }
        }, "frmPersonalLoan");
        navManager.updateForm(context, "frmPersonalLoan");
    };
    CampaignManagement_PresentationController.prototype.cleanAmount = function(amount) {
        for (var i = 1; i < amount.length; i++) {
            amount = amount.replace(/[^0-9.]/, "");
        }
        return amount;
    };

    return CampaignManagement_PresentationController;
});