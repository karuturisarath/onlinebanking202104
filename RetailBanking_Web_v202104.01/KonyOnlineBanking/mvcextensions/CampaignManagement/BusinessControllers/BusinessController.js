define([], function() {
    var BusinessController = kony.mvc.Business.Controller;
    var CommandExecutionEngine = kony.mvc.Business.CommandExecutionEngine;
    var CommandHandler = kony.mvc.Business.CommandHandler;
    var Command = kony.mvc.Business.Command;

    function CampaignManagement_BusinessController() {
        BusinessController.call(this);
    }

    inheritsFrom(CampaignManagement_BusinessController, BusinessController);


    CampaignManagement_BusinessController.prototype.initializeBusinessController = function() {

    };

    CampaignManagement_BusinessController.prototype.execute = function(command) {
        BusinessController.prototype.execute.call(this, command);
    };

    return CampaignManagement_BusinessController;
});