define(['CommonUtilities', 'OLBConstants'], function(CommonUtilities, OLBConstants) {
    /**
     * Description of New Account Opening Presentation Controller.
     * @class
     * @alias module:NAOPresentationController
     */
    function NAOPresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
    }
    inheritsFrom(NAOPresentationController, kony.mvc.Presentation.BasePresenter);
    NAOPresentationController.prototype.initializePresentationController = function() {};
    /**
     * Entry Point method for new account opening
     * @param {string} context context
     */
    NAOPresentationController.prototype.showNewAccountOpening = function(context) {
        applicationManager.getNavigationManager().navigateTo("frmNAO");
        this.presentNAO({
            resetForm: {}
        });
        this.getProducts();
        context = context || {
            NUOLanding: "true"
        }
    };
    /**
     * used to filter the products
     * @param {object} product list of products
     * @returns {object} list of accounts
     */
    function filterProductsForNao(product) {
        var accountTypes = [
            OLBConstants.ACCOUNT_TYPE.SAVING,
            OLBConstants.ACCOUNT_TYPE.CHECKING,
            OLBConstants.ACCOUNT_TYPE.CREDITCARD,
            "Credit Card" // Hard coding due to inconsistency between OLB and Admin console systems.
        ];
        return accountTypes.indexOf(product.productType) > -1
    }
    /**
     * used to fetch the products
     */
    NAOPresentationController.prototype.getProducts = function() {
        var self = this;
        self.showProgressBar();
        applicationManager.getProductManager().fetchProductsList(self.fetchProductsListSuccess.bind(self), self.fetchProductsListFailure.bind(self));
    };
    /**
     * function to get Campaigns
     */
    NAOPresentationController.prototype.getCampaignsNAO = function() {
        var self = this;
        if (OLBConstants.CLIENT_PROPERTIES && OLBConstants.CLIENT_PROPERTIES.OLB_ENABLE_INAPP_CAMPAIGNS && OLBConstants.CLIENT_PROPERTIES.OLB_ENABLE_INAPP_CAMPAIGNS.toUpperCase() === "TRUE") {
            var directMktManager = applicationManager.getDirectMarketingManager();
            directMktManager.getAds("NAOCampaignsWeb", self.getCampaignsSuccess.bind(self), self.getCampaignsFailure.bind(self));
        } else {
            self.getCampaignsSuccess([]);
        }
    };
    /**
     * Method that gets called when fetching unread messages is successful
     * @param {Object} messagesObj List of messages Object
     */
    NAOPresentationController.prototype.getCampaignsSuccess = function(res) {
        applicationManager.getNavigationManager().updateForm({
            "campaignRes": res
        });
    };
    NAOPresentationController.prototype.getCampaignsFailure = function(error) {
        applicationManager.getNavigationManager().updateForm({
            "campaignError": error
        });
    };
    /**
     * handels the products success schenario
     * @param {object} response list of products
     */
    NAOPresentationController.prototype.fetchProductsListSuccess = function(response) {
        var self = this;
        self.presentNAO({
            productSelection: {
                products: response
            }
        })
        self.hideProgressBar();
    };
    /**
     * handels the products Failure schenario
     * @param {object} response list of products
     */
    NAOPresentationController.prototype.fetchProductsListFailure = function(response) {
        CommonUtilities.showServerDownScreen();
    };
    /**
     * Shows Progress bar - Indicates Loading State
     */
    NAOPresentationController.prototype.showProgressBar = function() {
        var self = this;
        self.presentNAO({
            showProgressBar: "showProgressBar"
        });
    };
    /**
     * Hides Progress bar - Indicates Non Loading State
     * @returns None
     */
    NAOPresentationController.prototype.hideProgressBar = function() {
        var self = this;
        self.presentNAO({
            hideProgressBar: "hideProgressBar"
        });
    };
    /**
     * Present new user onboarding screen
     * @param {JSON} data View Model For NUO form
     */
    NAOPresentationController.prototype.presentNAO = function(data) {
        applicationManager.getNavigationManager().updateForm(data, "frmNAO");
    };
    /**
     * Show Server Error Flex on UI
     * @param {string} errorMessage errorMessage
     */
    NAOPresentationController.prototype.showServerErrorFlex = function(errorMessage) {
        this.hideProgressBar();
        this.presentNAO({
            serverError: errorMessage
        });
    };
    /**
     * Show Error Screen
     */
    NAOPresentationController.prototype.showErrorScreen = function() {
        CommonUtilities.showServerDownScreen();
    };
    /**
     * Save user products
     * @param {JSON} productList list of selcected products
     */
    NAOPresentationController.prototype.saveUserProducts = function(productList) {
        var self = this;
        self.showProgressBar();
        var data = productList.map(function(product) {
            return {
                product: JSON.stringify({
                    productId: product.productId,
                    productTypeId: product.productTypeId,
                    productName: product.productName,
                    productType: product.productType
                })
            };
        })
        var params = {
            productLi: JSON.stringify(data).replace(/\"/g, "'").replace(/\\\"/g, "\"").replace(/\\'/g, "\"")
        }
        applicationManager.getAccountManager().newAccountOpening(params, self.saveUserProductsSuccess.bind(self, productList), self.saveUserProductsFailure.bind(self));
    };
    /**
     * used to load the save product schenario
     * @param {object} productList  selected products
     * @param {object} response  response
     */
    NAOPresentationController.prototype.saveUserProductsSuccess = function(productList, response) {
        var self = this;
        self.hideProgressBar();
        self.performCreditCheck(productList);
    }
    /**
     * used to load the save product schenario
     * @param {object} response  response
     */
    NAOPresentationController.prototype.saveUserProductsFailure = function(response) {
        var self = this;
        if (response.isServerUnreachable) {
            self.hideProgressBar();
            self.showErrorScreen();
        } else {
            self.showServerErrorFlex(response.errorMessage);
        }
    }
    /**
     * used to navigate the Acknowledgement screen
     * @param {object} productList selected products
     */
    NAOPresentationController.prototype.performCreditCheck = function(productList) {
        this.showAcknowledgement(productList);
    }
    /**
     * used to show the Acknowledgement screen
     * @param {object} productList list of products
     */
    NAOPresentationController.prototype.showAcknowledgement = function(productList) {
        this.presentNAO({
            showAcknowledgement: {
                selectedProducts: productList
            }
        })
    }
    /**
     * used to navigate the new message scrren
     */
    NAOPresentationController.prototype.newMessage = function() {
        var alertsMsgsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AlertsMsgsModule");
        alertsMsgsModule.presentationController.showAlertsPage("AccountsLanding", {
            show: "CreateNewMessage"
        });
    };
    return NAOPresentationController;
});