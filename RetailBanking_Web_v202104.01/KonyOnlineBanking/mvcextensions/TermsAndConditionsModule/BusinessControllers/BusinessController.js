define([], function() {
    function TermsAndConditions_BusinessController() {
        kony.mvc.Business.Controller.call(this);
    }
    inheritsFrom(TermsAndConditions_BusinessController, kony.mvc.Business.Controller);
    TermsAndConditions_BusinessController.prototype.initializeBusinessController = function() {};
    TermsAndConditions_BusinessController.prototype.execute = function(command) {
        kony.mvc.Business.Controller.prototype.execute.call(this, command);
    };
    return TermsAndConditions_BusinessController;
});