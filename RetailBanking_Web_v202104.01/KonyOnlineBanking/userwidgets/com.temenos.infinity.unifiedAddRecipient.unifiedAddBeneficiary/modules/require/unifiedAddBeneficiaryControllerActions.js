define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** preShow defined for unifiedAddBeneficiary **/
    AS_FlexContainer_aff35e78e192477fa20d66b3f577eccf: function AS_FlexContainer_aff35e78e192477fa20d66b3f577eccf(eventobject) {
        var self = this;
        return self.preshow.call(this);
    },
    /** onBreakpointChange defined for unifiedAddBeneficiary **/
    AS_FlexContainer_ec3989a178ad4cb4816a5d31f3c3d6cb: function AS_FlexContainer_ec3989a178ad4cb4816a5d31f3c3d6cb(eventobject, breakpoint) {
        var self = this;
        return self.onBreakPointChange.call(this);
    },
    /** postShow defined for unifiedAddBeneficiary **/
    AS_FlexContainer_f6ad9a88bb6c494e860688725ac8e484: function AS_FlexContainer_f6ad9a88bb6c494e860688725ac8e484(eventobject) {
        var self = this;
        return self.postShow.call(this);
    },
    /** onTouchEnd defined for unifiedAddBeneficiary **/
    AS_FlexContainer_g1b502dbbea74769bbd29bfa2ceebeae: function AS_FlexContainer_g1b502dbbea74769bbd29bfa2ceebeae(eventobject, x, y) {
        var self = this;
        return self.setUpFormOnTouchEnd.call(this);
    }
});