define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxCheckboxEmailId **/
    AS_FlexContainer_b72a6c49a65943f08fd0437682839384: function AS_FlexContainer_b72a6c49a65943f08fd0437682839384(eventobject) {
        var self = this;
        this.toggleCheckbox(this.view.imgCheckboxEmailId);
    },
    /** onClick defined for flxCheckBoxPhoneNo **/
    AS_FlexContainer_f35592d95b6d40a7b5493107b2bbfa96: function AS_FlexContainer_f35592d95b6d40a7b5493107b2bbfa96(eventobject) {
        var self = this;
        this.toggleCheckbox(this.view.imgCheckBoxPhoneNo);
    },
    /** preShow defined for singleAuthenticator **/
    AS_FlexContainer_dad3abdd162c4ddbb8684a336195ff9f: function AS_FlexContainer_dad3abdd162c4ddbb8684a336195ff9f(eventobject) {
        var self = this;
        this.preShow();
    }
});