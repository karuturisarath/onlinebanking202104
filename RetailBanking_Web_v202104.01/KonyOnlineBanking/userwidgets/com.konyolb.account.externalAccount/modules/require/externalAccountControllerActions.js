define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxcheckbox **/
    AS_FlexContainer_eb8f3aca277748d289ca7830807408fa: function AS_FlexContainer_eb8f3aca277748d289ca7830807408fa(eventobject) {
        var self = this;
        this.checkboxActionDomestic();
    },
    /** onClick defined for flxcheckboximg **/
    AS_FlexContainer_ef7c6a670cab4ac2b66275ed2d761fcb: function AS_FlexContainer_ef7c6a670cab4ac2b66275ed2d761fcb(eventobject) {
        var self = this;
        this.checkboxActionInternational();
    }
});