define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnTab1 **/
    AS_Button_c8441d90b26340da9a4ead35186efa7d: function AS_Button_c8441d90b26340da9a4ead35186efa7d(eventobject) {
        var self = this;
        this.clickTab(eventobject);
    },
    /** onClick defined for btnTab2 **/
    AS_Button_jd9b70dfe27646d98a1f528b0f609bcd: function AS_Button_jd9b70dfe27646d98a1f528b0f609bcd(eventobject) {
        var self = this;
        this.clickTab(eventobject);
    },
    /** onClick defined for btnTab3 **/
    AS_Button_g046e9a2d55c4272aba3864be1d61a12: function AS_Button_g046e9a2d55c4272aba3864be1d61a12(eventobject) {
        var self = this;
        this.clickTab(eventobject);
    },
    /** onClick defined for btnTab4 **/
    AS_Button_db967164d0a342abb7f9f3981b4202a9: function AS_Button_db967164d0a342abb7f9f3981b4202a9(eventobject) {
        var self = this;
        this.clickTab(eventobject);
    },
    /** onClick defined for btnTab5 **/
    AS_Button_b9f30741e2964a76ac9798a528158602: function AS_Button_b9f30741e2964a76ac9798a528158602(eventobject) {
        var self = this;
        this.clickTab(eventobject);
    },
    /** onClick defined for btnTab6 **/
    AS_Button_b6208369b2cd46d484df0018d4d3a46e: function AS_Button_b6208369b2cd46d484df0018d4d3a46e(eventobject) {
        var self = this;
        this.clickTab(eventobject);
    },
    /** preShow defined for TabsHeaderNewCopy **/
    AS_FlexScrollContainer_ga7e780938f7494c99f2508f378b9fbf: function AS_FlexScrollContainer_ga7e780938f7494c99f2508f378b9fbf(eventobject) {
        var self = this;
        //this.invokePreShow();
    }
});