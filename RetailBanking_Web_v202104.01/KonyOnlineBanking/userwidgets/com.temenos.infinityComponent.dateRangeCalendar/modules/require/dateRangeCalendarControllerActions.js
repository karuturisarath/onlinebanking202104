define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for bttnPeriodicDays **/
    AS_Button_e34dc23471294d00a72f24fe44de745f: function AS_Button_e34dc23471294d00a72f24fe44de745f(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** onClick defined for btnThreeMonths **/
    AS_Button_h1a0e12bb8474c9ba8e049d138adc4ed: function AS_Button_h1a0e12bb8474c9ba8e049d138adc4ed(eventobject) {
        var self = this;
        this.showContactUsPage();
    },
    /** onClick defined for btnSixMonths **/
    AS_Button_c38a9a44daff4943bb1467ea8896ac1b: function AS_Button_c38a9a44daff4943bb1467ea8896ac1b(eventobject) {
        var self = this;
        this.showPrivacyPolicyPage();
    },
    /** onClick defined for btnOneYear **/
    AS_Button_id3dd3f1d5854a2fa645e10a1df360fe: function AS_Button_id3dd3f1d5854a2fa645e10a1df360fe(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    }
});