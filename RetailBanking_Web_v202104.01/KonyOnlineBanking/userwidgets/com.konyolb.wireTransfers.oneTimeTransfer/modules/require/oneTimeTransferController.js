define(['CommonUtilities'], function(CommonUtilities) {

  return {
    postshow: function(){
      CommonUtilities.setA11yFoucsHandlers(this.view.tbxAmount, this.view.flxAmount, this)
    }
  };
});