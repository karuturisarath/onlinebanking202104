define(['./AddressDAO','./ParserUtilsManager','./AddressUtility'],function(AddressDAO, ParserUtilsManager, AddressUtility) {

  //global variables
  var enableProperty = "yes";
  var addressLine1 = "";
  var addressLine2 = "";
  var city = "";
  var stateName = "";
  var country = "";
  var zipCode = "";
  var phoneNumber = "";
  var emailAddress = "";
  var countryNew = [];
  var stateNew = [];
  var countryWidget = "lbxCountry";
  var stateWidget = "lbxState";
  var countryId = "";
  var stateId = "";
  var disabledSkin = "";

  return {
    constructor: function(baseConfig, layoutConfig, pspConfig) {
      this._addressObjectService = "";
      this._countryObject = "";
      this._getCountriesOperation = "";
      this._getCountriesCriteria = "";
      this._getCountriesIdentifier = "";
      this._statesObject = "";
      this._getStatesOperation = "";
      this._getStatesCriteria = "";
      this._getStatesIdentifier = "";
      this._addressLine1 = "";
      this._addressLine2 = "";
      this._state = "";
      this._zipCode = "";
      this._cityName = "";
      this._phoneNumber = "";
      this._email = "";
      this._BREAKPTS="";
      this._addressLine1Label = "";
      this._addressLine1Value = "";
      this._addressLine2Label = "";
      this._addressLine2Value = "";
      this._countryLabel = "";
      this._countryValue = "";
      this._stateLabel = "";
      this._stateValue = "";
      this._cityLabel = "";
      this._cityValue = "";
      this._zipcodeLabel = "";
      this._zipcodeValue = "";
      this._phonenumberLabel = "";
      this._phonenumberValue = "";
      this._emailLabel = "";
      this._emailValue = "";
      this._addressLabel = "";
      this._addressTextBox = "";
      this._addressPlaceholderTextBox = "";
      this._addressFocusTextBox = "";
      this._addressHoverTextBox = "";
      this._addressListBox = "";
      this._payeeId = "";
      this._imgInfoClose = "";
      this._imgInfo = "";
      this._sknInfoHead = "";
      this._sknInfoContent = "";
      this._sknTextBoxDisabled = "";
      this.addressDAO = new AddressDAO();
      this.parserUtilsManager = new ParserUtilsManager();
      this.addressUtility = new AddressUtility();
      this.textInputsMapping = {};
      this.componentContext = {};
      this.context = {};
      this.parentScope = "";
      this.eventTriggered = "";
      this._countriesList = [];
      this._statesList = [];
      this.serviceCounter = 0;
      this.countriesMasterData = [];
      this.statesMasterData = [];
      this.FlowType = "";
    },

    //Logic for getters/setters of custom properties
    initGettersSetters: function() {
      defineSetter(this, "addressObjectService", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._addressObjectService = val;
        }
      });

      defineGetter(this, "addressObjectService", function() {
        return this._addressObjectService;
      }); 

      defineSetter(this, "imgInfoClose", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._imgInfoClose = val;
        }
      });

      defineGetter(this, "imgInfoClose", function() {
        return this._imgInfoClose;
      });

      defineSetter(this, "sknTextBoxDisabled", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._sknTextBoxDisabled = val;
        }
      });

      defineGetter(this, "sknTextBoxDisabled", function() {
        return this._sknTextBoxDisabled;
      });

      defineSetter(this, "imgInfo", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._imgInfo = val;
        }
      });

      defineGetter(this, "imgInfo", function() {
        return this._imgInfo;
      }); 

      defineSetter(this, "sknInfoContent", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._sknInfoContent = val;
        }
      });

      defineGetter(this, "sknInfoContent", function() {
        return this._sknInfoContent;
      }); 

      defineSetter(this, "sknInfoHead", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._sknInfoHead = val;
        }
      });

      defineGetter(this, "sknInfoHead", function() {
        return this._sknInfoHead;
      }); 

      defineSetter(this, "countryObject", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._countryObject = val;
        }
      });

      defineGetter(this, "countryObject", function() {
        return this._countryObject;
      });
      defineSetter(this, "getCountriesOperation", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._getCountriesOperation = val;
        }
      });

      defineGetter(this, "getCountriesOperation", function() {
        return this._getCountriesOperation;
      }); 
      defineSetter(this, "getCountriesCriteria", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._getCountriesCriteria = val;
        }
      });

      defineGetter(this, "getCountriesCriteria", function() {
        return this._getCountriesCriteria;
      });
      defineSetter(this, "getCountriesIdentifier", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._getCountriesIdentifier = val;
        }
      });

      defineGetter(this, "getCountriesIdentifier", function() {
        return this._getCountriesIdentifier;
      });
      defineSetter(this, "statesObject", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._statesObject = val;
        }
      });

      defineGetter(this, "statesObject", function() {
        return this._statesObject;
      });
      defineSetter(this, "getStatesOperation", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._getStatesOperation = val;
        }
      });

      defineGetter(this, "getStatesOperation", function() {
        return this._getStatesOperation;
      });
      defineSetter(this, "getStatesCriteria", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._getStatesCriteria = val;
        }
      });

      defineGetter(this, "getStatesCriteria", function() {
        return this._getStatesCriteria;
      });
      defineSetter(this, "getStatesIdentifier", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._getStatesIdentifier = val;
        }
      });

      defineGetter(this, "getStatesIdentifier", function() {
        return this._getStatesIdentifier;
      });
      defineSetter(this, "addressLine1", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._addressLine1 = val;
        }
      });

      defineGetter(this, "addressLine1", function() {
        return this._addressLine1;
      });
      defineSetter(this, "addressLine2", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._addressLine2 = val;
        }
      });

      defineGetter(this, "addressLine2", function() {
        return this._addressLine2;
      });
      defineSetter(this, "state", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._state = val;
        }
      });

      defineGetter(this, "state", function() {
        return this._state;
      });
      defineSetter(this, "zipCode", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._zipCode = val;
        }
      });

      defineGetter(this, "zipCode", function() {
        return this._zipCode;
      });
      defineSetter(this, "cityName ", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._cityName  = val;
        }
      });

      defineGetter(this, "cityName ", function() {
        return this._cityName ;
      });
      defineSetter(this, "phoneNumber", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._phoneNumber = val;
        }
      });

      defineGetter(this, "phoneNumber", function() {
        return this._phoneNumber;
      });
      defineSetter(this, "email", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._email = val;
        }
      });

      defineGetter(this, "email", function() {
        return this._email;
      });
      defineSetter(this, "addressLine1Label", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._addressLine1Label = val;
        }
      });

      defineGetter(this, "addressLine1Label", function() {
        return this._addressLine1Label;
      });
      defineSetter(this, "addressLine1Value", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._addressLine1Value = val;
        }
      });

      defineGetter(this, "addressLine1Value", function() {
        return this._addressLine1Value;
      });
      defineSetter(this, "addressLine2Label", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._addressLine2Label = val;
        }
      });

      defineGetter(this, "addressLine2Label", function() {
        return this._addressLine2Label;
      });
      defineSetter(this, "addressLine2Value", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._addressLine2Value = val;
        }
      });

      defineGetter(this, "addressLine2Value", function() {
        return this._addressLine2Value;
      });
      defineSetter(this, "countryLabel", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._countryLabel = val;
        }
      });

      defineGetter(this, "countryLabel", function() {
        return this._countryLabel;
      });
      defineSetter(this, "countryValue", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._countryValue = val;
        }
      });

      defineGetter(this, "countryValue", function() {
        return this._countryValue;
      });
      defineSetter(this, "stateLabel", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._stateLabel = val;
        }
      });

      defineGetter(this, "stateLabel", function() {
        return this._stateLabel;
      });
      defineSetter(this, "stateValue", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._stateValue = val;
        }
      });

      defineGetter(this, "stateValue", function() {
        return this._stateValue;
      });
      defineSetter(this, "cityLabel", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._cityLabel = val;
        }
      });

      defineGetter(this, "cityLabel", function() {
        return this._cityLabel;
      });
      defineSetter(this, "cityValue", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._cityValue = val;
        }
      });

      defineGetter(this, "cityValue", function() {
        return this._cityValue;
      });
      defineSetter(this, "zipcodeLabel", function(val) {
        if((typeof val==='string') && (val !=="")){
          this._zipcodeLabel = val;
        }
      });

      defineGetter(this, "zipcodeLabel", function() {
        return this._zipcodeLabel;
      });
      defineSetter(this, "zipcodeValue", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._zipcodeValue = val;
        }
      });

      defineGetter(this, "zipcodeValue", function() {
        return this._zipcodeValue;
      });
      defineSetter(this, "BREAKPTS", function(val) {
        if((typeof val=='string') && (val != "")){
          this._BREAKPTS=val;
        }
      });

      defineGetter(this, "BREAKPTS", function() {
        return this._BREAKPTS;
      });
      defineSetter(this, "phonenumberLabel", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._phonenumberLabel  = val;
        }
      });

      defineGetter(this, "phonenumberLabel", function() {
        return this._phonenumberLabel ;
      });
      defineSetter(this, "phonenumberValue", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._phonenumberValue = val;
        }
      });

      defineGetter(this, "phonenumberValue", function() {
        return this._phonenumberValue;
      });
      defineSetter(this, "emailLabel", function(val) {
        if((typeof val==='string') && (val !=="")){
          this._emailLabel   = val;
        }
      });

      defineGetter(this, "emailLabel", function() {
        return this._emailLabel ;
      });
      defineSetter(this, "emailValue", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._emailValue = val;
        }
      });

      defineGetter(this, "emailValue", function() {
        return this._emailValue;
      });
      defineSetter(this, "addressLabel", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._addressLabel  = val;
        }
      });

      defineGetter(this, "addressLabel", function() {
        return this._addressLabel ;
      });
      defineSetter(this, "addressTextBox", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._addressTextBox  = val;
        }
      });

      defineGetter(this, "addressTextBox", function() {
        return this._addressTextBox ;
      });
      defineSetter(this, "addressPlaceholderTextBox", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._addressPlaceholderTextBox = val;
        }
      });

      defineGetter(this, "addressPlaceholderTextBox", function() {
        return this._addressPlaceholderTextBox;
      });
      defineSetter(this, "addressFocusTextBox", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._addressFocusTextBox = val;
        }
      });

      defineGetter(this, "addressFocusTextBox", function() {
        return this._addressFocusTextBox;
      });
      defineSetter(this, "addressHoverTextBox", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._addressHoverTextBox  = val;
        }
      });

      defineGetter(this, "addressHoverTextBox", function() {
        return this._addressHoverTextBox ;
      });
      defineSetter(this, "payeeId", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._payeeId = val;
        }
      });

      defineGetter(this, "payeeId", function() {
        return this._payeeId;
      });
      defineSetter(this, "addressListBox", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._addressListBox = val;
        }
      });

      defineGetter(this, "addressListBox", function() {
        return this._addressListBox;
      });
    },

    /**
     * @api : preShow
     * Reponsible to retain the data for custom properties for multiple entries into the component
     * Invoke the DAO layer to collect information from the service.
     * @return : NA
     */
    preShow: function() {
      this.setComponentConfigs();
      this.resetComponentData(); 
      this.initActions();
    },

    /**
     * setComponentConfigs
     * @api : setComponentConfigs
     * responsible for sending componentContext passed into parserUtilManager.
     * @return : NA
     */
    setComponentConfigs: function() {
      this.parserUtilsManager.setBreakPointConfig(JSON.parse(this._BREAKPTS));
    },

    /**
     * @api : initActions
     * assigns actions to all the widgets
     * @return : NA
     */
    initActions: function() {
      this.view.tbxMobile.onKeyUp = this.editFlowDynamicFieldKeyUp.bind(this);
      this.view.imgMobileInfo.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxInfoPhoneNumber);
      this.view.imgEmailInfo.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxInfoEmail);
      this.view.imgAddressLine1Info.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxInfoAddressLine1);
      this.view.imgAddressLine2Info.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxInfoAddressLine2);
      this.view.imgCountryInfo.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxInfoCountry);
      this.view.imgStateInfo.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxInfoState);
      this.view.imgCityInfo.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxInfoCity);
      this.view.imgZipCodeInfo.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxInfoZipCode);
      this.view.imgCloseInfoPhoneNumber.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxInfoPhoneNumber);
      this.view.imgCloseInfoEmail.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxInfoEmail);
      this.view.imgCloseInfoAddressLine1.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxInfoAddressLine1);
      this.view.imgCloseInfoAddressLine2.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxInfoAddressLine2);
      this.view.imgCloseInfoCountry.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxInfoCountry);
      this.view.imgCloseInfoState.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxInfoState);
      this.view.imgCloseInfoCity.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxInfoCity);
      this.view.imgCloseInfoZipCode.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxInfoZipCode);
      this.view.tbxAddressLine1.onEndEditing = this.setTextData.bind(this, this.view.tbxAddressLine1, "addressLine01");
      this.view.tbxAddressLine2.onEndEditing = this.setTextData.bind(this, this.view.tbxAddressLine2, "addressLine02");
      this.view.tbxCity.onEndEditing = this.setTextData.bind(this, this.view.tbxCity, "city");
      this.view.tbxZipcode.onEndEditing = this.setTextData.bind(this, this.view.tbxZipcode, "zipCode");
      this.view.tbxMobile.onEndEditing = this.setTextData.bind(this, this.view.tbxMobile, "phone");
      this.view.tbxEmail.onEndEditing = this.setTextData.bind(this, this.view.tbxEmail, "email");
    },

    /**
     * @api : setTextData
     * sets the data in textboxes to a context
     * @return : NA
     */
    setTextData: function(widgetName, key) {
      var widgetData = widgetName.text;
      var contextData = { "contextData" : widgetData,
                         "widgetName" : key
                        };

      this.setAddressData(contextData);
    },

    /**
     * @api : setCountryDataValue
     * sets the data in listboxes to a context
     * @return : NA
     */
    setCountryDataValue: function(widgetData, key) {  
      var contextData = { "contextData" : widgetData,
                         "widgetName" : key
                        };
      this.setAddressData(contextData);
    },

    /**
     * @api : editFlowDynamicFieldKeyUp
     * formats the text entered
     * @return : NA
     */
    editFlowDynamicFieldKeyUp:function() {
      var phoneNoFormatted = "";
      var text =  this.view.tbxMobile.text;
      var phoneNo = text;
      phoneNo = phoneNo.replace(/\+1/g, "");
      phoneNo = phoneNo.replace(/\(/g, "");
      phoneNo = phoneNo.replace(/\)/g, "");
      phoneNo = phoneNo.replace(/-/g, "");
      phoneNo = phoneNo.replace(/ /g, "");
      phoneNoFormatted= "+1 " + "(" + phoneNo.slice(0, 3) + ") " + phoneNo.slice(3, 6) + "-" + phoneNo.slice(6, 10);
      if(phoneNoFormatted == "+1 () -" || phoneNoFormatted == "+1 () " || phoneNoFormatted == "+1 "){
        phoneNoFormatted = "";
      }
      this.view.tbxMobile.text = phoneNoFormatted;
    },

    /**
     * @api : showInfoPopup
     * turns on flxInfo
     * @return : NA
     */
    showInfoPopup: function(flxInfo) {
      flxInfo.isVisible = true;
    },

    /**
     * @api : hideInfoPopup
     * turns off flxInfo
     * @return : NA
     */
    hideInfoPopup: function(flxInfo) {
      flxInfo.isVisible = false;
    },

    /**
     * @api : resetComponentData
     * resets tha data
     * @return : NA
     */
    resetComponentData: function() {
      this.parserUtilsManager.clearContext(this.textInputsMapping);
      this.textInputsMapping = {};
      this.componentContext = {};
      this.context = {};
      this.serviceCounter = 0;
      this.eventTriggered = "";
    },

    /**
     * @api : postShow
     * event called after ui rendered on the screen, is a component life cycle event.
     * @return : NA
     */
    postShow: function() {
      this.setSkins();
      this.setScreenData();
      this.getCountriesandStates();
      this.setImage();
    },

    /**
     * @api : getCountriesandStates
     * gets the list of countries and states
     * @return : NA
     */
    getCountriesandStates: function() {     
      this.invokeAddressCountryServiceMasterData();
      this.invokeAddressStateServiceMasterData();         
    },

    /**
     * @api : getCriteria
     * Parse the criteria based on accountType.
     * @param : criteria {string} - value collected from exposed contract
     * @return : {JSONObject} - jsonvalue for criteria
     */
    getCriteria: function(criteria) {
      var criteriaJSON;
      try {
        criteriaJSON = JSON.parse(criteria);
      }
      catch(e) {
        criteriaJSON = criteria;
        kony.print(e);
      }      
      for(var key in  criteriaJSON){
        if(typeof(criteriaJSON[key]) === "string")
          criteriaJSON[key] = this.parserUtilsManager.getParsedValue(criteriaJSON[key]);
        else
          criteriaJSON[key] = criteriaJSON[key];
      }
      return criteriaJSON;
    },

    /**
     * @api : invokeAddressCountryServiceMasterData
     * invoke service for retrieving list of countries
     * @return : NA
     */
    invokeAddressCountryServiceMasterData: function() {
      var self = this;
      var objSvcName = this.getParsedValue(this._addressObjectService);
      var objName = this.getParsedValue(this._countryObject);
      var operationName = this.getParsedValue(this._getCountriesOperation);
      var criteria = this.getCriteria(this._getCountriesCriteria);
      var identifier = this.getParsedValue(this._getCountriesIdentifier);
      this.addressDAO.fetchCountriesList
      (objSvcName,objName,operationName,criteria,this.onSuccessFetchCountriesList,identifier,self.onError);
    },

    /**
     * @api : onSuccessFetchCountriesList
     * success callback for retrieving countries list
     * @return : NA
     */
    onSuccessFetchCountriesList: function(response, unicode) {
      this._countriesList = response;
      this.serviceCounter++;
      if(this.serviceCounter === 2)
        this.setCountryAndStateMasterData();
    },

    /**
     * @api : setCountryAndStateMasterData
     * populates the list of countries and states
     * @return : NA
     */
    setCountryAndStateMasterData: function() {
      kony.application.dismissLoadingScreen();
      if(!this.view[countryWidget].isVisible) {
        return;
      }
      this.serviceCounter = 0;
      countryNew.push(["0", "Select a Country"]);
      this._countriesList.map(function(country) {
        return countryNew.push([country.id, country.Name]);
      });
      stateNew.push(["1", "Select a State"]);
      this._statesList.map(function(state) {
        return stateNew.push([state.id, state.Name, state.Country_id]);
      });
      this.setMasterDataBasedOnPayee();
    },

    /**
     * @api : setMasterDataBasedOnPayee
     * sets the list of countries and states in respective widgets based on payee type
     * @return : NA
     */
    setMasterDataBasedOnPayee: function() {
      if(enableProperty === "no") {
        var existingCountry = [];
        if(country)
          existingCountry.push(["0", country]);
        else
          existingCountry.push(["0", "Select Country"]);

        this.view[countryWidget].masterData = existingCountry;
        var selectedCountry = existingCountry[0][0];
        this.view[countryWidget].selectedKey = selectedCountry;

        var existingState = [];
        if(stateName)
          existingState.push(["0", stateName]);
        else
          existingState.push(["0", "Select State"]);

        this.view[stateWidget].masterData = existingState;
        var selectedState = existingState[0][0];
        this.view[stateWidget].selectedKey = selectedState;
      }
      else {      
        this.view[countryWidget].masterData = countryNew;
        this.countriesMasterData = countryNew;
        var countrySelected = countryNew[0][0];
        var stateSelected = stateNew[0][0];
        this.statesMasterData = stateNew;
        this.countryMasterData = countryNew;
        if(countryWidget === "lbxCountry") {
          this.view[countryWidget].selectedKey = countrySelected;  
        }
        else {
          this.highlightSelectedCountry(countryWidget);  
        }
        if(!countryId) {
          countryId = this.view[countryWidget].selectedKeyValue[0];
        }
        if (countryId === "0") {
          this.view[stateWidget].setEnabled(false);
        }
        if(countryId !== "0") {
          var data = this.getSpecifiedStates(countryId);
          this.statesMasterData = data.states;
          this.view[stateWidget].masterData = this.statesMasterData;
          this.view[countryWidget].selectedKey = countryId;
          if(stateId){
            if(stateId === "1") {
              this.view[stateWidget].selectedKey = stateId;
            }
            else {
              this.view[stateWidget].selectedKey = stateId;
            }
          }
        }
        var self = this;
        this.view[countryWidget].onSelection = function() {
          var data = [];
          countryId = self.view[countryWidget].selectedKeyValue[0];
          if (countryId === "0") {
            self.view[stateWidget].masterData = stateNew;
            self.view[stateWidget].selectedKey = stateSelected;
            self.view[countryWidget].selectedKey = countryId;
            self.view[stateWidget].setEnabled(false);
          } else {
            self.view[stateWidget].setEnabled(true);
            self.view[countryWidget].selectedKey = countryId;
            if (stateWidget === "listState") {
              self.updateContext("listCountry",self.view[countryWidget].selectedKeyValue[1]);
            }
            else {            
              self.updateContext("countryName", self.view[countryWidget].selectedKeyValue[1]);
              self.setCountryDataValue(self.view[countryWidget].selectedKeyValue[1], "country");          
            }
            data = self.getSpecifiedStates(countryId);
            self.view[stateWidget].masterData = data.states;
            self.view[stateWidget].selectedKey = data.stateSelected;
            stateId = self.view[stateWidget].selectedKeyValue[0];
            if (stateId === "1") {
              self.view[countryWidget].masterData = countryNew;
              self.view[countryWidget].selectedKey = countryId;
            }              
          }        
        };
        this.view[stateWidget].onSelection = function() {
          if (stateWidget === "listState") {
            self.updateContext("listState",self.view[stateWidget].selectedKeyValue[1]);
          }
          else {          
            self.updateContext("state", self.view[stateWidget].selectedKeyValue[1]);  
            self.setCountryDataValue(self.view[stateWidget].selectedKeyValue[1], "state");
            stateId = self.view[stateWidget].selectedKeyValue[0];      
          }        
        };
      }
    },

    /**
     * @api : invokeAddressStateServiceMasterData
     * invoke service for retrieving list of states of respective countries
     * @return : NA
     */
    invokeAddressStateServiceMasterData: function() {
      var self = this;
      var objSvcName = this.getParsedValue(this._addressObjectService);
      var objName = this.getParsedValue(this._statesObject);
      var operationName = this.getParsedValue(this._getStatesOperation);
      var criteria = this.getCriteria(this._getStatesCriteria);
      var identifier = this.getParsedValue(this._getStatesIdentifier);
      this.addressDAO.fetchStatesList
      (objSvcName,objName,operationName,criteria,this.onSuccessFetchStatesList,identifier,self.onError);
    },

    /**
     * @api : onSuccessFetchStatesList
     * success callback for retrieving states list
     * @return : NA
     */
    onSuccessFetchStatesList: function(response, unicode) {
      this._statesList = response;
      this.serviceCounter++;
      if(this.serviceCounter === 2)
        this.setCountryAndStateMasterData();
    },

    /**
     * @api : getSpecifiedStates
     * returns the states of the specific country selected
     * @return : list of states
     */
    getSpecifiedStates: function(addressId) {
      var self = this;
      var data = [];
      var statesList = [];
      statesList.push(["1", "Select a State"]);
      for (var i = 0; i < this._statesList.length; ++i) {
        if (this._statesList[i]["Country_id"] === addressId) {
          statesList.push([this._statesList[i]["id"], this._statesList[i]["Name"], this._statesList[i]["Country_id"]]);
        }
      }
      data = {
        "states": statesList,
        "stateSelected" : statesList[0][0]
      };
      return data;
    },

    onError: function() {

    },

    /**
     * @api : updateContext
     * updates context
     * @return : NA
     */
    updateContext: function(key, value) {
      this.componentContext[this.textInputsMapping[key]] = value;
      this.parserUtilsManager.setContext(this.componentContext);
    },

    /**
     * @api : populateValuesforEdit
     * populate the values for each text boxes on edit mode
     * @return : NA
     */
    populateValuesforEdit: function() {
      var mobileJSON = this.getParsedValue(this._phonenumberValue);     
      this.setTextBoxInputModeAndMasking(mobileJSON, "tbxMobile");
      this.mapTextBoxValueToContext(mobileJSON, "tbxMobile");
      this.setTextBoxPlaceHolder(mobileJSON, "tbxMobile");        
      this.setTextBoxToolTip(mobileJSON, "tbxMobile"); 
      this.populateTextIntoTextInput(mobileJSON, "tbxMobile");
      this.setTextBoxInfoIcon(mobileJSON, "imgMobileInfo", "lblPhoneNumberInfoHead", "lblPhoneNumberInfoBody");
      this.updateContext("tbxMobile",this.view.tbxMobile.text);

      var emailJSON = this.getParsedValue(this._emailValue);
      this.setTextBoxInputModeAndMasking(emailJSON, "tbxEmail");       
      this.mapTextBoxValueToContext(emailJSON, "tbxEmail");
      this.setTextBoxPlaceHolder(emailJSON, "tbxEmail");        
      this.setTextBoxToolTip(emailJSON, "tbxEmail");
      this.populateTextIntoTextInput(emailJSON, "tbxEmail");
      this.setTextBoxInfoIcon(emailJSON, "imgEmailInfo", "lblEmailInfoHead", "lblEmailInfoBody");
      this.updateContext("tbxEmail",this.view.tbxEmail.text);

      var addressline1JSON = this.getParsedValue(this._addressLine1Value); 
      this.setTextBoxInputModeAndMasking(addressline1JSON, "tbxAddressLine1");
      this.mapTextBoxValueToContext(addressline1JSON, "tbxAddressLine1");
      this.setTextBoxPlaceHolder(addressline1JSON, "tbxAddressLine1");        
      this.setTextBoxToolTip(addressline1JSON, "tbxAddressLine1");
      this.populateTextIntoTextInput(addressline1JSON, "tbxAddressLine1");
      this.setTextBoxInfoIcon(addressline1JSON, "imgAddressLine1Info", "lblAddressLine1InfoHead", "lblAddressLine1InfoBody");
      this.updateContext("tbxAddressLine1",this.view.tbxAddressLine1.text);

      var addressline2JSON = this.getParsedValue(this._addressLine2Value);     
      this.setTextBoxInputModeAndMasking(addressline2JSON, "tbxAddressLine2");
      this.mapTextBoxValueToContext(addressline2JSON, "tbxAddressLine2");
      this.setTextBoxPlaceHolder(addressline2JSON, "tbxAddressLine2");        
      this.setTextBoxToolTip(addressline2JSON, "tbxAddressLine2");
      this.populateTextIntoTextInput(addressline2JSON, "tbxAddressLine2");
      this.setTextBoxInfoIcon(addressline2JSON, "imgAddressLine2Info", "lblAddressLine2InfoHead", "lblAddressLine2InfoBody");
      this.updateContext("tbxAddressLine2",this.view.tbxAddressLine2.text);

      var lbxCountryJSON = this.getParsedValue(this._countryValue);      
      this.mapTextBoxValueToContext(lbxCountryJSON, "lbxCountry");
      this.setTextBoxInfoIcon(lbxCountryJSON, "imgCountryInfo", "lblCountryInfoHead", "lblCountryInfoBody");
      this.updateContext("lbxCountry",this.view.lbxCountry.selectedKeyValue[1]);

      var lbxStateJSON = this.getParsedValue(this._stateValue);     
      this.mapTextBoxValueToContext(lbxStateJSON, "lbxState");
      this.setTextBoxInfoIcon(lbxStateJSON, "imgStateInfo", "lblStateInfoHead", "lblStateInfoBody");
      this.updateContext("lbxState",this.view.lbxState.selectedKeyValue[1]);

      var cityJSON = this.getParsedValue(this._cityValue);
      this.setTextBoxInputModeAndMasking(cityJSON, "tbxCity");
      this.mapTextBoxValueToContext(cityJSON, "tbxCity");
      this.setTextBoxPlaceHolder(cityJSON, "tbxCity");        
      this.setTextBoxToolTip(cityJSON, "tbxCity");
      this.populateTextIntoTextInput(cityJSON, "tbxCity");
      this.setTextBoxInfoIcon(cityJSON, "imgCityInfo", "lblCityInfoHead", "lblCityInfoBody");
      this.updateContext("tbxCity",this.view.tbxCity.text);

      var zipcodeJSON = this.getParsedValue(this._zipcodeValue);      
      this.setTextBoxInputModeAndMasking(zipcodeJSON, "tbxZipcode");
      this.mapTextBoxValueToContext(zipcodeJSON, "tbxZipcode");
      this.setTextBoxPlaceHolder(zipcodeJSON, "tbxZipcode");        
      this.setTextBoxToolTip(zipcodeJSON, "tbxZipcode");  
      this.populateTextIntoTextInput(zipcodeJSON, "tbxZipcode");
      this.setTextBoxInfoIcon(zipcodeJSON, "imgZipCodeInfo", "lblZipCodeInfoHead", "lblZipCodeInfoBody");
      this.updateContext("tbxZipcode",this.view.tbxZipcode.text);
    },

    /**
     * @api : setScreenData
     * sets value for the widgets from contract configurations
     * @return : NA
     */
    setScreenData: function() {
      this.view.lblMobile.text = this.getLabelText(this._phonenumberLabel);
      this.view.lblEmail.text = this.getLabelText(this._emailLabel);
      this.view.lblAddressLine1.text = this.getLabelText(this._addressLine1Label);
      this.view.lblAddressLine2.text = this.getLabelText(this._addressLine2Label);
      this.view.lblCountry.text = this.getLabelText(this._countryLabel);
      this.view.lblState.text = this.getLabelText(this._stateLabel);
      this.view.lblCity.text = this.getLabelText(this._cityLabel);
      this.view.lblZipcode.text = this.getLabelText(this._zipcodeLabel);
      if(!this.addressUtility.isNullOrUndefinedOrEmpty(this.getParsedValue(this._payeeId))){
        this.populateValuesforEdit();
      }
      else {
        var mobileJSON = this.getParsedValue(this._phonenumberValue);
        if(mobileJSON) {
          this.setTextBoxInputModeAndMasking(mobileJSON, "tbxMobile");
          this.setTextBoxMaximumLength(mobileJSON, "tbxMobile");
          this.mapTextBoxValueToContext(mobileJSON, "tbxMobile");
          this.setTextBoxPlaceHolder(mobileJSON, "tbxMobile");        
          this.setTextBoxToolTip(mobileJSON, "tbxMobile");
          this.setTextBoxInfoIcon(mobileJSON, "imgMobileInfo", "lblPhoneNumberInfoHead", "lblPhoneNumberInfoBody");
        }
        else {
          this.view.flxMobile.isVisible = false;
          this.view.tbxMobile.isVisible = false;
        }
        var emailJSON = this.getParsedValue(this._emailValue);
        if(emailJSON) {
          this.setTextBoxInputModeAndMasking(emailJSON, "tbxEmail");
          this.setTextBoxMaximumLength(emailJSON, "tbxEmail");
          this.mapTextBoxValueToContext(emailJSON, "tbxEmail");
          this.setTextBoxPlaceHolder(emailJSON, "tbxEmail");        
          this.setTextBoxToolTip(emailJSON, "tbxEmail");
          this.setTextBoxInfoIcon(emailJSON, "imgEmailInfo", "lblEmailInfoHead", "lblEmailInfoBody");
           if(!this.view.flxMobile.isVisible)
            this.view.flxEmail.left = "0dp";
        }
        else {
          this.view.flxEmail.isVisible = false;
          this.view.tbxEmail.isVisible = false;
        }
        var addressline1JSON = this.getParsedValue(this._addressLine1Value);
        if(addressline1JSON) {
          this.setTextBoxInputModeAndMasking(addressline1JSON, "tbxAddressLine1");
          this.setTextBoxMaximumLength(addressline1JSON, "tbxAddressLine1");
          this.mapTextBoxValueToContext(addressline1JSON, "tbxAddressLine1");
          this.setTextBoxPlaceHolder(addressline1JSON, "tbxAddressLine1");        
          this.setTextBoxToolTip(addressline1JSON, "tbxAddressLine1");
          this.setTextBoxInfoIcon(addressline1JSON, "imgAddressLine1Info", "lblAddressLine1InfoHead", "lblAddressLine1InfoBody");
        }
        else {
          this.view.flxAddressLine1.isVisible = false;
          this.view.tbxAddressLine1.isVisible = false;
        }
        var addressline2JSON = this.getParsedValue(this._addressLine2Value);
        if(addressline2JSON) {
          this.setTextBoxInputModeAndMasking(addressline2JSON, "tbxAddressLine2");
          this.setTextBoxMaximumLength(addressline2JSON, "tbxAddressLine2");
          this.mapTextBoxValueToContext(addressline2JSON, "tbxAddressLine2");
          this.setTextBoxPlaceHolder(addressline2JSON, "tbxAddressLine2");        
          this.setTextBoxToolTip(addressline2JSON, "tbxAddressLine2");
          this.setTextBoxInfoIcon(addressline2JSON, "imgAddressLine2Info", "lblAddressLine2InfoHead", "lblAddressLine2InfoBody");
        }
        else {
          this.view.flxAddressLine2.isVisible = false;
          this.view.tbxAddressLine2.isVisible = false;
        }
        var lbxCountryJSON = this.getParsedValue(this._countryValue);
        if(lbxCountryJSON) {
          this.mapTextBoxValueToContext(lbxCountryJSON, "lbxCountry");
          this.setTextBoxInfoIcon(lbxCountryJSON, "imgCountryInfo", "lblCountryInfoHead", "lblCountryInfoBody");
        }
        else {
          this.view.flxCountry.isVisible = false;
        }
        var lbxStateJSON = this.getParsedValue(this._stateValue);
        if(lbxStateJSON) {
          this.mapTextBoxValueToContext(lbxStateJSON, "lbxState");
          this.setTextBoxInfoIcon(lbxStateJSON, "imgStateInfo", "lblStateInfoHead", "lblStateInfoBody");
           if(!this.view.flxCountry.isVisible)
            this.view.flxState.left = "0dp";
        }
        else {
          this.view.flxState.isVisible = false;
        }
        var cityJSON = this.getParsedValue(this._cityValue);
        if(cityJSON) {
          this.setTextBoxInputModeAndMasking(cityJSON, "tbxCity");
          this.setTextBoxMaximumLength(cityJSON, "tbxCity");
          this.mapTextBoxValueToContext(cityJSON, "tbxCity");
          this.setTextBoxPlaceHolder(cityJSON, "tbxCity");        
          this.setTextBoxToolTip(cityJSON, "tbxCity");
          this.setTextBoxInfoIcon(cityJSON, "imgCityInfo", "lblCityInfoHead", "lblCityInfoBody");
        }
        else {
          this.view.flxCity.isVisible = false;
          this.view.tbxCity.isVisible = false;
        }
        var zipcodeJSON = this.getParsedValue(this._zipcodeValue);
        if(zipcodeJSON) {
          this.setTextBoxInputModeAndMasking(zipcodeJSON, "tbxZipcode");
          this.setTextBoxMaximumLength(zipcodeJSON, "tbxZipcode");
          this.mapTextBoxValueToContext(zipcodeJSON, "tbxZipcode");
          this.setTextBoxPlaceHolder(zipcodeJSON, "tbxZipcode");        
          this.setTextBoxToolTip(zipcodeJSON, "tbxZipcode");
          this.setTextBoxInfoIcon(zipcodeJSON, "imgZipCodeInfo", "lblZipCodeInfoHead", "lblZipCodeInfoBody");
          if(!this.view.flxCity.isVisible)
            this.view.flxZipcode.left = "0dp";
        }
        else {
          this.view.flxZipcode.isVisible = false;
          this.view.tbxZipcode.isVisible = false;
        }
        this.updateContext("tbxMobile",this.view.tbxMobile.text);
        this.updateContext("tbxEmail",this.view.tbxEmail.text); 
        this.updateContext("tbxAddressLine1",this.view.tbxAddressLine1.text);  
        this.updateContext("tbxAddressLine2",this.view.tbxAddressLine2.text);
        this.updateContext("tbxCity",this.view.tbxCity.text);
        this.updateContext("tbxZipcode",this.view.tbxZipcode.text); 
      }
    },

    /**
     * setTextBoxInputModeAndMasking
     * @api : setTextBoxInputModeAndMasking
     * sets the input mode of the text box based on contract config
     * @return : NA
     */
    setTextBoxInputModeAndMasking: function(contractJSON , srcWidget) {
      if(!this.addressUtility.isNullOrUndefinedOrEmpty(contractJSON.inputMode)) {
        if(contractJSON.inputMode === "NUMERIC"){
          this.view[srcWidget].restrictCharactersSet = 
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~!@#$%^&*()_-\\?/+={[]}:;,.<>'`|\"";          
        }        
        else {
          this.view[srcWidget].restrictCharactersSet = "";                      
        }
        return;
      }      
    },

    /**
     * populateTextIntoTextInput
     * @api : populateTextIntoTextInput
     * sets the data from the contractJSON to source widget
     * @return : NA
     */
    populateTextIntoTextInput: function(contractJSON, srcWidget) {
      if(!this.addressUtility.isNullOrUndefinedOrEmpty(contractJSON)) {
        var contractJSONValue = this.getParsedValue
        (contractJSON);
        var contextValue = this.getParsedValue(contractJSONValue.mapping);
        this.view[srcWidget].text = contextValue;
      }
    },

    /**
     * mapTextBoxValueToContext
     * @api : mapTextBoxValueToContext
     * maps the value of textbox to the context assigned in contracts
     * @return : NA
     */
    mapTextBoxValueToContext: function(contractJSON, textBoxID) {
      if(!this.addressUtility.isNullOrUndefinedOrEmpty(contractJSON) && !this.addressUtility.isNullOrUndefinedOrEmpty(contractJSON.mapping)){
        var inputMapper = contractJSON.mapping.substring(5,contractJSON.mapping.length-1);
        this.textInputsMapping[textBoxID] = inputMapper;
      }
    },

    /**
     * setTextBoxToolTip
     * @api : setTextBoxToolTip
     * maps the tooltip assigned in contracts
     * @return : NA
     */
    setTextBoxToolTip: function(contractJSON, tbxWidget) {
      if(contractJSON.tooltip) {
        this.view[tbxWidget].toolTip = this.getParsedValue
        (contractJSON.tooltip,kony.application.getCurrentBreakpoint());
      }
    },

    /**
     * setTextBoxMaximumLength
     * @api : setTextBoxMaximumLength
     * assigns maximum characters in textbox
     * @return : NA
     */
    setTextBoxMaximumLength: function(contractJSON , srcWidget) {
      if(!this.addressUtility.isNullOrUndefinedOrEmpty(contractJSON.maxLength)) {
        var contractJSONValue = this.getParsedValue(contractJSON);
        var contextValue = this.getParsedValue(contractJSONValue.maxLength);
        this.view[srcWidget].maxTextLength = contextValue;
      }      
    },

    /**
     * setTextBoxInfoIcon
     * @api : setTextBoxInfoIcon
     * sets the details into info flex on click of info icon
     * @return : NA
     */
    setTextBoxInfoIcon: function(contractJSON, imgWidget, headerWidget, labelWidget) {
      if(contractJSON.infoIconText) {        
        this.view[imgWidget] ? this.view[imgWidget].isVisible = true : kony.print("widget not present");
        var infoIconHeader = this.getParsedValue
        (contractJSON.infoIconText.header,kony.application.getCurrentBreakpoint());
        var infoIconText = this.getParsedValue
        (contractJSON.infoIconText.text,kony.application.getCurrentBreakpoint());
        this.view[headerWidget] ? this.view[headerWidget].text = infoIconHeader : kony.print("widget not present");
        this.view[labelWidget] ? this.view[labelWidget].text = infoIconText : kony.print("widget not present");
      }
      else {
        this.view[imgWidget] ? this.view[imgWidget].isVisible = false : kony.print("widget not present");
      }
    },

    /**
     * setTextBoxPlaceHolder
     * @api : setTextBoxPlaceHolder
     * maps the value of textbox to the placeholder assigned in contracts
     * @return : NA
     */
    setTextBoxPlaceHolder: function(contractJSON, tbxWidget) {
      if(!this.addressUtility.isNullOrUndefinedOrEmpty(contractJSON.placeHolder)) {
        var placeHolderValue = this.getParsedValue
        (contractJSON.placeHolder,kony.application.getCurrentBreakpoint());
        this.view[tbxWidget].placeholder =
          placeHolderValue ? placeHolderValue : "";
      }
    },

    /**
     * getLabelText
     * @api : getLabelText
     * parses label texts based on contract configurations
     * @return : NA
     */
    getLabelText: function(contractJSON) {
      var labelText = this.getParsedValue(contractJSON,kony.application.getCurrentBreakpoint());
      return labelText ? labelText : "";
    },

    /**
     * getParsedValue
     * @api : getParsedValue
     * parses the property and fetches the corresponding value
     * @return : NA
     */
    getParsedValue: function(property, selectedValue) {
      try{
        property = JSON.parse(property);
      }
      catch(e){
        property = property;
        kony.print(e);
      }
      if(typeof(property) === "string"){
        return this.getProcessedText(property);
      }

      else{
        if(selectedValue)
          return this.getProcessedText(this.parserUtilsManager.getComponentConfigParsedValue(property,selectedValue));
        else
          return property;
      }

    },

    /**
     * @api : getProcessedText
     * helper method to invoke parser utility functions to get the parsed value.
     * @param : text{object} -value collected from exposed contract
     * @return : parsed value result
     */
    getProcessedText: function(text) {     
      return this.parserUtilsManager.getParsedValue(text);          
    },

    /**
     * @api : onBreakPointChange
     * Triggers when break point change takes place.
     * @return : NA
     */
    onBreakPointChange: function() {
      this.setScreenData();
      this.resetComponentData();
      this.setSkins();
      this.setImage();
      this.eventTriggered = "BP";
      //   this.getCountriesandStates();
      this.eventTriggered = "";
    },

    /**
     * @api : setContext
     * To collect the context object required for the component. 
     * @param : context{JSONobject} - account object 
     * @return : NA
     */
    setContext: function(context, scope) {
      this.context = context;
      if(this.parentScope === ""){
        this.parentScope = scope;
      }
      this.parserUtilsManager.setContext(this.context);
    },

    /**
     * setSkins
     * @api : setSkins
     * event called to set the skins based on the contracts.
     * @return : NA
     */
    setSkins: function() {
      this.view.lblMobile.skin = this.getParsedValue(this._addressLabel, kony.application.getCurrentBreakpoint());
      this.view.lblEmail.skin = this.getParsedValue(this._addressLabel, kony.application.getCurrentBreakpoint());
      this.view.lblAddressLine1.skin = this.getParsedValue(this._addressLabel, kony.application.getCurrentBreakpoint());
      this.view.lblAddressLine2.skin = this.getParsedValue(this._addressLabel, kony.application.getCurrentBreakpoint());
      this.view.lblCountry.skin = this.getParsedValue(this._addressLabel, kony.application.getCurrentBreakpoint());
      this.view.lblState.skin = this.getParsedValue(this._addressLabel, kony.application.getCurrentBreakpoint());
      this.view.lblCity.skin = this.getParsedValue(this._addressLabel, kony.application.getCurrentBreakpoint());
      this.view.lblZipcode.skin = this.getParsedValue(this._addressLabel, kony.application.getCurrentBreakpoint());
      this.view.tbxMobile.skin = this.getParsedValue(this._addressTextBox, kony.application.getCurrentBreakpoint());
      this.view.tbxEmail.skin = this.getParsedValue(this._addressTextBox, kony.application.getCurrentBreakpoint());
      this.view.tbxAddressLine1.skin = this.getParsedValue(this._addressTextBox, kony.application.getCurrentBreakpoint());
      this.view.tbxAddressLine2.skin = this.getParsedValue(this._addressTextBox, kony.application.getCurrentBreakpoint());
      this.view.tbxCity.skin = this.getParsedValue(this._addressTextBox, kony.application.getCurrentBreakpoint());
      this.view.tbxZipcode.skin = this.getParsedValue(this._addressTextBox, kony.application.getCurrentBreakpoint());
      this.view.tbxMobile.focusSkin = this.getParsedValue(this._addressTextBox, kony.application.getCurrentBreakpoint());
      this.view.tbxEmail.focusSkin = this.getParsedValue(this._addressTextBox, kony.application.getCurrentBreakpoint());
      this.view.tbxAddressLine1.focusSkin = this.getParsedValue(this._addressTextBox, kony.application.getCurrentBreakpoint());
      this.view.tbxAddressLine2.focusSkin = this.getParsedValue(this._addressTextBox, kony.application.getCurrentBreakpoint());
      this.view.tbxCity.focusSkin = this.getParsedValue(this._addressTextBox, kony.application.getCurrentBreakpoint());
      this.view.tbxZipcode.focusSkin = this.getParsedValue(this._addressTextBox, kony.application.getCurrentBreakpoint());
      this.view.tbxMobile.placeholderSkin = this.getParsedValue(this._addressPlaceholderTextBox, kony.application.getCurrentBreakpoint());      
      this.view.tbxEmail.placeholderSkin = this.getParsedValue(this._addressPlaceholderTextBox, kony.application.getCurrentBreakpoint());      
      this.view.tbxAddressLine1.placeholderSkin = this.getParsedValue(this._addressPlaceholderTextBox, kony.application.getCurrentBreakpoint());      
      this.view.tbxAddressLine2.placeholderSkin = this.getParsedValue(this._addressPlaceholderTextBox, kony.application.getCurrentBreakpoint());
      this.view.tbxCity.placeholderSkin = this.getParsedValue(this._addressPlaceholderTextBox, kony.application.getCurrentBreakpoint());
      this.view.tbxZipcode.placeholderSkin = this.getParsedValue(this._addressPlaceholderTextBox, kony.application.getCurrentBreakpoint());
      this.view.lbxCountry.skin = this.getParsedValue(this._addressListBox, kony.application.getCurrentBreakpoint());
      this.view.lbxState.skin = this.getParsedValue(this._addressListBox, kony.application.getCurrentBreakpoint());
      this.view.lblAddressLine1InfoHead.skin = this.getParsedValue(this._sknInfoHead, kony.application.getCurrentBreakpoint());
      this.view.lblAddressLine1InfoBody.skin = this.getParsedValue(this._sknInfoContent, kony.application.getCurrentBreakpoint());
      this.view.lblAddressLine2InfoHead.skin = this.getParsedValue(this._sknInfoHead, kony.application.getCurrentBreakpoint());
      this.view.lblAddressLine2InfoBody.skin = this.getParsedValue(this._sknInfoContent, kony.application.getCurrentBreakpoint());
      this.view.lblEmailInfoHead.skin = this.getParsedValue(this._sknInfoHead, kony.application.getCurrentBreakpoint());
      this.view.lblEmailInfoBody.skin = this.getParsedValue(this._sknInfoContent, kony.application.getCurrentBreakpoint());
      this.view.lblCountryInfoHead.skin = this.getParsedValue(this._sknInfoHead, kony.application.getCurrentBreakpoint());
      this.view.lblCountryInfoBody.skin = this.getParsedValue(this._sknInfoContent, kony.application.getCurrentBreakpoint());
      this.view.lblPhoneNumberInfoHead.skin = this.getParsedValue(this._sknInfoHead, kony.application.getCurrentBreakpoint());
      this.view.lblPhoneNumberInfoBody.skin = this.getParsedValue(this._sknInfoContent, kony.application.getCurrentBreakpoint());
      this.view.lblStateInfoHead.skin = this.getParsedValue(this._sknInfoHead, kony.application.getCurrentBreakpoint());
      this.view.lblStateInfoBody.skin = this.getParsedValue(this._sknInfoContent, kony.application.getCurrentBreakpoint());
      this.view.lblCityInfoHead.skin = this.getParsedValue(this._sknInfoHead, kony.application.getCurrentBreakpoint());
      this.view.lblCityInfoBody.skin = this.getParsedValue(this._sknInfoContent, kony.application.getCurrentBreakpoint());
      this.view.lblZipCodeInfoHead.skin = this.getParsedValue(this._sknInfoHead, kony.application.getCurrentBreakpoint());
      this.view.lblZipCodeInfoBody.skin = this.getParsedValue(this._sknInfoContent, kony.application.getCurrentBreakpoint());
      disabledSkin = this.getParsedValue(this._sknTextBoxDisabled, kony.application.getCurrentBreakpoint());
    },

    /**
     * setImage
     * @api : setImage
     * sets image based on contract configurations
     * @return : NA
     */
    setImage: function() {
      var infoImage = this.getParsedImgSource(this._imgInfo);
      var infoCloseImage = this.getParsedImgSource(this._imgInfoClose);
      if(infoImage) {
        this.view.imgMobileInfo.src = infoImage.img;
        this.view.imgEmailInfo.src = infoImage.img;
        this.view.imgAddressLine1Info.src = infoImage.img;
        this.view.imgAddressLine2Info.src = infoImage.img;
        this.view.imgCountryInfo.src = infoImage.img;
        this.view.imgStateInfo.src = infoImage.img;
        this.view.imgCityInfo.src = infoImage.img;
        this.view.imgZipCodeInfo.src = infoImage.img;
      }

      if(infoCloseImage) {
        this.view.imgCloseInfoAddressLine1.src = infoCloseImage.img;
        this.view.imgCloseInfoAddressLine2.src = infoCloseImage.img;
        this.view.imgCloseInfoPhoneNumber.src = infoCloseImage.img;
        this.view.imgCloseInfoEmail.src = infoCloseImage.img;
        this.view.imgCloseInfoCountry.src = infoCloseImage.img;
        this.view.imgCloseInfoState.src = infoCloseImage.img;
        this.view.imgCloseInfoCity.src = infoCloseImage.img;
        this.view.imgCloseInfoZipCode.src = infoCloseImage.img;
      }
    },

    /**
         * Component isEmptyNullUndefined
         * Verifies if the value is empty, null or undefined
         * data {string} - value to be verified
         * @return : {boolean} - validity of the value passed
         */
    isEmptyNullUndefined: function(data) {
      if (data === null || data === undefined || data === "")
        return true;

      return false;
    },

    /**
     * getParsedImgSource
     * @api : getParsedImgSource
     * parses the property and fetches the corresponding Value
     * @return : NA
     */
    getParsedImgSource: function(property) {
      try {
        property=JSON.parse(property);
      }
      catch(e) {        
        kony.print(e);
      }      
      return property;
    },

    /**
     * getAddressData
     * @api : getAddressData
     * gets address data from parent component
     * @return : NA
     */
    getAddressData: function(context) {
      addressLine1 = context.AddressLine01;
      addressLine2 = context.AddressLine02;
      city = context.City;
      stateName = context.State;
      country = context.Country;
      zipCode = context.ZipCode;
      phoneNumber = context.PhoneNumber;
      emailAddress = context.EmailAddress;

      this.view.tbxAddressLine1.text = addressLine1;
      this.view.tbxAddressLine2.text = addressLine2;
      this.view.tbxCity.text = city;
      this.view.tbxZipcode.text = zipCode;
      this.view.tbxMobile.text = phoneNumber;
      this.view.tbxEmail.text = emailAddress;

      enableProperty = context.isEnabled;

      if(enableProperty === "no") {
        this.view.tbxAddressLine1.setEnabled(false);
        this.view.tbxAddressLine2.setEnabled(false);
        this.view.tbxCity.setEnabled(false);
        this.view.tbxZipcode.setEnabled(false);
        this.view.lbxCountry.setEnabled(false);
        this.view.lbxState.setEnabled(false);
        this.view.tbxEmail.setEnabled(false);
        this.view.tbxMobile.setEnabled(false);
        this.view.tbxAddressLine1.skin = disabledSkin;
        this.view.tbxAddressLine2.skin = disabledSkin;
        this.view.tbxCity.skin = disabledSkin;
        this.view.tbxZipcode.skin = disabledSkin;
        this.view.lbxCountry.skin = disabledSkin;
        this.view.lbxState.skin = disabledSkin;
        this.view.tbxEmail.skin = disabledSkin;
        this.view.tbxMobile.skin = disabledSkin;
      }
      if(enableProperty === "yes") {
        this.view.tbxAddressLine1.setEnabled(true);
        this.view.tbxAddressLine2.setEnabled(true);
        this.view.tbxCity.setEnabled(true);
        this.view.tbxZipcode.setEnabled(true);
        this.view.lbxCountry.setEnabled(true);
        this.view.lbxState.setEnabled(true);
        this.view.tbxEmail.setEnabled(true);
        this.view.tbxMobile.setEnabled(true);
        this.view.tbxMobile.skin = this.getParsedValue(this._addressTextBox, kony.application.getCurrentBreakpoint());
        this.view.tbxEmail.skin = this.getParsedValue(this._addressTextBox, kony.application.getCurrentBreakpoint());
        this.view.tbxAddressLine1.skin = this.getParsedValue(this._addressTextBox, kony.application.getCurrentBreakpoint());
        this.view.tbxAddressLine2.skin = this.getParsedValue(this._addressTextBox, kony.application.getCurrentBreakpoint());
        this.view.tbxCity.skin = this.getParsedValue(this._addressTextBox, kony.application.getCurrentBreakpoint());
        this.view.tbxZipcode.skin = this.getParsedValue(this._addressTextBox, kony.application.getCurrentBreakpoint());
        this.view.lbxCountry.skin = this.getParsedValue(this._addressListBox, kony.application.getCurrentBreakpoint());
        this.view.lbxState.skin = this.getParsedValue(this._addressListBox, kony.application.getCurrentBreakpoint());
      }
      this.setMasterDataBasedOnPayee();
    },

    /**
     * resetData
     * @api : resetData
     * resets data
     * @return : NA
     */
    resetData: function() {
      countryId = 0;
      stateId = 0;
      enableProperty = "yes";
      this.view.tbxAddressLine1.setEnabled(true);
      this.view.tbxAddressLine2.setEnabled(true);
      this.view.tbxCity.setEnabled(true);
      this.view.tbxZipcode.setEnabled(true);
      this.view.lbxCountry.setEnabled(true);
      this.view.lbxState.setEnabled(true);
      this.view.tbxEmail.setEnabled(true);
      this.view.tbxMobile.setEnabled(true);
      this.view.tbxAddressLine1.text = "";
      this.view.tbxAddressLine2.text = "";
      this.view.tbxCity.text = "";
      this.view.tbxZipcode.text = "";
      this.view.tbxEmail.text = "";
      this.view.tbxMobile.text = "";
      this.view.tbxMobile.skin = this.getParsedValue(this._addressTextBox, kony.application.getCurrentBreakpoint());
      this.view.tbxEmail.skin = this.getParsedValue(this._addressTextBox, kony.application.getCurrentBreakpoint());
      this.view.tbxAddressLine1.skin = this.getParsedValue(this._addressTextBox, kony.application.getCurrentBreakpoint());
      this.view.tbxAddressLine2.skin = this.getParsedValue(this._addressTextBox, kony.application.getCurrentBreakpoint());
      this.view.tbxCity.skin = this.getParsedValue(this._addressTextBox, kony.application.getCurrentBreakpoint());
      this.view.tbxZipcode.skin = this.getParsedValue(this._addressTextBox, kony.application.getCurrentBreakpoint());
      this.view.lbxCountry.skin = this.getParsedValue(this._addressListBox, kony.application.getCurrentBreakpoint());
      this.view.lbxState.skin = this.getParsedValue(this._addressListBox, kony.application.getCurrentBreakpoint());
      this.setMasterDataBasedOnPayee();
    }
  };
});