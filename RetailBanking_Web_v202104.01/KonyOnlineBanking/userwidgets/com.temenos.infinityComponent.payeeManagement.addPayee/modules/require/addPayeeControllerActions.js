define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for Button0c6ce6608aa4349 **/
    AS_Button_cc10e58ef1504da2b9f7fd753843eff1: function AS_Button_cc10e58ef1504da2b9f7fd753843eff1(eventobject) {
        var self = this;
        return self.contractBasedNavigation.call(this);
    },
    /** onBreakpointChange defined for addPayee **/
    AS_FlexContainer_e544ed5835e84efd8ed3da697ecad2c0: function AS_FlexContainer_e544ed5835e84efd8ed3da697ecad2c0(eventobject, breakpoint) {
        var self = this;
        this.onBreakPointChange();
    },
    /** preShow defined for addPayee **/
    AS_FlexContainer_a1fff180bc2f47e8be6f6b18b72f496c: function AS_FlexContainer_a1fff180bc2f47e8be6f6b18b72f496c(eventobject) {
        var self = this;
        this.preShow();
    },
    /** postShow defined for addPayee **/
    AS_FlexContainer_ee4325275d934bdca0ec4220b3ff5f4a: function AS_FlexContainer_ee4325275d934bdca0ec4220b3ff5f4a(eventobject) {
        var self = this;
        this.postShow();
    }
});