define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnTermsAndConditions **/
    AS_Button_aace2d0fbdc54c96a954056f17555448: function AS_Button_aace2d0fbdc54c96a954056f17555448(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    },
    /** onClick defined for btnFaqs **/
    AS_Button_d95c06e539ec40dba82103bfed0ef05c: function AS_Button_d95c06e539ec40dba82103bfed0ef05c(eventobject) {
        var self = this;
        this.showFAQs();
    },
    /** onClick defined for btnPrivacy **/
    AS_Button_ddcf8af3eaec4104a53d1c0140f0da84: function AS_Button_ddcf8af3eaec4104a53d1c0140f0da84(eventobject) {
        var self = this;
        this.showPrivacyPolicyPage();
    },
    /** onClick defined for btnContactUs **/
    AS_Button_h019ee9ecba34d13a36899bd3076b192: function AS_Button_h019ee9ecba34d13a36899bd3076b192(eventobject) {
        var self = this;
        this.showContactUsPage();
    },
    /** onClick defined for btnLocateUs **/
    AS_Button_j72b9de6e1a04646b7a3af0dc7da1b4a: function AS_Button_j72b9de6e1a04646b7a3af0dc7da1b4a(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** postShow defined for customfooternew **/
    AS_FlexContainer_d1bb849f7da14fcbb222ef0aa4c82412: function AS_FlexContainer_d1bb849f7da14fcbb222ef0aa4c82412(eventobject) {
        var self = this;
        this.postshow();
    }
});