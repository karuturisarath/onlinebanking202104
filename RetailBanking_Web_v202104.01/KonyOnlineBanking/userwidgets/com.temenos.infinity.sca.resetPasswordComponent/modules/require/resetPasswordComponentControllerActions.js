define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnSetPassword **/
    AS_Button_d956b1f493764602bfabdab500c880a6: function AS_Button_d956b1f493764602bfabdab500c880a6(eventobject) {
        var self = this;

        function INVOKE_SERVICE_h1e59ad582eb4ca79721905427f7fb0b_Callback(ActivationCodeAuthenticator) {
            var opstatus = ActivationCodeAuthenticator.opstatus;
            var errorCode = ActivationCodeAuthenticator.errorCode;
            if (opstatus !== 0) {
                self.view.lblPwdErrorMsg.setVisibility(true);
                if (errorCode !== '' && errorCode == 2100) self.view.lblPwdErrorMsg.text = "Invalid password, doesn't meet password constraints";
                self.view.forceLayout();
            } else {
                self.navigateTo('flxPasswordSuccess');
            }
            kony.application.dismissLoadingScreen();
        }
        kony.application.showLoadingScreen(null, null, constants.LOADING_SCREEN_POSITION_FULL_SCREEN, true, true, {});
        var authReqId = cibaRequestId;
        if (self.view.flxPasswordGen.flxPassword.tbxPassword.text == self.view.flxPasswordGen.flxConfirmPassword.tbxConfirmPassword.text) {
            if (ActivationCodeAuthenticator_inputparam == undefined) {
                var ActivationCodeAuthenticator_inputparam = {};
            }
            ActivationCodeAuthenticator_inputparam["serviceID"] = "SCAActivationObjects$ActivationCodeAuthenticator$resetPassword";
            ActivationCodeAuthenticator_inputparam["options"] = {
                "access": "online",
                "CRUD_TYPE": "resetPassword"
            };
            var data = {};
            data["password"] = self.view.tbxPassword.text;
            data["authReqId"] = authReqId;
            ActivationCodeAuthenticator_inputparam["options"]["data"] = data;
            var ActivationCodeAuthenticator_httpheaders = {};
            ActivationCodeAuthenticator_inputparam["httpheaders"] = ActivationCodeAuthenticator_httpheaders;
            var ActivationCodeAuthenticator_httpconfigs = {};
            ActivationCodeAuthenticator_inputparam["httpconfig"] = ActivationCodeAuthenticator_httpconfigs;
            SCAActivationObjects$ActivationCodeAuthenticator$resetPassword = mfobjectsecureinvokerasync(ActivationCodeAuthenticator_inputparam, "SCAActivationObjects", "ActivationCodeAuthenticator", INVOKE_SERVICE_h1e59ad582eb4ca79721905427f7fb0b_Callback);
        } else {
            self.view.lblPwdErrorMsg.setVisibility(true);
            self.view.lblPwdErrorMsg.text = "Password and Confirm Password values doesn't match";
            self.view.forceLayout();
        }
    },
    /** preShow defined for resetPasswordComponent **/
    AS_FlexContainer_d8c69cedb9f444cba6ef7f68676d0314: function AS_FlexContainer_d8c69cedb9f444cba6ef7f68676d0314(eventobject) {
        var self = this;
        return self.preshow.call(this);
    },
    /** onBreakpointChange defined for resetPasswordComponent **/
    AS_FlexContainer_ce23cbebda84483a87d7f818a9aead8b: function AS_FlexContainer_ce23cbebda84483a87d7f818a9aead8b(eventobject, breakpoint) {
        var self = this;
        return self.onBreakpointChange.call(this);
    }
});