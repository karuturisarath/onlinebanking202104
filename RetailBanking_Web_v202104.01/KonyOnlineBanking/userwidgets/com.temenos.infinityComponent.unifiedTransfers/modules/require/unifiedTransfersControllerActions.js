define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onRowClick defined for segTransferCCYList **/
    AS_Segment_ef85c48f225e408d8783fd51f77e63e0: function AS_Segment_ef85c48f225e408d8783fd51f77e63e0(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return self.setDropdownSelectedValue.call(this, this.view.segTransferCCYList, this.view.lblSelectedTransferCCY, this.view.imgTransferCCYDropdownIcon, this.view.flxTransferCCYList);
    },
    /** onClick defined for flxFrequencyDropdown **/
    AS_FlexContainer_bfd3c3fb37184d10912b2750807a6e51: function AS_FlexContainer_bfd3c3fb37184d10912b2750807a6e51(eventobject) {
        var self = this;
        return self.toggleDropdownVisibility.call(this, this.view.flxFrequencyDropdown, this.view.flxFrequencyList, this.view.imgFrequencyDropdownIcon);
    },
    /** onRowClick defined for segFrequencyList **/
    AS_Segment_j4dc948bef414b8e96432557d3e937e5: function AS_Segment_j4dc948bef414b8e96432557d3e937e5(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return self.onFrequencySelection.call(this, this.view.segFrequencyList, this.view.lblSelectedFrequency, this.view.imgFrequencyDropdownIcon, this.view.flxFrequencyList);
    },
    /** onClick defined for flxTransferDurationTypeDropdown **/
    AS_FlexContainer_e360b14a31ae40089984863824e6d586: function AS_FlexContainer_e360b14a31ae40089984863824e6d586(eventobject) {
        var self = this;
        return self.toggleDropdownVisibility.call(this, this.view.flxTransferDurationTypeDropdown, this.view.flxTransferDurationTypeList, this.view.imgTransferDurationTypeDropdownIcon);
    },
    /** onRowClick defined for segTransferDurationList **/
    AS_Segment_jbd55b60bffc4c729f245b1d94db0f33: function AS_Segment_jbd55b60bffc4c729f245b1d94db0f33(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return self.onTransferDurationSelection.call(this, this.view.segTransferDurationList, this.view.lblSelectedTransferDurationType, this.view.imgTransferDurationTypeDropdownIcon, this.view.flxTransferDurationTypeList);
    },
    /** preShow defined for unifiedTransfers **/
    AS_FlexContainer_bdeb4f34f25844b38477ae98412c32cb: function AS_FlexContainer_bdeb4f34f25844b38477ae98412c32cb(eventobject) {
        var self = this;
        this.preshow();
    },
    /** postShow defined for unifiedTransfers **/
    AS_FlexContainer_c8a962b4ca1b4e34b45ea248a8150fe8: function AS_FlexContainer_c8a962b4ca1b4e34b45ea248a8150fe8(eventobject) {
        var self = this;
        return self.postShow.call(this);
    },
    /** onBreakpointChange defined for unifiedTransfers **/
    AS_FlexContainer_g23945f5bd4d4317a0d832e40b2de761: function AS_FlexContainer_g23945f5bd4d4317a0d832e40b2de761(eventobject, breakpoint) {
        var self = this;
        return self.onBreakPointChange.call(this);
    },
    /** onClick defined for flxTransferCCYDropdown **/
    AS_FlexContainer_fd78badd8e1946198a0282845c712158: function AS_FlexContainer_fd78badd8e1946198a0282845c712158(eventobject) {
        var self = this;
        var self = this;
        return self.toggleDropdownVisibility.call(this, this.view.flxTransferCCYDropdown, this.view.flxTransferCCYList, this.view.imgTransferCCYDropdownIcon);
    }
});