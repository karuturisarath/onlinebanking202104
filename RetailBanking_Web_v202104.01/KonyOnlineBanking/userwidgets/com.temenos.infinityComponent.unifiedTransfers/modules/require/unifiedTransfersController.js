define(['./ParserUtilsManager','./UnifiedTransferDAO','./UnifiedTransfersUtility','FormatUtil','InfinityComponents/DataValidationFramework/DataValidationHandler'],function(ParserUtilsManager, UnifiedTransferDAO, UnifiedTransfersUtility, FormatUtil, DataValidationHandler) {
  return {
    constructor: function(baseConfig, layoutConfig, pspConfig) {
      this.existingPayeeAddressLine01 = "";
      this.existingPayeeAddressLine02 = "";
      this.existingPayeeCity = "";
      this.existingPayeeState = "";
      this.existingPayeeCountry = "";
      this.existingPayeeZipCode = "";
      this.existingPayeePhoneNumber = "";
      this.existingPayeeEmailAddress = "";
      this.newPayeeAddressLine01 = "";
      this.newPayeeAddressLine02 = "";
      this.newPayeeCity = "";
      this.newPayeeState = "";
      this.newPayeeCountry = "";
      this.newPayeeZipCode = "";
      this.newPayeePhoneNumber = "";
      this.newPayeeEmailAddress = "";
      this.newPayeeData1 = "";
      this.newPayeeData2 = "";
      this.newPayeeData3 = "";
      this.newPayeeData4 = "";
      this.existingPayeeData4 = "";
      this.existingPayeeData3 = "";
      this.existingPayeeData2 = "";
      this.existingPayeeData1 = "";
      this.creditOrLoan = ["txtBoxTransferAmount"];
      this.newPayee = ["txtBoxPayeeName","txtBoxNewAccountNumber","txtBoxReenterAccountNumber"];
      this.newPayeeP2P = ["txtBoxPayeeDetailField1","txtBoxPayeeDetailField2"];
      this.IBAN = "";
      this.isDVFValidated = "";
      this.isIBANValid = "";
      this.payeeTypeContext ="";
      this.dueTypes = "";
      this.paymentMethod = "";
      this.isPaidBy = "";
      this.uploadedAttachments = [];
      this.attachments = [];
      this.count = 0;
      this.docsServiceFormat = "";
      this.filesToBeUploaded = [];
      this.fromAccountsList=[];
      this.toAccountsList=[];					   
      this.creditCardAccounts=[];						
      this.parserUtilsManager = new ParserUtilsManager();
      this.unifiedTransferDAO = new UnifiedTransferDAO();
      this.unifiedTransfersUtility = new UnifiedTransfersUtility();
      this.formatUtil = new FormatUtil();
      this.dataValidationHandler = new DataValidationHandler();
      this.AccountsCurrencyList = [];
      this.fromAccount = "";
      this.toAccount = "";
      this.transferCurrency = "";	
      this.frequency = "Once";	  
      this.fromAccountAPISuccess = false;
      this.toAccountAPISuccess = false;
      this.crAccountAPISuccess = false;
      this.isServiceDone = 0;
      this.todayDate="";
      this.dueType="";
      this.dueAmount = "";	  

      this._sknFlexEnabled="";

      this._sknFlexDisabled="";						  
      this._currencyListSrc = "";
	  this._fromAccountActionKey="";							
      this._currencyList="";
      //declaration for Flow Type  in the group:General
      this._fromAccountListArray="";						

      this._FLOWTYPES="";
      this._sknTxtBoxFocus="";

      this._sknACEmptyRecordText="";

		this._sknACToDropdownField3 = "";							   
      this._sknACEmptyRecordActionText="";

      this._fromAccountGroupIdentifier="";

      this._amountFormat="";

      this._cityBankIcon="";
      this._chaseBankIcon="";
      this._BAOBankIcon="";
      this._HDFCBankIcon="";
      this._infinityBankIcon="";
      this._externalBankIcon="";					  
      this._backendDateFormat="";

      this._lblPayeeDetailField4="";
      this._txtInputPayeeDetail4="";								


      this._getAccountObjectServiceName="";
      this._getAccountObjectName="";
      this._getAccountOperationName="";
      this._getAccountCriteria="";
      this._getAccountListObject="";

      this._crAccountObjectServiceName="";
      this._crAccountObjectName="";
      this._crAccountOperationName="";
      this._crAccountscriteria="";
      this._crAccountListObject="";

      this._IBANSwiftObjectServiceName="";
      this._IBANSwiftObjectName="";
      this._IBANSwiftOperationName="";
      this._IBANSwiftCriteria="";

      //declaration for Object Name in the group:Bank Date Service
      this._bankDateObjectName="";


      //declaration for Operation Name in the group:Bank Date Service
      this._bankDateOperationName="";

      //declaration for Service Name in the group:Bank Date Service
      this._bankDateServiceName="";

      //declaration for Service Response Identifier in the group:Bank Date Service
      this._bankDateServiceResponseIdentifier="";

      //declaration for Object Name in the group:Data validation Service
      this._dvObjName="";
      //declaration for Operation Name in the group:Data validation Service
      this._dvOpName="";

      //declaration for Service Name in the group:Data validation Service
      this._dvObjServiceName="";

      //declaration for Address Line 1 in the group:Backend Address Data
      this._addressLine1Value="";

      //declaration for Address Line 2 in the group:Backend Address Data
      this._addressLine2Value="";

      //declaration for City in the group:Backend Address Data
      this._cityValue="";

      //declaration for State in the group:Backend Address Data
      this._stateValue="";

      //declaration for Country in the group:Backend Address Data
      this._countryValue="";

      //declaration for Zip Code in the group:Backend Address Data
      this._zipCodeValue="";

      //declaration for Phone Number in the group:Backend Address Data
      this._phoneNumberValue="";

      //declaration for Email Address in the group:Backend Address Data
      this._emailAddressValue="";

      //declaration for Text Area Skin in the group:Skins
      this._sknTextArea="";

      //declaration for Service Response Identifier in the group:Data validation Service
      this._dvIdentifier="";

      //declaration for Service Response Identifier in the group:Data validation Service
      this._dvCriteria="";

      //declaration for Data Validation Config in the group:Data Validation
      this._dvfConfig="";

      //declaration for Support Document Visibility in the group:Supporting Documents
      this._supportDocVisibility="";

      //declaration for Frequency Visibility in the group:Frequency
      this._frequencyVisibility="";

      //declaration for Text Box Flex Skin in the group:Skins
      this._sknTextBoxFlex="";

      //declaration for From Account Label in the group:From Account
      this._lblFromAccount="";

      //declaration for To Account Label in the group:To Account
      this._lblToAccount="";

      //declaration for Currency  Dropdown Values in the group:Currency Field
      this._dropdownTransferCurrency="";

      //declaration for Transfer Amount  Text Input Label in the group:Transfer Amount
      this._lblTransferAmount="";

      //declaration for FX Rate Reference Text Input  Label in the group:FX Rate Reference
      this._lblfxRateReference="";

      //declaration for Frequency Dropdown Label in the group:Frequency
      this._lblFrequency="";

      //declaration for Datepicker 1 Label in the group:Transfer Date
      this._lblDatepicker1="";

      //declaration for Payment Method Radio Input Label in the group:Payment Method
      this._lblPaymentMethod="";

      //declaration for Fees Paid Radio Input Label in the group:Fees Paid
      this._lblFeesPaid="";

      //declaration for Mandatory Text Box Hover Skin in the group:Skins
      this._sknMandatoryTextBox="";

      //declaration for Text Box Focus Skin in the group:Skins
      this._sknTextBoxFocus="";

      //declaration for Intermediary BIC Text Input  Label in the group:Intermediary BIC
      this._lblIntermediaryBIC="";

      //declaration for E2E Text Input  Label in the group:E2E
      this._lblE2E="";

      //declaration for Notes Text Input  Label in the group:Notes
      this._lblNotes="";

      //declaration for Field  Label in the group:Additional Field
      this._lblField="";

      //declaration for Button1 in the group:Action Buttons
      this._button1="";

      //declaration for Supporting Documents Label in the group:Supporting Documents
      this._lblSupportingDocuments="";

      //declaration for attachmentError1 Label in the group:Supporting Documents
      this._attachmentError1="";

      //declaration for attachmentError2 Label in the group:Supporting Documents
      this._attachmentError2="";

      //declaration for attachmentError3 Label in the group:Supporting Documents
      this._attachmentError3="";

      //declaration for attachmentError4 Label in the group:Supporting Documents
      this._attachmentError4="";

      //declaration for attachmentError5 Label in the group:Supporting Documents
      this._attachmentError5="";

      //declaration for Add Payee Address Label in the group:Payee Address Fields
      this._lblAddPayeeAddress="";

      this._lblPayeeAddressOptional = "";

      //declaration for Header in the group:SWIFT Lookup 
      this._swiftLookupHeader="";

      //declaration for Error Text Input Skin in the group:Skins
      this._sknErrorTextInput="";

      //declaration for Dropdown Expand Icon in the group:Icons
      this._dropdownExpandIcon="";

      //declaration for Object Name in the group:From Accounts List Service
      this._fromAccountObjectName="";

      //declaration for Object Service Name in the group:To Accounts List Service
      this._toAccountObjectServiceName="";

      //declaration for Object Name in the group:SWIFT Lookup Service
      this._lookupObjectName="";

      //declaration for Currency Dropdown Label in the group:Currency Field
      this._lblTransferCurrency="";

      //declaration for Segregation Types in the group:General
      this._SEGREGATIONTYPES="";

      //declaration for Payee Type Context in the group:General
      this._payeeType="";

      //declaration for Minimum Fill Mapping in the group:Data Validation
      this._minFillMapping="";

      //declaration for Maximum Fill Mapping in the group:Data Validation
      this._maxFillMapping="";

      //declaration for From Account Text Input in the group:From Account
      this._fromAccountTextInput="";

      //declaration for Payee Type 1 Option in the group:To Account
      this._payeeTypeOption1="";

      //declaration for Payment Amount Option1 in the group:Payment Amount
      this._paymentAmountOption1="";

      //declaration for Transfer Amount Text Input  Value in the group:Transfer Amount
      this._txtInputTransferAmount="";

      //declaration for FX Rate Reference Text Input Value in the group:FX Rate Reference
      this._txtInputfxRateReference="";

      //declaration for Frequency Dropdown  Value in the group:Frequency
      this._dropdownFrequency="";

      //declaration for Datepicker1 Value in the group:Transfer Date
      this._datepicker1Value="";

      //declaration for Payee Info Icon in the group:Icons
      this._payeeInfoIcon="";

      //declaration for Payment Method Option1 in the group:Payment Method
      this._paymentMethodOption1="";

      //declaration for Fees Paid Option1 in the group:Fees Paid
      this._feesPaidOption1="";

      //declaration for Intermediary BIC Text Input Value in the group:Intermediary BIC
      this._txtInputIntermediaryBIC="";

      //declaration for E2E Text Input  Value in the group:E2E
      this._txtInputE2E="";

      //declaration for Notes Text Input Value in the group:Notes
      this._txtInputNotes="";

      //declaration for Dropdown Field  Values in the group:Additional Field
      this._dropdownFieldValues="";

      //declaration for Button2 in the group:Action Buttons
      this._button2="";

      //declaration for Supported File Format in the group:Supporting Documents
      this._supportedFileFormat="";

      //declaration for New Payee Name Text Input Value in the group:Payee Detail Fields
      this._txtInputNewPayeeName="";

      //declaration for Description in the group:SWIFT Lookup 
      this._swiftLookupDescription="";

      //declaration for Field Label Skin in the group:Skins
      this._sknFieldLabel="";
      //declaration for Text Box Disabled Skin in the group:Skins
      this._sknTextBoxDisabled="";														 

      //declaration for Dropdown Collapse Icon in the group:Icons
      this._dropdownCollapseIcon="";

      //declaration for Operation Name in the group:From Accounts List Service
      this._fromAccountOperationName="";

      //declaration for Object Name in the group:To Accounts List Service
      this._toAccountObjectName="";

      //declaration for Operation Name in the group:SWIFT Lookup Service
      this._lookupOperationName="";

      //declaration for Frequency Type in the group:General
      this._FREQUENCYTYPES="";

      //declaration for Selected Account Detail1 in the group:From Account
      this._lblFromAccountDetail1="";

      //declaration for Payee Type 2 Option in the group:To Account
      this._payeeTypeOption2="";

      //declaration for Payment Amount Option2 in the group:Payment Amount
      this._paymentAmountOption2="";

      //declaration for Exchange Rate Label in the group:FX Rate Reference
      this._lblExchangeRate="";

      //declaration for Frequency Type  Label in the group:Frequency
      this._lblFrequencyType="";

      //declaration for Datepicker 2 Label in the group:Transfer Date
      this._lblDatepicker2=""; 

      //declaration for Datepicker 2 Label in the group:Transfer Date
      this._lblDatepicker3="";

      //declaration for Payment Method Option2 in the group:Payment Method
      this._paymentMethodOption2="";

      //declaration for Fees Paid Option2 in the group:Fees Paid
      this._feesPaidOption2="";

      //declaration for Transfer Flow in the group:General
      this._transferFlow="";

      //declaration for Label Field  Value in the group:Additional Field
      this._lblFieldValue="";

      //declaration for SelectedFile in the group:Supporting Documents
      this._selectedFileValue="";

      //declaration for Account Number Text Input Label in the group:Payee Detail Fields
      this._lblNewPayeeAccountNumber="";

      //declaration for Search Field 1 Label in the group:SWIFT Lookup 
      this._searchField1Label="";

      //declaration for Field Value Skin in the group:Skins
      this._sknFieldValue="";

      //declaration for Close Icon in the group:Icons
      this._closeIcon="";

      //declaration for Criteria in the group:From Accounts List Service
      this._fromAccountsCriteria="";

      //declaration for Operation Name in the group:To Accounts List Service
      this._toAccountOperationName="";

      //declaration for Service Name in the group:SWIFT Lookup Service
      this._lookupServiceName="";

      //declaration for Occurrence Type in the group:General
      this._OCCURRENCETYPES="";

      //declaration for Listing Account Types in the group:From Account
      this._accountTypesList="";

      //declaration for To Account Text Input in the group:To Account
      this._toAccountTextInput="";

      //declaration for Payment Amount Option3 in the group:Payment Amount
      this._paymentAmountOption3="";

      //declaration for Frequency Type Dropdown Value in the group:Frequency
      this._dropdownFrequencyType="";
      //declaration for Frequency to remove Value in the group:Frequency
      this._frequencyToRemove = "";

      //declaration for Datepicker2 Value in the group:Transfer Date
      this._datepicker2Value="";
      //declaration for Datepicker3 Value in the group:Transfer Date
      this._datepicker3Value="";

      //declaration for Payment Method Option3 in the group:Payment Method
      this._paymentMethodOption3="";

      //declaration for Fees Paid Option3 in the group:Fees Paid
      this._feesPaidOption3="";

      //declaration for Object Name in the group:Data validation
      this._jsonObjName="";

      //declaration for Text Input Field  Value in the group:Additional Field
      this._textInputValue="";

      //declaration for Account Number Text Input Value in the group:Payee Detail Fields
      this._txtInputNewPayeeAccountNumber="";

      //declaration for Search Field 1 Value in the group:SWIFT Lookup 
      this._searchField1Value="";

      //declaration for Lookup Label Skin in the group:Skins
      this._sknLookupLabel="";

      //declaration for Attach Documents Icon in the group:Icons
      this._attachDocumentsIcon="";

      //declaration for Service Response Identifier in the group:From Accounts List Service
      this._fromAccountListObject="";

      //declaration for Criteria in the group:To Accounts List Service
      this._toAccountscriteria="";

      //declaration for Service Response Identifier in the group:SWIFT Lookup Service
      this._lookupServiceResponseIdentifier="";

      //declaration for Payee Type in the group:General
      this._PAYEETYPES="";

      //declaration for Dropdown List Field 1 in the group:From Account
      this._lblFromAccountDropdownListField1="";

      //declaration for Selected Account Detail 1 in the group:To Account
      this._toAccountTypeList="";

      //declaration for Payment Amount Option4 in the group:Payment Amount
      this._paymentAmountOption4="";

      //declaration for Recurrence Text Input Label in the group:Transfer Date
      this._lblRecurrence="";

      //declaration for Payment Method Option4 in the group:Payment Method
      this._paymentMethodOption4="";

      //declaration for Radio Input  Field  Value in the group:Additional Field
      this._radioInputIValue="";

      //declaration for ReEnter Account Number Text Input Label in the group:Payee Detail Fields
      this._lblReenterAccountNumber="";

      //declaration for Search Field 2 Label in the group:SWIFT Lookup 
      this._searchField2Label="";

      //declaration for Radio Input Selected Text Skin in the group:Skins
      this._sknradioInputSelected="";

      //declaration for Payee Address Icon in the group:Icons
      this._payeeAddressIcon="";

      //declaration for Object Service Name in the group:From Accounts List Service
      this._fromAccountObjectServiceName="";

      //declaration for Service Response Identifier in the group:To Accounts List Service
      this._toAccountListObject="";

      //declaration for Account Type in the group:General
      this._ACCOUNTTYPES="";

      //declaration for Transfer Type in the group:General
      this._transferType="";

      //declaration for Dropdown List Field 2 in the group:From Account
      this._lblFromAccountDropdownListField2="";

      //declaration for Dropdown List Field 1 in the group:To Account
      this._lblToAccountDropdownField1="";

      //declaration for txtBoxOtherAmount in the group:Payment Amount
      this._otherAmountTxtBox="";

      //declaration for Recurrence Text Input Value in the group:Transfer Date
      this._txtInputRecurrences="";

      //declaration for Payment Method Option5 in the group:Payment Method
      this._paymentMethodOption5="";

      //declaration for ReEnter Account Number Text Input Value in the group:Payee Detail Fields
      this._txtInputReenterAccountNumber="";

      //declaration for Search Field 2 Value in the group:SWIFT Lookup 
      this._searchField2Value="";

      //declaration for Radio Input Text Skin in the group:Skins
      this._sknradioInputText="";

      //declaration for Text Box Placeholder Skin in the group:Skins
      this._sknTextBoxPlaceholder="";

      //declaration for Info Icon in the group:Icons
      this._infoIcon="";

      //declaration for Breakpoints in the group:General
      this._BREAKPTS="";

      //declaration for Dropdown List Field 3 in the group:From Account
      this._lblFromAccountDropdownListField3="";

      //declaration for Dropdown List Field 2 in the group:To Account
      this._lblToAccountDropdownField2="";

      //declaration for Payee Detail Field Info Label in the group:Payee Detail Fields
      this._lblPayeeDetailFieldInfo="";

      //declaration for Search Field 3 Label in the group:SWIFT Lookup 
      this._searchField3Label="";

      //declaration for Dropdown Text Skin in the group:Skins
      this._sknDropdownText="";

      //declaration for PDF  Icon in the group:Icons
      this._pdfIcon="";

      //declaration for Error Message Label in the group:General
      this._lblErrorMessage="";

      //declaration for Empty Record  Messge in the group:From Account
      this._lblFromAccountEmptyRecord="";

      //declaration for Dropdown List Field 3 in the group:To Account
      this._lblToAccountDropdownField3="";

      //declaration for Lookup Label in the group:Payee Detail Fields
      this._lblLookup="";

      //declaration for Search Field 3 Value in the group:SWIFT Lookup 
      this._searchField3Value="";

      //declaration for Secondary Button Skin in the group:Skins
      this._sknSecondaryBtn="";

      //declaration for JPEG  Icon in the group:Icons
      this._jpegIcon="";

      //declaration for Empty Record Action Button in the group:From Account
      this._btnFromAccountEmptyRecord="";

      //declaration for Empty Record Message in the group:To Account
      this._lblToAccountEmptyRecord="";

      //declaration for Payee Detail1 Text Input Label in the group:Payee Detail Fields
      this._lblPayeeDetail1="";

      //declaration for Search Field 4 Label in the group:SWIFT Lookup 
      this._searchField4Label="";

      //declaration for Secondary Button Hover Skin in the group:Skins
      this._sknSecondaryBtnHover="";

      //declaration for Clear Icon Autocomplete Dropdown in the group:Icons
      this._clearIcon="";

      //declaration for Empty Record Action Button in the group:To Account
      this._btnToAccountEmptyRecordAction="";

      //declaration for Payee Detail1 Text Input Value in the group:Payee Detail Fields
      this._txtInputPayeeDetail1="";

      //declaration for Search Field 4 Value in the group:SWIFT Lookup 
      this._searchField4Value="";

      //declaration for Secondary Button Focus Skin in the group:Skins
      this._sknSecondaryBtnFocus="";

      //declaration for Enabled Button Skin in the group:Skins
      this._sknEnabledBtn="";

      //declaration for Disabled Button Skin in the group:Skins
      this._sknDisabledBtn="";

      //declaration for Loader Icon Autocomplete Dropdown in the group:Icons
      this._loaderIcon="";

      //declaration for Dropdown List Icon1 in the group:From Account
      this._fromAccountDropdownListIcon1="";

      //declaration for Payee Detail2 Text Input Label in the group:Payee Detail Fields
      this._lblPayeeDetailField2="";

      //declaration for Column1 Label in the group:SWIFT Lookup 
      this._lblcolumn1="";

      //declaration for Primary Button Disabled Skin in the group:Skins
      this._sknPrimaryBtnDisabled="";

      //declaration for Dropdown List Icon2 in the group:From Account
      this._fromAccountDropdownListIcon2="";

      //declaration for Dropdown List Icon1 in the group:To Account
      this._toAccountGroupIdentifier="";

      //declaration for Payee Detail2 Text Input Value in the group:Payee Detail Fields
      this._txtInputPayeeDetail2="";

      //declaration for Column2 label  in the group:SWIFT Lookup 
      this._lblcolumn2="";

      //declaration for Primary Button Skin in the group:Skins
      this._sknPrimaryBtn="";

      //declaration for Dropdown List Icon2 in the group:To Account
      this._toAccountListArray="";

      //declaration for Payee Detail3 Text Input Label in the group:Payee Detail Fields
      this._lblPayeeDetailField3="";

      //declaration for column1 Label Value in the group:SWIFT Lookup 
      this._lblColumn1Value="";

      //declaration for Primary Button Hover Skin in the group:Skins
      this._sknPrimaryBtnHover="";

      //declaration for Payee Detail3 Text Input Value in the group:Payee Detail Fields
      this._txtInputPayeeDetail3="";

      //declaration for column2 Label Value in the group:SWIFT Lookup 
      this._lblColumn2Value="";

      //declaration for Primary Button Focus Skin in the group:Skins
      this._sknPrimaryBtnFocus="";

      //declaration for column3 Label Value in the group:SWIFT Lookup 
      this._lblColumn3Value="";

      //declaration for AutoComplete Dropdown Field 1 in the group:Skins
      this._sknACDropdownField1="";

      //declaration for Search Button in the group:SWIFT Lookup 
      this._btnSearch="";

      //declaration for AutoComplete Dropdown Field 2 in the group:Skins
      this._sknACDropdownField2="";

      //declaration for AutoComplete Dropdown Field 3 in the group:Skins
      this._sknACDropdownField3="";

      //declaration for AutoComplete Dropdown Field Type in the group:Skins
      this._sknLblDropdownFieldType="";

      //declaration for Payee Detail Info Text Skin Field Type in the group:Skins
      this._sknPayeeDetailInfoText="";

      //declaration for Swift Lookup Select Label Skin in the group:Skins
      this._sknSelectLabel="";

      this._radioButtonAction = "";

      this._removeAttachDocumentIcon = "";

      this._uploadDocumentErrorIcon = "";

      this._sknradioIconSelected = "";

      this._sknradioIconUnselected = "";
      
      this._sknradioIconDisabled = "";
      
      this._emptySearchResult = "";
      //declaration for read only field value Skin in the group:Skins
      this._sknFieldReadOnlyValue = "";

      this._fileConfiguration;

      //declaration for IBANObjectServiceName in the group: IBAN Validate Service
      this._IBANObjectServiceName ="";

      //declaration for IBANObjectName in the group: IBAN Validate Service
      this._IBANObjectName ="";

      //declaration for IBANOperationName in the group: IBAN Validate Service
      this._IBANOperationName ="";

      //declaration for IBANCriteria in the group: IBAN Validate Service
      this._IBANCriteria ="";

      //declaration for getBeneficiaryService in the group: Get beneficiary Name Service
      this._getBeneficiaryService ="";

      //declaration for getBeneficiaryObject in the group: Get beneficiary Name Service
      this._getBeneficiaryObject ="";

      //declaration for getBeneficiaryOperation in the group: Get beneficiary Name Service
      this._getBeneficiaryOperation ="";

      //declaration for getBeneficiaryCriteria in the group: Get beneficiary Name Service
      this._getBeneficiaryCriteria ="";

      //declaration for Maximum Character of Notes in the group: Notes
      this._maxCharacterNotes = "";

      //declaration for Info Head Skin in the group: Skins
      this._sknInfoHead = "";

      //declaration for Info Text Skin in the group: Skins
      this._sknInfoText = "";
      
      //declaration for baseCurrency in the group: General
      this._baseCurrency = "";

      this.context = {};
      //To map text box context with textbox id
      this.textInputsMapping = {};
      //To store collected user input data from form fields
      this.dataContext = {};
      //To set transfer type from context
      this.transferTypeContext = "";
      //To set Account type from context
      this.accountTypeContext = "";

      this.formScope = "";
      this.Skins = {
        "FieldLabel" : "",
        "TxtBoxField" : "",
        "FieldValue" : "",
        "ErrorField" : "",
        "LookupLabel" : "",
        "RadioInputSelectedTxt" : "",
        "RadioInputTxt" : "",
        "DropdownTxt" : "",
        "PrimaryBtn" : "",
        "PrimaryBtnHover" : "",
        "PrimaryBtnFocus" : "",
        "SecondayBtn" : "",
        "SecondaryBtnHover" : "",
        "SecondaryBtnFocus" : "",
        "InfoHeader" : "",
        "InfoDesc" : "",
        "PayeeDetailFieldInfo" : "",
        "selectedRadioButtonSkin" : "",
        "unseletedRadioBtnSkn" : "",
        "EnabledBtnSkin": "",
        "DisabledBtnSkin": "",
        "EnabledFlex" : "",
        "DisabledFlex" : ""				
      };
      this.Icons = {
        "DropdownExpand" : "",
        "DropdownCollapse" : "",
        "CloseIcon" : "",
        "AttachDocuments" : "",
        "AddAddress" : "",
        "InfoIcon" : "",
        "PDFIcon" : "",
        "JPEGIcon" : "",
        "RemoveAttachmentIcon" : "",
        "selectedRadioButton" : "",        
        "unSelectedRadioButton" : "",
        "UploadDocumentErrorIcon" : "",
        "clearIcon" : ""

      };


    },
    //Logic for getters/setters of custom properties
    initGettersSetters: function() {		

      defineSetter(this, "IBANSwiftObjectServiceName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._IBANSwiftObjectServiceName=val;
        }
      });


      defineGetter(this, "IBANSwiftObjectServiceName", function() {
        return this._IBANSwiftObjectServiceName;
      });

      defineSetter(this, "sknFlexEnabled", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknFlexEnabled=val;
        }
      });


      defineGetter(this, "sknFlexEnabled", function() {
        return this._sknFlexEnabled;
      });


      defineSetter(this, "sknFlexDisabled", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknFlexDisabled=val;
        }
      });


      defineGetter(this, "sknFlexDisabled", function() {
        return this._sknFlexDisabled;
      });


      defineSetter(this, "IBANSwiftObjectName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._IBANSwiftObjectName=val;
        }
      });


      defineGetter(this, "IBANSwiftObjectName", function() {
        return this._IBANSwiftObjectName;
      });

      defineSetter(this, "IBANSwiftOperationName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._IBANSwiftOperationName=val;
        }
      });


      defineGetter(this, "IBANSwiftOperationName", function() {
        return this._IBANSwiftOperationName;
      });

      defineSetter(this, "IBANSwiftCriteria", function(val) {
        if((typeof val=='string') && (val != "")){
          this._IBANSwiftCriteria=val;
        }
      });


      defineGetter(this, "IBANSwiftCriteria", function() {
        return this._IBANSwiftCriteria;
      });
      defineSetter(this, "crAccountObjectServiceName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._crAccountObjectServiceName=val;
        }
      });


      defineGetter(this, "crAccountObjectServiceName", function() {
        return this._crAccountObjectServiceName;
      });

      defineSetter(this, "crAccountObjectName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._crAccountObjectName=val;
        }
      });


      defineGetter(this, "crAccountObjectName", function() {
        return this._crAccountObjectName;
      });

      defineSetter(this, "crAccountOperationName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._crAccountOperationName=val;
        }
      });


      defineGetter(this, "crAccountOperationName", function() {
        return this._crAccountOperationName;
      });

      defineSetter(this, "crAccountscriteria", function(val) {
        if((typeof val=='string') && (val != "")){
          this._crAccountscriteria=val;
        }
      });


      defineGetter(this, "crAccountscriteria", function() {
        return this._crAccountscriteria;
      });

      defineSetter(this, "crAccountListObject", function(val) {
        if((typeof val=='string') && (val != "")){
          this._crAccountListObject=val;
        }
      });


      defineGetter(this, "crAccountListObject", function() {
        return this._crAccountListObject;
      });

      defineSetter(this, "getAccountObjectServiceName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._getAccountObjectServiceName=val;
        }
      });


      defineGetter(this, "getAccountObjectServiceName", function() {
        return this._getAccountObjectServiceName;
      });

      defineSetter(this, "getAccountObjectName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._getAccountObjectName=val;
        }
      });


      defineGetter(this, "getAccountObjectName", function() {
        return this._getAccountObjectName;
      });

      defineSetter(this, "getAccountOperationName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._getAccountOperationName=val;
        }
      });


      defineGetter(this, "getAccountOperationName", function() {
        return this._getAccountOperationName;
      });

      defineSetter(this, "getAccountCriteria", function(val) {
        if((typeof val=='string') && (val != "")){
          this._getAccountCriteria=val;
        }
      });


      defineGetter(this, "getAccountCriteria", function() {
        return this._getAccountCriteria;
      });

      defineSetter(this, "getAccountListObject", function(val) {
        if((typeof val=='string') && (val != "")){
          this._getAccountListObject=val;
        }
      });


      defineGetter(this, "getAccountListObject", function() {
        return this._getAccountListObject;
      });
      //setter method for Object Name in the group:Bank Date Service
      defineSetter(this, "bankDateObjectName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._bankDateObjectName=val;
        }
      });

      //getter method for Object Name in the group:Bank Date Service
      defineGetter(this, "bankDateObjectName", function() {
        return this._bankDateObjectName;
      });

      //setter method for Object Name in the group:Data validation
      defineSetter(this, "jsonObjName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._jsonObjName=val;
        }
      });

      //getter method for Object Name in the group:Bank Date Service
      defineGetter(this, "jsonObjName", function() {
        return this._jsonObjName;
      });

      //setter method for Payee Info Icon in the group:Icons
      defineSetter(this, "payeeInfoIcon", function(val) {
        if((typeof val=='string') && (val != "")){
          this._payeeInfoIcon=val;
        }
      });

      //getter method for Payee Info Icon in the group:Icons
      defineGetter(this, "payeeInfoIcon", function() {
        return this._payeeInfoIcon;
      });

      //setter method for Info Head Skin in the group: Skins
      defineSetter(this, "sknInfoHead", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknInfoHead=val;
        }
      });

      //getter method for Info Head Skin in the group: Skins
      defineGetter(this, "sknInfoHead", function() {
        return this._sknInfoHead;
      });

      //setter method for Payee Detail Info Text Skin Field Type in the group:Skins
      defineSetter(this, "sknPayeeDetailInfoText", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknPayeeDetailInfoText=val;
        }
      });

      //getter method for Payee Detail Info Text Skin Field Type in the group:Skins
      defineGetter(this, "sknPayeeDetailInfoText", function() {
        return this._sknPayeeDetailInfoText;
      });

      //setter method for Info Text Skin in the group: Skins
      defineSetter(this, "sknInfoText", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknInfoText=val;
        }
      });

      //getter method for Info Text Skin in the group: Skins
      defineGetter(this, "sknInfoText", function() {
        return this._sknInfoText;
      });

      //setter method for Maximum Character of Notes in the group:Notes
      defineSetter(this, "maxCharacterNotes", function(val) {
        if((typeof val=='string') && (val != "")){
          this._maxCharacterNotes=val;
        }
      });

      //getter method for Maximum Character of Notes in the group:Notes
      defineGetter(this, "maxCharacterNotes", function() {
        return this._maxCharacterNotes;
      });

      //setter method for Operation Name in the group:Bank Date Service
      defineSetter(this, "bankDateOperationName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._bankDateOperationName=val;
        }
      });

      //getter method for Operation Name in the group:Bank Date Service
      defineGetter(this, "bankDateOperationName", function() {
        return this._bankDateOperationName;
      });



      //setter method for Service Name in the group:Bank Date Service
      defineSetter(this, "bankDateServiceName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._bankDateServiceName=val;
        }
      });

      //getter method for Service Name in the group:Bank Date Service
      defineGetter(this, "bankDateServiceName", function() {
        return this._bankDateServiceName;
      });

      //setter method for Service Response Identifier in the group:Bank Date Service
      defineSetter(this, "bankDateServiceResponseIdentifier", function(val) {
        if((typeof val=='string') && (val != "")){
          this._bankDateServiceResponseIdentifier=val;
        }
      });

      //getter method for Service Response Identifier in the group:Bank Date Service
      defineGetter(this, "bankDateServiceResponseIdentifier", function() {
        return this._bankDateServiceResponseIdentifier;
      });

      defineSetter(this, "currencyListSrc", function(val) {
        if((typeof val=='string') && (val != "")){
          this._currencyListSrc=val;
        }
      });


      defineGetter(this, "currencyListSrc", function() {
        return this._currencyListSrc;
      });
	  
      defineSetter(this, "fromAccountActionKey", function(val) {
        if((typeof val=='string') && (val != "")){
          this._fromAccountActionKey=val;
        }
      });


      defineGetter(this, "fromAccountActionKey", function() {
        return this._fromAccountActionKey;
      });
      defineSetter(this, "currencyList", function(val) {
        if((typeof val=='string') && (val != "")){
          this._currencyList=val;
        }
      });


      defineGetter(this, "currencyList", function() {
        return this._currencyList;
      });

      defineSetter(this, "sknACEmptyRecordText", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknACEmptyRecordText=val;
        }
      });


      defineGetter(this, "sknACEmptyRecordText", function() {
        return this._sknACEmptyRecordText;
      });

      defineSetter(this, "sknACToDropdownField3", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknACToDropdownField3=val;
        }
      });


      defineGetter(this, "sknACToDropdownField3", function() {
        return this._sknACToDropdownField3;
      });
      defineSetter(this, "sknACEmptyRecordActionText", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknACEmptyRecordActionText=val;
        }
      });


      defineGetter(this, "sknACEmptyRecordActionText", function() {
        return this._sknACEmptyRecordActionText;
      });


      defineSetter(this, "sknTxtBoxFocus", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknTxtBoxFocus=val;
        }
      });


      defineGetter(this, "sknTxtBoxFocus", function() {
        return this._sknTxtBoxFocus;
      });

      defineSetter(this, "fromAccountGroupIdentifier", function(val) {
        if((typeof val=='string') && (val != "")){
          this._fromAccountGroupIdentifier=val;
        }
      });


      defineGetter(this, "fromAccountGroupIdentifier", function() {
        return this._fromAccountGroupIdentifier;
      });

      defineSetter(this, "cityBankIcon", function(val) {
        if((typeof val=='string') && (val != "")){
          this._cityBankIcon=val;
        }
      });


      defineGetter(this, "cityBankIcon", function() {
        return this._cityBankIcon;
      });

      defineSetter(this, "chaseBankIcon", function(val) {
        if((typeof val=='string') && (val != "")){
          this._chaseBankIcon=val;
        }
      });


      defineSetter(this, "backendDateFormat", function(val) {
        if((typeof val=='string') && (val != "")){
          this._backendDateFormat=val;
        }
      });


      defineGetter(this, "backendDateFormat", function() {
        return this._backendDateFormat;
      });


      defineGetter(this, "chaseBankIcon", function() {
        return this._chaseBankIcon;
      });

      defineSetter(this, "BAOBankIcon", function(val) {
        if((typeof val=='string') && (val != "")){
          this._BAOBankIcon=val;
        }
      });


      defineGetter(this, "BAOBankIcon", function() {
        return this._BAOBankIcon;
      });

      defineSetter(this, "HDFCBankIcon", function(val) {
        if((typeof val=='string') && (val != "")){
          this._HDFCBankIcon=val;
        }
      });


      defineGetter(this, "HDFCBankIcon", function() {
        return this._HDFCBankIcon;
      });

      defineSetter(this, "infinityBankIcon", function(val) {
        if((typeof val=='string') && (val != "")){
          this._infinityBankIcon=val;
        }
      });


      defineGetter(this, "infinityBankIcon", function() {
        return this._infinityBankIcon;
      });

      defineSetter(this, "externalBankIcon", function(val) {
        if((typeof val=='string') && (val != "")){
          this._externalBankIcon=val;
        }
      });


      defineGetter(this, "externalBankIcon", function() {
        return this._externalBankIcon;
      });


      defineSetter(this, "lblPayeeDetailField4", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblPayeeDetailField4=val;
        }
      });


      defineGetter(this, "lblPayeeDetailField4", function() {
        return this._lblPayeeDetailField4;
      });

      defineSetter(this, "txtInputPayeeDetail4", function(val) {
        if((typeof val=='string') && (val != "")){
          this._txtInputPayeeDetail4=val;
        }
      });


      defineGetter(this, "txtInputPayeeDetail4", function() {
        return this._txtInputPayeeDetail4;
      });


      //setter method for Flow Type  in the group:General
      defineSetter(this, "fromAccountListArray", function(val) {
        if((typeof val=='string') && (val != "")){
          this._fromAccountListArray=val;
        }
      });

      //getter method for Flow Type  in the group:General
      defineGetter(this, "fromAccountListArray", function() {
        return this._fromAccountListArray;
      });										  

      //setter method for Flow Type  in the group:General
      defineSetter(this, "FLOWTYPES", function(val) {
        if((typeof val=='string') && (val != "")){
          this._FLOWTYPES=val;
        }
      });

      //getter method for Flow Type  in the group:General
      defineGetter(this, "FLOWTYPES", function() {
        return this._FLOWTYPES;
      });

      //setter method for transferType Type  in the group:General
      defineSetter(this, "transferType", function(val) {
        if((typeof val=='string') && (val != "")){
          this._transferType=val;
        }
      });

      //getter method for Transfer Type  in the group:General
      defineGetter(this, "transferType", function() {
        return this._transferType;
      });

      //setter method for Data Validation Config in the group:Data Validation
      defineSetter(this, "dvfConfig", function(val) {
        if((typeof val=='string') && (val != "")){
          this._dvfConfig=val;
        }
      });

      //getter method for Data Validation Config in the group:Data Validation
      defineGetter(this, "dvfConfig", function() {
        return this._dvfConfig;
      });

      //setter method for Operation Name in the group:Date Validation Service
      defineSetter(this, "dvOpName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._dvOpName=val;
        }
      });

      //getter method for Operation Name in the group:Data Validation Service
      defineGetter(this, "dvOpName", function() {
        return this._dvOpName;
      });



      //setter method for Service Name in the group:Data Validation Service
      defineSetter(this, "dvObjServiceName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._dvObjServiceName=val;
        }
      });

      //getter method for Service Name in the group:Data Validation Service
      defineGetter(this, "dvObjServiceName", function() {
        return this._dvObjServiceName;
      });

      //setter method for Service Response Identifier in the group:Data Validation Service
      defineSetter(this, "dvIdentifier", function(val) {
        if((typeof val=='string') && (val != "")){
          this._dvIdentifier=val;
        }
      });

      //getter method for Service Response Identifier in the group:Data validation Service
      defineGetter(this, "dvIdentifier", function() {
        return this._dvIdentifier;
      });

      //setter method for Object name in the group:Data Validation Service
      defineSetter(this, "dvObjName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._dvObjName=val;
        }
      });

      //getter method for Object name in the group:Data validation Service
      defineGetter(this, "dvObjName", function() {
        return this._dvObjName;
      });

      //setter method for critera in the group:Data Validation Service
      defineSetter(this, "dvCriteria", function(val) {
        if((typeof val=='string') && (val != "")){
          this._dvCriteria=val;
        }
      });

      //getter method for criteria in the group:Data validation Service
      defineGetter(this, "dvCriteria", function() {
        return this._dvCriteria;
      });

      //setter method for From Account Label in the group:From Account
      defineSetter(this, "lblFromAccount", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblFromAccount=val;
        }
      });

      //getter method for From Account Label in the group:From Account
      defineGetter(this, "lblFromAccount", function() {
        return this._lblFromAccount;
      });

      //setter method for To Account Label in the group:To Account
      defineSetter(this, "lblToAccount", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblToAccount=val;
        }
      });

      //getter method for To Account Label in the group:To Account
      defineGetter(this, "lblToAccount", function() {
        return this._lblToAccount;
      });

      //setter method for Currency  Dropdown Values in the group:Currency Field
      defineSetter(this, "dropdownTransferCurrency", function(val) {
        if((typeof val=='string') && (val != "")){
          this._dropdownTransferCurrency=val;
        }
      });

      //getter method for Currency  Dropdown Values in the group:Currency Field
      defineGetter(this, "dropdownTransferCurrency", function() {
        return this._dropdownTransferCurrency;
      });

      //setter method for Transfer Amount  Text Input Label in the group:Transfer Amount
      defineSetter(this, "lblTransferAmount", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblTransferAmount=val;
        }
      });

      //getter method for Transfer Amount  Text Input Label in the group:Transfer Amount
      defineGetter(this, "lblTransferAmount", function() {
        return this._lblTransferAmount;
      });

      //setter method for FX Rate Reference Text Input  Label in the group:FX Rate Reference
      defineSetter(this, "lblfxRateReference", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblfxRateReference=val;
        }
      });

      //getter method for FX Rate Reference Text Input  Label in the group:FX Rate Reference
      defineGetter(this, "lblfxRateReference", function() {
        return this._lblfxRateReference;
      });

      //setter method for Frequency Dropdown Label in the group:Frequency
      defineSetter(this, "lblFrequency", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblFrequency=val;
        }
      });

      //getter method for Frequency Dropdown Label in the group:Frequency
      defineGetter(this, "lblFrequency", function() {
        return this._lblFrequency;
      });

      //setter method for Datepicker 1 Label in the group:Transfer Date
      defineSetter(this, "lblDatepicker1", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblDatepicker1=val;
        }
      });

      //getter method for Datepicker 1 Label in the group:Transfer Date
      defineGetter(this, "lblDatepicker1", function() {
        return this._lblDatepicker1;
      });

      //setter method for Payment Method Radio Input Label in the group:Payment Method
      defineSetter(this, "lblPaymentMethod", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblPaymentMethod=val;
        }
      });

      //getter method for Payment Method Radio Input Label in the group:Payment Method
      defineGetter(this, "lblPaymentMethod", function() {
        return this._lblPaymentMethod;
      });

      //setter method for Fees Paid Radio Input Label in the group:Fees Paid
      defineSetter(this, "lblFeesPaid", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblFeesPaid=val;
        }
      });

      //getter method for Fees Paid Radio Input Label in the group:Fees Paid
      defineGetter(this, "lblFeesPaid", function() {
        return this._lblFeesPaid;
      });

      //setter method for Intermediary BIC Text Input  Label in the group:Intermediary BIC
      defineSetter(this, "lblIntermediaryBIC", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblIntermediaryBIC=val;
        }
      });

      //getter method for Intermediary BIC Text Input  Label in the group:Intermediary BIC
      defineGetter(this, "lblIntermediaryBIC", function() {
        return this._lblIntermediaryBIC;
      });

      //setter method for E2E Text Input  Label in the group:E2E
      defineSetter(this, "lblE2E", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblE2E=val;
        }
      });

      //getter method for E2E Text Input  Label in the group:E2E
      defineGetter(this, "lblE2E", function() {
        return this._lblE2E;
      });

      //setter method for Notes Text Input  Label in the group:Notes
      defineSetter(this, "lblNotes", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblNotes=val;
        }
      });

      //getter method for Notes Text Input  Label in the group:Notes
      defineGetter(this, "lblNotes", function() {
        return this._lblNotes;
      });

      //setter method for Field  Label in the group:Additional Field
      defineSetter(this, "lblField", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblField=val;
        }
      });

      //getter method for Field  Label in the group:Additional Field
      defineGetter(this, "lblField", function() {
        return this._lblField;
      });

      //setter method for Address Line 1 in the group:Backend Address Data
      defineSetter(this, "addressLine1Value", function(val) {
        if((typeof val=='string') && (val != "")){
          this._addressLine1Value=val;
        }
      });

      //getter method for Address Line 1 in the group:Backend Address Data
      defineGetter(this, "addressLine1Value", function() {
        return this._addressLine1Value;
      });

      //setter method for Address Line 2 in the group:Backend Address Data
      defineSetter(this, "addressLine2Value", function(val) {
        if((typeof val=='string') && (val != "")){
          this._addressLine2Value=val;
        }
      });

      //getter method for Address Line 2 in the group:Backend Address Data
      defineGetter(this, "addressLine2Value", function() {
        return this._addressLine2Value;
      });

      //setter method for City in the group:Backend Address Data
      defineSetter(this, "cityValue", function(val) {
        if((typeof val=='string') && (val != "")){
          this._cityValue=val;
        }
      });

      //getter method for City in the group:Backend Address Data
      defineGetter(this, "cityValue", function() {
        return this._cityValue;
      });

      //setter method for State in the group:Backend Address Data
      defineSetter(this, "stateValue", function(val) {
        if((typeof val=='string') && (val != "")){
          this._stateValue=val;
        }
      });

      //getter method for State in the group:Backend Address Data
      defineGetter(this, "stateValue", function() {
        return this._stateValue;
      });

      //setter method for Country in the group:Backend Address Data
      defineSetter(this, "countryValue", function(val) {
        if((typeof val=='string') && (val != "")){
          this._countryValue=val;
        }
      });

      //getter method for Country in the group:Backend Address Data
      defineGetter(this, "countryValue", function() {
        return this._countryValue;
      });

      //setter method for Zip Code in the group:Backend Address Data
      defineSetter(this, "zipCodeValue", function(val) {
        if((typeof val=='string') && (val != "")){
          this._zipCodeValue=val;
        }
      });

      //getter method for Zip Code in the group:Backend Address Data
      defineGetter(this, "zipCodeValue", function() {
        return this._zipCodeValue;
      });

      //setter method for Phone Number in the group:Backend Address Data
      defineSetter(this, "phoneNumberValue", function(val) {
        if((typeof val=='string') && (val != "")){
          this._phoneNumberValue=val;
        }
      });

      //getter method for Phone Number in the group:Backend Address Data
      defineGetter(this, "phoneNumberValue", function() {
        return this._phoneNumberValue;
      });

      //setter method for Email Address in the group:Backend Address Data
      defineSetter(this, "emailAddressValue", function(val) {
        if((typeof val=='string') && (val != "")){
          this._emailAddressValue=val;
        }
      });

      //getter method for Email Address in the group:Backend Address Data
      defineGetter(this, "emailAddressValue", function() {
        return this._emailAddressValue;
      });

      //setter method for Button1 in the group:Action Buttons
      defineSetter(this, "button1", function(val) {
        if((typeof val=='string') && (val != "")){
          this._button1=val;
        }
      });

      //getter method for Button1 in the group:Action Buttons
      defineGetter(this, "button1", function() {
        return this._button1;
      });
      //setter method for Text Box Disabled Skin in the group:Skins
      defineSetter(this, "sknTextBoxDisabled", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknTextBoxDisabled=val;
        }
      });

      //getter method for Text Box Disabled Skin in the group:Skins
      defineGetter(this, "sknTextBoxDisabled", function() {
        return this._sknTextBoxDisabled;
      });													   

      //setter method for Text Box Placeholder Skin in the group:Skins
      defineSetter(this, "sknTextBoxPlaceholder", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknTextBoxPlaceholder=val;
        }
      });

      //getter method for Text Box Placeholder Skin in the group:Skins
      defineGetter(this, "sknTextBoxPlaceholder", function() {
        return this._sknTextBoxPlaceholder;
      });

      //setter method for Text Area Skin in the group:Skins
      defineSetter(this, "sknTextArea", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknTextArea=val;
        }
      });

      //getter method for Text Area Skin in the group:Skins
      defineGetter(this, "sknTextArea", function() {
        return this._sknTextArea;
      });

      //setter method for Mandatory Text Box Hover Skin in the group:Skins
      defineSetter(this, "sknMandatoryTextBox", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknMandatoryTextBox=val;
        }
      });

      //getter method for Mandatory Text Box Hover Skin in the group:Skins
      defineGetter(this, "sknMandatoryTextBox", function() {
        return this._sknMandatoryTextBox;
      });

      //setter method for Text Box Focus Skin in the group:Skins
      defineSetter(this, "sknTextBoxFocus", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknTextBoxFocus=val;
        }
      });

      //getter method for Text Box Focus Skin in the group:Skins
      defineGetter(this, "sknTextBoxFocus", function() {
        return this._sknTextBoxFocus;
      });

      //setter method for Supporting Documents Label in the group:Supporting Documents
      defineSetter(this, "lblSupportingDocuments", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._lblSupportingDocuments=val;
        }
      });

      //getter method for Supporting Documents Label in the group:Supporting Documents
      defineGetter(this, "lblSupportingDocuments", function() {
        return this._lblSupportingDocuments;
      });
      //setter method for Supporting Documents Label Suffix in the group:Supporting Documents
      defineSetter(this, "lblDocumentsOptionalSuffix", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._lblDocumentsOptionalSuffix=val;
        }
      });

      //getter method for Supporting Documents Label Suffix in the group:Supporting Documents
      defineGetter(this, "lblDocumentsOptionalSuffix", function() {
        return this._lblDocumentsOptionalSuffix;
      });

      //setter method for attachmentError1 Label in the group:Supporting Documents
      defineSetter(this, "attachmentError1", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._attachmentError1=val;
        }
      });

      //getter method for attachmentError1 Label in the group:Supporting Documents
      defineGetter(this, "attachmentError1", function() {
        return this._attachmentError1;
      });
      //setter method for attachmentError2 Label in the group:Supporting Documents
      defineSetter(this, "attachmentError2", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._attachmentError2=val;
        }
      });

      //getter method for attachmentError2 Label in the group:Supporting Documents
      defineGetter(this, "attachmentError2", function() {
        return this._attachmentError2;
      });
      //setter method for attachmentError3 Label in the group:Supporting Documents
      defineSetter(this, "attachmentError3", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._attachmentError3=val;
        }
      });

      //getter method for attachmentError3 Label in the group:Supporting Documents
      defineGetter(this, "attachmentError3", function() {
        return this._attachmentError3;
      });
      //setter method for attachmentError4 Label in the group:Supporting Documents
      defineSetter(this, "attachmentError4", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._attachmentError4=val;
        }
      });

      //getter method for attachmentError4 Label in the group:Supporting Documents
      defineGetter(this, "attachmentError4", function() {
        return this._attachmentError4;
      });
      //setter method for attachmentError5 Label in the group:Supporting Documents
      defineSetter(this, "attachmentError5", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._attachmentError5=val;
        }
      });

      //getter method for attachmentError5 Label in the group:Supporting Documents
      defineGetter(this, "attachmentError5", function() {
        return this._attachmentError5;
      });

      //setter method for Radio Button Action  in the group:icons
      defineSetter(this, "radioButtonAction", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._radioButtonAction=val;
        }
      });

      //getter method for Radio Button Action  in the group:icons
      defineGetter(this, "radioButtonAction", function() {
        return this._radioButtonAction;
      });

      //setter method for Add Payee Address Label in the group:Payee Address Fields
      defineSetter(this, "lblAddPayeeAddress", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblAddPayeeAddress=val;
        }
      });

      //getter method for Add Payee Address Label in the group:Payee Address Fields
      defineGetter(this, "lblAddPayeeAddress", function() {
        return this._lblAddPayeeAddress;
      });

      //setter method for Add Payee Address Optional Label in the group:Payee Address Fields
      defineSetter(this, "lblPayeeAddressOptional", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblPayeeAddressOptional=val;
        }
      });

      //getter method for Add Payee Address Optional Label in the group:Payee Address Fields
      defineGetter(this, "lblPayeeAddressOptional", function() {
        return this._lblPayeeAddressOptional;
      });

      //setter method for Header in the group:SWIFT Lookup 
      defineSetter(this, "swiftLookupHeader", function(val) {
        if((typeof val=='string') && (val != "")){
          this._swiftLookupHeader=val;
        }
      });

      //getter method for Header in the group:SWIFT Lookup 
      defineGetter(this, "swiftLookupHeader", function() {
        return this._swiftLookupHeader;
      });

      //setter method for Error Text Input Skin in the group:Skins
      defineSetter(this, "sknErrorTextInput", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknErrorTextInput=val;
        }
      });

      //getter method for Error Text Input Skin in the group:Skins
      defineGetter(this, "sknErrorTextInput", function() {
        return this._sknErrorTextInput;
      });
      //setter method for sknradioIconSelected Skin in the group:Skins
      defineSetter(this, "sknradioIconSelected", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknradioIconSelected=val;
        }
      });

      //getter method for sknradioIconSelected Skin in the group:Skins
      defineGetter(this, "sknradioIconSelected", function() {
        return this._sknradioIconSelected;
      });
      //setter method for sknradioIconUnselected Skin in the group:Skins
      defineSetter(this, "sknradioIconUnselected", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknradioIconUnselected=val;
        }
      });

      //getter method for sknradioIconUnselected Skin in the group:Skins
      defineGetter(this, "sknradioIconUnselected", function() {
        return this._sknradioIconUnselected;
      });
        //setter method for sknradioIconUnselected Skin in the group:Skins
      defineSetter(this, "sknradioIconDisabled", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknradioIconDisabled=val;
        }
      });

      //getter method for sknradioIconDisabled Skin in the group:Skins
      defineGetter(this, "sknradioIconDisabled", function() {
        return this._sknradioIconDisabled;
      });
      //setter method for Dropdown Expand Icon in the group:Icons
      defineSetter(this, "dropdownExpandIcon", function(val) {
        if((typeof val=='string') && (val != "")){
          this._dropdownExpandIcon=val;
        }
      });

      //getter method for Dropdown Expand Icon in the group:Icons
      defineGetter(this, "dropdownExpandIcon", function() {
        return this._dropdownExpandIcon;
      });

      //setter method for Object Name in the group:From Accounts List Service
      defineSetter(this, "fromAccountObjectName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._fromAccountObjectName=val;
        }
      });

      //getter method for Object Name in the group:From Accounts List Service
      defineGetter(this, "fromAccountObjectName", function() {
        return this._fromAccountObjectName;
      });

      //setter method for Object Service Name in the group:To Accounts List Service
      defineSetter(this, "toAccountObjectServiceName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._toAccountObjectServiceName=val;
        }
      });

      //getter method for Object Service Name in the group:To Accounts List Service
      defineGetter(this, "toAccountObjectServiceName", function() {
        return this._toAccountObjectServiceName;
      });

      //setter method for Object Name in the group:SWIFT Lookup Service
      defineSetter(this, "lookupObjectName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lookupObjectName=val;
        }
      });

      //getter method for Object Name in the group:SWIFT Lookup Service
      defineGetter(this, "lookupObjectName", function() {
        return this._lookupObjectName;
      });

      //setter method for Currency Dropdown Label in the group:Currency Field
      defineSetter(this, "lblTransferCurrency", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblTransferCurrency=val;
        }
      });

      //getter method for Currency Dropdown Label in the group:Currency Field
      defineGetter(this, "lblTransferCurrency", function() {
        return this._lblTransferCurrency;
      });

      //setter method for Segregation Types in the group:General
      defineSetter(this, "SEGREGATIONTYPES", function(val) {
        if((typeof val=='string') && (val != "")){
          this._SEGREGATIONTYPES=val;
        }
      });

      //getter method for Segregation Types in the group:General
      defineGetter(this, "SEGREGATIONTYPES", function() {
        return this._SEGREGATIONTYPES;
      });

      //setter method for Text Box Flex Skin in the group:Skins
      defineSetter(this, "sknTextBoxFlex", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknTextBoxFlex=val;
        }
      });

      //getter method for Text Box Flex Skin in the group:Skins
      defineGetter(this, "sknTextBoxFlex", function() {
        return this._sknTextBoxFlex;
      });

      //setter method for Minimum Fill Mapping in the group:Data Validation
      defineSetter(this, "minFillMapping", function(val) {
        if((typeof val=='string') && (val != "")){
          this._minFillMapping=val;
        }
      });

      //getter method for Minimum Fill Mapping in the group:Data Validation
      defineGetter(this, "minFillMapping", function() {
        return this._minFillMapping;
      });

      //setter method for Maximum Fill Mapping in the group:Data Validation
      defineSetter(this, "maxFillMapping", function(val) {
        if((typeof val=='string') && (val != "")){
          this._maxFillMapping=val;
        }
      });

      //getter method for Maximum Fill Mapping in the group:Data Validation
      defineGetter(this, "maxFillMapping", function() {
        return this._maxFillMapping;
      });

      //setter method for From Account Text Input in the group:From Account
      defineSetter(this, "fromAccountTextInput", function(val) {
        if((typeof val=='string') && (val != "")){
          this._fromAccountTextInput=val;
        }
      });

      //getter method for From Account Text Input in the group:From Account
      defineGetter(this, "fromAccountTextInput", function() {
        return this._fromAccountTextInput;
      });

      //setter method for Payee Type 1 Option in the group:To Account
      defineSetter(this, "payeeTypeOption1", function(val) {
        if((typeof val=='string') && (val != "")){
          this._payeeTypeOption1=val;
        }
      });

      //getter method for Payee Type 1 Option in the group:To Account
      defineGetter(this, "payeeTypeOption1", function() {
        return this._payeeTypeOption1;
      });

      //setter method for Payment Amount Option1 in the group:Payment Amount
      defineSetter(this, "paymentAmountOption1", function(val) {
        if((typeof val=='string') && (val != "")){
          this._paymentAmountOption1=val;
        }
      });

      //getter method for Payment Amount Option1 in the group:Payment Amount
      defineGetter(this, "paymentAmountOption1", function() {
        return this._paymentAmountOption1;
      });

      //setter method for Transfer Amount Text Input  Value in the group:Transfer Amount
      defineSetter(this, "txtInputTransferAmount", function(val) {
        if((typeof val=='string') && (val != "")){
          this._txtInputTransferAmount=val;
        }
      });

      //getter method for Transfer Amount Text Input  Value in the group:Transfer Amount
      defineGetter(this, "txtInputTransferAmount", function() {
        return this._txtInputTransferAmount;
      });

      //setter method for FX Rate Reference Text Input Value in the group:FX Rate Reference
      defineSetter(this, "txtInputfxRateReference", function(val) {
        if((typeof val=='string') && (val != "")){
          this._txtInputfxRateReference=val;
        }
      });

      //getter method for FX Rate Reference Text Input Value in the group:FX Rate Reference
      defineGetter(this, "txtInputfxRateReference", function() {
        return this._txtInputfxRateReference;
      });

      //setter method for Frequency Dropdown  Value in the group:Frequency
      defineSetter(this, "dropdownFrequency", function(val) {
        if((typeof val=='string') && (val != "")){
          this._dropdownFrequency=val;
        }
      });

      //getter method for Frequency Dropdown  Value in the group:Frequency
      defineGetter(this, "dropdownFrequency", function() {
        return this._dropdownFrequency;
      });

      //setter method for Transfer Flow in the group:General
      defineSetter(this, "transferFlow", function(val) {
        if((typeof val=='string') && (val != "")){
          this._transferFlow=val;
        }
      });

      //getter method for Transfer Flow in the group:General
      defineGetter(this, "transferFlow", function() {
        return this._transferFlow;
      });

      //setter method for Datepicker1 Value in the group:Transfer Date
      defineSetter(this, "datepicker1Value", function(val) {
        if((typeof val=='string') && (val != "")){
          this._datepicker1Value=val;
        }
      });

      //getter method for Datepicker1 Value in the group:Transfer Date
      defineGetter(this, "datepicker1Value", function() {
        return this._datepicker1Value;
      });

      //setter method for Payment Method Option1 in the group:Payment Method
      defineSetter(this, "paymentMethodOption1", function(val) {
        if((typeof val=='string') && (val != "")){
          this._paymentMethodOption1=val;
        }
      });

      //getter method for Payment Method Option1 in the group:Payment Method
      defineGetter(this, "paymentMethodOption1", function() {
        return this._paymentMethodOption1;
      });

      //setter method for Fees Paid Option1 in the group:Fees Paid
      defineSetter(this, "feesPaidOption1", function(val) {
        if((typeof val=='string') && (val != "")){
          this._feesPaidOption1=val;
        }
      });

      //getter method for Fees Paid Option1 in the group:Fees Paid
      defineGetter(this, "feesPaidOption1", function() {
        return this._feesPaidOption1;
      });

      //setter method for Intermediary BIC Text Input Value in the group:Intermediary BIC
      defineSetter(this, "txtInputIntermediaryBIC", function(val) {
        if((typeof val=='string') && (val != "")){
          this._txtInputIntermediaryBIC=val;
        }
      });

      //getter method for Intermediary BIC Text Input Value in the group:Intermediary BIC
      defineGetter(this, "txtInputIntermediaryBIC", function() {
        return this._txtInputIntermediaryBIC;
      });

      //setter method for E2E Text Input  Value in the group:E2E
      defineSetter(this, "txtInputE2E", function(val) {
        if((typeof val=='string') && (val != "")){
          this._txtInputE2E=val;
        }
      });

      //getter method for E2E Text Input  Value in the group:E2E
      defineGetter(this, "txtInputE2E", function() {
        return this._txtInputE2E;
      });

      //setter method for Notes Text Input Value in the group:Notes
      defineSetter(this, "txtInputNotes", function(val) {
        if((typeof val=='string') && (val != "")){
          this._txtInputNotes=val;
        }
      });

      //getter method for Notes Text Input Value in the group:Notes
      defineGetter(this, "txtInputNotes", function() {
        return this._txtInputNotes;
      });

      //setter method for Dropdown Field  Values in the group:Additional Field
      defineSetter(this, "dropdownFieldValues", function(val) {
        if((typeof val=='string') && (val != "")){
          this._dropdownFieldValues=val;
        }
      });

      //getter method for Dropdown Field  Values in the group:Additional Field
      defineGetter(this, "dropdownFieldValues", function() {
        return this._dropdownFieldValues;
      });

      //setter method for Button2 in the group:Action Buttons
      defineSetter(this, "button2", function(val) {
        if((typeof val=='string') && (val != "")){
          this._button2=val;
        }
      });

      //getter method for Button2 in the group:Action Buttons
      defineGetter(this, "button2", function() {
        return this._button2;
      });

      //setter method for Supported File Format in the group:Supporting Documents
      defineSetter(this, "supportedFileFormat", function(val) {
        if((typeof val=='string') && (val != "")){
          this._supportedFileFormat=val;
        }
      });

      //getter method for Supported File Format in the group:Supporting Documents
      defineGetter(this, "supportedFileFormat", function() {
        return this._supportedFileFormat;
      });
      //setter method for Supported fileConfiguration in the group:Supporting Documents
      defineSetter(this, "fileConfiguration", function(val) {
        if((typeof val=='string') && (val != "")){
          this._fileConfiguration=val;
        }
      });

      //getter method for Supported fileConfiguration in the group:Supporting Documents
      defineGetter(this, "fileConfiguration", function() {
        return this._fileConfiguration;
      });

      //setter method for New Payee Name Text Input Value in the group:Payee Detail Fields
      defineSetter(this, "txtInputNewPayeeName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._txtInputNewPayeeName=val;
        }
      });

      //getter method for New Payee Name Text Input Value in the group:Payee Detail Fields
      defineGetter(this, "txtInputNewPayeeName", function() {
        return this._txtInputNewPayeeName;
      });

      //setter method for Description in the group:SWIFT Lookup 
      defineSetter(this, "swiftLookupDescription", function(val) {
        if((typeof val=='string') && (val != "")){
          this._swiftLookupDescription=val;
        }
      });

      //getter method for Description in the group:SWIFT Lookup 
      defineGetter(this, "swiftLookupDescription", function() {
        return this._swiftLookupDescription;
      });

      //setter method for Field Label Skin in the group:Skins
      defineSetter(this, "sknFieldLabel", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknFieldLabel=val;
        }
      }); 


      //getter method for Field Label Skin in the group:Skins
      defineGetter(this, "sknFieldLabel", function() {
        return this._sknFieldLabel;
      });

      //setter method for Field Label Bold in the group:Skins
      defineSetter(this, "sknFieldLabelBold", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknFieldLabelBold=val;
        }
      }); 


      //getter method for Field Label Bold Skin in the group:Skins
      defineGetter(this, "sknFieldLabelBold", function() {
        return this._sknFieldLabelBold;
      });

      //setter method for Field Value Skin in the group:Skins
      defineSetter(this, "sknFieldReadOnlyValue", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknFieldReadOnlyValue=val;
        }
      }); 


      //getter method for Field Value  Skin in the group:Skins
      defineGetter(this, "sknFieldReadOnlyValue", function() {
        return this._sknFieldReadOnlyValue;
      });

      //setter method for Dropdown Collapse Icon in the group:Icons
      defineSetter(this, "dropdownCollapseIcon", function(val) {
        if((typeof val=='string') && (val != "")){
          this._dropdownCollapseIcon=val;
        }
      });

      //getter method for Dropdown Collapse Icon in the group:Icons
      defineGetter(this, "dropdownCollapseIcon", function() {
        return this._dropdownCollapseIcon;
      });

      //setter method for Operation Name in the group:From Accounts List Service
      defineSetter(this, "fromAccountOperationName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._fromAccountOperationName=val;
        }
      });

      //getter method for Operation Name in the group:From Accounts List Service
      defineGetter(this, "fromAccountOperationName", function() {
        return this._fromAccountOperationName;
      });

      //setter method for Object Name in the group:To Accounts List Service
      defineSetter(this, "toAccountObjectName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._toAccountObjectName=val;
        }
      });

      //getter method for Object Name in the group:To Accounts List Service
      defineGetter(this, "toAccountObjectName", function() {
        return this._toAccountObjectName;
      });

      //setter method for Operation Name in the group:SWIFT Lookup Service
      defineSetter(this, "lookupOperationName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lookupOperationName=val;
        }
      });

      //getter method for Operation Name in the group:SWIFT Lookup Service
      defineGetter(this, "lookupOperationName", function() {
        return this._lookupOperationName;
      });

      //setter method for Frequency Type in the group:General
      defineSetter(this, "FREQUENCYTYPES", function(val) {
        if((typeof val=='string') && (val != "")){
          this._FREQUENCYTYPES=val;
        }
      });

      //getter method for Frequency Type in the group:General
      defineGetter(this, "FREQUENCYTYPES", function() {
        return this._FREQUENCYTYPES;
      });



      //setter method for Payee Type 2 Option in the group:To Account
      defineSetter(this, "payeeTypeOption2", function(val) {
        if((typeof val=='string') && (val != "")){
          this._payeeTypeOption2=val;
        }
      });

      //getter method for Payee Type 2 Option in the group:To Account
      defineGetter(this, "payeeTypeOption2", function() {
        return this._payeeTypeOption2;
      });

      //setter method for Payment Amount Option2 in the group:Payment Amount
      defineSetter(this, "paymentAmountOption2", function(val) {
        if((typeof val=='string') && (val != "")){
          this._paymentAmountOption2=val;
        }
      });

      //getter method for Payment Amount Option2 in the group:Payment Amount
      defineGetter(this, "paymentAmountOption2", function() {
        return this._paymentAmountOption2;
      });

      //setter method for Exchange Rate Label in the group:FX Rate Reference
      defineSetter(this, "lblExchangeRate", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblExchangeRate=val;
        }
      });

      //getter method for Exchange Rate Label in the group:FX Rate Reference
      defineGetter(this, "lblExchangeRate", function() {
        return this._lblExchangeRate;
      });

      //setter method for Frequency Type  Label in the group:Frequency
      defineSetter(this, "lblFrequencyType", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblFrequencyType=val;
        }
      });

      //getter method for Frequency Type  Label in the group:Frequency
      defineGetter(this, "lblFrequencyType", function() {
        return this._lblFrequencyType;
      });

      //setter method for Datepicker 2 Label in the group:Transfer Date
      defineSetter(this, "lblDatepicker2", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblDatepicker2=val;
        }
      });

      //getter method for Datepicker 2 Label in the group:Transfer Date
      defineGetter(this, "lblDatepicker2", function() {
        return this._lblDatepicker2;
      });

      //setter method for Datepicker 3 Label in the group:Transfer Date
      defineSetter(this, "lblDatepicker3", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblDatepicker3=val;
        }
      });

      //getter method for Datepicker 3 Label in the group:Transfer Date
      defineGetter(this, "lblDatepicker3", function() {
        return this._lblDatepicker3;
      });

      //setter method for Payment Method Option2 in the group:Payment Method
      defineSetter(this, "paymentMethodOption2", function(val) {
        if((typeof val=='string') && (val != "")){
          this._paymentMethodOption2=val;
        }
      });

      //getter method for Payment Method Option2 in the group:Payment Method
      defineGetter(this, "paymentMethodOption2", function() {
        return this._paymentMethodOption2;
      });

      //setter method for Fees Paid Option2 in the group:Fees Paid
      defineSetter(this, "feesPaidOption2", function(val) {
        if((typeof val=='string') && (val != "")){
          this._feesPaidOption2=val;
        }
      });

      //getter method for Fees Paid Option2 in the group:Fees Paid
      defineGetter(this, "feesPaidOption2", function() {
        return this._feesPaidOption2;
      });

      //setter method for Label Field  Value in the group:Additional Field
      defineSetter(this, "lblFieldValue", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblFieldValue=val;
        }
      });

      //getter method for Label Field  Value in the group:Additional Field
      defineGetter(this, "lblFieldValue", function() {
        return this._lblFieldValue;
      });

      //setter method for SelectedFile in the group:Supporting Documents
      defineSetter(this, "selectedFileValue", function(val) {
        if((typeof val=='string') && (val != "")){
          this._selectedFileValue=val;
        }
      });

      //getter method for SelectedFile in the group:Supporting Documents
      defineGetter(this, "selectedFileValue", function() {
        return this._selectedFileValue;
      });

      //setter method for Account Number Text InputÂ  Label in the group:Payee Detail Fields
      defineSetter(this, "lblNewPayeeAccountNumber", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblNewPayeeAccountNumber=val;
        }
      });

      //getter method for Account Number Text InputÂ  Label in the group:Payee Detail Fields
      defineGetter(this, "lblNewPayeeAccountNumber", function() {
        return this._lblNewPayeeAccountNumber;
      });

      //setter method for Search Field 1 Label in the group:SWIFT Lookup 
      defineSetter(this, "searchField1Label", function(val) {
        if((typeof val=='string') && (val != "")){
          this._searchField1Label=val;
        }
      });

      //getter method for Search Field 1 Label in the group:SWIFT Lookup 
      defineGetter(this, "searchField1Label", function() {
        return this._searchField1Label;
      });

      //setter method for Field ValueÂ  Skin in the group:Skins
      defineSetter(this, "sknFieldValue", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknFieldValue=val;
        }
      });

      //getter method for Field ValueÂ  Skin in the group:Skins
      defineGetter(this, "sknFieldValue", function() {
        return this._sknFieldValue;
      });

      //setter method for Close Icon in the group:Icons
      defineSetter(this, "closeIcon", function(val) {
        if((typeof val=='string') && (val != "")){
          this._closeIcon=val;
        }
      });

      //getter method for Close Icon in the group:Icons
      defineGetter(this, "closeIcon", function() {
        return this._closeIcon;
      });

      //setter method for Criteria in the group:From Accounts List Service
      defineSetter(this, "fromAccountsCriteria", function(val) {
        if((typeof val=='string') && (val != "")){
          this._fromAccountsCriteria=val;
        }
      });

      //getter method for Criteria in the group:From Accounts List Service
      defineGetter(this, "fromAccountsCriteria", function() {
        return this._fromAccountsCriteria;
      });

      //setter method for Operation Name in the group:To Accounts List Service
      defineSetter(this, "toAccountOperationName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._toAccountOperationName=val;
        }
      });

      //getter method for Operation Name in the group:To Accounts List Service
      defineGetter(this, "toAccountOperationName", function() {
        return this._toAccountOperationName;
      });

      //setter method for lookupServiceName in the group:SWIFT Lookup Service
      defineSetter(this, "lookupServiceName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lookupServiceName=val;
        }
      });

      //getter method for lookupServiceName in the group:SWIFT Lookup Service
      defineGetter(this, "lookupServiceName", function() {
        return this._lookupServiceName;
      });

      //setter method for Occurrence Type in the group:General
      defineSetter(this, "OCCURRENCETYPES", function(val) {
        if((typeof val=='string') && (val != "")){
          this._OCCURRENCETYPES=val;
        }
      });

      //getter method for Occurrence Type in the group:General
      defineGetter(this, "OCCURRENCETYPES", function() {
        return this._OCCURRENCETYPES;
      });

      //setter method for Listing Account Types in the group:From Account
      defineSetter(this, "accountTypesList", function(val) {
        if((typeof val=='string') && (val != "")){
          this._accountTypesList=val;
        }
      });

      //getter method for Listing Account Types in the group:From Account
      defineGetter(this, "accountTypesList", function() {
        return this._accountTypesList;
      });

      //setter method for To Account Text Input in the group:To Account
      defineSetter(this, "toAccountTextInput", function(val) {
        if((typeof val=='string') && (val != "")){
          this._toAccountTextInput=val;
        }
      });

      //getter method for To Account Text Input in the group:To Account
      defineGetter(this, "toAccountTextInput", function() {
        return this._toAccountTextInput;
      });

      //setter method for Payee Type Context in the group:General
      defineSetter(this, "payeeType", function(val) {
        if((typeof val=='string') && (val != "")){
          this._payeeType=val;
        }
      });

      //getter method for Payee Type Context in the group:General
      defineGetter(this, "payeeType", function() {
        return this._payeeType;
      });

      //setter method for Amount Format Context in the group:General
      defineSetter(this, "amountFormat", function(val) {
        if((typeof val=='string') && (val != "")){
          this._amountFormat=val;
        }
      });

      //getter method for Amount Format Context in the group:General
      defineGetter(this, "amountFormat", function() {
        return this._amountFormat;
      });

      //setter method for Payment Amount Option3 in the group:Payment Amount
      defineSetter(this, "paymentAmountOption3", function(val) {
        if((typeof val=='string') && (val != "")){
          this._paymentAmountOption3=val;
        }
      });

      //getter method for Payment Amount Option3 in the group:Payment Amount
      defineGetter(this, "paymentAmountOption3", function() {
        return this._paymentAmountOption3;
      });

      //setter method for Frequency Type Dropdown Value in the group:Frequency
      defineSetter(this, "dropdownFrequencyType", function(val) {
        if((typeof val=='string') && (val != "")){
          this._dropdownFrequencyType=val;
        }
      });

      //getter method for Frequency Type Dropdown Value in the group:Frequency
      defineGetter(this, "dropdownFrequencyType", function() {
        return this._dropdownFrequencyType;
      });

      //setter method for frequencyToRemove Value in the group:Frequency
      defineSetter(this, "frequencyToRemove", function(val) {
        if((typeof val=='string') && (val != "")){
          this._frequencyToRemove=val;
        }
      });

      //getter method for frequencyToRemove Value in the group:Frequency
      defineGetter(this, "frequencyToRemove", function() {
        return this._frequencyToRemove;
      });

      //setter method for Datepicker2 Value in the group:Transfer Date
      defineSetter(this, "datepicker2Value", function(val) {
        if((typeof val=='string') && (val != "")){
          this._datepicker2Value=val;
        }
      });

      //getter method for Datepicker2 Value in the group:Transfer Date
      defineGetter(this, "datepicker2Value", function() {
        return this._datepicker2Value;
      });

      //setter method for Datepicker3 Value in the group:Transfer Date
      defineSetter(this, "datepicker3Value", function(val) {
        if((typeof val=='string') && (val != "")){
          this._datepicker3Value=val;
        }
      });

      //getter method for Datepicker3 Value in the group:Transfer Date
      defineGetter(this, "datepicker3Value", function() {
        return this._datepicker3Value;
      });

      //setter method for Payment Method Option3 in the group:Payment Method
      defineSetter(this, "paymentMethodOption3", function(val) {
        if((typeof val=='string') && (val != "")){
          this._paymentMethodOption3=val;
        }
      });

      //getter method for Payment Method Option3 in the group:Payment Method
      defineGetter(this, "paymentMethodOption3", function() {
        return this._paymentMethodOption3;
      });

      //setter method for Fees Paid Option3 in the group:Fees Paid
      defineSetter(this, "feesPaidOption3", function(val) {
        if((typeof val=='string') && (val != "")){
          this._feesPaidOption3=val;
        }
      });

      //getter method for Fees Paid Option3 in the group:Fees Paid
      defineGetter(this, "feesPaidOption3", function() {
        return this._feesPaidOption3;
      });

      //setter method for Text Input Field  Value in the group:Additional Field
      defineSetter(this, "textInputValue", function(val) {
        if((typeof val=='string') && (val != "")){
          this._textInputValue=val;
        }
      });

      //getter method for Text Input Field  Value in the group:Additional Field
      defineGetter(this, "textInputValue", function() {
        return this._textInputValue;
      });

      //setter method for Account Number Text Input Value in the group:Payee Detail Fields
      defineSetter(this, "txtInputNewPayeeAccountNumber", function(val) {
        if((typeof val=='string') && (val != "")){
          this._txtInputNewPayeeAccountNumber=val;
        }
      });

      //getter method for Account Number Text Input Value in the group:Payee Detail Fields
      defineGetter(this, "txtInputNewPayeeAccountNumber", function() {
        return this._txtInputNewPayeeAccountNumber;
      });

      //setter method for Search Field 1 Value in the group:SWIFT Lookup 
      defineSetter(this, "searchField1Value", function(val) {
        if((typeof val=='string') && (val != "")){
          this._searchField1Value=val;
        }
      });

      //getter method for Search Field 1 Value in the group:SWIFT Lookup 
      defineGetter(this, "searchField1Value", function() {
        return this._searchField1Value;
      });

      //setter method for Lookup Label Skin in the group:Skins
      defineSetter(this, "sknLookupLabel", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknLookupLabel=val;
        }
      });

      //getter method for Lookup Label Skin in the group:Skins
      defineGetter(this, "sknLookupLabel", function() {
        return this._sknLookupLabel;
      });

      //setter method for Attach Documents Icon in the group:Icons
      defineSetter(this, "attachDocumentsIcon", function(val) {
        if((typeof val=='string') && (val != "")){
          this._attachDocumentsIcon=val;
        }
      });

      //getter method for Attach Documents Icon in the group:Icons
      defineGetter(this, "attachDocumentsIcon", function() {
        return this._attachDocumentsIcon;
      });

      //setter method for Service Response Identifier in the group:From Accounts List Service
      defineSetter(this, "fromAccountListObject", function(val) {
        if((typeof val=='string') && (val != "")){
          this._fromAccountListObject=val;
        }
      });

      //getter method for Service Response Identifier in the group:From Accounts List Service
      defineGetter(this, "fromAccountListObject", function() {
        return this._fromAccountListObject;
      });

      //setter method for Criteria in the group:To Accounts List Service
      //setter method for Criteria in the group:To Accounts List Service
      defineSetter(this, "toAccountscriteria", function(val) {
        if((typeof val=='string') && (val != "")){
          this._toAccountscriteria=val;
        }
      });

      //getter method for Criteria in the group:To Accounts List Service
      defineGetter(this, "toAccountscriteria", function() {
        return this._toAccountscriteria;
      });

      //setter method for Service Response Identifier in the group:SWIFT Lookup Service
      defineSetter(this, "lookupServiceResponseIdentifier", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lookupServiceResponseIdentifier=val;
        }
      });

      //getter method for Service Response Identifier in the group:SWIFT Lookup Service
      defineGetter(this, "lookupServiceResponseIdentifier", function() {
        return this._lookupServiceResponseIdentifier;
      });

      //setter method for Payee Type in the group:General
      defineSetter(this, "PAYEETYPES", function(val) {
        if((typeof val=='string') && (val != "")){
          this._PAYEETYPES=val;
        }
      });

      //getter method for Payee Type in the group:General
      defineGetter(this, "PAYEETYPES", function() {
        return this._PAYEETYPES;
      });

      //setter method for Dropdown List Field 1 in the group:From Account
      defineSetter(this, "lblFromAccountDropdownListField1", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblFromAccountDropdownListField1=val;
        }
      });

      //getter method for Dropdown List Field 1 in the group:From Account
      defineGetter(this, "lblFromAccountDropdownListField1", function() {
        return this._lblFromAccountDropdownListField1;
      });

      //setter method for Selected Account Detail 1 in the group:To Account
      defineSetter(this, "toAccountTypeList", function(val) {
        if((typeof val=='string') && (val != "")){
          this._toAccountTypeList=val;
        }
      });

      //getter method for Selected Account Detail 1 in the group:To Account
      defineGetter(this, "toAccountTypeList", function() {
        return this._toAccountTypeList;
      });

      //setter method for Payment Amount Option4 in the group:Payment Amount
      defineSetter(this, "paymentAmountOption4", function(val) {
        if((typeof val=='string') && (val != "")){
          this._paymentAmountOption4=val;
        }
      });

      //getter method for Payment Amount Option4 in the group:Payment Amount
      defineGetter(this, "paymentAmountOption4", function() {
        return this._paymentAmountOption4;
      });

      //setter method for Recurrence Text Input Label in the group:Transfer Date
      defineSetter(this, "lblRecurrence", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblRecurrence=val;
        }
      });

      //getter method for Recurrence Text Input Label in the group:Transfer Date
      defineGetter(this, "lblRecurrence", function() {
        return this._lblRecurrence;
      });

      //setter method for Payment Method Option4 in the group:Payment Method
      defineSetter(this, "paymentMethodOption4", function(val) {
        if((typeof val=='string') && (val != "")){
          this._paymentMethodOption4=val;
        }
      });

      //getter method for Payment Method Option4 in the group:Payment Method
      defineGetter(this, "paymentMethodOption4", function() {
        return this._paymentMethodOption4;
      });

      //setter method for Radio Input  Field  Value in the group:Additional Field
      defineSetter(this, "radioInputIValue", function(val) {
        if((typeof val=='string') && (val != "")){
          this._radioInputIValue=val;
        }
      });

      //getter method for Radio Input  Field  Value in the group:Additional Field
      defineGetter(this, "radioInputIValue", function() {
        return this._radioInputIValue;
      });

      //setter method for ReEnter Account Number Text InputÂ  Label in the group:Payee Detail Fields
      defineSetter(this, "lblReenterAccountNumber", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblReenterAccountNumber=val;
        }
      });

      //getter method for ReEnter Account Number Text InputÂ  Label in the group:Payee Detail Fields
      defineGetter(this, "lblReenterAccountNumber", function() {
        return this._lblReenterAccountNumber;
      });

      //setter method for Search Field 2 Label in the group:SWIFT Lookup 
      defineSetter(this, "searchField2Label", function(val) {
        if((typeof val=='string') && (val != "")){
          this._searchField2Label=val;
        }
      });

      //getter method for Search Field 2 Label in the group:SWIFT Lookup 
      defineGetter(this, "searchField2Label", function() {
        return this._searchField2Label;
      });

      //setter method for Radio Input Selected Text Skin in the group:Skins
      defineSetter(this, "sknradioInputSelected", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknradioInputSelected=val;
        }
      });

      //getter method for Radio Input Selected Text Skin in the group:Skins
      defineGetter(this, "sknradioInputSelected", function() {
        return this._sknradioInputSelected;
      });

      //setter method for Payee Address Icon in the group:Icons
      defineSetter(this, "payeeAddressIcon", function(val) {
        if((typeof val=='string') && (val != "")){
          this._payeeAddressIcon=val;
        }
      });

      //getter method for Payee Address Icon in the group:Icons
      defineGetter(this, "payeeAddressIcon", function() {
        return this._payeeAddressIcon;
      });

      //setter method for Object Service Name in the group:From Accounts List Service
      defineSetter(this, "fromAccountObjectServiceName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._fromAccountObjectServiceName=val;
        }
      });

      //getter method for Object Service Name in the group:From Accounts List Service
      defineGetter(this, "fromAccountObjectServiceName", function() {
        return this._fromAccountObjectServiceName;
      });

      //setter method for Service Response Identifier in the group:To Accounts List Service
      defineSetter(this, "toAccountListObject", function(val) {
        if((typeof val=='string') && (val != "")){
          this._toAccountListObject=val;
        }
      });

      //getter method for Service Response Identifier in the group:To Accounts List Service
      defineGetter(this, "toAccountListObject", function() {
        return this._toAccountListObject;
      });

      //setter method for Account Type in the group:General
      defineSetter(this, "ACCOUNTTYPES", function(val) {
        if((typeof val=='string') && (val != "")){
          this._ACCOUNTTYPES=val;
        }
      });

      //getter method for Account Type in the group:General
      defineGetter(this, "ACCOUNTTYPES", function() {
        return this._ACCOUNTTYPES;
      });

      //setter method for Dropdown List Field 2 in the group:From Account
      defineSetter(this, "lblFromAccountDropdownListField2", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblFromAccountDropdownListField2=val;
        }
      });

      //getter method for Dropdown List Field 2 in the group:From Account
      defineGetter(this, "lblFromAccountDropdownListField2", function() {
        return this._lblFromAccountDropdownListField2;
      });

      //setter method for Dropdown List Field 1 in the group:To Account
      defineSetter(this, "lblToAccountDropdownField1", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblToAccountDropdownField1=val;
        }
      });

      //getter method for Dropdown List Field 1 in the group:To Account
      defineGetter(this, "lblToAccountDropdownField1", function() {
        return this._lblToAccountDropdownField1;
      });

      //setter method for txtBoxOtherAmount in the group:Payment Amount
      defineSetter(this, "otherAmountTxtBox", function(val) {
        if((typeof val=='string') && (val != "")){
          this._otherAmountTxtBox=val;
        }
      });

      //getter method for txtBoxOtherAmount in the group:Payment Amount
      defineGetter(this, "otherAmountTxtBox", function() {
        return this._otherAmountTxtBox;
      });

      //setter method for Recurrence Text Input Value in the group:Transfer Date
      defineSetter(this, "txtInputRecurrences", function(val) {
        if((typeof val=='string') && (val != "")){
          this._txtInputRecurrences=val;
        }
      });

      //getter method for Recurrence Text Input Value in the group:Transfer Date
      defineGetter(this, "txtInputRecurrences", function() {
        return this._txtInputRecurrences;
      });

      //setter method for Payment Method Option5 in the group:Payment Method
      defineSetter(this, "paymentMethodOption5", function(val) {
        if((typeof val=='string') && (val != "")){
          this._paymentMethodOption5=val;
        }
      });

      //getter method for Payment Method Option5 in the group:Payment Method
      defineGetter(this, "paymentMethodOption5", function() {
        return this._paymentMethodOption5;
      });

      //setter method for ReEnter Account Number Text Input Value in the group:Payee Detail Fields
      defineSetter(this, "txtInputReenterAccountNumber", function(val) {
        if((typeof val=='string') && (val != "")){
          this._txtInputReenterAccountNumber=val;
        }
      });

      //getter method for ReEnter Account Number Text Input Value in the group:Payee Detail Fields
      defineGetter(this, "txtInputReenterAccountNumber", function() {
        return this._txtInputReenterAccountNumber;
      });

      //setter method for Search Field 2 Value in the group:SWIFT Lookup 
      defineSetter(this, "searchField2Value", function(val) {
        if((typeof val=='string') && (val != "")){
          this._searchField2Value=val;
        }
      });

      //getter method for Search Field 2 Value in the group:SWIFT Lookup 
      defineGetter(this, "searchField2Value", function() {
        return this._searchField2Value;
      });

      //setter method for Radio InputÂ  TextÂ  Skin in the group:Skins
      defineSetter(this, "sknradioInputText", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknradioInputText=val;
        }
      });

      //getter method for Radio InputÂ  TextÂ  Skin in the group:Skins
      defineGetter(this, "sknradioInputText", function() {
        return this._sknradioInputText;
      });

      //setter method for Info Icon in the group:Icons
      defineSetter(this, "infoIcon", function(val) {
        if((typeof val=='string') && (val != "")){
          this._infoIcon=val;
        }
      });

      //getter method for Info Icon in the group:Icons
      defineGetter(this, "infoIcon", function() {
        return this._infoIcon;
      });

      //setter method for Breakpoints in the group:General
      defineSetter(this, "BREAKPTS", function(val) {
        if((typeof val=='string') && (val != "")){
          this._BREAKPTS=val;
        }
      });

      //getter method for Breakpoints in the group:General
      defineGetter(this, "BREAKPTS", function() {
        return this._BREAKPTS;
      });

      //setter method for Dropdown List Field 3 in the group:From Account
      defineSetter(this, "lblFromAccountDropdownListField3", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblFromAccountDropdownListField3=val;
        }
      });

      //getter method for Dropdown List Field 3 in the group:From Account
      defineGetter(this, "lblFromAccountDropdownListField3", function() {
        return this._lblFromAccountDropdownListField3;
      });

      //setter method for Dropdown List Field 2 in the group:To Account
      defineSetter(this, "lblToAccountDropdownField2", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblToAccountDropdownField2=val;
        }
      });

      //getter method for Dropdown List Field 2 in the group:To Account
      defineGetter(this, "lblToAccountDropdownField2", function() {
        return this._lblToAccountDropdownField2;
      });

      //setter method for Payee Detail Field Info Label in the group:Payee Detail Fields
      defineSetter(this, "lblPayeeDetailFieldInfo", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblPayeeDetailFieldInfo=val;
        }
      });

      //getter method for Payee Detail Field Info Label in the group:Payee Detail Fields
      defineGetter(this, "lblPayeeDetailFieldInfo", function() {
        return this._lblPayeeDetailFieldInfo;
      });

      //setter method for Search Field 3 Label in the group:SWIFT Lookup 
      defineSetter(this, "searchField3Label", function(val) {
        if((typeof val=='string') && (val != "")){
          this._searchField3Label=val;
        }
      });

      //getter method for Search Field 3 Label in the group:SWIFT Lookup 
      defineGetter(this, "searchField3Label", function() {
        return this._searchField3Label;
      });

      //setter method for Dropdown Text Skin in the group:Skins
      defineSetter(this, "sknDropdownText", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknDropdownText=val;
        }
      });

      //getter method for Dropdown Text Skin in the group:Skins
      defineGetter(this, "sknDropdownText", function() {
        return this._sknDropdownText;
      });

      //setter method for PDF  Icon in the group:Icons
      defineSetter(this, "pdfIcon", function(val) {
        if((typeof val=='string') && (val != "")){
          this._pdfIcon=val;
        }
      });

      //getter method for PDF  Icon in the group:Icons
      defineGetter(this, "pdfIcon", function() {
        return this._pdfIcon;
      });

      //setter method for Frequency Visibility in the group:Frequency
      defineSetter(this, "frequencyVisibility", function(val) {
        if((typeof val=='boolean') && (val != "")){
          this._frequencyVisibility=val;
        }
      });

      //getter method for Frequency Visibility in the group:Frequency
      defineGetter(this, "frequencyVisibility", function() {
        return this._frequencyVisibility;
      });

      //setter method for Support Document Visibility in the group:Supporting Documents
      defineSetter(this, "supportDocVisibility", function(val) {
        if((typeof val=='boolean') && (val != "")){
          this._supportDocVisibility=val;
        }
      });

      //getter method for Frequency Visibility in the group:Frequency
      defineGetter(this, "supportDocVisibility", function() {
        return this._supportDocVisibility;
      });

      //setter method for Error Message Label in the group:General
      defineSetter(this, "lblErrorMessage", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblErrorMessage=val;
        }
      });

      //getter method for Error Message Label in the group:General
      defineGetter(this, "lblErrorMessage", function() {
        return this._lblErrorMessage;
      });

      //setter method for Enabled Button Skin in the group:Skins
      defineSetter(this, "sknEnabledBtn", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknEnabledBtn=val;
        }
      });

      //getter method for Enabled Button Skin in the group:Skins
      defineGetter(this, "sknEnabledBtn", function() {
        return this._sknEnabledBtn;
      });

      //setter method for Disabed Button Skin in the group:Skins
      defineSetter(this, "sknDisabledBtn", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknDisabledBtn=val;
        }
      });

      //getter method for Disabed Button Skin in the group:Skins
      defineGetter(this, "sknDisabledBtn", function() {
        return this._sknDisabledBtn;
      });

      //setter method for Empty Record  Messge in the group:From Account
      defineSetter(this, "lblFromAccountEmptyRecord", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblFromAccountEmptyRecord=val;
        }
      });

      //getter method for Empty Record  Messge in the group:From Account
      defineGetter(this, "lblFromAccountEmptyRecord", function() {
        return this._lblFromAccountEmptyRecord;
      });

      //setter method for Dropdown List Field 3 in the group:To Account
      defineSetter(this, "lblToAccountDropdownField3", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblToAccountDropdownField3=val;
        }
      });

      //getter method for Dropdown List Field 3 in the group:To Account
      defineGetter(this, "lblToAccountDropdownField3", function() {
        return this._lblToAccountDropdownField3;
      });

      //setter method for Lookup Label in the group:Payee Detail Fields
      defineSetter(this, "lblLookup", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblLookup=val;
        }
      });

      //getter method for Lookup Label in the group:Payee Detail Fields
      defineGetter(this, "lblLookup", function() {
        return this._lblLookup;
      });

      //setter method for Search Field 3 Value in the group:SWIFT Lookup 
      defineSetter(this, "searchField3Value", function(val) {
        if((typeof val=='string') && (val != "")){
          this._searchField3Value=val;
        }
      });

      //getter method for Search Field 3 Value in the group:SWIFT Lookup 
      defineGetter(this, "searchField3Value", function() {
        return this._searchField3Value;
      });

      //setter method for Secondary Button Skin in the group:Skins
      defineSetter(this, "sknSecondaryBtn", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknSecondaryBtn=val;
        }
      });

      //getter method for Secondary Button Skin in the group:Skins
      defineGetter(this, "sknSecondaryBtn", function() {
        return this._sknSecondaryBtn;
      });

      //setter method for JPEG  Icon in the group:Icons
      defineSetter(this, "jpegIcon", function(val) {
        if((typeof val=='string') && (val != "")){
          this._jpegIcon=val;
        }
      });

      //getter method for JPEG  Icon in the group:Icons
      defineGetter(this, "jpegIcon", function() {
        return this._jpegIcon;
      });

      //setter method for Empty Record Action Button in the group:From Account
      defineSetter(this, "btnFromAccountEmptyRecord", function(val) {
        if((typeof val=='string') && (val != "")){
          this._btnFromAccountEmptyRecord=val;
        }
      });

      //getter method for Empty Record Action Button in the group:From Account
      defineGetter(this, "btnFromAccountEmptyRecord", function() {
        return this._btnFromAccountEmptyRecord;
      });

      //setter method for Empty RecordÂ  Message in the group:To Account
      defineSetter(this, "lblToAccountEmptyRecord", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblToAccountEmptyRecord=val;
        }
      });

      //getter method for Empty RecordÂ  Message in the group:To Account
      defineGetter(this, "lblToAccountEmptyRecord", function() {
        return this._lblToAccountEmptyRecord;
      });

      //setter method for Payee Detail1 Text Input Label in the group:Payee Detail Fields
      defineSetter(this, "lblPayeeDetail1", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblPayeeDetail1=val;
        }
      });

      //getter method for Payee Detail1 Text Input Label in the group:Payee Detail Fields
      defineGetter(this, "lblPayeeDetail1", function() {
        return this._lblPayeeDetail1;
      });

      //setter method for Search Field 4 Label in the group:SWIFT Lookup 
      defineSetter(this, "searchField4Label", function(val) {
        if((typeof val=='string') && (val != "")){
          this._searchField4Label=val;
        }
      });

      //getter method for Search Field 4 Label in the group:SWIFT Lookup 
      defineGetter(this, "searchField4Label", function() {
        return this._searchField4Label;
      });

      //setter method for Secondary Button HoverÂ  Skin in the group:Skins
      defineSetter(this, "sknSecondaryBtnHover", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknSecondaryBtnHover=val;
        }
      });

      //getter method for Secondary Button HoverÂ  Skin in the group:Skins
      defineGetter(this, "sknSecondaryBtnHover", function() {
        return this._sknSecondaryBtnHover;
      });

      //setter method for Clear Icon Autocomplete Dropdown in the group:Icons
      defineSetter(this, "clearIcon", function(val) {
        if((typeof val=='string') && (val != "")){
          this._clearIcon=val;
        }
      });

      //getter method for Clear Icon Autocomplete Dropdown in the group:Icons
      defineGetter(this, "clearIcon", function() {
        return this._clearIcon;
      });

      //setter method for Empty Record Action Button in the group:To Account
      defineSetter(this, "btnToAccountEmptyRecordAction", function(val) {
        if((typeof val=='string') && (val != "")){
          this._btnToAccountEmptyRecordAction=val;
        }
      });

      //getter method for Empty Record Action Button in the group:To Account
      defineGetter(this, "btnToAccountEmptyRecordAction", function() {
        return this._btnToAccountEmptyRecordAction;
      });

      //setter method for Payee Detail1 Text Input Value in the group:Payee Detail Fields
      defineSetter(this, "txtInputPayeeDetail1", function(val) {
        if((typeof val=='string') && (val != "")){
          this._txtInputPayeeDetail1=val;
        }
      });

      //getter method for Payee Detail1 Text Input Value in the group:Payee Detail Fields
      defineGetter(this, "txtInputPayeeDetail1", function() {
        return this._txtInputPayeeDetail1;
      });

      //setter method for Search Field 4 Value in the group:SWIFT Lookup 
      defineSetter(this, "searchField4Value", function(val) {
        if((typeof val=='string') && (val != "")){
          this._searchField4Value=val;
        }
      });

      //getter method for Search Field 4 Value in the group:SWIFT Lookup 
      defineGetter(this, "searchField4Value", function() {
        return this._searchField4Value;
      });

      //setter method for Secondary Button FocusÂ  Skin in the group:Skins
      defineSetter(this, "sknSecondaryBtnFocus", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknSecondaryBtnFocus=val;
        }
      });

      //getter method for Secondary Button FocusÂ  Skin in the group:Skins
      defineGetter(this, "sknSecondaryBtnFocus", function() {
        return this._sknSecondaryBtnFocus;
      });

      //setter method for Loader Icon Autocomplete Dropdown in the group:Icons
      defineSetter(this, "loaderIcon", function(val) {
        if((typeof val=='string') && (val != "")){
          this._loaderIcon=val;
        }
      });

      //getter method for Loader Icon Autocomplete Dropdown in the group:Icons
      defineGetter(this, "loaderIcon", function() {
        return this._loaderIcon;
      });

      //setter method for removeAttachDocumentIcon Icon Autocomplete Dropdown in the group:Icons
      defineSetter(this, "removeAttachDocumentIcon", function(val) {
        if((typeof val=='string') && (val != "")){
          this._removeAttachDocumentIcon=val;
        }
      });

      //getter method for removeAttachDocumentIcon Icon Autocomplete Dropdown in the group:Icons
      defineGetter(this, "removeAttachDocumentIcon", function() {
        return this._removeAttachDocumentIcon;
      });

      //setter method for uploadDocumentErrorIcon in the group:Icons
      defineSetter(this, "uploadDocumentErrorIcon", function(val) {
        if((typeof val=='string') && (val != "")){
          this._uploadDocumentErrorIcon=val;
        }
      });

      //getter method for uploadDocumentErrorIcon  in the group:Icons
      defineGetter(this, "uploadDocumentErrorIcon", function() {
        return this._uploadDocumentErrorIcon;
      });


      //setter method for Payee Detail2 Text Input Label in the group:Payee Detail Fields
      defineSetter(this, "lblPayeeDetailField2", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblPayeeDetailField2=val;
        }
      });

      //getter method for Payee Detail2 Text Input Label in the group:Payee Detail Fields
      defineGetter(this, "lblPayeeDetailField2", function() {
        return this._lblPayeeDetailField2;
      });

      //setter method for Column1 Label in the group:SWIFT Lookup 
      defineSetter(this, "lblcolumn1", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblcolumn1=val;
        }
      });

      //getter method for Column1 Label in the group:SWIFT Lookup 
      defineGetter(this, "lblcolumn1", function() {
        return this._lblcolumn1;
      });

      //setter method for Primary Button Disabled Skin in the group:Skins
      defineSetter(this, "sknPrimaryBtnDisabled", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknPrimaryBtnDisabled=val;
        }
      });

      //getter method for Primary Button Disabled Skin in the group:Skins
      defineGetter(this, "sknPrimaryBtnDisabled", function() {
        return this._sknPrimaryBtnDisabled;
      });


      defineSetter(this, "toAccountGroupIdentifier", function(val) {
        if((typeof val=='string') && (val != "")){
          this._toAccountGroupIdentifier=val;
        }
      });


      defineGetter(this, "toAccountGroupIdentifier", function() {
        return this._toAccountGroupIdentifier;
      });

      //setter method for Payee Detail2 Text Input Value in the group:Payee Detail Fields
      defineSetter(this, "txtInputPayeeDetail2", function(val) {
        if((typeof val=='string') && (val != "")){
          this._txtInputPayeeDetail2=val;
        }
      });

      //getter method for Payee Detail2 Text Input Value in the group:Payee Detail Fields
      defineGetter(this, "txtInputPayeeDetail2", function() {
        return this._txtInputPayeeDetail2;
      });

      //setter method for Column2 label  in the group:SWIFT Lookup 
      defineSetter(this, "lblcolumn2", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblcolumn2=val;
        }
      });

      //getter method for Column2 label  in the group:SWIFT Lookup 
      defineGetter(this, "lblcolumn2", function() {
        return this._lblcolumn2;
      });

      //setter method for Primary Button Skin in the group:Skins
      defineSetter(this, "sknPrimaryBtn", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknPrimaryBtn=val;
        }
      });

      //getter method for Primary Button Skin in the group:Skins
      defineGetter(this, "sknPrimaryBtn", function() {
        return this._sknPrimaryBtn;
      });

      //setter method for Dropdown List Icon2 in the group:To Account
      defineSetter(this, "toAccountListArray", function(val) {
        if((typeof val=='string') && (val != "")){
          this._toAccountListArray=val;
        }
      });

      //getter method for Dropdown List Icon2 in the group:To Account
      defineGetter(this, "toAccountListArray", function() {
        return this._toAccountListArray;
      });

      //setter method for Payee Detail3 Text Input Label in the group:Payee Detail Fields
      defineSetter(this, "lblPayeeDetailField3", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblPayeeDetailField3=val;
        }
      });

      //getter method for Payee Detail3 Text Input Label in the group:Payee Detail Fields
      defineGetter(this, "lblPayeeDetailField3", function() {
        return this._lblPayeeDetailField3;
      });

      //setter method for column1 Label Value in the group:SWIFT Lookup 
      defineSetter(this, "lblColumn1Value", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblColumn1Value=val;
        }
      });

      //getter method for column1 Label Value in the group:SWIFT Lookup 
      defineGetter(this, "lblColumn1Value", function() {
        return this._lblColumn1Value;
      });

      //setter method for Primary Button Hover Skin in the group:Skins
      defineSetter(this, "sknPrimaryBtnHover", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknPrimaryBtnHover=val;
        }
      });

      //getter method for Primary Button Hover Skin in the group:Skins
      defineGetter(this, "sknPrimaryBtnHover", function() {
        return this._sknPrimaryBtnHover;
      });

      //setter method for Payee Detail3 Text Input Value in the group:Payee Detail Fields
      defineSetter(this, "txtInputPayeeDetail3", function(val) {
        if((typeof val=='string') && (val != "")){
          this._txtInputPayeeDetail3=val;
        }
      });

      //getter method for Payee Detail3 Text Input Value in the group:Payee Detail Fields
      defineGetter(this, "txtInputPayeeDetail3", function() {
        return this._txtInputPayeeDetail3;
      });

      //setter method for column2 Label Value in the group:SWIFT Lookup 
      defineSetter(this, "lblColumn2Value", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblColumn2Value=val;
        }
      });

      //getter method for column2 Label Value in the group:SWIFT Lookup 
      defineGetter(this, "lblColumn2Value", function() {
        return this._lblColumn2Value;
      });

      //setter method for Primary Button Focus Skin in the group:Skins
      defineSetter(this, "sknPrimaryBtnFocus", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknPrimaryBtnFocus=val;
        }
      });

      //getter method for Primary Button Focus Skin in the group:Skins
      defineGetter(this, "sknPrimaryBtnFocus", function() {
        return this._sknPrimaryBtnFocus;
      });

      //setter method for column3 Label Value in the group:SWIFT Lookup 
      defineSetter(this, "lblColumn3Value", function(val) {
        if((typeof val=='string') && (val != "")){
          this._lblColumn3Value=val;
        }
      });

      //getter method for column3 Label Value in the group:SWIFT Lookup 
      defineGetter(this, "lblColumn3Value", function() {
        return this._lblColumn3Value;
      });

      //setter method for AutoComplete Dropdown Field 1 in the group:Skins
      defineSetter(this, "sknACDropdownField1", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknACDropdownField1=val;
        }
      });

      //getter method for AutoComplete Dropdown Field 1 in the group:Skins
      defineGetter(this, "sknACDropdownField1", function() {
        return this._sknACDropdownField1;
      });

      //setter method for Search Button in the group:SWIFT Lookup 
      defineSetter(this, "btnSearch", function(val) {
        if((typeof val=='string') && (val != "")){
          this._btnSearch=val;
        }
      });

      //getter method for Search Button in the group:SWIFT Lookup 
      defineGetter(this, "btnSearch", function() {
        return this._btnSearch;
      });

      //setter method for AutoComplete Dropdown Field 2 in the group:Skins
      defineSetter(this, "sknACDropdownField2", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknACDropdownField2=val;
        }
      });

      //getter method for AutoComplete Dropdown Field 2 in the group:Skins
      defineGetter(this, "sknACDropdownField2", function() {
        return this._sknACDropdownField2;
      });

      //setter method for AutoComplete Dropdown Field 3 in the group:Skins
      defineSetter(this, "sknACDropdownField3", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknACDropdownField3=val;
        }
      });

      //getter method for AutoComplete Dropdown Field 3 in the group:Skins
      defineGetter(this, "sknACDropdownField3", function() {
        return this._sknACDropdownField3;
      });

      //setter method for AutoComplete Dropdown Field Type in the group:Skins
      defineSetter(this, "sknLblDropdownFieldType", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknLblDropdownFieldType=val;
        }
      });

      //getter method for AutoComplete Dropdown Field Type in the group:Skins
      defineGetter(this, "sknLblDropdownFieldType", function() {
        return this._sknLblDropdownFieldType;
      });

      //setter method for Swift Lookup Select Label Skin in the group:Skins
      defineSetter(this, "sknSelectLabel", function(val) {
        if((typeof val=='string') && (val != "")){
          this._sknSelectLabel=val;
        }
      });

      //getter method for Swift Lookup Select Label Skin in the group:Skins
      defineGetter(this, "sknSelectLabel", function() {
        return this._sknSelectLabel;
      });

      //setter method for emptySearchResult Label Value in the group:SwiftLookup
      defineSetter(this, "emptySearchResult", function(val) {
        if((typeof val=='string') && (val != "")){
          this._emptySearchResult=val;
        }
      });

      //getter method for emptySearchResult Label Value in the group:SwiftLookup
      defineGetter(this, "emptySearchResult", function() {
        return this._emptySearchResult;
      });
      //setter method for IBANObjectServiceName in the group: IBAN Validate Service
      defineSetter(this, "IBANObjectServiceName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._IBANObjectServiceName=val;
        }
      });

      //getter method for IBANObjectServiceName in the group:IBAN Validate Service
      defineGetter(this, "IBANObjectServiceName", function() {
        return this._IBANObjectServiceName;
      });
      //setter method for IBANObjectName in the group: IBAN Validate Service
      defineSetter(this, "IBANObjectName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._IBANObjectName=val;
        }
      });

      //getter method for IBANObjectName in the group:IBAN Validate Service
      defineGetter(this, "IBANObjectName", function() {
        return this._IBANObjectName;
      });

      //setter method for IBANCriteria in the group: IBAN Validate Service
      defineSetter(this, "IBANCriteria", function(val) {
        if((typeof val=='string') && (val != "")){
          this._IBANCriteria=val;
        }
      });

      //getter method for IBANCriteria in the group:IBAN Validate Service
      defineGetter(this, "IBANCriteria", function() {
        return this._IBANCriteria;
      });
      //setter method for IBANOperationName in the group: IBAN Validate Service
      defineSetter(this, "IBANOperationName", function(val) {
        if((typeof val=='string') && (val != "")){
          this._IBANOperationName=val;
        }
      });

      //getter method for IBANOperationName in the group:IBAN Validate Service
      defineGetter(this, "IBANOperationName", function() {
        return this._IBANOperationName;
      });
      //setter method for getBeneficiaryService in the group: Get beneficiaryname service
      defineSetter(this, "getBeneficiaryService", function(val) {
        if((typeof val=='string') && (val != "")){
          this._getBeneficiaryService=val;
        }
      });

      //getter method for getBeneficiaryService in the group: Get beneficiaryname service
      defineGetter(this, "getBeneficiaryService", function() {
        return this._getBeneficiaryService;
      });
      //setter method for getBeneficiaryObject in the group: Get beneficiaryname service
      defineSetter(this, "getBeneficiaryObject", function(val) {
        if((typeof val=='string') && (val != "")){
          this._getBeneficiaryObject=val;
        }
      });

      //getter method for getBeneficiaryObject in the group: Get beneficiaryname service
      defineGetter(this, "getBeneficiaryObject", function() {
        return this._getBeneficiaryObject;
      });
      //setter method for getBeneficiaryOperation in the group: Get beneficiaryname service
      defineSetter(this, "getBeneficiaryOperation", function(val) {
        if((typeof val=='string') && (val != "")){
          this._getBeneficiaryOperation=val;
        }
      });

      //getter method for getBeneficiaryOperation in the group: Get beneficiaryname service
      defineGetter(this, "getBeneficiaryOperation", function() {
        return this._getBeneficiaryOperation;
      });
      //setter method for getBeneficiaryCriteria in the group: Get beneficiaryname service
      defineSetter(this, "getBeneficiaryCriteria", function(val) {
        if((typeof val=='string') && (val != "")){
          this._getBeneficiaryCriteria=val;
        }
      });

      //getter method for getBeneficiaryCriteria in the group: Get beneficiaryname service
      defineGetter(this, "getBeneficiaryCriteria", function() {
        return this._getBeneficiaryCriteria;
      });
       //setter method for baseCurrency in the group: General
      defineSetter(this, "baseCurrency", function(val) {
        if((typeof val=='boolean') && (val != "")){
          this._baseCurrency=val;
        }
      });

      //getter method for baseCurrency in the group: General
      defineGetter(this, "baseCurrency", function() {
        return this._baseCurrency;
      });

    },

    /**
	* @api : preShow
	* Reponsible to retain the data for custom properties for multiple entries into the component
	* Invoke the DAO layer to collect information from the service.
	* @return : NA
	*/
    preshow: function() {
      var scope = this;
      scope.resetErrors();
      if(this.context.flowType !== "edit")
        this.createTransfer();
    },

    /**
     * createTransfer
     * @api : createTransfer
     * manages UI
     * @return : NA
     */
    createTransfer: function () {
      var scope = this;
      this.setComponentConfigs();
      this.getTransferTypeFromContext();
      this.getPayeeTypeFromContext();
      this.getAccountType();
      this.context = this.setSwiftLookupContext();
      this.view.lblLookup.onTouchEnd = this.actionHandler.bind(this,this.context,this._lblLookup);
      this.view.imgAttachmentIcon.onTouchEnd = this.browseSupportingDoc;
      this.view.lblPayeeType1Option.onTouchEnd = this.onClickeExistingPayeeRadioButton;
      this.view.lblPayeeType2Option.onTouchEnd = this.onClickNewPayeeRadioButton;
      this.view.lblPaymentMethodOption1.onTouchEnd = this.onClickSEPARadioButton;
      this.view.lblPaymentMethodOption2.onTouchEnd = this.onClickInstantRadioButton;
      this.view.lblPaymentMethodOption3.onTouchEnd = this.onClickSWIFTRadioButton;
      this.view.lblPaymentMethodOption4.onTouchEnd = this.onClickCCYClearingRadioButton;
      this.view.lblFeePaidOption1.onTouchEnd = this.onClickMeRadioButton;
      this.view.lblFeePaidOption2.onTouchEnd = this.onClickBeneficiaryRadioButton;
      this.view.lblFeePaidOption3.onTouchEnd = this.onClickSharedRadioButton;
      this.view.lblPaymentAmountOption1.onTouchEnd = this.onClickOutStandingDueRadioButton;
      this.view.lblPaymentAmountOption2.onTouchEnd = this.onClickStatementDueRadioButton;
      this.view.lblPaymentAmountOption3.onTouchEnd = this.onClickMinimumDueRadioButton;
      this.view.lblPaymentAmountOption4.onTouchEnd = this.onClickOtherAmountRadioButton;
      this.view.txtBoxTransferAmount.onEndEditing = this.onAmountChanged;
      this.view.txtBoxTransferAmount.onBeginEditing = this.onEditAmount;
      this.view.txtBoxAmountOthers.onEndEditing = this.onOthersAmountChanged;
      this.view.txtBoxAmountOthers.onBeginEditing = this.onEditOthersAmount;																	 
      this.view.segTransferCCYList.onRowClick = this.onCurrencySelect;
      this.view.txtAreaNotes.onKeyUp = this.countLength;
      this.initActions();
    },

    /**
     * countLength
     * @api : countLength
     * counts the lenght of data on text box
     * @return : NA
     */
    countLength: function() {
      var scope = this;
      var textData = this.view.txtAreaNotes.text;
      var totalLength = textData.length;
      var stringData = totalLength.toString();
      scope.view.lblLength.text = stringData;
      scope.view.flxNotesLength.forceLayout();
    },

    /**
     * onAmountChanged
     * @api : onAmountChanged
     * triggered when amount gets changed
     * @return : NA
     */
    onAmountChanged: function() {
      var formatData = this.getParsedValue(this._amountFormat);
      this.unifiedTransfersUtility.validateAndFormatAmount(this.view.txtBoxTransferAmount, formatData);
      this.updateContext("txtBoxTransferAmount", this.view.txtBoxTransferAmount.text);
    },

    /**
     * onEditAmount
     * @api : onEditAmount
     * triggered when amount gets edited
     * @return : NA
     */
    onEditAmount: function() {
      this.unifiedTransfersUtility.removeCurrencyWithCommas(this.view.txtBoxTransferAmount);
    },

    /**
     * onOthersAmountChanged
     * @api : onOthersAmountChanged
     * triggered when others amount gets changed
     * @return : NA
     */
    onOthersAmountChanged: function() {
      this.unifiedTransfersUtility.validateAndFormatAmount(this.view.txtBoxAmountOthers);
      this.updateContext("txtBoxAmountOthers", this.view.txtBoxAmountOthers.text);
      this.dueAmount = this.view.txtBoxAmountOthers.text;
      this.minFillValidation();
    },

    /**
     * onEditOthersAmount
     * @api : onEditOthersAmount
     * triggered when others amount gets edited
     * @return : NA
     */
    onEditOthersAmount: function() {
      this.unifiedTransfersUtility.removeCurrencyWithCommas(this.view.txtBoxAmountOthers);
    },

    /**
     * onCurrencySelect
     * @api : onCurrencySelect
     * sets payment method for specific currency
     * @return : NA
     */
    onCurrencySelect: function() {
      var scope = this;
      var currentDatainSelectedRow = scope.view.segTransferCCYList.selectedRowItems[0];
      this.transferCurrency = currentDatainSelectedRow["key"];   
      scope.view.lblSelectedTransferCCY.text = currentDatainSelectedRow["value"]["text"];
      scope.view.lblCurrencySymbol.text = currentDatainSelectedRow["value"]["symbol"];
      scope.view.flxTransferCCYList.setVisibility(false);
      scope.view.imgTransferCCYDropdownIcon.src = this.Icons.DropdownExpand;	
      if(this.view.flxPaymentMethodField.isVisible === true) {
      for(var i=1;i<=4;i++) {
        if(scope.view["lblPaymentMethod"+i].text === "RINGS") 
          scope.view["lblPaymentMethod"+i].text = "SWIFT";
        else if(scope.view["lblPaymentMethod"+i].text === "BISERA")
          scope.view["lblPaymentMethod"+i].text = "Local Domestic Clearing(BGN)"
          }
      if(currentDatainSelectedRow["key"] === "EUR") {
        if(scope.isIBANValid === "YES") {
          for(var i=1;i<=4;i++) {
            if(scope.view["lblPaymentMethod"+i].text === "Local Domestic Clearing(BGN)"){
              scope.view["flxPaymentMethod"+i].setVisibility(false);
            } else{
              scope.view["flxPaymentMethod"+i].setVisibility(true);
              scope.view["flxPaymentMethod"+i].setEnabled(true);
              scope.view["lblPaymentMethodOption"+i].skin = this.Skins.unseletedRadioBtnSkn;
            }
          }
        }
        else if(scope.isIBANValid === "NO") {
          for(var i=1;i<=4;i++) {
            if(scope.view["lblPaymentMethod"+i].text === "SWIFT"){
              scope.view["flxPaymentMethod"+i].setVisibility(true);
              scope.view["flxPaymentMethod"+i].setEnabled(true);
              scope.view["lblPaymentMethodOption"+i].skin = this.Skins.unseletedRadioBtnSkn;
            } else if(scope.view["lblPaymentMethod"+i].text === "Local Domestic Clearing(BGN)"){
              scope.view["flxPaymentMethod"+i].setVisibility(false);
            } else {
              scope.view["flxPaymentMethod"+i].setVisibility(true);
              scope.view["flxPaymentMethod"+i].setEnabled(false);
              scope.view["lblPaymentMethodOption"+i].text = this.Icons.unSelectedRadioButton;
              scope.view["lblPaymentMethodOption"+i].skin = this.Skins.disabledRadioBtnSkn;
            }
          }
        }
      }
      else if(currentDatainSelectedRow["key"] === "BGN") {
        var count = 0;
        for(var i=1;i<=4;i++) {
          if(scope.view["lblPaymentMethod"+i].text === "SEPA" || scope.view["lblPaymentMethod"+i].text === "INSTANT"){
            scope.view["flxPaymentMethod"+i].setVisibility(false);
          } else {
            if(count) 
              scope.view["lblPaymentMethod"+i].text = "BISERA";
            else {
              scope.view["lblPaymentMethod"+i].text = "RINGS";
              count++; 
            }
            scope.view["flxPaymentMethod"+i].setVisibility(true);
            scope.view["flxPaymentMethod"+i].setEnabled(true);
            scope.view["lblPaymentMethodOption"+i].skin = this.Skins.unseletedRadioBtnSkn;
          }
        }
      }
      else {
        for(var i=1;i<=4;i++) {
          if(scope.view["lblPaymentMethod"+i].text === "SWIFT"){
            scope.view["flxPaymentMethod"+i].setVisibility(true);
            scope.view["flxPaymentMethod"+i].setEnabled(true);
            scope.view["lblPaymentMethodOption"+i].skin = this.Skins.unseletedRadioBtnSkn;
          } else {
            scope.view["flxPaymentMethod"+i].setVisibility(false);
            scope.view["flxPaymentMethod"+i].setEnabled(false);
            scope.view["lblPaymentMethodOption"+i].text = this.Icons.unSelectedRadioButton;
            scope.view["lblPaymentMethodOption"+i].skin = this.Skins.disabledRadioBtnSkn;
          }
        }
      }
      scope.onClickSWIFTRadioButton();
      }
      scope.minFillValidation();
    },
    
    /**
	* setComponentConfigs
	* @api : setComponentConfigs
	* responsible for sending componentContext passed into parserUtilManager.
	* @return : NA
	*/
    setComponentConfigs: function() {
      this.parserUtilsManager.setBreakPointConfig(JSON.parse(this._BREAKPTS));
    },

    /**
	* @api : postShow
	* event called after ui rendered on the screen, is a component life cycle event.
	* @return : NA
	*/
    postShow: function() {
      var scope = this;
      scope.resetErrors();
      this.setSupportDocumentVisibility();
      this.setFrequencyVisibility();
      this.setPayeeAddress();
      if(this.context.flowType !== "edit"){
        this.createTransferPostshow();}
     if(this.context.flowType === "edit"){
       this.setSwiftandPhoneFormats();
     }
    },

    /**
      * @api : createTransferPostshow
      * event called after ui rendered on the screen, on edit flow.
      * @return : NA
      */
    createTransferPostshow: function() {
      this.fetchFromAccounts();
      this.fetchToAccounts();
      this.setPayeeAddress();
      if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T1")) {
        this.fetchCreditCardAccounts(); 
      } else {
        this.crAccountAPISuccess = true;
      }																		  
      this.setFrequency();
      this.getBankDate();												
      this.setLabelTexts();
      this.storeIconValues();
      this.storeSkinValues();
      this.setFieldContracts();
      this.setFieldInfoTexts();
      this.populateButtonTexts();
      this.setSkins();
      this.setIcons();
      this.setTextBoxAction();
      this.setTextAreaProperties();
      this.setPayeeType();
      this.setLabelWidth();
    },
    /**
     * @api : onBreakPointChange
     * Triggers when break point change takes place.
     * @return : NA
     */
    onBreakPointChange: function() {
      this.setLabelTexts();
      this.setPayeeAddress();
      this.storeIconValues();
      //this.view.lblLookup.onTouchEnd = this.showLookup;
      this.storeSkinValues();
      this.setFieldContracts();
      this.setFieldInfoTexts();
      this.populateButtonTexts();
      this.setSkins();
      this.setIcons();
      this.setTextAreaProperties();
      this.setRadioButtonData();
      this.setSupportDocumentVisibility();
      this.setFrequencyVisibility();
      this.setLabelWidth();
      //  this.setPayeeType();
    },

    /**
     * @api : setFrequencyVisibility
     * sets the visibility of frequency related fields by default
     * @return : NA
     */
    setFrequencyVisibility: function() {
      if(this._frequencyVisibility) {
        this.view.flxFrequencyOptions.setVisibility(true);
        this.view.flxDateandRecurrenceField.setVisibility(true);
      }
      else {
        this.view.flxFrequencyOptions.setVisibility(false);
        this.view.flxDateandRecurrenceField.setVisibility(false);
      }
    },

    /**
     * @api : setSupportDocumentVisibility
     * sets the visibility of supporting document field by default
     * @return : NA
     */
    setSupportDocumentVisibility: function() {
      if(this._supportDocVisibility) {
        this.view.flxSupportingDocuments.setVisibility(true);
      }
      else {
        this.view.flxSupportingDocuments.setVisibility(false);
      }
    },

    /**
     * @api : setPayeeType
     * sets the default type of payee
     * @return : NA
     */
    setPayeeType: function() {
      if(this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT2"))
        this.onClickeExistingPayeeRadioButton(false);
      else if(this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT1"))
        this.onClickNewPayeeRadioButton(false);
    },

    /**
     * Component getTransferTypeFromContext.
     * To set transfer type from the context object.
     */
    getTransferTypeFromContext :function(){
      try {
        this.transferTypeContext = this.parserUtilsManager.getParsedValue(this._transferType);
      } catch(err) {
        var errObj = {
          "errorInfo" : "Error in getTransferTypeFromContext method of the component.",
          "errorLevel" : "Configuration",
          "error": err
        };
      }
    },

    /**
     * Component getPayeeTypeFromContext.
     * To set transfer type from the context object.
     */
    getPayeeTypeFromContext :function(){
      try {
        this.payeeTypeContext = this.parserUtilsManager.getParsedValue(this._payeeType);
      } catch(err) {
        var errObj = {
          "errorInfo" : "Error in getPayeeTypeFromContext method of the component.",
          "errorLevel" : "Configuration",
          "error": err
        };
      }
    },

    /**
     * Component getAccountType
     * To set Account type from the context object.
     */
    getAccountType: function() {
      try {
        this.accountTypeContext = this.parserUtilsManager.getParsedValue(this._ACCOUNTTYPES);
      } catch(err) {    
        var errObj = {
          "errorInfo" : "Error in getPayeeTypeFromContext method of the component.",
          "errorLevel" : "Configuration",
          "error": err
        };
      }
    },
    /**
     * @api : setRadioButtonData
     * sets data in radio buttons
     * @return : NA
     */
    setRadioButtonData: function() {
      if(!this.isEmptyNullUndefined(this.payeeTypeContext)) {
        if(this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT1")) this.onClickNewPayeeRadioButton(false);
        else if(this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT2")) this.onClickeExistingPayeeRadioButton(false);
      }
      if(!this.isEmptyNullUndefined(this.paymentMethod)) {
        if(this.paymentMethod === "SEPA") this.onClickSEPARadioButton();
        else if(this.paymentMethod === "Instant") this.onClickInstantRadioButton();
        else if(this.paymentMethod === "RINGS" || this.paymentMethod === "SWIFT") this.onClickSWIFTRadioButton();
        else if(this.paymentMethod === "BISERA" || this.paymentMethod === "Local CCY Clearing") this.onClickCCYClearingRadioButton();
      }
      if(!this.isEmptyNullUndefined(this.isPaidBy)) {
        if(this.isPaidBy === "OUR") this.onClickMeRadioButton();
        else if(this.isPaidBy === "BEN") this.onClickBeneficiaryRadioButton();
        else if(this.isPaidBy === "SHA") this.onClickSharedRadioButton();
      }
      if(!this.isEmptyNullUndefined(this.dueTypes)) {
        if(this.dueTypes === "Outstanding Due") this.onClickOutStandingDueRadioButton();
        else if(this.dueTypes === "Statement Due") this.onClickStatementDueRadioButton();
        else if(this.dueTypes === "Minimum Due") this.onClickMinimumDueRadioButton();
        else if(this.dueTypes === "Other Amount") this.onClickOtherAmountRadioButton();
      }
    },

    /**
     * @api : setTextAreaProperties
     * sets properties for text area
     * @return : NA
     */
    setTextAreaProperties: function() {
      var scope = this;
      var maxLength = this.getParsedValue(this._maxCharacterNotes);
      if(scope.isNullOrUndefinedOrEmpty(maxLength)) {
        scope.view.flxNotesLength.setVisibility(false);
      }
      this.view.txtAreaNotes.maxTextLength = maxLength;
      var stringData = maxLength.toString();
      this.view.lblTotalLength.text = stringData;
      this.view.lblCountSeperator.text = "/";
      this.countLength();
    },

    /**
     * @api : setTextBoxAction
     * sets action for text boxes
     * @return : NA
     */
    setTextBoxAction: function() {
      var scope = this;
      scope.view.txtBoxFXRateReference.onEndEditing = function() {
        scope.updateContext("txtBoxFXRateReference", scope.view.txtBoxFXRateReference.text);
      } 
      scope.view.txtBoxRecurrences.onEndEditing = function() {
        scope.updateContext("txtBoxRecurrences", scope.view.txtBoxRecurrences.text);
      }
      scope.view.txtBoxE2E.onEndEditing = function() {
        scope.updateContext("txtBoxE2E", scope.view.txtBoxE2E.text);
      }
      scope.view.txtBoxIntermediaryBIC.onEndEditing = function() {
        scope.updateContext("txtBoxIntermediaryBIC", scope.view.txtBoxIntermediaryBIC.text);
      }
      scope.view.txtBoxPayeeDetailField1.onEndEditing = function() {
        scope.updateContext("txtBoxPayeeDetailField1", scope.view.txtBoxPayeeDetailField1.text);
        if(scope.payeeTypeContext === scope.getFieldValue(scope._PAYEETYPES, "PT2"))
          scope.existingPayeeData1 = scope.view.txtBoxPayeeDetailField1.text;
        if(scope.payeeTypeContext === scope.getFieldValue(scope._PAYEETYPES, "PT1")){
          scope.phoneNumberVisibilityforP2P();
          scope.newPayeeData1 = scope.view.txtBoxPayeeDetailField1.text;
        }
      }
      scope.view.txtBoxPayeeDetailField2.onEndEditing = function() {
        scope.updateContext("txtBoxPayeeDetailField2", scope.view.txtBoxPayeeDetailField2.text);
        if(scope.payeeTypeContext === scope.getFieldValue(scope._PAYEETYPES, "PT2"))
          scope.existingPayeeData2 = scope.view.txtBoxPayeeDetailField2.text;
        if(scope.payeeTypeContext === scope.getFieldValue(scope._PAYEETYPES, "PT1")){
          scope.emailVisibilityforP2P();
          scope.newPayeeData2 = scope.view.txtBoxPayeeDetailField2.text;
        }  
      }
      scope.view.txtBoxPayeeDetailField3.onEndEditing = function()
      {
        scope.updateContext("txtBoxPayeeDetailField3", scope.view.txtBoxPayeeDetailField3.text);
        if(scope.payeeTypeContext === scope.getFieldValue(scope._PAYEETYPES, "PT2"))
          scope.existingPayeeData3 = scope.view.txtBoxPayeeDetailField3.text;
        if(scope.payeeTypeContext === scope.getFieldValue(scope._PAYEETYPES, "PT1"))
          scope.newPayeeData3 = scope.view.txtBoxPayeeDetailField3.text;
      }
      scope.view.txtBoxPayeeDetailField4.onEndEditing = function() {
        scope.updateContext("txtBoxPayeeDetailField4", scope.view.txtBoxPayeeDetailField4.text);
        if(scope.payeeTypeContext === scope.getFieldValue(scope._PAYEETYPES, "PT2"))
          scope.existingPayeeData4 = scope.view.txtBoxPayeeDetailField4.text;
        if(scope.payeeTypeContext === scope.getFieldValue(scope._PAYEETYPES, "PT1"))
          scope.newPayeeData4 = scope.view.txtBoxPayeeDetailField4.text;
      }
      scope.view.txtBoxPayeeName.onEndEditing = function() {
        scope.updateContext("txtBoxPayeeName", scope.view.txtBoxPayeeName.text);
      }
      scope.view.txtAreaNotes.onEndEditing = function() {
        scope.updateContext("txtAreaNotes", scope.view.txtAreaNotes.text);
      }
      scope.view.txtBoxPayeeDetailField1.onKeyDown = function() {
        if(scope.transferTypeContext === scope.getFieldValue(scope._transferFlow, "T2") || scope.transferTypeContext === scope.getFieldValue(scope._transferFlow, "T3")) {
          if(scope.view.txtBoxPayeeDetailField4.isVisible === true) {
            scope.view.txtBoxPayeeDetailField4.setEnabled(true);
            scope.view.txtBoxPayeeDetailField4.skin = scope.Skins.TxtBoxField;
          }
        }
      }
    },

    /**
     * @api : setPayloadData
     * carries the data to next form
     * @return : NA
     */
    setPayloadData: function() {
      var scope = this;
      this.dataContext["flowType"] = "add";
      this.dataContext["payeeType"] = this.payeeTypeContext; 
      this.dataContext["iban"] = this.IBAN;
      this.updateContext("txtBoxPayeeDetailField1", this.view.txtBoxPayeeDetailField1.text);
      //  this.updateContext("txtBoxFXRateReference", this.view.txtBoxFXRateReference.text);
      this.dataContext["notes"] = this.view.txtAreaNotes.text;
      this.updateContext("txtAreaNotes", this.view.txtAreaNotes.text);
      this.dataContext["transferCurrency"] = this.getCurrencyCode(this.transferCurrency)+" "+this.transferCurrency; 
      this.dataContext["transactionCurrency"] = this.transferCurrency;
      this.dataContext["paymentMethod"] = this.paymentMethod;     
      this.dataContext["paidBy"] = this.isPaidBy;
      var feesPaidSelected = "";
      if(this.view.lblFeePaidOption1.skin == this.Skins.selectedRadioButtonSkin){
        feesPaidSelected = this.view.lblFeePaidType1.text;
      }else if (this.view.lblFeePaidOption2.skin == this.Skins.selectedRadioButtonSkin){
         feesPaidSelected = this.view.lblFeePaidType2.text;
      }else if (this.view.lblFeePaidOption3.skin == this.Skins.selectedRadioButtonSkin){
        feesPaidSelected = this.view.lblFeePaidType3.text;
      }
      this.dataContext["feesPaidBy"] = feesPaidSelected;    
      this.dataContext["paymentType"] = "";        
      scope.dataContext["addressLine1"] = this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT1") ? scope.newPayeeAddressLine01 : scope.existingPayeeAddressLine01;
      scope.dataContext["addressLine2"] = this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT1") ? scope.newPayeeAddressLine02 : scope.existingPayeeAddressLine02;
      scope.dataContext["country"] = this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT1") ?scope.newPayeeCountry : scope.existingPayeeCountry;
      scope.dataContext["state"] = this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT1") ? scope.newPayeeState : scope.existingPayeeState;
      scope.dataContext["city"] = this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT1") ? scope.newPayeeCity : scope.existingPayeeCity;
      scope.dataContext["zipCode"] = this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT1") ? scope.newPayeeZipCode : scope.existingPayeeZipCode;
      scope.dataContext["phoneNumberDisplay"] = this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT1") ? scope.newPayeePhoneNumber : scope.existingPayeePhoneNumber;
      scope.dataContext["emailAddress"] = this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT1") ? scope.newPayeeEmailAddress : scope.existingPayeeEmailAddress;    
      this.dataContext["frequencyType"] = this.frequency;
      this.dataContext["frequencyStartDate"] = this.getBackendDateFormat(this.view.calStartDate.formattedDate,this.view.calStartDate.dateFormat);
      this.dataContext["scheduledDate"] = this.getBackendDateFormat(this.view.calStartDate.formattedDate,this.view.calStartDate.dateFormat);
      this.dataContext["sendOn"] = this.frequency === "Once" ? this.view.calStartDate.formattedDate : "";
      if (this.view.txtBoxRecurrences.isVisible) this.updateContext("txtBoxRecurrences", this.view.txtBoxRecurrences.text);
      if (this.view.flxFrequencyOptions.isVisible){	
        this.dataContext["frequency"] = this.view.lblSelectedFrequency.text;		
        this.dataContext["howLong"] = this.view.lblSelectedTransferDurationType.text;				
        if(this.view.calStartDate.isVisible ===true && this.view.flxEndDateField.isVisible === true){
          this.dataContext["frequencyEndDate"] = this.getBackendDateFormat(this.view.calEndDate.formattedDate,this.view.calEndDate.dateFormat);
          this.dataContext["endDate"] = this.view.calEndDate.formattedDate;
          this.dataContext["startDate"] = this.view.calStartDate.formattedDate;
          this.dataContext["numberOfRecurrences"] = "";
        }else{
          this.dataContext["frequencyEndDate"] = "";
          this.dataContext["endDate"] = "";
          this.dataContext["startDate"] = "";
          this.dataContext["numberOfRecurrences"] = "";
        }
      }
      if(this.view.flxRecurrencesField.isVisible === true){
        this.dataContext["sendOn"] = this.view.calStartDate.formattedDate;
        this.dataContext["numberOfRecurrences"] = this.view.txtBoxRecurrences.text;
        this.dataContext["frequencyEndDate"] = "";
        this.dataContext["endDate"] = "";
      }
      if (this.view.txtBoxTransferAmount.isVisible) this.updateContext("txtBoxTransferAmount", this.view.txtBoxTransferAmount.text); //use another amnt		
      this.dataContext["fromAccountNumber"] = this.fromAccount.accountID 
      this.dataContext["fromAccountCurrency"] = this.fromAccount.currencyCode;
      this.dataContext["from"] = this.fromAccount.lblField1["text"];		
      this.dataContext["transactionType"] = "ExternalTransfer";
      this.dataContext["toAccountCurrency"] = !this.isNullOrUndefinedOrEmpty(this.toAccount["currencyCode"]) ? this.toAccount["currencyCode"] : this.transferCurrency;      
      this.dataContext["serviceName"] = "";
      this.dataContext["uploadedattachments"] =  "";
      this.dataContext["serviceName"] =  "";
      if(this.view.flxIntermediaryBICE2EField.isVisible === "true")this.dataContext["intermidiaryBic"] = this.view.txtBoxIntermediaryBIC.text;
      if(this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT1"))
      {
        this.dataContext["beneficiaryNickname"] = this.view.txtBoxPayeeName.text;
        this.dataContext["beneficiaryName"] = this.view.txtBoxPayeeName.text;
        if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T1")) this.dataContext["serviceName"] =  "INTRA_BANK_FUND_TRANSFER_CREATE";
        if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T2")) this.dataContext["serviceName"] =  "INTER_BANK_ACCOUNT_FUND_TRANSFER_CREATE";
        if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T3")) this.dataContext["serviceName"] =  "INTERNATIONAL_ACCOUNT_FUND_TRANSFER_CREATE";
        if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T4")) this.dataContext["serviceName"] =  "";
        this.dataContext["IsExternalAccount"] = true;
        if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T2") || this.transferTypeContext === this.getFieldValue(this._transferFlow, "T3")) {
          this.dataContext["swiftCode"] = this.view.txtBoxPayeeDetailField1.text;
          this.dataContext["bankName"] = this.view.txtBoxPayeeDetailField4.text;
        }
        else if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T4")) {
          this.dataContext["phoneNumber"] = this.getDeformattedPhoneNumber(this.view.txtBoxPayeeDetailField1.text);
          this.dataContext["phoneNumerDisplay"] = this.view.txtBoxPayeeDetailField1.text;
        } 
        this.dataContext["to"] = this.view.txtBoxPayeeName.text;
        this.updateContext("txtBoxNewAccountNumber", this.view.txtBoxNewAccountNumber.text);
        this.dataContext["toAccountNumber"] =this.view.txtBoxNewAccountNumber.text; 
        this.dataContext["accountNumber"] = this.view.txtBoxNewAccountNumber.text;  
        this.dataContext["ExternalAccountNumber"] = this.view.txtBoxNewAccountNumber.text;        
      } else {
        this.dataContext["beneficiaryName"] = this.toAccount.beneficiaryName;
        this.dataContext["beneficiaryNickname"] = this.toAccount.nickName ? this.toAccount.nickName : "";
        this.dataContext["to"] = this.toAccount.lblField1["text"];
        this.dataContext["toAccountNumber"] = this.toAccount.accountNumber;
        this.dataContext["ExternalAccountNumber"] = this.toAccount.accountNumber;
        this.dataContext["swiftCode"] = this.toAccount.swiftCode;
        this.dataContext["bankName"] = this.toAccount.bankName;
        this.dataContext["IsExternalAccount"] = this.toAccount.isExternalAccount;
      }
      if(this.dataContext["IsExternalAccount"] == false){
        this.dataContext["transactionType"] = "InternalTransfer";
        this.dataContext["toAccountNumber"] = this.toAccount.accountID;
        this.dataContext["ExternalAccountNumber"] = this.toAccount.accountID;
      }
      if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T3")) {
        this.dataContext["accountType"] = "International Account";
        this.dataContext["transferType"] = "International Transfer";
        this.dataContext["beneAccountType"] = "International Account";
      }
      else if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T1")) {
        this.dataContext["transferType"] = "Same Bank Transfer";
        this.dataContext["beneAccountType"] = "Same Bank Account";
      }
      else if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T2")) {
        this.dataContext["beneAccountType"] = "Domestic Account";
        this.dataContext["transferType"] = "Domestic Transfer";
      }
      else {
        this.dataContext["transactionType"] = "P2P";
        this.dataContext["beneAccountType"] = "Pay a Person Account";
        this.dataContext["transferType"] = "Pay a Person";
        this.dataContext["PayPersonId"] = this.toAccount.PayPersonId;
      }
      this.dataContext["accountType"] = this.accountTypeContext;
      if(this.accountTypeContext === this.getFieldValue(this._ACCOUNTTYPES, "AT1") ||this.accountTypeContext === this.getFieldValue(this._ACCOUNTTYPES, "AT2") &&(this.view.flxPaymentAmountField.isVisible ===true)){
        this.dataContext["amount"] = this.getDeFormattedAmount(this.dueAmount);       
        this.dataContext["outstandingDue"] = this.view.lblPaymentAmountOption1.text === 'M' ?  this.getCurrencyCode(this.transferCurrency)+" "+this.dueAmount : "";
        this.dataContext["statementDue"] = this.view.lblPaymentAmountOption2.text === 'M' ?  this.getCurrencyCode(this.transferCurrency)+" "+this.dueAmount : "";
        this.dataContext["minimumDue"] = this.view.lblPaymentAmountOption3.text === 'M'  ?  this.getCurrencyCode(this.transferCurrency)+" "+this.dueAmount : "";
        this.dataContext["otherAmount"] =  this.view.lblPaymentAmountOption4.text === 'M'  ?  this.getCurrencyCode(this.transferCurrency)+" "+this.dueAmount : "";
        this.dataContext["dueDate"] = this.dueDate;
      }else{
        this.dataContext["outstandingDue"] = "";
        this.dataContext["statementDue"] = "";
        this.dataContext["minimumDue"] = "";
        this.dataContext["otherAmount"] = "";
        this.dataContext["dueDate"] = "";
        this.dataContext["amount"] = this.getDeFormattedAmount(this.view.txtBoxTransferAmount.text);
        this.dataContext["amountDisplay"] = this.getCurrencyCode(this.transferCurrency)+" "+this.view.txtBoxTransferAmount.text;
      }
      this.dataContext["attachedFileList"] = scope.filesToBeUploaded;        
      this.dataContext["isScheduled"] = (this.dataContext["frequency"] !== "Once" || this.compareStartAndEndDate(this.dataContext["frequencyStartDate"], this.todayDate)) ? "1" : "0";
      this.parserUtilsManager.setContext(this.dataContext);
      this.dataContext["transactionsNotes"] = this.view.txtAreaNotes.text;
    },

    /**
      * @api : compareStartAndEndDate
      * compares start and end date
      * @return : boolean
      */
    compareStartAndEndDate: function(startDate,todayDate) {
      startDate = new Date(startDate);
      startDate = startDate.setHours(0,0,0,0);
      todayDate = new Date(todayDate);
      todayDate = todayDate.setHours(0,0,0,0);
      return startDate > todayDate ? true : false;         
    },

    /**
	* @api : getDeFormattedAmount
	* Get formatted amount and returns  amount without format
	* @return : Deformatted amount
	*/
    getDeFormattedAmount : function(amount){
      return  amount.replace(/\,/g,'');
    },

    /**
	* @api : getDeformattedPhoneNumber
	* Get formatted phone number and returns  phone number without format
	* @return : Deformatted Phone number
	*/
    getDeformattedPhoneNumber : function(phoneNumber){
      return phoneNumber.replace(/\D/g,"");
    },

    /**
	* @api : initActions
	* assigns actions to all the widgets
	* @return : NA
	*/
    initActions: function() {
      var scope = this;
      this.setInfoIconPopupVisibility();
      this.view.btnContinue.setEnabled(false);
      // this.view.txtBoxPayeeDetailField1.onKeyUp = this.setSwiftandPhoneFormats.bind(this);
      this.view.flxAddAddressIcon.onTouchEnd = this.toggleAddressVisibility;
      //this.view.btnCancel.onClick =  this.actionHandler.bind(this,this.context,this._button1);
      this.view.btnCancel.onClick =  this.showCancelPopup;
      this.view.btnContinue.onClick = this.performDataValidation.bind(this);
      this.view.txtBoxReenterAccountNumber.onEndEditing = this.validateAccountNumber;
      this.view.txtBoxNewAccountNumber.onEndEditing = this.validateAccountNumber;
      this.view.fromAccount.onError = this.onError;
      this.view.fromAccount.fetchSelectedRecordDetail = this.onFromAccountSelection;
      this.view.fromAccount.emptyRecordAction = this.onFromAccountEmptyRecord;
      this.view.toAccount.onError = this.onError;
      this.view.toAccount.fetchSelectedRecordDetail = this.onToAccountSelection;
      this.view.toAccount.emptyRecordAction = this.onToAccountEmptyRecord;	
      this.view.fromAccount.getTxtBoxData = this.onEndEditingFromAccount;
      this.view.toAccount.getTxtBoxData = this.onEndEditingToAccount;
      this.view.txtBoxPayeeName.onKeyUp = this.minFillValidation.bind(this,"txtBoxPayeeName");
      this.view.txtBoxNewAccountNumber.onKeyUp = this.minFillValidation.bind(this,"txtBoxNewAccountNumber");
      this.view.txtBoxReenterAccountNumber.onKeyUp = this.minFillValidation.bind(this,"txtBoxReenterAccountNumber");
      this.view.txtBoxPayeeDetailField1.onKeyUp = this.minFillValidation.bind(this,"txtBoxPayeeDetailField1");
      this.view.txtBoxPayeeDetailField2.onKeyUp = this.minFillValidation.bind(this,"txtBoxPayeeDetailField2");
      this.view.txtBoxPayeeDetailField3.onKeyUp = this.minFillValidation.bind(this,"txtBoxPayeeDetailField3")
      this.view.txtBoxTransferAmount.onKeyUp = this.minFillValidation.bind(this,"txtBoxTransferAmount");
      this.setAddressContext();
      scope.view.lblFeePaidType1.doLayout = function(widget){
        scope.view.lblFeePaidType1.info.frame = widget.frame;
      }
      scope.view.lblFeePaidType2.doLayout = function(widget){
        scope.view.lblFeePaidType2.info.frame = widget.frame;
      }
      scope.view.lblToAccount.doLayout = function(widget) {
        scope.view.lblToAccount.info.frame = widget.frame;
      }
    },

    /**
      * @api : setLabelWidth
      * sets width of label dynamically
      * @return : NA
      */
    setLabelWidth: function() {
      var scope = this;
      if(kony.application.getCurrentBreakpoint()!= 640) {
      if(scope.view.lblToAccount.info.frame) {
        lblToAccountWidth = scope.view.lblToAccount.info.frame.width;
        if(scope.view.imgToAccountInfoIcon.isVisible) {
          scope.view.flxToAccountLabelIcon.width = scope.view.imgToAccountInfoIcon.width + scope.view.lblToAccount.info.frame.width + "dp";
        }
        else {
          scope.view.flxToAccountLabelIcon.width = scope.view.lblToAccount.info.frame.width + "dp";
        }
        scope.view.flxPayeeType.left = scope.view.flxToAccountLabelIcon.width;
      }}
    },

    /**
      * @api : onEndEditingFromAccount
      * triggered on setting/reseting data in from dropdown
      * @return : NA
      */
    onEndEditingFromAccount: function(data) {

    },

    /**
      * @api : onEndEditingToAccount
      * triggered on setting/reseting data in to dropdown
      * @return : NA
      */
    onEndEditingToAccount: function(data) {
      var scope = this;
      if(!data) {
        scope.view.txtBoxPayeeDetailField1.text = "";
        scope.existingPayeeData1 = scope.view.txtBoxPayeeDetailField1.text;
        scope.view.txtBoxPayeeDetailField1.setEnabled(true);
        scope.view.txtBoxPayeeDetailField1.skin = this.Skins.TxtBoxField;
        scope.view.txtBoxPayeeDetailField2.text = "";
        scope.existingPayeeData2 = scope.view.txtBoxPayeeDetailField2.text;
        scope.view.txtBoxPayeeDetailField2.setEnabled(true);
        scope.view.txtBoxPayeeDetailField2.skin = this.Skins.TxtBoxField;
        scope.view.txtBoxPayeeDetailField3.text = "";
        scope.existingPayeeData3 = scope.view.txtBoxPayeeDetailField3.text;
        scope.view.txtBoxPayeeDetailField3.setEnabled(true);
        scope.view.txtBoxPayeeDetailField3.skin = this.Skins.TxtBoxField;
        scope.view.txtBoxPayeeDetailField4.text = "";
        scope.existingPayeeData4 = scope.view.txtBoxPayeeDetailField4.text;
        scope.view.txtBoxPayeeDetailField4.setEnabled(true);
        scope.view.txtBoxPayeeDetailField4.skin = this.Skins.TxtBoxField;
        scope.view.address.resetData();
      }
    },

    /**
     * @api : setAddressContext
     * sets the address data from child component to context in parent component
     * @return : NA
     */
    setAddressContext: function() {
      var scope = this;
      this.view.address.setAddressData = function(context) {
        if(context.widgetName == "addressLine01") {
          scope.dataContext["addressLine1"] = context.contextData;
          scope.newPayeeAddressLine01 = context.contextData;
        }
        if(context.widgetName == "addressLine02") {
          scope.dataContext["addressLine2"] = context.contextData;
          scope.newPayeeAddressLine02 = context.contextData;
        }
        if(context.widgetName == "country") {
          scope.dataContext["country"] = context.contextData;
          scope.newPayeeCountry = context.contextData;
        }
        if(context.widgetName == "state") {
          scope.dataContext["state"] = context.contextData;
          scope.newPayeeState = context.contextData;
        }
        if(context.widgetName == "city") {
          scope.dataContext["city"] = context.contextData;
          scope.newPayeeCity = context.contextData;
        }
        if(context.widgetName == "zipCode") {
          scope.dataContext["zipCode"] = context.contextData;
          scope.newPayeeZipCode = context.contextData;
        }
        if(context.widgetName == "phone") {
          scope.dataContext["phoneNumerDisplay"] = context.contextData;
          scope.dataContext["phoneNumer"] = scope.getDeformattedPhoneNumber(context.contextData);
          scope.newPayeePhoneNumber = context.contextData;
        }
        if(context.widgetName == "email") {
          scope.dataContext["emailAddress"] = context.contextData;
          scope.newPayeeEmailAddress = context.contextData;
        }
      };
    },

    /**
         * @api : minFillValidation
         * minimum field level validation to enable continue button.
         * @return : NA
         */
    minFillValidation: function(tbxWidget) {
      var scope = this;
      var object = this.getParsedValue(this._jsonObjName);
      var minFillconfig = this.getParsedValue(this._minFillMapping);
      var maxFillconfig = this.getParsedValue(this._maxFillMapping);
      var transferType = this.getParsedValue(this._transferType);
      var minConfig;
      var maxConfig;
      if (this.transferTypeContext === this.getFieldValue(this._transferFlow, "T1")) {
        minConfig = minFillconfig["Same Bank Transfer"];
        maxConfig = maxFillconfig["Same Bank Transfer"];
      }
      if (this.transferTypeContext === this.getFieldValue(this._transferFlow, "T2")) {
        minConfig = minFillconfig["Domestic Transfer"];
        maxConfig = maxFillconfig["Domestic Transfer"];
      }
      if (this.transferTypeContext === this.getFieldValue(this._transferFlow, "T3")) {
        minConfig = minFillconfig["International Transfer"];
        maxConfig = maxFillconfig["International Transfer"];
      }
      if (this.transferTypeContext === this.getFieldValue(this._transferFlow, "T4")) {
        minConfig = minFillconfig["Pay a Person"];
        maxConfig = maxFillconfig["Pay a Person"];
      }
      if (this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT2")) {
        if (this.transferTypeContext === this.getFieldValue(this._transferFlow, "T4")) {
          for (var key in this.newPayeeP2P) {
            delete minConfig[object][
              [this.newPayeeP2P[key]]
            ];
            delete maxConfig[object][
              [this.newPayeeP2P[key]]
            ];
          }
        } else {
          for (var key in this.newPayee) {
            delete minConfig[object][
              [this.newPayee[key]]
            ];
            delete maxConfig[object][
              [this.newPayee[key]]
            ];
          }
        }
      }
      if (this.accountTypeContext === this.getFieldValue(this._ACCOUNTTYPES, "AT1") || this.accountTypeContext === this.getFieldValue(this._ACCOUNTTYPES, "AT2")) {
        for (var key in this.creditOrLoan) {
          delete minConfig[object][
            [this.creditOrLoan[key]]
          ];
          delete maxConfig[object][
            [this.creditOrLoan[key]]
          ];
        }
      }
      if (tbxWidget == "txtBoxPayeeName" && this.view.flxPayeeName.isVisible) {
        this.updateContext("txtBoxPayeeName", this.view.txtBoxPayeeName.text);
      }
      if (tbxWidget == "txtBoxNewAccountNumber" && this.view.flxNewPayeeAccNumberField.isVisible) {
        this.updateContext("txtBoxNewAccountNumber", this.view.txtBoxNewAccountNumber.text);
      }
      if (tbxWidget == "txtBoxReenterAccountNumber" && this.view.flxNewPayeeReenterAccNumberField.isVisible) {
        this.updateContext("txtBoxReenterAccountNumber", this.view.txtBoxReenterAccountNumber.text);
      }
      if (tbxWidget == "txtBoxPayeeDetailField1" && this.view.flxField1.isVisible) {
        this.updateContext("txtBoxPayeeDetailField1", this.view.txtBoxPayeeDetailField1.text);
        this.setSwiftandPhoneFormats();
      }
      if (tbxWidget == "txtBoxPayeeDetailField2" && this.view.flxField2.isVisible) {
        this.updateContext("txtBoxPayeeDetailField2", this.view.txtBoxPayeeDetailField2.text);
      }
      if (tbxWidget == "txtBoxPayeeDetailField3" && this.view.flxPayeeDetailFieldRow2.isVisible) {
        this.updateContext("txtBoxPayeeDetailField3", this.view.txtBoxPayeeDetailField3.text);
      }
      if (tbxWidget == "txtBoxTransferAmount" && this.view.flxTransferAmountField.isVisible) {
        this.updateContext("txtBoxTransferAmount", this.view.txtBoxTransferAmount.text);
      }
      var dataJson = this.constructDVFInput();
      var tempJson = {};
      for (var key in dataJson) {
        if (dataJson[key]) {
          tempJson[key] = dataJson[key];
        } else {
          tempJson[key] = "";
        }
      }
      if ((this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT1")) && (!this.isNullOrUndefinedOrEmpty(tempJson["txtBoxPayeeDetailField1"])) && (this.isNullOrUndefinedOrEmpty(this.view.txtBoxPayeeDetailField1.text))) {
        tempJson["txtBoxPayeeDetailField1"] = "";
      }
      var mindataValidator = this.dataValidationHandler.validateMinFill(tempJson, object, minConfig);
      var maxdataValidator = this.dataValidationHandler.validateMaxFill(tempJson, object, maxConfig);
      if ((Object.keys(mindataValidator).length === 0 && mindataValidator.constructor === Object) && (Object.keys(maxdataValidator).length === 0 && maxdataValidator.constructor === Object)) {
        if ((this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT2") && !this.isNullOrUndefinedOrEmpty(this.fromAccount) && !this.isNullOrUndefinedOrEmpty(this.toAccount)) || (this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT1") && !this.isNullOrUndefinedOrEmpty(this.fromAccount))) {
          if (!this.isNullOrUndefinedOrEmpty(this.transferCurrency) || !this.isNullOrUndefinedOrEmpty(this.view.lblSelectedTransferCCY.text)) {
            if ((this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT1") && !this.isNullOrUndefinedOrEmpty(this.view.txtBoxPayeeName.text))||(this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT2"))) {
              if (((scope.dueType === "Others Amount") && !this.isNullOrUndefinedOrEmpty(this.view.txtBoxAmountOthers.text)) || ((this.isNullOrUndefinedOrEmpty(scope.dueType)) || (scope.dueType !== "Others Amount"))) {
                //    if(((this.transferTypeContext === this.getFieldValue(this._transferFlow, "T4")) && (!this.isNullOrUndefinedOrEmpty(this.view.txtBoxPayeeDetailField1.text) || !this.isNullOrUndefinedOrEmpty(this.view.txtBoxPayeeDetailField2.text))))
                this.isDVFValidated = "YES";
                this.view.btnContinue.setEnabled(true);
                this.view.btnContinue.skin = this.Skins.EnabledBtnSkin;
                this.view.btnContinue.hoverSkin = this.Skins.EnabledBtnSkin;
                this.view.btnContinue.focusSkin = this.Skins.EnabledBtnSkin;
              } else {
                scope.disableButton();
              }
            } else {
              scope.disableButton();
            }
          } else {
            scope.disableButton();
          }
        } else {
          scope.disableButton();
        }
      } else {
        scope.disableButton();
      }
    },
    /**
      * Component disableButton.
      * disable the button action
      */
    disableButton: function() {
      this.isDVFValidated = "NO";
      this.view.btnContinue.setEnabled(false);
      this.view.btnContinue.skin = this.Skins.DisabledBtnSkin;
      this.view.btnContinue.hoverSkin = this.Skins.DisabledBtnSkin;
      this.view.btnContinue.focusSkin = this.Skins.DisabledBtnSkin;
    },

    /**
      * Component isEmptyNullUndefined.
      * Verifies if the value is empty, null or undefined.
      * data {string} - value to be verified.
      * @return : {boolean} - validity of the value passed.
      */
    isEmptyNullUndefined:function(data){
      if(data === null || data === undefined || data === "")
        return true;
      return false;
    },

    /**
      * Component constructDVFInput.
      * construcs the input for data validation framework
      * @return : JSON
      */
    constructDVFInput: function(){                  
      var txtBoxPayeeName = this.getParsedValue(this._txtInputNewPayeeName)
      if(!this.isEmptyNullUndefined(txtBoxPayeeName) && !this.isEmptyNullUndefined(txtBoxPayeeName.mapping)){    
        txtBoxPayeeName = this.mapTextInputContractToDvfKey(txtBoxPayeeName.mapping); 
      }
      var txtBoxNewAccountNumber = this.getParsedValue(this._txtInputNewPayeeAccountNumber)
      if(!this.isEmptyNullUndefined(txtBoxNewAccountNumber) && !this.isEmptyNullUndefined(txtBoxNewAccountNumber.mapping)){
        txtBoxNewAccountNumber = this.mapTextInputContractToDvfKey(txtBoxNewAccountNumber.mapping);
      }
      var txtBoxReenterAccountNumber = this.getParsedValue(this._txtInputReenterAccountNumber);
      if(!this.isEmptyNullUndefined(txtBoxReenterAccountNumber) && !this.isEmptyNullUndefined(txtBoxReenterAccountNumber.mapping)){
        txtBoxReenterAccountNumber = this.mapTextInputContractToDvfKey(txtBoxReenterAccountNumber.mapping);
      }
      var txtBoxPayeeDetailField1 = this.getParsedValue(this._txtInputPayeeDetail1);
      if(!this.isEmptyNullUndefined(txtBoxPayeeDetailField1) && !this.isEmptyNullUndefined(txtBoxPayeeDetailField1.mapping)){  
        txtBoxPayeeDetailField1 = this.mapTextInputContractToDvfKey(txtBoxPayeeDetailField1.mapping);
      }
      var txtBoxPayeeDetailField2 = this.getParsedValue(this._txtInputPayeeDetail2);
      if(!this.isEmptyNullUndefined(txtBoxPayeeDetailField2) && !this.isEmptyNullUndefined(txtBoxPayeeDetailField2.mapping)){  
        txtBoxPayeeDetailField2 = this.mapTextInputContractToDvfKey(txtBoxPayeeDetailField2.mapping);
      }
      var txtBoxPayeeDetailField3 = this.getParsedValue(this._txtInputPayeeDetail3);
      if(!this.isEmptyNullUndefined(txtBoxPayeeDetailField3) && !this.isEmptyNullUndefined(txtBoxPayeeDetailField3.mapping)){  
        txtBoxPayeeDetailField3 = this.mapTextInputContractToDvfKey(txtBoxPayeeDetailField3.mapping);
      }
      var txtBoxTransferAmount = this.getParsedValue(this._txtInputTransferAmount);
      if(!this.isEmptyNullUndefined(txtBoxTransferAmount) && !this.isEmptyNullUndefined(txtBoxTransferAmount.mapping)){  
        txtBoxTransferAmount = this.mapTextInputContractToDvfKey(txtBoxTransferAmount.mapping);
      }
      var jsonToReturn = {
        "txtBoxPayeeName": this.dataContext[txtBoxPayeeName],
        "txtBoxNewAccountNumber" :this.dataContext[txtBoxNewAccountNumber],
        "txtBoxReenterAccountNumber": this.dataContext[txtBoxReenterAccountNumber],
        "txtBoxPayeeDetailField1": this.dataContext[txtBoxPayeeDetailField1],
        "txtBoxPayeeDetailField2": this.dataContext[txtBoxPayeeDetailField2],
        "txtBoxPayeeDetailField3": this.dataContext[txtBoxPayeeDetailField3],
        "txtBoxTransferAmount" :this.dataContext[txtBoxTransferAmount],
      }      
      if(txtBoxPayeeName == "" || txtBoxPayeeName == null || txtBoxPayeeName == undefined){
        delete jsonToReturn['txtBoxPayeeName'];
      }
      if(txtBoxNewAccountNumber == "" || txtBoxNewAccountNumber == null || txtBoxNewAccountNumber == undefined){
        delete jsonToReturn['txtBoxNewAccountNumber'];
      }
      if(txtBoxReenterAccountNumber == "" || txtBoxReenterAccountNumber == null || txtBoxReenterAccountNumber == undefined){
        delete jsonToReturn['txtBoxReenterAccountNumber'];
      }
      if(txtBoxPayeeDetailField1== "" || txtBoxPayeeDetailField1 == null || txtBoxPayeeDetailField1 == undefined){
        delete jsonToReturn['txtBoxPayeeDetailField1'];
      }       
      if(txtBoxPayeeDetailField2== "" || txtBoxPayeeDetailField2 == null || txtBoxPayeeDetailField2 == undefined){
        delete jsonToReturn['txtBoxPayeeDetailField2'];
      }  
      if(txtBoxPayeeDetailField3== "" || txtBoxPayeeDetailField3 == null || txtBoxPayeeDetailField3 == undefined){
        delete jsonToReturn['txtBoxPayeeDetailField3'];
      }
      if(txtBoxTransferAmount == "" || txtBoxTransferAmount == null || txtBoxTransferAmount == undefined){
        delete jsonToReturn['txtBoxTransferAmount'];
      }
      return jsonToReturn;         
    },

    /**
  * mapTextInputContractToDvfKey
  *
  * retrieves the value required from textbox contracts to compare with dvf response error
  */
    mapTextInputContractToDvfKey: function(tbxJson) {
      var encodedText  = tbxJson;
      if(encodedText !== null && encodedText !== undefined && encodedText !==""){
        requiredText = encodedText.split('{$.c.')[1];
        requiredText = requiredText.split('}')[0];  
        return requiredText;
      }
    },

    /**
      * Component performDataValidation.
      * performs data validation
      * @return : NA
      */
    performDataValidation: function(successOnValidation) {
      var self = this;
      try{
        var dataJson = "";
        var object = this.getParsedValue(this._jsonObjName);
        var fieldMapper = this.getParsedValue(this._dvfConfig);
        var transferType = this.getParsedValue(this._transferType);
        var config;
        if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T1")) {
          config = fieldMapper["Same Bank Transfer"];
        }
        if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T2")) {
          config = fieldMapper["Domestic Transfer"];
        }
        if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T3")) {
          config = fieldMapper["International Transfer"];
        }
        if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T4")) {
          config = fieldMapper["Pay a Person"];
        }
        if(this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT2")) {
          if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T4")) {
            for(var key in this.newPayeeP2P) {
              delete config[object][[this.newPayeeP2P[key]]];
            }} else {
              for(var key in this.newPayee) {
                delete config[object][[this.newPayee[key]]];
              }} }
        if(this.accountTypeContext === this.getFieldValue(this._ACCOUNTTYPES, "AT1") ||this.accountTypeContext === this.getFieldValue(this._ACCOUNTTYPES, "AT2")) {
          for(var key in this.creditOrLoan) {
            delete config[object][[this.creditOrLoan[key]]];
            delete config[object][[this.creditOrLoan[key]]];
          }
        }
        dataJson = this.constructDVFInput();
        var dataValidator = this.dataValidationHandler.validateData(dataJson,object,config);
        if(Object.keys(dataValidator).length === 0 && dataValidator.constructor === Object){
          this.resetErrors();
          if(this.frequency !== "Once" && this.view.flxEndDateField.isVisible === true){
            var formatUtilManager = applicationManager.getFormatUtilManager();
            var sendOnDateComponent = this.view.calStartDate.dateComponents;
            var endOnDateComponent = this.view.calEndDate.dateComponents;
            var sendOnDate = formatUtilManager.getDateObjectFromDateComponents(sendOnDateComponent);
            var endOnDate = formatUtilManager.getDateObjectFromDateComponents(endOnDateComponent);
            if (endOnDate.getTime() === sendOnDate.getTime()) {
              this.view.txtErrormessage.text = kony.i18n.getLocalizedString("i18n.transfers.errors.sameEndDate");
              this.view.flxErrorWarning.setVisibility(true);   
            } else if (endOnDate.getTime() < sendOnDate.getTime()) {
              this.view.txtErrormessage.text = kony.i18n.getLocalizedString("i18n.transfers.errors.beforeEndDate");
              this.view.flxErrorWarning.setVisibility(true);   
            }
            else{
              this.setPayloadData();
              this.callValidateApi();
            }
          }else{
            this.setPayloadData();
            this.callValidateApi();
          }
        }
        else{
          this.showValidationErrors(dataValidator);
        }
      }
      catch(err){
        var errObj = {
          "errorInfo" : "Error in PerformValidation method of the component.",
          "error": err
        };
        //   self.onError(errObj);	
      }

    },

    /**
     * @api : resetErrors
     * resets the error skins and texts
     * @return : NA
     */
    resetErrors: function() {
      this.resetTextBoxesSkins();
      this.resetErrorMessages();
    },


    /**
     * @api : callValidateApi
     * validates the input data provided by the user
     * @return : NA
     */
    callValidateApi: function() {
      var transferType = this.getParsedValue(this._transferType);
      var objSvcName = this.getParsedValue(this._dvObjServiceName);
      var objName = this.getParsedValue(this._dvObjName);
      var operationName = this.getParsedValue(this._dvOpName);
      var criteria = this.getCriteria(this._dvCriteria);
      var opName;
      if(this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES,"PT1" )){
        opName = operationName["New Payee"];
        objSvcName = objSvcName["New Payee"];
        objName = objName["New Payee"];
      }else{
        operationName = operationName["Existing Payee"];
        if(this.toAccount.accountType !== this.getFieldValue(this._ACCOUNTTYPES, "AT1")){
          objSvcName = objSvcName["Existing Payee"];
          objName = objName["Existing Payee"];
        }
        if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T3")) {
          opName = operationName["International Transfer"];
        }
        else if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T1")) {
          if(this.toAccount["isExternalAccount"] === false) {  
            if(this.toAccount.accountType === this.getFieldValue(this._ACCOUNTTYPES, "AT1")){
              opName = operationName["Same Bank Transfer"]["creditCard"];
              objSvcName = objSvcName["creditCard"];
              objName = objName["creditCard"];
            }
            else
              opName = operationName["Same Bank Transfer"]["default"];
          }
          else
            opName = operationName["Same Bank Transfer"]["external"];
        }
        else if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T2")) {
          opName = operationName["Domestic Transfer"];
        }
        else if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T4")) {
          opName = operationName["Pay a Person"];
        }
      }
      this.unifiedTransferDAO.validateData(objSvcName,objName,opName,criteria,this.onValidateSuccess, this.onValidateError);
    },

    /**
     * @api : onValidateSuccess
     * called when validate data api returns success
     * @return : NA
     */
    onValidateSuccess: function(backendResponse, unicode) {
      var scope = this;
      var chargesList = [];
      var serviceCharge = 0;
      scope.docsServiceFormat = "";
      if(backendResponse.dbpErrMsg) {
        scope.view.txtErrormessage.text = backendResponse["dbpErrMsg"];
        scope.view.flxErrorWarning.setVisibility(true);
      }
      else if(backendResponse.errmsg){
        scope.view.txtErrormessage.text = backendResponse["errmsg"];
        scope.view.flxErrorWarning.setVisibility(true);
      }
      if(scope.uploadedAttachments.length >0){
        for (var i = 0; i < scope.uploadedAttachments.length; i++) {
          if(i !== 0) 
            scope.docsServiceFormat = scope.docsServiceFormat+","+scope.uploadedAttachments[i];
          else
            scope.docsServiceFormat = scope.uploadedAttachments[i];
        }
      } 
      scope.dataContext["uploadedattachments"] = scope.docsServiceFormat;
      if (backendResponse.charges) {
        chargesList = JSON.parse(backendResponse.charges);
        var formattedCharge = [];
        for(var i=0;i<chargesList.length ;i++){          
          var chargeLabel =  chargesList[i].chargeName;
          var chargeValue =  scope.getCurrencyCode(chargesList[i].chargeCurrency)+ " "+chargesList[i].chargeAmount;
          formattedCharge.push(chargeLabel,chargeValue);
          serviceCharge = serviceCharge +  chargesList[i].chargeAmount;
        }
        this.dataContext["charges"] = formattedCharge;
        this.dataContext["serviceCharge"] = serviceCharge;
      }else{
        this.dataContext["charges"] = "";
        this.dataContext["serviceCharge"] = "";
      }
      this.dataContext["exchangeRate"] = backendResponse.exchangeRate ? backendResponse.exchangeRate : "";
      this.dataContext["totalAmount"] = backendResponse.totalAmount ? this.getCurrencyCode(this.fromAccount.currencyCode) + " "+ backendResponse.totalAmount : "";      
      if(!backendResponse.dbpErrMsg && !backendResponse.errmsg) {  
        this.dataContext["transactionAmount"] = backendResponse.transactionAmount;
        this.dataContext["referenceId"] = backendResponse.referenceId;
        this.parserUtilsManager.setContext(this.dataContext);
        this.actionHandler(this.dataContext,this._button2);
      }
    },

    /**
     * @api : showValidationErrors
     * displays errors on validation of the fields in unified transfers screen.
     * @return : NA
     */
    showValidationErrors: function(response) {
      this.resetTextBoxesSkins();
      this.invokedvfFieldErrorParser(response);
    },

    /**
     * invokedvfFieldErrorParser
     * @api : invokedvfFieldErrorParser
     * gets invoked when validation fails
     * @return : NA
     */
    invokedvfFieldErrorParser : function(dvfError){
      var txtField;
      var transferType = this.getParsedValue(this._transferType);
      for(var iterator in dvfError){
        if("txtBoxPayeeName" == iterator){
          this.view.txtBoxPayeeName.skin = this.Skins.ErrorField
          txtField = "Payee Name";
        }
        if("txtBoxNewAccountNumber" == iterator){
          this.view.txtBoxNewAccountNumber.skin = this.Skins.ErrorField
          txtField = "Account Number"
        }
        if("txtBoxReenterAccountNumber" == iterator){
          this.view.txtBoxReenterAccountNumber.skin = this.Skins.ErrorField
          txtField = "Account Number"
        }
        if("txtBoxPayeeDetailField1" == iterator){
          this.view.txtBoxPayeeDetailField1.skin = this.Skins.ErrorField
          if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T4")) {
            txtField = "Mobile Number"} else {
              txtField = "Swift/BIC"
            }
        }
        if("txtBoxPayeeDetailField2" == iterator){
          this.view.txtBoxPayeeDetailField2.skin = this.Skins.ErrorField
          txtField = "Email"
        }
        if("txtBoxPayeeDetailField3" == iterator){
          this.view.txtBoxPayeeDetailField3.skin = this.Skins.ErrorField
        } 
        if("txtBoxTransferAmount" == iterator){
          this.view.txtBoxTransferAmount.skin = this.Skins.ErrorField
          txtField = "Amount"
        }
      }  
      var errorTxt = dvfError[iterator];
      errorTxt = errorTxt.replace(iterator, txtField);
      this.view.txtErrormessage.text = errorTxt;
      this.view.flxErrorWarning.setVisibility(true);
    },

    /**
     * onFromAccountSelection
     * @api : onFromAccountSelection
     * triggered on selection of from account
     * @return : NA
     */
    onFromAccountSelection: function(fromAcccountObj) {
      var scope = this;
      scope.resetErrors();
      this.fromAccount = fromAcccountObj;
      if(this.transferTypeContext !== this.getFieldValue(this._transferFlow, "T4")){
           this.transferCurrency = "";
           this.view.lblSelectedTransferCCY.text = "";
      }
      var transferType = this.getParsedValue(this._transferType);
      if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T1")) {
        this.removeSelectedFromAccount();
        this.toAccount = "";
      }
      if(this._currencyListSrc == "From and To Account"){
        this.setTransferCurrencyFieldFromAccounts(true);
        this.view.flxTransferCCYDropdown.setEnabled(true);
        this.view.flxTransferCCYDropdown.skin = this.Skins.EnabledFlex;
      }else if(this._currencyListSrc == "Currency List"){
        if(this._baseCurrency === false) {
        this.view.flxTransferCCYDropdown.setEnabled(true);
        this.view.flxTransferCCYDropdown.skin = this.Skins.EnabledFlex;
        }
      }  
      this.minFillValidation();
    },

    /**
     * onFromAccountEmptyRecord
     * @api : onFromAccountEmptyRecord
     * triggered when data in from account dropdown is empty
     * @return : NA
     */
    onFromAccountEmptyRecord: function(){

    },

    /**
     * removeSelectedFromAccount
     * @api : removeSelectedFromAccount
     * triggered on removal of selected from account
     * @return : NA
     */
    removeSelectedFromAccount: function() {
      var fromAccountId = this.fromAccount.accountID;
      var filteredAccounts = this.toAccountsList.filter(item => item.accountID !== fromAccountId);
      this.view.toAccount.updateList(filteredAccounts);
    },

    /**
     * onToAccountSelection
     * @api : onToAccountSelection
     * triggered on selection of To account
     * @return : NA
     */
    onToAccountSelection: function(toAcccountObj){
      var scope = this;
      scope.resetErrors();
      this.toAccount = toAcccountObj;
      if(this.transferTypeContext !== this.getFieldValue(this._transferFlow, "T4")){
           this.transferCurrency = "";
           this.view.lblSelectedTransferCCY.text = "";
      }
      this.getPayeeDetailFields();
      this.getBeneficiaryAddressDetails();
      this.setRetrievedAddressData();      
      this.resetFrequencyFieldVisibility();
      if(this._currencyListSrc == "From and To Account"){
        if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T1") && !(this.toAccount.currencyCode)){
        	this.updateContext("txtBoxNewAccountNumber",this.toAccount.accountNumber);
        	this.getCurrencyForThirdPartyToAccount();
        } else {
        this.setTransferCurrencyFieldFromAccounts(true);
        }
        this.view.flxTransferCCYDropdown.setEnabled(true);
        this.view.flxTransferCCYDropdown.skin = this.Skins.EnabledFlex;
      }else if(this._currencyListSrc == "Currency List"){
        if(this._baseCurrency === false) {
        this.view.flxTransferCCYDropdown.setEnabled(true);
        this.view.flxTransferCCYDropdown.skin = this.Skins.EnabledFlex;
        }
      }    
      if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T1")) {
        if(this.toAccount.accountType == this.getFieldValue(this._ACCOUNTTYPES, "AT1") || this.toAccount.accountType == this.getFieldValue(this._ACCOUNTTYPES, "AT2"))
        {
          this.accountTypeContext = this.toAccount.accountType;
          this.setLoanCreditCardFieldValue(this.toAccount.accountType);
          this.frequency = "Once";
          this.onClickOutStandingDueRadioButton();
          this.view.txtBoxAmountOthers.setEnabled(false);
        } else {
          this.dueType="";
          this.updateContext("txtBoxAmountOthers", "");
          this.view.txtBoxTransferAmount.text = "";
          this.updateContext("txtBoxTransferAmount", "");
          this.accountTypeContext = "";
          this.view.flxPaymentAmountField.setVisibility(false);
          this.view.flxTransferCurrencyAmount.setVisibility(true);
          this.view.flxFrequencyOptions.setVisibility(true);
          this.view.flxEndDateandRecurrences.setVisibility(false);											
          this.view.flxRecurrencesField.setVisibility(false);
          this.view.flxDueDate.setVisibility(false);
        }
      }else if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T2") || this.transferTypeContext === this.getFieldValue(this._transferFlow, "T3")){
        this.updateContext("txtBoxNewAccountNumber",this.toAccount["accountNumber"]);
        this.validateIBAN();
        this.updateContext("txtBoxNewAccountNumber","");
        if(this.isIBANValid == "YES"){        
          this.IBAN = this.toAccount["accountNumber"];
        }else{
          this.IBAN = "";
        }
      }
      if(this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT2")){
        this.changePayeeDetailFieldsVisibility();
      }


      this.minFillValidation("txtBoxPayeeDetailField1");

    },

    /**
     * resetFrequencyFieldVisibility
     * @api : resetFrequencyFieldVisibility
     * resets the data related to frequency on selection of To account
     * @return : NA
     */
    resetFrequencyFieldVisibility: function() {
      var scope = this;
      this.frequency = "Once";
      var frequencyData = this.view.segFrequencyList.data;
      this.view.lblSelectedFrequency.text = frequencyData[0]["value"]["text"];
      var transferDurationData = this.view.segTransferDurationList.data;
      scope.view.lblSelectedTransferDurationType.text = transferDurationData[0]["lblListValue"];      
      this.view.flxTranferDurationTypeField.setVisibility(false);
      this.view.flxEndDateandRecurrences.setVisibility(false);
	  scope.view.flxEndDateField.setVisibility(false);
      this.view.txtBoxRecurrences.text = "";
    },

    /**
     * getBeneficiaryAddressDetails
     * @api : getBeneficiaryAddressDetails
     * populates address details of corresponding beneficiary account
     * @return : NA
     */
    getBeneficiaryAddressDetails: function() {
      var addressLine1Contract, addressLine2Contract, cityContract, stateContract;
      var countryContract, zipCodeContract, phoneNumberContract, emailAddressContract;
      var addressLine1Mapping, addressLine2Mapping, cityMapping, stateMapping;
      var countryMapping, zipCodeMapping, phoneNumberMapping, emailAddressMapping;

      if(!this.isNullOrUndefinedOrEmpty(this._addressLine1Value)) {
        addressLine1Contract = JSON.parse(this._addressLine1Value);
        if(!this.isNullOrUndefinedOrEmpty(addressLine1Contract.backendMapping)) {
          addressLine1Mapping = this.getParsedValue(addressLine1Contract.backendMapping);
          if(!this.isNullOrUndefinedOrEmpty(this.toAccount[addressLine1Mapping])) {
            this.existingPayeeAddressLine01 = this.toAccount[addressLine1Mapping];
          }
          else
            this.existingPayeeAddressLine01 = "";
        }
      }

      if(!this.isNullOrUndefinedOrEmpty(this._addressLine2Value)) {
        addressLine2Contract = JSON.parse(this._addressLine2Value);
        if(!this.isNullOrUndefinedOrEmpty(addressLine2Contract.backendMapping)) {
          addressLine2Mapping = this.getParsedValue(addressLine2Contract.backendMapping);
          if(!this.isNullOrUndefinedOrEmpty(this.toAccount[addressLine2Mapping])) {
            this.existingPayeeAddressLine02 = this.toAccount[addressLine2Mapping];
          }
          else
            this.existingPayeeAddressLine02 = "";
        }
      }

      if(!this.isNullOrUndefinedOrEmpty(this._cityValue)) {
        cityContract = JSON.parse(this._cityValue);
        if(!this.isNullOrUndefinedOrEmpty(cityContract.backendMapping)) {
          cityMapping = this.getParsedValue(cityContract.backendMapping);
          if(!this.isNullOrUndefinedOrEmpty(this.toAccount[cityMapping])) {
            this.existingPayeeCity = this.toAccount[cityMapping];
          }
          else
            this.existingPayeeCity = "";
        }
      }

      if(!this.isNullOrUndefinedOrEmpty(this._stateValue)) {
        stateContract = JSON.parse(this._stateValue);
        if(!this.isNullOrUndefinedOrEmpty(stateContract.backendMapping)) {
          stateMapping = this.getParsedValue(stateContract.backendMapping);
          if(!this.isNullOrUndefinedOrEmpty(this.toAccount[stateMapping])) {
            this.existingPayeeState = this.toAccount[stateMapping];
          }
          else
            this.existingPayeeState = "";
        }
      }

      if(!this.isNullOrUndefinedOrEmpty(this._countryValue)) {
        countryContract = JSON.parse(this._countryValue);
        if(!this.isNullOrUndefinedOrEmpty(countryContract.backendMapping)) {
          countryMapping = this.getParsedValue(countryContract.backendMapping);
          if(!this.isNullOrUndefinedOrEmpty(this.toAccount[countryMapping])) {
            this.existingPayeeCountry = this.toAccount[countryMapping];
          }
          else
            this.existingPayeeCountry = "";
        }
      }

      if(!this.isNullOrUndefinedOrEmpty(this._zipCodeValue)) {
        zipCodeContract = JSON.parse(this._zipCodeValue);
        if(!this.isNullOrUndefinedOrEmpty(zipCodeContract.backendMapping)) {
          zipCodeMapping = this.getParsedValue(zipCodeContract.backendMapping);
          if(!this.isNullOrUndefinedOrEmpty(this.toAccount[zipCodeMapping])) {
            this.existingPayeeZipCode = this.toAccount[zipCodeMapping];
          }
          else
            this.existingPayeeZipCode = "";
        }
      }

      if(!this.isNullOrUndefinedOrEmpty(this._phoneNumberValue)) {
        phoneNumberContract = JSON.parse(this._phoneNumberValue);
        if(!this.isNullOrUndefinedOrEmpty(phoneNumberContract.backendMapping)) {
          phoneNumberMapping = this.getParsedValue(phoneNumberContract.backendMapping);
          if(!this.isNullOrUndefinedOrEmpty(this.toAccount[phoneNumberMapping])) {
            this.existingPayeePhoneNumber = this.toAccount[phoneNumberMapping];
          }
          else
            this.existingPayeePhoneNumber = "";
        }
      }

      if(!this.isNullOrUndefinedOrEmpty(this._emailAddressValue)) {
        emailAddressContract = JSON.parse(this._emailAddressValue);
        if(!this.isNullOrUndefinedOrEmpty(emailAddressContract.backendMapping)) {
          emailAddressMapping = this.getParsedValue(emailAddressContract.backendMapping);
          if(!this.isNullOrUndefinedOrEmpty(this.toAccount[emailAddressMapping])) {
            this.existingPayeeEmailAddress = this.toAccount[emailAddressMapping];
          }
          else
            this.existingPayeeEmailAddress = "";
        }
      }

      if(this.existingPayeeAddressLine01 ||  this.existingPayeeAddressLine02 || 
         this.existingPayeeCity || this.existingPayeeState || this.existingPayeeCountry || 
         this.existingPayeeZipCode || this.existingPayeePhoneNumber || this.existingPayeeEmailAddress) {
		if(this.transferTypeContext !== this.getFieldValue(this._transferFlow, "T4"))
        { this.view.flxPayeeAddress.isVisible = true; }
      }
      else {
        this.view.flxPayeeAddress.isVisible = false;
      }

    },

    /**
     * setRetrievedAddressData
     * @api : setRetrievedAddressData
     * set address of existing payee in respective fields
     * @return : NA
     */
    setRetrievedAddressData: function() {
      var contextData = { "AddressLine01" : this.existingPayeeAddressLine01,
                         "AddressLine02" : this.existingPayeeAddressLine02,
                         "City" : this.existingPayeeCity,
                         "State" : this.existingPayeeState,
                         "Country" : this.existingPayeeCountry,
                         "ZipCode" : this.existingPayeeZipCode,
                         "PhoneNumber" : this.existingPayeePhoneNumber,
                         "EmailAddress" : this.existingPayeeEmailAddress,
                         "isEnabled" : "no"
                        };
      this.view.address.getAddressData(contextData);
    },
    resetExistingPayeeAddressData : function(){
      this.existingPayeeAddressLine01 = "";
      this.existingPayeeAddressLine02 = "";
      this.existingPayeeCity = "";
      this.existingPayeeState = "";
      this.existingPayeeCountry = "";
      this.existingPayeeZipCode = "";
      this.existingPayeePhoneNumber = "";
      this.existingPayeeEmailAddress = "";
      this.view.address.resetData();
    },
    resetNewPayeeAddressData : function(){
      var scope = this;
      this.newPayeeAddressLine01 = "";
      this.newPayeeAddressLine02 = "";
      this.newPayeeCity = "";
      this.newPayeeState = "";
      this.newPayeeCountry = "";
      this.newPayeeZipCode = "";
      this.newPayeePhoneNumber = "";
      this.newPayeeEmailAddress = "";
      this.view.address.resetData();
    },

    /**
     * setNewPayeeAddressData
     * @api : setNewPayeeAddressData
     * set address of new payee in respective fields
     * @return : NA
     */
    setNewPayeeAddressData: function() {
      var scope = this;
      var contextData = { "AddressLine01" : scope.newPayeeAddressLine01,
                         "AddressLine02" : scope.newPayeeAddressLine02,
                         "City" : scope.newPayeeCity,
                         "State" : scope.newPayeeState,
                         "Country" : scope.newPayeeCountry,
                         "ZipCode" : scope.newPayeeZipCode,
                         "PhoneNumber" : scope.newPayeePhoneNumber,
                         "EmailAddress" : scope.newPayeeEmailAddress,
                         "isEnabled" : "yes"
                        };
      this.view.address.getAddressData(contextData);
    },

    /**
     * onToAccountEmptyRecord
     * @api : onToAccountEmptyRecord
     * invoked if beneficiary account dropdown is empty
     * @return : NA
     */							  
    onToAccountEmptyRecord: function(){
      this.onClickNewPayeeRadioButton();
    },

    /**
     * getFormattedDate
     * @api : getFormattedDate
     * formats the date picked in date picker
     * @return : NA
     */
    getFormattedDate : function(date){
      var inputDate = new Date(date);
      var day = inputDate.getDate() + "";
      var month = (inputDate.getMonth() + 1);
      var year = inputDate.getFullYear() + "";
      if(month<10){
        month = "0" + month;
      }
      var formattedDate = day + "/" + month + "/" + year;
      return formattedDate;
    },

    /**
     * setLoanCreditCardFieldValue
     * @api : setLoanCreditCardFieldValue
     * sets value in loan credit card field
     * @return : NA
     */
    setLoanCreditCardFieldValue: function(accountType) {
      var due1,due2,due3;
      var due1Contract,due2Contract,due3Contract,due4Contract;
      this.transferCurrency = this.toAccount["currencyCode"];			
      var currencySymbol = this.getCurrencyCode(this.toAccount["currencyCode"]);
      due1Contract = JSON.parse(this._paymentAmountOption1);
      due1 = this.getParsedValue(due1Contract["backendMapping"][accountType]);
      due1 = this.toAccount[due1];
      due2Contract = JSON.parse(this._paymentAmountOption2);
      due2 = this.getParsedValue(due2Contract["backendMapping"][accountType]);
      due2 = this.toAccount[due2];
      due3Contract = JSON.parse(this._paymentAmountOption3);
      due3 = this.getParsedValue(due3Contract["backendMapping"][accountType]);
      due3 = this.toAccount[due3];
      due4Contract = JSON.parse(this._paymentAmountOption4);
      this.view.lblPaymentAmount1Value.text =  currencySymbol + this.getFormattedAmount(due1);
      this.view.lblPaymentAmount2Value.text = currencySymbol + this.getFormattedAmount(due2);
      this.view.lblPaymentAmount3Value.text = currencySymbol + this.getFormattedAmount(due3);
      this.view.lblOtherAmountCurrencySymbol.text = currencySymbol;
      if(due1Contract["optionSeleted"] == true){
        this.dueType = due1Contract["optionValue"];
        this.dueAmount = due1;
      }else if(due1Contract["optionSeleted"] == true){
        this.dueType = due2Contract["optionValue"];
        this.dueAmount = due2;
      }else if(due1Contract["optionSeleted"] == true){
        this.dueType = due3Contract["optionValue"];
        this.dueAmount = due3;
      }else if(due1Contract["optionSeleted"] == true){
        this.dueType = due4Contract["optionValue"];
      }
      if(accountType == this.getFieldValue(this._ACCOUNTTYPES, "AT1")){
        if(!this.isNullOrUndefinedOrEmpty(this.toAccount["dueDate"])){
          var dueDate = this.getFormattedDate(this.toAccount["dueDate"]);
          this.view.lblDueDate.text = "(" + "DueDate:" + dueDate + ")";
          this.view.flxEndDateandRecurrences.setVisibility(true);
          this.view.flxEndDateField.setVisibility(false);
          this.view.flxRecurrencesField.setVisibility(false);
          this.view.flxDueDate.setVisibility(true);
        }else{
          this.view.flxEndDateandRecurrences.setVisibility(false);
          this.view.flxRecurrencesField.setVisibility(false);
          this.view.flxDueDate.setVisibility(false);
        }
      }
      else{
        this.dataContext["loanAccountID"] = this.toAccount["accountID"];
        this.parserUtilsManager.setContext(this.dataContext);
        this.getAccountDetails();
      }
      this.view.flxPaymentAmountField.setVisibility(true);
      this.view.flxTransferCurrencyAmount.setVisibility(false);
      this.view.flxFrequencyOptions.setVisibility(false);    
    },

    /**
     * getFormattedAmount
     * @api : getFormattedAmount
     * fetches formatted amount from FormatUtil
     * @return : formatted amount
     */
    getFormattedAmount: function(amount) {
      return this.formatUtil.formatAmount(amount);
    },

    /**
     * getCurrencyCode
     * @api : getCurrencyCode
     * fetches currecy symbol from FormatUtil
     * @return : currecny symbol
     */
    getCurrencyCode: function(currency) {
      return this.formatUtil.getCurrencySymbol(currency);
    },	

    /**
     * getPayeeDetailFields
     * @api : getPayeeDetailFields
     * sets data retrieved from to accounts in payee details flex
     * @return : NA
     */
    getPayeeDetailFields: function() {
      var scope = this;
      var field1Mapping,field2Mapping,field3Mapping,field4Mapping;
      var field1Contract,field2Contract,field3Contract,field4Contract;
      var txtboxDisabledSkin = this.getParsedValue(this._sknTextBoxDisabled);

      if(!this.isNullOrUndefinedOrEmpty(this._txtInputPayeeDetail1))
      {
        field1Contract = JSON.parse(this._txtInputPayeeDetail1);
        this.view.txtBoxPayeeDetailField1.setEnabled(false);
        this.view.txtBoxPayeeDetailField1.skin = txtboxDisabledSkin;
        if(!this.isNullOrUndefinedOrEmpty(field1Contract.backendMapping)) {
          field1Mapping = this.getParsedValue(field1Contract.backendMapping);
          if(!this.isNullOrUndefinedOrEmpty(this.toAccount[field1Mapping])) {
            this.view.txtBoxPayeeDetailField1.text = this.toAccount[field1Mapping];
            if(scope.payeeTypeContext === scope.getFieldValue(scope._PAYEETYPES, "PT2"))
              scope.existingPayeeData1 = scope.view.txtBoxPayeeDetailField1.text;			 
          } else {
            this.view.txtBoxPayeeDetailField1.text = "";
            if(scope.payeeTypeContext === scope.getFieldValue(scope._PAYEETYPES, "PT2"))
              scope.existingPayeeData1 = scope.view.txtBoxPayeeDetailField1.text;
          }
        }
      }

      if(!this.isNullOrUndefinedOrEmpty(this._txtInputPayeeDetail2))
      {
        field2Contract = JSON.parse(this._txtInputPayeeDetail2);
        this.view.txtBoxPayeeDetailField2.setEnabled(false);
        this.view.txtBoxPayeeDetailField2.skin = txtboxDisabledSkin;
        if(!this.isNullOrUndefinedOrEmpty(field2Contract.backendMapping)){
          field2Mapping = this.getParsedValue(field2Contract.backendMapping);
          if(!this.isNullOrUndefinedOrEmpty(this.toAccount[field2Mapping])){
            this.view.txtBoxPayeeDetailField2.text = this.toAccount[field2Mapping];
            if(scope.payeeTypeContext === scope.getFieldValue(scope._PAYEETYPES, "PT2"))
              scope.existingPayeeData2 = scope.view.txtBoxPayeeDetailField2.text;
          } else {
            this.view.txtBoxPayeeDetailField2.text = "";
			if(scope.payeeTypeContext === scope.getFieldValue(scope._PAYEETYPES, "PT2"))
              scope.existingPayeeData2 = scope.view.txtBoxPayeeDetailField2.text;
          }
        }
      }

      if(!this.isNullOrUndefinedOrEmpty(this._txtInputPayeeDetail3))
      {
        field3Contract = JSON.parse(this._txtInputPayeeDetail3);
        this.view.txtBoxPayeeDetailField3.setEnabled(false);
        this.view.txtBoxPayeeDetailField3.skin = txtboxDisabledSkin;
        if(!this.isNullOrUndefinedOrEmpty(field3Contract.backendMapping)){
          field3Mapping = this.getParsedValue(field3Contract.backendMapping);
          if(!this.isNullOrUndefinedOrEmpty(this.toAccount[field3Mapping])){
            this.view.txtBoxPayeeDetailField3.text = this.toAccount[field3Mapping];
            if(scope.payeeTypeContext === scope.getFieldValue(scope._PAYEETYPES, "PT2"))
              scope.existingPayeeData3 = scope.view.txtBoxPayeeDetailField3.text;												
          } else {
            this.view.txtBoxPayeeDetailField3.text = "";
            if(scope.payeeTypeContext === scope.getFieldValue(scope._PAYEETYPES, "PT2"))
              scope.existingPayeeData3 = scope.view.txtBoxPayeeDetailField3.text;
          }
        }
      }

      if(!this.isNullOrUndefinedOrEmpty(this._txtInputPayeeDetail4))
      {
        field4Contract = JSON.parse(this._txtInputPayeeDetail4);
        this.view.txtBoxPayeeDetailField4.setEnabled(false);
        this.view.txtBoxPayeeDetailField4.skin = txtboxDisabledSkin;
        if(!this.isNullOrUndefinedOrEmpty(field4Contract.backendMapping)) {
          field4Mapping = this.getParsedValue(field4Contract.backendMapping);
          if(!this.isNullOrUndefinedOrEmpty(this.toAccount[field4Mapping])) {
            this.view.txtBoxPayeeDetailField4.text = this.toAccount[field4Mapping];
            if(scope.payeeTypeContext === scope.getFieldValue(scope._PAYEETYPES, "PT2"))
              scope.existingPayeeData4 = scope.view.txtBoxPayeeDetailField4.text;												
          } else {
            this.view.txtBoxPayeeDetailField4.text = "";
            if(scope.payeeTypeContext === scope.getFieldValue(scope._PAYEETYPES, "PT2"))
              scope.existingPayeeData4 = scope.view.txtBoxPayeeDetailField4.text;	
          }
        }
      }
    },
    validateIBAN : function(){
      var scope = this;
      var objSvcName = this.getParsedValue(this._IBANObjectServiceName);
      var objName = this.getParsedValue(this._IBANObjectName);
      var operationName = this.getParsedValue(this._IBANOperationName);
      var criteria = this.getCriteria(this._IBANCriteria);
      if(scope.isServiceDone === 0) {
        this.unifiedTransferDAO.validateIBAN
        (objSvcName,objName,operationName,criteria,onSuccess.bind(this),self.onError);
      }
      function onSuccess(response) {
        kony.application.dismissLoadingScreen();
        if(this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT1")) {
          scope.isServiceDone = 1;
        }
        this.isIBANValid = response.isIBANValid;
        if(this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT1")){
          if(this.isIBANValid === "YES"){
            this.getSwiftFromIBAN();
          }

        }
      }
    },
    /**
     * validateAccountNumber
     * @api : validateAccountNumber
     * validates account number
     * @return : NA
     */
    validateAccountNumber: function() {
      var scope = this;
      this.isIBANValid = "";
      this.view.flxErrorWarning.setVisibility(false);
      this.view.txtErrormessage.text = "";
      this.view.txtBoxPayeeDetailField1.text = "";
      this.view.txtBoxPayeeDetailField4.text = ""									  
      var accNumber = this.view.txtBoxNewAccountNumber.text;
      var reenterAccNumber = this.view.txtBoxReenterAccountNumber.text;
      this.updateContext("txtBoxNewAccountNumber",this.view.txtBoxNewAccountNumber.text);
      this.updateContext("txtBoxReenterAccountNumber",this.view.txtBoxReenterAccountNumber.text);
      //  if(this.getParsedValue(this._transferType) !== 'Same Bank Transfer'){
      if(this.transferTypeContext !== this.getFieldValue(this._transferFlow, "T1")) {
        if((accNumber !== "" && accNumber !== undefined)&&(reenterAccNumber !== "" && reenterAccNumber !== undefined )){
          if(accNumber === reenterAccNumber ){
            this.view.txtBoxReenterAccountNumber.skin = this.Skins.TxtBoxField;
            if(this.unifiedTransfersUtility.isValidIBAN(accNumber) || this.unifiedTransfersUtility.isValidAccountNumber(accNumber)){
              if(scope.isServiceDone === 2)
                scope.isServiceDone = 0;
              var existingToAccounts = "";
              existingToAccounts = this.isExistingAccount(accNumber);
              if(this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT1")){
                if(this._currencyListSrc == "Currency List"){
                  this.view.flxTransferCCYDropdown.setEnabled(true);
                  this.view.flxTransferCCYDropdown.skin = this.Skins.EnabledFlex;
                  this.transferCurrency = "";
      			  this.view.lblSelectedTransferCCY.text = "";
                  this.setPaymentMethodFieldContract();
                }   
              }
              if(existingToAccounts === "") {
                this.validateIBAN();
                this.view.txtBoxNewAccountNumber.skin = this.Skins.TxtBoxField;
              } else {
                this.view.flxErrorWarning.setVisibility(true);
                this.view.txtBoxNewAccountNumber.skin = this.Skins.ErrorField;
                this.view.txtErrormessage.text = "This account already exists";
              }
            } else {
              this.isIBANValid = "NO";
              this.view.flxErrorWarning.setVisibility(true);
              this.view.txtBoxNewAccountNumber.skin = this.Skins.ErrorField;
              this.view.txtErrormessage.text = "The IBAN entered is not in the correct format.Check the IBAN and try again.";
            }
          } else {
            scope.isServiceDone = 2;
            this.view.flxErrorWarning.setVisibility(true);
            this.view.txtBoxReenterAccountNumber.skin = this.Skins.ErrorField;
            this.view.txtBoxNewAccountNumber.skin = this.Skins.TxtBoxField;
            this.view.txtErrormessage.text = "Account number does not match";
          }
        }
      }
      //For same bank transfer account number validation   
      else {
        if((accNumber !== "" && accNumber !== undefined)&&(reenterAccNumber !== "" && reenterAccNumber !== undefined )) {
          if(this.unifiedTransfersUtility.isValidAccountNumber(accNumber) &&
             (this.unifiedTransfersUtility.isValidAccountNumber(reenterAccNumber))) {
            if(accNumber === reenterAccNumber) {
              if(scope.isServiceDone === 2)
                scope.isServiceDone = 0;
              var results = "";
              results = this.isExistingAccount(accNumber);          
              if(results === "") {
                var objSvcName = this.getParsedValue(this._getBeneficiaryService);
                var objName = this.getParsedValue(this._getBeneficiaryObject);
                var operationName = this.getParsedValue(this._getBeneficiaryOperation);
                var criteria = this.getCriteria(this._getBeneficiaryCriteria);
                if(scope.isServiceDone === 0) {
                  this.unifiedTransferDAO.getBeneficiaryName
                  (objSvcName,objName,operationName,criteria,onSuccess.bind(this),self.onError);
                }
                function onSuccess(response) {
                  if(this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT1")) {            
                    scope.isServiceDone = 1;
                  }
                  kony.application.dismissLoadingScreen();
                  if(response.beneficiaryName == "") 
                    {                  
                  this.view.flxErrorWarning.setVisibility(true);
                  this.view.txtErrormessage.text = "The account number entered does not match an account on our records. Check the account number and try again.";                  
                    }
                  else
                    {
                      this.view.txtBoxPayeeName.text = response.beneficiaryName;
                      this.view.lblSelectedTransferCCY.text = response.currency;
                      this.transferCurrency = this.view.lblSelectedTransferCCY.text;
                      this.serviceCurrency = this.view.lblSelectedTransferCCY.text;
                      this.setTransferCurrencyFieldFromAccounts();
                      this.minFillValidation("txtBoxPayeeName");
                    }
                    }
              } else {
                this.view.flxErrorWarning.setVisibility(true);
                this.view.txtErrormessage.text = "This account  exists already";
              }
            } else {
              scope.isServiceDone = 2;
              this.view.flxErrorWarning.setVisibility(true);
              this.view.txtErrormessage.text = "Account number does not match";
            }
          }
          else {
            this.view.flxErrorWarning.setVisibility(true);
            this.view.txtErrormessage.text = "Enter a valid Account Format";
          }
        }
      }
      if(this.view.flxErrorWarning.isVisible === true) {
        this.disableButton();
      }
      else {
        this.minFillValidation();
      }
    },
    
    getCurrencyForThirdPartyToAccount: function(){
      var objSvcName = this.getParsedValue(this._getBeneficiaryService);
      var objName = this.getParsedValue(this._getBeneficiaryObject);
      var operationName = this.getParsedValue(this._getBeneficiaryOperation);
      var criteria = this.getCriteria(this._getBeneficiaryCriteria);
       this.unifiedTransferDAO.getBeneficiaryName
        (objSvcName,objName,operationName,criteria,this.onBeneficiaryDetailsSuccess,this.onError);
      
     },
    
    onBeneficiaryDetailsSuccess: function(response){
      if(response.currency){
      this.toAccount.currencyCode = response.currency;
      }
      kony.application.dismissLoadingScreen();
      this.setTransferCurrencyFieldFromAccounts(true);
    },

    /**
     * @api : updateContext
     * updates context.
     * @return : NA
     */
    updateContext: function(key, value) {
      this.dataContext[this.textInputsMapping[key]] = value;
      this.parserUtilsManager.setContext(this.dataContext);
    },
    /**
     * @api : getCriteria
     * Parse the criteria based on accountType.
     * @param : criteria {string} - value collected from exposed contract
     * @return : {JSONObject} - jsonvalue for criteria
     */
    getCriteria: function(criteria) {
      var criteriaJSON = JSON.parse(criteria);
      for(var key in  criteriaJSON){
        var key1="";
        if(key.indexOf("{$") > -1){
          key1=this.parserUtilsManager.getParsedValue(key);
        }
        criteriaJSON[key] = this.parserUtilsManager.getParsedValue(criteriaJSON[key]);
        // criteriaJSON[key1] = criteriaJSON[key];
        if(key.indexOf("{$") > -1){
          delete criteriaJSON[key];
        }
      }
      return criteriaJSON;
    },


    /**
     * setLookupData
     * @api : setLookupData
     * sets swift look up data
     * @return : NA
     */
    setLookupData : function(selectedSwiftData){
      var scope = this;
      scope.view.txtBoxPayeeDetailField1.text = selectedSwiftData[0].bic.text;
      scope.view.txtBoxPayeeDetailField4.text = selectedSwiftData[0].bankName;
      scope.view.txtBoxPayeeDetailField4.setEnabled(false);
      scope.view.txtBoxPayeeDetailField4.skin = this.getParsedValue(this._sknTextBoxDisabled);	
      scope.newPayeeData1 =  scope.view.txtBoxPayeeDetailField1.text;
      scope.newPayeeData2 = scope.view.txtBoxPayeeDetailField2.text ;
      scope.newPayeeData3 = scope.view.txtBoxPayeeDetailField3.text;
      scope.newPayeeData4 = scope.view.txtBoxPayeeDetailField4.text;
      scope.minFillValidation("txtBoxPayeeDetailField1");
    },

    /**
     * setSwiftandPhoneFormats
     * @api : setSwiftandPhoneFormats
     * formats data in swift and phone number field
     * @return : NA
     */
    setSwiftandPhoneFormats: function() {
      var contractJSON = this.getParsedValue(this._txtInputPayeeDetail1);

      var text =  this.view.txtBoxPayeeDetailField1.text;
      var valueEntered = text;

      if(contractJSON.formatType === "Swift") {
        valueEntered = valueEntered.toUpperCase();
        this.view.txtBoxPayeeDetailField1.text = valueEntered;
      }
      else if(contractJSON.formatType === "Phone") {
        valueEntered = valueEntered.replace(/\+1/g, "");
        valueEntered = valueEntered.replace(/\(/g, "");
        valueEntered = valueEntered.replace(/\)/g, "");
        valueEntered = valueEntered.replace(/-/g, "");
        valueEntered = valueEntered.replace(/ /g, "");
        phoneNoFormatted= "+1 " + "(" + valueEntered.slice(0, 3) + ") " + valueEntered.slice(3, 6) + "-" + valueEntered.slice(6, 10);
        if(phoneNoFormatted == "+1 () -" || phoneNoFormatted == "+1 () " || phoneNoFormatted == "+1 "){
          phoneNoFormatted = "";
        }
        this.view.txtBoxPayeeDetailField1.text = phoneNoFormatted;

      }
    },

    /**
         * Component isEmptyNullUndefined
         * Verifies if the value is empty, null or undefined
         * data {string} - value to be verified
         * @return : {boolean} - validity of the value passed
         */
    isEmptyNullUndefined: function(data) {
      if (data === null || data === undefined || data === "")
        return true;
      return false;
    },

    /**
     * @api : showInfoPopup
     * turns on flxInfo
     * @return : NA
     */
    showInfoPopup: function(flxInfo) {
      flxInfo.isVisible = true;
    },

    /**
     * @api : hideInfoPopup
     * turns off flxInfo
     * @return : NA
     */
    hideInfoPopup: function(flxInfo) {
      flxInfo.isVisible = false;
    },

    /**
     * setInfoIconPopupVisibility
     * @api : setInfoIconPopupVisibility
     * sets the visibility of info icon popup
     * @return : NA
     */
    setInfoIconPopupVisibility: function() {
      this.view.imgFromAccountInfoIcon.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxFromAccountInfo);
      this.view.imgToAccountInfoIcon.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxToAccountInfo);
      this.view.imgNewPayeeAccNumberInfoIcon.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxNewAccountInfo);
      this.view.imgNewPayeeReenterAccNumberInfoIcon.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxReenterNewAccountInfo);
      this.view.imgPayeeDetailField1InfoIcon.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxDetailField1Info);
      this.view.imgPayeeDetailField2InfoIcon.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxDetailField2Info);
      this.view.imgPayeeDetailField3InfoIcon.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxDetailField3Info);
      this.view.imgTransferCCYInfoIcon.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxTransferCurrencyInfo);
      this.view.imgTransferAmountInfoIcon.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxTransferAmountInfo);
      this.view.imgFXRateRefInfoIcon.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxFXRateReferenceInfo);
      this.view.imgPaymentMethodInfoIcon.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxPaymentMethodInfo);
      this.view.imgFrequencyInfoIcon.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxFrequencyInfo);
      this.view.imgTransferDurationTypeInfoIcon.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxTransferDurationTypeInfo);
      this.view.imgStartDateInfoIcon.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxStartDateInfo);
      this.view.imgEndDateInfoIcon.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxEndDateInfo);
      this.view.imgRecurrencesInfoIcon.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxRecurrencesInfo);
      this.view.imgFeePaidInfoIcon.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxFeePaidInfo);
      this.view.imgIntermediaryBICinfoIcon.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxIntermBICInfo);
      this.view.imgE2EInfoIcon.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxE2EInfo);
      this.view.imgFieldInfoIcon.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxFieldInfo);
      this.view.imgNotesInfoIcon.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxNotesInfo);
      this.view.imgSupportingDocsInfoIcon.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxSupportingDocumentsInfo);
      this.view.imgPayeeDetailField4InfoIcon.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxDetailField4Info);
      this.view.imgFromAccountInfoClose.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxFromAccountInfo);
      this.view.imgToAccountInfoClose.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxToAccountInfo);
      this.view.imgNewAccountInfoClose.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxNewAccountInfo);
      this.view.imgReenterNewAccountInfoClose.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxReenterNewAccountInfo);
      this.view.imgDetailFieldInfoClose.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxDetailField1Info);
      this.view.imgDetailField2InfoClose.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxDetailField2Info);
      this.view.imgDetailField3InfoClose.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxDetailField3Info);
      this.view.imgPayeeDetailField4InfoIcon.onTouchEnd = this.showInfoPopup.bind(this, this.view.flxDetailField4Info);
      this.view.imgDetailField4InfoClose.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxDetailField4Info);
      this.view.imgTransferCurrencyInfoClose.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxTransferCurrencyInfo);
      this.view.imgTransferAmountInfoClose.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxTransferAmountInfo);
      this.view.imgFXRateRefInfoClose.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxFXRateReferenceInfo);
      this.view.imgPaymentMethodInfoClose.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxPaymentMethodInfo);
      this.view.imgFrequencyInfoClose.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxFrequencyInfo);
      this.view.imgTransferDurationTypeInfoClose.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxTransferDurationTypeInfo);
      this.view.imgStartDateInfoClose.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxStartDateInfo);
      this.view.imgEndDateInfoClose.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxEndDateInfo);
      this.view.imgRecurrenceInfoClose.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxRecurrencesInfo);
      this.view.imgFeePaidInfoClose.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxFeePaidInfo);
      this.view.imgIntermBICInfoClose.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxIntermBICInfo);
      this.view.imgE2EInfoClose.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxE2EInfo);
      this.view.imgFIeldInfoIconClose.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxFieldInfo);
      this.view.imgNoteInfoIconClose.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxNotesInfo);
      this.view.imgSupportingDocumentsInfoClose.onTouchEnd = this.hideInfoPopup.bind(this, this.view.flxSupportingDocumentsInfo);
    },

    /**
     * setFrequency
     * @api : setFrequency
     * set frequency data
     * @return : NA
     */
    setFrequency : function(){
      var frequencyList = [{"Once" : "Once"},{"Daily" : "Daily"},{"Weekly" : "Weekly"},{"BiWeekly" : "Bi-weekly"},
                           {"Monthly" : "Monthly"},{"Quarterly" : "Qtrly"},{"Half Yearly" : "Half Yearly"},{"Yearly" : "Yearly"}];
      var tempFrequencyList = [];
      var freqTypeRemovelist = this.getParsedValue(this._frequencyToRemove);
      var resultFrequency = [];
      if(freqTypeRemovelist ){
        freqTypeRemovelist =  freqTypeRemovelist.split(",");
        resultFrequency = frequencyList.filter( function( el ) {
          return !freqTypeRemovelist.includes( Object.keys(el)[0] );
        });
      }
      else
        resultFrequency = frequencyList;
      this.setDropdownValues(this.view.segFrequencyList,resultFrequency,this.view.lblSelectedFrequency,false);
    },

    /**
     * setDropdownValues
     * @api : setDropdownValues
     * set values in drop down
     * @return : NA
     */
    setDropdownValues : function(seg, listValues, lblSelectedValue, valuesasKey) {
      var segmentData = [];
      var scope = this;
      if(listValues.length != 0){
        seg.widgetDataMap = this.getWidgetDataMap();
        var segmentContractArray = listValues;
        segmentContractArray.forEach(function(object) {
          for(var key in object){
            object["value"]={};
            object["value"]["text"] =  object[key];
            object["value"]["skin"] = scope.Skins.FieldValue;
            object["key"] =  key;
          }
          segmentData.push(object);
        });
        lblSelectedValue.text = segmentContractArray[0]["value"]["text"];
        seg.setData(segmentData);
      }
    },

    /**
     * onFrequencySelection
     * @api : onFrequencySelection
     * formats data on selection of frequency
     * @return : NA
     */
    onFrequencySelection: function(seg,lblSelectedValue,imgDropdownIcon,flxDropdownList) {
      var scope = this;
      imgDropdownIcon.src =  this.Icons.DropdownExpand;
      lblSelectedValue.text = seg.selectedRowItems[0]["value"]["text"];
      flxDropdownList.isVisible = false;
      this.frequency = seg.selectedRowItems[0]["key"];															   
      if(seg.selectedRowItems[0]["key"] === "Once"){
        scope.view.flxStartDateField.setVisibility(true);
        scope.view.flxTranferDurationTypeField.setVisibility(false);
        scope.view.flxEndDateandRecurrences.setVisibility(false);
        scope.view.flxEndDateField.setVisibility(false);
        this.view.lblStartDate.text = this.getLabelText(this._lblDatepicker1);
      }
      else {
        scope.view.flxStartDateField.setVisibility(true);
        scope.view.flxTranferDurationTypeField.setVisibility(true);
        scope.view.flxEndDateandRecurrences.setVisibility(true);
        scope.view.flxEndDateField.setVisibility(true);
        scope.view.flxRecurrencesField.setVisibility(false);
        scope.view.flxDueDate.setVisibility(false);
        this.view.lblStartDate.text = this.getLabelText(this._lblDatepicker2);
        var howLongTransferDuration = [{"On a Specific Date" : "On a Specific Date"},{"Number Of Recurrences" : "Number Of Recurrences"}];
        this.setDropdownValues(this.view.segTransferDurationList,howLongTransferDuration,this.view.lblSelectedTransferDurationType,false);
      }
    },

    /**
     * setDropdownSelectedValue
     * @api : setDropdownSelectedValue
     * sets the value selected in dropdown
     * @return : NA
     */
    setDropdownSelectedValue : function(seg, lblSelectedValue, imgDropdownIcon, flxDropdownList) {
      imgDropdownIcon.src =  this.Icons.DropdownExpand;
      lblSelectedValue.text = seg.selectedRowItems[0]["value"]["text"];
      flxDropdownList.isVisible = false;
    },

    /**
     * onTransferDurationSelection
     * @api : onTransferDurationSelection
     * sets the period of transfer
     * @return : NA
     */
    onTransferDurationSelection: function(seg, lblSelectedValue, imgDropdownIcon, flxDropdownList) {
      imgDropdownIcon.src =  this.Icons.DropdownExpand;
      lblSelectedValue.text = seg.selectedRowItems[0]["value"]["text"];
      flxDropdownList.isVisible = false;
      if(lblSelectedValue.text === "Number Of Recurrences") {
        this.view.lblStartDate.text = this.getLabelText(this._lblDatepicker1);
        this.view.flxEndDateField.setVisibility(false);
        this.view.flxRecurrencesField.setVisibility(true);
      } else {
        this.view.lblStartDate.text = this.getLabelText(this._lblDatepicker2);
        this.view.flxEndDateField.setVisibility(true);
        this.view.flxRecurrencesField.setVisibility(false);
      }
    },

    /**
     * toggleDropdownVisibility
     * @api : toggleDropdownVisibility
     * toggle dropdown visibility
     * @return : NA
     */
    toggleDropdownVisibility: function(flxDropdown, flxDropdwonList, imgDropdownIcon) {
      if(flxDropdwonList.isVisible) {
        flxDropdwonList.isVisible=  false;
        imgDropdownIcon.src = this.Icons.DropdownExpand;
      }
      else
      {
        flxDropdwonList.isVisible = true;
        imgDropdownIcon.src = this.Icons.DropdownCollapse;
      }
    },

    /**
     * setSwiftLookupContext
     * @api : setSwiftLookupContext
     * sets swift lookup context
     * @return : NA
     */
    setSwiftLookupContext: function() {
      var params  = {
        "BREAKPTS" : this._BREAKPTS,
        "swiftLookupDescription" : this._swiftLookupDescription,
        "swiftLookupHeader" : this._swiftLookupHeader,
        "searchField1Label" : this._searchField1Label,
        "searchField1Value" : this._searchField1Value,
        "searchField2Label" : this._searchField2Label,
        "searchField2Value" : this._searchField2Value,
        "searchField3Label" : this._searchField3Label,
        "searchField3Value" : this._searchField3Value,
        "searchField4Label" : this._searchField4Label,
        "searchField4Value" : this._searchField4Value,
        "lblColumn1" : this._lblcolumn1,
        "lblColumn2" : this._lblcolumn2,
        "lblColumn1Value" : this._lblColumn1Value,
        "lblColumn2Value" : this._lblColumn2Value,
        "lblColumn3Value" : this._lblColumn3Value,
        "btnSearch" : this._btnSearch,
        "sknSelectLabel" : "",
        "emptyResponseMessage" : this._emptySearchResult,
        "lookupServiceName" : this._lookupServiceName,
        "lookupObjectName" : this._lookupObjectName,
        "lookupOperationName" : this._lookupOperationName,
        //   "lookupCriteria" : this._lookupCriteria,
        "lookupIdentifier" : this._lookupServiceResponseIdentifier,
        "infoIcon" : this._infoIcon,
        "txtBoxSkn" : this._sknFieldValue,
        "txtBoxMandatorySkn" : "",
        "fieldValueSkn" : this._sknFieldReadOnlyValue,
        "fieldLabelSkn" : this._sknFieldLabel
      };
      return params;
    },

    /**
     * setFromAccountContext
     * @api : setFromAccountContext
     * sets To accounts context
     * @return : NA
     */
    setFromAccountContext: function() {
      var params = {
        "listObject" : this.fromAccountsList,
        "txtBox" : this._fromAccountTextInput,
        "removeTypeList" : this._accountTypesList,
        "listField1" : this._lblFromAccountDropdownListField1,
        "listField2" : this._lblFromAccountDropdownListField2,
        "listField3" : this._lblFromAccountDropdownListField3,													 													 
        "emptyRecordMessage" :  this._lblFromAccountEmptyRecord,
        "emptyRecordActionTxt" :  this._btnFromAccountEmptyRecord,
        "sknTxtBoxValue" : this._sknFieldValue,
        "sknTxtBoxFocus" : this._sknTxtBoxFocus, 
        "sknTxtBoxPlaceholder" : this._sknTextBoxPlaceholder, 
        "sknListFieldType" : this._sknLblDropdownFieldType, 
        "sknListField1" : this._sknACDropdownField1, 
        "sknListField2" : this._sknACDropdownField2, 
        "sknListField3" : this._sknACDropdownField3, 												   
        "sknEmptyRecord" : this._sknACEmptyRecordText, 
        "sknEmptyRecordAction" : this._sknACEmptyRecordActionText, 
        "groupIdentifier" : this._fromAccountGroupIdentifier,
        "iconClearFilter" : this.getParsedImageValue(this._clearIcon),
        "iconDropdownExpand" : this.getParsedImageValue(this._dropdownExpandIcon),
        "iconCityBank" : this.getParsedImageValue(this._cityBankIcon),
        "iconChaseBank": this.getParsedImageValue(this._chaseBankIcon),
        "iconBAOBank" : this.getParsedImageValue(this._BAOBankIcon),
        "iconHDFCBank" :  this.getParsedImageValue(this._HDFCBankIcon) ,
        "iconInfinityBank" : this.getParsedImageValue(this._infinityBankIcon),
        "iconExternalBank" : this.getParsedImageValue(this._externalBankIcon),
        "iconDropdownCollapse" :  this.getParsedImageValue(this._dropdownCollapseIcon)					  
      };
      this.view.fromAccount.setContext(params,this);
    },

    /**
     * setToAccountContext
     * @api : setToAccountContext
     * sets To accounts context
     * @return : NA
     */
    setToAccountContext: function() {
      var params = {
        "listObject" : this.toAccountsList,
        "txtBox" : this._toAccountTextInput,
        "removeTypeList" : this._toAccountTypeList,
        "listField1" : this._lblToAccountDropdownField1,
        "listField2" : this._lblToAccountDropdownField2,
        "listField3" : this._lblToAccountDropdownField3,
        "emptyRecordMessage" : this._lblToAccountEmptyRecord,
        "emptyRecordActionTxt" : this._btnToAccountEmptyRecordAction,
        "groupIdentifier" : this._toAccountGroupIdentifier,
        "iconClearFilter" : this.getParsedImageValue(this._clearIcon),
        "iconDropdownExpand" : this.getParsedImageValue(this._dropdownExpandIcon),
        "iconCityBank" : this.getParsedImageValue(this._cityBankIcon),
        "iconChaseBank": this.getParsedImageValue(this._chaseBankIcon),
        "iconBAOBank" : this.getParsedImageValue(this._BAOBankIcon),
        "iconHDFCBank" :  this.getParsedImageValue(this._HDFCBankIcon) ,
        "iconInfinityBank" : this.getParsedImageValue(this._infinityBankIcon),
        "iconExternalBank" : this.getParsedImageValue(this._externalBankIcon),
        "sknTxtBoxValue" : this._sknFieldValue,
        "sknTxtBoxFocus" : this._sknTxtBoxFocus, 
        "sknTxtBoxPlaceholder" : this._sknTextBoxPlaceholder, 
        "sknListFieldType" : this._sknLblDropdownFieldType, 
        "sknListField1" : this._sknACDropdownField1, 
        "sknListField2" : this._sknACDropdownField2, 
        "sknListField3" : this._sknACToDropdownField3, 												   
        "sknEmptyRecord" : this._sknACEmptyRecordText, 
        "sknEmptyRecordAction" : this._sknACEmptyRecordActionText,"iconDropdownCollapse" :  this.getParsedImageValue(this._dropdownCollapseIcon)																			  
      };
      this.view.toAccount.setContext(params,this);
    },

    /**
     * fetchFromAccounts
     * @api : fetchFromAccounts
     * fetch From accounts
     * @return : NA
     */
    fetchFromAccounts: function() {
      var self = this;
      var objSvcName = this.getParsedValue(this._fromAccountObjectServiceName);
      var objName = this.getParsedValue(this._fromAccountObjectName);
      var operationName = this.getParsedValue(this._fromAccountOperationName);
      var criteria = this.getCriteria(this._fromAccountsCriteria);
      this.unifiedTransferDAO.fetchFromAccounts
      (objSvcName,objName,operationName,criteria,this.processFromAccountsListResponse,self.fetchFromAccountsFailed);
    },
    fetchFromAccountsFailed : function(){
      this.fromAccountsList = [];
      var params = {
        "breakpts" : this._BREAKPTS,
        "listObject" : this.fromAccountsList,
        "txtBox" : this._fromAccountTextInput,
        "removeTypeList" : this._accountTypesList,
        "listField1" : this._lblFromAccountDropdownListField1,
        "listField2" : this._lblFromAccountDropdownListField2,
        "listField3" : this._lblFromAccountDropdownListField3,													 													 
        "emptyRecordMessage" :  this._lblFromAccountEmptyRecord,
        "emptyRecordActionTxt" :  this._btnFromAccountEmptyRecord,
        "sknTxtBoxValue" : this._sknFieldValue,
        "sknTxtBoxFocus" : this._sknTxtBoxFocus, 
        "sknTxtBoxPlaceholder" : this._sknTextBoxPlaceholder, 
        "sknListFieldType" : this._sknLblDropdownFieldType, 
        "sknListField1" : this._sknACDropdownField1, 
        "sknListField2" : this._sknACDropdownField2, 
        "sknListField3" : this._sknACDropdownField3, 												   
        "sknEmptyRecord" : this._sknACEmptyRecordText, 
        "sknEmptyRecordAction" : this._sknACEmptyRecordActionText, 
        "groupIdentifier" : this._fromAccountGroupIdentifier,
        "iconClearFilter" : this.getParsedImageValue(this._clearIcon),
        "iconDropdownExpand" : this.getParsedImageValue(this._dropdownExpandIcon),
        "iconCityBank" : this.getParsedImageValue(this._cityBankIcon),
        "iconChaseBank": this.getParsedImageValue(this._chaseBankIcon),
        "iconBAOBank" : this.getParsedImageValue(this._BAOBankIcon),
        "iconHDFCBank" :  this.getParsedImageValue(this._HDFCBankIcon) ,
        "iconInfinityBank" : this.getParsedImageValue(this._infinityBankIcon),
        "iconExternalBank" : this.getParsedImageValue(this._externalBankIcon),
        "iconDropdownCollapse" :  this.getParsedImageValue(this._dropdownCollapseIcon)					  
      };
      this.view.fromAccount.setContext(params,this);
    },
    /**
     * getAccountDetails
     * @api : getAccountDetails
     * get account details
     * @return : NA
     */
    getAccountDetails : function() {
      var self = this;
      var objSvcName = this.getParsedValue(this._getAccountObjectServiceName);
      var objName = this.getParsedValue(this._getAccountObjectName);
      var operationName = this.getParsedValue(this._getAccountOperationName);
      var criteria = this.getCriteria(this._getAccountCriteria);
      this.unifiedTransferDAO.getAccountDetails
      (objSvcName,objName,operationName,criteria,this.processAccountDetailsListResponse,self.onError);
    },

    /**
     * processFromAccountsListResponse
     * @api : processFromAccountsListResponse
     * fetch From accounts list response
     * @return : NA
     */

    processAccountDetailsListResponse : function(backendResponse){
      var listObj = this.getParsedValue(this._getAccountListObject);
      var accountDetail = backendResponse[listObj];
      if(!this.isNullOrUndefinedOrEmpty(accountDetail[0]["nextPaymentDate"])){
        var dueDate = this.getFormattedDate(accountDetail[0]["nextPaymentDate"]);
        this.view.lblDueDate.text = "(" + "DueDate:" + dueDate + ")";
        this.view.flxEndDateandRecurrences.setVisibility(true);
        this.view.flxEndDateField.setVisibility(false);
        this.view.flxRecurrencesField.setVisibility(false);
        this.view.flxDueDate.setVisibility(true);
      }else{
        this.view.flxEndDateandRecurrences.setVisibility(false);
        this.view.flxRecurrencesField.setVisibility(false);
        this.view.flxDueDate.setVisibility(false);
      }
      kony.application.dismissLoadingScreen();
    }, 

    /**
     * processFromAccountsListResponse
     * @api : processFromAccountsListResponse
     * fetch From accounts list response
     * @return : NA
     */
    processFromAccountsListResponse: function(backendResponse) {
      var transferType = this.getParsedValue(this._transferType);
      var listObj = this.getParsedValue(this._fromAccountListObject);
      this.fromAccountsList = backendResponse[listObj];
	  this.filterFromAccounts();											   
      for(var i=0;i<this.fromAccountsList.length;i++) {
        if(this.fromAccountsList[i]["isBusinessAccount"] == "false"){
          this.fromAccountsList[i]["otherAccounts"] = "Personal Accounts";
        }else {
          this.fromAccountsList[i]["otherAccounts"] = "Buisness Accounts";
        }
        this.fromAccountsList[i]["isExternalAccount"] = false;
        this.fromAccountsList[i]["GroupField"] = "default";									   
      }    
      this.fromAccountAPISuccess = true;
      if(this.fromAccountAPISuccess && this.toAccountAPISuccess && this.crAccountAPISuccess){
        if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T1")) {
          this.mergeInternalExternalAccounts(); 
        } 
        this.setFromAccountContext();
        this.setToAccountContext();
      }
    },

    /**
     * fetchToAccounts
     * @api : fetchToAccounts
     * fetch To accounts
     * @return : NA
     */
    fetchToAccounts: function() {
      var self = this;
      var objSvcName = this.getParsedValue(this._toAccountObjectServiceName);
      var objName = this.getParsedValue(this._toAccountObjectName);
      var operationName = this.getParsedValue(this._toAccountOperationName);
      var criteria = this.getCriteria(this._toAccountscriteria);
      this.unifiedTransferDAO.fetchToAccounts
      (objSvcName,objName,operationName,criteria,this.processToAccountsListResponse,self.fetchToAccountsFailed);
    },

    fetchToAccountsFailed : function(){
      this.toAccountsList = [];
      var params = {
        "breakpts" : this._BREAKPTS,
        "listObject" : this.toAccountsList,
        "txtBox" : this._toAccountTextInput,
        "removeTypeList" : this._toAccountTypeList,
        "listField1" : this._lblToAccountDropdownField1,
        "listField2" : this._lblToAccountDropdownField2,
        "listField3" : this._lblToAccountDropdownField3,
        "emptyRecordMessage" : this._lblToAccountEmptyRecord,
        "emptyRecordActionTxt" : this._btnToAccountEmptyRecordAction,
        "groupIdentifier" : this._toAccountGroupIdentifier,
        "iconClearFilter" : this.getParsedImageValue(this._clearIcon),
        "iconDropdownExpand" : this.getParsedImageValue(this._dropdownExpandIcon),
        "iconCityBank" : this.getParsedImageValue(this._cityBankIcon),
        "iconChaseBank": this.getParsedImageValue(this._chaseBankIcon),
        "iconBAOBank" : this.getParsedImageValue(this._BAOBankIcon),
        "iconHDFCBank" :  this.getParsedImageValue(this._HDFCBankIcon) ,
        "iconInfinityBank" : this.getParsedImageValue(this._infinityBankIcon),
        "iconExternalBank" : this.getParsedImageValue(this._externalBankIcon),
        "sknTxtBoxValue" : this._sknFieldValue,
        "sknTxtBoxFocus" : this._sknTxtBoxFocus, 
        "sknTxtBoxPlaceholder" : this._sknTextBoxPlaceholder, 
        "sknListFieldType" : this._sknLblDropdownFieldType, 
        "sknListField1" : this._sknACDropdownField1, 
        "sknListField2" : this._sknACDropdownField2, 
        "sknListField3" : this._sknACToDropdownField3, 												   
        "sknEmptyRecord" : this._sknACEmptyRecordText, 
        "sknEmptyRecordAction" : this._sknACEmptyRecordActionText,"iconDropdownCollapse" :  this.getParsedImageValue(this._dropdownCollapseIcon)																			  
      };
      this.view.toAccount.setContext(params,this);
    },
    /**
     * fetchCreditCardAccounts
     * @api : fetchCreditCardAccounts
     * fetch credit card accounts
     * @return : NA
     */
    fetchCreditCardAccounts: function(){
      var self = this;
      var objSvcName = this.getParsedValue(this._crAccountObjectServiceName);
      var objName = this.getParsedValue(this._crAccountObjectName);
      var operationName = this.getParsedValue(this._crAccountOperationName);
      var criteria = this.getCriteria(this._crAccountscriteria);
      this.unifiedTransferDAO.getCreditCardAccounts
      (objSvcName,objName,operationName,criteria,this.processCreditCardResponse,self.fetchToAccountsFailed);
    },

    /**
     * processToAccountsListResponse
     * @api : processToAccountsListResponse
     * processes theresponse of To accounts list
     * @return : NA
     */
    processToAccountsListResponse : function(backendResponse){
      var listObj = this.getParsedValue(this._toAccountListObject);
      var transferType = this.getParsedValue(this._transferType);
      this.toAccountsList = backendResponse[listObj];
      if(!(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T4"))){
        this.toAccountsList = this.filterAccounts(backendResponse[listObj],transferType);
      }

      for(var i=0;i<this.toAccountsList.length;i++){
        this.toAccountsList[i]["isExternalAccount"] = true;
        this.toAccountsList[i]["GroupField"] = "default";
      }

      this.toAccountAPISuccess = true;
      if(this.fromAccountAPISuccess && this.toAccountAPISuccess && this.crAccountAPISuccess){
        if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T1")){
          this.mergeInternalExternalAccounts(); 
        } 
        this.setFromAccountContext();
        this.setToAccountContext();
      }
    },

    /**
     * filterAccounts
     * @api : filterAccounts
     * filter accounts based on transfer type
     * @return : NA
     */
    filterAccounts : function(records, transferType) {
      var filteredRecords = [];
      if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T1")) {
        for(var i=0;i<records.length;i++){
          if(records[i]["isSameBankAccount"] == "true") {
            filteredRecords.push(records[i]);
          }
        }
      }
      else if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T2")) {
        for(var i=0;i<records.length;i++){
          if(records[i]["isSameBankAccount"] == "false" && records[i]["isInternationalAccount"] == "false"){
            filteredRecords.push(records[i]);
          }
        }
      }
      else if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T3")) {
        for(var i=0;i<records.length;i++) {
          if(records[i]["isInternationalAccount"] == "true" && records[i]["isSameBankAccount"] == "false"){
            filteredRecords.push(records[i]);
          }
        }
      }
      return filteredRecords;
    },

    /**
     * processCreditCardResponse
     * @api : processCreditCardResponse
     * processes credit card response data
     * @return : NA
     */
    processCreditCardResponse : function(backendResponse){
      var listObj = this.getParsedValue(this._crAccountListObject);
      this.creditCardAccounts = backendResponse[listObj];
      this.crAccountAPISuccess = true;
      for(var i=0;i<this.creditCardAccounts.length;i++){
        this.creditCardAccounts[i]["isBusinessAccount"] = "false";
        this.creditCardAccounts[i]["otherAccounts"] = "Personal Accounts";
        this.creditCardAccounts[i]["isExternalAccount"] = false;
        this.creditCardAccounts[i]["GroupField"] = "default";
      }
      if(this.fromAccountAPISuccess && this.toAccountAPISuccess && this.crAccountAPISuccess){
        if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T1")){
          this.mergeInternalExternalAccounts(); 
        } 
        this.setFromAccountContext();
        this.setToAccountContext();
      }
    },
    /**
     * mergeInternalExternalAccounts
     * @api : mergeInternalExternalAccounts
     * merges internal and external accounts
     * @return : NA
     */
    mergeInternalExternalAccounts : function(backendResponse) {
      var toAccountsList = [];
      var creditCardAccounts = this.creditCardAccounts;
      toAccountsList = this.fromAccountsList.concat(creditCardAccounts);
      toAccountsList = toAccountsList.concat(this.toAccountsList);	
      this.toAccountsList = toAccountsList;
    },

    /**
     * Component readObject
     * Helper method to parse the backend response
     * obj{JSONArray} - object containing any value
     * jsonPath{String} - jsonPath traversed till the search field is reachable
     */
    readObject: function(obj, jsonPath) {
      var self = this;
      try
      {
        var keysItr = Object.keys(obj);
        var parentPath = jsonPath;
        for (var i = 0; i < keysItr.length; i++) {
          var key = keysItr[i];
          var value = obj[key]
          if(parentPath)
            jsonPath = parentPath + "." + key;
          else
            jsonPath = key;
          if (value instanceof Array) {
            this.map[key] = value;
            this.readArray(value, jsonPath);
          } else if (value instanceof Object) {
            this.map[key] = value;
            this.readObject(value, jsonPath);
          } else { // is a value
            if(isNaN(value) && (value.indexOf("{")>-1 ||value.indexOf("[")>-1))
              value=eval('('+value+')');
            if (value instanceof Array) {
              this.readArray(value, jsonPath);
            } else if (value instanceof Object) {
              this.readObject(value, jsonPath);
            }else{
              this.map[jsonPath] = value;
            }
          }
        }
      }
      catch(err)
      {
        var errorObj =
            {
              "errorInfo" : "Error in reading the Object.",
              "errorLevel" : "Business",
              "error": err
            };
        self.onError(errorObj);
      }
    },

    /**
     * setIcons
     * @api : setIcons
     * sets images configured in contracts
     * @return : NA
     */
    setIcons: function() {
      this.view.imgAddAddressIcon.src = this.Icons.AddAddress;
      this.view.imgAttachmentIcon.src = this.Icons.AttachDocuments;
      this.view.imgDetailField2InfoClose.src = this.Icons.InfoClose;
      this.view.imgDetailField3InfoClose.src = this.Icons.InfoClose;
      this.view.imgDetailFieldInfoClose.src = this.Icons.InfoClose;
      this.view.imgDetailField4InfoClose.src = this.Icons.InfoClose;
      this.view.imgE2EInfoClose.src = this.Icons.InfoClose;
      this.view.imgE2EInfoIcon.src = this.Icons.InfoIcon;
      this.view.imgEndDateInfoClose.src = this.Icons.InfoClose;
      this.view.imgEndDateInfoIcon.src = this.Icons.InfoIcon;
      this.view.imgFeePaidInfoClose.src = this.Icons.InfoClose;
      this.view.imgFeePaidInfoIcon.src = this.Icons.InfoIcon;
      this.view.imgFieldInfoIcon.src = this.Icons.InfoIcon;
      this.view.imgFIeldInfoIconClose.src = this.Icons.InfoClose;
      this.view.imgFrequencyDropdownIcon.src = this.Icons.DropdownExpand;
      this.view.imgFrequencyInfoClose.src = this.Icons.InfoClose;
      this.view.imgFrequencyInfoIcon.src = this.Icons.InfoIcon;
      this.view.imgFromAccountInfoClose.src = this.Icons.InfoClose;
      this.view.imgFromAccountInfoIcon.src = this.Icons.InfoIcon;
      this.view.imgFXRateRefInfoClose.src = this.Icons.InfoClose;
      this.view.imgFXRateRefInfoIcon.src = this.Icons.InfoIcon;
      this.view.imgIntermBICInfoClose.src = this.Icons.InfoClose;
      this.view.imgIntermediaryBICinfoIcon.src = this.Icons.InfoIcon;
      this.view.imgNewAccountInfoClose.src = this.Icons.InfoClose;
      this.view.imgNewPayeeAccNumberInfoIcon.src = this.Icons.InfoIcon;
      this.view.imgNewPayeeReenterAccNumberInfoIcon.src = this.Icons.InfoIcon;
      this.view.imgReenterNewAccountInfoClose.src = this.Icons.InfoClose;
      this.view.imgNoteInfoIconClose.src = this.Icons.InfoClose;
      this.view.imgNotesInfoIcon.src = this.Icons.InfoIcon;
      this.view.imgPayeeDetailField1InfoIcon.src = this.Icons.InfoIcon;
      this.view.imgPayeeDetailField2InfoIcon.src = this.Icons.InfoIcon;
      this.view.imgPayeeDetailField3InfoIcon.src = this.Icons.InfoIcon;
      this.view.imgPayeeDetailField4InfoIcon.src = this.Icons.InfoIcon;
      this.view.imgSupportingDocsInfoIcon.src = this.Icons.InfoIcon;
      this.view.imgPayeeDetailFieldInfo.src = this.Icons.PayeeInfoIcon;
      this.view.imgPaymentMethodInfoClose.src= this.Icons.InfoClose;
      this.view.imgPaymentMethodInfoIcon.src = this.Icons.InfoIcon;
      this.view.imgRecurrenceInfoClose.src = this.Icons.InfoClose;
      this.view.imgRecurrencesInfoIcon.src = this.Icons.InfoIcon ;
      this.view.imgStartDateInfoClose.src = this.Icons.InfoClose;
      this.view.imgStartDateInfoIcon.src = this.Icons.InfoIcon;
      this.view.imgToAccountInfoClose.src = this.Icons.InfoClose;
      this.view.imgToAccountInfoIcon.src = this.Icons.InfoIcon;
      this.view.imgTransferAmountInfoClose.src = this.Icons.InfoClose;
      this.view.imgTransferAmountInfoIcon.src = this.Icons.InfoIcon;
      this.view.imgTransferCCYDropdownIcon.src = this.Icons.DropdownExpand;
      this.view.imgTransferCCYInfoIcon.src = this.Icons.InfoIcon;
      this.view.imgTransferCurrencyInfoClose.src = this.Icons.InfoClose;
      this.view.imgTransferDurationTypeInfoClose.src = this.Icons.InfoClose;
      this.view.imgSupportingDocumentsInfoClose.src = this.Icons.InfoClose;
      this.view.imgTransferDurationTypeDropdownIcon.src = this.Icons.DropdownExpand;
      this.view.imgTransferDurationTypeInfoIcon.src = this.Icons.InfoIcon;
      this.view.imgAttachmentUploadError.src = this.Icons.UploadDocumentErrorIcon;
    },

    /**
     * setFieldContracts
     * @api : setFieldContracts
     * maps the text box with the corresponding contracts configured
     * @return : NA
     */
    setFieldContracts: function() {
      var scope = this;
      this.setTxtBoxPropertiesFromContract("txtBoxTransferAmount",this._txtInputTransferAmount,"flxTransferAmountField");
      this.setTxtBoxPropertiesFromContract("txtBoxFXRateReference", this._txtInputfxRateReference,"flxFXRateReferenceExchangeRate");
      this.setTxtBoxPropertiesFromContract("txtBoxRecurrences", this._txtInputRecurrences,"flxRecurrencesField");
      this.setTxtBoxPropertiesFromContract("txtBoxE2E", this._txtInputE2E,"flxE2EField");
      this.setTxtBoxPropertiesFromContract("txtBoxIntermediaryBIC", this._txtInputIntermediaryBIC,"flxIntermediaryBICField");
      this.setTxtBoxPropertiesFromContract("txtBoxField", this._textInputValue,"txtBoxField");
      this.setTxtBoxPropertiesFromContract("txtBoxPayeeDetailField1",this._txtInputPayeeDetail1, "flxField1");
      this.setTxtBoxPropertiesFromContract("txtBoxPayeeDetailField2",this._txtInputPayeeDetail2, "flxField2");
      this.setTxtBoxPropertiesFromContract("txtBoxPayeeDetailField3", this._txtInputPayeeDetail3, "flxField3");
      this.setTxtBoxPropertiesFromContract("txtBoxPayeeDetailField4", this._txtInputPayeeDetail4, "flxField4");						   
      this.setTxtBoxPropertiesFromContract("txtAreaNotes",this._txtInputNotes,"flxNotesField");
      this.setTxtBoxPropertiesFromContract("txtBoxPayeeName", this._txtInputNewPayeeName,"flxPayeeName");
      this.setTxtBoxPropertiesFromContract("txtBoxNewAccountNumber", this._txtInputNewPayeeAccountNumber,"flxNewPayeeAccNumberField");
      this.setTxtBoxPropertiesFromContract("txtBoxReenterAccountNumber",this._txtInputReenterAccountNumber,"flxNewPayeeReenterAccNumberField");  
      this.setTxtBoxPropertiesFromContract("txtBoxAmountOthers",this._otherAmountTxtBox,"flxPaymentAmount4")																									

      this.setPayeeTypeFieldContract();
      this.setPayeeAmountFieldContract();
      this.setPaymentMethodFieldContract();
      this.setFeePaidFieldContract();
      //  this.setButtonTexts();
      this.setIntermediaryandE2EFlex();
      this.setPayeeDetailsFlex();
      if(this.context.flowType != "edit"){
        this.view.lblSelectedTransferCCY.text = "";
        if(this._currencyListSrc == "None"){
          this.view.flxTransferCurrencyField.setVisibility(false);
        } else if(this._currencyListSrc == "Currency List") {
          this.setTransferCurrencyFieldData();
          if(this._baseCurrency === true) {
            var configManager = applicationManager.getConfigurationManager();
            var baseCurrency;
            if(configManager.getDeploymentGeography() === "EUROPE") {
              baseCurrency =  "EUR";
              this.view.lblSelectedTransferCCY.text = baseCurrency;
            } else {
              baseCurrency =  "USD";
              this.view.lblSelectedTransferCCY.text = baseCurrency;
            }
            scope.transferCurrency = baseCurrency;
            this.view.flxTransferCCYDropdown.setEnabled(false);
            this.view.flxTransferCCYDropdown.skin = this.Skins.DisabledFlex;
          }
        }else{
          this.view.flxTransferCCYDropdown.setEnabled(false);
          this.view.flxTransferCCYDropdown.skin = this.Skins.DisabledFlex;
        }
      }
      this.parserUtilsManager.setContext(this.dataContext);
      //  this.setSegmentData("segTransferCCYList",this._dropdownTransferCurrency,"flxTransferCurrencyField","lblSelectedTransferCCY");
      //   this.setSegmentData("segFrequencyList",this._dropdownFrequency,"flxFrequencyField", "lblSelectedFrequency");
      //     this.setSegmentData("segTransferDurationList",this._dropdownFrequencyType,"flxTransferDurationTypeField", "lblSelectedTransferDurationType");
    },

    /**
     * setTransferCurrencyFieldFromAccounts
     * @api : setTransferCurrencyFieldFromAccounts
     * sets transfer currency from accounts
     * @return : NA
     */
    setTransferCurrencyFieldFromAccounts : function(includeToAccounts){
      var currencyList;  
      var scope = this;
      this.AccountsCurrencyList = [];
      var fromAccountCCY = this.isNullOrUndefinedOrEmpty(this.fromAccount.currencyCode) ? "" : this.fromAccount.currencyCode;
      var toAccountCCY = this.isNullOrUndefinedOrEmpty(this.toAccount.currencyCode) ? "" : this.toAccount.currencyCode;

      if(fromAccountCCY != toAccountCCY){
        if(fromAccountCCY != ""){
          this.AccountsCurrencyList.push({"from" : fromAccountCCY});
        }
        if(toAccountCCY != ""  && includeToAccounts){
          this.AccountsCurrencyList.push({"to" : toAccountCCY});
        }
        if((!scope.isNullOrUndefinedOrEmpty(scope.serviceCurrency)) && (fromAccountCCY != scope.serviceCurrency)) {
          this.AccountsCurrencyList.push({
            "to": scope.serviceCurrency
          });
        }
      }else{
        if(fromAccountCCY != ""){
          this.AccountsCurrencyList.push({"from" : fromAccountCCY});
      }

      }

      currencyList = this.AccountsCurrencyList;
      this.setTransferSegmentData(this.view.segTransferCCYList,currencyList,this.view.lblSelectedTransferCCY,true);
    },

    /**
     * setTransferCurrencyFieldData
     * @api : setTransferCurrencyFieldData
     * sets data in transfer currency field
     * @return : NA
     */
    setTransferCurrencyFieldData : function() {
      var currencyList;
      this.AccountsCurrencyList = [];
      if(this._currencyListSrc == "Currency List"){
        currencyList = JSON.parse(this._currencyList);
        this.setTransferSegmentData(this.view.segTransferCCYList,currencyList,this.view.lblSelectedTransferCCY,false);
      }
    },

    /**
     * setTransferSegmentData
     * @api : setTransferSegmentData
     * sets transfer data in segment
     * @return : NA
     */
    setTransferSegmentData : function(seg, listValues, lblSelectedValue, valuesasKey){
      var scope = this;
      var segmentData = [];
      if(listValues.length != 0){
        seg.widgetDataMap = this.getWidgetDataMap();
        var segmentContractArray = listValues;
        segmentContractArray.forEach(function(object){
          for(var key in object){
            object["value"]={};
            var currSymbol = scope.formatUtil.getCurrencySymbol(object[key]);
            object["value"]["text"] = currSymbol + " " + object[key];
            object["value"]["skin"] = scope.Skins.DropdownTxt;
            object["value"]["symbol"] = currSymbol;
            object["key"] = (valuesasKey == true) ? object[key] : key;
          } 
          segmentData.push(object);
        });
        seg.setData(segmentData);
        if(segmentData.length == 1) {
          this.view.flxTransferCCYList.height = "42dp";
        } else if(segmentData.length > 1) {
          this.view.flxTransferCCYList.height = "82dp";
        }													  
      }
    },

    /**
     * showCancelPopup
     * @api : showCancelPopup
     * displays popup on click of cancel button
     * @return : NA
     */
    showCancelPopup :function(){
      var scope = this;
      var form = kony.application.getCurrentForm();
      var popupObj = this.view.flxPopup.clone("prefix");
      form.add(popupObj); 
      popupObj.isVisible = true;  
      //  popupObj.bottom = "0dp"; 
      popupObj.left = "0dp";  
      popupObj.height = "100%"; 
      popupObj.prefixflxClosePopup.centerY = "50%"; 
      popupObj.prefixflxClosePopup.prefixbtnCancelNo.onClick = function() { 
        form.remove(popupObj);  
      };
      popupObj.prefixflxClosePopup.prefixflxClose.onClick = function() {  
        form.remove(popupObj);  
      };
      popupObj.prefixflxClosePopup.prefixbtnCancelYes.onClick = function() {        
        form.remove(popupObj);
        scope.onCancelTransfer();						 
      };
      this.view.forceLayout();
    },

    /**
     * setFieldInfoTexts
     * @api : setFieldInfoTexts
     * sets data in info popup
     * @return : NA
     */
    setFieldInfoTexts: function() {
      this.setInfoTexts(this.view.lblFromAcccountInfoHeader,this.view.lblFromAccountInfoText,this._fromAccountTextInput,this.view.imgFromAccountInfoIcon);
      this.setInfoTexts(this.view.lblToAcccountInfoHeader,this.view.lblToAccountInfoText,this._toAccountTextInput,this.view.imgToAccountInfoIcon);
      this.setInfoTexts(this.view.lblNewAcccountInfoHeader, this.view.lblNewAccountInfoText, this._txtInputNewPayeeAccountNumber, this.view.imgNewPayeeAccNumberInfoIcon);
      this.setInfoTexts(this.view.lblReenterNewAcccountInfoHeader, this.view.lblReenterNewAccountInfoText, this._txtInputReenterAccountNumber, this.view.imgNewPayeeReenterAccNumberInfoIcon);
      this.setInfoTexts(this.view.lblDetailField1InfoHeader,this.view.lblDetailField1InfoText,this._txtInputPayeeDetail1,this.view.imgPayeeDetailField1InfoIcon);
      this.setInfoTexts(this.view.lblDetailField2InfoHeader,this.view.lblDetailField2InfoText,this._txtInputPayeeDetail2,this.view.imgPayeeDetailField2InfoIcon);
      this.setInfoTexts(this.view.lblDetailField3InfoHeader,this.view.lblDetailField3Info,this._txtInputPayeeDetail3,this.view.imgPayeeDetailField3InfoIcon);
      this.setInfoTexts(this.view.lblDetailField4InfoHeader,this.view.lblDetailField4InfoTxt,this._txtInputPayeeDetail4,this.view.imgPayeeDetailField4InfoIcon);
      this.setInfoTexts(this.view.lblTransferCurrencyInfoHeader,this.view.lblTransferCurrencyInfoText,this._dropdownTransferCurrency,this.view.imgTransferCCYInfoIcon);
      this.setInfoTexts(this.view.lblTransferAmountInfoHeader,this.view.lblTransferAmountInfoText,this._txtInputTransferAmount,this.view.imgTransferAmountInfoIcon);

      this.setInfoTexts(this.view.lblFXRateRefInfoHeader,this.view.lblFXRateReferenceInfoText,this._txtInputfxRateReference,this.view.imgFXRateRefInfoIcon);
      this.setInfoTexts(this.view.lblFrequencyInfoHeader,this.view.lblFrequencyInfoText,this._dropdownFrequency,this.view.imgFrequencyInfoIcon);
      this.setInfoTexts(this.view.lblTransferDurationTypeInfoHeader,this.view.lblTransferDurationTypeInfoText,this._dropdownFrequencyType,this.view.imgTransferDurationTypeInfoIcon);
      this.setInfoTexts(this.view.lblStartDateInfoHeader,this.view.lblStartDateInfoText,this._datepicker1Value,this.view.imgStartDateInfoIcon);
      this.setInfoTexts(this.view.lblEndDateInfoHeader,this.view.lblEndDateInfoText,this._datepicker2Value,this.view.imgEndDateInfoIcon);
      this.setInfoTexts(this.view.lblIntermBICInfoHeader,this.view.lblIntermBICInfoText,this._txtInputIntermediaryBIC,this.view.imgIntermediaryBICinfoIcon);
      this.setInfoTexts(this.view.lblRecurrencesInfoHeader,this.view.lblRecurrenceInfoText,this._txtInputRecurrences,this.view.imgRecurrencesInfoIcon);
      this.setInfoTexts(this.view.lblE2EInfoHeader,this.view.lblE2EInfoText,this._txtInputE2E,this.view.imgE2EInfoIcon);
      this.setInfoTexts(this.view.lblNoteInfoHeader,this.view.lblNotesInfoTxt,this._txtInputNotes,this.view.imgNotesInfoIcon);
      this.setInfoTexts(this.view.lblSupportingDocumentsInfoHeader,this.view.lblSupportingDocumentsInfoTxt,this._lblSupportingDocuments,this.view.imgSupportingDocsInfoIcon);
      //Payment Method info text
      this.setInfoTexts(this.view.lblPaymentMethodInfoHeade,this.view.lblOption1InfoTxt,this._paymentMethodOption1,this.view.imgPaymentMethodInfoIcon);
      this.setInfoTexts("",this.view.lblOption2Info,this._paymentMethodOption2,"");
      this.setInfoTexts("",this.view.lblOption3Info,this._paymentMethodOption3,"");
      this.setInfoTexts("",this.view.lblOption4Info,this._paymentMethodOption4,"");
      //Fees Paid info text
      this.setInfoTexts(this.view.lblFeePaidInfoHeader,this.view.lblFeeOption1Info,this._feesPaidOption1,this.view.imgFeePaidInfoIcon);
      this.setInfoTexts("",this.view.lblFeeOption2Info,this._feesPaidOption2,"");
      this.setInfoTexts("",this.view.lblFeeOption3Info,this._feesPaidOption3,""); 
      // this.setInfoTexts(this.view.lblFieldInfoHeader,this.view.lblFieldInfoTxt,this._txtInputNotes,this.view.imgFieldInfoIcon);

    },

    /**
	* setSkins
	* @api : setSkins
	* event called to set the skins based on the contracts.
	* @return : NA
	*/
    setSkins: function() {
      var self = this;
      try
      {
        //Label Skins
        this.view.lblFromAccount.skin = this.Skins.FieldLabel;
        this.view.lblAttachDocuments.skin = this.Skins.FieldLabelBold;
        this.view.lblAttachDocumentsOptional.skin = this.Skins.FieldLabel;
        this.view.lblDetailField1InfoHeader.skin = this.Skins.InfoHeader;
        this.view.lblDetailField1InfoText.skin = this.Skins.InfoDesc;
        this.view.lblDetailField2InfoHeader.skin = this.Skins.InfoHeader;
        this.view.lblDetailField2InfoText.skin = this.Skins.InfoDesc;
        this.view.lblDetailField3Info.skin = this.Skins.InfoDesc;
        this.view.lblDetailField3InfoHeader.skin = this.Skins.InfoHeader;
        this.view.lblDueDate.skin = this.Skins.FieldLabel;
        this.view.lblE2E.skin = this.Skins.FieldLabel;
        this.view.lblE2EInfoHeader.skin = this.Skins.InfoHeader;
        this.view.lblE2EInfoText.skin = this.Skins.FieldLabel;
        this.view.lblEndDate.skin = this.Skins.FieldLabel;
        this.view.lblEndDateInfoHeader.skin = this.Skins.InfoHeader;
        this.view.lblEndDateInfoText.skin = this.Skins.InfoDesc;
        this.view.lblExchangeRate.skin = this.Skins.FieldLabel;
        this.view.lblFeeOption1Info.skin = this.Skins.InfoDesc;
        this.view.lblFeeOption2Info.skin = this.Skins.InfoDesc;
        this.view.lblFeeOption3Info.skin = this.Skins.InfoDesc;
        this.view.lblFeePaid.skin = this.Skins.FieldLabel;
        this.view.lblFeePaidInfoHeader.skin =this.Skins.InfoHeader;
        this.view.lblFeePaidType1.skin = this.Skins.RadioInputTxt;
        this.view.lblFeePaidType2.skin = this.Skins.RadioInputTxt;
        this.view.lblFeePaidType3.skin = this.Skins.RadioInputTxt;
        this.view.lblField.skin = this.Skins.FieldLabel;
        this.view.lblFieldInfoHeader.skin = this.Skins.FieldLabel;
        this.view.lblFieldInfoTxt.skin = this.Skins.InfoDesc;
        this.view.lblFieldValue.skin = this.Skins.FieldLabel;
        this.view.lblFrequency.skin = this.Skins.FieldLabel;
        this.view.lblFrequencyInfoHeader.skin = this.Skins.InfoHeader;
        this.view.lblFrequencyInfoText.skin = this.Skins.InfoDesc;
        this.view.lblFromAcccountInfoHeader.skin = this.Skins.FieldLabel;
        this.view.lblFromAccountInfoText.skin = this.Skins.InfoDesc;
        this.view.lblFXRateReference.skin = this.Skins.FieldLabel;
        this.view.lblFXRateReferenceInfoText.skin = this.Skins.InfoDesc;
        this.view.lblFXRateRefInfoHeader.skin = this.Skins.InfoHeader;
        this.view.lblIntermBICInfoHeader.skin = this.Skins.InfoHeader;
        this.view.lblIntermBICInfoText.skin = this.Skins.InfoDesc;
        this.view.lblIntermediaryBIC.skin = this.Skins.FieldLabel;
        this.view.lblLookup.skin = this.Skins.LookupLabel;
        this.view.lblNewAcccountInfoHeader.skin = this.Skins.InfoHeader;
        this.view.lblNewAccountInfoText.skin = this.Skins.InfoDesc;
        this.view.lblNewPayeeAccountNumber.skin = this.Skins.FieldLabel;
        this.view.lblNewPayeeReenterAccountNumber.skin = this.Skins.FieldLabel;
        this.view.lblReenterNewAcccountInfoHeader.skin = this.Skins.InfoHeader;
        this.view.lblReenterNewAccountInfoText.skin = this.Skins.InfoDesc;
        this.view.lblNoteInfoHeader.skin = this.Skins.InfoHeader;
        this.view.lblNotesInfoTxt.skin = this.Skins.InfoDesc;
        this.view.lblNotes.skin = this.Skins.FieldLabel;
        this.view.lblNumberOfRecurrences.skin = this.Skins.FieldLabel;
        this.view.lblRecurrencesInfoHeader.skin = this.Skins.InfoHeader;
        this.view.lblRecurrenceInfoText.skin = this.Skins.InfoDesc;
        this.view.lblOption1InfoTxt.skin = this.Skins.InfoDesc;
        this.view.lblOption2Info.skin = this.Skins.InfoDesc;
        this.view.lblOption3Info.skin = this.Skins.InfoDesc;
        this.view.lblOption4.skin = this.Skins.InfoDesc;
        this.view.lblPayeeAddress.skin = this.Skins.FieldLabelBold;
        this.view.lblPayeeAddressOptional.skin = this.Skins.FieldLabel;
        this.view.lblPayeeDetailField1.skin = this.Skins.FieldLabel;
        this.view.lblPayeeDetailField2.skin = this.Skins.FieldLabel;
        this.view.lblPayeeDetailField3.skin = this.Skins.FieldLabel;
        this.view.lblPayeeDetailField4.skin = this.Skins.FieldLabel;														
        this.view.lblPayeeDetailFieldInfo.skin = this.Skins.PayeeDetailFieldInfo;
        this.view.lblPayeeType1.skin = this.Skins.RadioInputTxt;
        this.view.lblPayeeType2.skin = this.Skins.RadioInputTxt;
        this.view.lblPaymentAmount1.skin = this.Skins.RadioInputTxt;
        this.view.lblPaymentAmount1Value.skin = this.Skins.FieldLabel;
        this.view.lblPaymentAmount2.skin = this.Skins.RadioInputTxt;
        this.view.lblPaymentAmount2Value.skin = this.Skins.RadioInputTxt;
        this.view.lblPaymentAmount3.skin = this.Skins.RadioInputTxt;
        this.view.lblPaymentAmount3Value.skin = this.Skins.FieldLabel;
        this.view.lblPaymentAmount4.skin = this.Skins.RadioInputTxt;
        this.view.lblPaymentMethod.skin = this.Skins.FieldLabel;
        this.view.lblPaymentMethod1.skin = this.Skins.RadioInputTxt;
        this.view.lblPaymentMethod2.skin = this.Skins.RadioInputTxt;
        this.view.lblPaymentMethod3.skin = this.Skins.RadioInputTxt;
        this.view.lblPaymentMethod4.skin = this.Skins.RadioInputTxt;
        this.view.lblPaymentMethodInfoHeade.skin = this.Skins.InfoHeader;
        this.view.lblSelectedFrequency.skin = this.Skins.DropdownTxt;
        this.view.lblSelectedFrequency.focusSkin = this.Skins.DropdownTxt;
        this.view.lblSelectedTransferCCY.skin = this.Skins.DropdownTxt;
        this.view.lblSelectedTransferCCY.focusSkin = this.Skins.DropdownTxt;
        this.view.lblSelectedTransferDurationType.skin = this.Skins.DropdownTxt;
        this.view.lblSelectedTransferDurationType.focusSkin = this.Skins.DropdownTxt;
        this.view.lblStartDate.skin = this.Skins.FieldLabel;
        this.view.lblStartDateInfoHeader.skin = this.Skins.InfoHeader;
        this.view.lblStartDateInfoText.skins = this.Skins.InfoDesc;
        this.view.lblToAccount.skin = this.Skins.FieldLabel;
        this.view.lblToAcccountInfoHeader.skin = this.Skins.InfoHeader;
        this.view.lblTransferAmount.skin = this.Skins.FieldLabel;
        this.view.lblTransferAmountInfoHeader.skin = this.Skins.FieldLabel;
        this.view.lblTransferAmountInfoText.skin = this.Skins.InfoDesc;
        this.view.lblTransferCurrency.skin = this.Skins.FieldLabel;
        this.view.lblTransferCurrencyInfoHeader.skin = this.Skins.InfoHeader;
        this.view.lblTransferCurrencyInfoText.skin = this.Skins.InfoDesc;
        this.view.lblTransferDuration.skin = this.Skins.FieldLabel;
        this.view.lblTransferDurationTypeInfoHeader.skin = this.Skins.InfoHeader;
        this.view.lblToAccountInfoText.skin = this.Skins.InfoDesc;
        this.view.lblTransferDurationTypeInfoText.skin = this.Skins.InfoDesc;

        //Text Box Skins
        this.view.txtBoxAmountOthers.skin = this.Skins.TxtBoxField;
        this.view.txtBoxAmountOthers.focusSkin = this.Skins.TxtBoxField;
        this.view.txtBoxE2E.skin = this.Skins.TxtBoxField;
        this.view.txtBoxE2E.focusSkin = this.Skins.TxtBoxField;
        this.view.txtBoxField.skin = this.Skins.TxtBoxField;
        this.view.txtBoxField.focusSkin = this.Skins.TxtBoxField;
        this.view.txtBoxFXRateReference.skin = this.Skins.TxtBoxField;
        this.view.txtBoxFXRateReference.focusSkin = this.Skins.TxtBoxField;
        this.view.txtBoxIntermediaryBIC.skin = this.Skins.TxtBoxField;
        this.view.txtBoxIntermediaryBIC.focusSkin = this.Skins.TxtBoxField;
        this.view.txtBoxNewAccountNumber.skin = this.Skins.TxtBoxField;
        this.view.txtBoxNewAccountNumber.focusSkin = this.Skins.TxtBoxField;
        this.view.txtBoxPayeeDetailField1.skin = this.Skins.TxtBoxField;
        this.view.txtBoxPayeeDetailField1.focusSkin = this.Skins.TxtBoxField;
        this.view.txtBoxPayeeDetailField2.skin = this.Skins.TxtBoxField;
        this.view.txtBoxPayeeDetailField2.focusSkin = this.Skins.TxtBoxField;
        this.view.txtBoxPayeeDetailField3.skin = this.Skins.TxtBoxField;
        this.view.txtBoxPayeeDetailField3.focusSkin = this.Skins.TxtBoxField;
        this.view.txtBoxPayeeDetailField4.skin = this.Skins.TxtBoxField;
        this.view.txtBoxPayeeDetailField4.focusSkin = this.Skins.TxtBoxField;
        this.view.txtBoxPayeeName.skin = this.Skins.TxtBoxField;
        this.view.txtBoxPayeeName.focusSkin = this.Skins.TxtBoxField;
        this.view.txtBoxRecurrences.skin = this.Skins.TxtBoxField;
        this.view.txtBoxRecurrences.focusSkin = this.Skins.TxtBoxField;
        this.view.txtBoxReenterAccountNumber.skin = this.Skins.TxtBoxField;
        this.view.txtBoxReenterAccountNumber.focusSkin = this.Skins.TxtBoxField;
        this.view.txtBoxTransferAmount.skin = this.Skins.TxtBoxField;
        this.view.txtBoxTransferAmount.focusSkin = this.Skins.TxtBoxField;

        //Teaxt Area Skins
        this.view.txtAreaNotes.skin = this.Skins.TextArea;
        this.view.txtAreaNotes.focusSkin = this.Skins.TextArea;

        //Placeholder Skins
        this.view.txtBoxPayeeDetailField1.placeholderSkin = this.Skins.TxtBoxPlaceholder;
        this.view.txtBoxFXRateReference.placeholderSkin = this.Skins.TxtBoxPlaceholder;
        this.view.txtBoxAmountOthers.placeholderSkin = this.Skins.TxtBoxPlaceholder;
        this.view.txtBoxE2E.placeholderSkin = this.Skins.TxtBoxPlaceholder;
        this.view.txtBoxField.placeholderSkin = this.Skins.TxtBoxPlaceholder;
        this.view.txtBoxIntermediaryBIC.placeholderSkin = this.Skins.TxtBoxPlaceholder;
        this.view.txtBoxNewAccountNumber.placeholderSkin = this.Skins.TxtBoxPlaceholder;
        this.view.txtBoxPayeeDetailField2.placeholderSkin = this.Skins.TxtBoxPlaceholder;
        this.view.txtBoxPayeeDetailField3.placeholderSkin = this.Skins.TxtBoxPlaceholder;
        this.view.txtBoxPayeeDetailField4.placeholderSkin = this.Skins.TxtBoxPlaceholder;
        this.view.txtBoxPayeeName.placeholderSkin = this.Skins.TxtBoxPlaceholder;
        this.view.txtBoxRecurrences.placeholderSkin = this.Skins.TxtBoxPlaceholder;
        this.view.txtBoxReenterAccountNumber.placeholderSkin = this.Skins.TxtBoxPlaceholder;
        this.view.txtBoxTransferAmount.placeholderSkin = this.Skins.TxtBoxPlaceholder;
        this.view.txtAreaNotes.placeholderSkin = this.Skins.TxtBoxPlaceholder;

        //Button Skins
        this.view.btnCancel.skin = this.Skins.PrimaryBtn;
        this.view.btnContinue.skin = this.Skins.SecondayBtn;
        this.view.btnCancel.focusSkin = this.Skins.PrimaryBtnFocus;
        this.view.btnContinue.focusSkin = this.Skins.SecondaryBtnFocus;
        this.view.btnCancel.hoverSkin = this.Skins.PrimaryBtnHover;
        this.view.btnContinue.hoverSkin = this.Skins.SecondaryBtnHover;

        if(this.isDVFValidated === "YES") {
          this.view.btnContinue.setEnabled(true);
          this.view.btnContinue.skin = this.Skins.EnabledBtnSkin;
          this.view.btnContinue.hoverSkin = this.Skins.EnabledBtnSkin;
          this.view.btnContinue.focusSkin = this.Skins.EnabledBtnSkin;
        }
      }
      catch(e) {
        var errorObj =
            {
              "errorInfo" : "Error in setting the skins",
              "errorLevel" : "Configuration",
              "error": e
            };
      }
    },

    /**
     * storeSkinValues
     * @api : storeSkinValues
     * stores skins provided in contracts in specidic variables
     * @return : NA
     */
    storeSkinValues: function() {
      this.Skins.FieldLabel = this.getParsedValue(this._sknFieldLabel, kony.application.getCurrentBreakpoint());
      this.Skins.TxtBoxField = this.getParsedValue(this._sknFieldValue, kony.application.getCurrentBreakpoint());
      this.Skins.FieldValue = this.getParsedValue(this._sknFieldReadOnlyValue, kony.application.getCurrentBreakpoint());
      this.Skins.TxtBoxPlaceholder = this.getParsedValue(this._sknTextBoxPlaceholder, kony.application.getCurrentBreakpoint());
      this.Skins.ErrorField = this.getParsedValue(this._sknErrorTextInput, kony.application.getCurrentBreakpoint());
      this.Skins.LookupLabel = this.getParsedValue(this._sknLookupLabel, kony.application.getCurrentBreakpoint());
      this.Skins.selectedRadioButtonSkin = this.getParsedValue(this._sknradioIconSelected, kony.application.getCurrentBreakpoint());
      this.Skins.unseletedRadioBtnSkn = this.getParsedValue(this._sknradioIconUnselected, kony.application.getCurrentBreakpoint());
      this.Skins.disabledRadioBtnSkn = this.getParsedValue(this._sknradioIconDisabled, kony.application.getCurrentBreakpoint());      
      this.Skins.RadioInputSelectedTxt = this.getParsedValue(this._sknradioInputSelected, kony.application.getCurrentBreakpoint());
      this.Skins.RadioInputTxt = this.getParsedValue(this._sknradioInputText, kony.application.getCurrentBreakpoint());
      this.Skins.DropdownTxt = this.getParsedValue(this._sknDropdownText, kony.application.getCurrentBreakpoint());
      this.Skins.PrimaryBtn = this.getParsedValue(this._sknPrimaryBtn, kony.application.getCurrentBreakpoint());
      this.Skins.PrimaryBtnHover = this.getParsedValue(this._sknPrimaryBtnHover, kony.application.getCurrentBreakpoint());
      this.Skins.PrimaryBtnFocus = this.getParsedValue(this._sknPrimaryBtnFocus, kony.application.getCurrentBreakpoint());
      this.Skins.SecondayBtn = this.getParsedValue(this._sknSecondaryBtn, kony.application.getCurrentBreakpoint());
      this.Skins.SecondaryBtnHover = this.getParsedValue(this._sknSecondaryBtnHover, kony.application.getCurrentBreakpoint());
      this.Skins.SecondaryBtnFocus = this.getParsedValue(this._sknSecondaryBtnFocus, kony.application.getCurrentBreakpoint());
      this.Skins.EnabledBtnSkin = this.getParsedValue(this._sknEnabledBtn, kony.application.getCurrentBreakpoint());
      this.Skins.DisabledBtnSkin = this.getParsedValue(this._sknDisabledBtn, kony.application.getCurrentBreakpoint());
      this.Skins.InfoHeader = this.getParsedValue(this._sknInfoHead, kony.application.getCurrentBreakpoint());
      this.Skins.InfoDesc = this.getParsedValue(this._sknInfoText, kony.application.getCurrentBreakpoint());
      this.Skins.TextArea = this.getParsedValue(this._sknTextArea, kony.application.getCurrentBreakpoint());
      this.Skins.PayeeDetailFieldInfo = this.getParsedValue(this._sknPayeeDetailInfoText, kony.application.getCurrentBreakpoint());
      this.Skins.FlexHoverSkin = this.getParsedValue(this._sknMandatoryTextBox, kony.application.getCurrentBreakpoint());
      this.Skins.FlexFocusSkin = this.getParsedValue(this._sknTextBoxFocus, kony.application.getCurrentBreakpoint());
      this.Skins.TextBoxFlexSkin = this.getParsedValue(this._sknTextBoxFlex, kony.application.getCurrentBreakpoint());
      this.Skins.EnabledFlex = this.getParsedValue(this._sknFlexEnabled, kony.application.getCurrentBreakpoint());
      this.Skins.DisabledFlex = this.getParsedValue(this._sknFlexDisabled, kony.application.getCurrentBreakpoint());
      this.Skins.FieldLabelBold = this.getParsedValue(this._sknFieldLabelBold, kony.application.getCurrentBreakpoint());
    },

    /**
     * resetTextBoxesSkins
     * @api : resetTextBoxesSkins
     * sets skin for text boxes
     * @return : NA
     */
    resetTextBoxesSkins: function() {
      this.view.txtBoxAmountOthers.skin = this.Skins.TxtBoxField;
      this.view.txtBoxE2E.skin = this.Skins.TxtBoxField;
      this.view.txtBoxField.skin = this.Skins.TxtBoxField;
      this.view.txtBoxFXRateReference.skin = this.Skins.TxtBoxField;
      this.view.txtBoxIntermediaryBIC.skin = this.Skins.TxtBoxField;
      this.view.txtBoxNewAccountNumber.skin = this.Skins.TxtBoxField;
      this.view.txtBoxPayeeDetailField1.skin = this.Skins.TxtBoxField;
      this.view.txtBoxPayeeDetailField2.skin = this.Skins.TxtBoxField;
      this.view.txtBoxPayeeDetailField3.skin = this.Skins.TxtBoxField;
      this.view.txtBoxPayeeName.skin = this.Skins.TxtBoxField;
      this.view.txtBoxRecurrences.skin = this.Skins.TxtBoxField;
      this.view.txtBoxReenterAccountNumber.skin = this.Skins.TxtBoxField;
      this.view.txtBoxTransferAmount.skin = this.Skins.TxtBoxField;
    },

    /**
     * setButtonText
     * @api : setButtonText
     * helper method for parsing button text
     * @return : returns corresponding property for the respective breakpoints
     */
    setButtonTexts: function() {
      this.view.btnSecondary.text = this.getParsedValue(this._button1, kony.application.getCurrentBreakpoint());
      this.view.btnPrimary.text = this.getParsedValue(this._button2, kony.application.getCurrentBreakpoint());
    },

    /**
     * setSegmentData
     * @api : setSegmentData
     * defines the data in segments
     * @return : NA
     */
    setSegmentData: function(seg, ContractJSON, flxSegement, lblSelectedValue) {
      var segmentData = [];
      if(ContractJSON != ""){
        seg.widgetDataMap = this.getWidgetDataMap();
        var segmentContractArray = JSON.parse(ContractJSON);
        segmentContractArray = segmentContractArray.optionValues;
        segmentContractArray.forEach(function(object){
          for(var key in object){
            object["value"]={};
            object["value"]["text"] = object[key];
            object["value"]["skin"] = scope.Skins.DropdownTxt;
            object["key"] = key;
          } 
          segmentData.push(object);
        });
        lblSelectedValue.text = segmentContractArray[0]["value"]["text"];
        seg.setData(segmentData);
      }
      else {
        flxSegement = {isVisible : false};
      }
    },

    /**
     * getWidgetDataMap
     * @api : getWidgetDataMap
     * defines the data mapping values and key
     * @return : widgetDataMap
     */
    getWidgetDataMap: function() {
      var widgetDataMap = {
        "lblListValue" : "value",
        "selectedKey" : "key"
      };
      return widgetDataMap;
    },

    /**
     * setFeePaidFieldContract
     * @api : setFeePaidFieldContract
     * sets the fees paid by field from contracts
     * @return : NA
     */
    setFeePaidFieldContract: function() {
      if(this._feesPaidOption1 != "") {
        var option1JSON = JSON.parse(this._feesPaidOption1);
        this.view.lblFeePaidType1.text = this.getLabelText(option1JSON.optionValue);
        if(option1JSON.optionSeleted == true) {
          this.view.lblFeePaidOption1.text = this.Icons.selectedRadioButton ;
          this.view.lblFeePaidOption1.skin = this.Skins.selectedRadioButtonSkin;
          this.isPaidBy = "OUR";
        } else if(this.view.lblPaymentMethodOption1.skin === this.Skins.selectedRadioButtonSkin || this.view.lblPaymentMethodOption2.skin === this.Skins.selectedRadioButtonSkin){
          this.view.flxFeePaidType1.setEnabled(false);
          this.view.lblFeePaidOption1.text = this.Icons.unSelectedRadioButton ;
          this.view.lblFeePaidOption1.skin = this.Skins.disabledRadioBtnSkn;
        }
        else {
          this.view.lblFeePaidOption1.text = this.Icons.unSelectedRadioButton ;
          this.view.lblFeePaidOption1.skin = this.Skins.unseletedRadioBtnSkn;
        }
        if(this._feesPaidOption2 != "") {
          var option2JSON = JSON.parse(this._feesPaidOption2);
          this.view.lblFeePaidType2.text = this.getLabelText(option2JSON.optionValue);
          if(option2JSON.optionSeleted == true) {
            this.view.lblFeePaidOption2.text = this.Icons.selectedRadioButton ;
            this.view.lblFeePaidOption2.skin = this.Skins.selectedRadioButtonSkin;
            this.isPaidBy = "BEN";
          } else if((this.view.lblPaymentMethodOption1.skin === this.Skins.selectedRadioButtonSkin && this.view.flxPaymentMethod1.isVisible) || (this.view.lblPaymentMethodOption2.skin === this.Skins.selectedRadioButtonSkin && this.view.flxPaymentMethod2.isVisible)){
            this.view.flxFeePaidType2.setEnabled(false);
            this.view.lblFeePaidOption2.text = this.Icons.unSelectedRadioButton ;
            this.view.lblFeePaidOption2.skin = this.Skins.disabledRadioBtnSkn;
        }
          else {
            this.view.lblFeePaidOption2.text = this.Icons.unSelectedRadioButton ;
            this.view.lblFeePaidOption2.skin = this.Skins.unseletedRadioBtnSkn;
          }
          if(this._feesPaidOption3 != "") {
            var option3JSON = JSON.parse(this._feesPaidOption3);
            this.view.lblFeePaidType3.text = this.getLabelText(option3JSON.optionValue);
            if(option3JSON.optionSeleted == true) {
              this.view.lblFeePaidOption3.text = this.Icons.selectedRadioButton ;
              this.view.lblFeePaidOption3.skin = this.Skins.selectedRadioButtonSkin;
              this.isPaidBy = "SHA";
            }
            else {
              this.view.lblFeePaidOption3.text = this.Icons.unSelectedRadioButton ;
              this.view.lblFeePaidOption3.skin = this.Skins.unseletedRadioBtnSkn;
            }
          }
          else {
            this.view.flxFeePaidType3.setVisibility(false);
          }
        }
        else {
          this.view.flxFeePaidType2.setVisibility(false);
        }
      }
      else {
        this.view.flxFeePaidType1.setVisibility(false);
      }
      // Hide Fees Paid field if all fees paid option contract values are empty
      if((this._feesPaidOption1 === "") && (this._feesPaidOption2 === "") && (this._feesPaidOption3 === "") )
        this.view.flxFeePaidField.setVisibility(false);
      //Adjust radio button position based on content width (Not for mobile break point)
      if(kony.application.getCurrentBreakpoint() != this.getFieldValue(this._BREAKPTS, "BP1") ){
        var scope = this;
        var lblFeePaidType1Width = "";
        var flxFeePaidType1Width = "";
        var lblFeePaidType2Width = "";
        var flxFeePaidType2Width = "";
        if(scope.view.lblFeePaidType1.info.frame){
          lblFeePaidType1Width = scope.view.lblFeePaidType1.info.frame.width;
          flxFeePaidType1Width = 35+lblFeePaidType1Width;
          scope.view.flxFeePaidType1.width = flxFeePaidType1Width+"dp";
          scope.view.flxFeePaidType2.left = 5+flxFeePaidType1Width+"dp";
          if(scope.view.lblFeePaidType2.info.frame){
            lblFeePaidType2Width = scope.view.lblFeePaidType2.info.frame.width;
            flxFeePaidType2Width = 30+lblFeePaidType2Width;
            scope.view.flxFeePaidType2.width = 30+flxFeePaidType2Width+"dp";
            scope.view.flxFeePaidType3.left = 10+flxFeePaidType1Width+flxFeePaidType2Width+"dp";
          }else{
            scope.view.flxFeePaidType3.left = scope.view.flxFeePaidType2.left;
          }
        }else if(scope.view.lblFeePaidType2.info.frame){
          lblFeePaidType2Width = scope.view.lblFeePaidType2.info.frame.width;
          flxFeePaidType2Width = 30+lblFeePaidType2Width;
          scope.view.flxFeePaidType2.left = "0dp";
          scope.view.flxFeePaidType2.width = 30+flxFeePaidType2Width+"dp";
          scope.view.flxFeePaidType3.left = 10+flxFeePaidType2Width+"dp";
        }
      }

      if(this._feesPaidOption1 == "" && this._feesPaidOption2 == "" && this._feesPaidOption3 != ""){
        scope.view.flxFeePaidType3.left = scope.view.flxFeePaidType1.left;
      }
    },

    /**
     * setPayeeAmountFieldContract
     * @api : setPayeeAmountFieldContract
     * sets the payment amount field from contracts
     * @return : NA
     */
    setPayeeAmountFieldContract: function() {
      if(this._paymentAmountOption1 != "") {
        var option1JSON = JSON.parse(this._paymentAmountOption1);
        this.view.lblPaymentAmount1.text = this.getLabelText(option1JSON.optionValue);
        if(option1JSON.optionSeleted == true) {
          this.view.lblPaymentAmountOption1.text = this.Icons.selectedRadioButton ;
          this.view.lblPaymentAmountOption1.skin = this.Skins.selectedRadioButtonSkin ;
        }
        else {
          this.view.lblPaymentAmountOption1.text = this.Icons.unSelectedRadioButton ;
          this.view.lblPaymentAmountOption1.skin = this.Skins.unseletedRadioBtnSkn ;
        }
        if(this._paymentAmountOption2 != "") {
          var option2JSON = JSON.parse(this._paymentAmountOption2);
          this.view.lblPaymentAmount2.text = this.getLabelText(option2JSON.optionValue);
          if(option2JSON.optionSeleted == true) {
            this.view.lblPaymentAmountOption2.text = this.Icons.selectedRadioButton ;
            this.view.lblPaymentAmountOption2.skin = this.Skins.selectedRadioButtonSkin ;
          }
          else {
            this.view.lblPaymentAmountOption2.text = this.Icons.unSelectedRadioButton ;
            this.view.lblPaymentAmountOption2.skin = this.Skins.unseletedRadioBtnSkn ;
          }
          if(this._paymentAmountOption3 != "") {
            var option3JSON = JSON.parse(this._paymentAmountOption3);
            this.view.lblPaymentAmount3.text = this.getLabelText(option3JSON.optionValue);
            if(option3JSON.optionSeleted == true) {
              this.view.lblPaymentAmountOption3.text = this.Icons.selectedRadioButton ;
              this.view.lblPaymentAmountOption3.skin = this.Skins.selectedRadioButtonSkin ;
            }
            else {
              this.view.lblPaymentAmountOption3.text = this.Icons.unSelectedRadioButton ;
              this.view.lblPaymentAmountOption3.skin = this.Skins.unseletedRadioBtnSkn ;
            }
            if(this._paymentAmountOption4 != "") {
              var option4JSON = JSON.parse(this._paymentAmountOption4);
              this.view.lblPaymentAmount4.text = this.getLabelText(option4JSON.optionValue);
              if(option4JSON.optionSeleted == true) {
                this.view.lblPaymentAmountOption4.text = this.Icons.selectedRadioButton ;
                this.view.lblPaymentAmountOption4.skin = this.Skins.selectedRadioButtonSkin ;
              }
              else {
                this.view.lblPaymentAmountOption4.text = this.Icons.unSelectedRadioButton ;
                this.view.lblPaymentAmountOption4.skin = this.Skins.unseletedRadioBtnSkn ;
              }
            }
            else {
              this.view.flxPaymentAmount4.setVisibility(false);
            }
          }
          else {
            this.view.flxPaymentAmount3.setVisibility(false);
          }
        }else{
          this.view.flxPaymentAmount2.setVisibility(false);
        }
      }
      else {
        this.view.flxPaymentAmountField.setVisibility(false);
      }
      // Hide Payment amount field if all payment amount contract values are empty
      if((this._paymentAmountOption1 === "") && (this._paymentAmountOption2 === "") && (this._paymentAmountOption3 === "") && (this._paymentAmountOption4 === ""))
        this.view.flxPaymentAmountField.setVisibility(false);
    },

    /**
     * setPaymentMethodFieldContract
     * @api : setPaymentMethodFieldContract
     * sets the payment method field from contracts
     * @return : NA
     */
    setPaymentMethodFieldContract: function() {
      if(this._paymentMethodOption1 != ""){
        var option1JSON = JSON.parse(this._paymentMethodOption1);
        this.view.flxPaymentMethod1.setVisibility(true);
        this.view.lblPaymentMethod1.text = this.getLabelText(option1JSON.optionValue);
        if(option1JSON.optionSeleted == true){
          this.view.lblPaymentMethodOption1.text = this.Icons.selectedRadioButton ;
          this.view.lblPaymentMethodOption1.skin = this.Skins.selectedRadioButtonSkin;
        }
        else {
          this.view.lblPaymentMethodOption1.text = this.Icons.unSelectedRadioButton ;
          this.view.lblPaymentMethodOption1.skin = this.Skins.unseletedRadioBtnSkn; 
        }
        if(this._paymentMethodOption2 != "") {
          var option2JSON = JSON.parse(this._paymentMethodOption2);
          this.view.flxPaymentMethod2.setVisibility(true);
          this.view.lblPaymentMethod2.text = this.getLabelText(option2JSON.optionValue);
          if(option2JSON.optionSeleted == true) {
            this.view.lblPaymentMethodOption2.text = this.Icons.selectedRadioButton ;
            this.view.lblPaymentMethodOption2.skin = this.Skins.selectedRadioButtonSkin; 
          }
          else {
            this.view.lblPaymentMethodOption2.text = this.Icons.unSelectedRadioButton ;
            this.view.lblPaymentMethodOption2.skin = this.Skins.unseletedRadioBtnSkn; 
          }
          if(this._paymentMethodOption3 != "") {
            var option3JSON = JSON.parse(this._paymentMethodOption3);
            this.view.flxPaymentMethod3.setVisibility(true);
            this.view.lblPaymentMethod3.text = this.getLabelText(option3JSON.optionValue);
            if(option3JSON.optionSeleted == true) {
              this.view.lblPaymentMethodOption3.text = this.Icons.selectedRadioButton ;
              this.view.lblPaymentMethodOption3.skin = this.Skins.selectedRadioButtonSkin; 
            }
            else {
              this.view.lblPaymentMethodOption3.text = this.Icons.unSelectedRadioButton ;
              this.view.lblPaymentMethodOption3.skin = this.Skins.unseletedRadioBtnSkn; 
            }
            if(this._paymentMethodOption4 != "") {
              var option4JSON = JSON.parse(this._paymentMethodOption4);
              this.view.flxPaymentMethod4.setVisibility(true);
              this.view.lblPaymentMethod4.text = this.getLabelText(option4JSON.optionValue);
              if(option4JSON.optionSeleted == true) {
                this.view.lblPaymentMethodOption4.text = this.Icons.selectedRadioButton ;
                this.view.lblPaymentMethodOption4.skin = this.Skins.selectedRadioButtonSkin;
              }
              else {
                this.view.lblPaymentMethodOption4.text = this.Icons.unSelectedRadioButton ;
                this.view.lblPaymentMethodOption4.skin = this.Skins.unseletedRadioBtnSkn; 
              }
            }
            else {
              this.view.flxPaymentMethod4.setVisibility(false);
            }
          }
          else {
            this.view.flxPaymentMethod3.setVisibility(false);
          }
        }
        else {
          this.view.flxPaymentMethod2.setVisibility(false);
        }
      }
      else {
        this.view.flxPaymentMethod1.setVisibility(false);
      }
      // Hide Payment method field if all payment method option contract values are empty
      if((this._paymentMethodOption1 === "") && (this._paymentMethodOption2 === "") && (this._paymentMethodOption3 === "") &&(this._paymentMethodOption4 === ""))
        this.view.flxPaymentMethodField.setVisibility(false);
    },

    /**
     * setPayeeTypeFieldContract
     * @api : setPayeeTypeFieldContract
     * sets the type of payee from contracts
     * @return : NA
     */
    setPayeeTypeFieldContract: function() {
      if(this._payeeTypeOption1 != "") {
        var option1JSON = JSON.parse(this._payeeTypeOption1);
        this.view.lblPayeeType1.text = this.getLabelText(option1JSON.optionValue);
        if(option1JSON.optionSeleted == true) {
          this.view.lblPayeeType1Option.text = this.Icons.selectedRadioButton ;
          this.view.lblPayeeType1Option.skin = this.Skins.selectedRadioButtonSkin;
        }
        else {
          this.view.lblPayeeType1Option.text = this.Icons.unSelectedRadioButton;
          this.view.lblPayeeType1Option.skin = this.Skins.unseletedRadioBtnSkn;
        }
        if(this._payeeTypeOption2 != "") {
          var option2JSON = JSON.parse(this._payeeTypeOption2);
          this.view.lblPayeeType2.text =this.getLabelText(option2JSON.optionValue);
          if(option2JSON.optionSeleted == true) {
            this.view.lblPayeeType2Option.text = this.Icons.selectedRadioButton ;
            this.view.lblPayeeType2Option.skin = this.Skins.selectedRadioButtonSkin;
          }
          else {
            this.view.lblPayeeType2Option.text = this.Icons.unSelectedRadioButton;
            this.view.lblPayeeType2Option.skin = this.Skins.unseletedRadioBtnSkn;
          }
        }
        else {
          this.view.flxPayeeType2.setVisibility(false);
        }
      }
      else {
        this.view.flxPayeeType.setVisibility(false);
      }
    },

    /**
     * toggleAddressVisibility
     * @api : toggleAddressVisibility
     * toggles the visibility of payee address flex
     * @return : NA
     */
    toggleAddressVisibility: function() {
      var addressVisibility =  this.view.flxAddressFields.isVisible;
      if(addressVisibility === true)
        this.view.flxAddressFields.isVisible = false;
      else
        this.view.flxAddressFields.isVisible = true;
    },

    /**
     * setTxtBoxPropertiesFromContract
     * @api : setTxtBoxPropertiesFromContract
     * sets the properties of text boxes from contracts
     * @return : NA
     */
    setTxtBoxPropertiesFromContract: function(txtBoxField, contractJSON, flxtxtBoxField) {
      if(contractJSON != ""){
        var txtBoxProps = JSON.parse(contractJSON);

        if(!this.isNullOrUndefinedOrEmpty(txtBoxProps)) {
          //mapping
          if(!this.isNullOrUndefinedOrEmpty(txtBoxProps.mapping)) {
            if(this.getParsedValue(txtBoxProps.mapping) != "" || this.getParsedValue(txtBoxProps.mapping) != undefined )
            {
              var mapText = this.getParsedValue(txtBoxProps.mapping);
              this.view[txtBoxField].text = mapText;
              var inputMapper = txtBoxProps.mapping.substring(5,txtBoxProps.mapping.length-1);
              this.textInputsMapping[txtBoxField] = inputMapper;
              this.dataContext[this.textInputsMapping[txtBoxField]] = mapText;

            }
          }
          //maximum length
          if(!this.isNullOrUndefinedOrEmpty(txtBoxProps.maxLength)) {
            var maximumLength = this.getParsedValue(txtBoxProps.maxLength);
            this.view[txtBoxField].maxTextLength = maximumLength;
          }
          //Placeholder
          if(!this.isNullOrUndefinedOrEmpty(txtBoxProps.placeHolder)) {
            var placeHolderValue = this.getParsedValue(txtBoxProps.placeHolder,kony.application.getCurrentBreakpoint());
            this.view[txtBoxField].placeholder =
              placeHolderValue ? placeHolderValue : "";
          }
          //tooltip
          if(!this.isNullOrUndefinedOrEmpty(txtBoxProps.tooltip)) {
            this.view[txtBoxField].toolTip = this.getParsedValue(txtBoxProps.tooltip,kony.application.getCurrentBreakpoint());
          }
          //inputMode
          /*  if(!this.isNullOrUndefinedOrEmpty(txtBoxProps.inputMode)) {
            if(txtBoxProps.inputMode === "NUMERIC" && txtBoxProps.isMaskingEnabled === true) {
              this.view[txtBoxField].restrictCharactersSet = 
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~!@#$%^&*()_-\\?/+={[]}:;,.<>'`|\"";
              this.view[txtBoxField].secureTextEntry = true;
            }
            else if(txtBoxProps.inputMode === "NUMERIC" && txtBoxProps.isMaskingEnabled === false) {
              this.view[txtBoxField].textInputMode 
                = constants.TEXTBOX_INPUT_MODE_NUMERIC;
              this.view[txtBoxField].secureTextEntry = false;
            }
            else {
              this.view[txtBoxField].restrictCharactersSet
                = "";
              this.view[txtBoxField].textInputMode 
                = constants.TEXTBOX_INPUT_MODE_ANY;
              this.view[txtBoxField].secureTextEntry = txtBoxProps.isMaskingEnabled ? txtBoxProps.isMaskingEnabled : false;
            }
            return;
          }*/
          if(!this.isNullOrUndefinedOrEmpty(txtBoxProps.inputMode)) {
            if(txtBoxProps.inputMode === "NUMERIC" && txtBoxProps.type !== "Amount"){
              this.view[txtBoxField].restrictCharactersSet = 
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~!@#$%^&*()_-\\?/+={[]}:;,.<>'`|\"";          
            }  
            else if(txtBoxProps.inputMode === "NUMERIC" && txtBoxProps.type === "Amount"){
              this.view[txtBoxField].restrictCharactersSet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~!@#$%^&*()_-\\?/+={[]}:;,<>'`|\"";
            }
            else {
              this.view[txtBoxField].restrictCharactersSet = "";                      
            }
            //    return;
          } 
          //this.view[txtBoxField].restrictCharactersSet = "";
          //  this.view[txtBoxField].textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
          this.view[txtBoxField].secureTextEntry = txtBoxProps.isMaskingEnabled ? txtBoxProps.isMaskingEnabled : false;

        }
      }
      else
      {
        // flxtxtBoxField = {isVisible : false};
        this.view[flxtxtBoxField].isVisible = false;
      }
    },

    /**
     * isNullOrUndefinedOrEmpty
     * @api : isNullOrUndefinedOrEmpty
     * checks whether the input is null or empty or undefined
     * @return : NA
     */
    isNullOrUndefinedOrEmpty: function(val) {
      if (val === null || val === undefined || val ==="") {
        return true;
      } else {
        return false;
      }
    },

    /**
     * setIntermediaryandE2EFlex
     * @api : setIntermediaryandE2EFlex
     * sets the visibility of intermediary bic and E2E flexes
     * @return : NA
     */
    setIntermediaryandE2EFlex: function() {
      var txtBoxProp1 = this._txtInputIntermediaryBIC;
      var txtBoxProp2 = this._txtInputE2E;
      if(txtBoxProp1 == "" && txtBoxProp2 == "") {
        this.view.flxIntermediaryBICE2EField.isVisible = false;
      }
    },

    /**
     * setPayeeDetailsFlex
     * @api : setPayeeDetailsFlex
     * sets the visibility of payee details flex
     * @return : NA
     */
    setPayeeDetailsFlex: function() {
      var txtBoxProp1 = this._txtInputPayeeDetail1;
      var txtBoxProp2 = this._txtInputPayeeDetail2;
      var txtBoxProp3 = this._txtInputPayeeDetail3;
      var txtBoxProp4 = this.txtInputPayeeDetail4;
      if(this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT2")) {
        if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T4")) {
          this.view.flxPayeeDetailFields.isVisible = false;
        }
      }
      if(txtBoxProp1 == "" && txtBoxProp2 == "" && txtBoxProp3 == "" && txtBoxProp4 == "")
      {
        this.view.flxPayeeDetailFields.isVisible = false;
      }
      if(txtBoxProp3 == "" && txtBoxProp4 == "")
      {
        this.view.flxPayeeDetailFieldRow2.isVisible = false;
      }
    },

    /**
     * setInfoTexts
     * @api : setLabelData
     * sets the data in info pop up
     * @return : NA
     */
    setInfoTexts: function(lblHeader, lblDescription, ContractJSON, infoIcon) {
      if(ContractJSON != "" && ContractJSON !== undefined){
        var infoTextJSON = JSON.parse(ContractJSON);
        if(!this.isNullOrUndefinedOrEmpty(infoTextJSON.infoIconText)){
          if(!this.isNullOrUndefinedOrEmpty(infoTextJSON.infoIconText.header) && !this.isNullOrUndefinedOrEmpty(lblHeader)){
            lblHeader.text = this.getLabelText(infoTextJSON.infoIconText.header);
          }
          if(!this.isNullOrUndefinedOrEmpty(infoTextJSON.infoIconText.text) &&!this.isNullOrUndefinedOrEmpty(lblDescription) ){
            lblDescription.text = this.getLabelText(infoTextJSON.infoIconText.text);
          }   
        }
        else{
          if(!this.isNullOrUndefinedOrEmpty(infoIcon)){
            infoIcon.isVisible = false;
          } 						   
        }
      }
    },

    /**
     * Component getFieldValue
     * Parse the exposed contract value based on accountType selected and breakpoint consideration
     * @param: Value{string} - value collected from exposed contract
     * @param: key{string} - lookup key in the JSON string
     * @return : {string} - Processed value
     */
    getFieldValue: function (Value, key) {
      try {
        var value = Value;
        if (typeof(Value) === "string") {
          value = JSON.parse(Value);
        }
        if (value[this.context["accountType"]]) {
          value = value[this.context["accountType"]];
        }
        if (value["default"]) {
          value = value["default"];
        }
        if (!this.isEmptyNullUndefined(value) && !this.isEmptyNullUndefined(key)) {
          value = value[key];
        }
        if (value !== null && value !== "" && value !== undefined) {
          if (typeof(value) === "string") 
            return this.getProcessedText(value);
          else {
            return this.getProcessedText(value);
          }

        } else 
          return "";

      } catch (err) {
        kony.print(err);
      }
      return this.getProcessedText(value);
    },

    /**
	* getLabelText
	* @api : getLabelText
	* parses label texts based on contract configurations
	* @return : NA
	*/
    getLabelText: function(contractJSON) {
      let labelText = this.getParsedValue(contractJSON,kony.application.getCurrentBreakpoint());
      return labelText ? labelText : "";
    },

    /**
	* getParsedValue
	* @api : getParsedValue
	* parses the property and fetches the corresponding value
	* @return : NA
	*/
    getParsedValue: function(property, selectedValue) {
      try{
        if(!this.isEmptyNullUndefined(property))
          property = JSON.parse(property);
        else
          return "";
      }
      catch(e){
        property = property;
        kony.print(e);
      }
      if(typeof(property) === "string")
        return this.getProcessedText(property);
      else{
        if(selectedValue)
          return this.getProcessedText(this.parserUtilsManager.getComponentConfigParsedValue(property,selectedValue));
        else
          return property;
      }
    },

    /**
	* @api : getProcessedText
	* helper method to invoke parser utility functions to get the parsed value.
	* @param : text{object} -value collected from exposed contract
	* @return : parsed value result
	*/
    getProcessedText: function(text) {
      return this.parserUtilsManager.getParsedValue(text);
    },

    /**
     * setLabelData
     * @api : setLabelData
     * sets the data in corresponding labels provided in contracts
     * @return : NA
     */
    setLabelTexts: function() {
      this.view.lblFromAccount.text = this.getLabelText(this._lblFromAccount);
      this.view.lblToAccount.text = this.getLabelText(this._lblToAccount);
      this.view.lblTransferCurrency.text = this.getLabelText(this._lblTransferCurrency);
      this.view.lblTransferAmount.text = this.getLabelText(this._lblTransferAmount);
      this.view.lblFXRateReference.text = this.getLabelText(this._lblfxRateReference);
      this.view.lblPaymentMethod.text = this.getLabelText(this._lblPaymentMethod);
      this.view.lblFrequency.text = this.getLabelText(this._lblFrequency);
      this.view.lblTransferDuration.text = this.getLabelText(this._lblFrequencyType);
      if(this.view.flxEndDateField.isVisible === false)
        this.view.lblStartDate.text = this.getLabelText(this._lblDatepicker1);
      else
        this.view.lblStartDate.text = this.getLabelText(this._lblDatepicker2);
      this.view.lblEndDate.text = this.getLabelText(this._lblDatepicker3);
      this.view.lblNumberOfRecurrences.text = this.getLabelText(this._lblRecurrence);
      this.view.lblFeePaid.text = this.getLabelText(this._lblFeesPaid);
      this.view.lblIntermediaryBIC.text = this.getLabelText(this._lblIntermediaryBIC);
      this.view.lblE2E.text = this.getLabelText(this._lblE2E);
      this.view.lblField.text = this.getLabelText(this._lblField);
      this.view.lblNotes.text = this.getLabelText(this._lblNotes);
      this.view.lblPayeeDetailFieldInfo.text = this.getLabelText(this._lblPayeeDetailFieldInfo);
      this.view.lblPayeeDetailField1.text = this.getLabelText(this._lblPayeeDetail1);
      this.view.lblPayeeDetailField2.text = this.getLabelText(this._lblPayeeDetailField2);
      this.view.lblPayeeDetailField3.text = this.getLabelText(this._lblPayeeDetailField3);
      this.view.lblPayeeDetailField4.text = this.getLabelText(this._lblPayeeDetailField4);																								
      this.view.lblNewPayeeAccountNumber.text = this.getLabelText(this._lblNewPayeeAccountNumber);
      this.view.lblNewPayeeReenterAccountNumber.text = this.getLabelText(this._lblReenterAccountNumber);
      this.view.lblPayeeAddress.text = this.getLabelText(this._lblAddPayeeAddress);
      this.view.lblPayeeAddressOptional.text = this.getLabelText(this._lblPayeeAddressOptional);
      this.view.lblAttachDocuments.text = this.getLabelText(this._lblSupportingDocuments);
      this.view.lblAttachDocumentsOptional.text = this.getLabelText(this._lblDocumentsOptionalSuffix);
      //this.view.lblLookup.text = this.getLabelText(this._lblLookup);
      this.view.lblLookup.text = this.getParsedTextValue(this._lblLookup,kony.application.getCurrentBreakpoint());
      if(this.isNullOrUndefinedOrEmpty(this._lblLookup)){
        this.view.lblLookup.setVisibility(false);
      } 
    },

    /**
	* populateButtonTexts
	* @api : populateButtonTexts
	* populates button texts in add beneficiary view
	* @return : NA
	*/
    populateButtonTexts: function() {
      var self = this;
      try{
        var parsedValue1 = this.unifiedTransfersUtility.buttonParsedValue(this._button1);
        if(parsedValue1) {
          var btn1Text = this.setButtonText(parsedValue1);
          this.view.btnCancel.text = btn1Text;
          this.view.btnCancel.setVisibility(true);
        }
        else {
          this.view.btnCancel.setVisibility(false);
        }

        var parsedValue2 = this.unifiedTransfersUtility.buttonParsedValue(this._button2);
        if(parsedValue2) {
          var btn2Text = this.setButtonText(parsedValue2);
          this.view.btnContinue.text = btn2Text;
          this.view.btnContinue.setVisibility(true);
        }
        else {
          this.view.btnContinue.setVisibility(false);
        }
      }
      catch(err) {
        var errObj = {
          "errorInfo" : "Error in populateButtonTexts method of the component.",
          "error": err
        };
        self.onError(errObj); 
      }
    },

    /**
     * resetErrorMessages
     * @api : resetErrorMessages
     * resets error messages
     * @return : NA
     */
    resetErrorMessages: function() {
      this.view.txtErrormessage.text = "";
      this.view.flxErrorWarning.setVisibility(false);
    },

    /**
	* setButtonText
	* @api : setButtonText
	* helper method for parsing button text
	* @return : returns corresponding property for the respective breakpoints
	*/
    setButtonText: function(value) {
      var self = this;
      try {
        var parsedValue = value;
        if (typeof(parsedValue !== "string")) {
          parsedValue = parsedValue.hasOwnProperty("text") ? parsedValue["text"] : parsedValue;
        }
        if ((typeof(parsedValue) !== "string" && Object.keys(parsedValue)[1].indexOf("$.BREAKPTS") > -1) || (typeof(parsedValue) === "string" && parsedValue.indexOf("$.BREAKPTS") > -1)) {
          parsedValue = this.getParsedTextValue(parsedValue, kony.application.getCurrentBreakpoint());
        } else parsedValue = this.getParsedTextValue(parsedValue, kony.application.getCurrentBreakpoint());
        return parsedValue;
      }
      catch(err){
        var errObj = {
          "errorInfo" : "Error in setButtonText method of the component.",
          "error": err
        };
        self.onError(errObj); 
      }
    },

    /**
     * actionHandler
     * @api : actionHandler
     * helper method used on initializing actions for the component, it retrieves the actionJSON from the property
     * context{JSON} - context object
     * property{String} - contains custom property defined
     */
    actionHandler: function(context,property) {
      if(property!==null && property !== undefined) {
        var propertyJSON = JSON.parse(property);
        var parsedValue=propertyJSON;
        if (typeof(parsedValue) !== "string") {
          parsedValue = parsedValue.hasOwnProperty("action") ? parsedValue["action"] : parsedValue;
        }
        var actionJSON = parsedValue;
        var level = actionJSON.level;  
        var method = actionJSON.method;
        this.getInstanceAction(level,method,context);
      }
    },

    /**
     * getInstanceAction
     * @api : getInstanceAction
     * helper method to retrieve the form/component method
     * method {String} - method to be invoked
     * context{JSON} -context
     */
    getInstanceAction: function(levelInstance, method, context) {     
      if(levelInstance.toLowerCase().trim() === "form")
      {  
        this.formScope[method](context);
      }
      if(levelInstance.toLowerCase().trim() === "component")
      {
        this[method](context);
      } 
    },

    /**
     * @api : setContext
     * To collect the context object required for the component. 
     * @param : context{JSONobject} - account object 
     * @return : NA
     */
    setContext: function(context, scope) {
      this.context = context; 
      this.parserUtilsManager.clearContext();
      this.formScope = scope;
	   
      this.parserUtilsManager.setContext(this.context);
      if(this.context.flowType == "add"){
        this.resetWidgetData();
      }
    },
    /**
     * getParsedTextValue
     * @api : getParsedTextValue
     * parses the property and fetches the corresponding text
     * @return : returns corresponding property for the respective breakpoints
     */
    getParsedTextValue: function(property, selectedValue) {
      try {
        property = JSON.parse(property).text;

      } catch (e) {
        property = property;
        kony.print(e);
      }
      if (typeof(property) === "string") return this.getProcessedText(property);
      else return this.parserUtilsManager.getComponentConfigParsedValue(property, selectedValue);
    },

    /**
     * browseSupportingDoc
     * @api : browseSupportingDoc
     * opens browse window to select the supporting document
     * @return : NA
     */
    browseSupportingDoc: function() {
      var scope = this;
      scope.view.flxAttachmentUploadError.setVisibility(false);
      var config = {
        selectMultipleFiles: false,
        filter: []
      };
      kony.io.FileSystem.browse(config, scope.selectedFileCallback);
      scope.count = scope.filesToBeUploaded.length;
    },

    /**
     * selectedFileCallback
     * @api : selectedFileCallback
     * callback event for the selected supporting document
     * @return : file name, type and contents
     */
    selectedFileCallback: function(events, files) {
      // var configManager = applicationManager.getConfigurationManager();
      var scope = this;
      this.view.flxAttachmentUploadError.setVisibility(false);
      var fileFormat = this.getParsedValue(this._supportedFileFormat);
      var fileConfiguration = this.getParsedValue(this._fileConfiguration);
      var maxFileSizeByte = 1000000 * parseInt(fileConfiguration.maxFileSizeMB);
      var fileNameRegex = new RegExp("^[a-zA-Z0-9.]*$");
      var supportedExtension = "";
      if(scope.count=== scope.filesToBeUploaded.length){
        if(files.length > 0){
          var fileName = files[0].file.name;
          var extension = files[0].file.name.split('.');
          var supportedFileFormatList = [];
          var supportedErrorMessage = "";
          if(fileFormat){
            // For loop to Store supported file format in a  variable(To show in UI incase wrong format file choosed)
            supportedFileFormatList = Object.entries(fileFormat);
            for(var i=0;i<Object.keys(fileFormat).length;i++){
              if(supportedFileFormatList[i][1].extension.toLowerCase()===extension[1].toLowerCase()){
                supportedErrorMessage = "";
                break;
              }else{
                supportedErrorMessage = 
                  supportedErrorMessage !=="" ? supportedErrorMessage +",."+supportedFileFormatList[i][1].extension : "."+supportedFileFormatList[i][1].extension ;
              }
            }   
          }
          if (extension.length > 0 && !fileFormat[extension[1].toLowerCase()]) {
            this.view.flxAttachmentUploadError.setVisibility(true);
            this.view.lblAttachmentUploadError.text =  scope.getParsedTextValue(scope._attachmentError1)+" " + files[0].name + " "+this.getParsedTextValue(this._attachmentError2)+""+supportedErrorMessage;
            this.view.flxFields.forceLayout();
            return;
          }
          if (files[0].file.size >= maxFileSizeByte) {
            this.view.flxAttachmentUploadError.setVisibility(true);
            this.view.lblAttachmentUploadError.text = this.getParsedTextValue(this._attachmentError1)+" " + files[0].name + " "+this.getParsedTextValue(this._attachmentError3);
            this.view.flxFields.forceLayout();
            return;
          }
          else if (fileName !== null && !fileNameRegex.test(fileName)) {
            this.view.flxAttachmentUploadError.setVisibility(true);
            this.view.lblAttachmentUploadError.text = this.getParsedTextValue(this._attachmentError4);
            this.view.flxFields.forceLayout();
            return;
          }
          else if (this.filesToBeUploaded.length >= fileConfiguration.maxFileAttached) {
            this.view.flxAttachmentUploadError.setVisibility(true);
            this.view.lblAttachmentUploadError.text = this.getParsedTextValue(this._attachmentError5);
            this.view.flxFields.forceLayout();
            return;
          } else {
            var fileData = {};
            scope.filesToBeUploaded.push([files[0].name,fileFormat[extension[1].toLowerCase()].imgSrc]);
            fileData.fileName = files[0].name;
            fileData.fileType = files[0].file.type;
            scope.getBase64(files[0].file, function(base64String) {
              scope.attachments = [];
              var fileType = files[0].file.type;
              base64String = base64String.replace("data:;base64\,", "");
              base64String = base64String.replace("data:application\/octet-stream;base64\,", "");
              fileData.fileContents = base64String.replace("data:"+fileType+";base64\,","");
              scope.attachments.push(fileData);
              var fileDataItemParsed = scope.attachments.map(function(item) {
                return item['fileName'] + "-" + item['fileType'] + "-" + item['fileContents'];
              });
              scope.uploadedAttachments.push(fileDataItemParsed);
            });
          }
        }
      }
      else
        return;
      if(scope.filesToBeUploaded.length <= fileConfiguration.maxFileAttached) {
        scope.setAttachmentsDataToSegment();
      } 
      scope.view.forceLayout();
    },

    /**
     * getBase64
     * @api : getBase64
     * converts the attched supporting document to base64 format
     * @return : NA
     */
    getBase64: function(file, successCallback) {
      var reader = new FileReader();
      reader.onloadend = function() {
        successCallback(reader.result);
      };
      reader.readAsDataURL(file);
    },

    /**
     * setAttachmentsDataToSegment
     * @api : setAttachmentsDataToSegment
     * sets the details of the documents attached in a segment
     * @return : NA
     */
    setAttachmentsDataToSegment: function() {
      var scope = this;
      this.view.flxFilesList.setVisibility(true);
      var attachmentsData = [];
      for (var i = 0; i < scope.filesToBeUploaded.length; i++) {
        attachmentsData[i] = {};
        var imageName = scope.filesToBeUploaded[i][1];
        attachmentsData[i]["imgFileTypeIcon"] = {
          "src": imageName
        };
        attachmentsData[i].filename = scope.filesToBeUploaded[i][0];
		var file = scope.filesToBeUploaded[i][0];
        var length = "";
		
        if(kony.application.getCurrentBreakpoint() === 640) {
			if(file.length > 24) {
                length = 25;
				attachmentsData[i].truncatedFilename = file.substring(0,length) + '...';
            }
			else{
				attachmentsData[i].truncatedFilename = file;
		}}
        else if(kony.application.getCurrentBreakpoint() === 1366 || kony.application.getCurrentBreakpoint() === 1380) {
			if(file.length > 58) {
                length = 59;
				attachmentsData[i].truncatedFilename = file.substring(0,length) + '...';
            }
			else{
				attachmentsData[i].truncatedFilename = file;
		}}
        else{
			if(attachmentsData[i].filename.length > 64) {
                length = 65;
				attachmentsData[i].truncatedFilename = file.substring(0,length) + '...';
            }
			else{
				attachmentsData[i].truncatedFilename = file;
			}  
        }										 		
        attachmentsData[i]["imgRemoveAttachment"] = {
          "src": scope.Icons.RemoveAttachmentIcon
        };
        attachmentsData[i].removeAction = {"onClick": scope.showRemoveAttachment};
      }
      this.view.segFilesList.widgetDataMap = {
        "imgFileTypeIcon" : "imgFileTypeIcon",
        "lblFileName": "truncatedFilename",
        "imgRemoveFileIcon": "imgRemoveAttachment",
        FlxRemoveIcon:"removeAction"
      };
      scope.view.segFilesList.setData(attachmentsData);
      this.view.forceLayout();
    },

    /**
     * showRemoveAttachment
     * @api : showRemoveAttachment
     * pop up is displayed on click of remove the attached supporting document
     * @return : NA
     */
    showRemoveAttachment : function(){
      var scope = this;
      var form = kony.application.getCurrentForm();
      var popupObj = this.view.flxCancelAttachment.clone("prefix");
      form.add(popupObj);
      popupObj.isVisible = true;
      popupObj.top = "0dp";
      popupObj.left = "0dp";
      popupObj.height = "100%";
      popupObj.prefixflxRemoveAttachment.centerX = "50%";
      popupObj.prefixflxRemoveAttachment.centerY = "50%";
      popupObj.prefixflxRemoveAttachment.prefixbtnNo.onClick = function() {
        form.remove(popupObj);
      }
      popupObj.prefixflxRemoveAttachment.prefixbtnYes.onClick = function() {
        form.remove(popupObj);
        scope.deleteAttachment();
      }
      this.view.forceLayout();
    },

    /**
     * deleteAttachment
     * @api : deleteAttachment
     * deletes the supporting documents attached
     * @return : NA
     */
    deleteAttachment: function() {
      var index = this.view.segFilesList.selectedRowIndex;
      var sectionIndex = index[0];
      var rowIndex = index[1];
      var deletedAttachment = this.view.segFilesList.data[rowIndex];
      this.view.segFilesList.removeAt(rowIndex, sectionIndex);
      this.removeAttachments(deletedAttachment);
    },

    /**
     * removeAttachments
     * @api : removeAttachments
     * removes the supporting documents attached
     * @return : NA
     */
    removeAttachments: function(data) {
      var scope = this;
      for (var i = 0; i < scope.filesToBeUploaded.length; i++) {
        if (scope.filesToBeUploaded[i][0] === data.filename) {
          scope.filesToBeUploaded.splice(i, 1);
          scope.attachments.splice(i,1);
          scope.uploadedAttachments.splice(i,1);
          break;
        } 
      }
      this.view.flxAttachmentUploadError.setVisibility(false);
      scope.setAttachmentsDataToSegment();
    },

    /**
     * storeIconValues
     * @api : storeIconValues
     * stores all the images provided in contracts in specific variables
     * @return : NA
     */
    storeIconValues: function(){
      var self = this;
      try{
        this.Icons.DropdownExpand = this.getParsedImageValue(this._dropdownExpandIcon);
        this.Icons.DropdownCollapse = this.getParsedImageValue(this._dropdownCollapseIcon);
        this.Icons.CloseIcon = this.getParsedImageValue(this._closeIcon);
        this.Icons.AttachDocuments = this.getParsedImageValue(this._attachDocumentsIcon);
        this.Icons.AddAddress = this.getParsedImageValue(this._payeeAddressIcon);
        this.Icons.InfoIcon = this.getParsedImageValue(this._infoIcon);
        this.Icons.PDFIcon = this.getParsedImageValue(this._pdfIcon);
        this.Icons.JPEGIcon = this.getParsedImageValue(this._jpegIcon);
        this.Icons.RemoveAttachmentIcon = this.getParsedImageValue(this._removeAttachDocumentIcon);
        this.Icons.selectedRadioButton = this.getParsedValue(this._radioButtonAction).selectedRadioButton;     
        this.Icons.unSelectedRadioButton = this.getParsedValue(this._radioButtonAction).unSelectedRadioButton;      
        this.Icons.UploadDocumentErrorIcon = this.getParsedImageValue(this._uploadDocumentErrorIcon);
        this.Icons.InfoClose = this.getParsedImageValue(this._closeIcon);
        this.Icons.PayeeInfoIcon = this.getParsedImageValue(this._payeeInfoIcon);
      }
      catch(err)
      {
        var errorObj =
            {
              "errorInfo" : "Error in setting the parsed icon value",
              "errorLevel" : "Business",
              "error": err
            };
        self.onError(errorObj);
      }
    },

    /**
     * getParsedImageValue
     * @api : getParsedImageValue
     * parses the property and fetches the corresponding Value
     * @return : NA
     */
    getParsedImageValue: function(contractJSON) {
      var self = this;
      try{
        contractJSON = JSON.parse(contractJSON );
        if(contractJSON.hasOwnProperty("img")){
          contractJSON = contractJSON["img"];
        }
      }
      catch(err) {
        var errorObj =
            {
              "errorInfo" : "Error in getting the parsed icon value",
              "errorLevel" : "Business",
              "error": err
            };
      }
      return contractJSON;
    },

    /**
     * onClickeExistingPayeeRadioButton
     * @api : onCliconClickeExistingPayeeRadioButtonkNewPayeeRadioButton
     * formats UI on click of existing payee radio button
     * @return : NA
     */
    onClickeExistingPayeeRadioButton: function (flag) {
      this.resetErrors();
      this.view.lblPayeeType1Option.text = this.Icons.selectedRadioButton;
      this.view.lblPayeeType1Option.skin = this.Skins.selectedRadioButtonSkin;
      this.view.lblPayeeType2Option.text = this.Icons.unSelectedRadioButton;
      this.view.lblPayeeType2Option.skin = this.Skins.unseletedRadioBtnSkn;  
      this.view.flxNewPayeeDetails.setVisibility(false);
      this.view.toAccount.setVisibility(true);
      this.view.lblLookup.setVisibility(false);
      this.payeeTypeContext = this.getFieldValue(this._PAYEETYPES, "PT2");
      if(this.context.flowType == "edit"){
      this.setRetrievedAddressData();
      }else if(flag != false){
        this.resetNewPayeeAddressData();
      }
      this.setPayeeDetailFieldProps(flag);
      this.view.txtBoxPayeeName.text = "";
      this.view.txtBoxReenterAccountNumber.text = "";
      this.view.txtBoxNewAccountNumber.text = "";
      this.updateContext("txtBoxPayeeName",this.view.txtBoxPayeeName.text);
      this.updateContext("txtBoxReenterAccountNumber",this.view.txtBoxReenterAccountNumber.text);
      this.updateContext("txtBoxNewAccountNumber",this.view.txtBoxNewAccountNumber.text);
      if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T1")) {
        if(this.toAccount.accountType == this.getFieldValue(this._ACCOUNTTYPES, "AT1") || this.toAccount.accountType == this.getFieldValue(this._ACCOUNTTYPES, "AT2"))
        {
          this.setLoanCreditCardFieldValue(this.toAccount.accountType);
          this.frequency = "Once";
        }
      }
      if(this._currencyListSrc == "From and To Account"){
        this.setTransferCurrencyFieldFromAccounts(true);
      }
      this.minFillValidation();
	  this.setPayeeDetailsFlex();
      this.changePayeeDetailFieldsVisibility();
      if(this.existingPayeeAddressLine01 ||  this.existingPayeeAddressLine02 || 
         this.existingPayeeCity || this.existingPayeeState || this.existingPayeeCountry || 
         this.existingPayeeZipCode || this.existingPayeePhoneNumber || this.existingPayeeEmailAddress) {
		if(this.transferTypeContext !== this.getFieldValue(this._transferFlow, "T4"))
        { this.view.flxPayeeAddress.isVisible = true; }
      }
      else {
        this.view.flxPayeeAddress.isVisible = false;
      }
    },

    /**
     * onClickNewPayeeRadioButton
     * @api : onClickNewPayeeRadioButton
     * formats UI on click of new payee radio button
     * @return : NA
     */
    onClickNewPayeeRadioButton: function (flag) {
      this.resetErrors();
      this.transferCurrency = "";
      this.view.lblSelectedTransferCCY.text = "";
      this.view.lblPayeeType1Option.text = this.Icons.unSelectedRadioButton;
      this.view.lblPayeeType1Option.skin = this.Skins.unseletedRadioBtnSkn;
      this.view.lblPayeeType2Option.text = this.Icons.selectedRadioButton;
      this.view.lblPayeeType2Option.skin = this.Skins.selectedRadioButtonSkin;
      this.view.flxPayeeAddress.isVisible = true; 
      this.view.toAccount.setVisibility(false);
      this.view.lblLookup.setVisibility(true);
      if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T4"))
        this.view.flxNewPayeeDetails.setVisibility(false);
      else
        this.view.flxNewPayeeDetails.setVisibility(true); 
      this.payeeTypeContext = this.getFieldValue(this._PAYEETYPES, "PT1");
      if(this.context.flowType == "edit"){
        this.setNewPayeeAddressData();
      }else if(flag != false){
        this.resetExistingPayeeAddressData();
      }  	
      this.view.flxPaymentAmountField.setVisibility(false);
      this.view.flxTransferCurrencyAmount.setVisibility(true);
      this.view.flxFrequencyOptions.setVisibility(true);
      this.view.flxEndDateandRecurrences.setVisibility(false);
      //this.view.flxEndDateField.setVisibility(true);
      this.view.flxRecurrencesField.setVisibility(false);
      this.view.flxDueDate.setVisibility(false);
      this.setPayeeDetailFieldProps(flag);							   						
      if(this._currencyListSrc == "From and To Account"){
        this.setTransferCurrencyFieldFromAccounts(false);
      }
      this.minFillValidation();
      this.view.toAccount.resetSelectedValue();
      this.toAccount = "";	  
      this.setPayeeDetailsFlex();					   
    },

    /**
     * setPayeeDetailFieldProps
     * @api : setPayeeDetailFieldProps
     * set properties of payee detail fields
     * @return : NA
     */
    setPayeeDetailFieldProps: function(flag) {
      var scope = this;
      var txtboxSkin = this.Skins.TxtBoxField;
      this.view.txtBoxPayeeDetailField1.text = "";
      this.view.txtBoxPayeeDetailField2.text = "";
      this.view.txtBoxPayeeDetailField3.text = "";
      this.view.txtBoxPayeeDetailField4.text = "";
      var txtboxDisabledSkin = this.getParsedValue(this._sknTextBoxDisabled);      scope.view.flxPayeeDetailFieldRow1.setVisibility(true);
      scope.view.flxPayeeDetailFieldRow2.setVisibility(true);
      scope.view.flxPayeeDetailFields.setVisibility(true);
      scope.view.flxField1.setVisibility(true);
      scope.view.flxField2.setVisibility(true);
      scope.view.flxField3.setVisibility(true);
      scope.view.flxField4.setVisibility(true);														 
      if(kony.application.getCurrentBreakpoint()!= 640) {
        if(this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT1")) {
          this.view.flxPayeeDetailFields.skin = "ICSknFlxf9f9f9";
          this.view.flxPayeeDetailFieldInfo.setVisibility(true);
          this.view.txtBoxPayeeDetailField1.setEnabled(true);
          this.view.txtBoxPayeeDetailField2.setEnabled(true);
          this.view.txtBoxPayeeDetailField3.setEnabled(true);
          this.view.txtBoxPayeeDetailField4.setEnabled(true);
          this.view.txtBoxPayeeDetailField1.skin = txtboxSkin;
          this.view.txtBoxPayeeDetailField2.skin = txtboxSkin;
          this.view.txtBoxPayeeDetailField3.skin = txtboxSkin;
          this.view.txtBoxPayeeDetailField4.skin = txtboxSkin;					   
          this.view.lblLookup.setVisibility(true);
          this.view.flxField1.left = "20dp";
          this.view.flxField1.width = "47%";
          this.view.flxField2.width = "47%";
          this.view.flxField3.left = "20dp";
          this.view.flxField3.width = "47%";
          this.view.flxField4.width = "47%";
          this.view.flxField2.left = "1.8%";
          this.view.flxField4.left = "1.8%";
          if(this.context.flowType == "edit" && flag == false){
            this.view.txtBoxPayeeDetailField1.text = scope.newPayeeData1;
            this.view.txtBoxPayeeDetailField2.text = scope.newPayeeData2;
            this.view.txtBoxPayeeDetailField3.text = scope.newPayeeData3;
            this.view.txtBoxPayeeDetailField4.text = scope.newPayeeData4;
          }
          this.view.flxSpace.setVisibility(true);									 
        }
        else {
          this.view.flxPayeeDetailFields.skin = "slFbox";
          this.view.flxPayeeDetailFieldInfo.setVisibility(false);

          this.view.txtBoxPayeeDetailField1.setEnabled(false);
          this.view.txtBoxPayeeDetailField1.skin = txtboxDisabledSkin;

          this.view.txtBoxPayeeDetailField2.setEnabled(false);
          this.view.txtBoxPayeeDetailField2.skin = txtboxDisabledSkin;

          this.view.txtBoxPayeeDetailField3.setEnabled(false);
          this.view.txtBoxPayeeDetailField3.skin = txtboxDisabledSkin;

          this.view.txtBoxPayeeDetailField4.setEnabled(false);
          this.view.txtBoxPayeeDetailField4.skin = txtboxDisabledSkin;

          this.view.lblLookup.setVisibility(false);
          this.view.flxField1.left = "0dp";
          this.view.flxField3.left = "0dp";
          this.view.flxField1.width = "49.1%";
          this.view.flxField2.width = "49.1%";
          this.view.flxField3.width = "49.1%";
          this.view.flxField4.width = "49.1%";
          if(this.context.flowType == "edit" && flag == false){
            this.view.txtBoxPayeeDetailField1.text = scope.existingPayeeData1;
            this.view.txtBoxPayeeDetailField2.text = scope.existingPayeeData2;
            this.view.txtBoxPayeeDetailField3.text = scope.existingPayeeData3;
            this.view.txtBoxPayeeDetailField4.text = scope.existingPayeeData4;
          }
          this.view.flxSpace.setVisibility(false);									  
        }
      }
      else {
        if(this.payeeTypeContext === this.getFieldValue(this._PAYEETYPES, "PT1")) {
          this.view.flxPayeeDetailFields.skin = "ICSknFlxf9f9f9";
          this.view.flxPayeeDetailFieldInfo.setVisibility(true);
          this.view.txtBoxPayeeDetailField1.setEnabled(true);
          this.view.txtBoxPayeeDetailField2.setEnabled(true);
          this.view.txtBoxPayeeDetailField3.setEnabled(true);
          this.view.txtBoxPayeeDetailField4.setEnabled(true);
          this.view.txtBoxPayeeDetailField1.skin = txtboxSkin;
          this.view.txtBoxPayeeDetailField2.skin = txtboxSkin;
          this.view.txtBoxPayeeDetailField3.skin = txtboxSkin;
          this.view.txtBoxPayeeDetailField4.skin = txtboxSkin;						 
          this.view.lblLookup.setVisibility(true);
          this.view.flxField1.left = "10dp";
          this.view.flxField2.left = "10dp";
          this.view.flxField3.left = "10dp";
          this.view.flxField4.left = "10dp";
          this.view.flxField1.width = "97%";
          this.view.flxField2.width = "97%";
          this.view.flxField3.width = "97%";
          this.view.flxField4.width = "97%";
          this.view.flxPayeeDetailFieldRow1.height = "150dp";
          this.view.flxPayeeDetailFieldRow2.height = "150dp";
          if(this.context.flowType == "edit" && flag == false){
            this.view.txtBoxPayeeDetailField1.text = scope.newPayeeData1;
            this.view.txtBoxPayeeDetailField2.text = scope.newPayeeData2;
            this.view.txtBoxPayeeDetailField3.text = scope.newPayeeData3;
            this.view.txtBoxPayeeDetailField4.text = scope.newPayeeData4;
          }
          this.view.flxSpace.setVisibility(true);										 
        }
        else {
          this.view.flxPayeeDetailFields.skin = "slFbox";
          this.view.flxPayeeDetailFieldInfo.setVisibility(false);
          this.view.txtBoxPayeeDetailField1.setEnabled(false);
          this.view.txtBoxPayeeDetailField2.setEnabled(false);
          this.view.txtBoxPayeeDetailField3.setEnabled(false);
          this.view.txtBoxPayeeDetailField4.setEnabled(false);
          this.view.txtBoxPayeeDetailField1.skin = txtboxDisabledSkin;
          this.view.txtBoxPayeeDetailField2.skin = txtboxDisabledSkin;
          this.view.txtBoxPayeeDetailField3.skin = txtboxDisabledSkin;
          this.view.txtBoxPayeeDetailField4.skin = txtboxDisabledSkin;

          this.view.lblLookup.setVisibility(false);
          this.view.flxField1.left = "0dp";
          this.view.flxField2.left = "0dp";
          this.view.flxField3.left = "0dp";
          this.view.flxField4.left = "0dp";
          this.view.flxField1.width = "100%";
          this.view.flxField2.width = "100%";
          this.view.flxField3.width = "100%";
          this.view.flxField4.width = "100%";
          if(this.context.flowType == "edit" && flag == false){
            this.view.txtBoxPayeeDetailField1.text = scope.existingPayeeData1;
            this.view.txtBoxPayeeDetailField2.text = scope.existingPayeeData2;
            this.view.txtBoxPayeeDetailField3.text = scope.existingPayeeData3;
            this.view.txtBoxPayeeDetailField4.text = scope.existingPayeeData4;
          }
          this.view.flxSpace.setVisibility(false);									  
        }       
      }
      this.view.forceLayout();
    },

    /**
     * onClickSEPARadioButton
     * @api : onClickInstantRadioButton
     * formats payment method options on click of sepa radio button
     * @return : NA
     */
   onClickSEPARadioButton: function () {
      this.view.lblPaymentMethodOption1.text = this.Icons.selectedRadioButton;
      this.view.lblPaymentMethodOption1.skin = this.Skins.selectedRadioButtonSkin;
      if(!(this.view.lblPaymentMethodOption2.skin === this.Skins.disabledRadioBtnSkn)){
        this.view.lblPaymentMethodOption2.text = this.Icons.unSelectedRadioButton;
        this.view.lblPaymentMethodOption2.skin = this.Skins.unseletedRadioBtnSkn;
      }
      if(!(this.view.lblPaymentMethodOption3.skin === this.Skins.disabledRadioBtnSkn)){
        this.view.lblPaymentMethodOption3.text = this.Icons.unSelectedRadioButton; 
        this.view.lblPaymentMethodOption3.skin = this.Skins.unseletedRadioBtnSkn; 
      }
      if(!(this.view.lblPaymentMethodOption4.skin === this.Skins.disabledRadioBtnSkn)){
        this.view.lblPaymentMethodOption4.text = this.Icons.unSelectedRadioButton;
        this.view.lblPaymentMethodOption4.skin = this.Skins.unseletedRadioBtnSkn;      
      }
      this.paymentMethod = 'SEPA';
      this.view.flxFeePaidType1.setEnabled(false);
        this.view.lblFeePaidOption1.text = this.Icons.unSelectedRadioButton;
      this.view.lblFeePaidOption1.skin = this.Skins.disabledRadioBtnSkn;
      this.view.flxFeePaidType2.setEnabled(false);
        this.view.lblFeePaidOption2.text = this.Icons.unSelectedRadioButton;
      this.view.lblFeePaidOption2.skin = this.Skins.disabledRadioBtnSkn;
      this.onClickSharedRadioButton();
      this.view.flxE2EField.setVisibility(true);
    },

    /**
     * onClickInstantRadioButton
     * @api : onClickInstantRadioButton
     * formats payment method options on click of instant radio button
     * @return : NA
     */
    onClickInstantRadioButton: function () {
      if(!(this.view.lblPaymentMethodOption1.skin === this.Skins.disabledRadioBtnSkn)){
        this.view.lblPaymentMethodOption1.text = this.Icons.unSelectedRadioButton;
        this.view.lblPaymentMethodOption1.skin = this.Skins.unseletedRadioBtnSkn;
      }
      this.view.lblPaymentMethodOption2.text = this.Icons.selectedRadioButton;
      this.view.lblPaymentMethodOption2.skin = this.Skins.selectedRadioButtonSkin;
      if(!(this.view.lblPaymentMethodOption3.skin === this.Skins.disabledRadioBtnSkn)){
        this.view.lblPaymentMethodOption3.text = this.Icons.unSelectedRadioButton;
        this.view.lblPaymentMethodOption3.skin = this.Skins.unseletedRadioBtnSkn; 
      }
      if(!(this.view.lblPaymentMethodOption4.skin === this.Skins.disabledRadioBtnSkn)){
        this.view.lblPaymentMethodOption4.text = this.Icons.unSelectedRadioButton;
        this.view.lblPaymentMethodOption4.skin = this.Skins.unseletedRadioBtnSkn; 
      }
      this.paymentMethod = 'Instant';
      this.view.flxFeePaidType1.setEnabled(false);
        this.view.lblFeePaidOption1.text = this.Icons.unSelectedRadioButton;
      this.view.lblFeePaidOption1.skin = this.Skins.disabledRadioBtnSkn;
      this.view.flxFeePaidType2.setEnabled(false);
        this.view.lblFeePaidOption2.text = this.Icons.unSelectedRadioButton;
      this.view.lblFeePaidOption2.skin = this.Skins.disabledRadioBtnSkn;
      this.onClickSharedRadioButton();
      this.view.flxE2EField.setVisibility(false);
    },

    /**
     * onClickSWIFTRadioButton
     * @api : onClickSWIFTRadioButton
     * formats payment method options on click of Swift radio button
     * @return : NA
     */
    onClickSWIFTRadioButton: function () {
      if(!(this.view.lblPaymentMethodOption1.skin === this.Skins.disabledRadioBtnSkn)){
        this.view.lblPaymentMethodOption1.text = this.Icons.unSelectedRadioButton;
        this.view.lblPaymentMethodOption1.skin = this.Skins.unseletedRadioBtnSkn;
      }
      if(!(this.view.lblPaymentMethodOption2.skin === this.Skins.disabledRadioBtnSkn)){
        this.view.lblPaymentMethodOption2.text = this.Icons.unSelectedRadioButton;
        this.view.lblPaymentMethodOption2.skin = this.Skins.unseletedRadioBtnSkn;
      }
      this.view.lblPaymentMethodOption3.text = this.Icons.selectedRadioButton;
      this.view.lblPaymentMethodOption3.skin = this.Skins.selectedRadioButtonSkin; 
      if(!(this.view.lblPaymentMethodOption4.skin === this.Skins.disabledRadioBtnSkn)){
        this.view.lblPaymentMethodOption4.text = this.Icons.unSelectedRadioButton;
        this.view.lblPaymentMethodOption4.skin = this.Skins.unseletedRadioBtnSkn;
      }
      if(this.view.lblPaymentMethod3.text === "RINGS") {
        this.paymentMethod = 'RINGS';
        this.view.flxFeePaidType1.setEnabled(true);
      this.view.lblFeePaidOption1.skin = this.Skins.unseletedRadioBtnSkn;
        this.view.flxFeePaidType2.setEnabled(true);
      this.view.lblFeePaidOption2.skin = this.Skins.unseletedRadioBtnSkn;
        this.onClickSharedRadioButton();
      } else {
        this.paymentMethod = 'SWIFT'; 
        this.view.flxFeePaidType1.setEnabled(true);
      this.view.lblFeePaidOption1.skin = this.Skins.unseletedRadioBtnSkn;
        this.view.flxFeePaidType2.setEnabled(true);
      this.view.lblFeePaidOption2.skin = this.Skins.unseletedRadioBtnSkn;
        this.onClickSharedRadioButton();
      } 
      this.view.flxE2EField.setVisibility(false);
    },

    /**
     * onClickCCYClearingRadioButton
     * @api : onClickCCYClearingRadioButton
     * formats payment method options on click of CCY Clearing radio button
     * @return : NA
     */
    onClickCCYClearingRadioButton: function () {
      if(!(this.view.lblPaymentMethodOption1.skin === this.Skins.disabledRadioBtnSkn)){
        this.view.lblPaymentMethodOption1.text = this.Icons.unSelectedRadioButton;
        this.view.lblPaymentMethodOption1.skin = this.Skins.unseletedRadioBtnSkn;
      }
      if(!(this.view.lblPaymentMethodOption2.skin === this.Skins.disabledRadioBtnSkn)){
        this.view.lblPaymentMethodOption2.text = this.Icons.unSelectedRadioButton;
        this.view.lblPaymentMethodOption2.skin = this.Skins.unseletedRadioBtnSkn;
      }
      if(!(this.view.lblPaymentMethodOption3.skin === this.Skins.disabledRadioBtnSkn)){
        this.view.lblPaymentMethodOption3.text = this.Icons.unSelectedRadioButton;
        this.view.lblPaymentMethodOption3.skin = this.Skins.unseletedRadioBtnSkn; 
      }
      this.view.lblPaymentMethodOption4.text = this.Icons.selectedRadioButton;
      this.view.lblPaymentMethodOption4.skin = this.Skins.selectedRadioButtonSkin; 
      if(this.view.lblPaymentMethod4.text === "BISERA") {
        this.paymentMethod = 'BISERA';
        this.view.flxFeePaidType1.setEnabled(true);
      this.view.lblFeePaidOption1.skin = this.Skins.unseletedRadioBtnSkn;
        this.view.flxFeePaidType2.setEnabled(true);
      this.view.lblFeePaidOption2.skin = this.Skins.unseletedRadioBtnSkn;
      } else {
        this.paymentMethod = 'Local CCY Clearing';
        this.view.flxFeePaidType1.setEnabled(true);
      this.view.lblFeePaidOption1.skin = this.Skins.unseletedRadioBtnSkn;
        this.view.flxFeePaidType2.setEnabled(true);
      this.view.lblFeePaidOption2.skin = this.Skins.unseletedRadioBtnSkn;
      }
      this.view.flxE2EField.setVisibility(false);
    },


    /**
     * setTransferAmountfromDueAmount
     * @api : setTransferAmountfromDueAmount
     * sets transfer amount from due amount
     * @return : NA
     */
    setTransferAmountfromDueAmount: function(paymentAmount,dueType,format) {
      var transferAmount;
      if(format){
        transferAmount = paymentAmount.substring(1,paymentAmount.length);
      }else{
        transferAmount = paymentAmount;
      }                                                      
      this.dueType = dueType;
      this.dueAmount = transferAmount;                             
    },

    /**
     * onClickMeRadioButton
     * @api : onClickMeRadioButton
     * formats fees paid by options on click of me radio button
     * @return : NA
     */
    onClickMeRadioButton: function () {
      this.view.lblFeePaidOption1.text = this.Icons.selectedRadioButton;
      this.view.lblFeePaidOption1.skin = this.Skins.selectedRadioButtonSkin;
      if(!(this.view.lblFeePaidOption2.skin === this.Skins.disabledRadioBtnSkn)){
      this.view.lblFeePaidOption2.text = this.Icons.unSelectedRadioButton;
      this.view.lblFeePaidOption2.skin = this.Skins.unseletedRadioBtnSkn;
      }
      if(!(this.view.lblFeePaidOption3.skin === this.Skins.disabledRadioBtnSkn)){
      this.view.lblFeePaidOption3.text = this.Icons.unSelectedRadioButton;
      this.view.lblFeePaidOption3.skin = this.Skins.unseletedRadioBtnSkn;
      }
      this.isPaidBy = 'OUR';
    },

    /**
     * onClickBeneficiaryRadioButton
     * @api : onClickBeneficiaryRadioButton
     * formats fees paid by options on click of beneficiary radio button
     * @return : NA
     */
    onClickBeneficiaryRadioButton: function () {      
      if(!(this.view.lblFeePaidOption1.skin === this.Skins.disabledRadioBtnSkn)){
      this.view.lblFeePaidOption1.text = this.Icons.unSelectedRadioButton;
      this.view.lblFeePaidOption1.skin = this.Skins.unseletedRadioBtnSkn;
      }
      this.view.lblFeePaidOption2.text = this.Icons.selectedRadioButton;
      this.view.lblFeePaidOption2.skin = this.Skins.selectedRadioButtonSkin;
      
      if(!(this.view.lblFeePaidOption3.skin === this.Skins.disabledRadioBtnSkn)){
      this.view.lblFeePaidOption3.text = this.Icons.unSelectedRadioButton;
      this.view.lblFeePaidOption3.skin = this.Skins.unseletedRadioBtnSkn; 
      }
      this.isPaidBy = 'BEN';
    },

    /**
     * onClickSharedRadioButton
     * @api : onClickOutStandingDueRadioButton
     * formats fees paid by options on click of both radio button
     * @return : NA
     */
    onClickSharedRadioButton: function () {      
      if(!(this.view.lblFeePaidOption1.skin === this.Skins.disabledRadioBtnSkn)){
      this.view.lblFeePaidOption1.text = this.Icons.unSelectedRadioButton;
      this.view.lblFeePaidOption1.skin = this.Skins.unseletedRadioBtnSkn;
      }
      
      if(!(this.view.lblFeePaidOption2.skin === this.Skins.disabledRadioBtnSkn)){
      this.view.lblFeePaidOption2.text = this.Icons.unSelectedRadioButton;
      this.view.lblFeePaidOption2.skin = this.Skins.unseletedRadioBtnSkn;
      }
      this.view.lblFeePaidOption3.text = this.Icons.selectedRadioButton;
      this.view.lblFeePaidOption3.skin = this.Skins.selectedRadioButtonSkin; 
      this.isPaidBy = 'SHA';
    },

    /**
     * onClickOutStandingDueRadioButton
     * @api : onClickOutStandingDueRadioButton
     * formats payment amount options on click of outstanding due radio button
     * @return : NA
     */
    onClickOutStandingDueRadioButton: function () {
      this.view.txtBoxAmountOthers.setEnabled(false);
      this.view.txtBoxAmountOthers.text = "";
      this.view.lblPaymentAmountOption1.text = this.Icons.selectedRadioButton;
      this.view.lblPaymentAmountOption1.skin = this.Skins.selectedRadioButtonSkin;
      this.view.lblPaymentAmountOption2.text = this.Icons.unSelectedRadioButton;
      this.view.lblPaymentAmountOption2.skin = this.Skins.unseletedRadioBtnSkn;
      this.view.lblPaymentAmountOption3.text = this.Icons.unSelectedRadioButton;
      this.view.lblPaymentAmountOption3.skin = this.Skins.unseletedRadioBtnSkn; 
      this.view.lblPaymentAmountOption4.text = this.Icons.unSelectedRadioButton;
      this.view.lblPaymentAmountOption4.skin = this.Skins.unseletedRadioBtnSkn;
      this.dueTypes = 'Outstanding Due';
      this.setTransferAmountfromDueAmount(this.view.lblPaymentAmount1Value.text,this.view.lblPaymentAmount1.text,true);  },

    /**
     * onClickStatementDueRadioButton
     * @api : onClickStatementDueRadioButton
     * formats payment amount options on click of statement due radio button
     * @return : NA
     */
    onClickStatementDueRadioButton: function () {
      this.view.txtBoxAmountOthers.setEnabled(false);
      this.view.txtBoxAmountOthers.text = "";
      this.view.lblPaymentAmountOption1.text = this.Icons.unSelectedRadioButton;
      this.view.lblPaymentAmountOption1.skin = this.Skins.unseletedRadioBtnSkn;
      this.view.lblPaymentAmountOption2.text = this.Icons.selectedRadioButton;
      this.view.lblPaymentAmountOption2.skin = this.Skins.selectedRadioButtonSkin;
      this.view.lblPaymentAmountOption3.text = this.Icons.unSelectedRadioButton;
      this.view.lblPaymentAmountOption3.skin = this.Skins.unseletedRadioBtnSkn; 
      this.view.lblPaymentAmountOption4.text = this.Icons.unSelectedRadioButton;
      this.view.lblPaymentAmountOption4.skin = this.Skins.unseletedRadioBtnSkn;
      this.dueTypes = 'Statement Due';
      this.setTransferAmountfromDueAmount(this.view.lblPaymentAmount2Value.text,this.view.lblPaymentAmount2.text,true);				 
    },

    /**
     * onClickMinimumDueRadioButton
     * @api : onClickMinimumDueRadioButton
     * formats payment amount options on click of minimum due radio button
     * @return : NA
     */
    onClickMinimumDueRadioButton: function () {
      this.view.txtBoxAmountOthers.setEnabled(false);
      this.view.txtBoxAmountOthers.text = "";
      this.view.lblPaymentAmountOption1.text = this.Icons.unSelectedRadioButton;
      this.view.lblPaymentAmountOption1.skin = this.Skins.unseletedRadioBtnSkn;
      this.view.lblPaymentAmountOption2.text = this.Icons.unSelectedRadioButton;
      this.view.lblPaymentAmountOption2.skin = this.Skins.unseletedRadioBtnSkn;
      this.view.lblPaymentAmountOption3.text = this.Icons.selectedRadioButton;
      this.view.lblPaymentAmountOption3.skin = this.Skins.selectedRadioButtonSkin; 
      this.view.lblPaymentAmountOption4.text = this.Icons.unSelectedRadioButton;
      this.view.lblPaymentAmountOption4.skin = this.Skins.unseletedRadioBtnSkn;
      this.dueTypes = 'Minimum Due';
      this.setTransferAmountfromDueAmount(this.view.lblPaymentAmount3Value.text,this.view.lblPaymentAmount3.text,true);												   
    },

    /**
     * onClickOtherAmountRadioButton
     * @api : onClickOtherAmountRadioButton
     * formats payment amount options on click of other amount radio button
     * @return : NA
     */
    onClickOtherAmountRadioButton: function () {
      this.view.txtBoxAmountOthers.setEnabled(true);
      this.view.lblPaymentAmountOption1.text = this.Icons.unSelectedRadioButton;
      this.view.lblPaymentAmountOption1.skin = this.Skins.unseletedRadioBtnSkn;
      this.view.lblPaymentAmountOption2.text = this.Icons.unSelectedRadioButton;
      this.view.lblPaymentAmountOption2.skin = this.Skins.unseletedRadioBtnSkn;
      this.view.lblPaymentAmountOption3.text = this.Icons.unSelectedRadioButton;
      this.view.lblPaymentAmountOption3.skin = this.Skins.unseletedRadioBtnSkn; 
      this.view.lblPaymentAmountOption4.text = this.Icons.selectedRadioButton;
      this.view.lblPaymentAmountOption4.skin = this.Skins.selectedRadioButtonSkin;
      this.dueTypes = 'Other Amount';
      this.setTransferAmountfromDueAmount(this.view.txtBoxAmountOthers.text,this.view.lblPaymentAmount4.text,false);	
      this.minFillValidation();
    },

    /**
     * setDateContracts
     * @api : setDateContracts
     * formats the date in code level as per the format provided in contracts
     * @return : NA
     */
    setDateContracts: function(calWidget, contractJSON) {
      var scope = this;
      var backendFormat = this._backendDateFormat;
      contractJSON = JSON.parse(contractJSON);
      var today = new Date();
      var bankDate = scope.getParsedValue(contractJSON.defaultDate);
      var startDate;
      if(!this.isNullOrUndefinedOrEmpty(bankDate)){
        startDate = bankDate;
        startDate = scope.formatUtil.getDateObjectFromCalendarString(startDate,backendFormat);
      }else{
        startDate = today;
      }
      this.todayDate = startDate;			   
      var placeHolderValue = this.getParsedValue(contractJSON.placeHolder,kony.application.getCurrentBreakpoint());
      calWidget.placeholder = placeHolderValue ? placeHolderValue : "";
      this.disableOldDaySelection(calWidget,startDate,contractJSON.restrictDays);


    },

    /**
     * getBankDate
     * @api : getBankDate
     * API to get the date provided by bank
     * @return : NA
     */
    getBankDate: function() {
      var self = this;
      var objSvcName = this.getParsedValue(this._bankDateServiceName);
      var objName = this.getParsedValue(this._bankDateObjectName);
      var operationName = this.getParsedValue(this._bankDateOperationName);
      var criteria = "{}";
      var identifier = this.getParsedValue(this._bankDateServiceResponseIdentifier);
      this.unifiedTransferDAO.getBankDate
      (objSvcName,objName,operationName,criteria,this.setBankDatefromAPI,identifier,self.onError);
    },

    /**
     * setBankDatefromAPI
     * @api : setBankDatefromAPI
     * enables the date provided by the bank to be configured in date picker
     * @return : NA
     */
    setBankDatefromAPI: function(res, unicode) {
      var bankObj = res.date[0];
      this.parserUtilsManager.setResponseData(unicode,bankObj);
      this.setDateContracts(this.view.calStartDate, this._datepicker1Value);  
      this.dataContext["frequencyStartDate"] = this.getBackendDateFormat(this.view.calStartDate.formattedDate,this.view.calStartDate.dateFormat);
      var context1 = {
        "widget": this.view.flxStartDatePicker,
        "anchor": "bottom"
      };
      this.view.calStartDate.setContext(context1);
      this.setDateContracts(this.view.calEndDate, this._datepicker2Value); 
      var context2 = {
        "widget": this.view.flxEndDateDatepicker,
        "anchor": "bottom"
      };
      this.view.calEndDate.setContext(context2);
    },

    /**
     * disableOldDaySelection
     * @api : disableOldDaySelection
     * enables the spefic range of date provided in contract
     * @return : NA
     */
    disableOldDaySelection: function(widgetId, startDate, restrictdays) {
      var today = new Date(startDate);
      var futureDate = new Date(startDate);
      futureDate.setDate(today.getDate() + restrictdays);
      widgetId.enableRangeOfDates([today.getDate(), today.getMonth() + 1, today.getFullYear()], [futureDate.getDate(), futureDate.getMonth() + 1, futureDate.getFullYear()], "ICSknDisabledDates", true);
      widgetId.dateComponents = [today.getDate(), today.getMonth() + 1, today.getFullYear()];
    },

    /**
     * phoneNumberVisibilityforP2P
     * @api : phoneNumberVisibilityforP2P
     * greys out email id field in P2P if mobile number is filled
     * @return : NA
     */
    phoneNumberVisibilityforP2P: function() {
      if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T4")) {
        var phoneNumber = this.view.txtBoxPayeeDetailField1.text;
        var email = this.view.txtBoxPayeeDetailField2.text;
        if(phoneNumber) {
          this.view.txtBoxPayeeDetailField2.setEnabled(false);
          this.view.txtBoxPayeeDetailField2.skin = this.getParsedValue(this._sknTextBoxDisabled);
        }
        else {
          this.view.txtBoxPayeeDetailField2.setEnabled(true);
          this.view.txtBoxPayeeDetailField2.skin = this.Skins.TxtBoxField;
        }
      }
    },

    /**
     * emailVisibilityforP2P
     * @api : emailVisibilityforP2P
     * greys out mobile number field in P2P if email id is filled
     * @return : NA
     */
    emailVisibilityforP2P: function() {
      if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T4")) {
        var phoneNumber = this.view.txtBoxPayeeDetailField1.text;
        var email = this.view.txtBoxPayeeDetailField2.text;
        if(email) {
          this.view.txtBoxPayeeDetailField1.setEnabled(false);
          this.view.txtBoxPayeeDetailField1.skin = this.getParsedValue(this._sknTextBoxDisabled);
        }
        else {
          this.view.txtBoxPayeeDetailField1.setEnabled(true);
          this.view.txtBoxPayeeDetailField1.skin = this.Skins.TxtBoxField;
        }
      }
    },

    /**
     * setPayeeAddress
     * @api : setPayeeAddress
     * displays address field other than P2P flow
     * @return : NA
     */
    setPayeeAddress: function() {
      if(this.transferTypeContext === this.getFieldValue(this._transferFlow, "T4")) {
        this.view.flxPayeeAddress.setVisibility(false);
      }
    },

    /**
     * getSwiftFromIBAN
     * @api : getSwiftFromIBAN
     * get Swift code for IBAN
     * @return : NA
     */
    getSwiftFromIBAN: function() {
      var self = this;
      var objSvcName = this.getParsedValue(this._IBANSwiftObjectServiceName);
      var objName = this.getParsedValue(this._IBANSwiftObjectName);
      var operationName = this.getParsedValue(this._IBANSwiftOperationName);
      var criteria = this.getCriteria(this._IBANSwiftCriteria);
      var identifier = this.getParsedValue(this._IBANSwiftResponseIdentifier);
      this.unifiedTransferDAO.getSwiftCode
      (objSvcName,objName,operationName,criteria,this.setSwiftCodeForNewPayee,this.setSwiftCodeForNewPayee);
    },

    /**
     * setSwiftCodeForNewPayee
     * @api : setSwiftCodeForNewPayee
     * set swift code returned from api
     * @return : NA
     */
    setSwiftCodeForNewPayee: function(response) {
      var bic = response["bic"];
      var bankName = response["bankName"];
      this.view.txtBoxPayeeDetailField1.text = bic;
      this.view.txtBoxPayeeDetailField4.text = bankName;
      kony.application.dismissLoadingScreen();
    },

   

    /**
     * getBackendDateFormat
     * @api : getBackendDateFormat
     * formats date
     * @return : formattedBackendDate
     */	
    getBackendDateFormat: function(frontEndDate,frontEndDateFormat) {
      var scope = this;
      var date = scope.formatUtil.getDateObjectFromCalendarString(frontEndDate,frontEndDateFormat);
      var formattedBackendDate = date.toISOString();
      return formattedBackendDate;
    },

    /**
     * isExistingAccount
     * @api : isExistingAccount
     * checks whether the beneficiary is existing
     * @return : NA
     */
    isExistingAccount: function(accountNumber) {
      try {
        var scope = this;
        var existingAccounts = this.toAccountsList;
        for(var index in existingAccounts) {
          if(!this.isNullOrUndefinedOrEmpty(existingAccounts[index].accountNumber)){
            if(accountNumber.toUpperCase() === existingAccounts[index].accountNumber.toUpperCase()) {
              return accountNumber;
            }
          }
          else if(!this.isNullOrUndefinedOrEmpty(existingAccounts[index].accountID)){
            if(accountNumber.toUpperCase() === existingAccounts[index].accountID.toUpperCase()) {
              return accountNumber;
            }
          }

        }
        return "";
      } catch(err) {
        var errObj = {
          "errorInfo" : "Error in isExistingAccount method of the component.",
          "errorLevel" : "Configuration",
          "error": err
        };
        scope.onError(errObj);
      }
    },

    /**
     * changePayeeDetailFieldsVisibility
     * @api : changePayeeDetailFieldsVisibility
     * change the visibility of payee detail fields
     * @return : NA
     */
    changePayeeDetailFieldsVisibility: function() {
      var scope = this;
      var field1Visibility = !(scope.isNullOrUndefinedOrEmpty(scope.view.txtBoxPayeeDetailField1.text));
      var field2Visibility = !(scope.isNullOrUndefinedOrEmpty(scope.view.txtBoxPayeeDetailField2.text));
      var field3Visibility = !(scope.isNullOrUndefinedOrEmpty(scope.view.txtBoxPayeeDetailField3.text));
      var field4Visibility = !(scope.isNullOrUndefinedOrEmpty(scope.view.txtBoxPayeeDetailField4.text));
      var currentBreakPoint = kony.application.getCurrentBreakpoint();
      if(!(field1Visibility) && !(field2Visibility) && !(field3Visibility) && !(field4Visibility)){
        scope.view.flxPayeeDetailFields.setVisibility(false);
      }else{
        scope.view.flxPayeeDetailFields.setVisibility(true);
        if(field1Visibility || field2Visibility){
          scope.view.flxPayeeDetailFieldRow1.setVisibility(true);
          if(field1Visibility){
            scope.view.flxField1.setVisibility(true);
          }else{
            scope.view.flxField1.setVisibility(false);
          }
          if(field2Visibility){
            scope.view.flxField2.setVisibility(true);
          }else{
            scope.view.flxField2.setVisibility(false);
          }
        }else{
          scope.view.flxPayeeDetailFieldRow1.setVisibility(false);
        }
        if(field3Visibility || field4Visibility){
          scope.view.flxPayeeDetailFieldRow2.setVisibility(true);
          if(field3Visibility){
            scope.view.flxField3.setVisibility(true);
          }else{
            scope.view.flxField3.setVisibility(false);
          }
          if(field4Visibility){
            scope.view.flxField4.setVisibility(true);
          }else{
            scope.view.flxField4.setVisibility(false);
          }
        }else{
          scope.view.flxPayeeDetailFieldRow2.setVisibility(false);
        }
      }
      if(currentBreakPoint != 640){
        if(!(field1Visibility) && field2Visibility){
          scope.view.flxField2.left = "0%";
        }else{
          scope.view.flxField2.left = "1.8%";
        }
        if(!(field3Visibility) && field4Visibility){
          scope.view.flxField4.left = "0%";
        }else{
          scope.view.flxField4.left = "1.8%";
        }
      }else{
        if((!(field1Visibility) && field2Visibility) || (field1Visibility && !(field2Visibility))){
          scope.view.flxPayeeDetailFieldRow1.height = "75dp";
        }else{
          scope.view.flxPayeeDetailFieldRow1.height = "150dp";
        }
        if((!(field3Visibility) && field4Visibility) || (field3Visibility && !(field4Visibility))){
          scope.view.flxPayeeDetailFieldRow2.height = "75dp";
        }else{
          scope.view.flxPayeeDetailFieldRow2.height = "150dp";
        }
      }
    },
	
	    /**
     * filterFromAccounts
     * @api : filterFromAccounts
     * Filter the from account which does not have action key to perform transfer
     * @return : NA
     */
	
    filterFromAccounts : function(){
      var actionKey  = this.getParsedValue(this._fromAccountActionKey);
      if(!this.isNullOrUndefinedOrEmpty(actionKey)){
        var tempRecords = this.fromAccountsList;
        var filteredRecords =tempRecords.filter(function (record) {
          var includeRecord =  false; 
          var Actions = JSON.parse(record["actions"]);
          includeRecord = Actions.includes(actionKey);
          return includeRecord;
        });
         this.fromAccountsList = filteredRecords;
      }
      
    },
    resetWidgetData : function(){
      this.view.txtBoxPayeeName.text = "";
      this.view.txtBoxNewAccountNumber.text = "";
      this.view.txtBoxReenterAccountNumber.text = "";
      this.view.txtBoxPayeeDetailField1.text = "";
      this.view.txtBoxPayeeDetailField2.text = "";
      this.view.txtBoxPayeeDetailField3.text = "";
      this.view.txtBoxPayeeDetailField4.text = "";
      this.view.txtBoxAmountOthers.text = "";
      this.view.txtBoxTransferAmount.text = "";
      this.view.fromAccount.resetSelectedValue();
      this.view.toAccount.resetSelectedValue();
      this.view.txtAreaNotes.text = "";
      this.existingPayeeAddressLine01 = "";
      this.existingPayeeAddressLine02 = "";
      this.existingPayeeCity = "";
      this.existingPayeeState = "";
      this.existingPayeeCountry = "";
      this.existingPayeeZipCode = "";
      this.existingPayeePhoneNumber = "";
      this.existingPayeeEmailAddress = "";
      this.newPayeeAddressLine01 = "";
      this.newPayeeAddressLine02 = "";
      this.newPayeeCity = "";
      this.newPayeeState = "";
      this.newPayeeCountry = "";
      this.newPayeeZipCode = "";
      this.newPayeePhoneNumber = "";
      this.newPayeeEmailAddress = "";
      this.newPayeeData1 = "";
      this.newPayeeData2 = "";
      this.newPayeeData3 = "";
      this.newPayeeData4 = "";
      this.existingPayeeData4 = "";
      this.existingPayeeData3 = "";
      this.existingPayeeData2 = "";
      this.existingPayeeData1 = "";
      this.IBAN = "";
      this.isDVFValidated = "";
      this.isIBANValid = "";
      this.payeeTypeContext ="";
      this.dueTypes = "";
      this.paymentMethod = "";
      this.isPaidBy = "";
      this.uploadedAttachments = [];
      this.attachments = [];
      this.count = 0;
      this.docsServiceFormat = "";
      this.filesToBeUploaded = [];
      this.fromAccountsList=[];
      this.toAccountsList=[];	
      this.serviceCurrency = "";
      this.creditCardAccounts=[];						
      this.AccountsCurrencyList = [];
      this.fromAccount = "";
      this.toAccount = "";
      this.transferCurrency = "";	
      this.frequency = "Once";	  
      this.fromAccountAPISuccess = false;
      this.toAccountAPISuccess = false;
      this.crAccountAPISuccess = false;
      this.isServiceDone = 0;
      this.todayDate="";
      this.dueType="";
      this.dueAmount = "";	
      this.context = {};
      this.textInputsMapping = {};
      this.dataContext = {};
      this.transferTypeContext = "";
      this.accountTypeContext = "";
      this.view.flxFilesList.setVisibility(false);
      this.view.segFilesList.removeAll();
      this.view.segTransferCCYList.removeAll();
      this.view.flxAddressFields.isVisible = false;
    }

  };
});