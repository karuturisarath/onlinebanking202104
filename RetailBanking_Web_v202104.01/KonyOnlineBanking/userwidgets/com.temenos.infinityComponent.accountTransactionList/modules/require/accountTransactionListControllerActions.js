define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for accountTransactionList **/
    AS_FlexContainer_b62e513ecc4c40f7b44975cfb9c5462e: function AS_FlexContainer_b62e513ecc4c40f7b44975cfb9c5462e(eventobject) {
        var self = this;
        this.preShow();
    },
    /** onBreakpointChange defined for accountTransactionList **/
    AS_FlexContainer_bf842de60bca48799ab5343010210ba8: function AS_FlexContainer_bf842de60bca48799ab5343010210ba8(eventobject, breakpoint) {
        var self = this;
        this.onBreakpointChange();
    }
});