define(['./ParserUtilsManager','./EntitlementUtils'],function(ParserUtilsManager,EntitlementUtils) {

  return {
    constructor: function(baseConfig, layoutConfig, pspConfig) {

      //declaration for Icon Visibility in the group:General
      this._iconVisibility="";

      //declaration for More Link in the group:Action Buttons
      this._moreLink="";

      //declaration for Breakpoints in the group:Breakpoint
      this._BREAKPTS="";

      //declaration for Icon in the group:General
      this._icon="";

      //declaration for More Link Action in the group:Action Buttons
      this._moreLinkAction="";

      //declaration for Title Skin in the group:Skins
      this._titleSkin="";

      //declaration for Title in the group:General
      this._title="";

      //declaration for Action Button 1 in the group:Action Buttons
      this._actionButton1="";

      //declaration for Description in the group:General
      this._description="";

      //declaration for Action Button 2 in the group:Action Buttons
      this._actionButton2="";

      //declaration for More Link Skin in the group:Skins
      this._moreLinkSkin="";

      //declaration for Payee Type Context in the group:General
      this._payeeType="";

      //declaration for Description Rendering in the group:General
      this._descriptionRendering="";

      //declaration for Details Action Button 1 in the group:Action Buttons
      this._detailsActionButton1="";

      //declaration for Action Button 1 in the group:Skins
      this._actionButton1Skin="";

      //declaration for Terms & Conditions in the group:General
      this._teamsConditions="";

      //declaration for Details Action Button 2 in the group:Action Buttons
      this._detailsActionButton2="";

      //declaration for Action Button 2 in the group:Skins
      this._actionButton2Skin="";

      //declaration for Details Action Button 1 in the group:Skins
      this._detailsActionButton1Skin="";

      //declaration for Details Action Button 2 in the group:Skins
      this._detailsActionButton2Skin="";

      this._detailDescriptionSkn = "";
      this._detailDescHeadingSkn = "";
      this._popupTitleSkin = "";
      this._popupDescriptionSkn = "";
      this._termsConditionTitleSkn = "";
      this._termsConditionSkn = "";
      this._popupCloseBtnSkn = "";
      this._termsConditionsTitle = "";
      this._closeImage = "";
      this._popupTitle = "";
      this._transferType = "";


      // Object for parserUtilsManager
      this.parserUtilsManager = new ParserUtilsManager();
      this.EntitlementUtils = new EntitlementUtils();
      this.popupObj = "";
    },


    //Logic for getters/setters of custom properties
    initGettersSetters: function() {


      //setter method for Icon Visibility in the group:General
      defineSetter(this, "iconVisibility", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._iconVisibility=val;
        }
      });

      //getter method for Icon Visibility in the group:General
      defineGetter(this, "iconVisibility", function() {
        return this._iconVisibility;
      });

      //setter method for More Link in the group:Action Buttons
      defineSetter(this, "moreLink", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._moreLink=val;
        }
      });

      //getter method for More Link in the group:Action Buttons
      defineGetter(this, "moreLink", function() {
        return this._moreLink;
      });

      //setter method for Breakpoints in the group:Breakpoint
      defineSetter(this, "BREAKPTS", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._BREAKPTS=val;
        }
      });

      //getter method for Breakpoints in the group:Breakpoint
      defineGetter(this, "BREAKPTS", function() {
        return this._BREAKPTS;
      });

      //setter method for Icon in the group:General
      defineSetter(this, "icon", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._icon=val;
        }
      });

      //getter method for Icon in the group:General
      defineGetter(this, "icon", function() {
        return this._icon;
      });

      //setter method for Popup Title in the group:General
      defineSetter(this, "popupTitle", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._popupTitle=val;
        }
      });

      //getter method for Popup Title in the group:General
      defineGetter(this, "popupTitle", function() {
        return this._popupTitle;
      });

      //setter method for Transfer Type in the group:General
      defineSetter(this, "transferType", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._transferType=val;
        }
      });

      //getter method for Transfer Type in the group:General
      defineGetter(this, "transferType", function() {
        return this._transferType;
      });

      //setter method for More Link Action in the group:Action Buttons
      defineSetter(this, "moreLinkAction", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._moreLinkAction=val;
        }
      });

      //getter method for More Link Action in the group:Action Buttons
      defineGetter(this, "moreLinkAction", function() {
        return this._moreLinkAction;
      });

      //setter method for Title Skin in the group:Skins
      defineSetter(this, "titleSkin", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._titleSkin=val;
        }
      });

      //getter method for Title Skin in the group:Skins
      defineGetter(this, "titleSkin", function() {
        return this._titleSkin;
      });

      //setter method for Title in the group:General
      defineSetter(this, "title", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._title=val;
        }
      });

      //getter method for Title in the group:General
      defineGetter(this, "title", function() {
        return this._title;
      });

      //setter method for Action Button 1 in the group:Action Buttons
      defineSetter(this, "actionButton1", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._actionButton1=val;
        }
      });

      //getter method for Action Button 1 in the group:Action Buttons
      defineGetter(this, "actionButton1", function() {
        return this._actionButton1;
      });

      //setter method for Description in the group:General
      defineSetter(this, "description", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._description=val;
        }
      });

      //getter method for Description in the group:General
      defineGetter(this, "description", function() {
        return this._description;
      });

      //setter method for Action Button 2 in the group:Action Buttons
      defineSetter(this, "actionButton2", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._actionButton2=val;
        }
      });

      //getter method for Action Button 2 in the group:Action Buttons
      defineGetter(this, "actionButton2", function() {
        return this._actionButton2;
      });

      //setter method for More Link Skin in the group:Skins
      defineSetter(this, "moreLinkSkin", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._moreLinkSkin=val;
        }
      });

      //getter method for More Link Skin in the group:Skins
      defineGetter(this, "moreLinkSkin", function() {
        return this._moreLinkSkin;
      });

      //setter method for Description Rendering in the group:General
      defineSetter(this, "descriptionRendering", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._descriptionRendering=val;
        }
      });

      //getter method for Description Rendering in the group:General
      defineGetter(this, "descriptionRendering", function() {
        return this._descriptionRendering;
      });

      //setter method for Details Action Button 1 in the group:Action Buttons
      defineSetter(this, "detailsActionButton1", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._detailsActionButton1=val;
        }
      });

      //getter method for Details Action Button 1 in the group:Action Buttons
      defineGetter(this, "detailsActionButton1", function() {
        return this._detailsActionButton1;
      });

      //setter method for Action Button 1 in the group:Skins
      defineSetter(this, "actionButton1Skin", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._actionButton1Skin=val;
        }
      });

      //getter method for Action Button 1 in the group:Skins
      defineGetter(this, "actionButton1Skin", function() {
        return this._actionButton1Skin;
      });

      //setter method for Initial Payee Type in the group:General
      defineSetter(this, "payeeType", function(val) {
        if((typeof val=='string') && (val != "")){
          this._payeeType=val;
        }
      });

      //getter method for Initial Payee Type in the group:General
      defineGetter(this, "payeeType", function() {
        return this._payeeType;
      });

      //setter method for Close Image in the group:General
      defineSetter(this, "closeImage", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._closeImage=val;
        }
      });

      //getter method for Close Image in the group:General
      defineGetter(this, "closeImage", function() {
        return this._closeImage;
      });

      //setter method for Terms & Conditions in the group:General
      defineSetter(this, "teamsConditions", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._teamsConditions=val;
        }
      });

      //getter method for Terms & Conditions in the group:General
      defineGetter(this, "teamsConditions", function() {
        return this._teamsConditions;
      });

      //setter method for Details Action Button 2 in the group:Action Buttons
      defineSetter(this, "detailsActionButton2", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._detailsActionButton2=val;
        }
      });

      //getter method for Details Action Button 2 in the group:Action Buttons
      defineGetter(this, "detailsActionButton2", function() {
        return this._detailsActionButton2;
      });

      //setter method for Action Button 2 in the group:Skins
      defineSetter(this, "actionButton2Skin", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._actionButton2Skin=val;
        }
      });

      //getter method for Action Button 2 in the group:Skins
      defineGetter(this, "actionButton2Skin", function() {
        return this._actionButton2Skin;
      });

      //setter method for Details Action Button 1 in the group:Skins
      defineSetter(this, "detailsActionButton1Skin", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._detailsActionButton1Skin=val;
        }
      });

      //getter method for Details Action Button 1 in the group:Skins
      defineGetter(this, "detailsActionButton1Skin", function() {
        return this._detailsActionButton1Skin;
      });

      //setter method for Details Action Button 2 in the group:Skins
      defineSetter(this, "detailsActionButton2Skin", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._detailsActionButton2Skin=val;
        }
      });

      //getter method for Details Action Button 2 in the group:Skins
      defineGetter(this, "detailsActionButton2Skin", function() {
        return this._detailsActionButton2Skin;
      });


      //setter method for Details Action Button 2 in the group:Skins
      defineSetter(this, "detailDescriptionSkn", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._detailDescriptionSkn=val;
        }
      });

      //getter method for Details Action Button 2 in the group:Skins
      defineGetter(this, "detailDescriptionSkn", function() {
        return this._detailDescriptionSkn;
      });

      //setter method for Details Action Button 2 in the group:Skins
      defineSetter(this, "detailDescHeadingSkn", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._detailDescHeadingSkn=val;
        }
      });

      //getter method for Details Action Button 2 in the group:Skins
      defineGetter(this, "detailDescHeadingSkn", function() {
        return this._detailDescHeadingSkn;
      });

      //setter method for Details Action Button 2 in the group:Skins
      defineSetter(this, "popupTitleSkin", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._popupTitleSkin=val;
        }
      });

      //getter method for Details Action Button 2 in the group:Skins
      defineGetter(this, "popupTitleSkin", function() {
        return this._popupTitleSkin;
      });


      //setter method for Details Action Button 2 in the group:Skins
      defineSetter(this, "popupDescriptionSkn", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._popupDescriptionSkn=val;
        }
      });

      //getter method for Details Action Button 2 in the group:Skins
      defineGetter(this, "popupDescriptionSkn", function() {
        return this._popupDescriptionSkn;
      });

      //setter method for Details Action Button 2 in the group:Skins
      defineSetter(this, "termsConditionTitleSkn", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._termsConditionTitleSkn=val;
        }
      });

      //getter method for Details Action Button 2 in the group:Skins
      defineGetter(this, "termsConditionTitleSkn", function() {
        return this._termsConditionTitleSkn;
      });

      //setter method for Details Action Button 2 in the group:Skins
      defineSetter(this, "termsConditionSkn", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._termsConditionSkn=val;
        }
      });

      //getter method for Details Action Button 2 in the group:Skins
      defineGetter(this, "termsConditionSkn", function() {
        return this._termsConditionSkn;
      });

      //setter method for Details Action Button 2 in the group:Skins
      defineSetter(this, "popupCloseBtnSkn", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._popupCloseBtnSkn=val;
        }
      });

      //getter method for Details Action Button 2 in the group:Skins
      defineGetter(this, "popupCloseBtnSkn", function() {
        return this._popupCloseBtnSkn;
      }); 

      //setter method for Terms & Conditions Title in the group:General
      defineSetter(this, "termsConditionsTitle", function(val) {
        if((typeof val==='string') && (val !== "")){
          this._termsConditionsTitle=val;
        }
      });

      //getter method for Terms & Conditions Title in the group:General
      defineGetter(this, "termsConditionsTitle", function() {
        return this._termsConditionsTitle;
      });

    },

    preShow: function(){
      var scope = this;
      //   this.view.flxTransactionType.onClick = this.selectedTransferType;
      this.view.btnMoreLink.onClick = this.detailsLinkEnabling;
      this.view.flxDescriptionPage.flxHeader.imgBackBtn.onTouchEnd = this.closeDescription;
      if(kony.application.getCurrentBreakpoint === undefined){
        this.bindFieldsData();
      }
      scope.view.description.closePopup = function(){
        var form = kony.application.getCurrentForm();
        form.remove(scope.popupObj);

      };
    },
    /**
     * Component onBreakPointChange
     * Set UI based on breakpoint
     */
    onBreakPointChange: function() {
      //   var currentBreakPoint = kony.application.getCurrentBreakpoint();
      this.bindFieldsData();

      if(this.popupObj !== undefined && this.popupObj !== null) {        
        var form = kony.application.getCurrentForm();
        form.remove(this.popupObj);
      }
    },

    //Selected Transfer Type
    selectedTransferType: function(){
      var transType = this.parseJsonAndGetValue(this._title);
      alert(transType);
    },
    
    /**
     * isNullOrUndefinedOrEmpty
     * @api : isNullOrUndefinedOrEmpty
     * checks whether the input is null or empty or undefined
     * @return : NA
     */
    isNullOrUndefinedOrEmpty: function(val) {
      if (val === null || val === undefined || val ==="") {
        return true;
      } else {
        return false;
      }
    },

    bindFieldsData: function(){
      //Set transfer type icon
      
      var btn1Entitlement = "";
      var btn2Entitlement = "";

      var iconImg = this.parseJsonAndGetValue(this._icon);
      this.view.imgTransactionIcon.src = iconImg;

      if(this.parseJsonAndGetValue(this._iconVisibility) === "On"){
        this.view.imgTransactionIcon.setVisibility(true);
      }
      else{
        this.view.imgTransactionIcon.setVisibility(false);
      }
      //Title properties
      var title = this.parseJsonAndGetValue(this._title);
      this.view.lblTransactionType.text = title;
      this.view.lblTransactionType.skin = this.parseJsonAndGetValue(this._titleSkin);
      //Short Description
      var descriptionText = this.getLabelText(this._description);
      var shortDescription = this.getShortDescription(descriptionText);

      this.view.rtLearnMore.text =shortDescription;
      this.view.rtLearnMore.linkSkin = this.parseJsonAndGetValue(this._moreLinkSkin).Normal;
      this.view.rtLearnMore.linkFocusSkin = this.parseJsonAndGetValue(this._moreLinkSkin).Focus;
      this.view.rtLearnMore.onClick = this.detailsLinkEnabling; 

      var actionBtn1 = this.parseJsonAndGetValue(this._actionButton1);
      this.view.btnAction1.text = actionBtn1.text;
      this.view.btnAction1.skin = this.parseJsonAndGetValue(this._actionButton1Skin).Normal;
      this.view.btnAction1.hoverSkin = this.parseJsonAndGetValue(this._actionButton1Skin).Hover;
      this.view.btnAction1.focusSkin = this.parseJsonAndGetValue(this._actionButton1Skin).Focus;
      this.view.btnAction1.onClick = this.buttonActionHandling.bind(this, actionBtn1.id);

      var actionBtn2 = this.parseJsonAndGetValue(this._actionButton2);
      this.view.btnAction2.text = actionBtn2.text; 
      this.view.btnAction2.skin = this.parseJsonAndGetValue(this._actionButton2Skin).Normal;
      this.view.btnAction2.hoverSkin = this.parseJsonAndGetValue(this._actionButton2Skin).Hover;
      this.view.btnAction2.focusSkin = this.parseJsonAndGetValue(this._actionButton2Skin).Focus;
      this.view.btnAction2.onClick = this.buttonActionHandling.bind(this, actionBtn2.id);

      if(!this.isNullOrUndefinedOrEmpty(this.parseJsonAndGetValue(this._actionButton1))) {
        btn1Entitlement = this.EntitlementUtils.isEntitled(this.parseJsonAndGetValue(this._actionButton1).entitlement_keys);
      }
      
      if(!this.isNullOrUndefinedOrEmpty(this.parseJsonAndGetValue(this._actionButton2))) {
      btn2Entitlement =this.EntitlementUtils.isEntitled(this.parseJsonAndGetValue(this._actionButton2).entitlement_keys);
      }
      if(btn1Entitlement && btn2Entitlement){
        this.view.btnAction1.setVisibility(true);
        this.view.btnAction2.setVisibility(true);
        this.view.flxSeperator2.setVisibility(true);
      }
      else if(btn1Entitlement){
        this.view.btnAction1.centerX = "50%";
        this.view.btnAction1.setVisibility(true);
        this.view.btnAction2.setVisibility(false);
        this.view.flxSeperator2.setVisibility(false);
      }
      else if(btn2Entitlement){
        this.view.btnAction2.centerX = "50%";
        this.view.btnAction1.setVisibility(false);
        this.view.btnAction2.setVisibility(true);
        this.view.flxSeperator2.setVisibility(false);
        this.view.forceLayout();
      }
      else {
        this.hideTile();
      }

      this.view.flxTransferTypes.forceLayout();

    },    

    detailsLinkEnabling: function(){
      var form = kony.application.getCurrentForm();	
      var visibleDesc = "true";
      if(this.parseJsonAndGetValue(this._moreLinkAction) === "New Form"){
        this.view.imgTransDescriptionIcon.src = this.parseJsonAndGetValue(this._icon);
        //Details page properties
        this.view.lblDescriptionHeading.text = this.parseJsonAndGetValue(this._title);
        this.view.lblDescriptionHeading.skin = this.parseJsonAndGetValue(this._detailDescHeadingSkn);

        this.view.lblDescription.text = this.getLabelText(this._description);
        this.view.lblDescription.skin = this.parseJsonAndGetValue(this._detailDescriptionSkn);

        this.view.btnDescriptionActionBtn1.text = this.parseJsonAndGetValue(this._detailsActionButton1).text; 
        this.view.btnDescriptionActionBtn1.skin = this.parseJsonAndGetValue(this._detailsActionButton1Skin).Active;
        this.view.btnDescriptionActionBtn1.onClick =this.buttonActionHandling.bind(this, this.parseJsonAndGetValue(this._detailsActionButton1).id);

        this.view.btnDescriptionActionBtn2.text = this.parseJsonAndGetValue(this._detailsActionButton2).text; 
        this.view.btnDescriptionActionBtn2.skin = this.parseJsonAndGetValue(this._detailsActionButton2Skin).Active;
        this.view.btnDescriptionActionBtn2.onClick =this.buttonActionHandling.bind(this, this.parseJsonAndGetValue(this._detailsActionButton2).id);

        this.popupObj = this.view.flxDescriptionPage.clone();	
        form.add(this.popupObj);
        this.popupObj.isVisible = true;
        this.enableHideDescription(visibleDesc);
        this.view.flxTransferTypes.forceLayout();
      }
      if(this.parseJsonAndGetValue(this._moreLinkAction) === "Popup"){
        var popupDetails = {
          "description":this.getLabelText(this._description),
          "termsCondtion":this.getLabelText(this._teamsConditions),
          "termsConditionTitle":this.parseJsonAndGetValue(this._termsConditionsTitle),
          "tranferType":this.parseJsonAndGetValue(this._popupTitle),
          "popupTitleSkin":this.parseJsonAndGetValue(this._popupTitleSkin),
          "popupDescriptionSkn":this.parseJsonAndGetValue(this._popupDescriptionSkn),
          "termsConditionTitleSkn":this.parseJsonAndGetValue(this._termsConditionTitleSkn),
          "termsConditionSkn":this.parseJsonAndGetValue(this._termsConditionSkn),
          "popupCloseBtnNormalSkn":this.parseJsonAndGetValue(this._popupCloseBtnSkn).Normal,
          "popupCloseBtnHoverSkn":this.parseJsonAndGetValue(this._popupCloseBtnSkn).Hover,
          "popupCloseBtnFocusSkn":this.parseJsonAndGetValue(this._popupCloseBtnSkn).Focus,
          "closeImage":this.parseJsonAndGetValue(this._closeImage)
        };
        //this.setPopupDetails(popupDetails);
        this.popupObj = this.view.description.setPopupDetails(popupDetails);
        form.add(this.popupObj);
        // popupObj.isVisible = true;
        this.view.forceLayout();
        //this.view.flxPopup.setVisibility(true);
      }

    },
    /**
     * Method getLabelText
     * @param: text{String}/{contractJSON} - text to be parsed
     * @return: {String}/{Object} - parsed text
     */
    getLabelText: function(contractJSON) {

      let labelText = this.getParsedValue(contractJSON,kony.application.getCurrentBreakpoint());
      return labelText ? labelText : "";
    },
    /**
     * Method getParsedValue
     * @param: text to be parsed,Breakpoint 
     * @return: {String}/{Object} - parsed text
     */
    getParsedValue: function(property, selectedValue) {
      try{
        property = JSON.parse(property);

      }
      catch(e){
        property = property;
        kony.print(e);
      }
      if(typeof(property) === "string"){
        return this.getProcessedText(property);
      }
      else{

        if(selectedValue)
          return this.getProcessedText(this.parserUtilsManager.getComponentConfigParsedValue(property,selectedValue));
        else
          return property;
      }
    },
    closeDescription:function(){
      var visibilityDesc = false;
      var currForm = kony.application.getCurrentForm();
      this.enableHideDescription(visibilityDesc);
      currForm.remove(this.popupObj);
      this.view.flxPopup.setVisibility(false);
    },


    buttonActionHandling: function(clickedButton){
      var selectedTrasferType = { "transferType" : this.parseJsonAndGetValue(this._transferType),
                                 "clickedButton" : clickedButton,
                                 "flowType" : "add",
                                 "payeeType" : this.parseJsonAndGetValue(this._payeeType)
                                };
      this.transferTypeDetails(selectedTrasferType);
    },


    /**
     * Component parseJsonAndGetValue
     * Parse the value and returns the processed value for given account type
     * @param: Value{String} - String taken from configuration
     * @return: {String} - Processed value
     */
    parseJsonAndGetValue: function(Value) {
      var valueJson = Value;
      try {
        if(typeof(Value) === "string"){
          valueJson = JSON.parse(Value);
        }
        if(kony.application.getCurrentBreakpoint !== undefined){
          if(this.breakPointParser(valueJson,kony.application.getCurrentBreakpoint())) {
            var text=this.breakPointParser(valueJson,kony.application.getCurrentBreakpoint()); 
            return text;
          }    
        }
        else{
          if(valueJson.default !== undefined){
            return valueJson.default;
          }
        }

      } catch (e) {
        kony.print(e);
      }
      return valueJson;
    },

    /**
     * Component breakPointParser
     * Parse the value and returns the processed value for the breakpoint
     * @param: inputJSON{JSON} - Json with breakpoint values
     * @param: lookUpKey{String} - breakpoint value
     * @return: {String} - value for  the given breakpoint
     */
    breakPointParser: function(inputJSON,lookUpKey) {
      inputJSON = this.getBreakPointValue(inputJSON);											 		   
      if(inputJSON.hasOwnProperty(lookUpKey)) {
        return inputJSON[lookUpKey];
      }
      else if(inputJSON["default"]) {
        return inputJSON["default"];
      }
      return inputJSON;
    },

    /**
     * Component getBreakPointValue
     * Reponsible for parsing the value for breakpoints configured
     */
    getBreakPointValue:function(contractJSON){
      try
      {
        if(typeof(this._BREAKPTS) == "string")
          this._BREAKPTS = JSON.parse(this._BREAKPTS);
        for(var key in contractJSON)
        {
          if(key.includes("$.BREAKPTS."))
          {
            keyValue = key.split("$.BREAKPTS.");
            var newKey = this._BREAKPTS[keyValue[1]];
            var prevValue = contractJSON[key];
            delete(contractJSON[key]);
            contractJSON[newKey] = prevValue;
          }
        }
        return contractJSON;
      }
      catch(err)
      {
        return contractJSON;
      }
    },

    /**
     * Component getProcessedText
     * Get the processed text from parser util
     * @param: text{String}/{Object} - text to be parsed
     * @return: {String}/{Object} - parsed text
     */
    getProcessedText: function(text) {
      return this.parserUtilsManager.getParsedValue(text);
    },

    getShortDescription(description) {
      var descriptionRender =this.parseJsonAndGetValue(this._descriptionRendering);       
      var moreLink  = "<a href = "+"#>"+this.parseJsonAndGetValue(this._moreLink)+"</a>";
      //Defining properties for a link widget.   
      var shortText = description.slice(0,descriptionRender.length) + descriptionRender.ellipsis + moreLink;       
      return shortText;
    },

    /**
     * Component setContext
     * To set context values
     */
    setContext:function(context){
      this.EntitlementUtils.setEntitlements(context);
    }
  };
});