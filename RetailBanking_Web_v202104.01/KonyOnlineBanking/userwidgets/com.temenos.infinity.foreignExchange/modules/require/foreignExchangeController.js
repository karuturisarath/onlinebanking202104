define(['./ForexPresenter'], function(ForexPresenter) {

  return {
    constructor: function(baseConfig, layoutConfig, pspConfig) {
      this.forexPresenter = new ForexPresenter(this);
    },
    
    //Logic for getters/setters of custom properties
    initGettersSetters: function() {  

    },
	
    preShow : function() {
      this.closeErrorMessage();
      this.view.segFilter.onRowClick = this.onFilterRowClick;
      this.view.segToCuurencyCode.onRowClick = this.onQuoteCurrenciesRowClick;
      this.view.flxDropDownIcon.onClick=this.onFlexDropDownIconClick.bind(this);
      this.flxDropDownCollapsed=true;
      this.view.tbxSearch.onKeyUp=this.onQuoteCurrencySearch.bind(this);
      this.view.flxDropDown.onClick = this.showFilterSegment;
      this.view.flxPlus.onClick=this.showConvertedValues;
      this.flxPlusCollapsed=true;
      this.view.segConvertedValues.onRowClick = this.onConversionChoiceSelection;
      this.view.tbxBaseCurrencyCodeValue.onKeyUp=this.convert.bind(this);
      this.view.onClick = this.setSegmentVisibility;
      this.view.imgCloseDowntimeWarning.onTouchStart = this.closeErrorMessage.bind(this);
      this.setAccessibilityConfig();
    },
    
    postShow : function() {
      this.forexPresenter.setInitialDataForComponent();
    },
    
    setAccessibilityConfig : function(){
      this.view.lblBaseCurrencyName.accessibilityConfig = {
        "a11yLabel" : this.view.lblBaseCurrencyName.text
      };
      this.view.lblDisclaimer.accessibilityConfig = {
        "a11yLabel" : kony.i18n.getLocalizedString("kony.i18n.forex.lblDisclaimer")
      };
      this.view.tbxBaseCurrencyCodeValue.accessibilityConfig = {
        "a11yLabel" : kony.i18n.getLocalizedString("kony.i18n.forex.tbxBaseCurrency")
      };
      this.view.flxDropDownIcon.accessibilityConfig = {
        "a11yARIA" :{
          "role":"button"
        }
      };
      this.view.imgDropDownIcon.accessibilityConfig = {
        "a11yLabel" : kony.i18n.getLocalizedString("kony.i18n.forex.imgDropDownIcon")
       };
      this.view.flxDropDownIcon.accessibilityConfig = {
        "a11yARIA" :{
          "role":"button"
        }
      };
      this.view.flxPlus.accessibilityConfig = {
        "a11yARIA" :{
          "role":"button"
        }
      };
      this.view.imgPlus.accessibilityConfig = {
        "a11yLabel" : kony.i18n.getLocalizedString("kony.i18n.forex.imgPlus")
        };
      this.view.flxDropDown.accessibilityConfig = {
        "a11yARIA" :{
          "role":"button"
        }
      };
      this.view.lblDropDown.accessibilityConfig = {
        "a11yLabel" : kony.i18n.getLocalizedString("kony.i18n.forex.imgDropDown")
      };
    },
    
    setSegmentVisibility : function(){
      this.forexPresenter.handleSegmentVisibility();
    },
    
    setCountryCode : function( countryCode ) {
      this.forexPresenter.handleSetCountryCode( countryCode );
    },
    
    setFeaturesAndPermissions : function( features, userPermissions ) {
      this.forexPresenter.handleSetFeaturesAndPermissions( features, userPermissions );
    },
    
    setBaseCurrency : function(baseCurrencyCode){
      this.view.lblBaseCurrencyName.text = baseCurrencyCode;
      this.view.forceLayout();
    },
    
    setContainersPosition : function(isMobile){
      if(!kony.sdk.isNullOrUndefined(isMobile) && isMobile){
        this.view.flxRecentCurrencySelectedContainer.setVisibility(false);
        this.view.lblDisclaimer.top="570dp";
        this.view.flxSeparatorCalculator.top="130dp";
        this.view.height = "670dp";
        this.view.flxMainContainer.height = "630dp";
      }
      else{
        this.view.flxRecentCurrencySelectedContainer.setVisibility(true);
        this.view.lblDisclaimer.top="720dp";
        this.view.height = "800dp";
        this.view.flxMainContainer.height = "750dp";
        this.view.flxScrollCurrencyCode.top = "40dp";
        this.view.flxSeparatorCalculator.top="140dp";
      }

      this.view.flxSelectedCurrenecyContainer.top="150dp";
      this.view.flxRecentCurrencySelectedContainer.top="350dp";
    },
    
    showBaseCurrency : function(visibility){
      this.view.flxBaseCurrency.setVisibility(visibility);
    },
    
    showToCurrency : function(visibility){
      this.view.flxToCurrency.setVisibility(visibility);
    },
    
    showCalculateBaseToCurrency: function(visibility){
      this.view.flxBaseCurrencyCodeContainer.setVisibility(visibility);
      this.view.lblExchangeRate.setVisibility(visibility);
      this.view.flxEqualContainer.setVisibility(visibility);
      this.view.flxToCurrencyCodeContainer.setVisibility(visibility);
    },
    
    showSelectedContainer : function(visibility){
    this.view.flxSelectedCurrenecyContainer.setVisibility(visibility);
    },
    
    showRecentCurrencyContainer : function(visibility, isMobile){
      this.view.flxRecentCurrencySelectedContainer.setVisibility((!isMobile) && visibility);
    },
    
    //method to set data for this.view.segToCuurencyCode
    setQuoteCurrencyList : function(currencyList) {
      if(currencyList.length === 0){
        this.view.flxSearchData.setVisibility(false);
        this.view.flxNoQuoteCurrencyList.setVisibility(true);
        this.view.flxPlus.setVisibility(false);
        this.view.lblTTBuyValue.text = kony.i18n.getLocalizedString("i18n.common.NA");
      }
      else{
        this.view.segToCuurencyCode.widgetDataMap = this.getWidgetDataMapForCurrencyList();
        this.view.segToCuurencyCode.rowTemplate = "flxRowExchangeRate";
        this.view.segToCuurencyCode.sectionHeaderTemplate = "flxAccountList";
        this.view.segToCuurencyCode.setData(currencyList);
        this.view.flxSearchData.setVisibility(true);
        this.view.flxNoQuoteCurrencyList.setVisibility(false);
        this.view.flxPlus.setVisibility(true);
      }
      this.view.forceLayout();
    },
    onQuoteCurrencySearch:function(){
			var searchQuery=this.view.tbxSearch.text;
			this.forexPresenter.searchCurrency(searchQuery);
	},
    uiSettingsonResettingSegment:function(){
      this.view.tbxSearch.text='';
    },
    getWidgetDataMapForRecentCurrency : function(){
      return{
        "lblCurrency":"lblCurrency",
        "lblCurrencyBuy":"header1BuyRate",
        "lblCurrencySell":"header1SellRate",
        "lblTTBuy":"header2BuyRate",
        "lblTTSell":"header2SellRate",
        "lblTCBuy":"header3BuyRate",
        "lblTCSell": "header3SellRate",
        "lblRowCurrency":"currency",
        "lblRowCurrencyBuy":"field1BuyRate",
        "lblRowCurrencySell":"field1SellRate",
        "lblRowTTBuy":"field2BuyRate",
        "lblRowTTSell":"field2SellRate",
        "lblRowTCBuy":"field3BuyRate",
        "lblRowTCSell": "field3SellRate",
        "flxSeparator": "flxSeparator",
        "template" : "template"
      };
    },
    
    getWidgetDataMapForRecentCurrencyMobile : function(){
      return{
        "lblRowCurrencyValue":"lblRowCurrencyValue",
        "lblRowCurrency":"lblRowCurrency",
        "flxSeparator":"flxSeparator"
      };
    },
    
    onQuoteCurrenciesRowClick : function(segWidget, sectionNumber, rowNumber){
      var record;
      if(Array.isArray(segWidget.data[0])){
         record=segWidget.data[sectionNumber][1][rowNumber];
      }
      else{
       record = segWidget.data[rowNumber];
      }
      this.forexPresenter.handleQuoteCurrencyCodeSelection(record);
      this.forexPresenter.reSettingOnQuoteCurrencyCodeSelection();
    },
    
    uiSettingsOnQuoteCurrenciesRowClick : function(quoteCurrency) {
      this.view.flxScrollCurrencyCode.setVisibility(false);
      this.view.imgDropDownIcon.src = "listboxuparrow.png";
      this.flxDropDownCollapsed = true;
      if(!kony.sdk.isNullOrUndefined(quoteCurrency.quoteCurrency))
        this.view.lblToCurrencyType.text = quoteCurrency.quoteCurrency;
      else
        this.view.lblToCurrencyType.text = kony.i18n.getLocalizedString("i18n.common.NA");
      this.view.forceLayout();      
    },
    
    //method to set data for this.view.segRecentCurrency
    setDashboardCurrencyRates : function(currencies) {
      if(currencies.length === 0){
        this.view.segRecentCurrency.isVisible = false;
        this.view.flxNoRecentRecords.isVisible = true;
      }
      else {               
        if(this.setTCDataFlag(currencies) === false)
          {
            this.view.segRecentCurrency.widgetDataMap = this.getWidgetDataMapForRecentCurrency();        
            this.view.segRecentCurrency.setData(currencies); 
          }
        else {
          this.view.segRecentCurrency.widgetDataMap = this.getWidgetDataMapForRecentCurrency();        
            this.view.segRecentCurrency.setData(currencies);
          for(i=0;i<this.view.segRecentCurrency.data[0][1].length;i++){
            this.view.segRecentCurrency.data[0][1][i].field3BuyRate.isVisible = true;
            this.view.segRecentCurrency.data[0][1][i].field3SellRate.isVisible = true;
            this.view.segRecentCurrency.data[0][0].header3BuyRate.isVisible = true;
            this.view.segRecentCurrency.data[0][0].header3SellRate.isVisible = true;
          }
        }
        this.view.segRecentCurrency.isVisible = true;
        this.view.flxNoRecentRecords.isVisible = false;
      }
      this.setFilterVisibility();
      this.view.forceLayout();
    },
    
    setTCDataFlag: function(currencies){
      var flag = false;
      for(i=0;i<currencies[0][1].length;i++){
        if(!kony.sdk.isNullOrUndefined(currencies[0][1][i].field3BuyRate))
          flag = true;
      }
      return flag;
    },

    showFilterSegment : function(){
      if(this.view.lblDropDown.text === "O"){
        this.view.flxSegFilter.setVisibility(true);
        this.view.lblDropDown.text = "P";
      }
      else {
        this.view.flxSegFilter.setVisibility(false);
        this.view.lblDropDown.text= "O";
      }
      this.view.forceLayout();
    },
    
    //method to set data for this.view.segFilter
    setFilterData : function(marketData) {
      this.view.segFilter.widgetDataMap = {
        "lblName" : "lblName"
      };
      this.view.segFilter.setData(marketData);
      this.view.forceLayout();
    },
    
    //method to set data for this.view.segSelectedExchangeRate
    setCurrencyRates : function(currencyRates, isMobile) {
      if(currencyRates.length === 0){
        this.view.segSelectedExchangeRate.isVisible = false;
        this.view.flxNoSelectedCurrency.isVisible = true;
      }
      else{
        if(!kony.sdk.isNullOrUndefined(isMobile) && isMobile){
          this.view.segSelectedExchangeRate.widgetDataMap = this.getWidgetDataMapForRecentCurrencyMobile();
          var rowData = currencyRates.map(function (dataItem) {
            var data = {
              "lblRowCurrency":{
                "text" :dataItem["header"],
                "accessibilityConfig" : {
                  "a11yLabel" : dataItem["header"]
                }
              },
              "lblRowCurrencyValue":{
                "text" :dataItem["rate"]
              },
              "flxSeparator": {
                "text" : "-"
              }
            };
            return data;
          });
          this.view.segSelectedExchangeRate.setData(rowData);
        }
        else{
          if(this.setTCDataFlag(currencyRates) === false)
          {
            this.view.segSelectedExchangeRate.widgetDataMap = this.getWidgetDataMapForRecentCurrency();
          this.view.segSelectedExchangeRate.setData(currencyRates);
          }
        else {
          this.view.segSelectedExchangeRate.widgetDataMap = this.getWidgetDataMapForRecentCurrency();
          this.view.segSelectedExchangeRate.setData(currencyRates);          
            this.view.segSelectedExchangeRate.data[0][1][0].field3BuyRate.isVisible = true;
            this.view.segSelectedExchangeRate.data[0][1][0].field3SellRate.isVisible = true;
            this.view.segSelectedExchangeRate.data[0][0].header3BuyRate.isVisible = true;
            this.view.segSelectedExchangeRate.data[0][0].header3SellRate.isVisible = true;          
        }          
        }
        this.view.segSelectedExchangeRate.isVisible = true;
        this.view.flxNoSelectedCurrency.isVisible = false;
      }
      this.setFilterVisibility();
      this.view.forceLayout();
    },

    //method to show or hide loading indicator on component
    showOrHideLoadingIndicator : function(isVisible) {
      if(isVisible){
        kony.application.showLoadingScreen();
      }
      else{
        kony.application.dismissLoadingScreen();
      }
    },
    
    //method to show any error message
    showErrorMessage : function(error){
      this.view.lblDowntimeWarning.text = error.dbpErrMsg;
      this.view.flxMainWrapper.setVisibility(true);
      this.view.segSelectedExchangeRate.setVisibility(false);
      this.view.flxNoSelectedCurrency.setVisibility(true);
      this.view.segRecentCurrency.setVisibility(false);
      this.view.flxNoRecentRecords.setVisibility(true);
      this.setFilterVisibility();
      this.showOrHideLoadingIndicator(false);
      this.view.forceLayout();
    },
    setHeaderandMarketValues:function(markets){
      this.view.segConvertedValues.widgetDataMap = this.getWidgetDataforMarketValues();
      this.view.segConvertedValues.setData(markets);
      this.view.forceLayout();
    },
     /**
      * getWidgetDataMap.
      * responsible for getting the widgetDataMap for desktop template
      * @return : {Object} - WidgetDataMap.
      */
    getWidgetDataMapForCurrencyList: function(){
        return {
          "flxAccountList":"flxAccountList",
          "flxRowExchangeRate": "flxRowExchangeRate",
          "lblHeader": "quoteCurrency",
          "lblName":"lblName",
          "lblSeparator":"lblSeparator"
        };
      },
    
    onFlexDropDownIconClick:function(){
      this.forexPresenter.handleQuoteCurrencyListVisibility();
    },
    onToggle: function(flxDropDownCollapsed) {
      if (flxDropDownCollapsed) {
        this.view.flxScrollCurrencyCode.isVisible = true;
        this.view.imgDropDownIcon.src = "listboxdownarrow.png"
       } else {
        this.view.flxScrollCurrencyCode.isVisible = false;
        this.view.imgDropDownIcon.src = "listboxuparrow.png"
       }
       this.view.forceLayout();
    },
    
    showConvertedValues:function(){
      if (this.flxPlusCollapsed) {
        this.view.flxToCurrencyCodeContainer.skin = "ICSknFlxffffffBorder003e751pxRadius2px";
        this.view.flxSegConvertedValues.isVisible = true;
        this.view.imgPlus.src = "plus_blue.png"
        this.flxPlusCollapsed=false;
      } else {
        this.view.flxToCurrencyCodeContainer.skin = "flxe3e3e3e30pxradius";        
        this.view.flxSegConvertedValues.isVisible = false;
        this.view.imgPlus.src = "plus_icon.png"
        this.flxPlusCollapsed=true;
      }
       this.view.forceLayout();
    },
    
    onFilterRowClick : function(segWidget, sectionNumber, rowNumber){
      var data = segWidget.data[rowNumber];
      this.view.lblSelectedValue.text = data.lblName;
      this.forexPresenter.setMarketFilter(data.lblName);
      this.view.flxSegFilter.setVisibility(false);
      this.view.lblDropDown.text = "O";
      this.view.forceLayout();
    },
    getWidgetDataforMarketValues:function(){
      return{
      "lblTitle":"header",
        "lblValue":"value"
      }
        
    },
   
    onConversionChoiceSelection : function(segWidget, sectionNumber, rowNumber){
      this.forexPresenter.handleConversionChoiceSelection(rowNumber);
    },

    uiSettingsOnConversionChoiceSelection : function(baseCurrency, quoteCurrency, conversionChoice) {
      var baseCurrencyValue = Number(this.view.tbxBaseCurrencyCodeValue.text);
      var conversionChoiceValue = Number(conversionChoice.value);
      conversionChoice.value = this.forexPresenter.formatRateValue(baseCurrencyValue * conversionChoiceValue);
      this.setCalculatedRate(conversionChoice);
      this.view.lblExchangeRate.text = "1 " +  baseCurrency + " = " + conversionChoice.rate + " " + quoteCurrency;
      this.view.flxSegConvertedValues.isVisible = false;
      this.view.flxToCurrencyCodeContainer.skin = "flxe3e3e3e30pxradius"; 
      this.view.imgPlus.src = "plus_icon.png";
      this.flxPlusCollapsed = true;
      this.view.forceLayout();
    },
    
    setCalculatedRate : function(conversionChoice) {
      this.view.lblTTBuy.text = conversionChoice.header;
      this.view.lblTTBuyValue.text = conversionChoice.value;
    },
    reSettingTextBaseCurrency : function(){
      this.view.tbxBaseCurrencyCodeValue.text="1";
    },
    setDefaultQuoteCurrency : function() {
      var record = this.view.segToCuurencyCode.data[0][1][0];
      this.forexPresenter.handleQuoteCurrencyCodeSelection(record);      
    },
    
    setDefaultConversionChoice : function() {
      this.forexPresenter.handleConversionChoiceSelection(0);      
    },
    
    convert : function(){
      var amount = this.view.tbxBaseCurrencyCodeValue.text;
      this.forexPresenter.convertCurrency(amount);
    },
    
    closeSegments : function(){
      this.view.flxSegFilter.setVisibility(false);
      this.view.flxScrollCurrencyCode.setVisibility(false);
      this.view.flxSegConvertedValues.setVisibility(false);
      this.view.flxToCurrencyCodeContainer.skin = "flxe3e3e3e30pxradius"; 
      this.view.imgPlus.src = "plus_icon.png";
      this.view.imgDropDownIcon.src = "listboxuparrow.png";
      this.view.lblDropDown.text = "O";
      this.view.forceLayout();
    },
    
    setBaseCurrencyCodeValue : function(value) {
      if(!kony.sdk.isNullOrUndefined(value)) {
        this.view.tbxBaseCurrencyCodeValue.text = value;
      }
    },
    
    closeErrorMessage : function(){
      this.view.flxMainWrapper.setVisibility(false);
      this.view.forceLayout();
    },
    
    setFilterVisibility : function(){
      var isSelectedDataAvailable = !this.view.flxNoSelectedCurrency.isVisible;
      var isRecentDataAvailable = !this.view.flxNoRecentRecords.isVisible;
      this.view.flxFilterContainer.isVisible = (isSelectedDataAvailable || isRecentDataAvailable);
    }
  };
});