define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onSelection defined for lbxPayFrom **/
    AS_ListBox_e668ec3ced7440e89fc8137ad2c37355: function AS_ListBox_e668ec3ced7440e89fc8137ad2c37355(eventobject) {
        var self = this;
        this.getFrequencyAndFormLayout();
    },
    /** onSelection defined for lbxCategory **/
    AS_ListBox_ee91d7b876c74753adfe1151eec05a57: function AS_ListBox_ee91d7b876c74753adfe1151eec05a57(eventobject) {
        var self = this;
        this.getFrequencyAndFormLayout();
    },
    /** postShow defined for oneTimePay **/
    AS_FlexContainer_ga9fabdac7b440cfa326026e6f5862a6: function AS_FlexContainer_ga9fabdac7b440cfa326026e6f5862a6(eventobject) {
        var self = this;
        this.renderCalendar();
    }
});