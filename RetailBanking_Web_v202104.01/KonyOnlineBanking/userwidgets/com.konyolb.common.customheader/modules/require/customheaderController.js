/*eslint require-jsdoc:0
          complexity:0
*/

define(['CommonUtilities', 'TopbarConfig', 'FormControllerUtility','ViewConstants'], function (CommonUtilities, TopbarConfig, FormControllerUtility,ViewConstants) {
  function getTotalHeight() {
        return kony.os.deviceInfo().screenHeight;
//     var height = 0;
//     var widgets = kony.application.getCurrentForm().widgets();
//     for (var i = 0; i < 3; i++) {
//       var widget = widgets[i];
//       height += widget.frame.height;
//     }
//     height += kony.application.getCurrentForm().flxFooter.frame.y;
//     return height;
  }
  var orientationHandler = new OrientationHandler();
  return {
    setupLogout: function () {
      this.view.customhamburger.flxLogout.onClick = this.showLogout.bind(this);
      if (TopbarConfig.config.LOGOUT.excludedForms.indexOf(kony.application.getCurrentForm().id) < 0) {
        this.view.headermenu.btnLogout.text = "";
        this.view.headermenu.btnLogout.setVisibility(true);
        this.view.headermenu.imgLogout.setVisibility(true);
        this.view.headermenu.btnLogout.onTouchEnd = this.showLogout.bind(this);
      }
    },
    showLogout: function () {
      var currentForm = kony.application.getCurrentForm();
      if ('flxLogout' in currentForm) {
        this.closeHamburgerMenu();
        var children = currentForm.widgets();
        var footerHeight = 0;
        if(!(currentForm.id == "frmLocateUs" && orientationHandler.isMobile)){
          footerHeight = currentForm.flxFooter ? currentForm.flxFooter.info.frame.height +  (currentForm.flxFooter.info.frame.y-(children[0].info.frame.height+ children[1].info.frame.height)): 0;
        }
        var height = children[0].info.frame.height + children[1].info.frame.height + footerHeight;
        currentForm.flxLogout.setVisibility(true);
        currentForm.flxLogout.height = height + "dp";
        currentForm.flxLogout.left = "0%";
        var popupComponent = currentForm.flxLogout.widgets()[0];
        popupComponent.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.logout");
        popupComponent.lblHeading.accessibilityConfig = {
          "a11yLabel": kony.i18n.getLocalizedString("i18n.common.logout")
        };
        popupComponent.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.common.LogoutMsg");
        popupComponent.lblPopupMessage.accessibilityConfig = {
          "a11yLabel": kony.i18n.getLocalizedString("i18n.common.LogoutMsg")
        };
        popupComponent.lblcross.accessibilityConfig = {
          "a11yLabel" :  kony.i18n.getLocalizedString("i18n.Signout.quitSignout")
        };
        popupComponent.top = ((kony.os.deviceInfo().screenHeight / 2) - 135 ) + "px";
        popupComponent.btnYes.onClick = function () {
          FormControllerUtility.showProgressBar(kony.application.getCurrentForm());
          TopbarConfig.config.LOGOUT.onClick();
          currentForm.flxLogout.left = "-100%";
        };
        popupComponent.btnNo.onClick = function () {
          currentForm.flxLogout.left = "-100%";
        }
        popupComponent.flxCross.onClick = function () {
          currentForm.flxLogout.left = "-100%";
        }
      }
    },
 
    initHeader: function () {
      FormControllerUtility.updateWidgetsHeightInInfo(this.view, ["FlexContainer0e2898aa93bca45"]);
      var configurationManager = applicationManager.getConfigurationManager();
      this.setupContextualActions();
      this.setupHamburger();      
      this.setupLogout();
      this.setupUserActions();
      this.setHoverSkins();
      this.setCustomHeaderLogo();
      var messageText = this.view.headermenu.imgMessages.text;
      CommonUtilities.setText(this.view.headermenu.imgMessages, kony.i18n.getLocalizedString("i18n.a11y.common.messages"), CommonUtilities.getaccessibilityConfig());
      this.view.headermenu.imgMessages.text = messageText;
       CommonUtilities.setText(this.view.headermenu.imgUserReset, kony.i18n.getLocalizedString("i18n.a11y.common.userSettings"), CommonUtilities.getaccessibilityConfig());
      flag = 0;
      if (configurationManager.isFastTransferEnabled == "true") 
        {
      		this.view.topmenu.lblTransferAndPay.text = kony.i18n.getLocalizedString("i18n.hamburger.transfers");
            this.view.topmenu.lblTransferAndPay.toolTip = kony.i18n.getLocalizedString("i18n.hamburger.transfers");
            this.view.topmenu.lblMyBills.toolTip = kony.i18n.getLocalizedString("i18n.Pay.MyBills");
            this.view.topmenu.imgLblTransfers.toolTip = kony.i18n.getLocalizedString("i18n.hamburger.transfers");
        }
      if(kony.application.getCurrentForm().id !== "frmEnrollBusiness")
      this.view.topmenu.flxaccounts.onClick = TopbarConfig.config.ACCOUNTS.onClick;
      this.view.topmenu.flxMyBills.onClick = TopbarConfig.config.BILLS.onClick;
      this.view.headermenu.flxNotifications.onClick = TopbarConfig.config.NOTIFICATIONS.onClick;
      this.view.headermenu.flxMessages.onClick = TopbarConfig.config.MESSAGES.onClick;
      this.view.topmenu.flxHelp.onClick = TopbarConfig.config.HELP.onClick;
      this.view.topmenu.flxFeedbackimg.onClick = TopbarConfig.config.FEEDBACK.onClick;
      this.view.imgKony.onTouchEnd= this.showDashboardScreen;
      this.view.topmenu.flxCombinedAccessMenu.isVisible = false;
      this.onBreakpointChangeComponent();
    },
    
     showDashboardScreen: function(){
      if(applicationManager.getUserPreferencesManager().isUserLoggedin()) {
        var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
        accountsModule.presentationController.showAccountsDashboard();
        }
     },
    setCustomHeaderLogo : function() {   
      CommonUtilities.setText(this.view.imgKony, kony.i18n.getLocalizedString("i18n.a11y.common.logo"), CommonUtilities.getaccessibilityConfig());
      var configurationManager = applicationManager.getConfigurationManager();
      if(configurationManager.isSMEUser === "true") {
        this.view.imgKony.src = ViewConstants.INFINITY_LOGOS.SME;
        this.setHeaderLogoResponsiveForSMEandMBB();
      }
      else if(configurationManager.isMBBUser === "true"){
        this.view.imgKony.src = ViewConstants.INFINITY_LOGOS.MBB;
        this.setHeaderLogoResponsiveForSMEandMBB();
      }
      else if(configurationManager.isRBUser === "true")this.view.imgKony.src = ViewConstants.INFINITY_LOGOS.RB;
      else this.view.imgKony.src = ViewConstants.INFINITY_LOGOS.GENERIC;
    },
    setHeaderLogoResponsiveForSMEandMBB : function() {
      var width = kony.application.getCurrentBreakpoint();
      var orientationHandler = new OrientationHandler();
      
      if(orientationHandler.isDesktop) {
        if(width === 1366){
          this.view.imgKony.width = "175dp";
          this.view.imgKony.left = "70dp";
        }
        else if(width === 1380 || width === constants.BREAKPOINT_MAX_VALUE){
          this.view.imgKony.width = "180dp";
          this.view.imgKony.left = "72dp";
        }
      }
      else if(orientationHandler.isTablet){
        if(width === 1024){
          this.view.imgKony.width = "175dp";
          this.view.imgKony.left = "70dp";
        }
        else if(width === 1366){
          this.view.imgKony.width = "175dp";
          this.view.imgKony.left = "72dp";
        }
        else if(width === 1380 || width === constants.BREAKPOINT_MAX_VALUE){
          this.view.imgKony.width = "180dp";
          this.view.imgKony.left = "75dp";
        }
      }
  },
   setupUserActions: function() {
      this.view.segUserActions.widgetDataMap = {
        lblSeparator: "lblSeparator",
        lblUsers: "lblUsers"
      };
      var actions = TopbarConfig.config.USER_ACTIONS.filter(function(action){
        return (!action.isVisible || action.isVisible());
      })
      this.view.segUserActions.setData(actions.map(function(configItem) {
        return {
          lblSeparator: "L",
          lblUsers: {
            "text": kony.i18n.getLocalizedString(configItem.text),
            "toolTip": kony.i18n.getLocalizedString(configItem.text)
          }
        }
      }));
      this.view.segUserActions.onRowClick = function(widget, section, rowIndex) {
        actions[rowIndex].onClick();
      }
    },
    
   /**
     * Method will set email attribute to  user from  entitlement configuration  
   **/
   setEmailForUserObj : function() {
    var userEntitlementsEmailIds = applicationManager.getUserPreferencesManager().getEntitlementEmailIds();
      for(var i = 0; i < userEntitlementsEmailIds.length; i++) {
           if (userEntitlementsEmailIds[i].isPrimary === "true") {
               return userEntitlementsEmailIds[i].Value;
           }
       }
       return "";
    },
    setupUserProfile: function () {
      var userObject = applicationManager.getUserPreferencesManager().getUserObj();
      var userImageURL = applicationManager.getUserPreferencesManager().getUserImage();
       if(applicationManager.getConfigurationManager().getProfileImageAvailabilityFlag() === true && userImageURL && userImageURL.trim() != "") 
        this.view.headermenu.imgUserReset.base64 = userImageURL ;
      else
        this.view.headermenu.imgUserReset.src =  ViewConstants.IMAGES.USER_DEFAULT_IMAGE;
      if(applicationManager.getConfigurationManager().getProfileImageAvailabilityFlag() === true && userImageURL && userImageURL.trim() != "") 
        this.view.CopyimgToolTip0i580d9acc07c42.base64 = userImageURL;
      else
        this.view.CopyimgToolTip0i580d9acc07c42.src =  ViewConstants.IMAGES.USER_DEFAULT_IMAGE;
      this.view.lblName.text = (userObject.userlastname === null) ? userObject.userfirstname : (userObject.userfirstname === null) ? userObject.userlastname : userObject.userfirstname + " " + userObject.userlastname;
      this.view.lblUserEmail.text = this.setEmailForUserObj();
      this.view.flxUserActions.isVisible = false;
    },
    setupContextualActions: function () {
     var scope = this;
            var configurationManager = applicationManager.getConfigurationManager();
            if(!this.isTransfersAndPayEnabled() && !this.isBillPayEnabled() && applicationManager.getUserPreferencesManager().isLoggedIn === true){
                this.view.topmenu.flxTransfersAndPay.setVisibility(false);
              	this.view.topmenu.flxSeperator1.setVisibility(false);
                this.view.topmenu.imgLblTransfers.setVisibility(false);
                this.view.topmenu.lblTransferAndPay.setVisibility(false);
            }else{
                 this.view.topmenu.flxTransfersAndPay.onClick = this.openContextualMenu.bind(this);
                 this.view.topmenu.flxTransfersAndPay.onTouchStart = function () {
                   if (scope.view.topmenu.flxContextualMenu.isVisible) {
                    scope.view.topmenu.flxTransfersAndPay.origin = true;
                    if(kony.application.getCurrentBreakpoint()==640 || kony.application.getCurrentBreakpoint()==1024){
                          scope.view.topmenu.flxContextualMenu.isVisible = false;
                          scope.view.topmenu.flxTransfersAndPay.skin="flxHoverSkinPointer";
                          scope.view.topmenu.imgLblTransfers.text = "O";
                   }
                }
                 }
                TopbarConfig.config.getContextualMenu().forEach(function(contextualItem) {
                scope.view.topmenu[contextualItem.widget].setVisibility(!contextualItem.isVisible || contextualItem.isVisible())
                scope.view.topmenu[contextualItem.widget].onClick = contextualItem.onClick;
            });
            }
    },
     openContextualMenu: function() {
            var scope = this;
            if (scope.view.topmenu.flxTransfersAndPay.skin === "sknFlxHeaderTransfersSelected") {
                scope.view.topmenu.flxTransfersAndPay.skin = "flxHoverSkinPointer";
                scope.view.topmenu.flxTransfersAndPay.hoverSkin = "flxHoverSkinPointer000000op10";
                scope.view.topmenu.imgLblTransfers.text = "O";
            } else {
                scope.view.topmenu.flxTransfersAndPay.skin = "sknFlxHeaderTransfersSelected";
                scope.view.topmenu.imgLblTransfers.text = "P";
            }
            scope.view.topmenu.showContextualMenu();
        },
    setupHamburger: function () {
      var scope = this;
      //this.view.customhamburger.setItemSelectListener(this.closeHamburgerMenu.bind(this));
      this.view.topmenu.flxMenu.onClick = function(){
        scope.openHamburgerMenu();
      }
      this.view.flxHamburgerBack.onClick = this.closeHamburgerMenu.bind(this);
      this.view.customhamburger.flxClose.onClick = this.closeHamburgerMenu.bind(this);
    },
    openHamburgerMenu: function () {
      // For Scroll to top.
      this.view.imgKony.setFocus(true);
      // if(kony.application.getCurrentBreakpoint()==640 || kony.application.getCurrentBreakpoint()==1024){
      //   this.view.flxHamburger.height = (kony.os.deviceInfo().screenHeight-15) + "dp";
      //   this.view.customhamburger.flxMenu.height = (kony.os.deviceInfo().screenHeight-15) + "dp";
      // }else{
        this.view.flxHamburger.height = kony.os.deviceInfo().screenHeight + "dp";
        this.view.customhamburger.flxMenu.height = (kony.os.deviceInfo().screenHeight) + "%";
        this.view.customhamburger.flxMenuWrapper.height = (kony.os.deviceInfo().screenHeight-160) + "dp";

        this.view.customhamburger.flxLogout.top = (kony.os.deviceInfo().screenHeight-80) + "dp";
        this.view.customhamburger.FlexContainer0a82014659b0e48.top = (kony.os.deviceInfo().screenHeight-100) + "dp";
        this.view.customhamburger.CopyFlexContainer0f46534a0a94e41.top = (kony.os.deviceInfo().screenHeight-100) + "%";
      // }

      this.showBackFlex();
      var scope = this;
      var animationDefinition = {
        100: {
          "left": 0
        }
      };
      var animationConfiguration = {
        duration: 0.8,
        fillMode: kony.anim.FILL_MODE_FORWARDS
      };
      var callbacks = {
        animationEnd: function () {
          scope.view.customhamburger.forceLayout();
        }
      };
      var animationDef = kony.ui.createAnimation(animationDefinition);
      this.view.flxHamburger.animate(animationDef, animationConfiguration, callbacks);
    },
    closeHamburgerMenu: function () {
      var self = this;
      var animationDefinition = {
        100: {
          "left": "-200.13%"
        }
      };
      var animationConfiguration = {
        duration: 0.8,
        fillMode: kony.anim.FILL_MODE_FORWARDS
      };
      var callbacks = {
        animationEnd: function () {
          self.hideBackFlex();
        }
      };
      var animationDef = kony.ui.createAnimation(animationDefinition);
      this.view.flxHamburger.animate(animationDef, animationConfiguration, callbacks);
    },
    showBackFlex: function () {
      this.view.flxHamburgerBack.height = getTotalHeight() + "px";
      this.view.flxHamburgerBack.isVisible = true;
      this.view.flxHamburger.isVisible = true;
      this.view.forceLayout();
    },
    hideBackFlex: function () {
      this.view.flxHamburgerBack.height = "0px";
      this.view.forceLayout();
    },
    showUserActions: function () {
      if(this.view.FlexContainer0e2898aa93bca45.info.frame.x==0){
        this.view.flxUserActions.left = -410 + this.view.FlexContainer0e2898aa93bca45.info.frame.width + "dp";
        if(kony.application.getCurrentBreakpoint()==1024|| orientationHandler.isTablet){
          this.view.flxUserActions.left = -365 + this.view.FlexContainer0e2898aa93bca45.info.frame.width + "dp";
        }
      }else{
        this.view.flxUserActions.left = 945 + this.view.FlexContainer0e2898aa93bca45.info.frame.x + "dp";
      }
      if (this.view.flxUserActions.isVisible === false) {
        this.view.headermenu.imgDropdown.src = "profile_dropdown_uparrow.png";
        this.view.flxUserActions.isVisible = true;
        this.view.lblName.setFocus(true);
      } else {
        this.view.headermenu.imgDropdown.src = "profile_dropdown_arrow.png";
        this.view.flxUserActions.isVisible = false;
      }
    },
    forceCloseHamburger: function () {
      this.hideBackFlex();
      this.view.flxHamburger.left = "-200%";
      this.view.forceLayout();
    },
    isBillPayEnabled: function () {
      return applicationManager.getConfigurationManager().checkUserFeature("BILL_PAY")
    },
    isTransfersAndPayEnabled: function () {
      return [
         "INTERNATIONAL_ACCOUNT_FUND_TRANSFER",
        "INTER_BANK_ACCOUNT_FUND_TRANSFER",
        "INTRA_BANK_FUND_TRANSFER",
        "TRANSFER_BETWEEN_OWN_ACCOUNT",
        "P2P"
    ].some(function (permission) {
      return applicationManager.getConfigurationManager().checkUserFeature(permission);
    })
    },
    postShowFunction: function() {
      this.view.flxUserActions.left = 945 + this.view.FlexContainer0e2898aa93bca45.frame.x + "dp";

      var configurationManager = applicationManager.getConfigurationManager();
      this.setupUserProfile();
      if (configurationManager.isFastTransferEnabled == "true") {
              if (this.isTransfersAndPayEnabled()) {
                     if (kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile){
                    	this.view.topmenu.flxSeperator1.setVisibility(false);
                       	this.view.topmenu.flxTransfersAndPay.setVisibility(false);
                     }
                    else{
                       this.view.topmenu.flxSeperator1.setVisibility(true);
                       this.view.topmenu.flxTransfersAndPay.setVisibility(true);
                    }
                } else {
                  	this.view.topmenu.flxSeperator1.setVisibility(false);
                    this.view.topmenu.flxTransfersAndPay.setVisibility(false);
                }
                if (kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile || !this.isBillPayEnabled())
                {
                this.view.topmenu.flxSeperator4.setVisibility(false);
                this.view.topmenu.flxMyBills.setVisibility(false);
                }
                else
                {
                  this.view.topmenu.flxSeperator4.setVisibility(true);
                  this.view.topmenu.flxMyBills.setVisibility(true);  
                }
            }
      this.view.flxHamburger.setVisibility(false);
      this.view.forceLayout();
      this.view.topmenu.flxFeedback.setVisibility(false);
      this.view.topmenu.flxHelp.setVisibility(false);
  },

    
    setHoverSkins: function() {
      this.view.imgKony.cursorType="pointer";
      var currForm = kony.application.getCurrentForm().id;
       this.setDefaultHoverSkins();
       this.view.topmenu.flxContextualMenu.isVisible = false;
       this.view.topmenu.flxCombinedAccessMenu.isVisible = false;
       this.view.topmenu.imgLblTransfers.text = "O";
       this.setDefaultSkins();
       if(currForm === 'frmAccountsDetails' || currForm === 'frmAccountsLanding' || currForm === 'frmBBAccountsLanding' || currForm === 'frmDashboard' || currForm === 'frmEnrollBusiness')
         this.view.topmenu.flxaccounts.hoverSkin = "sknFlxFFFFFbrdr3343a8Pointer";
       else if(currForm === 'frmTransfers' || currForm === 'frmPayAPerson' || currForm === 'frmWireTransfer' || currForm === 'frmAcknowledgement' || currForm === 'frmConfirm' || currForm === 'frmVerifyAccount')
         this.view.topmenu.flxTransfersAndPay.hoverSkin = "sknFlxFFFFFbrdr3343a8Pointer";
       else if(currForm === 'frmCustomerFeedback' || currForm === 'frmCustomerFeedbackSurvey'){
         this.view.topmenu.flxFeedbackimg.hoverSkin = "sknFlxFFFFFbrdr3343a8Pointer";
         this.view.topmenu.flxFeedbackimg.skin = "sknFlxFFFFFbrdr3343a8Pointer";
         this.view.topmenu.lblFeedback.skin = "sknLblffffff15pxSSP";
       }
       else if(currForm === 'frmOnlineHelp')
         this.view.topmenu.flxHelp.hoverSkin = "sknFlxFFFFFbrdr3343a8Pointer"; 
      else if(currForm === 'frmBillPay')
        {
         this.view.topmenu.flxMyBills.skin = "sknFlxFFFFFbrdr3343a8Pointer";
         this.view.topmenu.flxMyBills.hoverSkin = "sknFlxFFFFFbrdr3343a8Pointer"; 
        }
    },
    setDefaultHoverSkins: function() {
       this.view.topmenu.flxaccounts.hoverSkin = "flxHoverSkinPointer000000op10";
       this.view.topmenu.flxTransfersAndPay.hoverSkin = "flxHoverSkinPointer000000op10";
       this.view.topmenu.flxMyBills.hoverSkin = "flxHoverSkinPointer000000op10";
    }  ,
       setDefaultSkins: function(){
       this.view.topmenu.flxMyBills.skin = "flxHoverSkinPointer";
    },
    onBreakpointChangeComponent: function(width){
      if(width==undefined || width==null){
        width = kony.application.getCurrentBreakpoint();
      }
      var scope = this;

      this.view.flxHamburger.height = kony.os.deviceInfo().screenHeight + "dp";
      this.view.customhamburger.flxMenu.height = (kony.os.deviceInfo().screenHeight) + "dp";
      this.view.customhamburger.flxMenuWrapper.height = (kony.os.deviceInfo().screenHeight-160) + "dp";
      this.view.customhamburger.flxLogout.top = (kony.os.deviceInfo().screenHeight-80) + "dp";
      this.view.customhamburger.FlexContainer0a82014659b0e48.top = (kony.os.deviceInfo().screenHeight-100) + "dp";
      this.view.customhamburger.CopyFlexContainer0f46534a0a94e41.top = (kony.os.deviceInfo().screenHeight-100) + "dp";

      this.view.flxUserActions.setVisibility(false);
      this.view.lblHeaderMobile.setVisibility(false);

      if(this.isPreLoginView){
        this.showPreLoginView();
      }else{
        this.showPostLoginView();
      }

      if(width === 640 || orientationHandler.isMobile){
        scope.view.customhamburger.width = "90%";
        scope.view.imgKony.isVisible=false;
        scope.view.customhamburger.imgKony.left = "12%";
        scope.view.customhamburger.flxClose.left= "86.5%";
        this.view.lblHeaderMobile.setVisibility(true);
      }else if(width === 1024 || width === 768 || orientationHandler.isTablet){
        scope.view.customhamburger.width = "60%";
        scope.view.imgKony.isVisible=true;
        scope.view.imgKony.left="20dp";
        scope.view.headermenu.right="15dp";
        scope.view.topmenu.flxCombinedAccess.right="15dp";
        scope.view.topmenu.flxCombinedAccess.right="18dp";
        scope.view.topmenu.flxFeedback.right="-0.5%";
        scope.view.topmenu.flxFeedback.width="130dp";
        scope.view.topmenu.lblFeedback.right = "0px";
        scope.view.topmenu.flxverseperator2.right = "17dp"
        scope.view.topmenu.flxHelp.right = "132dp";
        scope.view.topmenu.flxHelp.width = "50dp";
        scope.view.topmenu.flxCombined.right = "0dp";
        scope.view.topmenu.lblHelp.centerx="50%";
        scope.view.topmenu.lblHelp.centerX = "50%";
      }else if(width===1366){
        scope.view.FlexContainer0e2898aa93bca45.width="88%"
        scope.view.FlexContainer0e2898aa93bca45.centerx="50%";
        scope.view.FlexContainer0e2898aa93bca45.centerX="50%";

        scope.view.headermenu.right="-10dp";
        scope.view.topmenu.flxCombinedAccess.right="-10dp";
        scope.view.topmenu.flxCombined.right="-13dp";
        
        scope.view.flxTopmenu.width = "100%"
        scope.view.topmenu.width = "100%";
        scope.view.topmenu.flxMenusMain.width = "100%"
        scope.view.topmenu.flxMenu.left="0dp"
        scope.view.topmenu.flxMenu.width="45dp";
        scope.view.topmenu.imgMenu.left= "-2dp";

        scope.view.topmenu.flxFeedback.right="-3.5%";
        scope.view.topmenu.flxFeedback.width="130dp";
        scope.view.topmenu.lblFeedback.right = "0px";
        scope.view.topmenu.flxverseperator2.right = "17dp"
        scope.view.topmenu.flxHelp.right = "102dp";
        scope.view.topmenu.flxHelp.width = "50dp";
        scope.view.topmenu.flxCombined.right = "0dp";
        scope.view.topmenu.lblHelp.centerx="50%";
        scope.view.topmenu.lblHelp.centerX = "50%";

        scope.view.imgKony.isVisible=true;
        scope.view.imgKony.left="0dp";
        scope.view.imgKony.width="100dp";


        scope.view.customhamburger.width = "500dp";
        scope.view.customhamburger.flxClose.left= "92%";
      }else{
        if(kony.application.getCurrentForm().id === "frmEnrollBusiness"){
           scope.view.FlexContainer0e2898aa93bca45.width = "88%";
        }
        else {
           scope.view.FlexContainer0e2898aa93bca45.width = "1366dp";
        }
        scope.view.FlexContainer0e2898aa93bca45.centerx="50%";
        scope.view.FlexContainer0e2898aa93bca45.centerX="50%";

        if(width === 1920 || width >1366) {
			scope.view.FlexContainer0e2898aa93bca45.centerX = "50%";
          //scope.view.FlexContainer0e2898aa93bca45.left = "4.5%";
        }
        
        //scope.view.headermenu.right="-7dp";
	    //scope.view.topmenu.flxCombinedAccess.right="-7dp";
        //scope.view.topmenu.flxCombined.right="-10dp";
        scope.view.flxTopmenu.width = "100%"
        scope.view.topmenu.width = "100%";
        scope.view.topmenu.flxMenusMain.width = "100%"
        //scope.view.topmenu.flxMenu.left = "0dp";
        scope.view.topmenu.flxMenu.width="45dp";
        //scope.view.topmenu.imgMenu.left= "-2dp";

        //scope.view.topmenu.flxFeedback.right="-3.5%";
        scope.view.topmenu.flxFeedback.width="130dp";
        //scope.view.topmenu.lblFeedback.right = "0px";
        //scope.view.topmenu.flxverseperator2.right = "15dp"
        //scope.view.topmenu.flxHelp.right = "102dp";
        scope.view.topmenu.flxHelp.width = "50dp";
        //scope.view.topmenu.flxCombined.right = "0dp";
        scope.view.topmenu.lblHelp.centerx="50%";
        scope.view.topmenu.lblHelp.centerX = "50%";

        scope.view.imgKony.isVisible=true;
        //scope.view.imgKony.left="0dp";
        scope.view.imgKony.width="100dp";

        scope.view.customhamburger.width = "28%";
        scope.view.customhamburger.flxClose.left= "92%";
      }
       scope.view.flxHamburger.width = "100%";
      
      if(width==640 || orientationHandler.isMobile){
        this.view.topmenu.flxTransfersAndPay.onTouchStart = null;
      }else if(width==1024 || orientationHandler.isTablet){
        this.view.topmenu.flxTransfersAndPay.onTouchStart = null;
      }else{

      }
      this.view.forceLayout();
    },
    isPreLoginView: false,
    showPreLoginView: function(){
      this.isPreLoginView = true;

      if(orientationHandler.isMobile || kony.application.getCurrentBreakpoint()==640){
        this.view.topmenu.flxMenusMain.setVisibility(false);
        this.view.flxTopmenu.setVisibility(true);
        this.view.flxBottomBlue.setVisibility(true);
      }else{
        this.view.height = "70dp";
        this.view.parent.height = "70dp";
        this.view.FlexContainer0e2898aa93bca45.height = "70dp";
        this.view.flxTopmenu.setVisibility(false);
        this.view.flxBottomContainer.height = "70dp";
        this.view.flxBottomBlue.setVisibility(false);
        this.view.flxSeperatorHor2.top = "70dp";
        this.view.headermenu.flxNotifications.isVisible = false;
        this.view.headermenu.flxMessages.isVisible = false;
        this.view.headermenu.flxVerticalSeperator1.isVisible = false;
        this.view.headermenu.flxVerticalSeperator3.isVisible = false;
        this.view.headermenu.flxResetUserImg.isVisible = false;
        this.view.headermenu.flxUserId.isVisible = false;            
        this.view.headermenu.btnLogout.toolTip = kony.i18n.getLocalizedString("i18n.common.login");
        this.view.headermenu.btnLogout.setVisibility(true);
        this.view.headermenu.imgLogout.src = "login_icon_locateus.png";
        this.view.headermenu.imgLogout.toolTip = kony.i18n.getLocalizedString("i18n.common.login");
        this.view.headermenu.flxWarning.right = "40dp";
        this.view.headermenu.flxWarning.setVisibility(true);
      }
      this.view.imgKony.onTouchEnd= function(){
            var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
            authModule.presentationController.showLoginScreen();
          };
    },
    showPostLoginView: function(){
      var isRetailUser = applicationManager.getConfigurationManager().getConfigurationValue('isRBUser') === "true";
      this.isPreLoginView = false;

      if(orientationHandler.isMobile || kony.application.getCurrentBreakpoint()==640){
        this.view.topmenu.flxMenusMain.setVisibility(true);
        this.view.flxTopmenu.setVisibility(true);
        this.view.flxBottomBlue.setVisibility(true);
      }else{
        this.view.height = "120dp";
        this.view.parent.height = "120dp";
        this.view.FlexContainer0e2898aa93bca45.height = "120dp";
        this.view.flxTopmenu.setVisibility(true);
        this.view.flxBottomContainer.height = "120dp";
        this.view.flxBottomBlue.setVisibility(true);
        this.view.flxSeperatorHor2.top = "120dp";

        this.view.headermenu.flxNotifications.isVisible = true;
        if(isRetailUser){
          this.view.headermenu.flxVerticalSeperator3.isVisible = true;
          this.view.headermenu.flxMessages.isVisible = true;
          this.view.headermenu.flxMessages.right = "210dp";
        }
        this.view.headermenu.flxVerticalSeperator1.isVisible = true;
        this.view.headermenu.flxResetUserImg.isVisible = true;
        this.view.headermenu.flxUserId.isVisible = true;            
        this.view.headermenu.btnLogout.toolTip = kony.i18n.getLocalizedString("i18n.common.logout");
        this.view.headermenu.imgMessages.toolTip = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Message");
        this.view.headermenu.lblNewNotifications.toolTip = kony.i18n.getLocalizedString("i18n.AlertsAndMessages.Alerts");
        this.view.headermenu.imgUserReset.toolTip = kony.i18n.getLocalizedString("i18n.ProfileManagement.Settingscapson");
        this.view.headermenu.btnLogout.setVisibility(true);
        this.view.headermenu.imgLogout.src = "logout.png";
        this.view.headermenu.imgLogout.toolTip = kony.i18n.getLocalizedString("i18n.common.logout");
        this.view.headermenu.flxWarning.setVisibility(false);
      }
    }
  };
});