define(function () {

  return {
    constructor: function (baseConfig, layoutConfig, pspConfig) {
      let scopeObj = this;
      scopeObj._primaryBtnEnableSkin = {};
      scopeObj._primaryBtnDisableSkin = {};
      scopeObj._flexSkins = "";
      scopeObj._textboxSkins = "";
      scopeObj._objectService = "";
      scopeObj._dataModel = "";
      scopeObj._dbxUserDataModel = "";
      scopeObj._getPasswordRulesAndPolicy = "";
      scopeObj._verifyExistingPassword = "";
      scopeObj._updatePassword = "";
      scopeObj._isCSRAssit = false;
      scopeObj.breakpoint = "";
      scopeObj.passwordPolicies = {
        "minLength": "",
        "maxLength": "",
        "specialCharactersAllowed": "",
        "atleastOneNumber": "",
        "atleastOneSymbol": "",
        "atleastOneUpperCase": "",
        "atleastOneLowerCase": "",
        "charRepeatCount": ""
      };
      scopeObj.passwordRegex = "";
      scopeObj.characterRepeatCountRegex = "";
    },
    //Logic for getters/setters of custom properties
    initGettersSetters: function () {
      defineSetter(this, "primaryBtnEnableSkin", function (val) {
        if ((typeof val === 'string') && (val !== "")) {
          this._primaryBtnEnableSkin = val;
        }
      });
      defineGetter(this, "primaryBtnEnableSkin", function () {
        return this._primaryBtnEnableSkin;
      });
      defineSetter(this, "primaryBtnDisableSkin", function (val) {
        if ((typeof val === 'string') && (val !== "")) {
          this._primaryBtnDisableSkin = val;
        }
      });
      defineGetter(this, "primaryBtnDisableSkin", function () {
        return this._primaryBtnDisableSkin;
      });
      defineSetter(this, "objectService", function (val) {
        if ((typeof val === 'string') && (val !== "")) {
          this._objectService = val;
        }
      });
      defineGetter(this, "objectService", function () {
        return this._objectService;
      });
      defineSetter(this, "dataModel", function (val) {
        if ((typeof val === 'string') && (val !== "")) {
          this._dataModel = val;
        }
      });
      defineGetter(this, "dataModel", function () {
        return this._dataModel;
      });
      defineSetter(this, "dbxUserDataModel", function (val) {
        if ((typeof val === 'string') && (val !== "")) {
          this._dbxUserDataModel = val;
        }
      });
      defineGetter(this, "dbxUserDataModel", function () {
        return this._dbxUserDataModel;
      });
      defineSetter(this, "getPasswordRulesAndPolicy", function (val) {
        if ((typeof val === 'string') && (val !== "")) {
          this._getPasswordRulesAndPolicy = val;
        }
      });
      defineGetter(this, "getPasswordRulesAndPolicy", function () {
        return this._getPasswordRulesAndPolicy;
      });
      defineSetter(this, "verifyExistingPassword", function (val) {
        if ((typeof val === 'string') && (val !== "")) {
          this._verifyExistingPassword = val;
        }
      });
      defineGetter(this, "verifyExistingPassword", function () {
        return this._verifyExistingPassword;
      });
      defineSetter(this, "updatePassword", function (val) {
        if ((typeof val === 'string') && (val !== "")) {
          this._updatePassword = val;
        }
      });
      defineGetter(this, "updatePassword", function () {
        return this._updatePassword;
      });
      defineSetter(this, "flexSkins", function (val) {
        if ((typeof val === 'string') && (val !== "")) {
          this._flexSkins = val;
        }
      });
      defineGetter(this, "flexSkins", function () {
        return this._flexSkins;
      });
      defineSetter(this, "textboxSkins", function (val) {
        if ((typeof val === 'string') && (val !== "")) {
          this._textboxSkins = val;
        }
      });
      defineGetter(this, "textboxSkins", function () {
        return this._textboxSkins;
      });
      defineSetter(this, "isCSRAssit", function(val) {
        if((typeof val === 'boolean')){
          this._isCSRAssit=val;
        }
      });
      defineGetter(this, "isCSRAssit", function() {
        return this._isCSRAssit;
      });
    },

    postShow: function () {
      this.onBreakpointChange();
      this.resetUI();
      this.setFlowActions();
      this.getPasswordPolicies();
    },

    onBreakpointChange: function () {
      let scopeObj = this;
      scopeObj.breakpoint = kony.application.getCurrentBreakpoint();
    },

    resetUI: function () {
      let scopeObj = this;
      scopeObj.setNormalSkin(scopeObj.view.flxExistingPasswordmod);
      scopeObj.setNormalSkin(scopeObj.view.flxNewPasswordmod);
      scopeObj.setNormalSkin(scopeObj.view.flxConfirmPasswordmod);
      scopeObj.view.tbxExistingPassword.text = "";
      scopeObj.view.tbxNewPassword.text = "";
      scopeObj.view.tbxConfirmPassword.text = "";
      scopeObj.view.tbxExistingPassword.skin = scopeObj.breakPointParser(scopeObj.textboxSkins);
      scopeObj.view.tbxNewPassword.skin = scopeObj.breakPointParser(scopeObj.textboxSkins);
      scopeObj.view.tbxConfirmPassword.skin = scopeObj.breakPointParser(scopeObj.textboxSkins);
      scopeObj.hideErrorMessages();
      scopeObj.enableOrDisbaleButton(scopeObj.view.btnEditPasswordProceed,false);
    },

    setFlowActions: function () {
      let scopeObj = this;

      scopeObj.view.tbxExistingPassword.onTouchStart = function () {
        scopeObj.setFocusSkin(scopeObj.view.flxExistingPasswordmod);
      };

      scopeObj.view.tbxExistingPassword.onKeyUp = function () {
        scopeObj.enableOrDisbaleButton(scopeObj.view.btnEditPasswordProceed, true);
      };

      scopeObj.view.tbxExistingPassword.onEndEditing = function () {
        scopeObj.setNormalSkin(scopeObj.view.flxExistingPasswordmod);
      };

      scopeObj.view.tbxNewPassword.onTouchStart = function () {
        scopeObj.setFocusSkin(scopeObj.view.flxNewPasswordmod);
      };

      scopeObj.view.tbxNewPassword.onEndEditing = function () {
        scopeObj.setNormalSkin(scopeObj.view.flxNewPasswordmod);
        scopeObj.validatePassword(scopeObj.view.tbxNewPassword.text);
      };

      scopeObj.view.tbxConfirmPassword.onTouchStart = function () {
        scopeObj.setFocusSkin(scopeObj.view.flxConfirmPasswordmod);
      };

      scopeObj.view.tbxConfirmPassword.onEndEditing = function () {
        scopeObj.setNormalSkin(scopeObj.view.flxConfirmPasswordmod);
        scopeObj.validatePassword(scopeObj.view.tbxConfirmPassword.text);
      };

      scopeObj.view.btnEditPasswordCancel.onClick = function () {
        scopeObj.view.setVisibility(false);
        if (scopeObj.onCancel)
          scopeObj.onCancel(); 
      };

      scopeObj.view.btnEditPasswordProceed.onClick = function () {
        if(!scopeObj.isCSRAssit){
          scopeObj.validateExistingPassword();
        }
      };

    },

    setFocusSkin: function (flexWidget) {
      flexWidget.skin = JSON.parse(this.flexSkins).focusSkin;
    },

    setNormalSkin: function (flexWidget) {
      flexWidget.skin = JSON.parse(this.flexSkins).normalSkin;
    },

    setErrorSkin: function (flexWidget) {
      flexWidget.skin = JSON.parse(this.flexSkins).errorSkin;
    },

    breakPointParser: function (inputJSON) {
      let jsonValue = (typeof inputJSON === "string") ? JSON.parse(inputJSON) : inputJSON;
      if (jsonValue.hasOwnProperty(this.breakpoint)) {
        return jsonValue[this.breakpoint];
      }
      else if (jsonValue["default"]) {
        return jsonValue["default"];
      }
      return jsonValue;
    },

    enableOrDisbaleButton: function (widget, isValidPassword) {
      let scopeObj = this;
      let existingPassword = scopeObj.view.tbxExistingPassword.text.trim();
      let password = scopeObj.view.tbxNewPassword.text.trim();
      let cnfPassword = scopeObj.view.tbxConfirmPassword.text.trim();
      let isEnabled =  isValidPassword && existingPassword && password && cnfPassword;
      isEnabled = isEnabled ? true : false;
      let skins = isEnabled ? scopeObj.breakPointParser(scopeObj.primaryBtnEnableSkin) : scopeObj.breakPointParser(scopeObj.primaryBtnDisableSkin);
      widget.setEnabled(isEnabled);
      widget.skin = skins.normalSkin;
      widget.hoverSkin = skins.hoverSkin;
      widget.focusSkin = skins.focusSkin;
    },

    validateResponse: function (status, response, error) {
      let res, isServiceFailure, data;
      if (status === kony.mvc.constants.STATUS_SUCCESS) {
        if (response.hasOwnProperty("errcode") || response.hasOwnProperty("dbpErrCode") || response.hasOwnProperty("errmsg") || response.hasOwnProperty("dbpErrMsg")) {
          data = {
            "errorCode": response.errcode ? response.errcode : response.dbpErrCode,
            "errorMessage": response.errmsg ? response.errmsg : response.dbpErrMsg,
            "serverErrorRes": response
          };
          res = {
            "status": false,
            "data": data,
            "isServerUnreachable": false
          };
        }
        else
          res = {
            "status": true,
            "data": response,
            "isServerUnreachable": false
          };
      }
      else {
        if (error.opstatus === 1011) {
          if (kony.os.deviceInfo().name === "thinclient" && kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY) === false) {
            location.reload(); //todo later so that it can be in sync with RB
          }
          else {
            isServiceFailure = true;
            errMsg = error.errmsg ? error.errmsg : error.dbpErrMsg;
          }
        }
        else {
          isServiceFailure = false;
          errMsg = error.errmsg ? error.errmsg : error.dbpErrMsg;
        }
        data = {
          "errorCode": error.errcode ? error.errcode : error.dbpErrCode,
          "errorMessage": error.errmsg ? error.errmsg : error.dbpErrMsg,
          "serverErrorRes": error
        };
        res = {
          "status": false,
          "data": data,
          "isServerUnreachable": isServiceFailure
        };
      }
      return res;
    },

    showErrorMessage: function (errorMessage) {
      let scopeObj = this;
      scopeObj.view.lblError1.text = errorMessage;
      scopeObj.view.flxErrorEditPassword.setVisibility(true);
      scopeObj.view.tbxExistingPassword.text = "";
      scopeObj.view.tbxNewPassword.text = "";
      scopeObj.view.tbxConfirmPassword.text = "";
      scopeObj.enableOrDisbaleButton(scopeObj.view.btnEditPasswordProceed,false);
      scopeObj.view.forceLayout();
      kony.application.dismissLoadingScreen();
    },

    hideErrorMessages: function () {
      let scopeObj = this;
      scopeObj.view.flxErrorEditPassword.setVisibility(false);
      scopeObj.view.flxPasswordCriteriaError.setVisibility(false);
      scopeObj.view.forceLayout();
    },

    getPasswordPolicies: function () {
      kony.application.showLoadingScreen();
      let scopeObj = this;
      let params = { "ruleForCustomer": true, "policyForCustomer": true };
      function completionCallback(status, data, error) {
        let response = scopeObj.validateResponse(status, data, error);
        if (response["status"]) {
          scopeObj.view.rtxRulesPassword.text = response.data.passwordpolicy.content;
          scopeObj.passwordPoliciesSuccessCallback(response.data.passwordrules);
        } else {
          scopeObj.view.rtxRulesPassword.text = response.data.errorMessage;
          kony.application.dismissLoadingScreen();
        }
      }
      let dataModelRepo = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository(scopeObj.dbxUserDataModel);
      dataModelRepo.customVerb(scopeObj.getPasswordRulesAndPolicy, params, completionCallback);
    },

    passwordPoliciesSuccessCallback: function (data) {
      let scopeObj = this;
      let policyData = "Minimum Length of Password:" + data.minLength + "\nMaximum Length of Password:" + data.maxLength + "\nSpecial Characters Allowed:" + data.supportedSymbols;
      if (data.atleastOneNumber === true)
        policyData += "\nAtleast One Number";
      if (data.atleastOneSymbol === true)
        policyData += "\nAtleast One Symbol";
      if (data.atleastOneUpperCase === true)
        policyData += "\nAtleast One Uppercase";
      if (data.atleastOneLowerCase === true)
        policyData += "\nAtleast One Lowercase";
      scopeObj.passwordPolicies.minLength = data.minLength;
      scopeObj.passwordPolicies.maxLength = data.maxLength;
      scopeObj.passwordPolicies.specialCharactersAllowed = data.supportedSymbols;
      scopeObj.passwordPolicies.atleastOneNumber = data.atleastOneNumber;
      scopeObj.passwordPolicies.atleastOneSymbol = data.atleastOneSymbol;
      scopeObj.passwordPolicies.atleastOneUpperCase = data.atleastOneUpperCase;
      scopeObj.passwordPolicies.atleastOneLowerCase = data.atleastOneLowerCase;
      scopeObj.passwordPolicies.charRepeatCount = data.charRepeatCount;
      kony.application.dismissLoadingScreen();
    },

    validatePassword: function (text) {
      let scopeObj = this;
      let data = scopeObj.passwordPolicies;
      if (scopeObj.passwordRegex === "" && scopeObj.characterRepeatCountRegex === "") {
        let repeatedCharRules = "(.)\\1{" + data.charRepeatCount + "}";
        scopeObj.characterRepeatCountRegex = new RegExp(repeatedCharRules);
        let passwordRules = "";
        if (data.specialCharactersAllowed && data.specialCharactersAllowed.includes("-")) {
          data.specialCharactersAllowed = data.specialCharactersAllowed.replace("-", "\\-");
        }
        if (data.atleastOneLowerCase) {
          passwordRules += "(?=.*\[a-z\])";
        }
        if (data.atleastOneUpperCase) {
          passwordRules += "(?=.*\[A-Z\])";
        }
        if (data.atleastOneNumber) {
          passwordRules += "(?=.*\\d)";
        }
        if (data.atleastOneSymbol) {
          passwordRules = passwordRules + "(?=(.*\[" + data.specialCharactersAllowed + "\]))";
          scopeObj.passwordRegex = new RegExp(passwordRules + "[A-Za-z0-9" + data.specialCharactersAllowed + "]{" + data.minLength + "," + data.maxLength + "}$");
        }
        else {
          scopeObj.passwordRegex = new RegExp(passwordRules + "\[^\\W\]{" + data.minLength + "," + data.maxLength + "}$");
        }
      }
      let isValidPassword = text.match(scopeObj.passwordRegex) && !scopeObj.characterRepeatCountRegex.test(text);
      scopeObj.hideErrorMessages();
      let isError = !isValidPassword;
      let errorMessage = isValidPassword ? "" : kony.i18n.getLocalizedString("i18n.enrollNow.validPassword");
      if(isValidPassword && scopeObj.view.tbxNewPassword.text.trim() !== "" && scopeObj.view.tbxConfirmPassword.text.trim() !== "" && scopeObj.view.tbxNewPassword.text !== scopeObj.view.tbxConfirmPassword.text){
        isError = true;
        errorMessage = kony.i18n.getLocalizedString("i18n.idm.newPasswordMismatch");
      }
      scopeObj.enableOrDisbaleButton(scopeObj.view.btnEditPasswordProceed,!isError);
      scopeObj.view.flxErrorEditPassword.setVisibility(isError);
      scopeObj.view.lblError1.text = errorMessage;
      
    },

    validateExistingPassword: function () {
      kony.application.showLoadingScreen();
      let scopeObj = this;
      scopeObj.hideErrorMessages();
      let params = { "password": scopeObj.view.tbxExistingPassword.text };
      function completionCallback(status, data, error) {
        let response = scopeObj.validateResponse(status, data, error);
        if (response.status) {
          if (response.data.result === "The user is verified") {
            scopeObj.updateUserPassword();
          } else if(response.data.result === "Invalid Credentials"){ 
            scopeObj.showErrorMessage(kony.i18n.getLocalizedString("i18n.idm.existingPasswordMismatch"));
          } else{
            scopeObj.showErrorMessage(kony.i18n.getLocalizedString("i18n.ProfileManagement.passwordExists"));
          }
        } else {
          scopeObj.showErrorMessage(kony.i18n.getLocalizedString("i18n.ProfileManagement.updateServerError"));
        }
      }
      let dataModelRepo = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository(scopeObj.dataModel);
      dataModelRepo.customVerb(scopeObj.verifyExistingPassword, params, completionCallback);
    },

    updateUserPassword: function () {
      let scopeObj = this;
      let params = {
        "oldPassword": scopeObj.view.tbxExistingPassword.text,
        "newPassword": scopeObj.view.tbxNewPassword.text
      };
      function completionCallback(status, data, error) {
        let response = scopeObj.validateResponse(status, data, error);
        if (response.status) {
          scopeObj.updateUserPasswordSuccessCallback(response.data);
        } else {
          scopeObj.showErrorMessage(response.data.errorMessage);
        }
      }
      let dataModelRepo = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository(scopeObj.dbxUserDataModel);
      dataModelRepo.customVerb(scopeObj.updatePassword, params, completionCallback);
    },

    updateUserPasswordSuccessCallback: function(data){
      let scopeObj = this;
      if(data.MFAAttributes && data.MFAAttributes.isMFARequired === "true"){
        var mfaJSON = {
          // "serviceName": mfaManager.getServiceId(),
          "flowType": "UPDATE_PASSWORD",
          "response": data,
          "objectServiceDetails" : {
            "action" : "Lock",
            "serviceName" : "RBObjects",
            "dataModel" : "DbxUser",
            "verifyOTPOperationName" : "updateDBXUserPassword",
            "requestOTPOperationName" : "updateDBXUserPassword",
            "resendOTPOperationName" : "updateDBXUserPassword",
          },
        };
        applicationManager.getMFAManager().initMFAFlow(mfaJSON);
      } else if(scopeObj.onSuccessCallback){
        scopeObj.onSuccessCallback();
      }
      kony.application.dismissLoadingScreen();
    }

  };
});