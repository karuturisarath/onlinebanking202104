define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for imgOption0 **/
    AS_Image_f165605ba66a49afb02f1a9c1264172e: function AS_Image_f165605ba66a49afb02f1a9c1264172e(eventobject, x, y) {
        var self = this;
        if (this.view.imgOption0.src === "radioinactivebb.png") {
            this.view.imgOption0.src = "radioactivebb.png";
            this.view.imgOption1.src = "radioinactivebb.png";
            this.view.flxToggleOptions.forceLayout();
            if (this.onOptionToggle !== undefined && this.onOptionToggle !== null) {
                this.onOptionToggle();
            }
        }
    },
    /** onTouchStart defined for imgOption1 **/
    AS_Image_f71336f103df447c856545b35ff5b232: function AS_Image_f71336f103df447c856545b35ff5b232(eventobject, x, y) {
        var self = this;
        if (this.view.imgOption1.src === "radioinactivebb.png") {
            this.view.imgOption1.src = "radioactivebb.png";
            this.view.imgOption0.src = "radioinactivebb.png";
            this.view.flxToggleOptions.forceLayout();
            if (this.onOptionToggle !== undefined && this.onOptionToggle !== null) {
                this.onOptionToggle();
            }
        }
    },
    /** onClick defined for btnTab1 **/
    AS_Button_c8441d90b26340da9a4ead35186efa7d: function AS_Button_c8441d90b26340da9a4ead35186efa7d(eventobject) {
        var self = this;
        this.clickTab(eventobject);
    },
    /** onClick defined for btnTab2 **/
    AS_Button_jd9b70dfe27646d98a1f528b0f609bcd: function AS_Button_jd9b70dfe27646d98a1f528b0f609bcd(eventobject) {
        var self = this;
        this.clickTab(eventobject);
    },
    /** onClick defined for btnTab3 **/
    AS_Button_g046e9a2d55c4272aba3864be1d61a12: function AS_Button_g046e9a2d55c4272aba3864be1d61a12(eventobject) {
        var self = this;
        this.clickTab(eventobject);
    },
    /** onClick defined for btnTab4 **/
    AS_Button_db967164d0a342abb7f9f3981b4202a9: function AS_Button_db967164d0a342abb7f9f3981b4202a9(eventobject) {
        var self = this;
        this.clickTab(eventobject);
    },
    /** onClick defined for btnTab5 **/
    AS_Button_b9f30741e2964a76ac9798a528158602: function AS_Button_b9f30741e2964a76ac9798a528158602(eventobject) {
        var self = this;
        this.clickTab(eventobject);
    },
    /** onClick defined for btnTab6 **/
    AS_Button_b6208369b2cd46d484df0018d4d3a46e: function AS_Button_b6208369b2cd46d484df0018d4d3a46e(eventobject) {
        var self = this;
        this.clickTab(eventobject);
    }
});